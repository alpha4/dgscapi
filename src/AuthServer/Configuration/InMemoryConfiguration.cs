﻿using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System.Collections.Generic;

namespace AuthServer.Configuration
{
    public class InMemoryConfiguration
    {
        public static IEnumerable<ApiResource> ApiResources()
        {
            return new List<ApiResource>
            {
                 new ApiResource(){
                     Name="casamielnetwork",
                     Scopes=new List<string>{ "casamielnetwork" },
                     Description="casamielApi",
                     Enabled=true
                 },

                 new ApiResource(){
                     Name="hgspApi",
                     Scopes=new List<string>{ "hgspApi" },
                     Description="hgspApi",
                     Enabled=true
                 },

                  new ApiResource(){
                     Name="cmlabApi",
                     Scopes=new List<string>{ "cmlabApi" },
                     Description="cmlabApi",
                     Enabled=true
                 },
                   new ApiResource(){
                     Name="doncoApi",
                     Scopes=new List<string>{ "doncoApi" },
                     Description="doncoApi",
                     Enabled=true
                 },
                new ApiResource("socialnetwork", "社交网络")

        };
        }
        public static IEnumerable<ApiScope> GetApiScopes()
        {
            return new List<ApiScope>
            {

                new ApiScope("socialnetwork"),
                new ApiScope("doncoApi"),

                new ApiScope("hgspApi"),
                new ApiScope("casamielnetwork"),


                new ApiScope("cmlabApi"),


            };
        }
        public static IEnumerable<IdentityResource> GetIdentityResourceResources()
        {
            return new List<IdentityResource>
           {
               new IdentityResources.OpenId(), //未添加导致scope错误
               new IdentityResources.Profile()
           };
        }

        public static IEnumerable<Client> Clients()
        {
            return new List<Client>
            {   new Client
        {
            ClientId = "client",
            ClientSecrets = new [] { new Secret("secrect".Sha256()) },
            AllowedGrantTypes = GrantTypes.ClientCredentials,
            AllowedScopes  = { "socialnetwork" }
        },new Client
        {
            ClientId = "AKIDDPSL7j0A4xqgP0tn0nV6Yq0AEAvW9EJE",
            ClientSecrets = new [] { new Secret("tUxISRFsKp9I9pcJpBMLNJ4g8LOFwUu9".Sha256()) },
            AccessTokenLifetime=3600*72,
            AllowedGrantTypes = GrantTypes.ClientCredentials,
                   //AccessTokenType= AccessTokenType.Reference,
                    //AllowedScopes  = { "casamielnetwork" }
                    AllowedScopes  = { "casamielnetwork" ,IdentityServerConstants.StandardScopes.OpenId, //必须要添加，否则报forbidden错误
                  IdentityServerConstants.StandardScopes.Profile}
        },new Client
        {
            ClientId = "hgspfirstapi",
            ClientSecrets = new [] { new Secret("9df30cJpBMLNJ4g8LOFwUu9".Sha256()) },
           AllowedGrantTypes = GrantTypes.ClientCredentials,
                   //AccessTokenType= AccessTokenType.Reference,
                    //AllowedScopes  = { "casamielnetwork" }
                     AccessTokenLifetime=3600*72,
            AllowedScopes  = { "hgspApi" ,IdentityServerConstants.StandardScopes.OpenId, //必须要添加，否则报forbidden错误
                  IdentityServerConstants.StandardScopes.Profile}
        },
        new Client
        {
            ClientId = "casafirstapi",
            ClientSecrets = new [] { new Secret("tUxISRFsKp9I9pcJpBMLNJ4g8LOFwUu9".Sha256()) },
           AccessTokenLifetime=3600*72,
           AllowedGrantTypes = GrantTypes.ClientCredentials,
                   //AccessTokenType= AccessTokenType.Reference,
                    //AllowedScopes  = { "casamielnetwork" }
                    AllowedScopes  = { "casamielnetwork" ,IdentityServerConstants.StandardScopes.OpenId, //必须要添加，否则报forbidden错误
                  IdentityServerConstants.StandardScopes.Profile}
                },
        new Client
        {
            ClientId = "cmlabfirstapi",
            ClientSecrets = new [] { new Secret("tUxISRFsKs90p9I9pcda3sJpBMLNJ4g8LOFwUu9".Sha256()) },
           AccessTokenLifetime=3600*10,
           AllowedGrantTypes = GrantTypes.ClientCredentials,
                   //AccessTokenType= AccessTokenType.Reference,
                    //AllowedScopes  = { "casamielnetwork" }
					AllowedScopes  = { "cmlabApi" ,IdentityServerConstants.StandardScopes.OpenId, //必须要添加，否则报forbidden错误
                  IdentityServerConstants.StandardScopes.Profile}
        },
        new Client
        {
            ClientId="doncoApi2018",
            ClientSecrets=new []{new Secret("dtxISRFsKs9ap9I9pc3da3sJpBMLNJxJg8LOF1iUu9".Sha256())},
            AccessTokenLifetime=3600*72,
            AllowedGrantTypes=GrantTypes.ClientCredentials,
                    AllowedScopes={"doncoApi"}
        }
        };
        }
        //openssl req -newkey rsa:2014 -nodes -keyout socialnetwork.key -x509 -days 365 -out socialnetwork.cer
        public static IEnumerable<TestUser> Users()
        {
            return new[]
            {                new TestUser
        {
            SubjectId = "1",
            Username = "mail@qq.com",
            Password = "password"
        }
        };
        }
    }
}
