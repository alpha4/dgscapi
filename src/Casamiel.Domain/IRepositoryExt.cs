﻿using Smooth.IoC.Repository.UnitOfWork;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// Repository ext.
    /// </summary>
	public interface IRepositoryExt<TEntity, TPk> : IRepository<TEntity, TPk>
	   where TEntity : class
	   where TPk : IComparable
	{
        /// <summary>
        /// Save the specified entity and uow.
        /// </summary>
        /// <param name="entity">Entity.</param>
        /// <param name="uow">Uow.</param>
		void Save(TEntity entity, IUnitOfWork uow);
		/// <summary>
        /// Saves the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="entity">Entity.</param>
        /// <param name="uow">Uow.</param>
        Task SaveAsync(TEntity entity, IUnitOfWork uow);
		/// <summary>
        /// Save the specified entity.
        /// </summary>
        /// <param name="entity">Entity.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        void Save<TSession>(TEntity entity) where TSession : class, ISession;
		/// <summary>
        /// Saves the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="entity">Entity.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task SaveAsync<TSession>(TEntity entity) where TSession : class, ISession;
		/// <summary>
        /// Update the specified entity and uow.
        /// </summary>
        /// <returns>The update.</returns>
        /// <param name="entity">Entity.</param>
        /// <param name="uow">Uow.</param>
        bool Update(TEntity entity, IUnitOfWork uow);
		/// <summary>
        /// Updates the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="entity">Entity.</param>
        /// <param name="uow">Uow.</param>
        Task<bool> UpdateAsync(TEntity entity, IUnitOfWork uow);
		/// <summary>
        /// Update the specified entity.
        /// </summary>
        /// <returns>The update.</returns>
        /// <param name="entity">Entity.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        bool Update<TSession>(TEntity entity) where TSession : class, ISession;
		/// <summary>
        /// 
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="entity">Entity.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<bool> UpdateAsync<TSession>(TEntity entity) where TSession : class, ISession;
		/// <summary>
        /// Gets the list by page.
        /// </summary>
        /// <returns>The list by page.</returns>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="sqlwhere">Sqlwhere.</param>
        /// <param name="orderby">Orderby.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<PagingResult<TEntity>> GetListAsync<TSession>(int pageIndex, int pageSize, string sqlwhere, string orderby) where TSession : class, ISession;
	}
}
