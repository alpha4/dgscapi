﻿using System;
namespace Casamiel.Domain.Request
{
    /// <summary>
    /// Remove sign in content request.
    /// </summary>
    public class RemoveSignInContentRequest
    {
        /// <summary>
        /// Gets or sets the content identifier.
        /// </summary>
        /// <value>The content identifier.</value>
        public int ContentId { get; set; }
    }
}
