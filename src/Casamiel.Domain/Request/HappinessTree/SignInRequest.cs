﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request.HappinessTress
{
    /// <summary>
    ///打卡请求
    /// </summary>
    public class SignInRequest
    {
        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>The content.</value>
        [StringLength(800,ErrorMessage ="内容最长不能超过800")]
        public string Content { get; set; }

        /// <summary>
        /// Gets or sets the image list.
        /// </summary>
        /// <value>The image list.</value>
        public List<string> ImageList { get; set; }

        /// <summary>
        ///  
        /// 0 正常提交 1 保留信息
        /// </summary>
        /// <value>The type.</value>
        public int Type { get; set; }
    }
}
