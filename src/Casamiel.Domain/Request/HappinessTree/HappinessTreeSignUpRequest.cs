﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request.HappinessTree
{
    /// <summary>
    /// Happiness tree sign up request.
    /// </summary>
    public class HappinessTreeSignUpRequest
    {
         
        /// <summary>
        /// 父亲名字
        /// </summary>
        public string FatherName { get; set; }
        /// <summary>
        /// 母亲名称
        /// </summary>
        public string MotherName { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 小孩年龄
        /// </summary>
        public int BabyAge { get; set; }

        /// <summary>
        /// 推荐门店
        /// </summary>
         
        public string StoreName { get; set; }
    }
}
