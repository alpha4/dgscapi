﻿using System;
namespace Casamiel.Domain.Request.HappinessTree
{
    /// <summary>
    /// Liked state change request.
    /// </summary>
    public class LikedStateChangeRequest
    {
        /// <summary>
        /// 0赞，1不赞
        /// </summary>
        /// <value>The type.</value>
        public int Type { get; set; }

        /// <summary>
        /// Gets or sets the content identifier.
        /// </summary>
        /// <value>The content identifier.</value>
        public int ContentId { get; set; }
    }
}
