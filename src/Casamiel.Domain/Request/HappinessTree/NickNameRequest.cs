﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request.HappinessTree
{
    /// <summary>
    /// Add nick name request.
    /// </summary>
    public class AddNickNameRequest
    {

        /// <summary>
        /// Gets or sets the name of the nick.
        /// </summary>
        /// <value>The name of the nick.</value>
        [Required]
        public string NickName { get; set; }
    }
}
