﻿using System;
namespace Casamiel.Domain.Request
{
    /// <summary>
    /// Get my sign in content list request.
    /// </summary>
    public class GetMySignInContentListRequest
    {
        /// <summary>
        /// Gets or sets the index of the page.
        /// </summary>
        /// <value>The index of the page.</value>
        public int PageIndex { get; set; }

        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; set; }
    }
}
