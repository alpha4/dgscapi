﻿using System;
namespace Casamiel.Domain.Request.HappinessTree
{
    /// <summary>
    /// Get rewards request.
    /// </summary>
    public class GetRewardsRequest
    {
        /// <summary>
        ///奖品id
        /// </summary>
        /// <value>The reward identifier.</value>
        public int RewardId { get; set; }
    }
}
