﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 
    /// </summary>
    public class GetMyCouponListRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 状态 0、已使用；1、已过期；3、可使用
        /// </summary>
        public int State { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetTicketCodeByIdRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public int McID { get; set; }
    }
}
