﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 消费
    /// </summary>
    public class IcconsumeReq
    {
        /// <summary>
        /// 
        /// </summary>
        public class IcconsumeProductItem
        {
            /// <summary>
            /// 产品标识
            /// </summary>
            /// 
            [Required(ErrorMessage = "请输入产品标识")]
            public string Pid { get; set; }
            /// <summary>
            /// 商品名称
            /// </summary>
            public string Pname { get; set; }
            /// <summary>
            /// 
            /// </summary>
            [Required(ErrorMessage = "请输入产品数量")]
            public int Count { get; set; }
        }

        //public string wxopenid { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phoneno { get; set; }

        /// <summary>
        /// 卡号
        /// </summary>
        public string Cardno { get; set; }

        /// <summary>
        /// 店标识
        /// </summary>
        [Required(ErrorMessage = "请输入门店标识")]
        public int Shopid { get; set; }
        /// <summary>
        /// 门店名称
        /// </summary>
        public string Shopname { get; set; }
        /// <summary>
        /// 需要发票
        /// </summary>
        /// <value><c>true</c> if createinvoice; otherwise, <c>false</c>.</value>
        public bool Createinvoice { get; set; }
        /// <summary>
        /// 折扣
        /// </summary>
        /// <value>The rebate.</value>
        public double Rebate { get; set; }

        /// <summary>
        /// 提货时间
        /// 格式为yyyy-MM-dd HH-mm-ss
        /// </summary>
        [Required(ErrorMessage = "请输入提货时间")]
        public string Pickuptime { get; set; }

        /// <summary>
        /// 商品
        /// </summary>
        public List<IcconsumeProductItem> Product { get; set; }

        /// <summary>
        /// 来源
        /// 1微信，15手机App（）默认,16pad,23 自有外卖商城，24自有蛋糕商城，25美团，26饿了么，28京东到家，29有赞订单，30口碑，31天猫，32抖音,33 自有商城全国送
        /// </summary>

        public int Datasource { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(typeof(string), "")]
        public string Timestamp { get; set; }


    }
}
