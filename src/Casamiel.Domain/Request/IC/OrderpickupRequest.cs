﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request.IC
{
    /// <summary>
    /// 提货请求
    /// </summary>
    public class OrderpickupRequest
    {
        /// <summary>
        /// 会原卡号
        /// </summary>
        public string Cardno { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string Phoneno { get; set; }
        /// <summary>
        /// 唯一交易号
        /// </summary>
        /// 
        [Required(ErrorMessage = "唯一交易号")]
        public string Tradeno { get; set; }
        /// <summary>
        /// 2018-02-16 12:01:02
        /// 格式为yyyy-MM-dd HH-mm-ss
        /// </summary>
        public string Timestamp { get; set; }

    }
}
