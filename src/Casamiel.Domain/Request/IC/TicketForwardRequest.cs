﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request.IC
{
	/// <summary>
	/// 会员券转赠
	/// </summary>
	public class TicketForwardRequest
	{
		/// <summary>
		/// 
		/// </summary>
		public long  Ticketid{ get; set; }

		/// <summary>
		/// 会员惠券名称
		/// </summary>
	    public string TicketName { get; set; }

		/// <summary>
		/// 金额
		/// </summary>
		public decimal Je { get; set; }
		/// <summary>
		/// 
		/// </summary>
		[StringLength(240, ErrorMessage = "最多240个字")]
		public string Message { get; set; }
	}
}
