﻿using System;
namespace Casamiel.Domain.Request.IC
{
    /// <summary>
    /// 删除团购订单
    /// </summary>
    public class GrouporderdeleteReq
    {
        /// <summary>
        /// 交易单号
        /// </summary>
        /// <value>The tradeno.</value>
        public string Tradeno { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>The timestamp.</value>
        public string Timestamp { get; set; }
    }
}
