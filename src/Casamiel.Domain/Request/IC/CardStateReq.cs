﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
	/// <summary>
	/// 卡状态设置
	/// </summary>
	public class CardStateReq
	{
		/// <summary>
		/// 卡号
		/// </summary>
		[Required]
		public string Cardno { get; set; }
		/// <summary>
		/// 手机号
		/// </summary>
		[Required]
		public string Phoneno { get; set; }

		/// <summary>
		/// 状态
		/// state：1、启用实体卡；0、停用实体卡
		/// </summary>
		[Required]
		public int State { get; set; }

		/// <summary>
		/// Gets or sets the timestamp.
		/// </summary>
		/// <value>The timestamp.</value>
		public string Timestamp { get; set; }
	}
}
