﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Casamiel.Domain.Request.IC
{
    /// <summary>
    /// Ticket largess cancel req.
    /// </summary>
    public class TicketLargessCancelReq
    {

        /// <summary>
        /// Gets or sets the largessno.
        /// </summary>
        /// <value>The largessno.</value>
        [Required(ErrorMessage = "单据号不能空")]
        public string Largessno { get; set; }
        /// <summary>
        /// Gets or sets the datasource.
        /// </summary>
        /// <value>The datasource.</value>
        public int Datasource { get; set; }
        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>The timestamp.</value>
        public string Timestamp { get; set; }
    }
}
 
