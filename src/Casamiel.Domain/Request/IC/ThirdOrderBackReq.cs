﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request.IC
{
    /// <summary>
    /// Third order back req.
    /// </summary>
    public class ThirdOrderBackReq
    {
        /// <summary>
        /// 唯一交易号
        /// </summary>
        /// 
        [Required(ErrorMessage = "唯一交易号")]
        public string Tradeno { get; set; }
        /// <summary>
        /// 2016-02-16 12:01:02
        /// 格式为yyyy-MM-dd HH-mm-ss
        /// </summary>

        public string Timestamp { get; set; }  
    }
}
