﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Request.IC
{
	/// <summary>
	/// 
	/// </summary>
	public class ICPushPlanRequst
	{
		/// <summary>
		/// 
		/// </summary>
		public string Pushurl { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public bool Start { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Timestamp { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public List<Pushplan> Pushplan { get; set; }
		/// <summary>
		/// 2018-08-26
		/// </summary>
		public string Startdate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Datasource { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Enddate { get; set; }

		/// <summary>
		/// 时间间隔，单位：秒，最小1秒
		/// </summary>
		public string Cardtype { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	public class Pushplan
	{

		/// <summary>
		/// 格式：HH:mm:ss，24小时制，默认 00:00:00
		/// </summary>
		public string Startdate { get; set; }

		/// <summary>
		/// 格式：HH:mm:ss，24小时制，默认 00:00:00
		/// </summary>
		public string Enddate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int Interval { get; set; }
	}
}
