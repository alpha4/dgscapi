﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 支付方式
    /// </summary>
    public class IcConsumepayReq
    {
        /// <summary>
        /// 
        /// </summary>
        public class PaycontentItem
		{
            /// <summary>
            /// （1、微信支付；2、支付宝；3、会员卡；4、银行卡；5、消费券；6、会员券；7、网购支付；8、充值码；9、京东到家；10、饿了么；11、美团券；12、美团外卖；）
            /// </summary>
            [Required(ErrorMessage = "请输入支付方式")]
            public string Paytype { get; set; }
            /// <summary>
            /// 
            /// </summary>
            /// 
            [Required(ErrorMessage = "支付金额")]
            public double Paymoney { get; set; }
            /// <summary>
            /// 
            /// </summary>
            [StringLength(64, ErrorMessage = "支付单号最大长度为64位")]
            [Required(ErrorMessage = "请输入门支付单号")]
            public string Paytradeno { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string Payuser { get; set; }
            /// <summary>
            /// 商户号
            /// </summary>
            public string Payid { get; set; }

        }
        /// <summary>
        /// Payfee.
        /// </summary>
        public class PayfeeItem
        {
            /// <summary>
            /// Gets or sets the feetype.
            /// </summary>
            /// <value>The feetype.</value>
            public string Feetype { get; set; }
            /// <summary>
            /// Gets or sets the fee.
            /// </summary>
            /// <value>The fee.</value>
            public double Fee { get; set; }
            /// <summary>
            /// Gets or sets the description.
            /// </summary>
            /// <value>The description.</value>
            public string Description { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Wxopenid { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phoneno { get; set; }
        /// <summary>
        /// 是否发票
        /// </summary>
        public bool Createinvoice { get; set; } = false;
        /// <summary>
        /// 卡号
        /// </summary>
        public string Cardno { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// 
        [Required(ErrorMessage = "请输入门店标识")]
        public int Shopid { get; set; }
        /// <summary>
        /// 门店名称
        /// </summary>
        public string Shopname { get; set; }

        /// <summary>
        /// 唯一交易号
        /// </summary>

        [Required(ErrorMessage = "请输入唯一交易号")]
        public string Tradeno { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// 
        [Required(ErrorMessage = "请输入支付方")]
        public List<PaycontentItem> Paycontent { get; set; }
        /// <summary>
        /// Gets or sets the payfees.
        /// </summary>
        /// <value>The payfee.</value>
        public List<PayfeeItem> Payfee { get; set; }
        /// <summary>
        /// 2016-02-16 12:01:02
        /// 格式为yyyy-MM-dd HH-mm-ss
        /// </summary>
        public string Timestamp { get; set; }
    }
}
