﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Request.IC
{
	/// <summary>
	/// 提货单退货
	/// </summary>
	public class IcSellBackRequest
	{
		/// <summary>
		/// 
		/// </summary>
		public string Tradeno;

		/// <summary>
		/// 退货入库后，是否生成报废出库单，默认是 true
		/// </summary>
		public bool Storeout { get; set; }

		/// <summary>
		/// paycontent：退部分产品时的金额明细。如果整单退款，不需要传这个参数
		/// </summary>
		public List<PayContnetItem> Paycontent;

		/// <summary>
		/// 推部分产品
		/// </summary>
		public List<ProductItem> Product = new List<ProductItem>();

	}

	/// <summary>
	/// 团购退货
	/// </summary>
	public class GroupOrderBackRequest
	{
		/// <summary>
		/// 
		/// </summary>
		public string Tradeno;
		 

		/// <summary>
		/// paycontent：退部分产品时的金额明细。如果整单退款，不需要传这个参数
		/// </summary>
		public List<PayContnetItem> Paycontent;

		/// <summary>
		/// 推部分产品
		/// </summary>
		public List<ProductItem> Product = new List<ProductItem>();

	}

	/// <summary>
	/// 
	/// </summary>
	public class GroupSellBackRequest
	{
		/// <summary>
		/// 
		/// </summary>
		public string Tradeno;
		/// <summary>
		/// 
		/// </summary>
		public bool Storeout = true;


		/// <summary>
		/// paycontent：退部分产品时的金额明细。如果整单退款，不需要传这个参数
		/// </summary>
		public List<PayContnetItem> Paycontent;

		/// <summary>
		/// 推部分产品
		/// </summary>
		public List<ProductItem> Product = new List<ProductItem>();

	  }


	/// <summary>
	/// 
	/// </summary>
	public class PayContnetItem
	{
		/// <summary>
		/// 
		/// </summary>
		public string Paytype;

		/// <summary>
		/// 
		/// </summary>
		public decimal Paymoney;

		/// <summary>
		/// 
		/// </summary>
		public string Paytradeno;

		/// <summary>
		/// 
		/// </summary>
		public string Payuser = "payuserid";
	}
}
