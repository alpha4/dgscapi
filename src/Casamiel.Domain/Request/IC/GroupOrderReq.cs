﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request.IC
{
    /// <summary>
    /// Group order req.
    /// </summary>
    public class GroupOrderReq
    {
        /// <summary>
        /// 卡号，建议填写.
        /// </summary>
        /// <value>The cardno.</value>

        public string Cardno { get; set; }

        

        /// <summary>
        ///消费门店标识（1.1版以上必填）
        /// </summary>
        /// <value>The shopid.</value>

        public int Shopid { get; set; }

        /// <summary>
        /// 取货时间
        /// 2018-07-01 00:00:00
        /// </summary>
        /// <value>The pickuptimev.</value>
        [Required]
        public string Pickuptime { get; set; }
        //stocklock：布尔值，是否需要锁（预留）库存
        //customerid：客户标识
        /// <summary>
        ///客户名称，单位名称或个人姓名，如果系统不存在这个单位名称或个人姓名，将创建一个。
        /// </summary>
        /// <value>The customername.</value>
        public string Customername { get; set; }
        /// <summary>
        /// 业务手机
        /// </summary>
        /// <value>The sellerphone.</value>
        [Required(ErrorMessage = "业务员手机号不能空")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "手机号11位")]
        public string Sellerphone { get; set; }
        /// <summary>
        ///字段如果为 true，将记录为挂空帐订单，将在后台审核后自动完成【产品销售发货、物流配送、配送收货】操作
        /// <see cref="T:Casamiel.API.Application.Models.ICModels.GroupOrderReq"/> is emptyaccount.
        /// </summary>
        /// <value><c>true</c> if emptyaccount; otherwise, <c>false</c>.</value>
        public bool Emptyaccount { get; set; }
        /// <summary>
        ///消费产品清单
        /// </summary>
        /// <value>The product.</value>
        public List<ProductItem> Product { get; set; }
        /// <summary>
        /// 客户信息
        /// </summary>
        /// <value>The customerinfo.</value>
        public Customerinfo Customerinfo { get; set; }

		/// <summary>
		/// 备注
		/// </summary>
		public string Memo { get; set; }
		/// <summary>
		/// Gets or sets the timestamp.
		/// </summary>
		/// <value>The timestamp.</value>

		public string Timestamp { get; set; }


    }
}
