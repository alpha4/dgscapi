﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Casamiel.Domain.Request.IC
{
    /// <summary>
    /// Add tickets req.
    /// </summary>
    public class AddTicketsReq
    {
        /// <summary>
        /// Gets or sets the phoneno.
        /// </summary>
        /// <value>The phoneno.</value>
        [Required(ErrorMessage = "手机号不能空")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "手机号11位")]
        public string Phoneno { get; set; }

        /// <summary>
        /// Gets or sets the cardno.
        /// </summary>
        /// <value>The cardno.</value>
        [Required(ErrorMessage = "卡号不能空")]
        public string Cardno { get; set; }
        /// <summary>
        /// Gets or sets the largesstickets.
        /// </summary>
        /// <value>The largesstickets.</value>
        public List<Ticket> Largesstickets { get; set; }
        /// <summary>
        /// Gets or sets the datasource.
        /// </summary>
        /// <value>The datasource.</value>
        public string Datasource { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>The timestamp.</value>
        public string Timestamp { get; set; }
    }

    /// <summary>
    /// Ticket.
    /// </summary>
    public class Ticket
    {
        /// <summary>
        /// Gets or sets the ticketid.
        /// </summary>
        /// <value>The ticketid.</value>
        public int Ticketid { get; set; }

        /// <summary>
        /// Gets or sets the ticketname.
        /// </summary>
        /// <value>The ticketname.</value>
        public string Ticketname { get; set; }

        /// <summary>
        /// Gets or sets the ticketprice.
        /// </summary>
        /// <value>The ticketprice.</value>
        public double Ticketprice { get; set; }

        /// <summary>
        /// Gets or sets the ticketnum.
        /// </summary>
        /// <value>The ticketnum.</value>
        public int Ticketnum { get; set; }
    }
}
