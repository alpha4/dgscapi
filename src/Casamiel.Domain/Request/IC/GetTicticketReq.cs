﻿using System;
namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 
    /// </summary>
    public class GetTicticketReq
    {

        /// <summary>
        /// 卡号
        /// </summary>
        public string Cardno { get; set; }

        /// <summary>
        /// 1、已使用；2、未使用；3、已过期；4、其它(如被
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Pageindex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Pagesize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Timestamp { get; set; }
    }
}
