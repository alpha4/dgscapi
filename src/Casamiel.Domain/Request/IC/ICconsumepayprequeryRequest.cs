﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request.IC
{
    /// <summary>
	/// 
	/// </summary>
	public class ICconsumepayprequeryRequest
    {
        //public string wxopenid { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phoneno { get; set; }

        /// <summary>
        /// 卡号
        /// </summary>
        public string Cardno { get; set; }

        /// <summary>
        /// 店标识
        /// </summary>
        [Required(ErrorMessage = "请输入门店标识")]
        public int Shopid { get; set; }
        /// <summary>
        /// 门店名称
        /// </summary>
        public string Shopname { get; set; }
        /// <summary>
        /// 折扣
        /// </summary>
        /// <value>The rebate.</value>

        public double Rebate { get; set; }

        /// <summary>
        /// 提货时间
        /// 格式为yyyy-MM-dd HH-mm-ss
        /// </summary>
        [Required(ErrorMessage = "请输入提货时间")]
        public string Pickuptime { get; set; }

        /// <summary>
		/// 默认为true（需配送），如果为 false，即使没有配送费用，也可以折扣
		/// </summary>
        public bool Needdelivery { get; set; }

        /// <summary>
        /// 商品
        /// </summary>
        public List<ICconsumepayprequeryProductItem> Product { get; set; }

        /// <summary>
		/// 
		/// </summary>
        [Required(ErrorMessage = "请输入支付方")]
        public List<PayItem> Paycontent { get; set; }

        /// <summary>
		/// 
		/// </summary>
        public List<PayfeeDetail> Payfee { get; set; }
        /// <summary>
        /// 来源
        /// 1微信，15手机App（）默认,16pad,23 自有外卖商城，24自有蛋糕商城，25美团，26饿了么，28京东到家，29有赞订单，30口碑，31天猫，32抖音,33 自有商城全国送
        /// </summary>

        public int Datasource { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(typeof(string), "")]
        public string Timestamp { get; set; }


        /// <summary>
        /// Payfee.
        /// </summary>
        public class PayfeeDetail
        {
            /// <summary>
            /// Gets or sets the feetype.
            /// </summary>
            /// <value>The feetype.</value>
            public string Feetype { get; set; }
            /// <summary>
            /// Gets or sets the fee.
            /// </summary>
            /// <value>The fee.</value>
            public double Fee { get; set; }
            /// <summary>
            /// Gets or sets the description.
            /// </summary>
            /// <value>The description.</value>
            public string Description { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class ICconsumepayprequeryProductItem
        {
            /// <summary>
            /// 产品标识
            /// </summary>
            /// 
            [Required(ErrorMessage = "请输入产品标识")]
            public string Pid { get; set; }
            /// <summary>
            /// 商品名称
            /// </summary>
            public string Pname { get; set; }

            /// <summary>
            /// 
            /// </summary>
            [Required(ErrorMessage = "请输入产品数量")]
            public int Count { get; set; }
        }

        /// <summary>
		/// 
		/// </summary>
        public class PayItem
        {
            /// <summary>
            /// （1、微信支付；2、支付宝；3、会员卡；4、银行卡；5、消费券；6、会员券；7、网购支付；8、充值码；9、京东到家；10、饿了么；11、美团券；12、美团外卖；）
            /// </summary>
            [Required(ErrorMessage = "请输入支付方式")]
            public string Paytype { get; set; }
            /// <summary>
            /// 
            /// </summary>
            /// 
            [Required(ErrorMessage = "支付金额")]
            public double Paymoney { get; set; }
            /// <summary>
            /// 
            /// </summary>
            [StringLength(64, ErrorMessage = "支付单号最大长度为64位")]
            [Required(ErrorMessage = "请输入门支付单号")]
            public string Paytradeno { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string Payuser { get; set; }
            /// <summary>
            /// 商户号
            /// </summary>
            public string Payid { get; set; }

        }
    }
}
