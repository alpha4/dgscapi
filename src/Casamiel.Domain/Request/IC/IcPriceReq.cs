﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Casamiel.Domain.Request.IC
{
    /// <summary>
    /// 查价格
    /// </summary>
    public class IcPriceReq
    {
        /// <summary>
        /// 
        /// </summary>
        public string Wxopenid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Cardno { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// 
        [Required]
        public int Shopid { get; set; }
        /// <summary>
        /// 店名
        /// </summary>
        public string Shopname { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<ProductItemReq> Product { get; set; }

        /// <summary>
		/// 
		/// </summary>
        public int Datasource { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Timestamp { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class ProductItemReq
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Pid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Pname { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int Count { get; set; }
 
    }
}
