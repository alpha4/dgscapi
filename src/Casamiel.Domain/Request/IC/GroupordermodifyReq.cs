﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request.IC
{


	/// <summary>
	/// Customerinfo.
	/// </summary>
	public class Customerinfo
	{
		/// <summary>
		/// 网上订单号（对方订单号)
		/// </summary>
		public string Weborderno { get; set; }
		/// <summary>
		/// 内部web团购订单
		/// </summary>
		public string Webordersource { get; set; }
		/// <summary>
		/// 订货人姓名
		/// </summary>
		public string Orderername { get; set; }
		/// <summary>
		/// 订货人电话
		/// </summary>
		public string Ordererphone { get; set; }
		/// <summary>
		/// 订货时间
		/// </summary>
		public string Ordertime { get; set; }
		/// <summary>
		/// 收货人姓名
		/// </summary>
		public string Takername { get; set; }

		/// <summary>
		/// 备注
		/// </summary>
		public string Memo { get; set; }
		/// <summary>
		/// 收货人电话
		/// </summary>
		public string Takerphone { get; set; }
		/// <summary>
		/// 收货时间（同订单的提货时间）
		/// </summary>
		public string Taketime { get; set; }
		/// <summary>
		/// 收货省市区县
		/// </summary>
		public string Takeaddressregion { get; set; }
		/// <summary>
		/// 收货详细地址，详细地=takeaddressregion+takeaddress
		/// </summary>
		public string Takeaddress { get; set; }
		/// <summary>
		/// 生产要求
		/// </summary>
		public string Requirement { get; set; }
		/// <summary>
		/// 送货员姓名
		/// </summary>
		public string Deliveryman { get; set; }
		/// <summary>
		/// 送货车牌号
		/// </summary>
		public string Deliveryno { get; set; }
		/// <summary>
		/// 送货员联系方式
		/// </summary>
		public string Deliveryphone { get; set; }
		/// <summary>
		///运输方式
		/// </summary>
		public string Deliverystyle { get; set; }
		/// <summary>
		/// 送达时间
		/// </summary>
		public string Deliverytime { get; set; }
		/// <summary>
		/// 发票类型：1、增值税普票；9、增值税专票
		/// </summary>
		public int Invoicetype { get; set; }
		/// <summary>
		/// 纳税人识别号，如：918742963582678542
		/// </summary>
		public string Taxid { get; set; }
		/// <summary>
		/// 开户行，如：工商银行
		/// </summary>
		public string Banktype { get; set; }
		/// <summary>
		/// 开户行帐号
		/// </summary>
		public string Bankaccount { get; set; }
		/// <summary>
		/// xx街道xx号xx公司
		/// </summary>
		public string Invoiceaddress { get; set; }
		/// <summary>
		/// 开票抬头（建议填写单位名称）
		/// </summary>
		public string Invoicehead { get; set; }
		/// <summary>
		/// 发票内容
		/// </summary>
		public string Invoicetitle { get; set; }

	}
	/// <summary>
	/// GroupordermodifyReq.
	/// </summary>
	public class GroupOrdermobifyRequest
	{
		/// <summary>
		/// Gets or sets the tradeno.
		/// </summary>
		/// <value>The tradeno.</value>
		public string Tradeno { get; set; }//”:”L51S12D3456”,

		/// <summary>
		/// Gets or sets the cardno.
		/// </summary>
		/// <value>The cardno.</value>
		public string Cardno { get; set; }

		/// <summary>
		/// Gets or sets the shopid.
		/// </summary>
		/// <value>The shopid.</value>
		public int Shopid { get; set; }

		/// <summary>
		/// Gets or sets the customername.
		/// </summary>
		/// <value>The customername.</value>
		public string Customername { get; set; }

		/// <summary>
		/// Gets or sets the pickuptime.
		/// </summary>
		/// <value>The pickuptime.</value>
		[Required]
		public string Pickuptime { get; set; }

		/// <summary>
		/// Gets or sets the sellerphone.
		/// </summary>
		/// <value>The sellerphone.</value>
		[Required(ErrorMessage = "业务员手机号不能空")]
		[StringLength(11, MinimumLength = 11, ErrorMessage = "手机号11位")]
		public string Sellerphone { get; set; }

		/// <summary>
		/// Gets or sets the listername.
		/// </summary>
		/// <value>The listername.</value>
		public string Listername { get; set; }

		/// <summary>
		/// Gets or sets the product.
		/// </summary>
		/// <value>The product.</value>
		public List<ProductItem> Product { get; set; }

		/// <summary>
		/// Gets or sets the customerinfo.
		/// </summary>
		/// <value>The customerinfo.</value>
		public Customerinfo Customerinfo { get; set; }
		/// <summary>
		/// Gets or sets the timestamp.
		/// </summary>
		/// <value>The timestamp.</value>

		public string Timestamp { get; set; }
	}
	/// <summary>
	/// Product item.
	/// </summary>
	public class ProductItem
	{
		/// <summary>
		/// 产品标识
		/// </summary>
		public string Pid { get; set; }

		/// <summary>
		/// 产品名称
		/// </summary>
		public string Pname { get; set; }

		/// <summary>
		/// 产品数量
		/// </summary>
		public int Count { get; set; }
		/// <summary>
		/// 折扣率
		/// 大于0小于等于1
		/// </summary>
		/// <value>The rebaterate.</value>
		public decimal Rebaterate { get; set; }
	}
}
