﻿using System;
using System.ComponentModel;

namespace Casamiel.Domain.Request.IC
{
	/// <summary>
	/// 会员卡转账
	/// </summary>
	public class ICTransferRequest
	{
		/// <summary>
		/// 礼品卡
		/// </summary>
		public string Cardnosource { get; set; }


		/// <summary>
		/// 转帐金额，可填写，如果不填写，将转出 cardnosource 所有金额
		/// </summary>
		public decimal Transfermoney { get; set; }
		/// <summary>
		/// 转入卡号
		/// </summary>
		public string Cardno { get; set; }

		/// <summary>
		/// 来源
		/// 1微信，15手机App（）默认,16pad,23 自有外卖商城，24自有蛋糕商城，25美团，26饿了么，28京东到家，29有赞订单，30口碑，31天猫，32抖音,33 自有商城全国送
		/// </summary>

		public int Datasource { get; set; }
		/// <summary>
		/// 
		/// </summary>
		[DefaultValue(typeof(string), "")]
		public string Timestamp { get; set; }


	}
}
