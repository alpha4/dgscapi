﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 
    /// </summary>
    public  class TmticketchangeRequest
    { 
        /// <summary>
        /// 
        /// </summary>
        public string Ticketcode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Timestamp { get; set; }
    }

	/// <summary>
	/// 
	/// </summary>
	public class IcticketchangeRequest
    {
        /// <summary>
        /// 
        /// </summary>
		public string Cardno { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Ticketid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Timestamp { get; set; }

    }

    /// <summary>
    /// 修改会员信息
    /// </summary>
    public class IcModifyRequest
    {

        /// <summary>
        /// 卡号，必填
        /// </summary>
        [Required(ErrorMessage = "卡号，必填")]
        public string Cardno { get; set; }

        /// <summary>
        /// 新手机号
        /// </summary>
        public string Newphoneno { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Timestamp { get; set; }

    }
}
