﻿namespace Casamiel.Domain.Request.IC
{
    /// <summary>
    /// 
    /// </summary>
    public class TMTicketRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public int Productid { get; set; }

        /// <summary>
        /// yyyy-MM-dd 
        /// </summary>
        public string Startdate { get; set; }

        /// <summary>
        /// yyyy-MM-dd 
        /// </summary>
        public string Enddate { get; set; }

        /// <summary>
        /// 售价
        /// </summary>
        public decimal Sellprice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Timestamp { get; set; }
    }
}