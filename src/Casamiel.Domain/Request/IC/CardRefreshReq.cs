﻿using System;
namespace Casamiel.Domain.Request
{
	/// <summary>
	/// Card refresh req
	/// </summary>
	public class CardRefreshRequest
	{
		/// <summary>
		/// Gets or sets the phoneno.
		/// </summary>
		/// <value>The phoneno.</value>
		public string Phoneno { get; set; }

		/// <summary>
		/// Gets or sets the wxopenid.
		/// </summary>
		/// <value>The wxopenid.</value>
		public string Wxopenid { get; set; }

		/// <summary>
		/// Gets or sets the cardno.
		/// </summary>
		/// <value>The cardno.</value>
		public string Cardno { get; set; }

		/// <summary>
		/// Gets or sets the startdate.
		/// </summary>
		/// <value>The startdate.</value>
		public string Startdate { get; set; }

		/// <summary>
		/// Gets or sets the enddate.
		/// </summary>
		/// <value>The enddate.</value>
		public string Enddate { get; set; }

		/// <summary>
		/// Gets or sets the timestamp.
		/// </summary>
		/// <value>The timestamp.</value>
		public string Timestamp { get; set; }

	}
}
