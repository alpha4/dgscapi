﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request.IC
{
    /// <summary>
    /// List sell req.
    /// </summary>
    public class ListSellReq
    {

        /// <summary>
        ///单据类型，7、产品销售；65、券卡销售。必填
        /// </summary>
        [Required(ErrorMessage = "listtype必填")]
        public int Listtype { get; set; }
        /// <summary>
        /// 门店id
        /// </summary>

        public int Shopid { get; set; }

        /// <summary>
        /// 客户标识
        /// </summary>
        public int Customerid { get; set; }
        /// <summary>
        /// 客户名称，单位名称或个人姓名，如果系统不存在这个单位名称或个人姓名，将创建一个
        /// </summary>
        public string Customername { get; set; }
        /// <summary>
        /// Gets or sets the sellerphone.
        /// </summary>
        /// <value>The sellerphone.</value>
        [Required(ErrorMessage = "业务员手机号不能空")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "手机号11位")]
        public string Sellerphone { get; set; }
        /// <summary>
        /// 业务员姓名
        /// </summary>
        public string Sellername { get; set; }


        /// <summary>
        /// 备注
        /// </summary>
        /// <value>The bz.</value>
        public string Bz { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Memo { get; set; }
		/// <summary>
		/// 销售时间
		/// 格式2018-12-11
		/// </summary>
		public string Listdate { get; set; }

        /// <summary>
        /// 消费产品清单
        /// </summary>
        public List<ProductItem> Product { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Customerinfo Customerinfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Datasource { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Timestamp { get; set; }
    
         
    }
}
