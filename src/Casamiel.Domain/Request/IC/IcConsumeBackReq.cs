﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 退款
    /// </summary>
    public class IcConsumeBackReq
    {
        /// <summary>
        /// 会原卡号
        /// </summary>
        public string Cardno { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Phoneno { get; set; }
        /// <summary>
        /// 唯一交易号
        /// </summary>
        /// 
        [Required(ErrorMessage = "唯一交易号")]
        public string Tradeno { get; set; }
        /// <summary>
        /// 2016-02-16 12:01:02
        /// 格式为yyyy-MM-dd HH-mm-ss
        /// </summary>
        public string Timestamp { get; set; }

    }
}
