﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Request.Waimai;

namespace Casamiel.Domain.Request.CakeMall
{
    /// <summary>
    /// 
    /// </summary>
    public class  OrderPreviewRequest
    {
		/// <summary>
		/// 商品信息
		/// </summary>
		public List<TakeOutOrderGoods> GoodsList { get; set; }

		/// <summary>
		/// 门店id
		/// </summary>
		public int StoreId { get; set; }
        
        /// <summary>
        /// 优惠券信息
        /// </summary>
        public List<OrderDiscountReq> DiscountList { get; set; }
 

        /// <summary>
        /// 
        /// </summary>
        public int ConsigneeId { get; set; }
    }
}
