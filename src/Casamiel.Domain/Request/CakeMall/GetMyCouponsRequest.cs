﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Request.Waimai;

namespace Casamiel.Domain.Request.CakeMall
{
    /// <summary>
    /// 
    /// </summary>
    public   class GetMyCouponsRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public int StoreId { get; set; }
		/// <summary>
		/// 商品信息
		/// </summary>
		public List<TakeOutOrderGoods> GoodsList { get; set; }
	}
}
