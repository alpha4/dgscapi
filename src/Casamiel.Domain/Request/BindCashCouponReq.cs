﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 
    /// </summary>
    public class BindCashCouponReq
    {
        /// <summary>
        /// Gets or sets the ticket code.
        /// </summary>
        /// <value>The ticket code.</value>
        [Required(ErrorMessage = "请输入券码")]
        public string TicketCode { get; set; }
    }
}
