﻿    using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 
    /// </summary>
    public  class GetOrderInvoceInfoByOrderCodeReq
    { 
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderCode { get; set; }
        
    }
}
