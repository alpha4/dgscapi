﻿using System;
namespace Casamiel.Domain.Request
{
    /// <summary>
    /// Get invoice by identifier req.
    /// </summary>
    public class GetInvoiceByIdReq
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public long Id { get; set; }
        
    }
}
