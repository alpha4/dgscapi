﻿using System;
namespace Casamiel.Domain.Request
{
	/// <summary>
	/// { "dispatcherMobile":"13466645645", "dispatcherName":"包晓明", "orderId":2450000102, "shippingStatus":40,"time":1477913825}
	/// </summary>
	public class ChangeExpressStatusRequest
	{

		/// <summary>
		/// 
		/// </summary>
		public string DispatcherMobile { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string DispatcherName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public long OrderId { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int ShippingStatus { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int Time { get; set; }
	}
}
