﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderDiscountReq
    {
        /// <summary>
        /// 优惠券id
        /// </summary>
        public long DiscountCouponId { get; set; }
        /// <summary>
        /// 优惠券名称
        /// </summary>
        public string DiscountCouponName { get; set; }
        /// <summary>
        /// 卡号
        /// </summary>
        public string CardNo { get; set; }
        /// <summary>
        /// 优惠券金额
        /// </summary>
        public decimal DiscountCouponMoney { get; set; }
        /// <summary>
        /// 优惠券类型 1 优惠券 2 代金券
        /// </summary>
        public int DiscountCouponType { get; set; }
    }
}
