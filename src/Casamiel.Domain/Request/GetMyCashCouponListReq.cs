﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 
    /// </summary>
    public  sealed  class GetMyCashCouponListReq: BasePagingReq
    {
        /// <summary>
        /// state：状态， 0、已使用；1、已过期；3、可使用;4、使用中;
        /// 
        /// </summary>
        public int State { get; set; }
    }
}
