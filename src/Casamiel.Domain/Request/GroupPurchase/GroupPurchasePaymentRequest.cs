﻿using System;
namespace Casamiel.Domain.Request.GroupPurchase
{
	/// <summary>
	/// 
	/// </summary>
	public class GroupPurchasePaymentRequest
	{
		/// <summary>
		/// 
		/// </summary>
		public string OrderCode { get; set; }

		/// <summary>
		/// 支付方式 :  1微信app支付，2支付宝app支付,4，小程序微信支付
		/// </summary>
		public int Paymethod { get; set; }


		/// <summary>
		///  小程序支付用
		/// </summary>
		public int Id { get; set; }
	}
}
