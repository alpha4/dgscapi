﻿using System;
namespace Casamiel.Domain.Request.GroupPurchase
{
	/// <summary>
	/// 
	/// </summary>
	public class GroupActivityListRequest
	{
		/// <summary>
		/// 平台：0小程序，1App
		/// </summary>
		public int Platform { get; set; }


		/// <summary>
		/// 拼图渠道 0 拼团商城， 1 全国送
		/// </summary>
		public int Source { get; set; }
	}
}
