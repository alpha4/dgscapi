﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
	/// <summary>
	/// 
	/// </summary>
	public class BindCouponRequest
	{
        /// <summary>
        /// 
        /// </summary>
		[Required]
		public string Yzm { get; set; }

        /// <summary>
        /// 
        /// </summary>
		[Required]
		public string Ticketcode { get; set; }
	}
}
