﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 
    /// </summary>
    public   class GetBoothListReq
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Code { get; set; }
    }
}
