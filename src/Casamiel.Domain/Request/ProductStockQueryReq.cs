﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// Product item.
    /// </summary>
    public class ProductStockReq
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string pid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string pname { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int count { get; set; }

        
    }
    /// <summary>
    /// 
    /// </summary>
    public class ProductStockQueryReq
    {
        /// <summary>
        /// 
        /// </summary>
        public string wxopenid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string cardno { get; set; }
        /// <summary>
        /// 来源，11、微信；15、手机APP(默认)；16、PAD
        /// </summary>
        /// <value>The datasource.</value>
        public int datasource { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// 
        [Required]
        public int shopid { get; set; }
        /// <summary>
        /// 店名
        /// </summary>
        public string shopname { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<ProductStockReq> product { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string timestamp { get; set; }
    }
}
