﻿using System;
namespace Casamiel.Domain.Request
{
	/// <summary>
	/// 
	/// </summary>
	public class GetCouponGiveRequest
	{
        /// <summary>
        /// 
        /// </summary>
		public string ForwardCode { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	public class GetCouponRequest : GetCouponGiveRequest
	{
		/// <summary>
		/// 
		/// </summary>
		public string Yzm { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; set; }
	}
}
