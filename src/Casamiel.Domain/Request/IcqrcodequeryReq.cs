﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 幸福卡查询
    /// </summary>
    public class IcqrcodequeryReq
    {
        /// <summary>
        /// 
        /// </summary>
        public string QrCodeNO { get; set; }
    }
}
