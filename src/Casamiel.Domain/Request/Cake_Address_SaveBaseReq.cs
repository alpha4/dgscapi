﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// Cake address save base req.
    /// </summary>
    public class CakeAddressSaveBaseReq
    {
        /// <summary>
        /// Gets or sets the consignee identifier.
        /// </summary>
        /// <value>The consignee identifier.</value>
        public int ConsigneeId { get; set; }
        /// <summary>
        /// 是否默认
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// 排序编号，倒序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 联系人姓名
        /// </summary>
        [Required(ErrorMessage = "联系人姓名不能空")]
        public string RealName { get; set; }

        /// <summary>
        /// 电话 
        /// </summary>
        [Required(ErrorMessage ="电话不能空")]
        public string Phone { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        [Required(ErrorMessage = "详细地址不能空")]
        public string FullAddress { get; set; }

        /// <summary>
        /// 省ID
        /// </summary>
        public int ProvinceId { get; set; }

        /// <summary>
        /// 市ID
        /// </summary>
        public int CityId { get; set; }

        /// <summary>
        /// 区ID
        /// </summary>
        public int DistrictId { get; set; }

        /// <summary>
        /// 邮编
        /// </summary>
        public string Postcode { get; set; }

        /// <summary>
        /// 纬度
        /// </summary>
        public double Latitude { get; set; }
        /// <summary>
        /// 经度
        /// </summary>
        public double Longitude { get; set; }
        /// <summary>
        /// 门店ID 可以为0 在地址管理内没有门店id 
        /// </summary>
        public int StoreId { get; set; } = 0;
    }
}
