﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 
    /// </summary>
    public   class ThirdPaymentReq
    {
      
        /// <summary>
        /// 
        /// </summary>
        public double Amount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string IcTradeNO { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int OperationMethod { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        /// <value>The order code.</value>
        public string OrderCode { get; set; }
        /// <summary>
        ///支付方式 1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
        /// </summary>
        /// <value>The pay method.</value>
        public int PayMethod { set; get; }

        /// <summary>
        /// 微信小程序支付用（id）
        /// 
        /// </summary>
        public int Id { get; set; }
    }
}
