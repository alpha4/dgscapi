﻿using System;
namespace Casamiel.Domain.Request
{
    /// <summary>
    /// Product add collect req.
    /// </summary>
    public class ProductCollectReq
    {
        /// <summary>
        /// Gets or sets the product base identifier.
        /// </summary>
        /// <value>The product base identifier.</value>
        public int ProductBaseId
        {
            get;set;
        }
    }
}
