﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderUnusualCallbackReq
    {

        /// <summary>
        /// 配送活动标识
        /// </summary>
        public long DeliveryId
        {
            get;
            set;
        }

        /// <summary>
        /// 美团配送内部订单id，最长不超过32个字符
        /// </summary>
        public string MtPeisongId
        {
            get;
            set;
        }

        /// <summary>
        /// 外部订单号，最长不超过32个字符
        /// </summary>
        public string OrderId
        {
            get;
            set;
        }

        /// <summary>
        /// 异常ID，用来唯一标识一个订单异常信息。接入方用此字段用保证接口调用的幂等性。
        /// </summary>
        public long ExceptionId
        {
            get;
            set;
        }

        /// <summary>
        /// 订单异常代码，当前可能的值为：
        ///10001：顾客电话关机
        ///10002：顾客电话已停机
        ///10003：顾客电话无人接听
        ///10004：顾客电话为空号
        ///10005：顾客留错电话
        ///10006：联系不上顾客其他原因
        ///10101：顾客更改收货地址
        ///10201：送货地址超区
        ///10202：顾客拒收货品
        ///10203：顾客要求延迟配送
        ///10401：商家关店/未营业
        /// </summary>
        public int ExceptionCode
        {
            get;
            set;
        }

        /// <summary>
        /// 订单异常详细信息
        /// </summary>
        public string ExceptionDescr
        {
            get;
            set;
        }
        /// <summary>
        ///  配送员上报订单异常的时间，格式为long，时区为GMT+8，距离Epoch(1970年1月1日) 以秒计算的时间，即unix-timestamp。
        /// </summary>
        public string ExceptionTime
        {
            get;
            set;
        }

        /// <summary>
        /// 上报订单异常的配送员姓名
        /// </summary>
        public string CourierName
        {
            get;
            set;
        }

        /// <summary>
        /// 上报订单异常的配送员电话
        /// </summary>
        public string CourierPhone
        {
            get;
            set;
        }

         

        /// <summary>
        ///  时间戳，格式为long，时区为GMT+8，当前距 离Epoch（1970年1月1日) 以秒计算的时间，即 unix-timestamp。
        /// </summary>
        public long Timestamp
        {
            get;
            set;
        }
 
    }
}
