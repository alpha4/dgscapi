﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
	/// <summary>
	/// 
	/// </summary>
	public class SendPreOrderSubcribeMessagRequest
	{
		/// <summary>
		/// 
		/// </summary>
		[Required(ErrorMessage = "取货码")]
		public string TakeCode { get; set; }

		/// <summary>
		/// 订单号
		/// </summary>
		[Required(ErrorMessage = "订单号")]
		public string OrderCode { get; set; }

		/// <summary>
		/// 门店名称
		/// </summary>
		[Required(ErrorMessage = "门店名称")]
		public string StoreName { get; set; }

		/// <summary>
		/// 门店店址
		/// </summary>
		[Required(ErrorMessage = "门店名称")]
		public string Address { get; set; }

		/// <summary>
		/// 手机号
		/// </summary>
		[Required(ErrorMessage = "手机号")]
		public string Mobile { get; set; }
	}
}
