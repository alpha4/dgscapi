﻿using System;
namespace Casamiel.Domain.Request
{
	/// <summary>
	/// {"orderCode":"","CancelReason":"","secrect":""}
	/// </summary>
	public class RefundReq
	{
		/// <summary>
		/// 
		/// </summary>
		public string OrderCode { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string CancelReason { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string Secrect { get; set; }
	}
	/// <summary>
	/// {"msg":"","secrect":"","version":""}
	/// </summary>
	public class ErrorlogReq
	{
		/// <summary>
		/// 
		/// </summary>
		public string Msg { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Secrect { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Version { get; set; }
	}
	/// <summary>
	/// 
	/// </summary>
	public class IcQrcodecreateReq
	{
		/// <summary>
		/// 
		/// </summary>
		public string Shopid { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string Secrect { get; set; }
	}
	/// <summary>
	/// 
	/// </summary>
	public class StoreGetProductReq
	{
		/// <summary>
		/// 
		/// </summary>
		public int RelationId { get; set; }
	}
	/// <summary>
	/// 
	/// </summary>
	public class GetBaseIDBySkuReq
	{
		/// <summary>
		/// 
		/// </summary>
		public string SkuId { get; set; }
	}
}
