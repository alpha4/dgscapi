﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Request.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public  sealed     class ShoppingCardAddBaseReq
    {
        /// <summary>
        /// 
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int GoodsBaseId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int Quantity { get; set; }
    }
}
