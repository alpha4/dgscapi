﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Request.Waimai
{
    /// <summary>
    /// 订单预览
    /// </summary>
    public class OrderPreviewRequest
    {
		/// <summary>
		/// 门店id
		/// </summary>
		public int StoreId { get; set; }

		/// <summary>
		/// 商品信息
		/// </summary>
		public List<TakeOutOrderGoods> GoodsList { get; set; }

		/// <summary>
		/// 优惠券信息
		/// </summary>
		public List<OrderDiscountReq> DiscountList { get; set; }

		/// <summary>
		/// 配送方式 订单类型 1: 自提 2:外卖 3 快递配送
		/// </summary>
		public int OrderType { get; set; }

    }
}
