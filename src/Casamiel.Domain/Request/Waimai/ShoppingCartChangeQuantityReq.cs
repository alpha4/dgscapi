﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Request.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class ShoppingCartChangeQuantityReq
    {
        /// <summary>
        /// 门店id
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 产品id
        /// </summary>
        public int GoodsBaseId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity{get;set;}
    }
}
