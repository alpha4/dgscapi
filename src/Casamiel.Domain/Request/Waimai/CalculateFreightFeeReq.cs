﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Request.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public  class CalculateFreightFeeReq
    {
        /// <summary>
        /// 订单金额
        /// </summary>
        public decimal OrderMoney { get; set; }
    }
}
