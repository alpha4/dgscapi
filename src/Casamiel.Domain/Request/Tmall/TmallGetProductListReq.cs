﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request.Tmall
{
    /// <summary>
    /// Tmall get product list req.
    /// </summary>
    public class TmallGetProductListReq
    {
        /// <summary>
        /// Gets or sets the name of the product.
        /// </summary>
        /// <value>The name of the product.</value>
        public string Title { get; set; }
        /// <summary>
        /// Gets or sets the token.
        /// </summary>
        /// <value>The token.</value>
        [Required]
        public string Token { get; set; }
        /// <summary>
        /// 门店id 切换门店专用字段
        /// </summary>
        /// <value>The store base identifier.</value>
        public int StoreBaseId { get; set; }

        

    }
}
