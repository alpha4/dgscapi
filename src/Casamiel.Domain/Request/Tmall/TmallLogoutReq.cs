﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request.Tmall
{
    /// <summary>
    /// Tmall logout req.
    /// </summary>
    public class TmallLogoutReq
    {
        /// <summary>
        /// Gets or sets the token.
        /// </summary>
        /// <value>The token.</value>
        [Required]
        public string Token { get;  set; }
        /// <summary>
        /// 门店id 切换门店专用字段
        /// </summary>
        /// <value>The store base identifier.</value>
        public int StoreBaseId { get;  set; }
    }
}
