﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request.Tmall
{
    /// <summary>
    /// Tmall login req.
    /// </summary>
    public class TmallLoginReq
    {
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>The name of the user.</value>
        [Required(ErrorMessage = "用户名不能为空")]
        public string UserName { get; set; }
        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        [Required(ErrorMessage = "密码不能为空")]
        public string Password { get; set; }
    }
}
