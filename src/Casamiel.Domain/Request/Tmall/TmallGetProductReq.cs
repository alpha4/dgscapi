﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class TmallGetProductReq
    {
        /// <summary>
        /// 
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ProductId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage ="token不能为空")]
        public string Token { get; set; }
}
}
