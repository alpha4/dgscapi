﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 
    /// </summary>
    public class GetYzmRequest
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string IdentityCard { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Mobile { get; set; }
    }
}
