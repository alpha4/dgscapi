﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
	/// <summary>
	/// 
	/// </summary>
	public class ResetPasswordRequest
	{

		/// <summary>
		///   身份证号
		/// </summary>
		[StringLength(18, MinimumLength = 15, ErrorMessage = "身份证号15-18位")]
		public string IdentityCard { get; set; }
		/// <summary>
		/// 
		/// </summary>
		[Required]
		 public string Mobile { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[Required]
		public string Yzm { get; set; }

		 
	}
	/// <summary>
	/// 
	/// </summary>
	public class ResetPasswordEndRequest: ResetPasswordRequest
	{
 

		/// <summary>
		/// 
		/// </summary>
		[Required]
		public string Password { get; set; }
	}
}
