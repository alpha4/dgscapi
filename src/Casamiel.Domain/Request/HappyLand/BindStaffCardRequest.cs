﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
	/// <summary>
	/// 
	/// </summary>
	public class BindStaffCardRequest
	{

		/// <summary>
		/// Gets or sets the card no.
		/// </summary>
		/// <value>The card no.</value>
		 
		[StringLength(16, MinimumLength = 6, ErrorMessage = "卡号长度6-16位")]
		public string Cardno { get; set; }
		/// <summary>
		/// Gets or sets the yzm.
		/// </summary>
		/// <value>The yzm.</value>
		[Required()]
		public string Yzm { get; set; }
	}
}
