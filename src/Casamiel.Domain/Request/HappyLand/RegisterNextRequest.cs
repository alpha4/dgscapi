﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 
    /// </summary>
    public class RegisterNextRequest
    {
        /// <summary>
        ///   身份证号
        /// </summary>
        [StringLength(18, MinimumLength = 15, ErrorMessage = "身份证号15-18位")]
        public string IdentityCard { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [StringLength(11, MinimumLength = 11, ErrorMessage = "手机号11位")]
        public string Mobile { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StringLength(4, MinimumLength = 4, ErrorMessage = "验证码4位")]
         
        public string Yzm { get; set; }
 
    }
    /// <summary>
    /// 
    /// </summary>
    public class RegisterRequest: RegisterNextRequest
    {
        /// <summary>
        /// 
        /// </summary>
        [StringLength(6, MinimumLength = 6, ErrorMessage = "密码6位")]
        public string Password { get; set; }
    }

    }
