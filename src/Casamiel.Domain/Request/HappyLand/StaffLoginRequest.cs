﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
	/// <summary>
	/// 
	/// </summary>
	public class StaffLoginRequest
	{
		/// <summary>
		/// 
		/// </summary>
		[Required]
		public string Mobile { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[Required]
		public string PassWord { get; set; }
	}
}
