﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
	/// <summary>
	/// 解锁
	/// </summary>
	public class UnlockRequest
	{
		/// <summary>
		/// 密码
		/// </summary>
		[StringLength(6, MinimumLength = 6, ErrorMessage = "密码4位")]
		public string Password { get; set; }
	}
}
