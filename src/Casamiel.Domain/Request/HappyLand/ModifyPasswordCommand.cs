﻿using System;
namespace Casamiel.Domain.Request
{
	/// <summary>
	/// 
	/// </summary>
	public class ModifyPassordRequest
	{
		/// <summary>
		/// 
		/// </summary>
		public string Password { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string OldPassword { get; set; }
	}
}
