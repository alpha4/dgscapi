﻿using System;
namespace Casamiel.Domain.Request
{
    /// <summary>
    /// Get list by store identifier req.
    /// </summary>
    public class GetListByStoreIdReq
    {
        /// <summary>
        /// Gets or sets the store identifier.
        /// </summary>
        /// <value>The store identifier.</value>
        public int StoreId { get; set; }
    }
}
