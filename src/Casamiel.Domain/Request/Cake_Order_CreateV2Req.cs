﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Casamiel.Domain.Request.Waimai;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// Cake order create req.
    /// </summary>
    public class CakeOrderCreateV2Req
    {
        /// <summary>
        /// 商品sku Id
        /// </summary>
        public int GoodsBaseId { get; set; }
        /// <summary>
        /// 门店id
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 会员价
        /// </summary>
        public decimal CostPrice { get; set; }

        /// <summary>
        /// 购买数量
        /// </summary>
        public int Quantity { get; set; }


        /// <summary>
        /// 联系人
        /// </summary>
        [Required]
        public string ContactName { get; set; }
        /// <summary>
        /// 联系方式
        /// </summary>
        [Required]
        public string ContactPhone { get; set; }
         
        /// <summary>
        /// 卡号
        /// </summary>
        public string CardNO { get; set; }

        /// <summary>
        /// 支付卡号
        /// </summary>
        public string PayCardNO { get; set; }
        
        /// <summary>
        /// 备注信息
        /// </summary>
        public string Remark { get; set; }

        
        /// <summary>
        /// 配送时间
        /// </summary>
        public DateTime TakeTime { get; set; }

        /// <summary>
        /// 配送方式 订单类型 1: 自提 2:外卖  
        /// </summary>
        public int OrderType { get; set; }
        /// <summary>
        /// 地址Id
        /// </summary>
        public int ConsigneeId { get; set; }

        /// <summary>
        /// 支付方式 1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
        /// </summary>
        /// <value>The paymeothd.</value>
        [Required(ErrorMessage = "支付方式必填")]
        public int PayMethod { get; set; }

        /// <summary>
        /// 优惠券信息
        /// </summary>
        public List<OrderDiscountReq> DiscountList { get; set; }
        /// <summary>
        /// 小程序专用Id
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }

        /// <summary>
        /// 发票信息Id
        /// </summary>
        public int InvoiceId { get; set; }
    }

	/// <summary>
	/// 
	/// </summary>
	public class CakeOrderCreateV3Req
	{
		/// <summary>
		/// 门店id
		/// </summary>
		public int StoreId { get; set; }
		/// <summary>
		/// 商品信息
		/// </summary>
		public List<TakeOutOrderGoods> GoodsList { get; set; }


		/// <summary>
		/// 联系人
		/// </summary>
		[Required]
		public string ContactName { get; set; }
		/// <summary>
		/// 联系方式
		/// </summary>
		[Required]
		public string ContactPhone { get; set; }

		/// <summary>
		/// 卡号
		/// </summary>
		public string CardNO { get; set; }

		/// <summary>
		/// 支付卡号
		/// </summary>
		public string PayCardNO { get; set; }

		/// <summary>
		/// 备注信息
		/// </summary>
		public string Remark { get; set; }


		/// <summary>
		/// 配送时间
		/// </summary>
		public DateTime TakeTime { get; set; }

		/// <summary>
		/// 配送方式 订单类型 1: 自提 2:外卖  
		/// </summary>
		public int OrderType { get; set; }
		/// <summary>
		/// 地址Id
		/// </summary>
		public int ConsigneeId { get; set; }

		/// <summary>
		/// 支付方式 1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
		/// </summary>
		/// <value>The paymeothd.</value>
		[Required(ErrorMessage = "支付方式必填")]
		public int PayMethod { get; set; }

		/// <summary>
		/// 优惠券信息
		/// </summary>
		public List<OrderDiscountReq> DiscountList { get; set; }
		/// <summary>
		/// 小程序专用Id
		/// </summary>
		/// <value>The identifier.</value>
		public int Id { get; set; }

		/// <summary>
		/// 发票信息Id
		/// </summary>
		public int InvoiceId { get; set; }
	}
}
