﻿using System;
namespace Casamiel.Domain.Request.Pre
{
	/// <summary>
	/// 
	/// </summary>
	public class ProductSearchRequest
	{
		/// <summary>
		/// 
		/// </summary>
		public string Keyword { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int StoreId { get; set; }
	}
}
