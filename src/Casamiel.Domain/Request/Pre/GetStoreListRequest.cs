﻿using System;
namespace Casamiel.Domain.Request.Pre
{
    /// <summary>
	/// 
	/// </summary>
	public class GetStoreListRequest
	{
        /// <summary>
        /// 
        /// </summary>
        public string StoreName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double Lat { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double Lng { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; }
    }
}
