﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// Get product list by search req.
    /// </summary>
    public class GetProductListBySearchReq
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the index of the page.
        /// </summary>
        /// <value>The index of the page.</value>
        public int PageIndex { get; set; }
        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; set; }
    }
}
