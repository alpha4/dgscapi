﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// 
    /// </summary>
    public class CakeBuyNowReq
    {
        /// <summary>
        /// 门店id
        /// </summary>
        [Required(ErrorMessage = "门店id必填")]
        public int StoreId { get; set; }
        /// <summary>
        /// 门店关联id
        /// </summary>
        [Required(ErrorMessage = "门店关联id必填")]
        public int StoreRelationId { get; set; }
        /// <summary>
        /// 产品sku id
        /// </summary>
        [Required(ErrorMessage = "产品sku id必填")]
        public int GoodsBaseId { get; set; }

        /// <summary>
        /// 产品sku关联Id
        /// </summary>
        [Required(ErrorMessage = "产品sku关联Id必填")]
        public int GoodsRelationId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        [Required(ErrorMessage = "数量必填")]
        public int Count { get; set; }

        /// <summary>
        /// 
        /// 配送方式 订单类型 1: 自提 2:外卖 3 快递配送
        /// </summary>
        /// <value>The type of the buy.</value>
        public int BuyType { get; set; }
        /// <summary>
        /// 自提时间\配送时间
        /// </summary>
        [Required(ErrorMessage = "自提时间必填")]
        public DateTime TakeTime { get; set; }
        //{"storeid":1,"storeRelationId":1,"goodsBaseId": 1,"relationId": 141783,"count":1,"picktime":"2018-04-20 12:30"}
    }
}
