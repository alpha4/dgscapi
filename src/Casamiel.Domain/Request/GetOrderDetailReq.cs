﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
    /// <summary>
    /// Get order detail req.
    /// </summary>
    public sealed class GetOrderDetailReq
    {
        /// <summary>
        /// Gets or sets the order code.
        /// </summary>
        /// <value>The order code.</value>
        [Required]
        public string OrderCode { get; set; }
    }
}
