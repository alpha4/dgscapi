﻿using System;
namespace Casamiel.Domain.Request
{
    /// <summary>
    /// Base paging req.
    /// </summary>
    public class BasePagingReq
    {
        /// <summary>
        /// 页码
        /// </summary>
        /// <value>The index of the page.</value>
        public int PageIndex { get; set; }
        /// <summary>
        /// 每页显示记录数
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; set; }
    }
}
