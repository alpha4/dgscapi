﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
	/// <summary>
	/// 
	/// </summary>
	public class CouponForwardRequest
	{
        /// <summary>
        /// 
        /// </summary>
		 public long MCID { get; set; }
        /// <summary>
        /// 
        /// </summary>
		[StringLength(240,ErrorMessage ="最多240个字")]
		public string Message { get; set; }
	}
}
