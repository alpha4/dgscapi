﻿using System;
namespace Casamiel.Domain.Request
{
    /// <summary>
    /// Ticket opening detail req.
    /// </summary>
    public class TicketOpeningDetailReq
    {
        /// <summary>
        /// Gets or sets the goodsname.
        /// </summary>
        /// <value>The goodsname.</value>
        public string Goodsname { get; set; } = "蛋糕";
        /// <summary>
        /// Gets or sets the hsbz.
        /// </summary>
        /// <value>The hsbz.</value>
        public string Hsbz { get; set; } = "1";
        /// <summary>
        /// Gets or sets the taxrate.
        /// </summary>
        /// <value>The taxrate.</value>
        public string Taxrate { get; set; } = "0.16";
        /// <summary>
        /// Gets or sets the spbm.
        /// </summary>
        /// <value>The spbm.</value>
        public string Spbm { get; set; } = "103020101";
        /// <summary>
        /// Gets or sets the taxfreeamt.
        /// </summary>
        /// <value>The taxfreeamt.</value>
        public string taxfreeamt { get; set; }
        /// <summary>
        /// Gets or sets the tax.
        /// </summary>
        /// <value>The tax.</value>
        public string tax { get; set; }
        /// <summary>
        /// Gets or sets the taxamt.
        /// </summary>
        /// <value>The taxamt.</value>
        public string taxamt { get; set; }
    }
}