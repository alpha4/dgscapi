﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Request
{
    /// <summary>
    ///  美团配送订单状态
    /// </summary>
    public partial class CallbackOrderStatusReq
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long? DeliveryId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MtPeisongId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CourierName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CourierPhone { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? CancelReasonId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CancelReason { get; set; }

       

        /// <summary>
        /// 
        /// </summary>
        public long Timestamp { get; set; }

        

        /// <summary>
        /// 
        /// </summary>
        public DateTime? Creationtime { get; set; }
    }
}
