﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
	/// <summary>
	/// 
	/// </summary>
	public class MemberSaveCardRequest
	{
		/////<summary>
		/////会员注册手机号
		/////</summary>		
		//[Required(ErrorMessage = "会员手机号不能为空")]
		//public string Mobile { get; set; }

		/////<summary>
		/////注册时间
		/////</summary>		
		//[Required(ErrorMessage = "注册时间不能为空")]
		//public DateTime RegisterTime { get; set; }

		/////<summary>
		/////卡号
		/////</summary>		
		//public string CardNo { get; set; }

		/////<summary>
		/////卡类型
		/////</summary>		
		//public int? CardType { get; set; }

		/////<summary>
		/////绑定时间
		/////</summary>		
		//public DateTime? BindTime { get; set; }

		/////<summary>
		/////绑定来源 1 小程序 2 app
		/////</summary>		
		//public int? BindSource { get; set; }

		///<summary>
		///姓名
		///</summary>		
		[StringLength(20, ErrorMessage = "姓名不能超过20个字符")]
		public string RealName { get; set; }

		///<summary>
		///0 女 1 男
		///</summary>		
		public bool? Sex { get; set; }

		///<summary>
		///生日
		///</summary>		
		public DateTime? Birthday { get; set; }

		///<summary>
		/// 省
		///</summary>		
		[StringLength(50, ErrorMessage = "省名称不能超过50个字符")]
		public string Provice { get; set; }

		///<summary>
		///市
		///</summary>		
		[StringLength(50, ErrorMessage = "市名称不能超过50个字符")]
		public string City { get; set; }

		///<summary>
		///区
		///</summary>		
		[StringLength(50, ErrorMessage = "区名称不能超过50个字符")]
		public string District { get; set; }

		///<summary>
		/// 门店id
		///</summary>		
		public int? StoreId { get; set; }

		///<summary>
		/// 门店名称
		///</summary>		
		[StringLength(50, ErrorMessage = "门店名称不能超过50个字符")]
		public string StoreName { get; set; }

		///<summary>
		/// 最近门店距离
		///</summary>		
		[StringLength(50, ErrorMessage = "最近门店距离不能超过50个字符")]
		public string StoreDistance { get; set; }

		///<summary>
		/// 居住方式
		///</summary>		
		[StringLength(50, ErrorMessage = "居住方式不能超过50个字符")]
		public string LiveWay { get; set; }

		///<summary>
		/// 是否加店长微笑
		///</summary>		
		public bool? IsAddWeixin { get; set; }

		/// <summary>
		/// 东哥用
		/// 邀请码/推荐码
		/// </summary>
		[StringLength(50, ErrorMessage = "邀请码最大长度为50位")]
		public string InvitationCode { get; set; }
	}
}

