﻿using System;
namespace Casamiel.Domain.Request
{
    /// <summary>
    /// Get address req.
    /// </summary>
    public class BaseAddressReq
    {
        /// <summary>
        /// Gets or sets the consignee identifier.
        /// </summary>
        /// <value>The consignee identifier.</value>
        public int ConsigneeId { get; set; }
    }
}
