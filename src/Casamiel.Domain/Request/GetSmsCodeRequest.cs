﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.Domain.Request
{
	/// <summary>
	/// 
	/// </summary>
	public class GetSmsCodeRequest
	{
		/// <summary>
		/// 
		/// </summary>
		///
		[StringLength(11, MinimumLength = 11, ErrorMessage = "手机号11位")]
		public string Mobile { get; set; }
	}
}
