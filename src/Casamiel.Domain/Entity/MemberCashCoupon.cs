﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// 代金券
    /// </summary>
    [Table("member_cash_coupon")]
    public  class MemberCashCoupon
    {
       /// <summary>
       /// 
       /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
         

        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; set; }

        /// <summary>
        /// 券码
        /// </summary>
        [Key]
        public string TicketCode { get; set; }
        /// <summary>
        /// 代金券名称
        /// </summary>
        public string CouponName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 0、已使用；-1、不可用；-2、已销毁；1、已过期；2、未启用；3、可使用；4、使用中
        /// </summary>
        public int State { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateTime { get; set; }
  //      ticketcode": "865667427528342380",
  //"productname": "蜜兒10元提货券(160*65)mm",
  //"customername": "",
  //"price": 10,
  //"state": 3,
  //"description": "可使用",
  //"startdate": "2019-01-29 00:00:00",
  //"enddate": "2024-12-31 23:59:55"
    }
}
