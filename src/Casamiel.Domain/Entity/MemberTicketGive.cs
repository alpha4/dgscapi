﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{
	/// <summary>
	/// 
	/// </summary>
	[Table("member_ticket_give")]
	public class MemberTicketGive
	{
		/// <summary>
		/// 流水号
		/// </summary>
		
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long MTGID { get; set; }

		/// <summary>
		/// 券水号
		/// </summary>
		public long Ticketid { get; set; }
 
		/// <summary>
		/// 券名称
		/// </summary>
		public string Ticketname { get; set; }

		/// <summary>
		/// 金额
		/// </summary>
		public decimal Je { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string ForwardCode { get; set; }

		/// <summary>
		/// 0发起转赠；1转赠成功，-1转赠超时
		/// </summary>
		public int Status { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public DateTime? LastTime { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public DateTime CreateTime { get; set; }
	}
}
