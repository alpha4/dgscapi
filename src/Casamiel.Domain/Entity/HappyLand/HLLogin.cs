﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{
	/// <summary>
	/// 
	/// </summary>
	[Table("hl_login")]
	public class HLLogin
	{
		/// <summary>
		/// Gets or sets the identifier.
		/// </summary>
		/// <value>The identifier.</value>
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]

		public int LoginID { get; set; }
		/// <summary>
		/// Gets or sets the mobile.
		/// </summary>
		/// <value>The mobile.</value>

		public string Mobile { get; set; }
		/// <summary>
		/// Gets or sets the password.
		/// </summary>
		/// <value>The password.</value>
		public string Password { get; set; }
		/// <summary>
		/// Gets or sets the password salt.
		/// </summary>
		/// <value>The password salt.</value>
		public string PasswordSalt { get; set; }
		 
		/// <summary>
		/// 
		/// </summary>
		public DateTime CreateDate { get; set; }
		/// <summary>
		/// Gets or sets the last login date.
		/// </summary>
		/// <value>The last login date.</value>
		public DateTime? LastLoginDate { get; set; }
		 
	}
}
