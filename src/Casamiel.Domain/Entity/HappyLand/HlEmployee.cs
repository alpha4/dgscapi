﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// 
    /// </summary>
    [Table("hl_employee")]
    public class HlEmployee
    {

        /// <summary>
        /// 
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int EmployeeId { get; set; }

        /// <summary>
        /// 部门id
        /// </summary>		
        public int DepartMentId { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>		
        public string Name { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>		
        public string Mobile { get; set; }

        /// <summary>
        /// 卡号
        /// </summary>		
        public string CardNo { get; set; } = "";

        /// <summary>
        /// 身份证
        /// </summary>		
        public string IdentityCard { get; set; }

        /// <summary>
        /// 状态  1在职 2离职
        /// </summary>		
        public int Status { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int? CardState { get; set; }


	}
}
