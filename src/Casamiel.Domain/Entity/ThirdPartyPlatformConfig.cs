﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain.Entity
{
	/// <summary>
	/// 
	/// </summary>
	[Table("third_party_platform_config")]
	public class ThirdPartyPlatformConfig
	{
		/// <summary>
		/// 门店标识
		/// </summary>
		[Key]
		public int ShopId { get; set; }
		/// <summary>
		/// 门店名称
		/// </summary>
        public string ShopName { get; set; }
		/// <summary>
		/// 美团是否启用
		/// </summary>
		public bool MeiTuanEnable { get; set; }
		/// <summary>
		/// 饿了么是否启用
		/// </summary>
		public bool EleEnable { get; set; }
		/// <summary>
		/// 饿了么标识
		/// </summary>
		public long? EleStoreId{ get; set; }
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime CreateTime { get; set; }
	}
}
