﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// Payment record.
    /// </summary>
	[Table("payment_record")]
	public class PaymentRecord
	{
		/// <summary>
		///  支付单流水号
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long ID { get; private set; }
		/// <summary>
		/// 手机号
		/// </summary>
		public string Mobile { get; set; }
		/// <summary>
		/// 金额
		/// </summary>
		public double Amount { get; set; }
		/// <summary>
		/// 交易单号
		/// </summary>
		/// 
		[Key]
		public string TradeNo { get; set; }
		/// <summary>
		/// 外部交易单号
		/// </summary>
		public string OutTradeNo { get; set; }
		/// <summary>
		/// 商户号
		/// </summary>
		public string MchId { get; set; }
		/// <summary>
		/// 支付方式1、微信app；2、支付宝app，3会员卡,4微信小程序,5幸福卡充值，6支付宝小程序
		/// </summary>
		public int PayMethod { get; set; }

		/// <summary>
		/// 支付状态 0未支付，1已支付
		/// </summary>
		[ConcurrencyCheck]
		public int State { get; set; }

		/// <summary>
		/// 业务单据
		/// </summary>
		public string BillNO { get; set; }
		/// <summary>
		/// 操作方式1充值，2商城,3，退款，4 全国配,5外卖,6幸福卡充值
		/// </summary>
		/// 
		public int OperationMethod { get; set; }

		/// <summary>
		/// 操作状态
		/// </summary>
		[ConcurrencyCheck]
		public int OperationState { get; set; }

		/// <summary>
		/// 最后操作时间
		/// </summary>
		public DateTime? LastOperationTime { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public DateTime CreateTime { get; set; }
		/// <summary>
		///支付时间
		/// </summary>
		public DateTime? PayTime { get; set; }
		/// <summary>
		/// 备注
		/// </summary>
		public string Remark { get; set; }
		/// <summary>
		/// ic交易号
		/// </summary>
		public string IcTradeNO { get; set; }

		/// <summary>
		/// 充值赠额
		/// </summary>
		public double Largessmoney { get; set; }
		/// <summary>
		/// 门店标识
		/// </summary>
		public	long? ShopId { get; set; }

        /// <summary>
        /// 门店名称
        /// </summary>
        /// <value>The name of the shop.</value>
        public string ShopName { get; set; }
        /// <summary>
        /// 门店编号
        /// </summary>
        public string ShopNO { get; set; }
    }
}
