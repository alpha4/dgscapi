﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// Member.
    /// </summary>
	[Table("member")]
	public class Member
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
		[Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
       
		public long ID { get; set; }
        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        
        public string Mobile { get; set; }
		/// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        public string Password { get; set; }
		/// <summary>
        /// Gets or sets the password salt.
        /// </summary>
        /// <value>The password salt.</value>
        public string PasswordSalt { get; set; }
		/// <summary>
        /// Gets or sets the nick.
        /// </summary>
        /// <value>The nick.</value>
        public string Nick { get; set; }
		/// <summary>
        /// Gets or sets the name of the true.
        /// </summary>
        /// <value>The name of the true.</value>
        public string TrueName { get; set; }
        /// <summary>
        /// Gets or sets the sex.
        /// </summary>
        /// <value>The sex.</value>
		public int? Sex { get; set; }
		/// <summary>
        /// Gets or sets the birthday.
        /// </summary>
        /// <value>The birthday.</value>
        public DateTime? Birthday { get; set; }
		/// <summary>
        /// /
        /// </summary>
        /// <value>The create date.</value>
        public DateTime? CreateDate { get; set; }
		/// <summary>
        /// Gets or sets the last login date.
        /// </summary>
        /// <value>The last login date.</value>
        public DateTime? LastLoginDate { get; set; }
		/// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }

		/// <summary>
		/// 邀请码
		/// </summary>
		public string InvitationCode { get; set; }
		/// <summary>
		/// 来源1app，2小程序，3其它
		/// </summary>
		/// <value>The source.</value>
		public int? Source { get; set; }
        //public string Access_Token
        //{ get; set; }
        ///// <summary>
        ///// 是否注销
        ///// </summary>
        //public bool Is_Logout { get; set; }
        ///// <summary>
        ///// 设备号
        ///// </summary>
        //public string Registration_Id { get; set; }
    }
}
