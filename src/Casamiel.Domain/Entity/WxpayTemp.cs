﻿using Dapper.FastCrud;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// Wxpay temp.
    /// </summary>
    [Table("wxpay_temp")]
    public class WxpayTemp//: Smooth.IoC.Repository.UnitOfWork.Entity<int>
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }
		/// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        [Key]
		public string code { get; set; }
        /// <summary>
        /// 手机号码
        /// </summary>
        [Required]
        public string mobile { get; set; }

        /// <summary>
        /// 操作方式1卡充值
        /// </summary>
        [Required]
        public int operationMethod { get; set; }
        /// <summary>
        /// 充值金额
        /// </summary>
        [Required]
        public double chargemoney { get; set; }
        /// <summary>
        /// 业务单据（卡号）
        /// </summary>
        public string billNO { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string summary { get; set; }
        /// <summary>
        /// 返回url
        /// </summary>
        public string returnUrl { get; set; }
        /// <summary>
        /// Gets or sets the create date.
        /// </summary>
        /// <value>The create date.</value>
        public DateTime CreateDate { get; set; }
    }
}
