﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// Member.
    /// </summary>
	[Table("mbr_shoppingCart")]
    public  class Mbr_shoppingCartEntity
    {
        /// <summary>
        /// 
        /// </summary>
        /// 
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ShoppingCartId { get; set; }

        /// <summary>
        /// 门店关联Id
        /// </summary>
        public int StoreRelationId { get; set; }

        /// <summary>
        /// 门店Id
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
        /// 门店商品ID
        /// </summary>
        public int GoodsId { get; set; }

        /// <summary>
        /// 商品ID
        /// </summary>
        public int GoodsBaseId { get; set; }
        /// <summary>
        /// 商品关联Id
        /// </summary>
        public int GoodsRelationId { get; set; }

       /// <summary>
       /// 
       /// </summary>
        public int GoodsQuantity { get; set; }

        /// <summary>
        /// 状态：（0:正常）
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 手机号 
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 是否选中
        /// </summary>
        public bool IsCheck { get; set; }
    }
}
