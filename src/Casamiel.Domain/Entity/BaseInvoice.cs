﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// Base invoice.
    /// </summary>
    [Table("base_invoice")]
    public class BaseInvoice
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID { get; set; }
        /// <summary>
        /// Gets or sets the order no.
        /// </summary>
        /// <value>The order no.</value>
        public string OrderNO { get; set; }
        /// <summary>
        /// 订单总价
        /// </summary>
        /// <value>The order total.</value>
        public decimal OrderTotal { get; set; }
        /// <summary>
        /// 0个人 ，1企业
        /// </summary>
        /// <value>The type of the invoice.</value>
        public int InvoiceType { get; set; }
        /// <summary>
        ///购方名称 
        /// </summary>
        /// <value>The name of the buyer.</value>
        public string BuyerName { get; set; }
        /// <summary>
        /// 银行账号
        /// </summary>
        /// <value>The account.</value>
        public string Account { get; set; }
        /// <summary>
        /// 合计税额 必填:是 20 位，精确到小数点 后面两位
        /// </summary>
        /// <value>The tax tota.</value>
        public decimal TaxTotal { get; set; }
        /// <summary>
        /// 购方税号 否（企业要填，个人 可为空）） 20
        /// </summary>
        /// <value>The tax number.</value>
        public string TaxNum { get; set; }
        /// <summary>
        /// Gets or sets the bhtaxtotal.
        /// </summary>
        /// <value>The bhtaxtotal.</value>
        public decimal Bhtaxtotal { get; set; }
        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }
        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; set; }
        /// <summary>
        /// Gets or sets the sms mobile.
        /// </summary>
        /// <value>The sms mobile.</value>
        public string SmsMobile { get; set; }
        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>The address.</value>
        public string Address { get; set; }
        /// <summary>
        /// Gets or sets the remark.
        /// </summary>
        /// <value>The remark.</value>
        public string Remark { get; set; }
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public int Status { get; set; }
        /// <summary>
        /// Gets or sets the create time.
        /// </summary>
        /// <value>The create time.</value>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 发票流水号
        /// </summary>
        /// <value>The fpqqlsh.</value>
        public string fpqqlsh{get;set;}
    }

}


