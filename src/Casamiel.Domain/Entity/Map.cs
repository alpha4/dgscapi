﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// Map.
    /// </summary>
	public class Map
	{
		 
		/// <summary>
		/// 纬度
		/// </summary>
		/// 
		[Required]
		public double lat { get; set; }
		/// <summary>
		/// 经度
		/// </summary>
		/// 
		[Required]
		public double lng { get; set; }
	}
}
