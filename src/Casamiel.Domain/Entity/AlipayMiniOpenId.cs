﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{
	/// <summary>
	/// Base invoice.
	/// </summary>
	[Table("alipay_mini_openid")]
	public class AlipayMiniOpenId
	{
		/// <summary>
		/// 
		/// </summary>
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long ID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string AppId { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public long UserId { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; set; }

		/// <summary>
		/// Gets or sets the create time.
		/// </summary>
		/// <value>The create time.</value>
		public DateTime CreateTime { get; set; }
		/// <summary>
		/// Gets or sets the create time.
		/// </summary>
		/// <value>The create time.</value>
		public DateTime? UpdateTime { get; set; }
	}

}
