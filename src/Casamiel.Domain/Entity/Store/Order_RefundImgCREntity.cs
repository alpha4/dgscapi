
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CasaMiel.Domain.Entity
{
    /// <summary>
    /// 退款信息图片关系表 - 实体类
    /// </summary>	
    [Table("order_GoodsRefundImage")]
    public class Order_RefundImgCREntity
    {

        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RefundImgCRId { get; set; }

        /// <summary>
        /// 退款单Id
        /// </summary>
        public int RefundId { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public string ImgUrl { get; set; }

        /// <summary>
        /// 图片应用，1、申请凭证、2、回应凭证
        /// </summary>
        public int ImgFor { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CreateUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataTimestamp { get; set; }

    }
}
