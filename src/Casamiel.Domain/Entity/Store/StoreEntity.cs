﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain.Entity
{
	/// <summary>
	/// Mbr store entity.
	/// </summary>
	[Table("mbr_store")]
	public class StoreEntity
	{

		/// <summary>
		/// 自增Id
		/// </summary>
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int StoreId { get; set; }

		/// <summary>
		/// 状态，1有效，11锁定
		/// </summary>
		public int Status { get; set; }

		/// <summary>
		/// 对接信息Id
		/// </summary>
		public int RelationId { get; set; }

		/// <summary>
		/// 门店名称
		/// </summary>
		public string StoreName { get; set; }

		/// <summary>
		/// Gets or sets the name of the sub store.
		/// </summary>
		/// <value>The name of the sub store.</value>
		public string SubStoreName { get; set; }

		/// <summary>
		/// 背景图
		/// </summary>
		public string BackImage { get; set; }

		/// <summary>
		/// 营业时间（8：00--18：00）
		/// </summary>
		public string ShopHours { get; set; }

		/// <summary>
		/// 门店电话
		/// </summary>
		public string Phone { get; set; }

		/// <summary>
		/// 省ID
		/// </summary>
		public int ProvinceId { get; set; }

		/// <summary>
		/// 城市ID
		/// </summary>
		public int CityId { get; set; }

		/// <summary>
		/// 区县ID
		/// </summary>
		public int DistrictId { get; set; }

		/// <summary>
		/// 详细地址
		/// </summary>
		public string FullAddress { get; set; }

		/// <summary>
		/// 经度
		/// </summary>
		public double Longitude { get; set; }

		/// <summary>
		/// 纬度
		/// </summary>
		public double Latitude { get; set; }

		/// <summary>
		/// 是否更新坐标
		/// </summary>
		public bool IsChangeCoor { get; set; }
		/// <summary>
		/// 0自提，1非自提
		/// </summary>
		/// <value>The is self take.</value>
		public int IsSelfTake { get; set; }
		/// <summary>
		/// 是否开启配送
		/// 1开启，0未开启
		/// </summary>
		/// <value>The is open send.</value>
		public int IsOpenSend { get; set; }
		/// <summary>
		/// Gets or sets the is netstore.
		/// </summary>
		/// <value>The is netstore.</value>
		public int IsNetstore { get; set; }
		/// <summary>
		/// 默认配送方式，0不配送
		/// </summary>
		public int DefaultExpress { get; set; }
		/// <summary>
		/// 饿了么商家ID
		/// </summary>
		/// <value>The ELES tore identifier.</value>
		public string ELEStoreId { get; set; }
		/// <summary>
		/// 0正常，1闭店
		/// </summary>
		public int IsClose { get; set; }

		///// <summary>
		///// 创建人
		///// </summary>
		//public int CreateUserId { get; set; }

		///// <summary>
		///// 创建时间
		///// </summary>
		//public DateTime CreateTime { get; set; }

		///// <summary>
		///// 修改人
		///// </summary>
		//public int LastUpdateUserId { get; set; }

		///// <summary>
		///// 最后修改时间
		///// </summary>
		//public DateTime LastUpdateTime { get; set; }

		///// <summary>
		///// 
		///// </summary>
		//public DateTime DataTimestamp { get; set; }
		/// <summary>
		/// 是否网购门店 0默认 1网购
		/// </summary>
		/// <value>The is net store.</value>
		public int IsNetStore { get; set; }
		/// <summary>
		/// 是否无限库存
		/// 1无限库存，0有限库存
		/// </summary>
		/// <value>The is limit stock.</value>
		public int IsLimitStock { get; set; }

		/// <summary>
		/// 距离
		/// </summary>
		[NotMapped]
		public double Distance { get; set; }

		/// <summary>
		/// 配送半径
		/// </summary>
		/// <value>The distribution radius.</value>
		public decimal DistributionRadius { get; set; }

		/// <summary>
		/// 是否配送
		/// </summary>
		[NotMapped]
		public bool IsSend { get; set; }

		/// <summary>
		/// 蛋糕商场是否关闭
		/// 1关闭，0开启
		/// </summary>
		public int IsCakeShopClose { get; set; }

		/// <summary>
		/// 是否蛋糕商城营业
		/// 0营业，1未营业
		/// </summary>
		public int IsCakeClose { get; set; }

		/// <summary>
		/// 是否关闭外卖
		/// 1关闭，0开启
		/// </summary>
		public int IsWaimaiShopClose { get; set; }

		/// <summary>
		/// 是否外卖关闭 1 是 0 否
		/// </summary>
		public int IsWaimaiClose { get; set; }
		/// <summary>
		/// 是否外卖自提 1 是 0 否
		/// </summary>
		public int IsWaimaiSelfTake { get; set; }

		/// <summary>
		/// 是否外卖开启配送  1 是 0 否
		/// </summary>
		public int IsWaimaiOpenSend { get; set; }

		/// <summary>
		/// 是否显示
		/// </summary>
		/// <value>The is show.</value>
		public int IsShow { get; set; }



	}

}