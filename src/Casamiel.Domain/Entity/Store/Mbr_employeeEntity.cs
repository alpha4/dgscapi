﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// 员工信息表 - 实体类
    /// </summary>  
    [Table("mbr_employee")]
    public class Mbr_employeeEntity
    {
     
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeId { get; set; }                                     
        
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }                                     
        
        /// <summary>
        /// 手机号
        /// </summary>
        public string Mobile { get; set; }                                     
        
        /// <summary>
        /// 状态，1有效，11锁定
        /// </summary>
        public int Status { get; set; }                                     
        
        /// <summary>
        /// 
        /// </summary>
        public int CreateUserId { get; set; }                                     
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateTime { get; set; }                                     
        
        /// <summary>
        /// 
        /// </summary>
        public int LastUpdateUserId { get; set; }                                     
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime LastUpdateTime { get; set; }

        /// <summary>
        ///关联ID
        /// </summary>
        /// <value>The store relation identifier.</value>
        public int StoreRelationId { get; set; }
    } 
}
