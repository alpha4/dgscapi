﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// Shopping cart base.
    /// </summary>
	public class ShoppingCartBase
	{
		/// <summary>
		/// 购物Id
		/// </summary>
		public int ShoppingCartId { get; set; }
		/// <summary>
		/// 门店关联Id
		/// </summary>
		public int StoreRelationId { get; set; }
		/// <summary>
		/// 门店Id
		/// </summary>
		public int StoreId { get; set; }
		/// <summary>
		/// 门店商品ID
		/// </summary>
		public int GoodsId { get; set; }
		/// <summary>
		/// 商品ID
		/// </summary>
		public int GoodsBaseId { get; set; }
		/// <summary>
		/// 数量
		/// </summary>
		public int GoodsQuantity { get; set; }
		/// <summary>
		/// 是否选中
		/// </summary>
		public bool IsCheck { get; set; }
		/// <summary>
		/// 商品关联id
		/// </summary>
		public int GoodsRelationId { get; set; }
		/// <summary>
		/// 手机号 
		/// </summary>
		public string Mobile { get; set; }
		/// <summary>
		/// 标题
		/// </summary>
		public string Title { get; set; }
		/// <summary>
		/// 商品图片
		/// </summary>
		public string ImageUrl { get; set; }
		/// <summary>
		/// 状态 0 上架 1 下架
		/// </summary>
		public int Status { get; set; }
		/// <summary>
		/// 售价
		/// </summary>
		public decimal Price { get; set; }
		/// <summary>
		/// 原价
		/// </summary>
		public decimal CostPrice { get; set; }
	}
}
