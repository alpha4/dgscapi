  
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// 订单基础信息 - 实体类
    /// </summary>	
    [Table("order_base")]
    public sealed class OrderBaseEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderBaseId { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderCode { get; set; }

        /// <summary>
        /// 应付金额
        /// </summary>
        public Decimal OrderMoney { get; set; }

        /// <summary>
        /// 支付金额
        /// </summary>
        public Decimal PayMoney { get; set; }

        /// <summary>
        /// 订单状态，1：新建、2:已支付、7：完成、3:已换货 、9：交易关闭
        /// </summary>

        public int OrderStatus { get; set; }

        ///// <summary>
        ///// 支付状态，1：待支付、2：已支付
        ///// </summary>
        //public int PayStatus { get; set; }

        /// <summary>
        /// 快递状态，1：待发货、2：已发货、3：已签收
        /// </summary>
        public int ExpressStatus { get; set; }

        /// <summary>
        /// 省ID
        /// </summary>
        public int ProvinceId { get; set; }

        /// <summary>
        /// 城市ID
        /// </summary>
        public int CityId { get; set; }

        /// <summary>
        /// 区县ID
        /// </summary>
        public int DistrictId { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string FullAddress { get; set; }

        /// <summary>
        /// 联系人姓名
        /// </summary>
        public string ContactName { get; set; }

        /// <summary>
        /// 联系人电话
        /// </summary>
        public string ContactPhone { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 发货时间
        /// </summary>
        public DateTime? ShipmentsTime { get; set; }

        /// <summary>
        /// 收货时间 (完成时间)
        /// </summary>
        public DateTime? HarvestTime { get; set; }

        /// <summary>
        /// 备注包括发票抬头
        /// </summary>
        public string OrderRemark { get; set; }

        /// <summary>
        /// 订单类型 1: 自提 2:外卖 3 快递配送
        /// </summary>
        public int OrderType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string ResBillNo { get; set; }
		/// <summary>
		/// 运费金额
		/// </summary>
		public Decimal FreightMoney { get; set; }

        /// <summary>
        /// 确认收货到期时间
        /// </summary>
        public DateTime? ConfrimReceiveTime { get; set; }

        /// <summary>
        /// 付款时间
        /// </summary>
        public DateTime? PayTime { get; set; }

        /// <summary>
        /// 状态：0 删除，1正常
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 关闭时间
        /// </summary>
        public DateTime? CloseTime { get; set; }

        /// <summary>
        /// 实际支付现金金额
        /// </summary>
        public Decimal PayCashMoney { get; set; }

        /// <summary>
        /// 优惠价格（包含优惠卷）
        /// </summary>
        public Decimal DiscountMoney { get; set; }
		/// <summary>
		/// 0默认 1 美团 2 饿了么 3 京东 4 滴滴 5天猫,8外卖
		/// </summary>
		public int Source { get; set; }
		/// <summary>
		/// 手机号 识别身份
		/// </summary>
		public string Mobile { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CreateUserId { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int LastUpdateUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime LastUpdateTime { get; set; }

        ///// <summary>
        ///// 
        ///// </summary>
        //[NotMapped]
        //public DateTime DataTimestamp { get; set; }

        /// <summary>
        /// 提货码
        /// </summary>
        public string TakeCode { get; set; }

        /// <summary>
        /// IC交易号
        /// </summary>
        public string IcTradeNO { get; set; }

        /// <summary>
        /// 取消理由
        /// </summary>
        /// <value>The cancel reason.</value>
        public string CancelReason { get; set; }

        /// <summary>
        /// 门店Id
        /// </summary>
        public int StoreId { get; set; }

		/// <summary>
		/// 支付方式:1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
		/// </summary>
		public int PayMethod { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string InvoiceTitle { get; set; }
        
        /// <summary>
        /// 发票税号
        /// </summary>
        public string TaxpayerId { get; set; }
        /// <summary>
        /// 发票链接
        /// </summary>
        /// <value>The invoice URL.</value>
        public string InvoiceUrl { get; set; }
        /// <summary>
        /// 发票类型 0 公司抬头 1 个人
        /// </summary>
        public int? InvoiceType { get; set; }

        /// <summary>
        /// 地址 电话
        /// </summary>
        public string InvoiceAddress { get; set; }

        /// <summary>
        /// 开户行 账号
        /// </summary>
        public string InvoiceOpeningBank { get; set; }

        /// <summary>
        /// 接受发票邮箱
        /// </summary>
        public string InvoiceEMail { get; set; }

        /// <summary>
        /// 0 未申请 1 已申请 2 已开发票 
        /// </summary>
        public int? IsOpenInvoice { get; set; } 
    }         
} 
