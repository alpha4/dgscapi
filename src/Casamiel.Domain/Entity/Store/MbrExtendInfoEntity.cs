﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// Mbr extend info entity.
    /// </summary>
    [Table("mbr_extendinfo")]
    public class MbrExtendInfoEntity
    {
        /// <summary>
        /// Gets or sets the extend info identifier.
        /// </summary>
        /// <value>The extend info identifier.</value>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ExtendInfoId { get; set; }

        /// <summary>
        /// Gets or sets the member identifier.
        /// </summary>
        /// <value>The member identifier.</value>
        public int MemberId { get; set; }

        /// <summary>
        /// Gets or sets the user agent.
        /// </summary>
        /// <value>The user agent.</value>
        public string UserAgent{get;set;}
        /// <summary>
        /// Gets or sets the create time.
        /// </summary>
        /// <value>The create time.</value>
        public DateTime CreateTime { get; set; }
    }
}
