﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain.Entity
{
	/// <summary>
	/// 产品标签表 - 实体类
	/// </summary>	
	[Table("product_tagbase")]
	public class Product_tagbaseEntity
	{

		/// <summary>
		/// 自增Id
		/// </summary>
		[Key]
		[DatabaseGenerated( DatabaseGeneratedOption.Identity)]
		public int TagBaseId { get; set; }

		/// <summary>
		/// 标签名称，必填
		/// </summary>
		public string TagName { get; set; }

		/// <summary>
		/// 标签图片
		/// </summary>
		public string SImgUrl { get; set; }

		/// <summary>
		/// 标签大图
		/// </summary>
		public string BImgUrl { get; set; }

		/// <summary>
		/// 排序编号，倒序
		/// </summary>
		public int OrderNo { get; set; }

		/// <summary>
		/// 状态，1：正常、0：禁用
		/// </summary>
		public int Status { get; set; }

		/// <summary>
		/// 备注
		/// </summary>
		public string Remark { get; set; }

	}
}
