  
using System;
namespace Casamiel.Domain.Entity
{
    /// <summary>
	/// 订单退款信息 - 实体类
	/// </summary>	
	public class Order_RefundEntity
    {
	 
		/// <summary>
		/// 
		/// </summary>
        public int RefundId { get; set; }                                     
        
		/// <summary>
		/// 订单商品ID
		/// </summary>
        public int OrderGoodsId { get; set; }                                     
        
		/// <summary>
		/// 退款原因类型
		/// </summary>
        public int RefundCauseType { get; set; }                                     
        
		/// <summary>
		/// 退款原因文本
		/// </summary>
        public string RefundCauseTxt { get; set; }                                     
        
		/// <summary>
		/// 答复原因类型
		/// </summary>
        public int ReplyCauseType { get; set; }                                     
        
		/// <summary>
		/// 答复原因文本
		/// </summary>
        public string ReplyCauseTxt { get; set; }                                     
        
		/// <summary>
		/// 答复时间
		/// </summary>
        public DateTime ReplyTime { get; set; }                                     
        
		/// <summary>
		/// 状态，0：待确认、1：已确认、2：已驳回、3：撤销
		/// </summary>
        public int Status { get; set; }                                     
        
		/// <summary>
		/// 类型(0:系统默认处理,1:人工处理)
		/// </summary>
        public int RefundType { get; set; }                                     
        
		/// <summary>
		/// 退款金额
		/// </summary>
        public Decimal RefundMoney { get; set; }                                     
        
		/// <summary>
		/// 手机号 识别身份
		/// </summary>
        public string Mobile { get; set; }                                     
        
		/// <summary>
		/// 退款类型 1 仅退款 2 退款退货
		/// </summary>
        public int Type { get; set; }                                     
        
		/// <summary>
		/// 备注
		/// </summary>
        public string Remark { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public int CreateUserId { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public DateTime CreateTime { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public int LastUpdateUserId { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public DateTime LastUpdateTime { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public DateTime DataTimestamp { get; set; }                                     
                 
    }  
} 
	