﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// Good base entity.
    /// </summary>
    [Table("goods_base")]
    public sealed class GoodBaseEntity
    {
        /// <summary>
        /// Gets or sets the goods base identifier.
        /// </summary>
        /// <value>The goods base identifier.</value>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GoodsBaseId { get; set; } 
        /// <summary>
        /// Gets or sets the relation identifier.
        /// </summary>
        /// <value>The relation identifier.</value>
        public int RelationId { get; set; }
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        public string Title { get; set; }
        /// <summary>
        /// Gets or sets the product base identifier.
        /// </summary>
        /// <value>The product base identifier.</value>
        public int ProductBaseId { get; set; }
        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>The price.</value>
        public decimal Price { get; set; }
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public int Status { get; set; }
        /// <summary>
        /// Gets or sets the stock quentity.
        /// </summary>
        /// <value>The stock quentity.</value>
        public int StockQuentity { get; set; }
    }
}
