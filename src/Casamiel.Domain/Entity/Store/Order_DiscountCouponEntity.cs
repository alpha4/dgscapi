  
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{

    /// <summary>
    /// 订单关联优惠券 - 实体类
    /// </summary>	
    [Table("order_DiscountCoupon")]
    
    public class Order_DiscountCouponEntity
    {
        /// <summary>
        /// 自增ID
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int OrderDiscountId { get; set; }                                     
        /// <summary>
        /// 0 正常 1 已取消
        /// </summary>
        public int Status { get; set; }                                     
        
        /// <summary>
        /// 订单Id
        /// </summary>
        public int OrderBaseId { get; set; }                                     
        
        /// <summary>
        /// 优惠卷Id
        /// </summary>
        public long DiscountCouponId { get; set; }                                     
        
        /// <summary>
        /// 优惠券实际金额
        /// </summary>
        public Decimal DiscountCouponActualMoney { get; set; }                                     
        
        /// <summary>
        /// 优惠券抵用金额
        /// </summary>
        public Decimal DiscountCouponUseMoney { get; set; }             
        /// <summary>
        /// 会员卡号
        /// </summary>
        public string CardNO { get; set; }
        
        /// <summary>
        /// 1会员券，2代金券
        /// </summary>
        public int DiscountCouponType { get; set; }
            /// <summary>
            /// 
            /// </summary>
        public int CreateUserId { get; set; }                                     
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateTime { get; set; }                                     
        
                                    
                 
    }  
} 
	