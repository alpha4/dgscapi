﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// 首页产品
    /// </summary>
    public sealed class IndexGoods
    {
        /// <summary>
        /// 标签名称
        /// </summary>
        public string TagName { get; set; }
        /// <summary>
        /// 标签Id
        /// </summary>
        public int TagId { get; set; }
        /// <summary>
        /// 标签背景图
        /// </summary>
        public string TagBigImage { get; set; }

        /// <summary>
        /// 标签小图
        /// </summary>
        public string SImgUrl { get; set; }
        /// <summary>
        /// 门店Id
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 列表
        /// </summary>
        public List<ProductBase> ProductList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public class ProductBase
        {
            /// <summary>
            /// 产品Id
            /// </summary>
            public int ProductId { get; set; }
            /// <summary>
            /// 产品标题
            /// </summary>
            public string Title { get; set; }

			/// <summary>
			/// 产品副标题
			/// </summary>
			public string SubTitle { get; set; }
			/// <summary>
			/// 产品图片
			/// </summary>
			public string Image { get; set; }
            /// <summary>
            /// 产品价格
            /// </summary>
            public decimal Price { get; set; }
            /// <summary>
            /// 会员价
            /// </summary>
            /// <value>The cost price.</value>
            public decimal CostPrice { get; set; }
        }
    }
}
