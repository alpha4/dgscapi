  
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// 订单提货明细 - 实体类
    /// </summary>	
    [Table("order_goodstake")]
    public class Order_GoodsTakeEntity
    {

        /// <summary>
        /// 自增ID
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderGoodsrTakeId { get; set; }                                     
        
		/// <summary>
		/// 提货单ID
		/// </summary>
        public int OrderTakeId { get; set; }                                     
        
		/// <summary>
		/// 状态 0 未提货 1 已提货
		/// </summary>
        public int Status { get; set; }                                     
        
		/// <summary>
		/// 订单商品Id
		/// </summary>
        public int OrderGoodsId { get; set; }                                     
        
		/// <summary>
		/// 订单Id
		/// </summary>
        public string OrderBaseId { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public string Mobile { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public int CreateUserId { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public DateTime CreateTime { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public DateTime DataTimestamp { get; set; }                                     
                 
    }  
} 
	