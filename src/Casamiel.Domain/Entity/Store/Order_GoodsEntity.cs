  
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// 订单关联商品信息 - 实体类
    /// </summary>	
    [Table("order_goods")]
    public class Order_GoodsEntity
    {

        /// <summary>
        /// 自增Id
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderGoodsId { get; set; }                                     
        
		/// <summary>
		/// 订单ID
		/// </summary>
        public int OrderBaseId { get; set; }                                     
        
		/// <summary>
		/// 门店商品Id
		/// </summary>
        public int GoodsId { get; set; }                                     
        
		/// <summary>
		/// 门店关联Id
		/// </summary>
        public int StoreRelationId { get; set; }                                     
        
		/// <summary>
		/// 门店Id
		/// </summary>
        public int StoreId { get; set; }                                     
        
		/// <summary>
		/// 商品ID
		/// </summary>
        public int GoodsBaseId { get; set; }                                     
        
		/// <summary>
		/// 商品关联id
		/// </summary>
        public int GoodsRelationId { get; set; }                                     
        
		/// <summary>
		/// 商品标题
		/// </summary>
        public string GoodsTitle { get; set; }                                     
        
		/// <summary>
		/// 商品图片
		/// </summary>
        public string GoodsImageUrl { get; set; }                                     
        
		/// <summary>
		/// 数量
		/// </summary>
        public int GoodsQuantity { get; set; }                                     
        
		/// <summary>
		/// 商品单价，单位：元
		/// </summary>
        public Decimal GoodsPrice { get; set; }                                     
        
		/// <summary>
		/// 商品原价
		/// </summary>
        public Decimal GoodsCostPrice { get; set; }                                     
        
		/// <summary>
		/// 手机号 识别身份
		/// </summary>
        public string Mobile { get; set; }                                     
        
		/// <summary>
		/// 实际支付金额(不包含运费)
		/// </summary>
        public Decimal ActualPayMoney { get; set; }                                     
        
		/// <summary>
		/// 商品属性
		/// </summary>
        public string PropertyItemValues { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public int LastUpdateUserId { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public DateTime LastUpdateTime { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public int CreateUserId { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public DateTime CreateTime { get; set; }                                     
                                        
        
		                             
                 
    }  
} 
	