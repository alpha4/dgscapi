  
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// 商品退货凭证表 - 实体类
    /// </summary>	
    [Table("order_goodsrefundimage")]
    public class Order_GoodsRefundImageEntity
    {

        /// <summary>
        /// 
        /// </summary>

        public int Id { get; set; }                                     
        
		/// <summary>
		/// 商品退货记录ID
		/// </summary>
        public int GoodsRefundRecordId { get; set; }                                     
        
		/// <summary>
		/// 图片
		/// </summary>
        public string ImgUrl { get; set; }                                     
        
		/// <summary>
		/// 图片应用，1、申请凭证、2：回应凭证
		/// </summary>
        public int ImgFor { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public int CreateUserId { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public DateTime CreateTime { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public DateTime DataTimestamp { get; set; }                                     
                 
    }  
} 
	