﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// 提示记录表 - 实体类
    /// </summary>  
    [Table("notice_base")]
    public class NoticeBaseEntity
    {
     
        /// <summary>
        /// 
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int NoticeId { get; set; }                                     
        
        /// <summary>
        /// 提示类型 0 订单下单 1 提货单提醒  
        /// </summary>
        public int NoticeType { get; set; }                                     
        
        /// <summary>
        /// 关联Id
        /// </summary>
        public int RelationId { get; set; }                                     
        
        /// <summary>
        /// 提醒的门店Id
        /// </summary>
        public int StoreId { get; set; }                                     
        
        /// <summary>
        /// 状态 0 默认 1 已知晓  2 删除
        /// </summary>
        public int Status { get; set; }                                     
        
        /// <summary>
        /// 提醒次数
        /// </summary>
        public int Numbers { get; set; }                                     
        
        /// <summary>
        /// 提醒时间
        /// </summary>
        public DateTime RemindTime { get; set; }                                     
        
        /// <summary>
        /// CreateUserId
        /// </summary>
        public int CreateUserId { get; set; }                                     
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }                                     
                 
    }  
}
