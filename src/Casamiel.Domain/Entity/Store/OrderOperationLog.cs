﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{
	/// <summary>
	/// 
	/// </summary>
	///
	[Table("order_OperationLog")]
	public class OrderOperationLog
	{
		/// <summary>
		/// 
		/// </summary>
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int OrderBaseId { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string OrderCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Remark { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public int BillType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int CreateUserId { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public DateTime CreateTime { get; set; }
		 
	}
}
