  
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// 订单商品退货记录表 - 实体类
    /// </summary>	
    [Table("order_GoodsRefundRecord")]
    public class Order_GoodsRefundRecordEntity
    {

        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }                                     
        
		/// <summary>
		/// 物流公司ID
		/// </summary>
        public string LogisticsCompanyId { get; set; }                                     
        
		/// <summary>
		/// 物流单号
		/// </summary>
        public string LogisticsNo { get; set; }                                     
        
		/// <summary>
		/// 备注
		/// </summary>
        public string Remark { get; set; }                                     
        
		/// <summary>
		/// 商品退货地址
		/// </summary>
        public string GoodsRefundAddress { get; set; }                                     
        
		/// <summary>
		/// 商品联系人姓名
		/// </summary>
        public string GoodsRefundContactName { get; set; }                                     
        
		/// <summary>
		/// 商品联系人手机
		/// </summary>
        public string GoodsRefundContactMobile { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public int CreateUserId { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public DateTime CreateTime { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public int LastUpdateUserId { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public DateTime LastUpdateTime { get; set; }                                     
        
		/// <summary>
		/// 
		/// </summary>
        public DateTime DataTimestamp { get; set; }                                     
        
		/// <summary>
		/// 订单商品ID
		/// </summary>
        public int OrderGoodsId { get; set; }                                     
        
		/// <summary>
		/// 答复原因类型，SysConfig中的Key
		/// </summary>
        public string GoodsReplyCauseType { get; set; }                                     
        
		/// <summary>
		/// 答复原因文本
		/// </summary>
        public string GoodsReplyCauseTxt { get; set; }                                     
        
		/// <summary>
		/// 答复时间
		/// </summary>
        public DateTime GoodsReplyTime { get; set; }                                     
        
		/// <summary>
		/// 状态，0：待确认【待买家发货】、1：买家确认【买家已发货】、2:卖家确认【买家收货】、3：已驳回【卖家拒绝收货】
		/// </summary>
        public int Status { get; set; }                                     
        
		/// <summary>
		/// 类型(0:系统默认处理,1:人工处理)
		/// </summary>
        public int RefundType { get; set; }                                     
        
		/// <summary>
		/// 退款ID
		/// </summary>
        public int RefundId { get; set; }                                     
        
		/// <summary>
		/// 手机号 识别身份
		/// </summary>
        public string Mobile { get; set; }                                     
                 
    }  
} 
	