  
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// 订单提货单 - 实体类
    /// </summary>	
    [Table("order_take")]
    public class Order_TakeEntity
    {

        /// <summary>
        /// 自增ID
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderTakeId { get; set; }                                     
        /// <summary>
        /// 订单Id
        /// </summary>
        public int OrderBaseId { get; set; }                                     
        
        /// <summary>
        /// 提货码
        /// </summary>
        public string TakeCode { get; set; }                                     
        
        /// <summary>
        /// IC交易号
        /// </summary>
        public string IcTradeNO { get; set; }                                     
        
        /// <summary>
        /// 状态 0 未提货 1 已提货
        /// </summary>
        public int Status { get; set; }                                     
        
        /// <summary>
        /// 提货时间
        /// </summary>
        public DateTime TakeTime { get; set; }                                     
        
        /// <summary>
        /// 已提货时间
        /// </summary>
        public DateTime TakedTime { get; set; }                                     
        
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; set; }                                     
        
        /// <summary>
        /// 门店Id
        /// </summary>
        public int StoreId { get; set; }                                     
        
        /// <summary>
        /// 门店关联Id
        /// </summary>
        public int StoreRelationId { get; set; }                                     
        
        /// <summary>
        /// 
        /// </summary>
        public int CreateUserId { get; set; }                                     
        
        /// <summary>
        /// Gets or sets the create time.
        /// </summary>
        /// <value>The create time.</value>
        public DateTime CreateTime { get; set; }                                     
        
        ///// <summary>
        ///// Gets or sets the data timestamp.
        ///// </summary>
        ///// <value>The data timestamp.</value>
        //public DateTime DataTimestamp { get; set; }                                     
              
    }  
} 
	