﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain
{
	/// <summary>
	/// 会员券
	/// </summary>
	[Table("member_coupon")]
	public class MemberCoupon
	{
		/// <summary>
		/// 流水号
		/// </summary>
		[Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long MCID { get; set; }

		/// <summary>
		/// 会员流水号
		/// </summary>
		public long MID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Ticketcode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string OldTicketcode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public long Productid { get; set; }

		 
		/// <summary>
		/// 券名称
		/// </summary>
		public string Productname { get; set; }

		/// <summary>
		///  金额
		/// </summary>
		public decimal Price { get; set; }

		/// <summary>
		/// 状态，-2、已销毁；-1、不可用； 0、已使用；1、已过期；2、未启用；3、可使用
		/// </summary>
		public int State { get; set; }

		/// <summary>
		/// 0可赠送；1获赠；-1赠送中；-2已赠送
		/// </summary>
		public int GiveStatus { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public DateTime Startdate{get;set;}

		/// <summary>
		/// 
		/// </summary>
		public DateTime Enddate { get; set; }

		/// <summary>
		/// 使用日期
		/// </summary>
		public DateTime? UsedDate { get; set; }

		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime CreateTime { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string OrderCode { get; set; }

	}
}
