﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// Sa tester.
    /// </summary>
	[Table("sa_tester")]
    public class SaTester
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id
		{
			get;set;
		}
        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
		[Key]
		public string Mobile { get; set; }
        /// <summary>
        /// Gets or sets the remark.
        /// </summary>
        /// <value>The remark.</value>
		public string Remark { get; set; }
        /// <summary>
        /// Gets or sets the chargemoney.
        /// </summary>
        /// <value>The chargemoney.</value>
        public double chargemoney { get; set; }
        /// <summary>
        /// Gets or sets the create time.
        /// </summary>
        /// <value>The create time.</value>
		public DateTime? CreateTime { get; set; }
	}
}
