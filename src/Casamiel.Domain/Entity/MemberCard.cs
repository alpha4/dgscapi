﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// Member card.
    /// </summary>
	[Table("member_card")]
	public class MemberCard
    {
        /// <summary>
        /// Gets or sets the c identifier.
        /// </summary>
        /// <value>The c identifier.</value>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long C_ID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long? M_ID { get; set; }
		/// <summary>
        /// Gets or sets the card no.
        /// </summary>
        /// <value>The card no.</value>
        [Key]
		public string CardNO { get; set; }
        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
		//[ForeignKey("FK_MEMBER_C_REFERENCE_MEMBER")]
		public string Mobile { get; set; }
		/// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:Casamiel.Domain.Entity.MemberCard"/> is bind.
        /// </summary>
        /// <value><c>true</c> if is bind; otherwise, <c>false</c>.</value>
        public bool IsBind { get; set; }
		/// <summary>
		/// 卡类型（1实体卡，2虚拟卡,3礼品卡，4员工卡）
		/// </summary>
		public string CardType { get; set; }
		/// <summary>
        /// Gets or sets the bind time.
        /// </summary>
        /// <value>The bind time.</value>
        public DateTime? BindTime { get; set; }
		/// <summary>
        /// Gets or sets the un bind time.
        /// </summary>
        /// <value>The un bind time.</value>
        public DateTime? UnBindTime { get; set; }
		/// <summary>
		/// 实体卡状态:1启用，2禁用
		/// </summary>
		public int State { get; set; }
        /// <summary>
        /// Gets or sets the create time.
        /// </summary>
        /// <value>The create time.</value>
		public DateTime CreateTime { get; set; }
        /// <summary>
        /// 来源，11、微信(默认)；15、手机APP；16、PAD.
        /// </summary>
        /// <value>The source.</value>
        public int Source { get; set; }
        // public Member Member { get; set; }
    }
}
