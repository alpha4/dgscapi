﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// Wx open identifier.
    /// </summary>
	[Table("wx_openid")]
	public class WxOpenId
    {
		/// <summary>
		/// 
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AppId { get; set; }
		/// <summary>
		/// 
		/// </summary>
		[Key]
		public string OpenId { get; set; }


		/// <summary>
		/// 
		/// </summary>
		public string NickName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int Gender { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string UnionId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Session_key { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime Create_time { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? Update_time { get; set; }
    }
}
