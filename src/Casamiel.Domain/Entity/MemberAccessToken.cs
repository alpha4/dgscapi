﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain.Entity
{
	/// <summary>
	/// 会员token
	/// </summary>
	[Table("member_access_token")]
	public class MemberAccessToken
	{
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }
		/// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
		public string Mobile { get; set; }
		/// <summary>
        ///  1App,2Pc,3微信，4预定下单,5快乐土地
        /// </summary>
        /// <value>The type of the login.</value>
        public int Login_Type { get; set; }
		/// <summary>
        /// Gets or sets the access token.
        /// </summary>
        /// <value>The access token.</value>
        public string Access_Token { get; set; }
		/// <summary>
        /// Gets or sets the expires in.
        /// </summary>
        /// <value>The expires in.</value>
        public long Expires_In { get; set; }
		/// <summary>
        /// Gets or sets the resgistration identifier.
        /// </summary>
        /// <value>The resgistration identifier.</value>
        public string Resgistration_Id { get; set; }
		/// <summary>
        /// Gets or sets the login count.
        /// </summary>
        /// <value>The login count.</value>
        public int Login_Count { get; set; }
		/// <summary>
        /// Gets or sets the create time.
        /// </summary>
        /// <value>The create time.</value>
        public DateTime CreateTime { get; set; }
		/// <summary>
        /// Gets or sets the update time.
        /// </summary>
        /// <value>The update time.</value>
        public DateTime UpdateTime { get; set; }

	}
}
