﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Entity
{
	/// <summary>
	/// Order add ele req.
	/// </summary>
	public class ElmOrderView
	{
		/// <summary>
		/// Gets or sets the order identifier.
		/// </summary>
		/// <value>The order identifier.</value>
		public string OrderId { get; set; }
		/// <summary>
		/// Gets or sets the ic trade no.
		/// </summary>
		/// <value>The ic trade no.</value>


		/// <summary>
		/// 顾客送餐地址
		/// </summary>
		public string Address { get; set; }

		/// <summary>
		/// 下单时间
		/// </summary>
		public DateTime? CreatedAt { get; set; }

		/// <summary>
		/// 订单生效时间
		/// </summary>
		public DateTime? ActiveAt { get; set; }

		/// <summary>
		/// 配送费
		/// </summary>
		public Decimal DeliverFee { get; set; }

		/// <summary>
		/// 会员减配送费
		/// </summary>
		public Decimal VipDeliveryFeeDiscount { get; set; }

		/// <summary>
		/// 预计送达时间
		/// </summary>
		public DateTime? DeliverTime { get; set; }
#pragma warning disable IDE1006 // 命名样式
		/// <summary>
		/// 备注
		/// </summary>
		public string Description { get; set; }
#pragma warning restore IDE1006 // 命名样式

#pragma warning disable IDE1006 // 命名样式
		/// <summary>
		/// 订单详细类目的列表
		/// </summary>
		public List<OGoodsGroup> Groups { get; set; }
#pragma warning restore IDE1006 // 命名样式

		/// <summary>
		/// 发票抬头
		/// </summary>
		public string Invoice { get; set; }

		/// <summary>
		/// 是否预订单
		/// </summary>
		public bool Book { get; set; }

		/// <summary>
		/// 是否在线支付
		/// </summary>
		public bool OnlinePaid { get; set; }

		/// <summary>
		/// 订单Id
		/// </summary>
		public string Id { get; set; }

		/// <summary>
		/// 顾客联系电话 ["13507701342"]
		/// </summary>
		public List<string> PhoneList { get; set; }

		/// <summary>
		/// 店铺Id
		/// </summary>
		public long ShopId { get; set; }

		/// <summary>
		/// 店铺绑定的外部ID
		/// </summary>
		public string OpenId { get; set; }

		/// <summary>
		/// 店铺名称
		/// </summary>
		public string ShopName { get; set; }

		/// <summary>
		/// 店铺当日订单流水号
		/// </summary>
		public int DaySn { get; set; }

		/// <summary>
		/// pending=未生效;unprocessed=未处理;refunding=退单处理中;valid=已处理;invalid=无效订单;settled=已完成
		/// </summary>
		public string Status { get; set; }

		/// <summary>
		/// noRefund=未申请;applied=用户申请退单;rejected=店铺拒绝退单;arbitrating=客服仲裁中;failed=退单失败;successful=退单成功
		/// </summary>
		public string RefundStatus { get; set; }

		/// <summary>
		/// 下单用户的Id
		/// </summary>
		public int UserId { get; set; }

		/// <summary>
		/// 下单用户的Id
		/// </summary>
		public string UserIdStr { get; set; }

		/// <summary>
		/// 订单总价，用户实际支付的金额，单位：元
		/// </summary>
		public Decimal TotalPrice { get; set; }

		/// <summary>
		/// 订单原始价格
		/// </summary>
		public Decimal OriginalPrice { get; set; }

		/// <summary>
		/// 订单收货人姓名
		/// </summary>
		public string Consignee { get; set; }

		/// <summary>
		/// 订单收货地址经纬度(高德地图坐标系)
		/// </summary>
		public string DeliveryGeo { get; set; }

		/// <summary>
		/// 顾客送餐地址
		/// </summary>
		public string DeliveryPoiAddress { get; set; }

		/// <summary>
		/// 顾客是否需要发票
		/// </summary>
		public bool Invoiced { get; set; }

		/// <summary>
		/// 店铺实收
		/// </summary>
		public Decimal Income { get; set; }

		/// <summary>
		/// 饿了么服务费率
		/// </summary>
		public Decimal ServiceRate { get; set; }

		/// <summary>
		/// 饿了么服务费
		/// </summary>
		public Decimal ServiceFee { get; set; }

		/// <summary>
		/// 订单中红包金额
		/// </summary>
		public Decimal Hongbao { get; set; }

		/// <summary>
		/// 餐盒费
		/// </summary>
		public Decimal PackageFee { get; set; }

		/// <summary>
		/// 订单活动总额
		/// </summary>
		public Decimal ActivityTotal { get; set; }

		/// <summary>
		/// 店铺承担活动费用
		/// </summary>
		public Decimal ShopPart { get; set; }

		/// <summary>
		/// 饿了么承担活动费用
		/// </summary>
		public Decimal ElemePart { get; set; }

		/// <summary>
		/// 降级标识
		/// </summary>
		public bool Downgraded { get; set; }

		/// <summary>
		/// 保护小号失效时间
		/// </summary>
		public DateTime SecretPhoneExpireTime { get; set; }

		/// <summary>
		/// 订单参加活动信息
		/// </summary>
		public List<OActivity> OrderActivities { get; set; }

		/// <summary>
		/// 发票类型 personal=个人;company=企业
		/// </summary>
		public string InvoiceType { get; set; }

		/// <summary>
		/// 纳税人识别号
		/// </summary>
		public string TaxpayerId { get; set; }

		/// <summary>
		/// 冷链加价费
		/// </summary>
		public Decimal ColdBoxFee { get; set; }

		/// <summary>
		/// 用户取消原因
		/// </summary>
		public string CancelOrderDescription { get; set; }

		/// <summary>
		/// 用户申请取消时间
		/// </summary>
		public DateTime? CancelOrderCreatedAt { get; set; }

		/// <summary>
		/// 抽佣
		/// </summary>
		public List<OCommission> OrderCommissions { get; set; }

		/// <summary>
		/// 是否百度外卖来源
		/// </summary>
		public bool BaiduWaimai { get; set; }

#pragma warning disable IDE1006 // 命名样式
		/// <summary>
		/// 顾客联系电话["1350****342"]
		/// </summary>
		public List<string> ConsigneePhones { get; set; }
#pragma warning restore IDE1006 // 命名样式

		/// <summary>
		/// 订单商品额外信息
		/// </summary>
		public UserExtraInfo UserExtraInfo { get; set; }

		/// <summary>
		/// 超级会员标识 NOT_VIP=非会员;ELEME_SUPER_VIP=饿了么超级会员;BAIDU_SUPER_VIP=百度超级会员;
		/// </summary>
		public string SuperVip { get; set; }

		/// <summary>
		/// 商家确认出餐时间
		/// </summary>
		public DateTime? ConfirmCookingTime { get; set; }

	}


	#region 抽佣
	/// <summary>
	/// OC ommission.
	/// </summary>
	public class OCommission
	{
		/// <summary>
		/// 活动id
		/// </summary>
		public long Id { get; set; }
		/// <summary>
		/// 活动类别
		/// </summary>
		public int CategoryId { get; set; }
		/// <summary>
		/// CPS类型
		/// </summary>
		public int Type { get; set; }
		/// <summary>
		/// 活动名称
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// 描述
		/// </summary>
		public string DescriptionddD { get; set; }
	}
	#endregion


	#region 订单活动信息类
	/// <summary>
	/// 活动信息
	/// </summary>
	public class OActivity
	{
		/// <summary>
		/// 活动id
		/// </summary>
		public long Id { get; set; }
		/// <summary>
		/// 活动名称
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// 活动类别 11=食物活动；12=餐厅活动；15=商家代金券抵扣；200=限时抢购
		/// </summary>
		public int CategoryId { get; set; }
		/// <summary>
		/// 饿了么承担部分
		/// </summary>
		public decimal ElemePart { get; set; }
		/// <summary>
		/// 商家承担部分
		/// </summary>
		public decimal RestaurantPart { get; set; }
		/// <summary>
		/// 代理商承担部分
		/// </summary>
		public decimal FamilyPart { get; set; }
		/// <summary>
		/// 金额
		/// </summary>
		public decimal Amount { get; set; }
		/// <summary>
		/// 各方承担部分
		/// </summary>
		public List<AllPartiesPart> OrderAllPartiesPartList { get; set; }
	}
	/// <summary>
	/// 各方承担部分
	/// </summary>
	public class AllPartiesPart
	{
		/// <summary>
		/// 承担方名称
		/// </summary>
		public string PartName { get; set; }
		/// <summary>
		/// 承担方承担金额
		/// </summary>
		public string PartAmount { get; set; }
	}
	#endregion
	#region 订单详细类    
	/// <summary>
	/// 订单详细类目
	/// </summary>
	public class OGoodsGroup
	{
		/// <summary>
		/// 分组名称 1号篮子
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// 分组类型 normal=正常的菜品;discount=赠品
		/// </summary>
		public string Type { get; set; }
		/// <summary>
		/// 商品信息的列表
		/// </summary>
		public List<OGoodsItem> Items { get; set; }
		/// <summary>
		/// 套餐关联 [{"8888888888","3333333333"}]
		/// </summary>
		public List<List<string>> RelatedItems { get; set; }
	}
	/// <summary>
	/// 商品信息
	/// </summary>
	public class OGoodsItem
	{
		/// <summary>
		/// 食物id
		/// </summary>
		public long Id { get; set; }
		/// <summary>
		/// 规格Id
		/// </summary>
		public long SkuId { get; set; }
		/// <summary>
		/// 商品名称
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// 订单中商品项的标识
		/// </summary>
		public long CategoryId { get; set; }
		/// <summary>
		/// 商品单价
		/// </summary>
		public decimal Price { get; set; }
		/// <summary>
		/// 商品数量
		/// </summary>
		public int Quantity { get; set; }
		/// <summary>
		/// 总价
		/// </summary>
		public decimal Total { get; set; }
		/// <summary>
		/// 多规格
		/// </summary>
		public List<OGroupItemSpec> NewSpecs { get; set; }
		/// <summary>
		/// 多属性
		/// </summary>
		public List<OGroupItemAttribute> Attributes { get; set; }
		/// <summary>
		/// 商品扩展码 重要
		/// </summary>
		public string ExtendCode { get; set; }
		/// <summary>
		/// 商品条形码
		/// </summary>
		public string BarCode { get; set; }

		/// <summary>
		/// 用户侧价格
		/// </summary>
		public decimal UserPrice { get; set; }
		/// <summary>
		/// 商户侧价格
		/// </summary>
		public decimal ShopPrice { get; set; }
		/// <summary>
		/// 商品id
		/// </summary>
		public long VfoodId { get; set; }
		/// <summary>
		/// 配料
		/// </summary>
		public List<Ingredient> Ingredients { get; set; }
	}

	/// <summary>
	/// 规格
	/// </summary>
	public class OGroupItemSpec
	{
		/// <summary>
		/// 规格名称
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// 规格值
		/// </summary>
		public string Value { get; set; }
	}
	/// <summary>
	/// 属性
	/// </summary>
	public class OGroupItemAttribute
	{
		/// <summary>
		/// 属性名称
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// 属性值
		/// </summary>
		public string Value { get; set; }
	}
	/// <summary>
	/// 配料
	/// </summary>
	public class Ingredient
	{
		/// <summary>
		/// 配料code
		/// </summary>
		public string SkuCode { get; set; }
		/// <summary>
		/// 配料名称
		/// </summary>
		public string Name { get; set; }
	}
	#endregion

	/// <summary>
	/// User extra info.
	/// </summary>
	public class UserExtraInfo
	{
		/// <summary>
		/// 
		/// </summary>
		public string GiverPhone { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string Greeting { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string Remark { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string InvoiceTitle { get; set; }
	}
}
