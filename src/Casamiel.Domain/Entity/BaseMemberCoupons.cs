﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{
	/// <summary>
	/// 代金券
	/// </summary>
	[Table("base_member_coupons")]
	public class BaseMemberCoupons
	{
		/// <summary>
		/// 流水号
		/// </summary>
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long ID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string TicketCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Productname { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public long ProductId { get; set; }
		/// <summary>
		/// 金额
		/// </summary>
		public decimal Price { get; set; }

		/// <summary>
		/// 1未分配，0已分配
		/// </summary>
		public int State { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public DateTime CreateTime { get; set; }

		/// <summary>
		///
		/// </summary>
		public DateTime? UpdateTime{get;set;}
	}
}
