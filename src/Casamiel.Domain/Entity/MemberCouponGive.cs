﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain
{
	/// <summary>
	/// 券转赠
	/// 
	/// </summary>
	[Table("member_coupon_give")]
	public class MemberCouponGive
	{
		/// <summary>
		/// 流水号
		/// </summary>
		
		[Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long MCGID { get; set; }

		/// <summary>
		/// 券流水号
		/// </summary>
		public long MCID { get; set; }

		/// <summary>
		/// 会员流水号
		/// </summary>
		public long MID { get; set; }

		/// <summary>
		/// 券名称
		/// </summary>
		public string Productname { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string ForwardCode { get; set; }

		/// <summary>
		/// 0发起转赠；1转赠成功，-1转赠超时
		/// </summary>
		public int Status { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public DateTime? LastTime { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public DateTime CreateTime { get; set; }
	}
}
