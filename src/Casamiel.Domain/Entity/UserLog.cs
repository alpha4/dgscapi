﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain
{
	/// <summary>
	/// sa_user_log: sa_user_log
	/// </summary>
	[Table("sa_user_log")]
	public partial class UserLog
    {
        private long _id;
        private DateTime _optime;
        private string _url;
        private string _opinfo;
        private string _username;
        private string _userip;
        
        /// <summary>
        /// ID
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long ID
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// OPTime
        /// </summary>
        public DateTime OPTime
        {
            get { return _optime; }
            set { _optime = value; }
        }

        /// <summary>
        /// Url
        /// </summary>
        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }

        /// <summary>
        /// OPInfo
        /// </summary>
        public string OPInfo
        {
            get { return _opinfo; }
            set { _opinfo = value; }
        }

        /// <summary>
        /// UserName
        /// </summary>
        public string UserName
        {
            get { return _username; }
            set { _username = value; }
        }
        

        /// <summary>
        /// UserIP
        /// </summary>
        public string UserIP
        {
            get { return _userip; }
            set { _userip = value; }
        }


    }
}
