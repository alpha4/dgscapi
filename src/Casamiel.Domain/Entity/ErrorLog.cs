﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain
{
    /// <summary>
    /// Error log.
    /// </summary>
	[Table("sa_error_log")]
	public  class ErrorLog
    {

		/// <summary>
		/// ID
		/// </summary>
		/// 
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long LogID
        {
            get;set;
        }

        /// <summary>
        /// OPTime
        /// </summary>
        public DateTime OPTime
        {
            get;set;
        }

        /// <summary>
        /// Url
        /// </summary>
        public string Url
        {
            get;set;
        }

        /// <summary>
        /// Loginfo
        /// </summary>
        public string Loginfo
        {
            get;set;
        }

        /// <summary>
        /// StackTrace
        /// </summary>
        public string StackTrace
        {
            get;set;
        }
        /// <summary>
        /// 类型：1系统，2业务
        /// </summary>
        /// <value>The type.</value>
        public int Type{
            get;set;
        }

    }
}
