﻿using Dapper.FastCrud;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// Mobile checkcode.
    /// </summary>
    [Table("mobile_checkcode")]
    public partial class MobileCheckcode//: Smooth.IoC.Repository.UnitOfWork.Entity<int>
    {
        private long _id;

        private string _mobile;
        private string _checkcode;
        private DateTime _checkcode_outdate;
        private int _send_num;
        private DateTime _create_time;

        /// <summary>
        /// id
        /// </summary>
        //[DatabaseGeneratedDefaultValue]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// 手机号
        /// </summary>
        /// 
        [Key]
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        /// <summary>
        /// 激活或找回密码时,存储的校验码
        /// </summary>
        public string Checkcode
        {
            get { return _checkcode; }
            set { _checkcode = value; }
        }

        /// <summary>
        /// 激活码的过期时间
        /// </summary>
        public DateTime Checkcode_outdate
        {
            get { return _checkcode_outdate; }
            set { _checkcode_outdate = value; }
        }

        /// <summary>
        /// 当天发送的次数
        /// </summary>
        public int Send_num
        {
            get { return _send_num; }
            set { _send_num = value; }
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime Create_time
        {
            get { return _create_time; }
            set { _create_time = value; }
        }
    }
}
