﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// Member coupon record.
    /// </summary>
    [Table("member_coupon_record")]
    public class MemberCouponRecord
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        /// <summary>
        /// Gets or sets the activity identifier.
        /// </summary>
        /// <value>The activity identifier.</value>
        public int? ActivityId { get; set; }

        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; set; }

        /// <summary>
        /// Gets or sets the card no.
        /// </summary>
        /// <value>The card no.</value>
        public string CardNO { get; set; }
        /// <summary>
        /// Gets or sets the largessno.
        /// </summary>
        /// <value>The largessno.</value>
        public string Largessno { get; set; }
        /// <summary>
        /// Gets or sets the request info.
        /// </summary>
        /// <value>The request info.</value>
        public string RequestInfo { get; set; }
        /// <summary>
        /// Gets or sets the create time.
        /// </summary>
        /// <value>The create time.</value>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public int Status { get; set; }

        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderCode { get; set; }
    }
}
