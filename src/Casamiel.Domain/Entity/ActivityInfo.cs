﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// Activity info.
    /// </summary>
    public class ActivityInfo
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the name of the activity.
        /// </summary>
        /// <value>The name of the activity.</value>
        public string ActivityName{get;set;}
        /// <summary>
        /// 描述
        /// </summary>
        /// <value>The describe.</value>
        public string Describe { get; set; }
        /// <summary>
        /// Gets or sets the begin time.
        /// </summary>
        /// <value>The begin time.</value>
        public DateTime BeginTime { get; set; }
        /// <summary>
        /// Gets or sets the end time.
        /// </summary>
        /// <value>The end time.</value>
        public DateTime EndTime { get; set; }
        /// <summary>
        ///总共领用次数
        /// </summary>
        /// <value>The total.</value>
        public int Total { get; set; }
		
		/// <summary>
		/// 0当次活动，1每天
		/// </summary>
		public int Flag { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:Casamiel.Domain.Entity.ActivityInfo"/> is enable.
        /// </summary>
        /// <value><c>true</c> if enable; otherwise, <c>false</c>.</value>
        public bool Enable { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public bool IsShow { get; set; }

		/// <summary>
		/// 领用次数
		/// </summary>
		public int GetCount { get; set; }
		/// <summary>
		/// 排序
		/// </summary>
		public int Sort { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsTest { get; set; }
        /// <summary>
        /// Gets or sets the testers.
        /// </summary>
        /// <value>The testers.</value>
        public List<Tester> Testers { get; set; }
        /// <summary>
        /// Gets or sets the tickets.
        /// </summary>
        /// <value>The tickets.</value>
        public List<Ticket> Tickets { get; set; }
    }
    /// <summary>
    /// Ticket.
    /// </summary>
    public class Ticket
    {
        /// <summary>
        /// Gets or sets the ticket identifier.
        /// </summary>
        /// <value>The ticket identifier.</value>
        public int TicketId { get; set; }
        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>The price.</value>
        public double Price { get; set; }
        /// <summary>
        /// Gets or sets the count.
        /// </summary>
        /// <value>The count.</value>
        public int Count { get; set; }
    }
    /// <summary>
    /// Tester.
    /// </summary>
    public class Tester{
        /// <summary>
        /// Gets or sets the phoneno.
        /// </summary>
        /// <value>The phoneno.</value>
        public string phoneno { get; set; }
    }
}
