﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Casamiel.Domain.Entity
{
    /// <summary>
    /// 小程序设置
    /// </summary>
	/// 
	[Table("miniapp_settings")]
    public class MiniAppSettings
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }
        /// <summary>
        /// 小程序appId
        /// </summary>
		[Key]
        public string AppId { get; set; }
        /// <summary>
        /// 密钥
        /// </summary>
        public string Secret { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Sort { get; set; }
    }
}
