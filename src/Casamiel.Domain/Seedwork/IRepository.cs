﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Seedwork
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAggregateRoot
    {

    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : IAggregateRoot
    {
        /// <summary>
        /// 
        /// </summary>
        IUnitOfWork UnitOfWork { get; }
    }
}
 
