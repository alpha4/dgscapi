﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// Member card repository.
    /// </summary>
	public interface IMemberCardRepository:IRepositoryExt<MemberCard,string>
    {
        /// <summary>
        /// Gets  list async 
        /// </summary>
        /// <returns>The card list async by mobile.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
		Task<List<MemberCard>> GetListAsync<TSession>(string mobile) where TSession : class, ISession;

        /// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="CardNO"></param>
		/// <returns></returns>
        Task<bool> DeleteByCardNO<TSession>(string CardNO) where TSession : class, ISession;

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="mobile"></param>
        /// <param name="isGiftCard"></param>
        /// <returns></returns>
        Task<MemberCard> FindAsync<TSession>(string mobile, bool isGiftCard) where TSession : class, ISession;

        /// <summary>
        /// Finds the async by cardno mobile.
        /// </summary>
        /// <returns>The async by cardno mobile.</returns>
        /// <param name="cardno">Cardno.</param>
        /// <param name="mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<MemberCard> FindAsync<TSession>(string cardno, string mobile) where TSession : class, ISession;
        /// <summary>
        /// 绑定的卡
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="mobile"></param>
        /// <returns></returns>
        Task<MemberCard> GetBindCardAsync<TSession>(string mobile) where TSession : class, ISession;
        //Task Tes<TSession>() where TSession : class, ISession;
        //Task<MemberCard> GetWithJoins<TSession>(string key) where TSession : class, ISession;

            /// <summary>
            /// 
            /// </summary>
            /// <typeparam name="TSession"></typeparam>
            /// <param name="a"></param>
            /// <param name="b"></param>
            /// <returns></returns>
        Task<List<MemberCard>> GetListAsny<TSession>(string a, string b) where TSession : class, ISession;

        
    }

}
