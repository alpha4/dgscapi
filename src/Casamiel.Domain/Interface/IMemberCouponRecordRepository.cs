﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Domain
{
    /// <summary>
    /// Member coupon record repository.
    /// </summary>
    public interface IMemberCouponRecordRepository:IRepositoryExt<MemberCouponRecord, int>
    {
        /// <summary>
        /// Gets the count by activity identifier aync.
        /// </summary>
        /// <returns>The count by activity identifier aync.</returns>
        /// <param name="ActivityId">Activity identifier.</param>
		/// <param name="flag"></param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        
        Task<int> GetCountAsync<TSession>(int ActivityId,int flag) where TSession : class, ISession;
        /// <summary>
        /// Gets the count by activity identifier mobile aync.
        /// </summary>
        /// <returns>The count by activity identifier mobile aync.</returns>
        /// <param name="ActivityId">Activity identifier.</param>
        /// <param name="Mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<int> GetCountAsync<TSession>(int ActivityId, string Mobile) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="ActivityId"></param>
		/// <param name="Mobile"></param>
		/// <param name="flag">0当次活动，1每天</param>
		/// <returns></returns>
		Task<int> GetCountAsync<TSession>(int ActivityId, string Mobile,int flag) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="ActivityId"></param>
		/// <param name="CardNO"></param>
		/// <returns></returns>
		Task<MemberCouponRecord> FindAsync<TSession>(int ActivityId, string CardNO) where TSession : class, ISession;


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="ActivityId"></param>
        /// <param name="OrderCode"></param>
        /// <returns></returns>
        Task<MemberCouponRecord> FindByCorderCodeAsync<TSession>(int ActivityId, string OrderCode) where TSession : class, ISession;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="ActivityId"></param>
        /// <returns></returns>
        Task<List<MemberCouponRecord>> GetListAsync<TSession>(int ActivityId) where TSession : class, ISession;
    }
}
