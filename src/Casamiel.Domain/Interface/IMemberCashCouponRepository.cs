﻿using Casamiel.Domain.Entity;
using Casamiel.Domain.Response;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// 
    /// </summary>
    public  interface IMemberCashCouponRepository:IRepositoryExt<MemberCashCoupon, string>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="mobie"></param>
        /// <param name="state"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<PagingResult<MemberCashCouponRsp>> GetListAsnyc<TSession>(string mobie,int state,int pageIndex, int pageSize) where TSession : class, ISession;
       
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="mobie"></param>
        /// <returns></returns>
        Task<List<MemberCashCoupon>> GetListAsnyc<TSession>(string mobie) where TSession : class, ISession;

    }
}
