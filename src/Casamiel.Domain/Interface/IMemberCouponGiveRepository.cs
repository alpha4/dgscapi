﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Domain
{
	/// <summary>
	/// 
	/// </summary>
	public interface IMemberCouponGiveRepository : IRepositoryExt<MemberCouponGive, long>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="ForwardCode"></param>
		/// <returns></returns>
		Task<MemberCouponGive>FindAsync<TSession>(string ForwardCode)where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="MID"></param>
		/// <param name="Status">0发起转赠；1转赠成功，-1转赠超时</param>
		/// <param name="IsGreaterThan"></param>
		/// <param name="dt"></param>
		/// <returns></returns>
		Task<List<MemberCouponGive>> GetListAsync<TSession>(long MID, int Status, bool IsGreaterThan, DateTime dt) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="MID"></param>
		/// <param name="MCID"></param>
		/// <param name="Status">0发起转赠；1转赠成功，-1转赠超时</param>
		/// <param name="IsGreaterThan"></param>
		/// <param name="dt"></param>
		/// <returns></returns>
		Task<List<MemberCouponGive>> GetListAsync<TSession>(long MID, long MCID, int Status, bool IsGreaterThan, DateTime dt) where TSession : class, ISession;


		/// <summary>
		/// 
		/// </summary>
		/// <param name="MCID"></param>
		/// <param name="Status"></param>
		/// <param name="uow"></param>
		/// <returns></returns>
		Task UpdateAsync(long MCID, int Status, IUnitOfWork uow);
	}
}
