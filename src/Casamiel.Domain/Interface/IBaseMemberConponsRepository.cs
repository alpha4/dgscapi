﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Smooth.IoC.Repository.UnitOfWork;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Domain
{
	/// <summary>
	/// 
	/// </summary>
	public interface IBaseMemberConponsRepository : IRepositoryExt<BaseMemberCoupons, long>, IRepository<BaseMemberCoupons, long>
	{
 
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="ProductId"></param>
		/// <param name="State"></param>
		/// <param name="top"></param>
		/// <returns></returns>
		Task<List<BaseMemberCoupons>> GetListAsync<TSession>(int ProductId, int  State, int top) where TSession : class, ISession;


		/// <summary>
		/// 
		/// </summary>
		/// <param name="TicketCode"></param>
		/// <param name="NewState"></param>
		/// <param name="OldState"></param>
		/// <param name="uow"></param>
		/// <returns></returns>
		Task<bool> UpdateAsync(string TicketCode,int NewState,int OldState, IUnitOfWork uow);
	}
}
