﻿using Casamiel.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain
{
    /// <summary>
    /// Mobile checkcode repository.
    /// </summary>
	public interface IMobileCheckcodeRepository : IRepositoryExt<MobileCheckcode, string>
    {
    }
}
