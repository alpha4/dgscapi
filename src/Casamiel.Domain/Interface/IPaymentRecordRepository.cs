﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
	/// <summary>
	/// 
	/// </summary>
	public interface IPaymentRecordRepository:IRepositoryExt<PaymentRecord, string>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="BillNO"></param>
		/// <param name="OperationMethod"></param>
		/// <param name="State"></param>
		/// <returns></returns>
        Task<PaymentRecord> FindAsync<TSession>(string BillNO,int OperationMethod, int State) where TSession:class,ISession;
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="IcTradeNO"></param>
		/// <returns></returns>
		Task<PaymentRecord> FindAsync<TSession>(string IcTradeNO) where TSession : class, ISession;
       
        /// <summary>
        /// Gets the shop identifier and shop name by bill NOA sync.
        /// </summary>
        /// <returns>The shop identifier and shop name by bill NOA sync.</returns>
        /// <param name="billno">Billno.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<Tuple<int, string>> GetShopIdAndShopNameByBillNOAsync<TSession>(string billno) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="OutTradeNO">外部交易号</param>
		/// <returns></returns>
		Task<PaymentRecord> FindByOutTradeNOAsync<TSession>(string OutTradeNO) where TSession : class, ISession;
		 
		}
}
