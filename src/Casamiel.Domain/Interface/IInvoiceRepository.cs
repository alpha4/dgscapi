﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Smooth.IoC.Repository.UnitOfWork;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Domain
{
    /// <summary>
    /// Invoice repository.
    /// </summary>
    public interface IInvoiceRepository : IRepositoryExt<BaseInvoice, long>, IRepository<BaseInvoice, long>
    {
         
        /// <summary>
        /// Gets the list asnyc.
        /// </summary>
        /// <returns>The list asnyc.</returns>
        /// <param name="Mobile">Mobile.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<PagingResult<BaseInvoice>> GetListAsnyc<TSession>(string Mobile, int pageIndex, int pageSize) where TSession : class, ISession;
    }
 
}
