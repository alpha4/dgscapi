﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Casamiel.Domain.Response;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Domain
{
	/// <summary>
	/// 
	/// </summary>
	public interface IMemberCouponRepository:IRepositoryExt<MemberCoupon, long>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="tickectCode"></param>
		/// <returns></returns>

		Task<dynamic> GetListAsync<TSession>(string tickectCode) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="MId"></param>
		/// <param name="state">0、已使用；1、已过期；2、未启用；3、可使用</param>
		/// <param name="pageIndex"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>

		Task<PagingResultRsp<List<MemberCoupon>>> GetListAsnyc<TSession>(long MId, int state, int pageIndex, int pageSize) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="MID"></param>
		/// <param name="state"></param>
		/// <param name="UsedDate"></param>
		/// <returns></returns>
		Task<List<MemberCoupon>> GetListAsnyc<TSession>(long MID, int state, DateTime UsedDate) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="OrderCode"></param>
		/// <returns></returns>
		Task<List<MemberCoupon>> GetListAsnyc<TSession>(string OrderCode) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="OldTicketcode"></param>
		/// <returns></returns>
		Task<MemberCoupon> FindAsync<TSession>(string OldTicketcode) where TSession : class, ISession;
		/// <summary>
		/// 
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="GiveStatus">0可赠送；1获赠；-1赠送中；-2已赠送</param>
		/// <param name="uow"></param>
		/// <returns></returns>
		Task<bool> UpdateCouponGiveStatusAsnync(MemberCoupon entity,int GiveStatus, IUnitOfWork uow);
	}
}
