﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
	/// <summary>
	/// 
	/// </summary>
	public interface IThirdPartyPlatformConfigRepository : IRepositoryExt<ThirdPartyPlatformConfig, int>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="eleStoreId"></param>
		/// <returns></returns>
		Task<ThirdPartyPlatformConfig> FindByEleStoreIdAsync<TSession>(long eleStoreId) where TSession : class, ISession;

	}
}
