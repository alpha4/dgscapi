﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// Member access token repository.
    /// </summary>
	public interface IMemberAccessTokenRepository : IRepositoryExt<MemberAccessToken, int>
    {
        /// <summary>
        /// Finds the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <param name="loginType">Login type.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
		Task<MemberAccessToken> FindAsync<TSession>(string mobile, int loginType) where TSession : class, ISession;
	}
}
