﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// Error log repository.
    /// </summary>
	public interface IErrorLogRepository : IRepositoryExt<ErrorLog,int>
    {

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <returns></returns>
		Task<String> GetHappyLandAppUrlAsnyc<TSession>() where TSession : class, ISession;
		/// <summary>
		/// Gets the name of the store by.
		/// </summary>
		/// <returns>The store by name.</returns>
		/// <param name="pageIndex">Page index.</param>
		/// <param name="pageSize">Page size.</param>
		/// <typeparam name="TSession">The 1st type parameter.</typeparam>
		Task<PagingResult<ErrorLog>> GetListAsnyc<TSession>(int pageIndex, int pageSize) where TSession : class, ISession;



		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <returns></returns>
		Task<String> GetAppDownlaodUrlAsnyc<TSession>(int apptype) where TSession : class, ISession;
         
    }
}
