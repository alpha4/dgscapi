﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// 
    /// </summary>
    public   interface IHlEmployeeRepository : IRepositoryExt<HlEmployee, int>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="Mobile"></param>
        /// <returns></returns>
        Task<HlEmployee> FindAsync<TSession>(string Mobile) where TSession : class, ISession;
    }
}
