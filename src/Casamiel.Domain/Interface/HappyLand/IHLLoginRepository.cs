﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Domain
{
	/// <summary>
	/// 
	/// </summary>
	public interface IHLLoginRepository:IRepositoryExt<HLLogin, int>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="Mobile"></param>
		/// <returns></returns>
		Task<HLLogin> FindAsync<TSession>(string Mobile) where TSession : class, ISession;
	}
}
