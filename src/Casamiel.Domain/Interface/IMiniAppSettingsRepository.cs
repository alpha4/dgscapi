﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// Mini app settings repository.
    /// </summary>
	public interface IMiniAppSettingsRepository:IRepositoryExt<MiniAppSettings,string>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="appId"></param>
        /// <returns></returns>
		Task<MiniAppSettings> FindAsync<TSession>(string appId) where TSession : class, ISession;
	}
}
