﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// 提货单
    /// </summary>
	public interface IOrderTakeRepository:IRepositoryExt<Order_TakeEntity, int>
	{
        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <returns>The status.</returns>
        /// <param name="OrderBaseId">Order base identifier.</param>
        /// <param name="status">Status.</param>
        /// <param name="billno">Billno.</param>
        /// <param name="uow">Uow.</param>
        Task<bool> UpdateStatus(int OrderBaseId, int status, string billno,IUnitOfWork uow);
        /// <summary>
        /// Gets the by order code async.
        /// </summary>
        /// <returns>The by order code async.</returns>
        /// <param name="OrderBaseId">Order code.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<Order_TakeEntity> GetByOrderBaseIdAsync<TSession>(int OrderBaseId) where TSession : class, ISession;
	}
}
