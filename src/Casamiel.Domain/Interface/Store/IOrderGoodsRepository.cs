﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// Order goods repository.
    /// </summary>
	public   interface IOrderGoodsRepository : IRepositoryExt<Order_GoodsEntity, int>
	{
        /// <summary>
        /// Gets the by store identifier async.
        /// </summary>
        /// <returns>The by store identifier async.</returns>
        /// <param name="OrderBaseId">Order base identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<Order_GoodsEntity>> GetByOrderBaseIdAsync<TSession>(int OrderBaseId) where TSession : class, ISession;

    }
}
