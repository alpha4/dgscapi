﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// Order discount coupon repository.
    /// </summary>
	public interface IOrderDiscountCouponRepository : IRepositoryExt<Order_DiscountCouponEntity, int>
	{
        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <returns>The status.</returns>
        /// <param name="OrderBaseId">Order base identifier.</param>
        /// <param name="Status">Status.</param>
        /// <param name="uow">Uow.</param>
        Task<bool> UpdateStatus(int OrderBaseId, int Status, IUnitOfWork uow);

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="OrderBaseId"></param>
        /// <returns></returns>
        Task<Order_DiscountCouponEntity> GetByOrderBaseIdAsync<TSession>(int OrderBaseId) where TSession : class, ISession;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="OrderBaseId"></param>
        /// <returns></returns>
        Task<List<Order_DiscountCouponEntity>> GetListByOrderBaseIdAsync<TSession>(int OrderBaseId) where TSession : class, ISession;
    }
}
