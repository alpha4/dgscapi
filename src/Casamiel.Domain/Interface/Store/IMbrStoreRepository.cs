﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// Mbr store repository.
    /// </summary>
	public   interface IMbrStoreRepository:IRepositoryExt<StoreEntity, int>
	{
        /// <summary>
        /// Ges the rect range async.
        /// </summary>
        /// <returns>The rect range async.</returns>
        /// <param name="minLat">Minimum lat.</param>
        /// <param name="minLng">Minimum lng.</param>
        /// <param name="maxLat">Max lat.</param>
        /// <param name="maxLng">Max lng.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
		Task<List<StoreEntity>> GeRectRangeAsync<TSession>(double minLat, double minLng, double maxLat, double maxLng) where TSession : class,ISession;
		/// <summary>
        /// Gets the name of the store by.
        /// </summary>
        /// <returns>The store by name.</returns>
        /// <param name="StoreName">Store name.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
		Task<PagingResult<StoreEntity>> GetStoreByName<TSession>(string StoreName,int pageIndex,int pageSize) where TSession : class, ISession;


        /// <summary>
        /// Gets the name of the take out store by.
        /// </summary>
        /// <returns>The take out store by name.</returns>
        /// <param name="StoreName">Store name.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<PagingResult<StoreEntity>>  GetTakeOutStoreByNameAsync<TSession>(string StoreName, int pageIndex, int pageSize) where TSession : class, ISession;
        /// <summary>
        /// 门店信息
        /// </summary>
        /// <returns>The by store identifier async.</returns>
        /// <param name="StoreId">Store identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<StoreEntity>GetByStoreIdAsync<TSession>(int StoreId)where TSession : class, ISession;

        /// <summary>
        /// Gets the store by name async.
        /// </summary>
        /// <returns>The store by name async.</returns>
        /// <param name="StoreName">Store name.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<StoreEntity>>GetStoreByNameAsync<TSession>(string StoreName) where TSession : class, ISession;
        /// <summary>
        /// Gets the by relation identifier async.
        /// </summary>
        /// <returns>The by relation identifier async.</returns>
        /// <param name="RelationId">Relation identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<StoreEntity> GetByRelationIdAsync<TSession>(int RelationId)where TSession : class, ISession;
        /// <summary>
        /// Gets the by ELES tore identifier async.
        /// </summary>
        /// <returns>The by ELES tore identifier async.</returns>
        /// <param name="ELEStoreId">ELES tore identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<StoreEntity> GetByELEStoreIdAsync<TSession>(string ELEStoreId) where TSession : class, ISession;
        /// <summary>
        ///所有门店检索
        /// </summary>
        /// <returns>The all store by name async.</returns>
        /// <param name="StoreName">Store name.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<StoreEntity>> GetAllStoreByNameAsync<TSession>(string StoreName) where TSession : class, ISession;

        /// <summary>
        /// Gets the list async.
        /// </summary>
        /// <returns>The list async.</returns>
        /// <param name="StoreName">Store name.</param>
        /// <param name="IsShow">Is show.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<StoreEntity>> GetListAsync<TSession>(string StoreName, int IsShow) where TSession : class, ISession;
    }
}
