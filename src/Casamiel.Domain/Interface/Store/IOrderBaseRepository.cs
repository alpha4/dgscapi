﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// Order base repository.
    /// </summary>
	public interface IOrderBaseRepository:IRepositoryExt<OrderBaseEntity, int>
	{

        //Task<List<Order_BaseEntity>> GetList<TSession>() where TSession : class, ISession;
        /// <summary>
        /// Gets the by order code async.
        /// </summary>
        /// <returns>The by order code async.</returns>
        /// <param name="orderCode">Order code.</param>
        /// <param name="mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<OrderBaseEntity> GetByOrderCodeAsync<TSession>(string orderCode,string mobile) where TSession : class, ISession;
        /// <summary>
        /// Gets the by order code async.
        /// </summary>
        /// <returns>The by order code async.</returns>
        /// <param name="orderCode">Order code.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<OrderBaseEntity> GetByOrderCodeAsync<TSession>(string orderCode) where TSession : class, ISession;
        /// <summary>
        /// Gets the list async.
        /// </summary>
        /// <returns>The list async.</returns>
        /// <param name="n">N.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<OrderBaseEntity>> GetListAsync<TSession>(int n) where TSession : class, ISession;
        /// <summary>
        /// Gets the payed order list async.
        /// </summary>
        /// <returns>The payed order list async.</returns>
        /// <param name="source">Source.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<OrderBaseEntity>> GetPayedOrderListAsync<TSession>(int source) where TSession : class, ISession;

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="IcTradeNO"></param>
        /// <returns></returns>
        Task<OrderBaseEntity> GetByIcTradeNOAsync<TSession>(string IcTradeNO) where TSession : class, ISession;
        /// <summary>
        /// Updates the status and res bill NOA sync.
        /// </summary>
        /// <returns>The status and res bill NOA sync.</returns>
        /// <param name="OrderBaseId">Order base identifier.</param>
        /// <param name="OrderStatus">Order status.</param>
        /// <param name="Resbillno">Resbillno.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task UpdateOrderStatusAndResBillNOAsync<TSession>(int OrderBaseId, int OrderStatus, string Resbillno) where TSession : class, ISession;

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="OrderCode"></param>
        /// <param name="InvoiceUrl"></param>
        /// <returns></returns>
        Task UpdateOrderInvoiceUrlByOrderCodeAsync<TSession>(string OrderCode, string InvoiceUrl) where TSession : class, ISession;
        /// <summary>
        /// Goodses the quantity by pid async.
        /// </summary>
        /// <returns>The quantity by pid async.</returns>
        /// <param name="pid">Pid.</param>
        /// <param name="dt">Dt.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<int> GetGoodsQuantityByPidAsync<TSession>(int pid, DateTime dt) where TSession : class, ISession;

        /// <summary>
        /// Updates the dada status async.
        /// </summary>
        /// <returns>The dada status async.</returns>
        /// <param name="OrderBaseId">Order base identifier.</param>
        /// <param name="DataStatus">Data status.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task UpdateDadaStatusAsync<TSession>(int OrderBaseId, int DataStatus) where TSession : class, ISession;

        /// <summary>
        /// Updates the order status and res bill NOA sync.
        /// </summary>
        /// <returns>The order status and res bill NOA sync.</returns>
        /// <param name="entity">Entity.</param>
        /// <param name="NewOrderStatus">New order status.</param>
        /// <param name="Resbillno">Resbillno.</param>
        /// <param name="uow">Uow.</param>
        Task<bool> UpdateOrderStatusAndResBillNOAsync(OrderBaseEntity entity, int NewOrderStatus, string Resbillno, IUnitOfWork uow);

        /// <summary>
        /// Deletes the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="OrderCode">Order code.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task DeleteAsync<TSession>(string OrderCode) where TSession : class, ISession;

          
        }
    }
