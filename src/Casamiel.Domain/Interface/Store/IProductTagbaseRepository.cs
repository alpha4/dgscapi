﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Entity;
using System.Threading.Tasks;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Domain
{
    /// <summary>
    /// Product tagbase repository.
    /// </summary>
	public  interface IProductTagbaseRepository : IRepositoryExt<Product_tagbaseEntity, int>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="status"></param>
		/// <param name="orderby"></param>
		/// <returns></returns>
		 Task<List<Product_tagbaseEntity>> GetListByStateAsync<TSession>(int status, string orderby) where TSession : class, ISession;
    }
}
