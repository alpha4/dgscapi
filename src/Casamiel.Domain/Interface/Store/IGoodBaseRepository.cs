﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Request;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Domain
{
    /// <summary>
    /// Good base repository.
    /// </summary>
    public interface IGoodBaseRepository
    {
        /// <summary>
        /// Gets the list async.
        /// </summary>
        /// <returns>The list async.</returns>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<GoodBaseEntity>> GetListAsync<TSession>() where TSession : class, ISession;
        /// <summary>
        /// Gets the dishcode.
        /// </summary>
        /// <returns>The dishcode.</returns>
        /// <param name="RelationId">Relation identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<string> GetDishcode<TSession>(int RelationId) where TSession : class, ISession;
        /// <summary>
        /// Gets the detail by releation identifier async.
        /// </summary>
        /// <returns>The detail by releation identifier async.</returns>
        /// <param name="RelationId">Relation identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<GoodBaseEntity> GetDetailByReleationIdAsync<TSession>(int RelationId) where TSession : class, ISession;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="RelationId"></param>
        /// <returns></returns>
        Task<GoodBaseEntity> GetDetailV2ByReleationIdAsync<TSession>(int RelationId) where TSession : class, ISession;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="GoodsBaseId"></param>
        /// <returns></returns>
        Task<GoodBaseEntity> GetDetailV2ByGoodsBaseIdAsync<TSession>(int GoodsBaseId) where TSession : class, ISession;
        /// <summary>
        /// Updates the stock.
        /// </summary>
        /// <returns>The stock.</returns>
        /// <param name="storeid">Storeid.</param>
        /// <param name="pid">Pid.</param>
        /// <param name="stock">Stock.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task UpdateStock<TSession>(int storeid, int pid, int stock) where TSession : class, ISession;
        /// <summary>
        /// Updates the stock v2.
        /// </summary>
        /// <returns>The stock v2.</returns>
        /// <param name="storeid">Storeid.</param>
        /// <param name="pid">Pid.</param>
        /// <param name="stock">Stock.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task UpdateStockV2<TSession>(int storeid, int pid, int stock) where TSession : class, ISession;
        /// <summary>
        /// Gets the product identifier s async.
        /// </summary>
        /// <returns>The product identifier s async.</returns>
        /// <param name="StoreId">Store identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<string>> GetProductIDsAsync<TSession>(int StoreId) where TSession : class, ISession;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="StoreId"></param>
        /// <returns></returns>
        Task<List<string>> GetProductIDsV2Async<TSession>(int StoreId) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="StoreId"></param>
		/// <returns></returns>
		Task<IEnumerable<dynamic>> GetProductSkuList<TSession>(int StoreId) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="list"></param>
		/// <returns></returns>
		Task UpdateStockquentityAsync<TSession>(List<StoreGoodsStock> list) where TSession : class, ISession;

	}
}
