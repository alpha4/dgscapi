﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// 提货单明细
    /// </summary>
	public interface IOrderGoodsTakeRepository:IRepositoryExt<Order_GoodsTakeEntity, int>
	{
		
	}
}
