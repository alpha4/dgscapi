﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// Mbr employee repository.
    /// </summary>
	public interface IMbrEmployeeRepository : IRepositoryExt<Mbr_employeeEntity, int>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="mobile"></param>
		/// <returns></returns>
		Task<Mbr_employeeEntity> FindAsync<TSession>(string  mobile) where TSession : class, ISession;
	}
}
