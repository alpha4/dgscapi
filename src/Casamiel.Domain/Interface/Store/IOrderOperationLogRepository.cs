﻿using System;
using Casamiel.Domain.Entity;

namespace Casamiel.Domain
{
	/// <summary>
	/// 
	/// </summary>
	public interface IOrderOperationLogRepository : IRepositoryExt<OrderOperationLog, int>
	{

	}
}
