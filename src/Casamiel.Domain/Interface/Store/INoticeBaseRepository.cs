﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Domain
{
    /// <summary>
    /// Notice base repository.
    /// </summary>
    public interface INoticeBaseRepository : IRepositoryExt<NoticeBaseEntity, int>
    {
        /// <summary>
        /// Updates the by relation identifier async.
        /// </summary>
        /// <returns>The by relation identifier async.</returns>
        /// <param name="RelationId">Relation identifier.</param>
        /// <param name="Status">Status.</param>
        /// <param name="uow">Uow.</param>
        Task UpdateByRelationIdAsync(int RelationId, int Status, IUnitOfWork uow);
    }
}