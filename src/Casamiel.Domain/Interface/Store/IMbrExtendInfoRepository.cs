﻿using System;
using Casamiel.Domain.Entity;

namespace Casamiel.Domain
{
    /// <summary>
    /// Mbr extend info repository.
    /// </summary>
    public interface IMbrExtendInfoRepository : IRepositoryExt<MbrExtendInfoEntity, int>
    {
    }
}
