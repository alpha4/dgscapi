﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Domain
{
	/// <summary>
	/// 
	/// </summary>
	public interface IMemberTicketGiveRepository : IRepositoryExt<MemberTicketGive, long>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="v"></param>
		/// <returns></returns>
	    Task<MemberTicketGive> FindAsync<TSession>(string v) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="MTGID"></param>
		/// <param name="Status"></param>
		/// <param name="uow"></param>
		/// <returns></returns>
        Task UpdateAsync(long MTGID, int Status, IUnitOfWork uow);

	}
}
