﻿using Casamiel.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain
{
    /// <summary>
    /// Sa tester repository.
    /// </summary>
	public interface ISaTesterRepository : IRepositoryExt<SaTester, string>
	{
    }
}
