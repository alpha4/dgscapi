﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Smooth.IoC.Repository.UnitOfWork;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Domain
{
	/// <summary>
	/// 
	/// </summary>
	public interface IAlipayMiniOpenIdRepository : IRepositoryExt<AlipayMiniOpenId, long>, IRepository<AlipayMiniOpenId, long>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="AppId"></param>
		/// <param name="UserId"></param>
		/// <returns></returns>
		Task<AlipayMiniOpenId>FindAsync<TSession>(string AppId,long UserId) where TSession : class, ISession;
	}
}
