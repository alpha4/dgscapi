﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// 
    /// </summary>
    public  interface IShoppingCartRepository : IRepositoryExt<Mbr_shoppingCartEntity, int>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="storeId"></param>
        /// <param name="mobile"></param>
        /// <param name="goodsBaseId"></param>
        /// <returns></returns>
        Task<Mbr_shoppingCartEntity> FindAsync<TSession>(int storeId, string mobile,int goodsBaseId) where TSession : class, ISession;

    }
}
