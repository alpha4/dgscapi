﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// User log repository.
    /// </summary>
	public interface IUserLogRepository : IRepositoryExt<UserLog,int>
    {
        /// <summary>
        /// Gets the list by mobile asnyc.
        /// </summary>
        /// <returns>The list by mobile asnyc.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
		Task<IEnumerable<dynamic>> GetListByMobileAsnyc<TSession>(string mobile) where TSession : class, ISession;
	}
}
