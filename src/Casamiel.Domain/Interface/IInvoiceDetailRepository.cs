﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Smooth.IoC.Repository.UnitOfWork;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Domain
{
    /// <summary>
    /// Invoice detail repository.
    /// </summary>
    public interface IInvoiceDetailRepository : IRepositoryExt<InvoiceDetail, long>, IRepository<InvoiceDetail, long>
    {
        /// <summary>
        /// Gets for KBA sync.
        /// </summary>
        /// <returns>The for KBA sync.</returns>
        /// <param name="Ids">Identifiers.</param>
        /// <param name="Mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<InvoiceDetail>> GetListAsync<TSession>(string Ids, string Mobile) where TSession : class, ISession;

        /// <summary>
        /// Gets the list by invoice IDA sync.
        /// </summary>
        /// <returns>The list by invoice IDA sync.</returns>
        /// <param name="InvoiceID">Invoice identifier.</param>
        /// <param name="Mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<InvoiceDetail>> GetListAsync<TSession>(long InvoiceID, string Mobile) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="OutTradeNO"></param>
		/// <returns></returns>
		Task<InvoiceDetail>FindByOutTradeNOAsync<TSession>(string OutTradeNO) where TSession : class, ISession;

		/// <summary>
		/// Gets the list by mobile date time async.
		/// </summary>
		/// <returns>The list by mobile date time async.</returns>
		/// <param name="Mobile">Mobile.</param>
		/// <param name="dt">Dt.</param>
		/// <param name="pageIndex">Page index.</param>
		/// <param name="pageSize">Page size.</param>
		/// <typeparam name="TSession">The 1st type parameter.</typeparam>
		Task<PagingResult<InvoiceDetail>> GetListAsync<TSession>(string Mobile, DateTime dt, int pageIndex, int pageSize) where TSession : class, ISession;
       
        /// <summary>
        /// Updates the by list async.
        /// </summary>
        /// <returns>The by list async.</returns>
        /// <param name="list">List.</param>
        /// <param name="uow">Uow.</param>
        Task UpdateByListAsync(List<InvoiceDetail> list, IUnitOfWork uow);
    }

}
