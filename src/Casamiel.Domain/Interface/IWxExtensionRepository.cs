﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Domain
{
    /// <summary>
    /// Wx extension repository.
    /// </summary>
    public interface IWxExtensionRepository :IRepositoryExt<WxExtension, int>
    {
        /// <summary>
        /// Gets the by mobile async.
        /// </summary>
        /// <returns>The by mobile async.</returns>
        /// <param name="Mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<WxExtension> FindAsync<TSession>(string Mobile) where TSession : class, ISession;
    }
}
