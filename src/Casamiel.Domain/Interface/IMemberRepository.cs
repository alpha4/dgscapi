﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain
{
    /// <summary>
    /// Member repository.
    /// </summary>
	public interface IMemberRepository:IRepositoryExt<Member,long>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Mobile"></param>
        /// <returns></returns>
        Task<Member> FindAsync<TSession>(string Mobile) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="ID"></param>
		/// <param name="LastLoginDate"></param>
		/// <returns></returns>
		Task<bool> UpdateLastLoginDateAsync<TSession>(long ID, DateTime LastLoginDate) where TSession : class, ISession;

	}
}
