﻿using System;
namespace Casamiel.Domain.Response
{
	/// <summary>
	/// 
	/// </summary>
	public record MemberCouponReponse
	{

		/// <summary>
		/// 
		/// </summary>
		public long MCID { get; set; }

		/// <summary>
		/// 会员流水号
		/// </summary>
		public long MID { get; set; }

		/// <summary>
		/// 0可赠送；1获赠；-1赠送中；-2已赠送
		/// </summary>
		public int GiveStatus { get; set; }

		 
		/// <summary>
		/// 券名称
		/// </summary>
		public string Productname { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public long Productid { get; set; }

		/// <summary>
		///  金额
		/// </summary>
		public decimal Price { get; set; }

		/// <summary>
		/// 状态，-2、已销毁；-1、不可用； 0、已使用；1、已过期；2、未启用；3、可使用
		/// </summary>
		public int State { get; set; }

		 
		/// <summary>
		/// 
		/// </summary>
		public DateTime Startdate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public DateTime Enddate { get; set; }

		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime CreateTime { get; set; }

	}
}
