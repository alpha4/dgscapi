﻿using System;
namespace Casamiel.Domain.Response
{
    /// <summary>
    /// Cake address base rsp.
    /// </summary>
    public record  CakeAddressBaseRsp
    {
        /// <summary>
        /// 地址id
        /// </summary>
        public int ConsigneeId { get; set; }
        /// <summary>
        /// 是否默认
        /// </summary>
        public bool IsDefault { get; set; }
        /// <summary>
        /// 联系人姓名
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 手机 会员信息
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 电话 
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 详细地址
        /// </summary>
        public string FullAddress { get; set; }
        /// <summary>
        /// 省ID
        /// </summary>
        public int ProvinceId { get; set; }
        /// <summary>
        /// 市ID
        /// </summary>
        public int CityId { get; set; }
        /// <summary>
        /// 区ID
        /// </summary>
        public int DistrictId { get; set; }
        /// <summary>
        /// 邮编
        /// </summary>
        public string Postcode { get; set; }
        /// <summary>
        /// 纬度
        /// </summary>
        public double Latitude { get; set; }
        /// <summary>
        /// 经度
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// 是否可用 在配送范围内
        /// </summary>
        public bool IsEnabled { get; set; }
    }
}
