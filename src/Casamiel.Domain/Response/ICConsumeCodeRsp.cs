﻿using System;
namespace Casamiel.Domain.Response
{
    /// <summary>
    /// ICC onsume code rsp.
    /// </summary>
    public record ICConsumeCodeRsp
    {
        /// <summary>
        /// Gets or sets the card no.
        /// </summary>
        /// <value>The card no.</value>
        public string CardNO { get; set; }
        /// <summary>
        /// Gets or sets the consumecode.
        /// </summary>
        /// <value>The consumecode.</value>
        public string Consumecode { get; set; }
    }
}
