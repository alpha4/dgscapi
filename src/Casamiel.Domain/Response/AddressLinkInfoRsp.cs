﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response
{
    /// <summary>
    /// 
    /// </summary>
    public record AddressLinkInfoRsp
    {
        /// <summary>
        /// 联系人
        /// </summary>
        public string LinkName { get; set; }
        /// <summary>
        /// 联系方式
        /// </summary>
        public string LinkPhone { get; set; }
    }
}
