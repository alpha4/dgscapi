﻿using System;

namespace Casamiel.Domain.Response.HappinessTree
{
    /// <summary>
    /// User info.
    /// </summary>
    public class UserInfoResponse
    {
        /// <summary>
        /// 会员手机号
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// 玩家等级 幸福树等级
        /// </summary>
        public int Lever { get; set; }
        /// <summary>
        /// 原始等级 跟Lever对比判断使用
        /// </summary>
        public int OLever { get; set; }

        /// <summary>
        /// 状态  0 正常 1 黑名单
        /// </summary>
        /// <value>The status.</value>
        public int Status { get; set; }
    }
}
