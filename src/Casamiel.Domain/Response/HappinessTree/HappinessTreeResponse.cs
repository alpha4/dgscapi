﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Response.HappinessTree
{
    /// <summary>
    /// Happiness tree response.
    /// </summary>
    public class HappinessTreeResponse
    {
        /// <summary>
        /// id
        /// </summary>
        public int ContentId { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 图片列表
        /// </summary>
        public List<string> ImageList { get; set; }
        /// <summary>
        /// 点赞次数
        /// </summary>
        public int StarTimes { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 是否被赞 0 不是 1 是
        /// </summary>
        public int IsStar { get; set; }

        /// <summary>
        /// 是否是本人发布 
        /// </summary>
        public bool IsCreater { get; set; }
    }
}
