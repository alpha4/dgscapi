﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response.HappinessTree
{
    /// <summary>
    /// 
    /// </summary>
    public  class RewardResponse
    {
        /// <summary>
        /// 奖励id
        /// </summary>
        public int RewardId { get; set; }

        /// <summary>
        /// 优惠券id
        /// </summary>
        public string CouponId { get; set; }

        /// <summary>
        /// 优惠券类型 1 优惠券 2 代金券
        /// </summary>
        public int CouponType { get; set; }

        /// <summary>
        /// 优惠券名称
        /// </summary>
        public string CouponName { get; set; }

        /// <summary>
        /// 时间
        /// </summary>
        public string CreateTime { get; set; }
    }
}
