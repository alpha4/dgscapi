﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response.HappinessTree
{
    /// <summary>
    /// 
    /// </summary>
    public class AliyunSignResponse
    {
        /// <summary>
        /// StatusCode
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// AccessKeyId
        /// </summary>
        public string AccessKeyId { get; set; }

        /// <summary>
        /// AccessKeySecret
        /// </summary>
        public string AccessKeySecret { get; set; }

        /// <summary>
        /// SecurityToken
        /// </summary>
        public string SecurityToken { get; set; }
        /// <summary>
        /// Expiration
        /// </summary>
        public string Expiration { get; set; }
        /// <summary>
        /// ErrorCode
        /// </summary>
        public string ErrorCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string  ErrorMessage { get; set; }
    }
}
