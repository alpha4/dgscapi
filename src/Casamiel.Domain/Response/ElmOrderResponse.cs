﻿using System;
using Casamiel.Domain.Entity;

namespace Casamiel.Domain.Response
{
	/// <summary>
	/// 
	/// </summary>
	public class ElmOrderResponse
	{
		/// <summary>
		/// 
		/// </summary>
		public dynamic Error;

		/// <summary>
		/// 
		/// </summary>
		public ElmOrderView Result;
	}
}
