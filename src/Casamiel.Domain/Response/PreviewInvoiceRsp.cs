﻿using System;
namespace Casamiel.Domain.Response
{
    /// <summary>
    /// Preview invoice rsp.
    /// </summary>
    public class PreviewInvoiceRsp
    {
        /// <summary>
        /// 
        /// 开票总金额
        /// </summary>
        /// <value>The total amount.</value>
        public decimal TotalAmount { get; set; }
        /// <summary>
        /// 流水号集合
        /// </summary>
        /// <value>The identifiers.</value>
        public string Ids { get; set; }
    }
}
