﻿using System;
namespace Casamiel.Domain.Response
{

	
	/// <summary>
	/// 
	/// </summary>
	public record AccessTokenRsp
	{
		/// <summary>
		/// 
		/// </summary>
		public string Access_token { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public long Expires_in { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Token_type { get; set; }
	}
}
