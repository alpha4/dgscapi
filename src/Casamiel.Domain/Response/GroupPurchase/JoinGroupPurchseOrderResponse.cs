﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Response.GroupPurchase
{
	/// <summary>
	/// 参与拼团信息
	/// </summary>
	public record JoinGroupPurchseOrderResponse
	{
		/// <summary>
		/// 
		/// </summary>
		public int OrderBaseId { get; set; }
		 
		/// <summary>		/// 订单编号		/// </summary>        public string OrderCode { get; set; }
		 
		/// <summary>		/// 成团人数		/// </summary>        public int Numbers { get; set; }
		 
		/// <summary>		/// 创建时间		/// </summary>        public string CreateTime { get; set; }
		 
		/// <summary>		/// 拼团活动名称		/// </summary>        public string GrouponName { get; set; }
		 
		/// <summary>		/// 商品名称		/// </summary>        public string GoodsName { get; set; }
		 
		/// <summary>		/// 商品图片		/// </summary>        public string ImagePath { get; set; }

		 
		/// <summary>		/// 拼团价		/// </summary>        public decimal GrouponPrice { get; set; }

		 
		/// <summary>		/// 此订单手机号		/// </summary>        public string Mobile { get; set; }
		 
		/// <summary>		/// 购买数量		/// </summary>        public int Quantity { get; set; }
		 
		/// <summary>		/// 剩余时间 秒		/// </summary>        public long Expirtime { get; set; }
		 
		/// <summary>		/// 团长订单编号		/// </summary>        public string LeaderCode { get; set; }

		/// <summary>
		/// 库存数量
		/// </summary>
		public int Stocks { get; set; }

		/// <summary>
		/// 是否无限库存
		/// </summary>
		public int IsLimitlessStock { get; set; }
		 
		///<summary>		///限购件数		///</summary>        public int MaxBuyNums { get; set; }
		/// <summary>		/// 参与人数		/// </summary>        public List<Person> GrouponOrders { get; set; }

 		/// <summary>		/// 拼团id		/// </summary>        public int GrouponId { get; set; }
 
		/// <summary>		/// 订单状态 0 待付款 1 待成团 2 待发货 3 已发货 4 已完成 5 已关闭 支付超时  6 已关闭拼团失败 7 已退款 		/// </summary>        public int OrderStatus { get; set; }
	}
	/// <summary>
	/// 参团信息
	/// </summary>
	public class Person
	{		
 
		/// <summary>		/// 订单编号		/// </summary>        public string OrderCode { get; set; }
		/// <summary>		/// 微信头像		/// </summary>        public string HeadImgurl { get; set; }

		 
		/// <summary>		/// 是否团长 true 是 false 不是		/// </summary>        public bool IsLeader { get; set; }
	}
}
