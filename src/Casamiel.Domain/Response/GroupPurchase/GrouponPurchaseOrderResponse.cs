﻿using System;
namespace Casamiel.Domain.Response.GroupPurchase
{
	/// <summary>
	/// 
	/// </summary>
	public record GrouponPurchaseOrderResponse
	{
		/// <summary>		/// 订单id		/// </summary>        public int OrderBaseId { get; set; }
		 
		/// <summary>		/// 订单编号		/// </summary>        public string OrderCode { get; set; }
		 
		/// <summary>		/// 创建时间		/// </summary>        public string CreateTime { get; set; }
		 
		/// <summary>		/// 拼团活动名称		/// </summary>        public string GrouponName { get; set; }
		 
		/// <summary>		/// 商品名称		/// </summary>        public string GoodsName { get; set; }
		 
		/// <summary>		/// 商品图片		/// </summary>        public string ImagePath { get; set; }
		 
		/// <summary>		/// 单价		/// </summary>        public decimal Price { get; set; }
		 
		/// <summary>		/// 购买数量		/// </summary>        public int Quantity { get; set; }
		 
		/// <summary>		/// 剩余时间 秒		/// </summary>        public long Expirtime { get; set; }
 		/// <summary>		/// 团长订单编号		/// </summary>        public string LeaderCode { get; set; }
		 
		/// <summary>		/// 订单状态 0 待付款 1 待成团 2 待发货 3 已发货 4 已完成 5 已关闭 支付超时  6 已关闭拼团失败 7 已退款		/// </summary>        public int OrderStatus { get; set; }
	}
}
