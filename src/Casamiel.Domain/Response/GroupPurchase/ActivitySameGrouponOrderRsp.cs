﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Response.GroupPurchase
{
	/// <summary>
	/// 
	/// </summary>
	public record ActivitySameGrouponOrderRsp
	{
 
		/// <summary>		/// 订单编号		/// </summary>        public string OrderCode { get; set; }

		/// <summary>
		/// 手机号
		/// </summary>
		public string Mobile { get; set; }
		 
		/// <summary>		/// 购买商品明细		/// </summary>        public List<GroupOrderGoodsDetailRsp> OrderGoodsList { get; set; }
	}
}
