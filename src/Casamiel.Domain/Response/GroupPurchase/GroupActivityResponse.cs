﻿using System;
namespace Casamiel.Domain.Response.GroupPurchase
{
	/// <summary>
	///  团购活动
	/// </summary>
	public record GroupActivityResponse
	{
 
		/// <summary>		/// 活动id		/// </summary>        public int GrouponId { get; set; }
 
		///<summary>		///活动名称		///</summary>        public string ActiveName { get; set; }
		 
		///<summary>		///商品名称		///</summary>        public string GoodsName { get; set; }
		 
		///<summary>		///商品图片 列表页		///</summary>        public string ImagePath { get; set; }
		 
		///<summary>		///倒计时 剩余多少秒		///</summary>        public long LastSecond { get; set; }
		 
		///<summary>		///成团人数		///</summary>        public int Numbers { get; set; }

		///<summary>		///单独购买价		///</summary>        public decimal OriginalPrice { get; set; }


		///<summary>		///拼团价		///</summary>        public decimal GrouponPrice { get; set; }

		///<summary>		///库存数量		///</summary>        public int Stocks { get; set; }
		 
		///<summary>		/// 是否无线库存		///</summary>        public int IsLimitlessStock { get; set; }
		 
		///<summary>		///拼团状态 0 未开始 1 进行中 2 已结束		///</summary>        public int Status { get; set; }
		 
		/// <summary>		/// 是否app专享 true 是 false 不是		/// </summary>        public bool IsOnlyApp { get; set; }
		 
		///<summary>		///排序字段 正序		///</summary>        public int Sort { get; set; }

		/// <summary>
		/// 活动开始时间
		/// </summary>
		public string StartTime { get; set; }
		/// <summary>
		/// 活动结束时间
		/// </summary>
		public string EndTime { get; set; }

		/// <summary>
		/// 原价
		/// </summary>
		public decimal CostPrice { get; set; }
	}
}
