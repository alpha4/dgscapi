﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Response.GroupPurchase
{
	/// <summary>
	/// 
	/// </summary>
	public record GroupPurchaseOrder
	{
 
		/// <summary>		/// 订单编号		/// </summary>        public string OrderCode { get; set; }
		 
		/// <summary>		/// 微信头像		/// </summary>        public string HeadImgurl { get; set; }

		 
		/// <summary>		/// 是否团长 true 是 false 不是		/// </summary>        public bool IsLeader { get; set; }
	}
	/// <summary>
	/// 
	/// </summary>
	public record GroupPurchaseOrderDetailResponse
	{

 
		/// <summary>		/// 订单id		/// </summary>        public int OrderBaseId { get; set; }
		 
		/// <summary>		/// 订单编号		/// </summary>        public string OrderCode { get; set; }
 
		/// <summary>		/// 创建时间		/// </summary>        public string CreateTime { get; set; }
		/// <summary>		/// 拼团活动名称		/// </summary>        public string GrouponName { get; set; }

		/// <summary>		/// 商品名称		/// </summary>        public string GoodsName { get; set; }
 
		/// <summary>		/// 商品图片		/// </summary>        public string ImagePath { get; set; }
 
		/// <summary>		/// 单价		/// </summary>        public decimal Price { get; set; }
 
		/// <summary>		/// 购买数量		/// </summary>        public int Quantity { get; set; }
 
		/// <summary>		/// 剩余时间 秒		/// </summary>        public long Expirtime { get; set; }
  
		/// <summary>		/// 团长订单编号		/// </summary>        public string LeaderCode { get; set; }
 
		/// <summary>		/// 订单状态 0 待付款 1 待成团 2 待发货 3 已发货 4 已完成 5 已关闭 支付超时  6 已关闭拼团失败 7 已退款		/// </summary>        public int OrderStatus { get; set; }

 
		///<summary>		///实付金额		///</summary>        public decimal PayMoney { get; set; }

 
		///<summary>		///支付方式 :0 默认   1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付		///</summary>        public int PayMethod { get; set; }
		 
		/// <summary>		/// 成团人数		/// </summary>        public int Numbers { get; set; }

 
		/// <summary>		/// 参与人数		/// </summary>
		public List<GroupPurchaseOrder> GrouponOrders { get; set; }
		 
		 
		/// <summary>		/// 购买商品明细		/// </summary>        public List<GroupOrderGoodsDetailRsp> OrderGoodsList { get; set; }
		/// <summary>		/// 拼团id		/// </summary>        public int GrouponId { get; set; }


		///<summary>
		///单独购买价
		///</summary>
		public decimal OriginalPrice { get; set; }

		///<summary>
		///拼团价
		///</summary>
		public decimal GrouponPrice { get; set; }
 
		/// <summary>		/// 原价		/// </summary>        public decimal CostPrice { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	public record GroupOrderGoodsDetailRsp
	{


 
		/// <summary>		/// 商品名称		/// </summary>        public string GoodsName { get; set; }
		 
		/// <summary>		/// 图片		/// </summary>        public string ImagePath { get; set; }


		 
		/// <summary>		/// sku		/// </summary>        public string SkuCode { get; set; }

 
		/// <summary>		/// 数量		/// </summary>        public int Quantity { get; set; }
	}
}
