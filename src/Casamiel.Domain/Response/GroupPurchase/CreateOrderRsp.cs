﻿using System;
namespace Casamiel.Domain.Response.GroupPurchase
{
	/// <summary>
	/// 
	/// </summary>
	public record CreateOrderRsp
	{
		/// <summary>
		/// 订单id
		/// </summary>
		public int OrderBaseId { get; set; }
		/// <summary>
		/// 订单编号
		/// </summary>
		public string OrderCode { get; set; }
		/// <summary>
		/// 订单金额
		/// </summary>
		public decimal OrderMoney { get; set; }
		/// <summary>
		/// 订单支付金额
		/// </summary>
		public decimal PayMoney { get; set; }
	}
}
