﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response
{
    /// <summary>
    /// 
    /// </summary>
    public record TPPTMallOrderBaseRsp
    {
        /// <summary>
        /// 订单id
        /// </summary>
        public int OrderBaseId { get; set; }
        /// <summary>
        /// 订单编码
        /// </summary>
        public string OrderCode { get; set; }
        /// <summary>
        /// 订单状态
        /// </summary>
        public int OrderStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string OrderStatusName { get; set; }
         
        /// <summary>
        /// 删除状态
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 门店ID
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int StoreRelationId { get; set; }
        /// <summary>
        /// 提货人
        /// </summary>
        public string TakeName { get; set; }
        /// <summary>
        /// 提货手机
        /// </summary>
        public string TakeMobile { get; set; }

        /// <summary>
        /// 快递状态
        /// </summary>
        public int ExpressStatus { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 订单金额
        /// </summary>
        public decimal OrderMoney { get; set; }
        /// <summary>
        /// Gets or sets the pay money.
        /// </summary>
        /// <value>The pay money.</value>
        public decimal PayMoney { get; set; }
        /// <summary>
        /// Gets or sets the freight money.
        /// </summary>
        /// <value>The freight money.</value>
        public decimal FreightMoney { get; set; }
        /// <summary>
        /// 提货时间
        /// </summary>
        public DateTime? TakeTime { get; set; }
        /// <summary>
        /// Gets or sets the ic trade no.
        /// </summary>
        /// <value>The ic trade no.</value>
        public string IcTradeNo { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int OrderType { get; set; }
        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; set; }
        /// <summary>
        /// 来源 0默认 1 美团 2 饿了么 3 京东 4 滴滴
        /// </summary>
        public int Source { get; set; }
        /// <summary>
        /// 来源string
        /// </summary>
        public string SoureName { get; set; }
        
        /// <summary>
        /// 门店订单序号
        /// </summary>
        public long OrderNum { get; set; }
        /// <summary>
        /// 配送地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// Gets or sets the order goods list.
        /// </summary>
        /// <value>The order goods list.</value>
        #region 商品信息
        public List<OrderGoodsBaseRsp> OrderGoodsList { get; set; }

        #endregion
    }
    /// <summary>
    /// 
    /// </summary>
    public class OrderGoodsBaseRsp
    {
        /// <summary>
        /// 商品ID
        /// </summary>
        public int GoodsBaseId { get; set; }
        /// <summary>
        /// 商品价格
        /// </summary>
        public decimal GoodsPrice { get; set; }
        /// <summary>
        /// 商品名称
        /// </summary>
        public string GoodsTitle { get; set; }
        /// <summary>
        /// 一网名称
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 购买数量
        /// </summary>
        public int GoodsQuantity { get; set; }
        /// <summary>
        /// 商品属性
        /// </summary>
        public string GoodsPropery { get; set; }
    }
}
