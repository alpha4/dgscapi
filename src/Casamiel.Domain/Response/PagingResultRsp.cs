﻿using System;
namespace Casamiel.Domain.Response
{
    /// <summary>
    /// Paging results.
    /// </summary>
    public class PagingResultRsp<T>  
    {
         

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Application.Models.PagingResults`1"/> class.
        /// </summary>
        /// <param name="defaultValue">Default value.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="total">Total.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        public PagingResultRsp(T defaultValue, int pageIndex, int pageSize, int total, int code, string msg)
        {
            Code = code;
            Msg = msg;
            Content = defaultValue;
            PageSize = pageSize;
            PageIndex = pageIndex;
            Total = total;
        }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        public int Code { get; private set; }
        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>The content.</value>
        public T Content{ get;  set; }
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Msg { get; private set; }

        /// <summary>
        /// Gets the total.
        /// </summary>
        /// <value>The total.</value>
        public int Total { get; private set; }
        /// <summary>
        /// Gets the index of the page.
        /// </summary>
        /// <value>The index of the page.</value>
        public int PageIndex { get; private set; }
        /// <summary>
        /// Gets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; private set; }
    }
}
