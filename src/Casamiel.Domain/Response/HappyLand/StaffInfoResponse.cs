﻿using System;
namespace Casamiel.Domain.Response
{
	/// <summary>
	/// 
	/// </summary>
	public class StaffInfoResponse
	{
		/// <summary>
		/// 
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// 手机号
		/// </summary>		
		public string Mobile { get; set; }

		/// <summary>
		/// 卡号
		/// </summary>		
		public string CardNo { get; set; } 
	}
}
