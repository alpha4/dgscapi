﻿using System;
namespace Casamiel.Domain.Response.Pre
{
    /// <summary>
	/// 
	/// </summary>
	public record OrderInformationReponse
	{
        /// <summary>
        /// 前面订单数
        /// </summary>
        public int PreOrderCount { get; set; }

        /// <summary>
        /// 预计多少分钟
        /// </summary>
        public double ExpectTime { get; set; }
    }
}
