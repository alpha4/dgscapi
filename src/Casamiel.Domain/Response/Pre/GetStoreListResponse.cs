﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Domain.Response.Pre
{
    /// <summary>
    /// 
    /// </summary>
    public record GetStoreListResponse
    {
        /// <summary>
        /// 门店id
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
        /// 对接信息Id
        /// </summary>
        public int RelationId { get; set; }

        /// <summary>
        /// 门店名称
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 门店小名
        /// </summary>
        public string SubStoreName { get; set; }
        /// <summary>
        /// 营业时间（8：00--18：00）
        /// </summary>
        public string ShopHours { get; set; }

        /// <summary>
        /// 门店电话
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string FullAddress { get; set; }

        /// <summary>
        /// 经度
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// 纬度
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// 距离
        /// </summary>
        public decimal Distance { get; set; }

    }
}
