﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Response
{
    /// <summary>
    /// Third result.
    /// </summary>
    public class ThirdResult<T>
    {
        /// <summary>
        /// The data.
        /// </summary>
        public T Data { get; set; }
        /// <summary>
        /// The result no.
        /// </summary>
        public string ResultNo { get; set; }
        /// <summary>
        /// The result remark.
        /// </summary>
        public string ResultRemark { get; set; }
        /// <summary>
        /// Gets a value indicating whether this <see cref="T:Casamiel.Domain.Response.ThirdResult`1"/> is success.
        /// </summary>
        /// <value><c>true</c> if is success; otherwise, <c>false</c>.</value>
        public bool IsSuccess
        {
            get { return ResultNo == "00000000"; }
        }
    }
    /// <summary>
    /// Third pageing result.
    /// </summary>
    public class ThirdPageingResult<T> : ThirdResult<T>
    {
        /// <summary>
        /// The index of the page.
        /// </summary>
        public int PageIndex { get; set; }

       /// <summary>
       /// The size of the page.
       /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// The total.
        /// </summary>
        public int Total { get; set; }
    }


	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class BaseThirdPageingResult<T>
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="defaultValue"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="total"></param>
        /// <param name="code"></param>
        /// <param name="msg"></param>
		public BaseThirdPageingResult(List<T> defaultValue, int pageIndex, int pageSize, int total, int code, string msg)
		{
			Code = code;
			Msg = msg;
			Content = defaultValue;
			PageSize = pageSize;
			PageIndex = pageIndex;
			Total = total;
		}
		  
		/// <summary>
		/// Gets or sets the code.
		/// </summary>
		/// <value>The code.</value>
		public int Code { get; private set; }
		/// <summary>
		/// Gets or sets the content.
		/// </summary>
		/// <value>The content.</value>
		public List<T> Content { get; set; }
		/// <summary>
		/// Gets or sets the message.
		/// </summary>
		/// <value>The message.</value>
		public string Msg { get; private set; }

		/// <summary>
		/// Gets the total.
		/// </summary>
		/// <value>The total.</value>
		public int Total { get; private set; }
		/// <summary>
		/// Gets the index of the page.
		/// </summary>
		/// <value>The index of the page.</value>
		public int PageIndex { get; private set; }
		/// <summary>
		/// Gets the size of the page.
		/// </summary>
		/// <value>The size of the page.</value>
		public int PageSize { get; private set; }
	}
}
