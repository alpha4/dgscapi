﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Response
{
	/// <summary>
	/// 
	/// </summary>
	public record CakeOrderBaseRespone
	{
		/// <summary>
		/// 订单id
		/// </summary>
		public int OrderBaseId { get; set; }
		/// <summary>
		/// 订单编码
		/// </summary>
		public string OrderCode { get; set; }
		/// <summary>
		/// 订单状态
		/// </summary>
		public int OrderStatus { get; set; }
		/// <summary>
		/// 门店ID
		/// </summary>
		public int StoreId { get; set; }
		/// <summary>
		/// 门店名称
		/// </summary>
		public string StoreName { get; set; }
		/// <summary>
		/// 门店关联Id
		/// </summary>
		public int StoreRelationId { get; set; }
		/// <summary>
		/// 门店联系方式
		/// </summary>
		public string StorePhone { get; set; }
		/// <summary>
		/// 门店地址
		/// </summary>
		public string StoreAddress { get; set; }
		/// <summary>
		/// 联系人
		/// </summary>
		public string ContactName { get; set; }
		/// <summary>
		/// 联系电话
		/// </summary>
		public string ContactPhone { get; set; }
		/// <summary>
		/// 支付方式 :  1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
		/// </summary>
		public int PayMethod { get; set; }
		/// <summary>
		/// 备注
		/// </summary>
		public string Remark { get; set; }
		/// <summary>
		/// 订单应付金额
		/// </summary>
		public decimal OrderMoney { get; set; }
		/// <summary>
		/// 订单支付金额
		/// </summary>
		public decimal PayMoney { get; set; }
		/// <summary>
		/// 优惠金额
		/// </summary>
		public decimal DiscountMoney { get; set; }
		/// <summary>
		/// 运费金额
		/// </summary>
		public decimal FreightMoney { get; set; }
		/// <summary>
		/// 送达时间
		/// </summary>
		public string ConfrimReceiveTime { get; set; }
		/// <summary>
		/// 支付时间
		/// </summary>
		public string PayTime { get; set; }
		/// <summary>
		/// IC交易号
		/// </summary>
		public string IcTradeNo { get; set; }
		/// <summary>
		/// 订单类型
		/// </summary>
		public int OrderType { get; set; }
		/// <summary>
		/// 收获地址
		/// </summary>
		public string Address { get; set; }
		/// <summary>
		/// 提货码
		/// </summary>
		public string TakeCode { get; set; }
		/// <summary>
		/// 门店地址
		/// </summary>
		public string FullAddress { get; set; }
		/// <summary>
		/// 创建时间
		/// </summary>
		public string CreateTime { get; set; }
		/// <summary>
		/// 第三方 配送状态 
		/// 未配送=0 待接单＝1 待取货＝2 配送中＝3 已完成＝4 已取消＝5 已过期＝7 
		/// 指派单=8 妥投异常之物品返回中=9 妥投异常之物品返回完成=10 创建达达运单失败=1000 可参考文末的状态说明
		/// </summary>
		public string TPP_OrderStatus { get; set; } = "";
		/// <summary>
		/// 配送员姓名
		/// </summary>
		public string dm_name { get; set; } = "";
		/// <summary>
		/// 配送员电话
		/// </summary>
		public string dm_mobile { get; set; } = "";
		#region 商品信息
		/// <summary>
		/// Gets or sets the order goods list.
		/// </summary>
		/// <value>The order goods list.</value>
		public List<MWebOrderGoodsBaseRsp> OrderGoodsList { get; set; }
		#endregion


		/// <summary>
		/// 优惠券信息
		/// </summary>
		public List<OrderDisCountRsp> DiscountList { get; set; }

		/// <summary>
		/// 发票信息
		/// </summary>
		public InvoiceInfo InvoiceModel { get; set; }

		/// <summary>
		/// 减免金额
		/// </summary>
		public decimal Rebate { get; set; }


        /// <summary>
        /// 订单来源
        ///  0  蛋糕商城 1 美团 2 饿了么 3 京东到家 4 滴滴 5天猫 6 口碑 7 全国送商城 8 外卖 9 京东商城 10 抖音带货  11 预点单
        /// </summary>
        public int Source { get; set; }

        /// <summary>
        /// 预计多少分钟
        /// </summary>
        public double ExpectTime { get; set; }

        /// <summary>
        /// 前面订单数
        /// </summary>
        public int PreOrderCount { get; set; }
    }



	/// <summary>
	/// Cake order base rsp.
	/// </summary>
	public sealed class CakeOrderBaseRsp
    {
        /// <summary>
        /// 订单id
        /// </summary>
        public int OrderBaseId { get; set; }
        /// <summary>
        /// 订单编码
        /// </summary>
        public string OrderCode { get; set; }
        /// <summary>
        /// 订单状态
        /// </summary>
        public int OrderStatus { get; set; }
        /// <summary>
        /// 门店ID
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 门店名称
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 门店关联Id
        /// </summary>
        public int StoreRelationId { get; set; }
        /// <summary>
        /// 门店联系方式
        /// </summary>
        public string StorePhone { get; set; }
        /// <summary>
        /// 门店地址
        /// </summary>
        public string StoreAddress { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
        public string ContactName { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string ContactPhone { get; set; }
        /// <summary>
        /// 支付方式 :  1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
        /// </summary>
        public int PayMethod { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 订单应付金额
        /// </summary>
        public decimal OrderMoney { get; set; }
        /// <summary>
        /// 订单支付金额
        /// </summary>
        public decimal PayMoney { get; set; }
        /// <summary>
        /// 优惠金额
        /// </summary>
        public decimal DiscountMoney { get; set; }
        /// <summary>
        /// 运费金额
        /// </summary>
        public decimal FreightMoney { get; set; }
        /// <summary>
        /// 送达时间
        /// </summary>
        public string ConfrimReceiveTime { get; set; }
        /// <summary>
        /// 支付时间
        /// </summary>
        public string PayTime { get; set; }
        /// <summary>
        /// IC交易号
        /// </summary>
        public string IcTradeNo { get; set; }
        /// <summary>
        /// 订单类型
        /// </summary>
        public int OrderType { get; set; }
        /// <summary>
        /// 收获地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
		/// 订单来源
		///  0  蛋糕商城 1 美团 2 饿了么 3 京东到家 4 滴滴 5天猫 6 口碑 7 全国送商城 8 外卖 9 京东商城 10 抖音带货  11 预点单
		/// </summary>
        public int Source { get; set; }
        /// <summary>
        /// 提货码
        /// </summary>
        public string TakeCode { get; set; }
        /// <summary>
        /// 门店地址
        /// </summary>
        public string FullAddress { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 第三方 配送状态 
        /// 未配送=0 待接单＝1 待取货＝2 配送中＝3 已完成＝4 已取消＝5 已过期＝7 
        /// 指派单=8 妥投异常之物品返回中=9 妥投异常之物品返回完成=10 创建达达运单失败=1000 可参考文末的状态说明
        /// </summary>
        public string TPP_OrderStatus { get; set; } = "";
        /// <summary>
        /// 配送员姓名
        /// </summary>
        public string dm_name { get; set; } = "";
        /// <summary>
        /// 配送员电话
        /// </summary>
        public string dm_mobile { get; set; } = "";
        #region 商品信息
        /// <summary>
        /// Gets or sets the order goods list.
        /// </summary>
        /// <value>The order goods list.</value>
        public List<MWebOrderGoodsBaseRsp> OrderGoodsList { get; set; }
 
        #endregion

        /// <summary>        /// 优惠券信息        /// </summary>        public List<OrderDisCountRsp> DiscountList { get; set; }

        /// <summary>
        /// 预计多少分钟
        /// </summary>
        public double ExpectTime { get; set; }
        /// <summary>
        /// 发票信息
        /// </summary>
        public InvoiceInfo InvoiceModel { get; set; }

		/// <summary>
		/// 减免金额
		/// </summary>
		public decimal Rebate { get; set; }
	}

    /// <summary>
    /// 
    /// </summary>
    public class InvoiceInfo
    {
        /// <summary>
        /// 发票类型 0 公司抬头 1 个人
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 发票抬头
        /// </summary>
        public string InvoiceTitle { get; set; }

        /// <summary>
        /// 发票税号
        /// </summary>
        public string TaxpayerId { get; set; }

        /// <summary>
        /// 地址 电话
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 开户行 账号
        /// </summary>
        public string OpeningBank { get; set; }

        /// <summary>
        /// 接受邮箱
        /// </summary>
        public string EMail { get; set; }
        /// <summary>
        /// 接受发票手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 0 未申请 1 已申请 2 已开发票 
        /// </summary>
        public int IsOpenInvoice { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public sealed class Cake_OrderBaseV2Rsp
    {
        /// <summary>
        /// 订单id
        /// </summary>
        public int OrderBaseId { get; set; }
        /// <summary>
        /// 订单编码
        /// </summary>
        public string OrderCode { get; set; }
        /// <summary>
        /// 订单状态
        /// </summary>
        public int OrderStatus { get; set; }
        /// <summary>
        /// 门店ID
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 门店名称
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 门店关联Id
        /// </summary>
        public int StoreRelationId { get; set; }
        /// <summary>
        /// 门店联系方式
        /// </summary>
        public string StorePhone { get; set; }
        /// <summary>
        /// 门店地址
        /// </summary>
        public string StoreAddress { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
        public string ContactName { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string ContactPhone { get; set; }
        /// <summary>
        /// 支付方式 :  1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
        /// </summary>
        public int PayMethod { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 订单应付金额
        /// </summary>
        public decimal OrderMoney { get; set; }
        /// <summary>
        /// 订单支付金额
        /// </summary>
        public decimal PayMoney { get; set; }
        /// <summary>
        /// 优惠金额
        /// </summary>
        public decimal DiscountMoney { get; set; }
        /// <summary>
        /// 运费金额
        /// </summary>
        public decimal FreightMoney { get; set; }
        /// <summary>
        /// 送达时间
        /// </summary>
        public string ConfrimReceiveTime { get; set; }
        /// <summary>
        /// 支付时间
        /// </summary>
        public string PayTime { get; set; }
        /// <summary>
        /// IC交易号
        /// </summary>
        public string IcTradeNo { get; set; }
        /// <summary>
        /// 订单类型
        /// </summary>
        public int OrderType { get; set; }
        /// <summary>
        /// 收获地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 提货码
        /// </summary>
        public string TakeCode { get; set; }
        /// <summary>
        /// 门店地址
        /// </summary>
        public string FullAddress { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 第三方 配送状态 
        /// 未配送=0 待接单＝1 待取货＝2 配送中＝3 已完成＝4 已取消＝5 已过期＝7 
        /// 指派单=8 妥投异常之物品返回中=9 妥投异常之物品返回完成=10 创建达达运单失败=1000 可参考文末的状态说明
        /// </summary>
        public string TPP_OrderStatus { get; set; } = "";
        /// <summary>
        /// 配送员姓名
        /// </summary>
        public string dm_name { get; set; } = "";
        /// <summary>
        /// 配送员电话
        /// </summary>
        public string dm_mobile { get; set; } = "";
        #region 商品信息
        /// <summary>
        /// Gets or sets the order goods list.
        /// </summary>
        /// <value>The order goods list.</value>
        public List<MWebOrderGoodsBaseRsp> OrderGoodsList { get; set; }
        #endregion
        /// <summary>
        /// 优惠券信息
        /// </summary>
        public List<OrderDisCountRsp> DiscountList { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public record OrderDisCountRsp
    {
        /// <summary>
        /// 状态
        /// </summary>
        public int DiscountCouponStatus { get; set; }
        /// <summary>
        /// id
        /// </summary>
        public long DiscountCouponId { get; set; }
        /// <summary>
        /// 卡号
        /// </summary>
        public string CardNo { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        public decimal DiscountCouponActualMoney { get; set; }
        /// <summary>
        /// 优惠券名称
        /// </summary>
        public string DiscountCouponName { get; set; }
        /// <summary>
        /// 优惠券类型 1 优惠券 2 代金券
        /// </summary>
        public int DiscountCouponType { get; set; }
    }
}
