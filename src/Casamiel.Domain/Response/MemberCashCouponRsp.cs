﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class MemberCashCouponRsp
    {

        /// <summary>
        /// 
        /// </summary>
        public int ID{ get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TicketCode { get; set; }
        /// <summary>
        /// 代金券名称
        /// </summary>
        public string CouponName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 0、已使用；-1、不可用；-2、已销毁；1、已过期；2、未启用；3、可使用；4、使用中
        /// </summary>
        public int State { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public string StartDate { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public string EndDate { get; set; }
    }
}
