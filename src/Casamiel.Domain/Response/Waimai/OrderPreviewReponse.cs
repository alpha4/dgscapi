﻿using System;
namespace Casamiel.Domain.Response.Waimai
{
    /// <summary>
    /// 外卖下单预览输出
    /// </summary>
    public record OrderPreviewReponse
    {
        /// <summary>
        /// 运费金额
        /// </summary>
        public decimal FreightMoney { get; set; }

        /// <summary>
        /// 减免金额
        /// </summary>
        public decimal Discount { get; set; }


		 /// <summary>
		 /// 订单金额
		 /// </summary>
		public decimal OrderAmount { get; set; }

		 
		/// <summary>
		/// 实付金额
		/// </summary>
		public decimal PayAmount { get; set; }
	}
}
