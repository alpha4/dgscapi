﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public class TakeOutGetFreightMoneyRsp
    {
        /// <summary>
        /// 运费金额
        /// </summary>
        public decimal FreightMoney { get; set; }
    }
}
