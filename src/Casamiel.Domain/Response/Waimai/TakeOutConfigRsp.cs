﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public record TakeOutConfigRsp
    {
        /// <summary>
        /// 起送金额
        /// </summary>
        public decimal StartMoney { get; set; }
        /// <summary>
        /// 免运费金额
        /// </summary>
        public decimal FreeMoney { get; set; }
        /// <summary>
        /// 文字描述
        /// </summary>
        public string FreightTips { get; set; }
        /// <summary>
        /// 运费规则
        /// </summary>
        public List<FreightRule> FreightRuleList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public record FreightRule
        {
            /// <summary>
            /// 区间金额最小值
            /// </summary>
            public decimal MinMoney { get; set; }
            /// <summary>
            /// 区间金额最大值
            /// </summary>
            public decimal MaxMoney { get; set; }
            /// <summary>
            /// 运费金额
            /// </summary>
            public decimal FreightMoney { get; set; }
        }

    }
}
