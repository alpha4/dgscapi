﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Entity;

namespace Casamiel.Domain.Response
{
    /// <summary>
    /// Get member info card inf rsp.
    /// </summary>
    public record GetMemberInfoCardInfoRsp
    {
        /// <summary>
        /// Gets or sets the member info.
        /// </summary>
        /// <value>The member info.</value>
        public string Mobile { get; set; }
        /// <summary>
        /// Gets or sets the nick.
        /// </summary>
        /// <value>The nick.</value>
        public string Nick { get; set; }
        /// <summary>
        /// Gets or sets the name of the true.
        /// </summary>
        /// <value>The name of the true.</value>
        public string TrueName { get; set; }
        /// <summary>
        /// Gets or sets the birthday.
        /// </summary>
        /// <value>The birthday.</value>
        public DateTime? Birthday { get; set; }
        /// <summary>
        /// Gets or sets the sex.
        /// </summary>
        /// <value>The sex.</value>
        public int? Sex { get; set; }
        /// <summary>
        /// Gets or sets the create date.
        /// </summary>
        /// <value>The create date.</value>
        public DateTime? CreateDate { get; set; }
        /// <summary>
        /// Gets or sets the last login date.
        /// </summary>
        /// <value>The last login date.</value>
        public DateTime? LastLoginDate { get; set; }

        /// <summary>
        /// 来源1app，2小程序，3其它
        /// </summary>
        /// <value>The source.</value>
        public int? Source { get; set; }
        /// <summary>
        /// Gets or sets the card info list.
        /// </summary>
        /// <value>The card info list.</value>
        public List<CardInfoRsp> CardInfoList { get; set; }

    }

    /// <summary>
    /// Card info rsp.
    /// </summary>
    public record CardInfoRsp
    {
        /// <summary>
        /// Gets or sets the cardno.
        /// </summary>
        /// <value>The cardno.</value>
        public string Cardno { get; set; }
        /// <summary>
        /// 卡状态1启用，2禁用
        /// </summary>
        public string Sate { get; set; }
        /// <summary>
        /// Gets or sets the totalmoney.
        /// </summary>
        /// <value>The totalmoney.</value>
        public string Totalmoney { get; set; }
        /// <summary>
        /// Gets or sets the directmoney.
        /// </summary>
        /// <value>The directmoney.</value>
        public string Directmoney { get; set; }
        /// <summary>
        /// Gets or sets the othermoney.
        /// </summary>
        /// <value>The othermoney.</value>
        public string Othermoney { get; set; }
        /// <summary>
        /// Gets or sets the cardtype.
        /// </summary>
        /// <value>The cardtype.</value>
        public string Cardtype { get; set; }
        /// <summary>
        /// Gets or sets the point.
        /// </summary>
        /// <value>The point.</value>
        public int Source { get; set; }
        /// <summary>
        /// bindtime
        /// 
        /// </summary>
        public DateTime BindTime { get; set; }
    }
}
