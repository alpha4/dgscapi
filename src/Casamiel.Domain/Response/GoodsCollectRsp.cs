﻿using System;
namespace Casamiel.Domain.Response
{
    /// <summary>
    /// Goods collect rsp.
    /// </summary>
    public record GoodsCollectRsp
    {
        /// <summary>
        /// 收藏id
        /// </summary>
        public int CollectId { get; set; }
        /// <summary>
        /// 收藏商品id
        /// </summary>
        public int ProductBaseId { get; set; }
        /// <summary>
        /// 收藏人手机
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 商品标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 商品图片
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 商品价格
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 会员价
        /// </summary>
        public decimal CostPrice { get; set; }
    }
}
