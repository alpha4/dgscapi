﻿using System;
namespace Casamiel.Domain.Response
{

	/// <summary>
	/// 邮寄达下单预览输出
	/// </summary>
	public class MallOrderPreviewReponse
	{
		/// <summary>
		/// 运费金额 满88包邮
		/// </summary>
		public decimal FreightMoney { get; set; }

		/// <summary>
		/// 订单金额
		/// </summary>
		public decimal OrderAmount { get; set; }

		/// <summary>
		/// 优惠金额
		/// </summary>
		public decimal Discount { get; set; }

		/// <summary>
		/// 实付金额
		/// </summary>
		public decimal PayAmount { get; set; }
	}

    /// <summary>
    /// 蛋糕商场下单预览输出
    /// </summary>
    public class CakeOrderPreviewReponse
    {
        
        /// <summary>
        /// 订单金额
        /// </summary>
        public decimal OrderAmount { get; set; }

        /// <summary>
        /// 优惠金额
        /// </summary>
        public decimal Discount { get; set; }

        /// <summary>
        /// 实付金额
        /// </summary>
        public decimal PayAmount { get; set; }
    }
}
