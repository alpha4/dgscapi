﻿using System;
namespace Casamiel.Domain.Response
{
    /// <summary>
    /// Goods get list rsp.
    /// </summary>
    public record GoodsGetListRsp
    {
        /// <summary>
        /// Gets or sets the product base identifier.
        /// </summary>
        /// <value>The product base identifier.</value>
        public int ProductBaseId { get; set; }
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        public string Title { get; set; }
        /// <summary>
        /// Gets or sets the image URL.
        /// </summary>
        /// <value>The image URL.</value>
        public string ImageUrl { get; set; }
        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>The price.</value>
        public decimal Price { get; set; }
        /// <summary>
        /// Gets or sets the cost price.
        /// </summary>
        /// <value>The cost price.</value>
        public decimal CostPrice { get; set; }
    }
}
