﻿using System;
namespace Casamiel.Domain.Response
{
    /// <summary>
    /// Cake goods get base rsp.
    /// </summary>
    public record CakeGoodsGetBaseRsp
    {
        /// <summary>
        /// 商品id
        /// </summary>
        public int ProductBaseId { get; set; }
        /// <summary>
        /// 商品sku id
        /// </summary>
        public int GoodsBaseId { get; set; }
        /// <summary>
        /// 一网sku
        /// </summary>
        public int GoodsRelationId { get; set; }
        /// <summary>
        /// 门店id
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 规格
        /// </summary>
        public string GoodsProperty { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 会员价
        /// </summary>
        public decimal CostPrice { get; set; }
        /// <summary>
        /// 状态 0 上架 1 下架
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 备货天数
        /// </summary>
        public int StockDays { get; set; }
       
    }
}
