﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response
{
	/// <summary>
	/// 
	/// </summary>
	public class TakeOutProductBaseRsp: ProductBaseRsp
	{
		/// <summary>
		/// 是否单个规格 true 单个 false 多个
		/// </summary>
		public bool IsSingle { get; set; }

        /// <summary>
        /// 规格值
        /// </summary>
        public string SpecValue { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int GoodeBaseId { get; set; }
   
        /// <summary>        /// 商品关联Id        /// </summary>        public int GoodsRelationId { get; set; }

        /// <summary> 
        /// 库存数量  
        /// </summary> 
        public int StockQuentity { get; set; }
    }
	/// <summary>
	/// 
	/// </summary>
	public class ProductBaseRsp
    {
        /// <summary>
        /// 产品Id
        /// </summary>
        public int ProductId { get; set; }
        /// <summary>
        /// 产品标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 副标题
        /// </summary>
        public string SubTitle { get; set; }
        /// <summary>
        /// 产品图片
        /// </summary>
        public string Image { get; set; }
        /// <summary>
        /// 产品价格
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 会员价
        /// </summary>
        public decimal CostPrice { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 标签id
        /// </summary>
        public int TagId { get; set; }

        /// <summary>
        /// 0正常2 售罄
        /// </summary>
        /// <value>The status.</value>
        public int Status { get; set; }

        /// <summary>
		/// 
		/// </summary>
        public int SalesCount { get; set; }

        /// <summary>
		/// 
		/// </summary>
        public  bool IsLimtSell { get; set; }

        /// <summary>
		/// 
		/// </summary>
        public int LimitNums { get; set; }
    }
}
