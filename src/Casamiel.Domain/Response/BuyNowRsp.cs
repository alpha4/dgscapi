﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Casamiel.Domain.Response
{
   /// <summary>
   /// 
   /// </summary>
    public record  BuyNowRsp
    {
        /// <summary>
        /// Gets or sets the product base identifier.
        /// </summary>
        /// <value>The product base identifier.</value>
        public int ProductBaseId { get; set; }
        /// <summary>
        /// 关联id
        /// </summary>
        /// <value>The goods relation identifier.</value>
        public int GoodsRelationId { get; set; }
        /// <summary>
        /// Gets or sets the goods identifier.
        /// </summary>
        /// <value>The goods identifier.</value>
        public int GoodsId { get; set; }
        /// <summary>
        /// Gets or sets the store identifier.
        /// </summary>
        /// <value>The store identifier.</value>
        public int StoreId { get; set; }
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        public string Title { get; set; }
        /// <summary>
        /// Gets or sets the image URL.
        /// </summary>
        /// <value>The image URL.</value>
        public string ImageUrl { get; set; }
       

        /// <summary>
        /// Gets or sets the name of the store.
        /// </summary>
        /// <value>The name of the store.</value>
        public string StoreName { get; set; }

        /// <summary>
        /// 提货时间
        /// </summary>
        /// <value>The picktime.</value>
        public DateTime TakeTime { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string GoodsProperty
        {
            get;set;
        }
        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>The price.</value>
        public decimal Price { get; set; }
        /// <summary>
        /// Gets or sets the cost price.
        /// </summary>
        /// <value>The cost price.</value>
        public decimal CostPrice { get; set; }


        /// <summary>
        /// 数量
        /// </summary>
        /// <value>The stock.</value>
        public int Quantity { get; set; }


        /// <summary>
        /// 状态 0 上架 1 下架
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// Gets or sets the stock days.
        /// </summary>
        /// <value>The stock days.</value>
        public int StockDays { get; set; }
        /// <summary>
        /// Gets or sets the store relation identifier.
        /// </summary>
        /// <value>The store relation identifier.</value>
        public int StoreRelationId { get; set; }
    }
}
