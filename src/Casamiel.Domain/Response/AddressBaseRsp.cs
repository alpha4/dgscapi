﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response
{/// <summary>
///
/// </summary>
    public record AddressBaseRsp
    {
        /// <summary>
        /// Gets or sets the consignee identifier.
        /// </summary>
        /// <value>The consignee identifier.</value>
        public int ConsigneeId { get; set; }


        /// <summary>
        /// 是否默认
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// 排序编号，倒序
        /// </summary>
        public int OrderNo { get; set; }

        /// <summary>
        /// 联系人姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 手机 会员信息
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 电话 
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string FullAddress { get; set; }

        /// <summary>
        /// 省ID
        /// </summary>
        public int ProvinceId { get; set; }
        /// <summary>
        /// Gets or sets the name of the provice.
        /// </summary>
        /// <value>The name of the provice.</value>
        public string ProviceName { get; set; }

        /// <summary>
        /// 市ID
        /// </summary>
        public int CityId { get; set; }
        /// <summary>
        /// Gets or sets the name of the city.
        /// </summary>
        /// <value>The name of the city.</value>
        public string CityName { get; set; }

        /// <summary>
        /// 区ID
        /// </summary>
        public int DistrictId { get; set; }
        /// <summary>
        /// Gets or sets the name of the district.
        /// </summary>
        /// <value>The name of the district.</value>
        public string DistrictName { get; set; }

        /// <summary>
        /// 邮编
        /// </summary>
        public string Postcode { get; set; }


        /// <summary>
        /// 身份证
        /// </summary>
        public string IdentityCard { get; set; }


        /// <summary>
        /// 纬度
        /// </summary>
        public double Latitude { get; set; }
        /// <summary>
        /// 经度
        /// </summary>
        public double Longitude { get; set; }
        /// <summary>
        /// 门店ID
        /// </summary>
        public int StoreId { get; set; } = 0;
        /// <summary>
        /// 配送方式 订单类型 1: 自提 2:本地配送 3：快递配送
        /// </summary>
        public int OrderType { get; set; }
    }
}
