﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Response.IC
{
	/// <summary>
	/// 
	/// </summary>
	public class TicketsRole
	{
		/// <summary>
		/// 
		/// </summary>
		public int Productid { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public int Ticketid { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string Ticketcode { get; set; }
		/// <summary>
		/// 幸福蛋糕树专享优惠券-满50元立减10元
		/// </summary>
		public string Productname { get; set; }

		/// <summary>
		/// 51、当前券要求最低的单据金额，limitvalue为单据金额
		///21、券仅允许消费的产品，limitvalue 为限制的产品标识
		///215、券必须消费的产品，limitvalue 为限制的产品标识
		///216、券需要消费的产品(之一)，limitvalue 为限制的产品标识，和 215 不同，216 只需要有一种产品就可以了，如果 limitvalue 只有一种产品，则和 215 效果相同
		///22、券不允许消费的产品，limitvalue 为限制的产品标识
		/// </summary>
		public int Limittype { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string Limitvalue { get; set; }
		/// <summary>
		/// 单据金额至少需要达到 50.00 才允许使用
		/// </summary>
		public string Limitdesc { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	public class TicketsRules
	{
		/// <summary>
		/// 
		/// </summary>
		public List<TicketsRole> Tickets { get; set; }
	}
}
