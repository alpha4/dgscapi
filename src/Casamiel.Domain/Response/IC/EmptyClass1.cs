﻿using System;
namespace Casamiel.Domain.Response.IC
{
	/// <summary>
	/// 消费记录
	/// </summary>
	public class IcRecord
	{
		 
		/// <summary>
		/// 卡号
		/// </summary>
		public string Cardno { get; set; }

		/// <summary>
		/// 门店id
		/// </summary>
		public int Shopid { get; set; }

		/// <summary>
		/// 门店名称
		/// </summary>
		public string Shopname { get; set; }

		/// <summary>
		/// 消费时间
		/// </summary>
		public DateTime Recdate { get; set; }

		/// <summary>
		/// 充值赠送
		/// </summary>
		public string Consumetype { get; set; }

		/// <summary>
		/// 唯一交易号
		/// </summary>
		public string Tradeno { get; set; }

		/// <summary>
		/// 总金额
		/// </summary>
		public decimal Totalmoney { get; set; }

		/// <summary>
		/// 折扣金额
		/// </summary>
		public decimal Discountmoney { get; set; }

		/// <summary>
		/// 会员卡实付金额
		/// </summary>
		public decimal Realpaymoney { get; set; }

		/// <summary>
		/// 使用卡上直接金额（充值金额）
		/// </summary>
		public decimal Directmoney { get; set; }

		/// <summary>
		/// 使用卡上其它金额（赠送金额）
		/// </summary>
		public decimal Othermoney { get; set; }
		 
	}
}
