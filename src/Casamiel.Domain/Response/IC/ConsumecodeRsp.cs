﻿using System;
namespace Casamiel.Domain.Response.IC
{
	/// <summary>
	/// {\"cardno\":\"91000003\",\"wxopenid\":\"\",\"phoneno\":\"\",\"consumecode\":\"672691164922858035\",\"qrcodeurl\":\"\",\"barcodeurl\":\"\"}
	/// </summary>
	public class ConsumecodeRsp
	{
		/// <summary>
		/// 
		/// </summary>
		public string Cardno { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Phoneno { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Consumecode { get; set; }

	}

	/// <summary>
	/// {\"cardno\":\"91000003\",\"phoneno\":\"\",\"wxopenid\":\"\",\"consumecode\":\"751102631897364858\",\"money\":0.00}
	/// </summary>
	public class CheckConsumecodeRsp:ConsumecodeRsp
	{
		/// <summary>
		/// 
		/// </summary>
		public decimal Money { get; set; }
	}
}
