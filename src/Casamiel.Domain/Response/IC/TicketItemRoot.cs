﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Response
{
    /// <summary>
    /// Ticket item root.
    /// </summary>
    public class TicketItemRoot
    {
        /// <summary>
        /// 
        /// </summary>
        public string Cardno { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public List<TicketItem> Tickets;

        
    }
    /// <summary>
    /// Ticket item.
    /// </summary>
    public class TicketItem
    {
        /// <summary>
        /// 
        /// </summary>
        public int Productid { get; set; }
        /// <summary>
        /// 卡号
        /// </summary>
        public string Cardno { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Ticketid { get; set; }
        /// <summary>
        /// 券名称
        /// </summary>
        public string Ticketname { get; set; }
        /// <summary>
        /// 券类型
        /// </summary>
        public string Tickettype { get; set; }
        /// <summary>
        /// 券来源方式
        /// </summary>
        public string Source { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int State { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Je { get; set; }
        /// <summary>
        /// Gets or sets the prage.
        /// </summary>
        /// <value>The prage.</value>
        public List<prangeItem> Pragen { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime Makedate { get; set; }
        /// <summary>
        /// startdate
        /// </summary>
        public DateTime Startdate { get; set; }
        /// <summary>
        /// Gets or sets the enddate.
        /// </summary>
        /// <value>The enddate.</value>
        public DateTime Enddate { get; set; }
        /// <summary>
        /// Prange item.
        /// </summary>
        public class prangeItem
        {
            /// <summary>
            /// Gets or sets the pid.
            /// </summary>
            /// <value>The pid.</value>
            public int Pid { get; set; }
            /// <summary>
            /// Gets or sets the count.
            /// </summary>
            /// <value>The count.</value>
            public int Count { get; set; }
        }
    }
}
