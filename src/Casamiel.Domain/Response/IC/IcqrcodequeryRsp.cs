﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response.IC
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class IcqrcodequeryRsp
    {
        /// <summary>
        /// 充值金额
        /// </summary>
        public decimal Chargemoney { get; set; }

        /// <summary>
        /// 赠送金额
        /// </summary>
        public decimal Largessmoney { get; set; }
        //"point": 0,
        //"promotion": 1,
         /// <summary>
         /// 制作时间
         /// </summary>
        public string Makedate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Startdate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Enddate { get; set; }
        /// <summary>
        /// 使用时间
        /// </summary>
        public string  Useddate { get; set; }

        /// <summary>
        /// 客户名称
        /// </summary>
        public string Customername { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public string StateDesc { get; set; }
    }
}
