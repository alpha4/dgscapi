﻿using System;
namespace Casamiel.Domain.Response.IC
{
	/// <summary>
	/// Card info response.
	/// </summary>
	public class CardInfoResponse
	{
		/// <summary>
		/// Gets or sets the cardno.
		/// </summary>
		/// <value>The cardno.</value>
		public string cardno { get; set; }
		/// <summary>
		/// 卡状态1启用，2禁用
		/// </summary>
		public string state { get; set; }
		/// <summary>
		/// Gets or sets the totalmoney.
		/// </summary>
		/// <value>The totalmoney.</value>
		public string totalmoney { get; set; }
		/// <summary>
		/// Gets or sets the directmoney.
		/// </summary>
		/// <value>The directmoney.</value>
		public string directmoney { get; set; }
		/// <summary>
		/// Gets or sets the othermoney.
		/// </summary>
		/// <value>The othermoney.</value>
		public string othermoney { get; set; }
		/// <summary>
		/// Gets or sets the cardtype.
		/// </summary>
		/// <value>The cardtype.</value>
		public string cardtype { get; set; }

		/// <summary>
		/// 等级
		/// 91,
		/// 92,
		/// 93-幸福卡
		/// </summary>
		public  int Grade { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public  string Gradename { get; set; }

		/// <summary>
		/// Gets or sets the point.
		/// </summary>
		/// <value>The point.</value>
		public string point { get; set; }
	}
}
