﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response.IC
{
    /// <summary>
    /// IC产品价格
    /// </summary>
    public class ICPriceRsp
    {
        /// <summary>
        /// 
        /// </summary>
        public List<ProductPrice> product { get; set; }
        /// <summary>
        ///  ProductPrice
        /// </summary>
        public class ProductPrice
        {
            /// <summary>
            /// 
            /// </summary>
            public string pid { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string pname { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int count { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public decimal originprice { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public decimal icprice { get; set; }
        }
    }
}
