﻿using System;
namespace Casamiel.Domain.Response.IC
{
	/// <summary>
	/// 
	/// </summary>
	public class TicketGiveReponse
	{
		/// <summary>
		/// 
		/// </summary>
		public string Ticketname { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public decimal Je { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string ForwardCode { get; set; }

		/// <summary>
		/// 0发起转赠；1转赠成功，-1转赠超时
		/// </summary>
		public int State { get; set; }
	}
}
