﻿using System;
namespace Casamiel.Domain.Response
{
    /// <summary>
    /// Order create rsp.
    /// </summary>
    public record OrderCreateRsp
    {
        /// <summary>
        /// 订单id
        /// </summary>
        public int OrderBaseId { get; set; }
        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderCode { get; set; }
        /// <summary>
        /// 订单金额
        /// </summary>
        public decimal OrderMoney { get; set; }
        /// <summary>
        /// Gets or sets the pay cash money.
        /// </summary>
        /// <value>The pay cash money.</value>
        public decimal PayMoney { get; set; }
    }
}
