﻿using System;
namespace Casamiel.Domain.Response
{
    /// <summary>
    /// ICR esult.
    /// </summary>
    public class ICResult
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        public int code { get; set; }
        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>The content.</value>
        public string content { get; set; }
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string msg { get; set; }
    }
    /// <summary>
    /// ICR esult ext.
    /// </summary>
    public class ICResultExt : ICResult
    {
        /// <summary>
        /// Gets or sets the pagesize.
        /// </summary>
        /// <value>The pagesize.</value>
        public int pagesize { get; set; }
        /// <summary>
        /// Gets or sets the pageindex.
        /// </summary>
        /// <value>The pageindex.</value>
        public int pageindex { get; set; }
        /// <summary>
        /// Gets or sets the pagecount.
        /// </summary>
        /// <value>The pagecount.</value>
        public int pagecount { get; set; }
    }
}
