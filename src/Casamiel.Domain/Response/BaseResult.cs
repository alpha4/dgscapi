﻿using System;
namespace Casamiel.Domain.Response
{
     

    /// <summary>
    /// BaseResult
    /// </summary>
    public class BaseResult<T>
    {
       
       /// <summary>
       /// Initializes a new instance of the <see cref="T:Casamiel.Domain.Response.BaseResult`1"/> class.
       /// </summary>
       /// <param name="defaultValue">Default value.</param>
        public BaseResult(T defaultValue)
        {
            Content = defaultValue;
            Code = 0;
            Msg = "";

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Application.Models.BaseResult`1"/> class.
        /// </summary>
        public BaseResult()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Controllers.V2.sModel`1"/> class.
        /// </summary>
        /// <param name="defaultValue">Default value.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        public BaseResult(T defaultValue, int code, string msg)
        {
            Content = defaultValue;
            Code= code;
            Msg = msg;

        }
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        public int Code { get; private set; } 

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>The content.</value>
        public T Content
        { get; private set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Msg{ get; private set; }

    }
}
