﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Response
{
    /// <summary>
    /// 产品库存
    /// </summary>
    public class ICProductStockQueryRoot
    {

        /// <summary>
        /// 
        /// </summary>
        public List<ProductStockExt> product { get; set; }
    }
    /// <summary>
    /// Product stock ext.
    /// </summary>
    public class ProductStockExt : ProductStock
    {
        /// <summary>
        /// 扩展title
        /// </summary>
        /// <value>The title.</value>
        public string title { get; set; }
    }
    /// <summary>
    /// Product stock.
    /// </summary>
    public class ProductStock
    {
        /// <summary>
        /// 产品标识
        /// </summary>
        public string pid { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        public string pname { get; set; }
        /// <summary>
        /// 库存
        /// </summary>
        public int count { get; set; }
        /// <summary>
        /// 产品原价
        /// </summary>
        public decimal originprice { get; set; }
        /// <summary>
        /// 产品会员价
        /// </summary>
        public decimal icprice { get; set; }


    }
}
