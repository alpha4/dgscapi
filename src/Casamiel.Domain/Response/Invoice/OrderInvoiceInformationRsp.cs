﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response.Invoice
{
    /// <summary>
    /// 开票发票信息
    /// </summary>
    public sealed class OrderInvoiceInformationRsp
    {
        /// <summary>
        /// 
        /// </summary>
        public OrderInvoice OrderInvoice { get;set;}
        /// <summary>
        /// 明细
        /// </summary>
        public List<InvoiceItem> OrderInvoiceDetails { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class OrderInvoice
    {
        /// <summary>
        /// 发票单号
        /// </summary>
        public string OrderNO { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        public decimal Ordertotal { get; set; }
        /// <summary>
        /// 税额
        /// </summary>
        public decimal Taxtotal { get; set; }
        /// <summary>
        /// 税后价
        /// </summary>
        public decimal Bhtaxtotal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Account { get; set; }

    }
    /// <summary>
    /// 
    /// </summary>
    public class InvoiceItem
    {
        /// <summary>
        /// 
        /// </summary>
        public string GoodsName { get; set; }
        /// <summary>
        /// 税率
        /// </summary>
        public decimal Taxrate { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        public decimal Taxamt { get; set; }

    }
}
