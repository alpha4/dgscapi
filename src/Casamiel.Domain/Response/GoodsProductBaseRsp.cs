﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response.MWeb;

namespace Casamiel.Domain.Response
{
    /// <summary>
    /// Goods product base rsp.
    /// </summary>
    public record GoodsProductBaseRsp
    {
        /// <summary>
        /// 门店id
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 门店关联id
        /// </summary>
        public int StoreRelationId { get; set; }
        /// <summary>
        /// 产品Id
        /// </summary>
        public int ProductBaseId { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 商品子标题
        /// </summary>
        public string SubTitle { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 售卖价格
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 原价
        /// </summary>
        public decimal CostPrice { get; set; }
        /// <summary>
        /// 地区
        /// </summary>
        public string Area { get; set; }
        /// <summary>
        /// 备货天数
        /// </summary>
        /// <value>The stock days.</value>
        public int stockDays { get; set; }
        /// <summary>
        /// 销售属性
        /// </summary>
        public List<PropertyBaseRsp> SalePropertyList { get; set; }

        /// <summary>
        /// 商品图片列表
        /// </summary>
        public List<string> Images { get; set; }
        /// <summary>
        /// 包含的商品SKU
        /// </summary>
        public List<GoodsSkuRsp> GoodsList { get; set; }


    }
}
