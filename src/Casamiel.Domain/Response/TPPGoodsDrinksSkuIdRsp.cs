﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response
{
    /// <summary>
    /// 
    /// </summary>
    public record TPPGoodsDrinksSkuIdRsp
    {
        /// <summary>
        /// 商品sku Id
        /// </summary>
        public int GoodsRelationId { get; set; }
    }
}
