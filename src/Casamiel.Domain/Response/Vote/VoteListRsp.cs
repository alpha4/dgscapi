﻿using System;
namespace Casamiel.Domain.Response.Vote
{

	/// <summary>
	/// 
	/// </summary>
	public class VoteListRsp
	{
 

		/// <summary>		/// 被投票人Id		/// </summary>        public int MemberId { get; set; }

        /// <summary>
        /// 门店名称
        /// </summary>
        public string StoreName { get; set; }

        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; }
		 
		/// <summary>		/// 相片		/// </summary>        public string Image { get; set; }
		 
		/// <summary>		/// 描述		/// </summary>        public string Description { get; set; }

		 
		/// <summary>		/// 票数		/// </summary>        public int VoteNums { get; set; }

 
		/// <summary>		/// 今天是否投过票 true 已投 false 未投		/// </summary>        public bool IsVote { get; set; }
	}
	/// <summary>
	/// 
	/// </summary>
	public class VoteDetailRsp
	{
 
		/// <summary>		/// 被投票人Id		/// </summary>        public int MemberId { get; set; }
 
		/// <summary>		/// 相片		/// </summary>        public string Image { get; set; }

        /// <summary>
        /// 门店名称
        /// </summary>
        public string StoreName { get; set; }

 
		/// <summary>		/// 描述		/// </summary>        public string Description { get; set; }
		 
		/// <summary>		/// 详情		/// </summary>        public string Detail { get; set; }

		 
		/// <summary>		/// 票数		/// </summary>        public int VoteNums { get; set; }
	}
}
