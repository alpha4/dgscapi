﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response
{
    /// <summary>
    /// 
    /// </summary>
    public record IndexTagBaseRsp
    {
        /// <summary>
        /// 标签id
        /// </summary>
        public int TagBaseId { get; set; }
        /// <summary>
        /// 标签名称
        /// </summary>
        public string TagName { get; set; }
        /// <summary>
        /// 标签图标
        /// </summary>
        public string SImgUrl { get; set; }
    }
}
