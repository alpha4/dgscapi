﻿using System;
namespace Casamiel.Domain.Response.Account
{
	/// <summary>
	/// 
	/// </summary>
	public record MemberInfoResponse
	{
		///<summary>
		///会员注册手机号
		///</summary>		
		public string Mobile { get; set; }

		///<summary>
		///注册时间
		///</summary>		
		public DateTime RegisterTime { get; set; }

		///<summary>
		///卡号
		///</summary>		
		public string CardNo { get; set; }

		///<summary>
		///卡类型
		///</summary>		
		public int? CardType { get; set; }

		///<summary>
		///绑定时间
		///</summary>		
		public DateTime? BindTime { get; set; }

		///<summary>
		///绑定来源 1 小程序 2 app
		///</summary>		
		public int? BindSource { get; set; }

		///<summary>
		///更新时间
		///</summary>		
		public DateTime? UpdateTime { get; set; }

		///<summary>
		///姓名
		///</summary>		
		public string RealName { get; set; }

		///<summary>
		///0 女 1 男
		///</summary>		
		public bool? Sex { get; set; }

		///<summary>
		///生日
		///</summary>		
		public DateTime? Birthday { get; set; }

		///<summary>
		/// 省
		///</summary>		
		public string Provice { get; set; }

		///<summary>
		///市
		///</summary>		
		public string City { get; set; }

		///<summary>
		///区
		///</summary>		
		public string District { get; set; }

		///<summary>
		/// 门店id
		///</summary>		
		public int? StoreId { get; set; }

		///<summary>
		/// 门店名称
		///</summary>		
		public string StoreName { get; set; }

		///<summary>
		/// 最近门店距离
		///</summary>		
		public string StoreDistance { get; set; }

		///<summary>
		/// 居住方式
		///</summary>		
		public string LiveWay { get; set; }

		///<summary>
		/// 是否加店长微笑
		///</summary>		
		public bool? IsAddWeixin { get; set; }

		/// <summary>
		/// 邀请码
		/// </summary>
		public string InvitationCode { get; set; }
	}
}