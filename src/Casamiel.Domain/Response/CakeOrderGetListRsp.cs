﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Response
{
    /// <summary>
    /// Cake order get list rsp.
    /// </summary>
    public record CakeOrderGetListRsp
    {
        /// <summary>
        /// 订单id
        /// </summary>
        public int OrderBaseId { get; set; }
        /// <summary>
        /// 订单编码
        /// </summary>
        public string OrderCode { get; set; }
        /// <summary>
        /// 订单状态，1：新建、2：已支付、3:退款中、4：退款成功 、5：申请取消、 7：已完成 、8：已取消(退款) 、9：交易关闭(未支付自动取消) 
        /// </summary>
        public int OrderStatus { get; set; }
        /// <summary>
        /// 来源 0  蛋糕商城 1 美团 2 饿了么 3 京东 4 滴滴 5天猫 6 口碑 7 全国送商城
        /// </summary>
        public int Source { get; set; }
        /// <summary>
        /// 快递状态，1：待发货、2：已发货、3：已签收
        /// </summary>
        public int ExpressStatus { get; set; }
        /// <summary>
        /// 订单应付金额
        /// </summary>
        public decimal OrderMoney { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 门店ID
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 门店名称
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 门店关联Id
        /// </summary>
        public int StoreRelationId { get; set; }
        /// <summary>
        /// 门店联系方式
        /// </summary>
        public string StorePhone { get; set; }
        /// <summary>
        /// 订单类型 1: 自提 2:外卖 3 快递配送
        /// </summary>
        public int OrderType { get; set; }
        /// <summary>
        /// 下单时间
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 送达时间
        /// </summary>
        public string ConfrimReceiveTime { get; set; }
        /// <summary>
        /// 订单商品
        /// </summary>
        public List<MWebOrderGoodsBaseRsp> OrderGoodsList { get; set; }
    }
}
