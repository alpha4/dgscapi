﻿using System;
namespace Casamiel.Domain.Response
{
    /// <summary>
    /// Payment rsp.
    /// </summary>
    public class PaymentRsp
    {
        /// <summary>
        /// 支付请求
        /// </summary>
        /// <value>The paymentrequest.</value>
        public string Paymentrequest { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        /// <value>The order code.</value>
        public string OrderCode { get; set; }
        /// <summary>
        ///支付方式 1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
        /// </summary>
        /// <value>The pay method.</value>
        public int PayMethod { set; get; }
        /// <summary>
        /// 是否支付
        /// </summary>
        /// <value><c>true</c> if payed; otherwise, <c>false</c>.</value>
        public bool Payed { get; set; }
        // new { paymentrequest = "", ordercode = entity.OrderCode, payMethod = "3", payed = true } 
    }
}
