﻿using System;
namespace Casamiel.Domain.Response
{
    /// <summary>
    /// Ge store list for navigation rsp.
    /// </summary>
    public record GetStoreListForNavigationRsp
    {
        /// <summary>
        /// 
        /// </summary>
        public int StoreId { get; set; }

     
        /// <summary>
        /// 对接信息Id
        /// </summary>
        public int RelationId { get; set; }

        /// <summary>
        /// 门店名称
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// Gets or sets the name of the sub store.
        /// </summary>
        /// <value>The name of the sub store.</value>
        public string SubStoreName { get; set; }

        
        /// <summary>
        /// 营业时间（8：00--18：00）
        /// </summary>
        public string ShopHours { get; set; }

        /// <summary>
        /// 门店电话
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 省ID
        /// </summary>
        public int ProvinceId { get; set; }

        /// <summary>
        /// 城市ID
        /// </summary>
        public int CityId { get; set; }

        /// <summary>
        /// 区县ID
        /// </summary>
        public int DistrictId { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string FullAddress { get; set; }

        /// <summary>
        /// 经度
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// 纬度
        /// </summary>
        public double Latitude { get; set; }

         /// <summary>
         ///  距离
         /// </summary>
         /// <value>The distance.</value>
        public double Distance { get; set; }


    }
}
