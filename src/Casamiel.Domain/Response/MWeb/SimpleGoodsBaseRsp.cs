﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Response.MWeb
{
    /// <summary>
    /// Simple goods base rsp.
    /// </summary>
    public sealed class SimpleGoodsBaseRsp
    {
        /// <summary>
        /// 产品Id
        /// </summary>
        public int ProductBaseId { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 商品子标题
        /// </summary>
        public string SubTitle { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 售卖价格
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 原价
        /// </summary>
        public decimal CostPrice { get; set; }

        /// <summary>
        /// 商品图片列表
        /// </summary>
        public List<string> Images { get; set; }
    }
}
