﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Response.MWeb
{
    /// <summary>
    /// Property item rsp.
    /// </summary>
    public class PropertyItemRsp
    {
        /// <summary>
        /// 属性值Id
        /// </summary>
        public int PropertyItemId { get; set; }
        /// <summary>
        /// 属性ID
        /// </summary>
        public int PropertyId { get; set; }
        /// <summary>
        /// 属性值
        /// </summary>
        public string PropertyValue { get; set; }
    }
    /// <summary>
    /// Property base rsp.
    /// </summary>
    public class PropertyBaseRsp
    {
        /// <summary>
        /// 属性ID
        /// </summary>
        public int PropertyId { get; set; }
        /// <summary>
        /// 属性名称
        /// </summary>
        public string PropertyName { get; set; }
        /// <summary>
        /// 属性值集
        /// </summary>
        public List<PropertyItemRsp> ItemList { get; set; }
    }
}
