﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response.MWeb
{
    /// <summary>
    /// 
    /// </summary>
    public class MWebNewsBaseRsp
    {

        /// <summary>
        /// 图文信息id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 副标题
        /// </summary>
        public string SubTitle { get; set; }
        /// <summary>
        ///图片
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
    }
}
