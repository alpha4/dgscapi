﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Response.MWeb
{
    /// <summary>
    /// Goods base rsp.
    /// </summary>
    public class GoodsBaseRsp
    {
        /// <summary>
        /// 产品Id
        /// </summary>
        public int ProductBaseId { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 商品子标题
        /// </summary>
        public string SubTitle { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 售卖价格
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 原价
        /// </summary>
        public decimal CostPrice { get; set; }

        /// <summary>
        /// 商品图片列表
        /// </summary>
        public List<string> Images { get; set; }

        /// <summary>
        /// 包含的商品SKU
        /// </summary>
        public List<GoodsSkuRsp> GoodsList { get; set; }

        /// <summary>
        /// 销售属性
        /// </summary>
        public List<PropertyBaseRsp> SalePropertyList { get; set; }
    }
    /// <summary>
    /// Goods sku rsp.
    /// </summary>
    public class GoodsSkuRsp
    {
        /// <summary>
        /// sku  Id
        /// </summary>
        public int GoodsBaseId { get; set; }
        /// <summary>
        /// 关联Id
        /// </summary>
        public int RelationId { get; set; }
        /// <summary> 
        /// 价格  
        /// </summary> 
        public decimal Price { get; set; }
        /// <summary>
        /// 原价
        /// </summary>
        public decimal CostPrice { get; set; }
        /// <summary> 
        /// 状态 0 下架 1 上架
        /// </summary> 
        public int Status { get; set; }
        /// <summary> 
        /// 库存数量  
        /// </summary> 
        public int StockQuentity { get; set; }
        /// <summary>
        /// 当前sku下图片
        /// </summary>
        public string ImgPath { get; set; }
        /// <summary>
        /// 商品销售属性列表
        /// </summary>
        public List<PropertyItemRsp> GoodsTypeItemList { get; set; }
    }
}
