﻿using System;
namespace Casamiel.Domain.Response.MWeb
{
    /// <summary>
    /// Tpp goods product rsp.
    /// </summary>
    public class TPP_Goods_ProductRsp
    {
        /// <summary>
        /// 商品id
        /// </summary>
        public int ProductBaseId { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 商品图片
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 会员价
        /// </summary>
        public decimal CostPrice { get; set; }
    }
}
