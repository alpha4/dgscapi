﻿using System;
using System.Collections.Generic;

namespace Casamiel.Domain.Response.MWeb
{
	/// <summary>
	/// 
	/// </summary>
	public class OrderExpressTracesReponse
	{
 
		/// <summary>		/// 物流id		/// </summary>        public int ExpressTracesId { get; set; }

		/// <summary>		/// 快递单号		/// </summary>        public string ExpressCode { get; set; }

		/// <summary>		/// 物流公司		/// </summary>        public string ExpressName { get; set; }

		/// <summary>		/// 物流轨迹		/// </summary>        public List<ExpressTraces> ExpressTraces { get; set; }

	}

	/// <summary>
	/// 
	/// </summary>
	public class ExpressTraces
	{
		/// <summary>
		/// 时间
		/// </summary>
		public string AcceptTime { get; set; }
		/// <summary>
		/// 描述
		/// </summary>
		public string AcceptStation { get; set; }

	}
}
