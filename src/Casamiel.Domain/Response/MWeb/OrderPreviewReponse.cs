﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain.Response.MWeb
{
    /// <summary>
    /// 
    /// </summary>
    public class OrderPreviewReponse
    {
        /// <summary>
        /// 运费金额 满88包邮
        /// </summary>
        public decimal FreightMoney { get; set; }

        /// <summary>
        /// 订单金额
        /// </summary>
        public decimal OrderAmount { get; set; }

        /// <summary>
        /// 优惠金额
        /// </summary>
        public decimal Discount { get; set; }

        /// <summary>
        /// 实付金额
        /// </summary>
        public decimal PayAmount { get; set; }
    }
    }
