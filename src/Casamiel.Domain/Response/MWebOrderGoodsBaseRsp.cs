﻿using System;
namespace Casamiel.Domain.Response
{
    /// <summary>
    /// MW eb order goods base rsp.
    /// </summary>
    public record MWebOrderGoodsBaseRsp
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string GoodsTitle { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public string GoodsImage { get; set; }

        /// <summary>
        /// 单价
        /// </summary>
        public decimal GoodsPrice { get; set; }

        /// <summary>
        /// 会员价
        /// </summary>
        /// <value>The cost price.</value>
        public decimal CostPrice { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int GoodsQuantity { get; set; }

        /// <summary>
        /// 规格
        /// </summary>
        public string GoodsPropery { get; set; }

        /// <summary>
        /// 商品Id
        /// </summary>
        public int GoodsBaseId { get; set; }

        /// <summary>
        /// 产品ID
        /// </summary>
        public int ProductBaseId { get; set; }

    }
}
