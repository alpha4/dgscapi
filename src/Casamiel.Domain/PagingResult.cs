﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Domain
{
	/// <summary>
	/// 分页结果
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class PagingResult<T>
	{
		/// <summary>
		/// 分页数据
		/// </summary>
		public List<T> Data { get; set; }

		/// <summary>
		/// 页索引，从1开始
		/// </summary>
		public int PageIndex { get; set; }

		/// <summary>
		/// 每页记录条数
		/// </summary>
		public int PageSize { get; set; }

		/// <summary>
		/// 总行数
		/// </summary>
		public int Total { get; set; }
	}
}
