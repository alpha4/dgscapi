﻿using System;
using Casamiel.API.Application.Models;
using Casamiel.API.Application.Services;
using Casamiel.API.Infrastructure.Middlewares;
using Casamiel.Application;

namespace Casamiel.API.Application.Commands
{
    /// <summary>
    /// Base command.
    /// </summary>
    public class BaseCommandHandler
    {

        /// <summary>
        /// 
        /// </summary>
        public IMobileCheckCode MobileCheckCode { get; private set; }



        /// <summary>
        /// The member card.
        /// </summary>
        public IMemberCardService MemberCardService { get; private set; }

        /// <summary>
        /// The user log.
        /// </summary>
        public IUserLogService UserLogService { get; private set; }

        /// <summary>
        /// The logger.
        /// </summary>
        public NLog.ILogger Logger = NLog.LogManager.GetLogger("BizInfo");
        /// <summary>
        /// The member service.
        /// </summary>
        public IMemberService MemberService {
            get; private set;
        }
        /// <summary>
        /// The iic API service.
        /// </summary>
        public IIcApiService IcApiService { get; private set; }
        /// <summary>
        /// The token provider.
        /// </summary>
        public ITokenProvider TokenProvider { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iicApiService"></param>
        /// <param name="memberCard"></param>
        /// <param name="member"></param>
        /// <param name="mobileCheckCode"></param>
        /// <param name="userLogService"></param>
        public BaseCommandHandler(IIcApiService iicApiService,
            IMemberCardService memberCard, IMemberService member,
              IMobileCheckCode mobileCheckCode, IUserLogService userLogService)
        {
            MemberCardService = memberCard;
            MobileCheckCode = mobileCheckCode;
            IcApiService = iicApiService;
            UserLogService = userLogService;
            MemberService = member;
        }
        /// <summary>
        /// 
        /// /// </summary>
        /// <param name="iicApiService">Iic API service.</param>
        /// <param name="memberCard">Member card.</param>
        /// <param name="mobileCheckCode">Mobile check code.</param>
        /// <param name="memberService">Member service.</param>
        /// <param name="userLogService">User log service.</param>
        /// <param name="tokenProvider">Token provider.</param>
        public BaseCommandHandler(IIcApiService iicApiService,
            IMemberCardService memberCard,
              IMobileCheckCode mobileCheckCode, IMemberService memberService, IUserLogService userLogService, ITokenProvider tokenProvider)
        {
            MemberCardService = memberCard;
            this.MobileCheckCode = mobileCheckCode;
            IcApiService = iicApiService;
            UserLogService = userLogService;
            TokenProvider = tokenProvider;
            MemberService = memberService;
        }
    }
}
