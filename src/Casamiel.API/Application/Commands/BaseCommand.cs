﻿using System;
using Newtonsoft.Json;

namespace Casamiel.API.Application.Commands
{
    /// <summary>
    /// Base command.
    /// </summary>
    public class BaseCommand
    {

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public string RequestUrl { get; set; }
        /// <summary>
        /// Gets or sets the user ip.
        /// </summary>
        /// <value>The user ip.</value>
        [JsonIgnore]
        public string UserIP { get; set; }
       
       
    }
}
