﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.IC;
using MediatR;

namespace Casamiel.API.Application.Commands
{
    /// <summary>
    /// Get my card command.
    /// </summary>
    public class GetMyCardCommand: BaseCommand, IRequest<BaseResult<List<CardInfoResponse>>>
    {
        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
        /// <summary>
        /// Gets the source.
        /// </summary>
        /// <value>The source.</value>
        public int Source { get; private set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Application.Commands.GetMyCardCommand"/> class.
        /// </summary>
        /// <param name="Mobile">Mobile.</param>
        /// <param name="Source">Source.</param>
        public GetMyCardCommand(string Mobile,int Source)
        {
            this.Mobile = Mobile;
            this.Source = Source;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        public GetMyCardCommand(GetMyCardCommand other)
        :  this(PassThroughNonNull(other).Mobile,
          PassThroughNonNull(other).Source)
            {}

        private static GetMyCardCommand PassThroughNonNull(GetMyCardCommand person)
        {
            if (person == null)
                throw new ArgumentNullException(nameof(person));
            return person;
        }
    }
}
