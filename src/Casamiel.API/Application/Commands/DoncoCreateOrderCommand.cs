﻿using System;
using MediatR;

namespace Casamiel.API.Application.Commands
{
    /// <summary>
    /// Donco create order command.
    /// </summary>
    public class DoncoCreateOrderCommand:IRequest<bool>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Application.Commands.DoncoCreateOrderCommand"/> class.
        /// </summary>
        public DoncoCreateOrderCommand()
        {
        }
    }
}
