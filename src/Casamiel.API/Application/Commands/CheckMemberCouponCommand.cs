﻿using System;
using Casamiel.API.Application.Models;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.API.Application.Commands
{

    /// <summary>
    /// Check member coupon command.
    /// </summary>
    public class CheckMemberCouponCommand : BaseCommand, IRequest<BaseResult<bool>>
    {

        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }

        /// <summary>
        /// Gets the activity identifier.
        /// </summary>
        /// <value>The activity identifier.</value>
        public int ActivityId { get; private set; }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.API.Application.Commands.CreateMemberCouponCommand"/> class.
        /// </summary>
        /// <param name="ActivityId">Activity identifier.</param>
        /// <param name="Mobile">Mobile.</param>
        public CheckMemberCouponCommand(int ActivityId, string Mobile)
        {
            this.Mobile = Mobile;
            this.ActivityId = ActivityId;
        }
    }

}
