﻿using Casamiel.API.Application.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Commands
{
	/// <summary>
	/// 第三方订单提货
	/// </summary>
	public class ThirdOrderPickupCommand:IRequest<Zmodel>
	{
		/// <summary>
		/// 
		/// </summary>
		public string OrderCode { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public int Source { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderCode">订单号</param>
		/// <param name="source">订单来源，1 美团 2 饿了么 3 京东 4 滴滴 5天猫</param>
		public ThirdOrderPickupCommand(string orderCode,int source)
		{
			OrderCode = orderCode;
			Source = source;
		}
	}
}
