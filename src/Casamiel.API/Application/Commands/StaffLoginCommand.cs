﻿using System;
using Casamiel.API.Infrastructure.Middlewares;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.API.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
	public sealed class StaffLoginCommand : BaseCommand, IRequest<BaseResult<TokenEntity>>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RegistrationId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int LogType { get; set; } = 5;


    }
}
