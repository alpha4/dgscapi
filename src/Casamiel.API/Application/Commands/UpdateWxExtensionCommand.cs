﻿using System;
using MediatR;
namespace Casamiel.API.Application.Commands
{
    /// <summary>
    /// Update wx extension command.
    /// </summary>
    public sealed class UpdateWxExtensionCommand:IRequest<bool>
    {
        /// <summary>
        /// Gets the shop identifier.
        /// </summary>
        /// <value>The shop identifier.</value>
        public int WxId { get; private set; }
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
        /// <summary>
        /// Gets the card no.
        /// </summary>
        /// <value>The card no.</value>
        public string CardNO { get; private set; }
        /// <summary>
        /// </summary>
        /// <param name="WxId">Wx identifier.</param>
        /// <param name="Mobile">Mobile.</param>
        public UpdateWxExtensionCommand(int WxId,string Mobile)
        {
            this.WxId = WxId;
            this.Mobile = Mobile;
        }
        /// <summary>
        /// </summary>
        /// <param name="Mobile">Mobile.</param>
        /// <param name="CardNO">Card no.</param>
        public UpdateWxExtensionCommand(string Mobile,string CardNO)
        {
            this.CardNO = CardNO;
            this.Mobile = Mobile;
        }
    }
}
