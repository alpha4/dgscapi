﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;

namespace Casamiel.API.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class LogoutCommand:BaseCommand,IRequest<bool>
	{
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public  int LoginType { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="loginType">  1App,2Pc,3微信，4预定下单</param>
        public LogoutCommand(string mobile,int loginType)
		{
			Mobile = mobile;
			LoginType = loginType;
		}
	}
}
