﻿using System;
using Casamiel.Domain.Entity;
using MediatR;
namespace Casamiel.API.Application.Commands
{
    /// <summary>
    /// Create wx extension command.
    /// </summary>
    public sealed class CreateWxExtensionCommand : IRequest<bool>
    {
        /// <summary>
        /// Gets the wx info.
        /// </summary>
        /// <value>The wx info.</value>
        public WxOpenId WxInfo { get; private set; }
        
        /// <summary>
        /// Gets the shop identifier.
        /// </summary>
        /// <value>The shop identifier.</value>
        public int ShopId { get; private set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Application.Commands.CreateWxExtensionCommand"/> class.
        /// </summary>
        /// <param name="wxOpen">Wx open.</param>
        /// <param name="ShopId">Shop identifier.</param>
        public CreateWxExtensionCommand(WxOpenId wxOpen, int ShopId)
        {
            this.WxInfo = wxOpen;
            this.ShopId = ShopId;
        }
    }
}
