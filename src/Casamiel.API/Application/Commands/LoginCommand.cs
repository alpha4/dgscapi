﻿using System;
using Casamiel.API.Infrastructure.Middlewares;
using Casamiel.Domain.Entity;
using MediatR;

namespace Casamiel.API.Application.Commands
{
    /// <summary>
    /// Login command.
    /// </summary>
    public class LoginCommand:BaseCommand,IRequest<Tuple<Member,TokenEntity,string>>
    {
        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; set; }
        /// <summary>
        /// Gets or sets the type of the login.
        /// </summary>
        /// <value>The type of the login.</value>
        public int LoginType { get; set; }
        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>The source.</value>
        public int Source { get; set; }
        /// <summary>
        /// Gets or sets the registration identifier.
        /// </summary>
        /// <value>The registration identifier.</value>
        public string Registration_Id { get; set; }

        /// <summary>
        /// 邀请码
        /// </summary>
        public string InvitationCode { get; set; }
    }
}
