﻿using Casamiel.API.Infrastructure.Middlewares;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class CreateMemberAccessTokenComand:BaseCommand,IRequest<TokenEntity>
	{
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public int LogType { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public string Registration_Id { get; private set; }

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="mobile"></param>
		/// <param name="loginType"></param>
		/// <param name="registration_Id"></param>
		public CreateMemberAccessTokenComand(string mobile,int loginType,string registration_Id)
		{
			Mobile = mobile;
			LogType = loginType;
			Registration_Id = registration_Id;
		}
	}
}
