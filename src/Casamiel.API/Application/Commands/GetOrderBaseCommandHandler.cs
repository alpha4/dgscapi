﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.API.Application.Models.Store;
using MediatR;
namespace Casamiel.API.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class GetOrderBaseCommandHandler : IRequestHandler<GetOrderBaseCommand, OrderBaseRsp>
    {
        private readonly IStoreApiService _storeApiService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="storeApiService"></param>
        public GetOrderBaseCommandHandler(IStoreApiService storeApiService) {
            _storeApiService = storeApiService;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<OrderBaseRsp> Handle(GetOrderBaseCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            return await _storeApiService.GetOrderByOrderCodeAsync(request.OrderCode, request.Mobile).ConfigureAwait(false);
        }
    }
}
