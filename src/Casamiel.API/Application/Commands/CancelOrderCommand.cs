﻿using Casamiel.API.Application.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class CancelOrderCommand:IRequest<Zmodel>
	{
		/// <summary>
		/// 订单号
		/// </summary>
		public string OrderCode { get; private set; }

		/// <summary>
		/// 订单来源，0,默认1 美团 2 饿了么 3 京东 4 滴滴 5天猫
		/// </summary>
		public int Source { get; private set; }

		/// <summary>
		/// 卡号
		/// </summary>
		public string CardNO { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public string ReasonCode { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public string Reason { get; private set; }

        /// <summary>
        ///是否退款
        /// </summary>
        public bool IsRefund { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderCode">订单号</param>
		/// <param name="source">订单来源，1 美团 2 饿了么 3 京东 4 滴滴 5天猫</param>
		/// <param name="reasoncode"></param>
		/// <param name="reason"></param>
        /// <param name="IsRefund"></param>
		public CancelOrderCommand(string orderCode,int source,string reasoncode,string reason,bool IsRefund=false)
		{
			Source = source;
			OrderCode = orderCode;
			ReasonCode = reasoncode;
			Reason = reason;
            this.IsRefund = IsRefund;
		}
		/// <summary>
		/// 自有订单取消
		/// </summary>
		/// <param name="orderCode"></param>
		 
		/// <param name="cardNO"></param>
		public CancelOrderCommand(string orderCode, string cardNO)
		{
			Source = 0;
			OrderCode = orderCode;
			CardNO = cardNO;
		}
	}
}
