﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.API.Application.Models;
using Casamiel.Common;
using Casamiel.Domain.Entity;
using MediatR;

namespace Casamiel.API.Application.Commands
{
	/// <summary>
	/// 创建支付请求
	/// </summary>
	public class CreatePaymentRequestCommand:BaseCommand,IRequest<BasePaymentRsp>
	{
		/// <summary>
		/// 
		/// </summary>
		public PaymentRecord PaymentInfo { get; private set; }

		/// <summary>
		/// openid 
		/// </summary>
		public int Id { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public PaymentScenario PaymentScenario { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="paymentRecord"></param>
		/// <param name="Id"></param>
        /// <param name="scenario"></param>
		public CreatePaymentRequestCommand(PaymentRecord paymentRecord,int Id , PaymentScenario scenario)
		{
			this.PaymentInfo = paymentRecord;
            this.Id = Id;
            this.PaymentScenario = scenario;

        }
	}
}
