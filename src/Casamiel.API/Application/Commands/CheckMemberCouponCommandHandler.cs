﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.API.Application.Models;
using Casamiel.API.Application.Services;
using Casamiel.Application;
using Casamiel.Common;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Response;
using Enyim.Caching;
using MediatR;
using Microsoft.Extensions.Options;

namespace Casamiel.API.Application.Commands
{
    /// <summary>
    /// Check member coupon command handler.
    /// </summary>
    public sealed class CheckMemberCouponCommandHandler : IRequestHandler<CheckMemberCouponCommand, BaseResult<bool>>
    {
        private readonly IUserLogService _userLogService;
        //private readonly IMemcachedClient _memcachedClient;
        private readonly IMemberCouponRecordService _memberCouponRecordService;
        private readonly IIcApiService _icApiService;
        private readonly IMemberCardService _memberCardService;

        private readonly List<ActivityInfo> _activityInfos;

        // private readonly string _memcachedPrex;
        // private readonly List<ActivityInfo> _activityInfos;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userLogService"></param>
        /// <param name="optionsSnapshot"></param>
        /// <param name="memberCardService"></param>
        /// <param name="memberCouponRecordService"></param>
        /// <param name="icApiService"></param>
        public CheckMemberCouponCommandHandler( IUserLogService userLogService, IOptionsSnapshot<List<ActivityInfo>> optionsSnapshot,
                                                IMemberCardService memberCardService, IMemberCouponRecordService memberCouponRecordService, IIcApiService icApiService)
        {
            if (optionsSnapshot is null) {
                throw new ArgumentNullException(nameof(optionsSnapshot));
            }
            // _memcachedPrex = casamielSettings.Value.MemcachedPre;
            _userLogService = userLogService;
           // _memcachedClient = memcachedClient;
            _icApiService = icApiService;
            _memberCouponRecordService = memberCouponRecordService;
            //_activityInfos = optionsSnapshot.Value;
            _memberCardService = memberCardService;
            _activityInfos = optionsSnapshot.Value;
        }


        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<BaseResult<bool>> Handle(CheckMemberCouponCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            
            var activityinfo = _activityInfos.Find(c => c.Id == request.ActivityId);
            if (activityinfo == null) {
                return new BaseResult<bool>(false, 0, "");
            }
            var mycouponcount = await _memberCouponRecordService.GetCountAsync<ICasaMielSession>(request.ActivityId, request.Mobile, activityinfo.Flag).ConfigureAwait(false);
            if (mycouponcount < activityinfo.GetCount)
            {
                return new BaseResult<bool>(true, 0, "");
            }
            return new BaseResult<bool>(false, 0, "");
        }
    }
}
