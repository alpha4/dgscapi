﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.API.Application.Models.Store;
using MediatR;

namespace Casamiel.API.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class GetOrderBaseCommand:IRequest<OrderBaseRsp>
    {
        /// <summary>
        /// 
        /// </summary>
        public string OrderCode { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Mobile"></param>
        /// <param name="OrderCode"></param>
        public GetOrderBaseCommand(string Mobile,string OrderCode)
        {
            this.Mobile = Mobile;
            this.OrderCode = OrderCode;
        }
    }
}
