﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.API.Application.Models;
using Casamiel.API.Application.Services;
using Casamiel.Application;
using Casamiel.Common.Extensions;
using Casamiel.Domain;
using MediatR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.API.Application.Commands
{

    /// <summary>
    /// Un bind member card command.
    /// </summary>
    public class UnBindMemberCardCommand : BaseCommand, IRequest<Zmodel>
    {
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        [JsonIgnore]
        public string Mobile { get; set; }
        /// <summary>
        /// Gets or sets the card no.
        /// </summary>
        /// <value>The card no.</value>
        [Required()]
        [StringLength(16, MinimumLength = 6, ErrorMessage = "卡号长度6-16位")]
        public string Cardno { get; set; }
        /// <summary>
        /// Gets or sets the yzm.
        /// </summary>
        /// <value>The yzm.</value>
        [Required()]
        public string Yzm { get; set; }
        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>The source.</value>
        public int Source { get; set; }

    }
}
