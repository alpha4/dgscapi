﻿using System;
using System.ComponentModel.DataAnnotations;
using Casamiel.API.Application.Models;
using MediatR;
using Newtonsoft.Json;

namespace Casamiel.API.Application.Commands
{
    /// <summary>
    /// Gift card charge command.
    /// </summary>
    public sealed class GiftCardReChargeCommand:BaseCommand,IRequest<Zmodel>
    {
        /// <summary>
        /// 卡号
        /// </summary>
        public string Cardno { get; private set; }
		/// <summary>
		/// 充值码
		/// </summary>

		public string Qrcode { get; private set; }
        
		/// <summary>
		/// Gets or sets the mobile.
		/// </summary>
		/// <value>The mobile.</value>
		 
        public string Mobile { get; private set; }
        /// <summary>
        /// Gets the source.
        /// </summary>
        /// <value>The source.</value>
        public int Source { get; private set; }


        /// <summary>
        /// 
        /// </summary>
        public bool IsGift { get; private set; }

        
        /// <summary>
        /// </summary>
        /// <param name="cardNO">Card no.</param>
        /// <param name="qrcode">Qrcode.</param>
        /// <param name="mobile">Mobile.</param>
        /// <param name="Source">Source.</param>
        /// <param name="IsGift"></param>
        public GiftCardReChargeCommand(string cardNO, string qrcode, string mobile, int Source, bool IsGift = true)
        {
            Cardno = cardNO;
            Qrcode = qrcode;
            Mobile = mobile;
            this.Source = Source;
            this.IsGift = IsGift;
        }
    }
}
