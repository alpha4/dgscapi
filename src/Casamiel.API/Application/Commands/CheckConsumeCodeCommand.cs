﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.API.Application.Models;
using MediatR;

namespace Casamiel.API.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class CheckConsumeCodeCommand:BaseCommand,IRequest<Zmodel>
	{
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public string CardNO { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public string ConsumeCode { get; private set; }
		/// <summary>
        /// Gets the source.
        /// </summary>
        /// <value>The source.</value>
        public int Source { get; private set; }

		/// <summary>
        /// 
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="cardNO"></param>
        /// <param name="consumeCode"></param>
        /// <param name="Source"></param>
        public CheckConsumeCodeCommand(string mobile,string cardNO,string consumeCode,int Source)
		{
			this.CardNO = cardNO;
			this.Mobile = mobile;
			this.ConsumeCode = consumeCode;
            this.Source =Source;
		}

	}
}
