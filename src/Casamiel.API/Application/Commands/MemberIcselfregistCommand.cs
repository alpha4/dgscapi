﻿using Casamiel.API.Application.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class MemberIcselfregistCommand : BaseCommand, IRequest<Zmodel>
	{
		/// <summary>
		/// 手机号
		/// </summary>
		public string Mobile { get; private set; }
        /// <summary>
        ///来源，11、微信；15、手机APP(默认)；16、PAD
        /// </summary>
        /// <value>The data source.</value>
        public int Source { get; private set; }
      /// <summary>
      /// </summary>
      /// <param name="mobile">Mobile.</param>
      /// <param name="Source">Source.</param>
        public MemberIcselfregistCommand(string mobile,int Source)
		{
			Mobile = mobile;
            this.Source = Source;
		}
         
    }
}
