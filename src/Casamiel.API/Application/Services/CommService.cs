﻿using Casamiel.API.Application.Interface;
using Casamiel.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class CommService : ICommonService
    {
        private readonly IicApiService _iicApiService;
        public readonly IMemberCardService  _memberCard;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="iicApiService"></param>
        public CommService(IicApiService iicApiService, IMemberCardService memberCard,IMemberService  memberService)
        {
            _iicApiService = iicApiService;
            _memberCard = memberCard;
        }

        public IMemberCardService MemberService =>_memberCard;

        IicApiService ICommonService.ICService => _iicApiService;

		public IMemberCardService MemberCardService => _memberCard;
	}
}
