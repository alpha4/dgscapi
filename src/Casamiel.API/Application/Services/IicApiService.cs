﻿using Casamiel.API.Application.Models;
using Casamiel.API.Application.Models.ICModels;
using Casamiel.Domain.Request;
using Casamiel.Domain.Request.IC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Services
{
    /// <summary>
    /// 郑工接口服务
    /// </summary>
    public interface IIcApiService
    {
        /// <summary>
        /// Gets the card balance.
        /// </summary>
        /// <returns>The card balance.</returns>
        /// <param name="cardnum">Cardnum.</param>
        /// <param name="datasource"></param>
        Task<Zmodel> GetCardBalance(string cardnum, int datasource );
        /// <summary>
        /// 查询会员卡余额
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="datasource">来源11、微信；15、手机APP(默认)；16、PAD</param>
        /// <returns></returns>
        Task<Zmodel> Iclessquery(string cardno, int datasource);

        /// <summary>
        /// 查询会员卡消费和充值记录
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <param name="datasource"></param>
        /// <returns></returns>
        Task<Zmodel> GetIcRecord(string cardno, DateTime startdate, DateTime enddate ,int datasource);

        /// <summary>
        /// 查询会员卡消费和充值记录
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<ZmodelExt> IcRecordQuery(IcRecordqueryReq data);
        /// <summary>
        /// 获取操作记录
        /// </summary>
        /// <param name="cardno"></param>
        /// <returns></returns>
        Task<Zmodel> Icstatequery(string cardno);

        /// <summary>
        /// 查询会员券
        /// </summary>
        /// <param name="cardno"></param>
        /// <returns></returns>
        Task<Zmodel> icticket(string cardno);

        /// <summary>
        /// 查询会员券
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<ZmodelExt> Geticticket(Domain.Request.GetTicticketReq data);
        /// <summary>
        /// 会员卡充值
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="chargemoney"></param>
        /// <param name="paytype">paytype：支付方式（1、微信；2、支付宝；3、会员卡；4、银行卡；5、券）</param>
        /// <param name="paytradeno"></param>
		/// <param name="payid"></param>
		/// <param name="shopid"></param>
        /// <returns></returns>
        Task<Zmodel> IcCharge(string cardno, double chargemoney, int paytype, string paytradeno, string payid, long? shopid = null);

        /// <summary>
        /// 充值
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="chargemoney"></param>
        /// <param name="paytype"></param>
        /// <param name="paytradeno"></param>
        /// <returns></returns>
        Zmodel IcChargeSync(string cardno, double chargemoney, int paytype, string paytradeno);
        /// <summary>
        /// 生成会员消费码
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="datasource"></param>
        /// <returns></returns>
        Task<Zmodel> Geticconsumecode(string cardno, int datasource);

        /// <summary>
        /// 检查会员消费码处理状态
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="consumecode"></param>
        /// <param name="datasource"></param>
        /// <returns></returns>
        Task<Zmodel> Icconsumecheck(string cardno, string consumecode, int datasource);

        /// <summary>
        /// 绑定会员卡
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="mobile"></param>
        /// <param name="datasource"></param>
        /// <returns></returns>
        Task<Zmodel> Icregist(string cardno, string mobile, int datasource);
        /// <summary>
        /// 注册虚拟会员卡
        /// </summary>
        /// <param name="mobile"></param>
        ///<param name="datasource">来源，11、微信；15、手机APP(默认)；16、PAD</param>
        /// <returns></returns>
        Task<Zmodel> Icselfregist(string mobile, int datasource);
        /// <summary>
        /// 
        /// 解除手机绑定会员卡
        /// http://xxx.xxx.com:port/icapi/unicregist；
        /// <paramref name="cardno"/>
        /// <paramref name="mobile"/>
        /// <paramref name="datasource"/>
        /// </summary>
        Task<Zmodel> Unicregist(string cardno, string mobile, int datasource);

        /// <summary>
        /// 会员卡订单清单查询
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="mobile"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <returns></returns>
        Task<Zmodel> IcConsumeList(string cardno, string mobile, string startdate, string enddate);
        /// <summary>
        /// 查询订单处理状态
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="tradeno"></param>
        /// <returns></returns>
        Task<Zmodel> IcConsumeStatus(string cardno, string tradeno);

        /// <summary>
        /// 二维码查询
        /// </summary>
        /// <param name="cardno">卡号</param>
        /// <param name="mobile">手机号不为空</param>
        /// <param name="qrcode">二维码</param>
        /// <returns></returns>
        Task<Zmodel> IcQrcodeQuery(string cardno, string mobile, string qrcode);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="qrcode">二维码</param>
        /// <param name="qrcodeno"></param>
        /// <returns></returns>
        Task<Zmodel> icqrcodequery(string qrcode, string qrcodeno);

        /// <summary>
        /// 二维码使用
        /// </summary>
        /// <param name="cardno">卡号</param>
        /// <param name="mobile">手机号不为空</param>
        /// <param name="qrcode">二维码</param>
        /// <param name="datasource">datasource：可选项，来源，11、微信；15、手机APP(默认)；16、PAD</param>
        /// <returns></returns>
        Task<Zmodel> IcqrcodeVerify(string cardno, string mobile, string qrcode, int datasource);

        /// <summary>
        /// 查价格
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<ICPriceRoot> Icprice(IcPriceReq data);
        /// <summary>
        /// 产品库存
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<Domain.Response.ICProductStockQueryRoot> ProductStockQuery(IcPriceReq data);

        /// <summary>
        /// 产品库存
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<Zmodel> ProductstockqueryAsync(IcPriceReq data);
        /// <summary>
        /// 实体卡状态调整接
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<Zmodel> IcRealCardAdjust(CardStateReq data);
        /// <summary>
        ///  刷新会员卡信息
        /// </summary>
        /// <returns>The icrefresh.</returns>
        /// <param name="data">Data.</param>
        Task<Zmodel> Icrefresh(CardRefreshRequest data);
        /// <summary>
        /// 消费
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<Zmodel> Icconsume(Domain.Request.IcconsumeReq data);

        /// <summary>
        /// 查看订单状态
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<Zmodel> Icorderstate(IcorderstateReq data);

        /// <summary>
        /// 消费退货
        /// </summary>
        /// <returns></returns>
        Task<Zmodel> Icconsumeback(IcConsumeBackReq data);

        /// <summary>
        /// 门店核销
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<Zmodel> Icorderpickup(IcorderpickupReq data);

        /// <summary>
        /// 支付
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<Zmodel> Icconsumepay(IcConsumepayReq data);

        /// <summary>
        /// Icchargelist the specified cardno and phoneno.
        /// </summary>
        /// <returns>The icchargelist.</returns>
        /// <param name="cardno">Cardno.</param>
        /// <param name="phoneno">Phoneno.</param>
        Task<Zmodel> IcChargeList(string cardno, string phoneno);

        /// <summary>
        /// 二维码生成
        /// </summary>
        /// <returns>The icqrcodecreate.</returns>
        /// <param name="data">Data.</param>
        Task<Zmodel> Icqrcodecreate(IcQrcodeCreateReq data);

        /// <summary>
        /// 充值退款
        /// </summary>
        /// <returns>The icchargeback.</returns>
        /// <param name="data">Data.</param>
        Task<Zmodel> Icchargeback(CardChargeBackReq data);
        /// <summary>
        /// 第三方订货
        /// </summary>
        /// <returns>The thirdorder.</returns>
        /// <param name="req"></param>
        Task<Zmodel> ThirdOrder(IcconsumeReq req);
        /// <summary>
        /// 查询资料
        /// </summary>
        /// <returns>The basedataquery.</returns>
        /// <param name="data">Data.</param>
        Task<ZmodelExt> BasedataQuery(BasedataqueryReq data);

        /// <summary>
        /// 查询特定产品清单
        /// </summary>
        /// <returns>The productlist.</returns>
        /// <param name="req">Req.</param>
        Task<Zmodel> Productlist(ProductlistReq req);


        /// <summary>
        /// 团购提货单退货
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        Task<Zmodel> GroupSellback(GroupSellBackRequest req);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        Task<Zmodel> Liststatus(ListstatusReq req);
        /// <summary>
        /// Listsell the specified req.
        /// </summary>
        /// <returns>The listsell.</returns>
        /// <param name="req">Req.</param>

        Task<Zmodel> Listsell(ListSellReq req);

        //增加团购订货接口：
        /// <summary>
        /// Grouporder this instance.
        /// </summary>
        /// <returns>The grouporder.</returns>
        Task<Zmodel> Grouporder(GroupOrderReq data);

        /// <summary>
        /// 团购订货退货
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        Task<Zmodel> GroupOrderlback(GroupOrderBackRequest req);

        /// <summary>
        ///  团购订货编辑
        /// </summary>
        /// <returns>The groupordermodify.</returns>
        /// <param name="req">Req.</param>
        Task<Zmodel> Groupordermodify(GroupOrdermobifyRequest req);

        /// <summary>
        /// 团购订货删除
        /// </summary>
        /// <returns>The req.</returns>
        /// <param name="req">Req.</param>
        Task<Zmodel> Grouporderdelete(GrouporderdeleteReq req);


        /// <summary>
        /// 提货单退货
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        Task<Zmodel> IcSellback(IcSellBackRequest req);

        /// <summary>
        /// 团购订单查询接口
        /// </summary>
        /// <returns>The listquery.</returns>
        /// <param name="data">Data.</param>
        Task<ZmodelExt> Listquery(ListQueryReq data);

        /// <summary>
        /// 单据明细查询
        /// </summary>
        /// <returns>The listdetail.</returns>
        /// <param name="req"></param>
        Task<Zmodel> Listdetail(ListDetailReq req);

        /// <summary>
        /// Productdetail the specified data.
        /// </summary>
        /// <returns>The productdetail.</returns>
        /// <param name="data">Data.</param>
        Task<Zmodel> Productdetail(ProductDetailReq data);
        /// <summary>
        /// 第三方订单退货
        /// </summary>
        /// <returns>The thirdorderback.</returns>
        /// <param name="data">Data.</param>
        Task<Zmodel> ThirdOrderBack(ThirdOrderBackReq data);
        /// <summary>
        ///开具发票明细
        /// </summary>
        /// <returns>The invoice.</returns>
        /// <param name="data">Data.</param>
        Task<Zmodel> Invoice(InvoiceReq data);
        /// <summary>
        /// 单据发票查询
        /// </summary>
        /// <returns>The invoicequery.</returns>
        /// <param name="data">Data.</param>
        Task<Zmodel> Invoicequery(InvoiceQueryReq data);

        /// <summary>
        /// 库存推送计划
        /// </summary>
        /// <returns>The stockpushplan.</returns>
        /// <param name="data">Data.</param>
        Task<Zmodel> Stockpushplan(StockpushPlanReq data);

        /// <summary>
        ///  通过手机号赠送会员券
        /// </summary>
        /// <returns>The icticket.</returns>
        /// <param name="data">Data.</param>
        Task<Zmodel> Icticketlargess(AddTicketsReq data);

        /// <summary>
        ///撤销通过手机号赠送的会员券
        /// </summary>
        /// <returns>The icticketlargesscancel.</returns>
        /// <param name="data">Data.</param>
        Task<Zmodel> Icticketlargesscancel(TicketLargessCancelReq data);

    }
}
