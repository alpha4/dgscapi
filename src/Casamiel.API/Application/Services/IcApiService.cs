﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using Casamiel.API.Application.Models;
using Casamiel.API.Application.Models.ICModels;
using Casamiel.Common;
using Casamiel.Common.Utilities;
using Casamiel.Domain.Request;
using Casamiel.Domain.Request.IC;
using Casamiel.Domain.Response;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

using Newtonsoft.Json;

using NLog;

namespace Casamiel.API.Application.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class IcApiService : IIcApiService
    {
        // private HttpClient _client;
        private readonly string appkey = "";
        private readonly string signKey = "";
        private readonly string token = "";
        private string appurl = "";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private SimpleStringCypher sc;
        private readonly IcServiceSettings _settings;
        private readonly NLog.ILogger logger = LogManager.GetLogger("IicApiService");
        private readonly IHttpClientFactory _clientFactory;
        private readonly JsonSerializerSettings jsonSerializerSettings;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="clientFactory"></param>
        public IcApiService(IOptionsSnapshot<IcServiceSettings> settings, IHttpContextAccessor httpContextAccessor, IHttpClientFactory clientFactory)
        {
            if (settings == null) {
                throw new ArgumentNullException(nameof(settings));
            }
            _clientFactory = clientFactory;
            _settings = settings.Value;
            _httpContextAccessor = httpContextAccessor;

            appkey = _settings.appKey;
            signKey = _settings.signKey;
            token = _settings.token;

            //appurl = _settings.appUrl;// http://www.crowncake.cn:12345/";
            sc = new SimpleStringCypher(appkey);

            jsonSerializerSettings = new JsonSerializerSettings {
                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
            };
        }



        /// <summary>
        /// 签名字符串
        /// </summary>
        /// <param name="prestr">需要签名的字符串</param>
        /// <param name="keyPre">密钥：前置密钥</param>
        /// <param name="keyAfter">密钥：后置密钥</param>
        /// <param name="inputcharset">编码格式</param>
        /// <returns>签名结果</returns>
        public static string Sign(string prestr, string keyPre, string keyAfter, string inputcharset = "utf-8")
        {
            StringBuilder sb = new StringBuilder(32);

            prestr = keyPre + prestr + keyAfter;
            byte[] t;
            using (MD5 md5 = new MD5CryptoServiceProvider()) {
                t = md5.ComputeHash(Encoding.GetEncoding(inputcharset).GetBytes(prestr));
            }

            for (int i = 0; i < t.Length; i++) {
                sb.Append(t[i].ToString("x").PadLeft(2, '0'));
            }
            return sb.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cardnum"></param>
        /// <param name="datasource">11、微信；15、手机APP(默认)；16、PAD</param>
        /// <returns></returns>
        public async Task<Zmodel> GetCardBalance(string cardnum, int datasource)
        {
            // var content = "{\"wxopenid\":\"\",\"phoneno\":\"\",\"cardno\":\"" + cardnum + "\",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\"}";
            var data = new { cardno = cardnum, datasource = datasource, timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) };
            //logger.Trace($"查余额：{content}");
            var model = await PostAsync(JsonConvert.SerializeObject(data), "icapi/iclessquery").ConfigureAwait(false);
            //logger.Trace($"查余额：{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// 消费记录查询
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        /// <remarks>
        /// {
        /// "phoneno":"",
        /// "cardno":"xxxx”,
        /// "wxopenid”:”xxxx”,
        /// "startdate”:”2016-01-01”,
        /// "enddate”:”2016-01-31 23:59:59”,
        /// "pagesize”:10,
        /// "pageindex”:1,
        /// "timestamp”:”2016-02-16 12:01:02”
        /// }
        /// </remarks>
        public async Task<ZmodelExt> IcRecordQuery(IcRecordqueryReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(nameof(data));
            }

            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            //logger.Trace($"icrecordquery:{content}");
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            return await PostExtAsync(content, "icapi/icrecordquery").ConfigureAwait(false);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ICPriceRoot> Icprice(IcPriceReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(nameof(data));
            }

            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            //Console.WriteLine(content);
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            var tmodel = await PostAsync(content, "icapi/icprice").ConfigureAwait(false);
            //Console.WriteLine(tmodel.content);
            var aa = JsonConvert.DeserializeObject<ICPriceRoot>(tmodel.content);
            //Console.WriteLine(JsonConvert.SerializeObject(aa));
            return aa;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ICProductStockQueryRoot> ProductStockQuery(IcPriceReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(nameof(data));
            }

            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            // Console.WriteLine(content);
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            var tmodel = await PostAsync(content, "icapi/productstockquery").ConfigureAwait(false);
            logger.Trace($"{content},{JsonConvert.SerializeObject(tmodel)}");
            //Console.WriteLine(JsonConvert.SerializeObject(tmodel));
            var root = JsonConvert.DeserializeObject<ICProductStockQueryRoot>(tmodel.content);
            return root;
        }


        /// <summary>
        /// Productstockqueries the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="data">Data.</param>
		public async Task<Zmodel> ProductstockqueryAsync(IcPriceReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(nameof(data));
            }

            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            // Console.WriteLine(content);
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            var tmodel = await PostAsync(content, "icapi/productstockquery").ConfigureAwait(false);
            //Console.WriteLine(JsonConvert.SerializeObject(tmodel));
            //var root = JsonConvert.DeserializeObject<ICProductStockQueryRoot>(tmodel.content);
            return tmodel;
        }

        /// <summary>
        /// 获取操作记录
        /// </summary>
        /// <param name="cardno"></param>
        /// <returns></returns>
        public async Task<Zmodel> Icstatequery(string cardno)
        {
            //            wxopenid：微信id（可为空）
            //cardno：卡号（可为空）
            //phoneno：手机号（可为空）
            //以上三个数据必选一个
            //数据content示例：
            //{
            //“wxopenid”:”xxxx”,
            //“cardno”:”xxxx”,
            //“phoneno”:”xxxx”,
            //”timestamp”:”2016 - 02 - 16 12:01:02”
            //}
            var content = "{\"wxopenid\":\"\",\"phoneno\":\"\",\"cardno\":\"" + cardno + "\",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            return await PostAsync(content, "icapi/icstatequery").ConfigureAwait(false);
        }

        private void SetAuthorizationHeader(HttpRequestMessage requestMessage)
        {
            var authorizationHeader = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            if (!string.IsNullOrEmpty(authorizationHeader)) {
                requestMessage.Headers.Add("Authorization", new List<string>() { authorizationHeader });
            }
        }
        /// <summary>
        ///     查询会员券
        /// </summary>
        /// <param name="cardno"></param>
        /// <returns></returns>
        public async Task<Zmodel> icticket(string cardno)
        {
            //            1、    发送数据内容：
            //cardno：卡号（可为空）
            //wxopenid：微信id（可为空）
            //phoneno：手机号（可为空）
            //以上三个数据必须至少提供一个
            //数据content示例：
            //{
            //“cardno”:”xxxx”,
            //”wxopenid”:”xxxx”,
            //”phoneno”:”xxxx”,
            //”timestamp”:”2016 - 02 - 16 12:01:02”
            //}
            var content = "{\"wxopenid\":\"\",\"phoneno\":\"\",\"cardno\":\"" + cardno + "\",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            return await PostAsync(content, "icapi/icticket").ConfigureAwait(false);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ZmodelExt> Geticticket(GetTicticketReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(nameof(data));
            }

            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            return await PostExtAsync(content, "icapi/icticket").ConfigureAwait(false);
        }

        /// <summary>
        /// Posts the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="content">Content.</param>
        /// <param name="servicename">Servicename.</param>
        private async Task<Zmodel> PostAsync(string content, string servicename)
        {
            var _sc = new SimpleStringCypher(appkey);
            var _content = _sc.Encrypt(content);
            var data = "";
            string signData = Sign("token" + token + "content" + content, signKey, signKey);
            // System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();

            List<KeyValuePair<String, String>> paramList = new List<KeyValuePair<String, String>> {
                new KeyValuePair<String, String>("token", token),
                new KeyValuePair<String, String>("sign", signData),
                new KeyValuePair<String, String>("content", _content)
            };




            Random a = new Random();
            var aaa = a.NextDouble();
            HttpClient client;
            if (aaa < 0.8) {
                client = _clientFactory.CreateClient("icapi");
                appurl = _settings.appUrls[0].Url;
            } else {
                client = _clientFactory.CreateClient("icapi1");
                appurl = _settings.appUrls[1].Url;
            }
            var begin = DatetimeUtil.GetUnixTime(DateTime.Now, TimePrecision.Millisecond);
            try {
                //watch.Start();
                using (var request = new HttpRequestMessage(HttpMethod.Post, servicename) {
                    Content = new FormUrlEncodedContent(paramList)
                }) {
                    using (var response = await client.SendAsync(request).ConfigureAwait(false)) {
                        //watch.Stop();
                        var end = DatetimeUtil.GetUnixTime(DateTime.Now, TimePrecision.Millisecond);
                        if ((end - begin) > 100) {
                            //if (servicename != "icapi/productstockquery") {
                                logger.Warn($"data:{content},url：{appurl + servicename}，执行时间{ end - begin}毫秒");
                                Console.WriteLine($"url：{appurl + servicename}，执行时间{ end - begin}毫秒");
                            //}
                        }
                        string result = "";
                        if (response.IsSuccessStatusCode) {
                            using (var s = await response.Content.ReadAsStreamAsync().ConfigureAwait(false)) {
                                System.Xml.XmlDocument xmld = new System.Xml.XmlDocument();
                                xmld.Load(s);
                                result = xmld.InnerText;
                            }
                            //result = await response.Content.ReadAsStringAsync();

                            if (string.IsNullOrEmpty(result)) {
                                return new Zmodel { code = 9999, content = "", msg = "接口出错了" };
                            }
                        } else {
                            logger.Error($"{servicename},{response.StatusCode}");
                            return new Zmodel { code = 999, content = "", msg = "出错了" };
                        }

                        Zmodel jdata;
                        //try
                        //{
                        //    response = await _client.PostAsync(new Uri(appurl + servicename), new FormUrlEncodedContent(paramList));
                        if (response.StatusCode == System.Net.HttpStatusCode.OK) {
                            // data = response.Content.ReadAsStringAsync().Result;
                            //var dynamicdata = JsonConvert.DeserializeObject<dynamic>(result);
                            //string _data = dynamicdata.ToString();
                            jdata = JsonConvert.DeserializeObject<Zmodel>(result);
                            if (jdata.content.Trim().Length > 0) {
                                jdata.content = _sc.Decrypt(jdata.content);
                                // var psigndata = Sign("token" + token + "content" + jdata.content, signKey, signKey);
                            }
                        } else {
                            logger.Error($"StatusCode:{response.StatusCode},data:{content},url：{appurl},servicename:{servicename},data:{data}");
                            throw new ArgumentException(response.StatusCode.ToString());
                        }

                        return jdata;
                    }
                }



                // logger.Info(data);
            } catch (HttpRequestException ex) {
                logger.Error($"data:{content},url：{appurl},servicename:{servicename},data:{data}");
                logger.Error(ex.Message);
                throw;
            } catch (System.Net.Sockets.SocketException ex) {
                logger.Error($"data:{content},url：{appurl},servicename:{servicename},data:{data}");
                logger.Error(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Posts the ext async.
        /// </summary>
        /// <returns>The ext async.</returns>
        /// <param name="content">Content.</param>
        /// <param name="servicename">Servicename.</param>
        private async Task<ZmodelExt> PostExtAsync(string content, string servicename)
        {
            var _content = sc.Encrypt(content);
            string signData = Sign("token" + token + "content" + content, signKey, signKey);
            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            watch.Start();
            List<KeyValuePair<String, String>> paramList = new List<KeyValuePair<String, String>> {
                new KeyValuePair<String, String>("token", token),
                new KeyValuePair<String, String>("sign", signData),
                new KeyValuePair<String, String>("content", _content)
            };
            string data;
            using (var request = new HttpRequestMessage(HttpMethod.Post, servicename) {
                Content = new FormUrlEncodedContent(paramList)
            }) {
                var client = _clientFactory.CreateClient("icapi");

                try {
                    using (var response = await client.SendAsync(request).ConfigureAwait(false)) {
                        if (response.IsSuccessStatusCode) {
                            //data = await response.Content.ReadAsStringAsync();
                            using (var s = await response.Content.ReadAsStreamAsync().ConfigureAwait(false)) {
                                System.Xml.XmlDocument xml = new System.Xml.XmlDocument();
                                xml.Load(s);
                                data = xml.InnerText;
                            }
                        } else {
                            logger.Error(response.Content.ReadAsStringAsync().Result);
                            throw new ArgumentException(response.StatusCode.ToString());
                        }
                    }

                } catch (System.Net.Sockets.SocketException ex) {
                    logger.Error($"content:{content},url：{appurl + servicename}");
                    logger.Error(ex.Message);
                    throw;
                } catch (HttpRequestException ex) {
                    logger.Error($"content:{content},url：{appurl + servicename}");
                    logger.Error(ex.Message);
                    throw;
                }
            }
            //_ = new HttpResponseMessage();

            watch.Stop();
            if (watch.ElapsedMilliseconds > 100) {
                logger.Warn($"content:{content},url：{appurl + servicename}，执行时间{ watch.ElapsedMilliseconds}毫秒");
                Console.WriteLine($"url：{appurl + servicename}，执行时间{ watch.ElapsedMilliseconds}毫秒");
            }
            //try
            //{
            //    var dynamicdata = JsonConvert.DeserializeObject<dynamic>(data);
            //    data = dynamicdata.ToString();
            //}
            //catch (Exception ex)
            //{
            //    logger.Error($"url：{appurl},servicename:{servicename},data:{content}");
            //    logger.Error(ex.Message);
            //}
            //var reader = new Casamiel.Infrastructure.JsonReader();
            //var dic = reader.Read(data, "SendSmsResponse");
            //JsonReader reader1 = new JsonTextReader(new StringReader(data));

            //if (!string.IsNullOrEmpty(data))
            //{
            //    if (data.StartsWith('"'))
            //    {
            //        data = data.Substring(1, data.Length - 2).Replace("\\", "");
            //    }

            //}

            //logger.Trace(data);
            var jdata = JsonConvert.DeserializeObject<ZmodelExt>(data);
            if (jdata.content.Trim().Length > 0) {
                jdata.content = sc.Decrypt(jdata.content);
                // var psigndata = Sign("token" + token + "content" + jdata.content, signKey, signKey);
            }
            return jdata;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <param name="signData"></param>
        /// <param name="servicename"></param>
        /// <returns></returns>
        public Zmodel Post(string content, string signData, string servicename)
        {
            Zmodel jdata;
            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            watch.Start();
            List<KeyValuePair<String, String>> paramList = new List<KeyValuePair<String, String>> {
                new KeyValuePair<String, String>("token", token),
                new KeyValuePair<String, String>("sign", signData),
                new KeyValuePair<String, String>("content", content)
            };

            //HttpClientHandler handler = new HttpClientHandler();

            //HttpClient client = new HttpClient(handler);
            //MemoryStream ms = new MemoryStream();
            //HttpContent hc = new StreamContent(ms);


            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/html"));
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xhtml+xml"));
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("image/webp"));
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*", 0.8));
            //hc.Headers.Add("UserAgent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36");
            //hc.Headers.Add("Timeout", "120");
            //hc.Headers.Add("KeepAlive", "true");
            string result = "";
            using (var request = new HttpRequestMessage(HttpMethod.Post, servicename) {
                Content = new FormUrlEncodedContent(paramList)
            }) {
                var client = _clientFactory.CreateClient("icapi");

                var t = client.SendAsync(request);
                t.Wait();

                if (t.Result.IsSuccessStatusCode) {
                    var s = t.Result.Content.ReadAsStreamAsync();
                    //result = await response.Content.ReadAsStringAsync();
                    System.Xml.XmlDocument xmld = new System.Xml.XmlDocument();
                    xmld.Load(s.Result);
                    result = xmld.InnerText;
                }
            }

            //         var t = client.PostAsync(request);
            //         t.Wait();
            //var s =  t.Result.Content.ReadAsStreamAsync();
            //System.Xml.XmlDocument xml = new System.Xml.XmlDocument();
            //xml.Load(s.Result);
            //var data = xml.InnerText;
            //var t2 = t.Result.Content.ReadAsByteArrayAsync();
            //var data = Encoding.UTF8.GetString(t2.Result);
            //try
            //{
            //    var dynamicdata = JsonConvert.DeserializeObject<dynamic>(data);
            //    data = dynamicdata.ToString();
            //}
            //catch (Exception ex)
            //{
            //    logger.Error($"url：{appurl},servicename:{servicename},data:{data}");
            //    logger.Error(ex, "");
            //}
            jdata = JsonConvert.DeserializeObject<Zmodel>(result);
            if (jdata.content.Trim().Length > 0) {
                jdata.content = sc.Decrypt(jdata.content);
            }
            return jdata;
        }

        //http://xxx.xxx.com:port/icapi/iccharge；
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="chargemoney"></param>
        /// <param name="paytype">paytype：支付方式（1、微信；2、支付宝；3、会员卡；4、银行卡；5、券）</param>
        /// <param name="paytradeno"></param>
		/// <param name="payid"></param>
		/// <param name="shopid">门店id</param>
        /// <returns></returns>
        public async Task<Zmodel> IcCharge(string cardno, double chargemoney, int paytype, string paytradeno, string payid, long? shopid)
        {
            if (paytype == 4)//1和4都是微信支付
            {
                paytype = 1;
            }
            if (paytype == 6) {
                paytype = 2;
            }
            var content = "{\"wxopenid\":\"\",\"cardno\":\"" + cardno + "\",\"paytype\":" + paytype + ",\"paytradeno\":\"" + paytradeno + "\",\"payid\":\"" + payid + "\",\"payuser\":\"payuserid\",\"chargemoney\":" + chargemoney + ",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            if (shopid.HasValue) {
                content = "{\"wxopenid\":\"\",\"shopid\":" + shopid.Value + ",\"cardno\":\"" + cardno + "\",\"paytype\":" + paytype + ",\"paytradeno\":\"" + paytradeno + "\",\"payid\":\"" + payid + "\",\"payuser\":\"payuserid\",\"chargemoney\":" + chargemoney + ",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            }
            #region 示例

            /*    
2、    发送数据内容：
wxopenid：微信id
cardno：卡号（可为空）
chargemoney：充值金额
paytradeno：调用方支付唯一号
数据content示例：
{
”wxopenid”:”xxxx”,
“cardno”:”xxxx”,
”paytype”:”xxxx”,
”paytradeno”:”xxxxxxxxx”
“payuser”:”payuserid”,
”chargemoney”:xxx.xx,
”timestamp”:”2016 - 02 - 16 12:01:02”
}
            3、        返回数据内容：
tradeno：充值单号（退充时需要这个单号）
chargemoney：充值金额
largessmoney：充值赠送金额
point：充值积分
tickets：充值赠送的会员券明细
{
"code”:0,
”msg”:”处理成功”, 
”content”:”
{
"wxopenid":"xxxx",
"cardno":"xxxx",
"tradeno":"C13S1234G4567",
"chargemoney":xx.xx,
"largessmoney":x.xx,
"point":xxx,
"tickets":
[
{
"ticketname":"xxxx",
"ticketid":xxxx,
"je":xxx.xx,
"makedate","yyyy-mm-dd",
"startdate","yyyy-mm-dd",
"enddate","yyyy-mm-dd",
"description",""    
},
{
"ticketname":"xxxx",
"ticketid":xxxx,
"je":xxx.xx,
"makedate","yyyy-mm-dd",
"startdate","yyyy-mm-dd",
"enddate","yyyy-mm-dd",
"description",""    
}

]
}”,
"sign”:”9ade8ba90cf38745bea56398d235daae”
}*/
            #endregion
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            var zmodel = await PostAsync(content, "icapi/ICCharge").ConfigureAwait(false);
            logger.Trace($"充值:{JsonConvert.SerializeObject(zmodel)}");
            return zmodel;
        }

        /// <summary>
        /// Icchargeback the specified data.
        /// </summary>
        /// <returns>The icchargeback.</returns>
        /// <param name="data">Data.</param>
        public async Task<Zmodel> Icchargeback(CardChargeBackReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(nameof(data));
            }

            data.timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var zmodel = await PostAsync(content, "icapi/icchargeback").ConfigureAwait(false);
            logger.Trace($"充值退款:{JsonConvert.SerializeObject(zmodel)}");
            return zmodel;
        }
        /// <summary>
        /// IcChargeSync
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="chargemoney"></param>
        /// <param name="paytype">paytype：支付方式（1、微信；2、支付宝；3、会员卡；4、银行卡；5、券）</param>
        /// <param name="paytradeno"></param>
        /// <returns></returns>
        public Zmodel IcChargeSync(string cardno, double chargemoney, int paytype, string paytradeno)
        {
            #region 示例

            /*    
2、    发送数据内容：
wxopenid：微信id
cardno：卡号（可为空）
chargemoney：充值金额
paytradeno：调用方支付唯一号
数据content示例：
{
”wxopenid”:”xxxx”,
“cardno”:”xxxx”,
”paytype”:”xxxx”,
”paytradeno”:”xxxxxxxxx”
“payuser”:”payuserid”,
”chargemoney”:xxx.xx,
”timestamp”:”2016 - 02 - 16 12:01:02”
}
            3、        返回数据内容：
tradeno：充值单号（退充时需要这个单号）
chargemoney：充值金额
largessmoney：充值赠送金额
point：充值积分
tickets：充值赠送的会员券明细
{
"code”:0,
”msg”:”处理成功”, 
”content”:”
{
"wxopenid":"xxxx",
"cardno":"xxxx",
"tradeno":"C13S1234G4567",
"chargemoney":xx.xx,
"largessmoney":x.xx,
"point":xxx,
"tickets":
[
{
"ticketname":"xxxx",
"ticketid":xxxx,
"je":xxx.xx,
"makedate","yyyy-mm-dd",
"startdate","yyyy-mm-dd",
"enddate","yyyy-mm-dd",
"description",""    
},
{
"ticketname":"xxxx",
"ticketid":xxxx,
"je":xxx.xx,
"makedate","yyyy-mm-dd",
"startdate","yyyy-mm-dd",
"enddate","yyyy-mm-dd",
"description",""    
}

]
}”,
"sign”:”9ade8ba90cf38745bea56398d235daae”
}*/
            #endregion
            var content = "{\"wxopenid\":\"\",\"cardno\":\"" + cardno + "\",\"paytype\":" + paytype + ",\"paytradeno\":\"" + paytradeno + "\",\"payuser\":\"payuserid\",\"chargemoney\":" + chargemoney + ",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            var _content = sc.Encrypt(content);
            string signData = Sign("token" + token + "content" + content, signKey, signKey);
            var zmodel = Post(_content, signData, "icapi/ICCharge");
            logger.Trace($"{zmodel.content}");
            return zmodel;

        }
        /// <summary>
        /// Iclessquery the specified cardno.
        /// </summary>
        /// <returns>The iclessquery.</returns>
        /// <param name="cardno">Cardno.</param>
		/// <param name="datasource"></param>
        public async Task<Zmodel> Iclessquery(string cardno, int datasource)
        {
            //            wxopenid：微信id
            //cardno：卡号（可为空）
            //数据content示例：
            //{
            //”wxopenid”:”xxxx”,
            //“cardno”:”xxxx”,
            //”timestamp”:”2016 - 02 - 16 12:01:02”
            //}
            // var content = "{\"wxopenid\":\"\",\"cardno\":\"" + cardno + "\",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\"}";
            var data = new { cardno = cardno, datasource = datasource, timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) };
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            return await PostAsync(JsonConvert.SerializeObject(data), "icapi/iclessquery").ConfigureAwait(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <param name="datasource"></param>
        /// <returns></returns>
        public async Task<Zmodel> GetIcRecord(string cardno, DateTime startdate, DateTime enddate,int datasource)
        {
            //            wxopenid：微信id
            // cardno：卡号（可为空）
            //startdate：起始时间，最长允许为当前时间往前三个月
            //enddate：结束时间，如果没有这个参数或为null，则查询到当前时间
            //数据content示例：
            //{
            //”wxopenid”:”xxxx”,
            //“cardno”:”xxxx”,
            //”startdate”:”2016 - 01 - 01”,
            //”enddate”:”2016 - 01 - 31 23:59:59”,
            //”timestamp”:”2016 - 02 - 16 12:01:02”
            //}
            var content = "{\"wxopenid\":\"\",\"datasource\":" + datasource + ",\"cardno\":\"" + cardno + "\",\"startdate\":\"" + startdate.ToString("yyyy-MM-dd", CultureInfo.CurrentCulture) + "\",\"enddate\":\"" + enddate.ToString("yyyy-MM-dd 23:59:59", CultureInfo.CurrentCulture) + "\",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            return await PostAsync(content, "icapi/icrecordquery").ConfigureAwait(false);
        }
        /// <summary>
        ///    生成会员消费码
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="datasource"></param>
        /// <returns></returns>
        public async Task<Zmodel> Geticconsumecode(string cardno, int datasource)
        {
            #region
            /*
            1、    生成消费码地址：http://xxx.xxx.com:port/icapi/icconsumecode；
            2、    发送数据内容
            wxopenid：微信id
            cardno：卡号（可为空）
数据content示例：
{
”wxopenid”:”xxxx”,
"phoneno":""
“cardno”:”xxxx”
}
            3、    返回数据内容
{
“code”:0,
”msg”:”处理成功”,
"content":"
{
"wxopenid":"xxx",
"cardno":"xxx",
"consumecode":"123456789012345678",
"qrcodeurl":"http://*.*.*:port/images/abc.jpg",
"barcodeurl":"http://*.*.*:port/images/cde.jpg",
}"
“sign":"9ade8ba90cf38745bea56398d235daae"
}
            建议手机端同时显示条码和二维码
            说明：
A、    消费码生成后，直接显示在手机端，由门店收银系统扫描消费码来消费；
B、    消费码在手机端显示后，开始通过检查会员消费码接口，来轮询消费码使用情况；
C、    在查询到消费码已显示后，建议给顾客显示消费金额，或是超时后(60秒)，重新生成一个消费码。
*/
            #endregion
            var content = "{\"wxopenid\":\"\",\"phoneno\":\"\",\"cardno\":\"" + cardno + "\",\"datasource\":" + datasource + ",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            return await PostAsync(content, "icapi/icconsumecode").ConfigureAwait(false);
        }
        /// <summary>
        /// 检查会员消费码处理状态
        /// </summary>
        /// <param name="cardno">卡号</param>
        /// <param name="consumecode">消费码</param>
        /// <param name="datasource"></param>
        /// <returns></returns>
        public async Task<Zmodel> Icconsumecheck(string cardno, string consumecode, int datasource)
        {
            //            wxopenid：微信id
            //cardno：卡号（可为空）
            //consumecode：消费码
            //数据content示例：
            //{
            //”wxopenid”:”xxxx”,
            //"phoneno":"",
            //“cardno”:”xxxx”,
            //“consumecode”:”123456789012345678”
            //}
            var content = "{\"wxopenid\":\"\",\"phoneno\":\"\",\"cardno\":\"" + cardno + "\",\"datasource\":" + datasource + ",\"consumecode\":\"" + consumecode + "\",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            return await PostAsync(content, "icapi/icconsumecheck").ConfigureAwait(false);
        }
        /// <summary>
        /// 绑定会员卡
        /// </summary>
        /// <param name="cardno">卡号</param>
        /// <param name="mobile">手机号</param>
        /// <param name="datasource"></param>
        /// <returns></returns>
        public async Task<Zmodel> Icregist(string cardno, string mobile, int datasource)
        {
            var content = "{\"cardno\":\"" + cardno + "\",\"phoneno\":\"" + mobile + "\",\"datasource\":" + datasource + ",\"phonesms\":\"\",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            logger.Trace($"绑定会员卡：{content}");
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            var model = await PostAsync(content, "icapi/icregist").ConfigureAwait(false);
            logger.Trace($"绑定会员卡：{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// 会员卡订单清单查询
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="mobile"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <returns></returns>
        public async Task<Zmodel> IcConsumeList(string cardno, string mobile, string startdate, string enddate)
        {
            /*
             cardno：卡号，建议填写
phoneno：手机号，可不填，如果填写且也填写了卡号，服务端将判断手机号和卡号是否相符
wxopenid：微信id，可不填，如果填写且也填写了卡号，服务端将判断微信号和卡号是否相符
startdate：起始日期
enddate：结束日期

           数据content示例：
{
“cardno”:”xxxx”,
“phoneno”:”xxxx”,
”wxopenid”:”xxxx”,
”startdate”:”xxxx”,
“enddate”:”xxxx”
}*/
            var content = "{\"cardno\":\"" + cardno + "\",\"phoneno\":\"" + mobile + "\",\"wxopenid\":\"\",\"startdate\":\"" + startdate + "\",\"enddate\":\"" + enddate + "\",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            return await PostAsync(content, "icapi/icconsumelist").ConfigureAwait(false);

        }
        /// <summary>
        /// 注册会员卡
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="datasource">11、微信；15、手机APP(默认)；16、PAD</param>
        /// <returns></returns>
        public async Task<Zmodel> Icselfregist(string mobile, int datasource)
        {
            /*
            //            {
            //”phoneno”:”13912345678”,
            //“name”:”张三”,
            //“sex”:”0”,
            //“idtype”:”1”,
            //“idcard”:”330xxxxxxxxxxxxxxx”,
            //“birthday”:”1990 - 01 - 02”,
            //”phonesms”:”123654”,
            //”timestamp”:”2016 - 02 - 16 12:01:02”
            //}
            */
            var content = "{\"phoneno\":\"" + mobile + "\",\"datasource\":" + datasource + ",\"name\":\"1\",\"sex\":\"1\",\"idtype\":\"1\",\"birthday\":\"\",\"phonesms\":\"\",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            var zmodel = await PostAsync(content, "icapi/Icselfregist").ConfigureAwait(false);
            logger.Trace($"{JsonConvert.SerializeObject(zmodel)}");
            if (zmodel.code == 0) {
                logger.Trace($"[{mobile}]注册会员卡{zmodel.content}");
            }
            return zmodel;
        }
        /// <summary>
        /// 解绑会员卡
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="mobile"></param>
        /// <param name="datasource"></param>
        /// <returns></returns>
        public async Task<Zmodel> Unicregist(string cardno, string mobile, int datasource)
        {
            var content = "{\"cardno\":\"" + cardno + "\",\"phoneno\":\"" + mobile + "\",\"datasource\":" + datasource + ",\"phonesms\":\"\",\"timestamp\":\"" +
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            logger.Trace($"解绑会员卡{content}");
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            var model = await PostAsync(content, "icapi/unicregist").ConfigureAwait(false);
            logger.Trace($"解绑会员卡{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// 查询订单处理状态
        /// </summary>
        /// <param name="cardno">卡号</param>
        /// <param name="tradeno"></param>
        /// <returns></returns>
        public async Task<Zmodel> IcConsumeStatus(string cardno, string tradeno)
        {
            #region MyRegion
            /*
             1、    消费地址：http://xxx.xxx.com:port/icapi/icconsumestatus；
            2、    发送数据内容：
            cardno：卡号，建议填写
            phoneno：手机号，可不填，如果填写且也填写了卡号，服务端将判断手机号和卡号是否相符
            wxopenid：微信id，可不填，如果填写且也填写了卡号，服务端将判断微信号和卡号是否相符
            tradeno：交易号
            数据content示例：
            {
            “cardno”:”xxxx”,
            “phoneno”:”xxxx”,
            ”wxopenid”:”xxxx”,
            ”tradeno”:”xxxx”,
            }
            3、        返回数据内容：
            {
            “code”:0,
            ”msg”:”处理成功”,
            ”content”:”
            [
            {
            ”cardno”:”xxx”,
            “wxopenid”:”xxx”,
            ”tradeno”:”L61S1234D4567B1601010001”,
            “qrcodeurl”:”http://*.*.*:port/images/abc.jpg”,
            “barcodeurl”:”http://*.*.*:port/images/cde.jpg”,
            ”optime”:”2016-01-01 12:00:00”,
            ”opaction”:”订单已提交生产”,
            “operator”:”张三”
            },
            ……
            ]”
            “sign”:”9ade8ba90cf38745bea56398d235daae”
            }
            如果会员卡和手机（或微信）没有绑定，将不会折扣。
            qrcodeurl 是订单进门店提货对应的提货二维码图片地址。如果订单已提货，将不会有值返回
            barcodeurl是订单进门店提货对应的提货条码码图片地址。如果订单已提货，将不会有值返回

             */
            #endregion
            var content = "{\"cardno\":\"" + cardno + "\",\"phoneno\":\"\",\"tradeno\":\"" + tradeno + "\",\"timestamp\":\"" +
                 DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            return await PostAsync(content, "icapi/icconsumestatus").ConfigureAwait(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="mobile"></param>
        /// <param name="qrcode"></param>
        /// <returns></returns>
        public async Task<Zmodel> IcQrcodeQuery(string cardno, string mobile, string qrcode)
        {
            #region MyRegion
            /*
            qrcodetype：二维码处理方式，1、充值；2、充券(会员)
qrcodetypedesc：二维码处理方式描述，“充值”、“充券”
chargemoney：充值金额
largessmoney：充值赠送金额
point：积分
Promotion：是否可促销（积分、折扣等），0、不可以；1、可以
tickets：充券(会员券)明细
{
"code":0,
"msg":"处理成功",
"content":"
{
"cardno":"xxxx",
"phoneno";"xxxx",
"wxopenid":"xxxx",
"qrcodetype":1,
"qrcodetypedesc":"充值",
"chargemoney":xx.xx,
"largessmoney":x.xx,
"point":xxx,
"tickets":
[
{
"ticketname":"xxxx",
"ticketid”:xxxx,
"je":xxx.xx,
"makedate","yyyy-mm-dd",
"startdate","yyyy-mm-dd",
"enddate","yyyy-mm-dd",
"description",""    
},
{
"ticketname”:”xxxx”,
"ticketid”:xxxx,
"je”:xxx.xx,
"makedate”,”yyyy-mm-dd”,
"startdate”,”yyyy-mm-dd”,
"enddate”,”yyyy-mm-dd”,
"description”,””    
},
…
]
}”,
“sign”:”9ade8ba90cf38745bea56398d235daae”
}
相关错误代码：
321、二维码不存在
322、二维码已使用
323、二维码暂时不能使用
324、二维码已过期
*/
            #endregion
            var content = "{\"cardno\":\"" + cardno + "\",\"phoneno\":\"" + mobile + "\",\"qrcode\":\"" + qrcode + "\",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            logger.Trace($"{content}");
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            var model = await PostAsync(content, "icapi/icqrcodequery").ConfigureAwait(false);
            return model;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="qrcode"></param>
        /// <param name="qrcodeno"></param>
        /// <returns></returns>
        public async Task<Zmodel> icqrcodequery(string qrcode, string qrcodeno)
        {
            #region MyRegion
            /*
            qrcodetype：二维码处理方式，1、充值；2、充券(会员)
qrcodetypedesc：二维码处理方式描述，“充值”、“充券”
chargemoney：充值金额
largessmoney：充值赠送金额
point：积分
Promotion：是否可促销（积分、折扣等），0、不可以；1、可以
tickets：充券(会员券)明细
{
"code":0,
"msg":"处理成功",
"content":"
{
"cardno":"xxxx",
"phoneno";"xxxx",
"wxopenid":"xxxx",
"qrcodetype":1,
"qrcodetypedesc":"充值",
"chargemoney":xx.xx,
"largessmoney":x.xx,
"point":xxx,
"tickets":
[
{
"ticketname":"xxxx",
"ticketid”:xxxx,
"je":xxx.xx,
"makedate","yyyy-mm-dd",
"startdate","yyyy-mm-dd",
"enddate","yyyy-mm-dd",
"description",""    
},
{
"ticketname”:”xxxx”,
"ticketid”:xxxx,
"je”:xxx.xx,
"makedate”,”yyyy-mm-dd”,
"startdate”,”yyyy-mm-dd”,
"enddate”,”yyyy-mm-dd”,
"description”,””    
},
…
]
}”,
“sign”:”9ade8ba90cf38745bea56398d235daae”
}
相关错误代码：
321、二维码不存在
322、二维码已使用
323、二维码暂时不能使用
324、二维码已过期
*/
            #endregion
            var content = "{\"cardno\":\"\",\"phoneno\":\"\",\"qrcodeno\":\"" + qrcodeno + "\",\"qrcode\":\"" + qrcode + "\",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            return await PostAsync(content, "icapi/icqrcodequery").ConfigureAwait(false);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="mobile"></param>
        /// <param name="qrcode"></param>
        /// <param name="datasource"></param>
        /// <returns></returns>
        public async Task<Zmodel> IcqrcodeVerify(string cardno, string mobile, string qrcode, int datasource)
        {

            #region MyRegion
            /*  1、    ；
  2、    发送数据内容：
  cardno：卡号，建议填写
  phoneno：手机号，可不填，如果填写且也填写了卡号，服务端将判断手机号和卡号是否相符
  wxopenid：微信id，可不填，如果填写且也填写了卡号，服务端将判断微信号和卡号是否相符
  qrcode：充值码，以10开头的20位数字代码
  数据content示例：
  {
  "cardno":"xxxx",
  "phoneno":"xxxx",
  "wxopenid":"xxxx",
  "qrcode":"xxxxxxx",
  "timestamp":"2016 - 02 - 16 12:01:02"
  }
              3、        返回数据内容：
  qrcodetype：二维码处理方式，1、充值；2、充券(会员)
  chargemoney：充值金额
  largessmoney：充值赠送金额
  point：积分
  Promotion：是否可促销（积分、折扣等），0、不可以；1、可以
  tickets：充券(会员券)明细
  如果对应手机号没有绑定的卡类型，将创建一个绑定卡
  {
  "code":0,
  "msg":"处理成功", 
  "content":"
  {
  "cardno":"xxxx",
  "phoneno";"xxxx",
  "wxopenid":"xxxx",
  "qrcodetype":"1",
  "chargemoney":xx.xx,
  "largessmoney":x.xx,
  "point":xxx,
  "tickets":
  [
  {
  "ticketname":"xxxx",
  "ticketid":xxxx,
  "je":xxx.xx,
  "makedate","yyyy-mm-dd",
  "startdate","yyyy-mm-dd",
  "enddate","yyyy-mm-dd",
  "description",""    
  },
  {
  "ticketname":"xxxx",
  "ticketid":xxxx,
  "je":xxx.xx,
  "makedate","yyyy-mm-dd",
  "startdate","yyyy-mm-dd",
  "enddate","yyyy-mm-dd",
  "description",""    
  },
  …
  ]
  }",
  "sign":"9ade8ba90cf38745bea56398d235daae"
  }
  321、二维码不存在
  322、二维码已使用
  323、二维码暂时不能使用
  324、二维码已过期

  */

            #endregion
            if (string.IsNullOrEmpty(cardno)) {
                cardno = "";
            }
            var content = "{\"cardno\":\"" + cardno + "\",\"phoneno\":\"" + mobile + "\",\"qrcode\":\"" + qrcode + "\",\"datasource\":" + datasource + ",\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            var model = await PostAsync(content, "icapi/icqrcodeverify").ConfigureAwait(false);
            logger.Trace($"icqrcodeverify,req:{content},result:{JsonConvert.SerializeObject(model)}");
            return model;
        }
        /// <summary>
        /// 实体卡状态调整
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<Zmodel> IcRealCardAdjust(CardStateReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(CardStateReq).Name);
            }
            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            logger.Trace($"icrealcardadjust:{content}");
            return await PostAsync(content, "icapi/icrealcardadjust").ConfigureAwait(false);
        }
        /// <summary>
        /// Icrefresh the specified data.
        /// </summary>
        /// <returns>The icrefresh.</returns>
        /// <param name="data">Data.</param>
        public async Task<Zmodel> Icrefresh(CardRefreshRequest data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(CardRefreshRequest).Name);
            }
            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            logger.Trace($"icrefresh:{content}");
            var model = await PostAsync(content, "icapi/icrefresh").ConfigureAwait(false);
            logger.Trace($"icrefresh:{JsonConvert.SerializeObject(model)}");
            return model;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<Zmodel> Icconsume(IcconsumeReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(IcconsumeReq).Name);
            }
            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            logger.Trace($"icconsume:{content}");
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            var model = await PostAsync(content, "icapi/icconsume").ConfigureAwait(false);
            logger.Trace($"icconsume,req{content},rsp:{JsonConvert.SerializeObject(model)}");
            return model;
        }
        /// <summary>
        /// 门店核销
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<Zmodel> Icorderpickup(IcorderpickupReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(IcorderpickupReq).Name);
            }
            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/icorderpickup").ConfigureAwait(false);
            logger.Trace($"icorderpickup,Result:{JsonConvert.SerializeObject(model)},Req:{JsonConvert.SerializeObject(data)}");
            return model;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<Zmodel> Icorderstate(IcorderstateReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(IcorderstateReq).Name);
            }
            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/icorderstate").ConfigureAwait(false);
            logger.Trace($"icorderstate:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<Zmodel> Icconsumeback(IcConsumeBackReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(IcConsumeBackReq).Name);
            }
            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            var model = await PostAsync(content, "icapi/icconsumeback").ConfigureAwait(false);
            logger.Trace($"icconsumeback:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<Zmodel> Icconsumepay(IcConsumepayReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(IcConsumepayReq).Name);
            }
            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            //var _content = sc.Encrypt(content);
            //string signData = Sign("token" + token + "content" + content, signKey, signKey);
            var model = await PostAsync(content, "icapi/icconsumepay").ConfigureAwait(false);
            logger.Trace($"icconsumepay,req:{content},result:{JsonConvert.SerializeObject(model)}");
            return model;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public async Task<Zmodel> IcChargeList(string cardno, string mobile)
        {
            var content = "{\"cardno\":\"" + cardno + "\",\"phoneno\":\"" + mobile + "\",\"shopid\":0,\"timestamp\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) + "\"}";
            return await PostAsync(content, "icapi/icqrcodeverify").ConfigureAwait(false);
        }

        /// <summary>
        /// 二维码
        /// </summary>
        /// <returns>The icqrcodecreate.</returns>
        /// <param name="data">Data.</param>
        public async Task<Zmodel> Icqrcodecreate(IcQrcodeCreateReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(IcQrcodeCreateReq).Name);
            }
            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/icqrcodecreate").ConfigureAwait(false);
            logger.Trace($"icqrcodecreate: req:{ content} ,{ JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// Basedataquery the specified req.
        /// </summary>
        /// <returns>The basedataquery.</returns>
        /// <param name="req">Req.</param>
        public async Task<ZmodelExt> BasedataQuery(BasedataqueryReq req)
        {
            if (req == null) {
                throw new ArgumentNullException(typeof(BasedataqueryReq).Name);
            }
            var data = new { req.Pageindex, req.Pagesize, datatype = req.Datatype, datavalue = req.Datavalue, timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) };
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var model = await PostExtAsync(content, "icapi/basedataquery").ConfigureAwait(false);
            logger.Trace($"basedataquery:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// Productlist the specified req.
        /// </summary>
        /// <returns>The productlist.</returns>
        /// <param name="req">Req.</param>
        public async Task<Zmodel> Productlist(ProductlistReq req)
        {
            if (req == null) {
                throw new ArgumentNullException(typeof(ProductlistReq).Name);
            }
            //                cardno：卡号，可不填
            //phoneno：手机号，可不填，如果填写且也填写了卡号，服务端将判断手机号和卡号是否相符
            //wxopenid：微信id，可不填，如果填写且也填写了卡号，服务端将判断微信号和卡号是否相符
            //proudcttype：产品类型，C1、生日蛋糕；C2、端午产品；C4、中秋产品；C8、饮料；T1、生日蛋糕券；T2；端午产品券；T4、中秋产品券；T8、饮料券；
            var data = new { req.Producttype, req.Sellerphone, Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) };
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/productlist").ConfigureAwait(false);
            logger.Trace($"productlist:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<Zmodel> GroupSellback(GroupSellBackRequest req)
        {
            if (req == null) {
                throw new ArgumentNullException(nameof(req));
            }
            string content;
            if (req.Product != null && req.Product.Any()) {
                var data = new { tradeno = req.Tradeno, storeout = req.Storeout, product = req.Product, paycontent = req.Paycontent, timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) };
                content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            } else {
                content = JsonConvert.SerializeObject(new { tradeno = req.Tradeno, storeout = req.Storeout, timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) });

            }

            //var data = new { req.producttype, req.sellerphone, timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) };
            //var content = JsonConvert.SerializeObject(data);
            var model = await PostAsync(content, "icapi/groupsellback").ConfigureAwait(false);
            logger.Trace($"GroupSellback,req:{content},rsp:{JsonConvert.SerializeObject(model)}");
            return model;

        }

        /// <summary>
        /// 	团购订货退货
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<Zmodel> GroupOrderlback(GroupOrderBackRequest req)
        {
            if (req == null) {
                throw new ArgumentNullException(nameof(req));
            }
            string content;
            if (req.Product != null && req.Product.Any()) {
                var data = new { tradeno = req.Tradeno, product = req.Product, paycontent = req.Paycontent, timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) };
                content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            } else {
                content = JsonConvert.SerializeObject(new { tradeno = req.Tradeno, timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) });

            }

            //var data = new { req.producttype, req.sellerphone, timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) };
            //var content = JsonConvert.SerializeObject(data);
            var model = await PostAsync(content, "icapi/grouporderback").ConfigureAwait(false);
            logger.Trace($"GroupSellback,req:{content},rsp:{JsonConvert.SerializeObject(model)}");
            return model;

        }

        /// <summary>
        /// 提货单退货
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<Zmodel> IcSellback(IcSellBackRequest req)
        {
            if (req == null) {
                throw new ArgumentNullException(nameof(req));
            }
             
            string content;
            if (req.Product != null && req.Product.Any()) {
                var data = new { tradeno = req.Tradeno, storeout = req.Storeout, product = req.Product, paycontent = req.Paycontent, timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) };
                content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            } else {
                content = JsonConvert.SerializeObject(new { tradeno = req.Tradeno, storeout = req.Storeout, timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) });

            }

            //var data = new { req.producttype, req.sellerphone, timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) };
            //var content = JsonConvert.SerializeObject(data);
            var model = await PostAsync(content, "icapi/icsellback").ConfigureAwait(false);
            logger.Trace($"icsellback,req:{content},rsp:{JsonConvert.SerializeObject(model)}");
            return model;

        }
        /// <summary>
        /// Liststatus the specified req.
        /// </summary>
        /// <returns>The liststatus.</returns>
        /// <param name="req">Req.</param>
        public async Task<Zmodel> Liststatus(ListstatusReq req)
        {
            if (req == null) {
                throw new ArgumentNullException(typeof(ListstatusReq).Name);
            }
            //                1、  发送数据内容：
            //cardno：卡号，可不填写
            //phoneno：手机号，可不填，如果填写且也填写了卡号，服务端将判断手机号和卡号是否相符
            //wxopenid：微信id，可不填，如果填写且也填写了卡号，服务端将判断微信号和卡号是否相符
            //tradeno：单据交易号，必填
            //datasource：可选项，来源，11、微信；15、手机APP(默认)；16、PAD
            var data = new { cardno = "", phoneno = "", req.Tradeno, timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) };
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/liststatus").ConfigureAwait(false);
            logger.Trace($"liststatus:{JsonConvert.SerializeObject(model)}");
            return model;

        }

        /// <summary>
        /// Listsell the specified req.
        /// </summary>
        /// <returns>The listsell.</returns>
        /// <param name="req">Req.</param>
        public async Task<Zmodel> Listsell(ListSellReq req)
        {
            if (req == null) {
                throw new ArgumentNullException(typeof(ListSellReq).Name);
            }
            req.Memo = req.Bz;
            req.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(req, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/listsell").ConfigureAwait(false);
            logger.Trace($"listsell:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// Grouporder the specified data.
        /// </summary>
        /// <returns>The grouporder.</returns>
        /// <param name="data">Data.</param>
        public async Task<Zmodel> Grouporder(GroupOrderReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(GroupOrderReq).Name);
            }
            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/grouporder").ConfigureAwait(false);
            logger.Trace($"grouporder:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// Groupordermodify the specified req.
        /// </summary>
        /// <returns>The groupordermodify.</returns>
        /// <param name="req">Req.</param>
        public async Task<Zmodel> Groupordermodify(GroupOrdermobifyRequest req)
        {
            if (req == null) {
                throw new ArgumentNullException(typeof(GroupOrdermobifyRequest).Name);
            }
            req.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(req, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/groupordermodify").ConfigureAwait(false);
            logger.Trace($"groupordermodify:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// Grouporderdelete the specified req.
        /// </summary>
        /// <returns>The grouporderdelete.</returns>
        /// <param name="req">Req.</param>
        public async Task<Zmodel> Grouporderdelete(GrouporderdeleteReq req)
        {
            if (req == null) {
                throw new ArgumentNullException(typeof(GrouporderdeleteReq).Name);
            }
            req.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(req, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/grouporderdelete").ConfigureAwait(false);
            logger.Trace($"grouporderdelete:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// Listquery the specified data.
        /// </summary>
        /// <returns>The listquery.</returns>
        /// <param name="data">Data.</param>
        public async Task<ZmodelExt> Listquery(ListQueryReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(ListQueryReq).Name);
            }

            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var model = await PostExtAsync(content, "icapi/listquery").ConfigureAwait(false);
            logger.Trace($"listquery:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// Listdetail the specified req.
        /// </summary>
        /// <returns>The listdetail.</returns>
        /// <param name="req">Req.</param>
        public async Task<Zmodel> Listdetail(ListDetailReq req)
        {
            if (req == null) {
                throw new ArgumentNullException(typeof(ListDetailReq).Name);
            }
            req.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(req, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/listdetail").ConfigureAwait(false);
            logger.Trace($"listdetail:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        ///  查询单个产品信息
        /// </summary>
        /// <returns>The productdetail.</returns>
        /// <param name="req">Req.</param>
        public async Task<Zmodel> Productdetail(ProductDetailReq req)
        {
            if (req == null) {
                throw new ArgumentNullException(typeof(ProductDetailReq).Name);
            }
            var data = new { pid = req.Pid, timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture) };
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/productdetail").ConfigureAwait(false);
            logger.Trace($"productdetail:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// 第三方订货
        /// </summary>
        /// <returns>The thirdorder.</returns>
        /// <param name="req">Req.</param>
        public async Task<Zmodel> ThirdOrder(IcconsumeReq req)
        {
            if (req == null) {
                throw new ArgumentNullException(typeof(IcconsumeReq).Name);
            }
            req.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            req.Createinvoice = true;
            var content = JsonConvert.SerializeObject(req, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/thirdorder").ConfigureAwait(false);
            logger.Trace($"thirdorder,req:{content},result:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// Thirdorderback the specified data.
        /// </summary>
        /// <returns>The thirdorderback.</returns>
        /// <param name="data">Data.</param>
        public async Task<Zmodel> ThirdOrderBack(ThirdOrderBackReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(ThirdOrderBackReq).Name);
            }
            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/ThridOrderBack").ConfigureAwait(false);
            logger.Trace($"thirdorderback,req:{content},result:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        ///开具发票明细
        /// </summary>
        /// <returns>The invoice.</returns>
        /// <param name="data">Data.</param>
        public async Task<Zmodel> Invoice(InvoiceReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(InvoiceReq).Name);
            }
            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/invoice").ConfigureAwait(false);
            logger.Trace($"invoice:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// 单据发票查询
        /// </summary>
        /// <returns>The invoicequery.</returns>
        /// <param name="data">Data.</param>
        public async Task<Zmodel> Invoicequery(InvoiceQueryReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(InvoiceQueryReq).Name);
            }
            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/invoicequery").ConfigureAwait(false);
            logger.Trace($"invoicequery:{JsonConvert.SerializeObject(model)}");
            return model;
        }
        /// <summary>
        /// Stockpushplan the specified data.
        /// </summary>
        /// <returns>The stockpushplan.</returns>
        /// <param name="data">Data.</param>
        public async Task<Zmodel> Stockpushplan(StockpushPlanReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(StockpushPlanReq).Name);
            }
            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/stockpushplan").ConfigureAwait(false);
            logger.Trace($"stockpushplan:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// Icticket the specified data.
        /// </summary>
        /// <returns>The icticket.</returns>
        /// <param name="data">Data.</param>
        public async Task<Zmodel> Icticketlargess(AddTicketsReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(AddTicketsReq).Name);
            }
            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/ICTicketLargess").ConfigureAwait(false);
            logger.Trace($"icticket，req:{JsonConvert.SerializeObject(content)},reuslt:{JsonConvert.SerializeObject(model)}");
            return model;
        }

        /// <summary>
        /// Icticketlargesscancel the specified data.
        /// </summary>
        /// <returns>The icticketlargesscancel.</returns>
        /// <param name="data">Data.</param>
        public async Task<Zmodel> Icticketlargesscancel(TicketLargessCancelReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(typeof(TicketLargessCancelReq).Name);
            }
            data.Timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            var content = JsonConvert.SerializeObject(data, Formatting.None, jsonSerializerSettings);
            var model = await PostAsync(content, "icapi/icticketlargesscancel").ConfigureAwait(false);
            logger.Trace($"icticket，req:{JsonConvert.SerializeObject(content)},reuslt:{JsonConvert.SerializeObject(model)}");
            return model;
        }
    }
}
