﻿using System.Threading;

namespace Casamiel.API.Application.Services
{
    /// <summary>
    /// Ticket largess service.
    /// </summary>
    public class TicketLargessService:ITicketLargessService
    {
        private readonly IIcApiService _icApiService;
        /// <summary>
        /// Initializes a new instance .
        /// </summary>
        /// <param name="iicApiService">Iic API service.</param>
        public TicketLargessService(IIcApiService iicApiService)
        {
            _icApiService = iicApiService;
            Thread t1 = new Thread(new ThreadStart(Dequeue)) {
                IsBackground = true
            };
            t1.Start();

            Thread t2 = new Thread(new ThreadStart(TryDequeue)) {
                IsBackground = true
            };
            t2.Start();
        }
        private void Dequeue(){
            while (true)
            {
                Thread.Sleep(50);
            }
        }
        private void TryDequeue(){
            while (true)
            {
                Thread.Sleep(50);
            }
        }
        /// <summary>
        /// Adds the ticket.
        /// </summary>
        public void AddTicket()
        {

        }
    }
}
