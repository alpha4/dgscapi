﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Casamiel.API.Application.Models.Mall.Req;
using Casamiel.API.Application.Models.Mall.Rsp;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.API.Application.Services
{
    /// <summary>
    /// Mall service.
    /// </summary>
    public class MallService : IMallService
    {
        private readonly NLog.ILogger logger = NLog.LogManager.GetLogger("MallService");
        private readonly IHttpClientFactory _clientFactory;
        private readonly MallSettings _mallSettings;
        //private readonly IMemcachedClient _memcachedClient;
        private readonly string utoken = "";
        /// <summary>
        /// Initializes a new instance of the <see cref="Casamiel.API.Application.Services.MallService"/> class.
        /// </summary>
        /// <param name="settings">Settings.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        ///<param name="snapshot"></param>
        public MallService(IOptionsSnapshot<CasamielSettings> settings, IHttpClientFactory httpClientFactory, IOptionsSnapshot<MallSettings> snapshot)
        {
            if (settings == null) {
                throw new ArgumentNullException(nameof(settings));
            }

            if (snapshot == null) {
                throw new ArgumentNullException(nameof(snapshot));
            }

            _clientFactory = httpClientFactory;
            utoken = settings.Value.ApiToken;
            _mallSettings = snapshot.Value;
        }

        /// <summary>
        ///删除收货地址
        /// </summary>
        /// <param name="mobile">Mobile.</param>
        /// <param name="consigneeId">Consignee identifier.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        public void AddressDelete(string mobile, int consigneeId, ref int code, ref string msg)
        {
            var url = "MWeb/v1/Address/Delete";
            var data = new { mobile, consigneeId };
            var result = PostAsync(url, JsonConvert.SerializeObject(data)).GetAwaiter().GetResult();
            var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            if (dynamicdata["resultNo"].ToString() == "00000000") {
                code = 0;
            } else {
                msg = dynamicdata["resultRemark"].ToString();
                code = 999;
            }
        }

        /// <summary>
        /// Addresses the delete async.
        /// </summary>
        /// <returns>The delete async.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <param name="consigneeId">Consignee identifier.</param>
        public async Task<BaseResult<string>> AddressDeleteAsync(string mobile, int consigneeId)
        {
            var url = "MWeb/v1/Address/Delete";
            var data = new {
                mobile,
                consigneeId
            };
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 全国送 运费规则
        /// </summary>
        /// <returns></returns>
        public async Task<dynamic> GetFreightRule()
        {
            var url = "MWeb/v1/Index/GetConfig";

            var result = await PostAsync(url, "").ConfigureAwait(false);
            var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            if (dynamicdata["resultNo"].ToString() == "00000000") {
                return JsonConvert.DeserializeObject<dynamic>(dynamicdata["data"].ToString());
            }
            return result;
        }
        /// <summary>
        /// 保存收货地址
        /// </summary>
        /// <param name="data">Data.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        public void AddressSaveBase(string data, ref int code, ref string msg)
        {
            var url = "MWeb/v1/Address/SaveBase";

            var result = PostAsync(url, data).GetAwaiter().GetResult();
            var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            if (dynamicdata["resultNo"].ToString() == "00000000") {
                code = 0;
            } else {
                msg = dynamicdata["resultRemark"].ToString();
                code = 999;
            }
        }

        /// <summary>
        /// Saves the address async.
        /// </summary>
        /// <returns>The address async.</returns>
        /// <param name="data">Data.</param>
        public async Task<BaseResult<string>> SaveAddressAsync(string data)
        {
            var url = "MWeb/v1/Address/SaveBase";

            var result = await PostThirdApiAsync<string>(url, data).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 获取地址明细
        /// </summary>
        /// <returns>The address base async.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <param name="consigneeId">Consignee identifier.</param>
        public async Task<BaseResult<AddressBaseRsp>> GetAddressBaseAsync(string mobile, int consigneeId)
        {
            var url = "MWeb/v1/Address/SaveBase";
            var data = new { mobile, consigneeId };
            var result = await PostThirdApiAsync<AddressBaseRsp>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 获取地址明细
        /// </summary>
        /// <param name="mobile">Mobile.</param>
        /// <param name="consigneeId">Consignee identifier.</param>
        /// <param name="rsp">Rsp.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        public void SetAddressBase(string mobile, int consigneeId, ref AddressBaseRsp rsp, ref int code, ref string msg)
        {
            var url = "MWeb/v1/Address/SaveBase";
            var data = new { mobile = mobile, consigneeId = consigneeId };
            var result = PostAsync<ThirdResult<AddressBaseRsp>>(url, JsonConvert.SerializeObject(data)).GetAwaiter().GetResult();
            if (result.IsSuccess) {
                rsp = result.Data;
                code = 0;
            } else {
                code = 999;
                msg = result.ResultRemark;
            }
            // var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            //if (dynamicdata["resultNo"].ToString() == "00000000")
            //{
            //    rsp = JsonConvert.DeserializeObject<Address_BaseRsp>(dynamicdata["data"].ToString());
            //    code = 0;
            //}
            //else
            //{
            //    msg = dynamicdata["resultRemark"].ToString();
            //    code = 999;
            //}
        }

        /// <summary>
        /// 获取收获地址
        /// </summary>
        /// <param name="mobile">mobile</param>
        /// <param name="list">List.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        public void SetAddressList(string mobile, ref List<AddressBaseRsp> list, ref int code, ref string msg)
        {
            var url = "MWeb/v1/Address/GetList";
            var data = new { mobile = mobile };
            var result = PostAsync<ThirdResult<List<AddressBaseRsp>>>(url, JsonConvert.SerializeObject(data)).GetAwaiter().GetResult();

            if (result.IsSuccess) {
                list = result.Data;
                code = 0;
            } else {
                msg = result.ResultRemark;
                code = 999;
            }
        }

        /// <summary>
        /// Gets the address list.
        /// </summary>
        /// <returns>The address list.</returns>
        /// <param name="mobile">Mobile.</param>
        public async Task<BaseResult<List<AddressBaseRsp>>> GetAddressListAsync(string mobile)
        {
            var url = "MWeb/v1/Address/GetList";
            var data = new { mobile = mobile };
            var result = await PostThirdApiAsync<List<AddressBaseRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// Gets the index goods list async.
        /// </summary>
        /// <returns>The index goods list async.</returns>
        public void SetIndexGoodsList(ref List<IndexProductBaseRsp> list, ref int code, ref string msg)
        {
            var url = "MWeb/v1/Index/GetGoodsList";

            var result = PostAsync<ThirdResult<List<IndexProductBaseRsp>>>(url, "").GetAwaiter().GetResult();
            // var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            if (result.IsSuccess) {
                list = result.Data;
                code = 0;
            } else {
                msg = result.ResultRemark;
                code = 999;
            }
        }
        /// <summary>
        /// Gets the index goods list async.
        /// </summary>
        /// <returns>The index goods list async.</returns>
        public async Task<BaseResult<List<IndexProductBaseRsp>>> GetIndexGoodsListAsync()
        {
            var url = "MWeb/v1/Index/GetGoodsList";

            var result = await PostThirdApiAsync<List<IndexProductBaseRsp>>(url, "").ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 获取首页标签列表
        /// </summary>
        /// <param name="list">List.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        public void SetIndexTagsList(ref List<IndexTagBaseRsp> list, ref int code, ref string msg)
        {
            var url = "MWeb/v1/Index/GetTagsList";
            var result = PostAsync<ThirdResult<List<IndexTagBaseRsp>>>(url, "").GetAwaiter().GetResult();
            if (result.IsSuccess) {
                list = result.Data;
                code = 0;
            } else {
                msg = result.ResultRemark;
                code = 999;
            }
        }

        /// <summary>
        /// Gets the index tags list async.
        /// </summary>
        /// <returns>The index tags list async.</returns>
        public async Task<BaseResult<List<IndexTagBaseRsp>>> GetIndexTagsListAsync()
        {
            var url = "MWeb/v1/Index/GetTagsList";
            var result = await PostThirdApiAsync<List<IndexTagBaseRsp>>(url, "").ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// Sets the address default async.
        /// </summary>
        /// <returns>The address default async.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <param name="consigneeId">Consignee identifier.</param>
        public async Task<BaseResult<string>> SetAddressDefaultAsync(string mobile, int consigneeId)
        {
            var url = "MWeb/v1/Address/SetDefault";
            var data = new {
                mobile,
                consigneeId
            };
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// Sets the address default.
        /// </summary>
        /// <param name="mobile">Mobile.</param>
        /// <param name="consigneeId">Consignee identifier.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        public void SetAddressDefault(string mobile, int consigneeId, ref int code, ref string msg)
        {
            var url = "MWeb/v1/Address/SetDefault";
            var data = new { mobile = mobile, consigneeId = consigneeId };
            var result = PostAsync(url, JsonConvert.SerializeObject(data)).GetAwaiter().GetResult();
            var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            if (dynamicdata["resultNo"].ToString() == "00000000") {
                code = 0;
            } else {
                msg = dynamicdata["resultNo"].ToString() + "  " + dynamicdata["resultRemark"].ToString();
                code = 999;
            }
        }
        /// <summary>
        /// 通过产品ProductBaseId 获取商品明细
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="rsp">Rsp.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        public void SetGoodDetail(int id, ref GoodsBaseRsp rsp, ref int code, ref string msg)
        {
            var url = "MWeb/v1/Goods/GetDetail";
            var data = new { id = id };
            var result = PostAsync<ThirdResult<GoodsBaseRsp>>(url, JsonConvert.SerializeObject(data)).GetAwaiter().GetResult();
            //var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            if (result.IsSuccess) {
                code = 0;
                rsp = result.Data;// JsonConvert.DeserializeObject<GoodsBaseRsp>(dynamicdata["data"].ToString());
            } else {
                msg = result.ResultNo + result.ResultRemark;
                code = 999;
            }
        }

        /// <summary>
        /// Gets the goods detail async.
        /// </summary>
        /// <returns>The goods detail async.</returns>
        /// <param name="id">Identifier.</param>
        public async Task<BaseResult<GoodsBaseRsp>> GetGoodsDetailAsync(int id)
        {
            var url = "MWeb/v1/Goods/GetDetail";
            var data = new { id = id };
            var result = await PostThirdApiAsync<GoodsBaseRsp>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 获取分类下产品列表
        /// </summary>
        /// <param name="req">Req.</param>
        /// <param name="list">List.</param>
        /// <param name="total">Total.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        public void SetGoodsList(GoodsGetListReq req, ref List<ProductBaseRsp> list, ref int total, ref int code, ref string msg)
        {
            var url = "MWeb/v1/Goods/GetList";

            var result = PostThirdApiWithPagingAsync<List<ProductBaseRsp>>(url, JsonConvert.SerializeObject(req)).GetAwaiter().GetResult();
            // var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);

            list = result.Content;
            total = result.Total;
            code = result.Code;
            //}
            //else
            //{
            //    logger.Error($"{url},{JsonConvert.SerializeObject(req)},{dynamicdata["resultRemark"].ToString()}");
            //    msg = dynamicdata["resultNo"].ToString() + "  " + dynamicdata["resultRemark"].ToString();
            //    code = 999;
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<PagingResultRsp<List<ProductBaseRsp>>> GetGoodsListAsync(GoodsGetListReq req)
        {
            var url = "MWeb/v1/Goods/GetList";

            var result = await PostThirdApiWithPagingAsync<List<ProductBaseRsp>>(url, JsonConvert.SerializeObject(req)).ConfigureAwait(false);
            return result;

        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public async Task<BaseResult<string>> AddShoppingCartAsync(AddShoppingCartReq req, string mobile)
        {
            if (req == null) {
                throw new ArgumentNullException(nameof(req));
            }

            var url = "/MWeb/v1/ShoppingCart/AddBase";
            var data = new {
                mobile,
                req.Quantity,
                req.GoodsBaseId
            };
            var rsp = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return rsp;
        }


        /// <summary>
        /// Shoppings the cart change quantity async.
        /// </summary>
        /// <returns>The cart change quantity async.</returns>
        /// <param name="req">Req.</param>
        /// <param name="mobile">Mobile.</param>
        public async Task<BaseResult<string>> ShoppingCartChangeQuantityAsync(AddShoppingCartReq req, string mobile)
        {
            if (req == null) {
                throw new ArgumentNullException(nameof(req));
            }

            var url = "/MWeb/v1/ShoppingCart/ChangeQuantity";
            var data = new {
                mobile,
                req.Quantity,
                req.GoodsBaseId
            };
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }


        /// <summary>
        /// Shoppings the cart set check async.
        /// </summary>
        /// <returns>The cart set check async.</returns>
        /// <param name="req">Req.</param>
        public async Task<BaseResult<string>> ShoppingCartSetCheckAsync(ShoppingCartSetIsCheckReq req)
        {
            var url = "/MWeb/v1/ShoppingCart/SetCheck";
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(req)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 删除购物车商品
        /// </summary>
        /// <param name="req">Req.</param>
        /// <param name="mobile">Mobile.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        public void ShoppingCartDelete(List<int> req, string mobile, ref int code, ref string msg)
        {
            var url = "/MWeb/v1/ShoppingCart/Delete";
            var data = new { mobile = mobile, ids = req };
            var result = PostAsync(url, JsonConvert.SerializeObject(data)).GetAwaiter().GetResult();
            var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            if (dynamicdata["resultNo"].ToString() == "00000000") {
                code = 0;
            } else {
                msg = dynamicdata["resultNo"].ToString() + "  " + dynamicdata["resultRemark"].ToString();
                code = 999;
            }
        }

        /// <summary>
        /// Shoppings the cart delete async.
        /// </summary>
        /// <returns>The cart delete async.</returns>
        /// <param name="req">Req.</param>
        /// <param name="mobile">Mobile.</param>
        public async Task<BaseResult<string>> ShoppingCartDeleteAsync(List<int> req, string mobile)
        {
            var url = "/MWeb/v1/ShoppingCart/Delete";
            var data = new {
                mobile,
                ids = req
            };
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;

        }
        /// <summary>
        ///获取购物车商品集合
        /// </summary>
        /// <param name="rsp">Rsp.</param>
        /// <param name="mobile">Mobile.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        public void SetShoppingCartList(ref List<ShoppingCartBaseRsp> rsp, string mobile, ref int code, ref string msg)
        {
            var url = "MWeb/v1/ShoppingCart/GetList";
            var data = new { mobile };
            var result = PostAsync(url, JsonConvert.SerializeObject(data)).GetAwaiter().GetResult();
            var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            if (dynamicdata["resultNo"].ToString() == "00000000") {
                rsp = JsonConvert.DeserializeObject<List<ShoppingCartBaseRsp>>(dynamicdata["data"].ToString());
                code = 0;
            } else {
                msg = dynamicdata["resultNo"].ToString() + "  " + dynamicdata["resultRemark"].ToString();
                code = 999;
            }
        }

        /// <summary>
        /// Gets the shopping cart list async.
        /// </summary>
        /// <returns>The shopping cart list async.</returns>
        /// <param name="mobile">Mobile.</param>
        public async Task<BaseResult<List<ShoppingCartBaseRsp>>> GetShoppingCartListAsync(string mobile)
        {
            var url = "MWeb/v1/ShoppingCart/GetList";
            var data = new { mobile };
            var result = await PostThirdApiAsync<List<ShoppingCartBaseRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Creates the order.
        /// </summary>
        /// <param name="req">Req.</param>

        public async Task<BaseResult<OrderCreateRsp>> CreateOrder(OrderCreateReq req)
        {
            var url = "MWeb/v1/Order/Create";

            var result = await PostAsync(url, JsonConvert.SerializeObject(req)).ConfigureAwait(false);
            var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            if (dynamicdata["resultNo"].ToString() == "00000000") {

                var rsp = JsonConvert.DeserializeObject<OrderCreateRsp>(dynamicdata["data"].ToString());
                return new BaseResult<OrderCreateRsp>(rsp, 0, "");
            } else {
                //  msg = dynamicdata["resultNo"].ToString() + "  " + dynamicdata["resultRemark"].ToString();

                return new BaseResult<OrderCreateRsp>(null, 999, dynamicdata["resultRemark"].ToString());
            }
        }

        /// <summary>
        /// Gets the order base.
        /// </summary>
        /// <returns>The order base.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <param name="ordercode">Ordercode.</param>
        public async Task<BaseResult<MWebOrderBaseRsp>> GetOrderBase(string mobile, string ordercode)
        {
            var url = "MWeb/v1/Order/GetBase";
            var data = new { mobile, orderCode = ordercode };
            var result = await PostAsync<ThirdResult<MWebOrderBaseRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            if (result.IsSuccess) {
                return new BaseResult<MWebOrderBaseRsp>(result.Data, 0, "");
            }
            return new BaseResult<MWebOrderBaseRsp>(null, 999, result.ResultRemark);
            //var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            //if (dynamicdata["resultNo"].ToString() == "00000000")
            //{
            //    var    rsp = JsonConvert.DeserializeObject<MWeb_Order_BaseRsp>(dynamicdata["data"].ToString());
            //    return new BaseResult<MWeb_Order_BaseRsp>(rsp, 0, "");
            //}
            //else
            //{
            //    return new BaseResult<MWeb_Order_BaseRsp>(null, 999, dynamicdata["resultRemark"].ToString());
            //}
        }
        /// <summary>
        /// Orders the change status.
        /// </summary>
        /// <returns>The change status.</returns>
        /// <param name="ordercode">Ordercode.</param>
        /// <param name="mobile">Mobile.</param>
        /// <param name="type">Type.</param>
        /// <param name="payMethod">Pay menthod.</param>
        /// <param name="resBillNo"></param>
        public async Task<BaseResult<string>> OrderChangeStatus(string ordercode, string mobile, int type, int payMethod, string resBillNo)
        {
            var url = "MWeb/v1/Order/ChangeStatus";
            var data = new { ordercode, mobile, type, payMethod, resBillNo };
            logger.Trace($"OrderChangeStatus:{JsonConvert.SerializeObject(data)}");
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            if (dynamicdata["resultNo"].ToString() == "00000000") {
                return new BaseResult<string>("", 0, "");
            } else {
                return new BaseResult<string>("", 999, dynamicdata["resultRemark"].ToString());
            }
        }

        /// <summary>
        /// Gets the order list.
        /// </summary>
        /// <returns>The order list.</returns>
        /// <param name="status">Status.</param>
        /// <param name="mobile">Mobile.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        public async Task<PagingResultRsp<List<MWebOrderListRsp>>> GetOrderList(int status, string mobile, int pageIndex, int pageSize)
        {
            var url = "MWeb/v1/Order/GetList";
            var data = new {
                status,
                mobile,
                pageIndex,
                pageSize
            };
            var rsp = await PostThirdApiWithPagingAsync<List<MWebOrderListRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return rsp;
        }

        /// <summary>
        /// Gets the booth list.
        /// </summary>
        /// <returns>The booth list.</returns>
        /// <param name="code">Code.</param>
        public async Task<BaseResult<List<MWeb_Booth_GetListRsp>>> GetBoothList(string code)
        {
            var url = "MWeb/v1/Booth/GetList";
            var data = new { code };
            var result = await PostThirdApiAsync<List<MWeb_Booth_GetListRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// Gets the new by identifier.
        /// </summary>
        /// <returns>The new by identifier.</returns>
        /// <param name="Id">Identifier.</param>
        public async Task<BaseResult<MWebNewsBaseRsp>> GetNewById(int Id)
        {
            var url = "MWeb/v1/News/GetOne";
            var data = new { id = Id };
            var result = await PostThirdApiAsync<MWebNewsBaseRsp>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        private async Task<string> PostAsync(string url, string data)
        {
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, url) {
                Content = new StringContent(data, System.Text.Encoding.UTF8, "application/json")
            }) {
                requestMessage.Headers.Add("u-token", utoken);
                var client = _clientFactory.CreateClient("mallapi");
                using (var response = await client.SendAsync(requestMessage).ConfigureAwait(false)) {
                    if (response.IsSuccessStatusCode) {
                        var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        return result;
                    }
                }
                throw new HttpRequestException();
            }

        }
        private async Task<BaseResult<T>> PostThirdApiAsync<T>(string url, string data) where T : class
        {
            var ass = await PostAsync<ThirdResult<T>>(url, data).ConfigureAwait(false);
            if (ass.IsSuccess) {
                return new BaseResult<T>(ass.Data, 0, "");
            }
            if (ass.ResultNo == "02020003") {
                logger.Error($"url:{url},req:{data},rsp:{JsonConvert.SerializeObject(ass)}");
            }
            return new BaseResult<T>(ass.Data, 9999, ass.ResultRemark);
        }

        private async Task<PagingResultRsp<T>> PostThirdApiWithPagingAsync<T>(string url, string data) where T : class
        {
            var rsp = await PostAsync<ThirdPageingResult<T>>(url, data).ConfigureAwait(false);
            if (rsp.IsSuccess) {
                return new PagingResultRsp<T>(rsp.Data, rsp.PageIndex, rsp.PageSize, rsp.Total, 0, rsp.ResultRemark);
            }
            return new PagingResultRsp<T>(null, rsp.PageIndex, rsp.PageSize, rsp.Total, 9999, rsp.ResultRemark);
        }

        private async Task<T> PostAsync<T>(string url, string data) where T : class
        {
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, url) {
                Content = new StringContent(data, System.Text.Encoding.UTF8, "application/json")
            }) {
                requestMessage.Headers.Add("u-token", utoken);
                var client = _clientFactory.CreateClient("mallapi");
                using (var response = await client.SendAsync(requestMessage).ConfigureAwait(false)) {
                    if (response.IsSuccessStatusCode) {
                        var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        return JsonConvert.DeserializeObject<T>(result);
                    }
                    throw new HttpRequestException();
                }
            }
        }
    }
}
