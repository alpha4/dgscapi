﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Casamiel.API.Application.Models;
using Casamiel.API.Application.Models.Ele;
using Casamiel.API.Application.Models.Store;
using Casamiel.Common;
using Casamiel.Common.Extensions;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Request.Vote;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Vote;
using Enyim.Caching;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.API.Application.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class StoreApiService : IStoreApiService
    {
        //private readonly HttpClient _httpClient;
        private readonly CasamielSettings _settings;
        private readonly NLog.ILogger logger = NLog.LogManager.GetLogger("StoreService");
        private readonly IHttpClientFactory _clientFactory;
        private readonly IMemcachedClient _memcachedClient;
        private readonly string _memcachedPre = "";
        private readonly bool _cacheEnable = false;
        private readonly string Utoken;
        //private readonly int _cacheSeconds = 720;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="httpClientFactory"></param>
        /// <param name="memcachedClient"></param>
        public StoreApiService(IOptionsSnapshot<CasamielSettings> settings, IHttpClientFactory httpClientFactory, IMemcachedClient memcachedClient)
        {
            if (settings == null) {
                throw new ArgumentNullException(nameof(settings));
            }

            _settings = settings.Value;
            Utoken = settings.Value.ApiToken;
            _clientFactory = httpClientFactory;
            _memcachedClient = memcachedClient;
            _memcachedPre = settings.Value.MemcachedPre;
            //_httpClient = new HttpClient();
            //_httpClient.Timeout = new TimeSpan(0, 0, 30);
            //_httpClient.DefaultRequestHeaders.Connection.Add("keep-alive");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public async Task<string> ChangeCartStore(int storeId, string mobile)
        {
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, "mweb/ShoppingCart/ChangeCartStore")) {
                var data = new { storeId, mobile };
                requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");
                var client = _clientFactory.CreateClient("storeapi");
                using (var response = await client.SendAsync(requestMessage).ConfigureAwait(false)) {
                    if (response.IsSuccessStatusCode) {
                        var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        return result;
                    }
                }
            }

            throw new HttpRequestException();
        }


        /// <summary>
        /// Orders the change status.
        /// </summary>
        /// <returns>The change status.</returns>
        /// <param name="orderCode">Order code.</param>
        /// <param name="mobile">Mobile.</param>
        /// <param name="type">类型 1 支付完成 2 确认收货 3 用户取消订单 4 超时自动取消</param>
        /// <param name="payMethod">支付方式 只有在Type为1 可用</param>
        /// <param name="resBillNo">处理完成单号 只有在Type为1 可用</param>
        public async Task<BaseResult<string>> CakeOrderChangeStatus(string orderCode, string mobile, int type, int payMethod, string resBillNo)
        {
            var url = "CakeMall/v3/Order/ChangeStatus";
            var data = new { orderCode, mobile, type, payMethod, resBillNo };
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Gets the index goods list.
        /// </summary>
        /// <returns>The index goods list.</returns>
        /// <param name="storeId">Store identifier.</param>
        public async Task<List<IndexGoods>> GetIndexGoodsList(int storeId)
        {
            var url = "mweb/goods/GetIndexGoodsList";
            var data = new { storeId };

            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            var entity = JsonConvert.DeserializeObject<List<IndexGoods>>(dynamicdata["Data"].ToString());
            return entity;
        }
        /// <summary>
        /// Posts the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="url">URL.</param>
        /// <param name="data">Data.</param>
        private async Task<string> PostAsync(string url, string data)
        {
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, url)) {
                requestMessage.Content = new StringContent(data, System.Text.Encoding.UTF8, "application/json");

                var client = _clientFactory.CreateClient("storeapi");
                using (var response = await client.SendAsync(requestMessage).ConfigureAwait(false)) {
                    if (response.IsSuccessStatusCode) {
                        var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        return result;
                    }
                    throw new HttpRequestException();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="urlstr"></param>
        /// <param name="data"></param>
        /// <param name="api"></param>
        /// <returns></returns>
        private async Task<BaseResult<T>> PostThirdApiAsync<T>(string urlstr, string data, string api = "mallapi") where T : class
        {
            var ass = await PostAsync<ThirdResult<T>>(urlstr, data, api).ConfigureAwait(false);
            if (ass.IsSuccess) {
                return new BaseResult<T>(ass.Data, 0, "");
            }
            if (ass.ResultNo == "02020003") {
                logger.Error($"url:{urlstr},req:{data},rsp:{JsonConvert.SerializeObject(ass)}");
            }
            return new BaseResult<T>(ass.Data, 9999, ass.ResultRemark);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="data"></param>
        /// <param name="api"></param>
        /// <returns></returns>
        private async Task<T> PostAsync<T>(string url, string data, string api = "storeapi") where T : class
        {
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, url) {
                Content = new StringContent(data, System.Text.Encoding.UTF8, "application/json")
            }) {
                requestMessage.Headers.Add("u-token", Utoken);
                var client = _clientFactory.CreateClient(api);
                using (var response = await client.SendAsync(requestMessage).ConfigureAwait(false)) {
                    if (response.IsSuccessStatusCode) {
                        var result1 = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        T result = JsonConvert.DeserializeObject<T>(result1);
                        return result;
                    }
                    throw new HttpRequestException();
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="storeid"></param>
        /// <param name="productid"></param>
        /// <returns></returns>
        public async Task<ProductBase> GetGoodsById(string storeid, string productid)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/Goods/GetGoodsById");
            var data = new { storeid, productid };

            var url = "mweb/Goods/GetGoodsById";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            var entity = JsonConvert.DeserializeObject<ProductBase>(dynamicdata["Data"].ToString());
            return entity;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="GoodsId"></param>
        /// <param name="GoodsQuantity"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public async Task<string> ShoppingCartInsetOrUpate(int Id, int GoodsId, int GoodsQuantity, string mobile = "")
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/ShoppingCart/Save");
            var url = "mweb/ShoppingCart/Save";
            var data = new { Id, GoodsId, GoodsQuantity, mobile };

            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            var dynamicdata = JsonConvert.DeserializeObject<dynamic>(result);
            Console.WriteLine(dynamicdata.ResultNo);
            return result;
        }
        /// <summary>
        /// Shoppings the cart items checked.
        /// </summary>
        /// <returns>The cart items checked.</returns>
        /// <param name="Ids">Identifiers.</param>
        /// <param name="isCheck">If set to <c>true</c> is check.</param>
        /// <param name="Mobile">Mobile.</param>
        public async Task<string> ShoppingCartItemsChecked(string Ids, bool isCheck, string Mobile)
        {
            //            {
            //                "ResultNo": "00000000",
            //    "ResultRemark": ""
            //}
            //List<KeyValuePair<String, String>> paramList = new List<KeyValuePair<String, String>>();

            //paramList.Add(new KeyValuePair<String, string>("Ids", Ids));
            //paramList.Add(new KeyValuePair<String, string>("isCheck", isCheck.ToString()));
            //paramList.Add(new KeyValuePair<String, String>("Mobile", $"{Mobile}"));
            //HttpResponseMessage response = new HttpResponseMessage();
            var data = new { Ids, isCheck, Mobile };
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/ShoppingCart/SetCheck");

            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");
            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            var url = "mweb/ShoppingCart/SetCheck";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            //var result = await response.Content.ReadAsStringAsync();
            var dynamicdata = JsonConvert.DeserializeObject<dynamic>(result);
            Console.WriteLine(dynamicdata.ResultNo);
            return result;
        }
        /// <summary>
        /// 删除购物车项
        /// </summary>
        /// <param name="Ids"></param>
        /// <param name="Mobile"></param>
        /// <returns></returns>
        public async Task<string> ShoppingCartItemsDelete(string Ids, string Mobile)
        {
            List<KeyValuePair<String, String>> paramList = new List<KeyValuePair<String, String>>
            {
                new KeyValuePair<String, string>("Ids", Ids),
                new KeyValuePair<String, string>("Mobile", Mobile)
            };
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, "mweb/ShoppingCart/Delete") {
                Content = new FormUrlEncodedContent(paramList)
            }) {
                var client = _clientFactory.CreateClient("storeapi");
                using (var response = await client.SendAsync(requestMessage).ConfigureAwait(false)) {
                    if (response.IsSuccessStatusCode) {
                        var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        return result;
                    }
                    throw new HttpRequestException();
                }
            }
        }

        /// <summary>
        /// 我的购物车
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public async Task<List<ShoppingCartBaseRsp>> GetMyCartList(string mobile)
        {
            List<KeyValuePair<String, String>> paramList = new List<KeyValuePair<String, String>>
            {
                new KeyValuePair<String, string>("Mobile", mobile)
            };
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, "mweb/ShoppingCart/GetList") {
                Content = new FormUrlEncodedContent(paramList)
            }) {
                var client = _clientFactory.CreateClient("storeapi");
                using (var response = await client.SendAsync(requestMessage).ConfigureAwait(false)) {
                    if (response.IsSuccessStatusCode) {
                        var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
                        var entity = JsonConvert.DeserializeObject<List<ShoppingCartBaseRsp>>(dynamicdata["Data"].ToString());
                        return entity; ;
                    }
                }
                throw new HttpRequestException();
            }

            //HttpResponseMessage response = new HttpResponseMessage();
            //response = await _httpClient.PostAsync(new Uri($"{_settings.StoreApiUrl}/ShoppingCart/GetList"), new FormUrlEncodedContent(paramList));
            //var result = await response.Content.ReadAsStringAsync();
            //var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            //var entity = JsonConvert.DeserializeObject<ProductBase>(dynamicdata["Data"].ToString());
            //var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            //var entity = JsonConvert.DeserializeObject<List<ShoppingCartBaseRsp>>(dynamicdata["Data"].ToString());
            //return entity; ;
        }
        /// <summary>
        /// Gets the goods get base.
        /// </summary>
        /// <returns>The goods get base.</returns>
        /// <param name="storeid">Storeid.</param>
        /// <param name="GoodsId">Goods identifier.</param>
        public async Task<GoodsGetBaseRsp> GetGoodsBase(int storeid, int GoodsId)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/goods/GetBase");
            var data = new { storeid = storeid, GoodsId = GoodsId };

            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            var url = "mweb/goods/GetBase";

            var result = await PostAsync<ThirdResult<GoodsGetBaseRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            if (result.IsSuccess) {
                return result.Data;
            }

            throw new Exception(result.ResultRemark);
        }
        /// <summary>
        /// Gets the group identifier.
        /// </summary>
        /// <returns>The group identifier.</returns>
        /// <param name="Id">Identifier.</param>
        public async Task<string> GetGroupId(string Id)
        {
            var url = "tpp/Goods/GetGroupId";
            var data = new { Id };
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }//TPP/v1/Goods/GetProductBaseId

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<ThirdResult<int>> GetGroupIdAsync(int Id)
        {
            var url = "TPP/v1/Goods/GetProductBaseId";
            var data = new { goodsRelationId = Id };
            var result = await PostAsync<ThirdResult<int>>(url, JsonConvert.SerializeObject(data), "mallapi").ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<BaseResult<string>> AddVoteAsync(VoteAddReq req)
        {
            var url = "v3/Vote/AddVote";
            var data = new { req.MemberId ,OpenId=$"{req.Id }"};
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data), "mallapi").ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="OpenId"></param>
        /// <returns></returns>
        public async Task<BaseResult<List<VoteListRsp>>>GetVoteListAsync(string OpenId)
        {
            var url = "v3/Vote/GetMemberList";
            var data = new {OpenId };
            var result = await PostThirdApiAsync<List<VoteListRsp>>(url, JsonConvert.SerializeObject(data), "mallapi").ConfigureAwait(false);
            return result;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<BaseResult<VoteDetailRsp>> GetVoteDetail(VoteGetDetailReq req)
        {
            var url = "v3/Vote/GetMemberDetail";
           // var data = new { req.MemberId, OpenId = $"{req.Id }" };
            var result = await PostThirdApiAsync<VoteDetailRsp>(url, JsonConvert.SerializeObject(req), "mallapi").ConfigureAwait(false);
            return result;
        }
        //TPP/v1/Goods/GetProductBaseId
        /// <summary>
        /// 创建订单
        /// </summary>
        /// <returns>The single order.</returns>
        /// <param name="data">Data.</param>
        public async Task<string> CreateSingleOrder(Order_AddSingleReq data)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/Order/AddSingle");

            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            var url = "mweb/Order/AddSingle";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// Gets the order by order code async.
        /// </summary>
        /// <returns>The order by order code async.</returns>
        /// <param name="orderCode">Order code.</param>
        /// <param name="mobile">Mobile.</param>
        public async Task<OrderBaseRsp> GetOrderByOrderCodeAsync(string orderCode, string mobile)
        {
            // var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/Order/GetBase");
            var data = new { orderCode = orderCode, mobile = mobile };

            var url = "mweb/Order/GetBase";
            var result = await PostAsync<ThirdResult<OrderBaseRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            if (result.IsSuccess) { return result.Data; }
            throw new Exception(result.ResultRemark);
            //Console.WriteLine($"Order_BaseRsp:{result}");
            //var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            //var entity = JsonConvert.DeserializeObject<Order_BaseRsp>(dynamicdata["Data"].ToString());
            //return entity;
        }

        /// <summary>
        /// Adds the collect.
        /// </summary>
        /// <returns>The collect.</returns>
        /// <param name="productBaseId">Product base identifier.</param>
        /// <param name="mobile">Mobile.</param>
        public async Task AddCollectAsync(int productBaseId, string mobile)
        {
            // var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/Goods/AddCollect");
            var data = new { productBaseId, mobile };
            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            var url = "mweb/Goods/AddCollect";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            Console.WriteLine($"result:{result}");
        }
        /// <summary>
        /// Checks the collect.
        /// </summary>
        /// <returns>The collect.</returns>
        /// <param name="productBaseId">Product base identifier.</param>
        /// <param name="mobile">Mobile.</param>
        public async Task<bool> ProductCheckCollect(int productBaseId, string mobile)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/Goods/CheckCollect");
            var data = new { productBaseId, mobile };
            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            var url = "mweb/Goods/CheckCollect";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            if (result == "True") {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Cancels the collect async.
        /// </summary>
        /// <returns>The collect async.</returns>
        /// <param name="productBaseId">Product base identifier.</param>
        /// <param name="mobile">Mobile.</param>
        public async Task CancelCollectAsync(int productBaseId, string mobile)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/Goods/CancelCollect");
            var data = new { productBaseId, mobile };
            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            var url = "mweb/Goods/CancelCollect";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            Console.WriteLine($"result:{result}");
        }
        /// <summary>
        /// Clears the collect async.
        /// </summary>
        /// <returns>The collect async.</returns>
        /// <param name="mobile">Mobile.</param>
        public async Task ClearCollectAsync(string mobile)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/Goods/ClearCollect");
            var data = new { mobile = mobile };
            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            var url = "mweb/Goods/ClearCollect";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            Console.WriteLine($"result:{result}");
        }
        /// <summary>
        /// Gets the collect list async.
        /// </summary>
        /// <returns>The collect list async.</returns>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="mobile">Mobile.</param>
        public async Task<string> GetCollectListAsync(int pageIndex, int pageSize, string mobile)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/Goods/GetCollectList");
            var data = new { pageIndex, pageSize, mobile };
            var url = "mweb/Goods/GetCollectList";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Gets the goods list.
        /// </summary>
        /// <returns>The goods list.</returns>
        /// <param name="TagId">Tag identifier.</param>
        /// <param name="pageindex">Pageindex.</param>
        /// <param name="pagesize">Pagesize.</param>
        public async Task<string> GetGoodsList(int TagId, int pageindex, int pagesize)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/goods/GetList");
            var data = new { TagId = TagId, pagesize = pagesize, pageindex = pageindex };
            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            var url = "mweb/goods/GetList";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Gets my order list.
        /// </summary>
        /// <param name="mobile">Mobile.</param>
        /// <param name="pageindex">Pageindex.</param>
        /// <param name="pagesize">Pagesize.</param>
        /// <param name="OrderStatus">Order status.</param>
        /// <param name="list">List.</param>
        /// <param name="total">Total.</param>
        public void GetMyOrderList(string mobile, int pageindex, int pagesize, int OrderStatus, ref List<OrderBaseRsp> list, ref int total)
        {
            List<KeyValuePair<String, String>> paramList = new List<KeyValuePair<String, String>>
            {
                new KeyValuePair<String, string>("mobile", $"{mobile}"),
                new KeyValuePair<String, String>("pageindex", $"{pageindex}"),
                new KeyValuePair<String, String>("pagesize", $"{pagesize}"),
                new KeyValuePair<String, String>("OrderStatus", $"{OrderStatus}")
            };
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, "mweb/order/GetList")) {
                requestMessage.Content = new FormUrlEncodedContent(paramList);
                var result = "";


                var client = _clientFactory.CreateClient("storeapi");
                using (var response = client.SendAsync(requestMessage).GetAwaiter().GetResult()) {
                    if (response.IsSuccessStatusCode) {
                        result = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                        var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
                        list = JsonConvert.DeserializeObject<List<OrderBaseRsp>>(dynamicdata["Data"].ToString());
                        total = dynamicdata["Total"].ToInt32(0);
                    } else {
                        throw new HttpRequestException();
                    }
                }
            }



            //HttpResponseMessage response = new HttpResponseMessage();

            //response = await _httpClient.PostAsync(new Uri($"{_settings.StoreApiUrl}/order/GetList"), new FormUrlEncodedContent(paramList));

            ////var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            //return result;
        }

        /// <summary>
        /// my order  list
        /// </summary>
        /// <returns>The MYO rder list.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <param name="pageindex">Pageindex.</param>
        /// <param name="pagesize">Pagesize.</param>
        /// <param name="OrderStatus"></param>
        public async Task<string> GetMYOrderList(string mobile, int pageindex, int pagesize, int OrderStatus = 99)
        {
            List<KeyValuePair<String, String>> paramList = new List<KeyValuePair<String, String>>
            {
                new KeyValuePair<String, string>("mobile", $"{mobile}"),
                new KeyValuePair<String, String>("pageindex", $"{pageindex}"),
                new KeyValuePair<String, String>("pagesize", $"{pagesize}"),
                new KeyValuePair<String, String>("OrderStatus", $"{OrderStatus}")
            };
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, "mweb/order/GetList") {
                Content = new FormUrlEncodedContent(paramList)
            }) {
                var client = _clientFactory.CreateClient("storeapi");
                using (var response = await client.SendAsync(requestMessage).ConfigureAwait(false)) {
                    if (response.IsSuccessStatusCode) {
                        return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    }
                    throw new HttpRequestException();
                }
            }
        }
        /// <summary>
        /// Gets the index tag list.
        /// </summary>
        /// <returns>The index tag list.</returns>
        public async Task<List<TagModel>> GetIndexTagList()
        {
            var url = "mweb/goods/GetTagList";
            var result = await PostAsync<ThirdResult<List<TagModel>>>(url, "").ConfigureAwait(false);
            if (result.IsSuccess) { return result.Data; }
            throw new Exception(result.ResultRemark);
        }
        /// <summary>
        /// Gets the take order detail.
        /// </summary>
        /// <returns>The take order detail.</returns>
        /// <param name="orderCode">Order code.</param>
        /// <param name="mobile">Mobile.</param>
        /// <exception cref="TaskCanceledException"></exception>
        public async Task<OrderTakeBaseRsp> GetTakeOrderDetail(string orderCode, string mobile)
        {
            var data = new { orderCode = orderCode, mobile = mobile };
            var url = "mweb/Order/GetTakeOrder";
            var result = await PostAsync<ThirdResult<OrderTakeBaseRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            if (result.IsSuccess) return result.Data;
            throw new Exception(result.ResultRemark);
            //var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            //var entity = JsonConvert.DeserializeObject<Order_TakeBaseRsp>(dynamicdata["Data"].ToString());
            //return entity;
        }
        /// <summary>
        /// Gets the slide list.
        /// </summary>
        /// <returns>The slide list.</returns>
        /// <param name="code">Code.</param>
        public async Task<List<SlideGetListRsp>> GetSlideList(string code)
        {
            var url = "mweb/Slide/GetList";
            var ckey = _memcachedPre + Constant.SLIDELIST + code;
            if (_cacheEnable) {
                var list = await _memcachedClient.GetValueAsync<List<SlideGetListRsp>>(ckey).ConfigureAwait(false);
                if (list == null) {
                    var result1 = await PostAsync<ThirdResult<List<SlideGetListRsp>>>(url, JsonConvert.SerializeObject(new { code = code })).ConfigureAwait(false);
                    if (result1.IsSuccess) {
                        return result1.Data;
                    }
                    throw new Exception(result1.ResultRemark);
                    //var dynamicdata1 = JsonConvert.DeserializeObject<JObject>(result1);
                    //   list = JsonConvert.DeserializeObject<List<Slide_GetListRsp>>(dynamicdata1["Data"].ToString());
                    //if (list != null)
                    //{
                    //	await _memcachedClient.AddAsync(ckey,list,_cacheSeconds);
                    //}
                }
                return list;
            }

            var data = new { code = code };
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            var entity = JsonConvert.DeserializeObject<List<SlideGetListRsp>>(dynamicdata["Data"].ToString());
            return entity;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task<List<SlideGetListRsp>> GetSlideListAsync(string code)
        {
            var url = "CakeMall/v3/Booth/GetList";
            var data = new { code };
            var result = await PostThirdApiAsync<List<SlideGetListRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result.Content;//reason
        }
        /// <summary>
        /// Gets the new by identifier.
        /// </summary>
        /// <returns>The new by identifier.</returns>
        /// <param name="id">Identifier.</param>
        public async Task<NewsBaseRsp> GetNewsById(int id)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/news/get");
            var data = new { Id = id, };
            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            //Console.WriteLine($"News_BaseRsp:{result}");
            var url = "mweb/news/get";
            var result = await PostAsync<ThirdResult<NewsBaseRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            if (result.IsSuccess) { return result.Data; }
            throw new Exception(result.ResultRemark);
            //var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            //var entity = JsonConvert.DeserializeObject<News_BaseRsp>(dynamicdata["Data"].ToString());
            //return entity;
        }
        /// <summary>
        /// Gets the list.
        /// </summary>
        /// <returns>The list.</returns>
        /// <param name="mobile">Mobile.</param>
        public async Task<List<AddressBaseRsp>> GetList(string mobile)
        {
            // var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/address/GetList");
            var data = new { mobile = mobile };

            var url = "mweb/address/GetList";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            Console.WriteLine($"Order_TakeBaseRsp:{result}");
            var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            var entity = JsonConvert.DeserializeObject<List<AddressBaseRsp>>(dynamicdata["Data"].ToString());
            return entity;
        }
        /// <summary>
        /// Gets the base.
        /// </summary>
        /// <returns>The base.</returns>
        /// <param name="id">Identifier.</param>
        /// <param name="mobile">Mobile.</param>
        public async Task<AddressBaseRsp> GetBase(int id, string mobile)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/address/GetBase");
            var data = new { id, mobile };

            var url = "mweb/address/GetBase";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            //Console.WriteLine($"News_BaseRsp:{result}");
            var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            var entity = JsonConvert.DeserializeObject<AddressBaseRsp>(dynamicdata["Data"].ToString());
            return entity;
        }
        /// <summary>
        /// Saves the address.
        /// </summary>
        /// <returns>The address.</returns>
        /// <param name="req">Req.</param>
        public async Task<BaseResult<string>> SaveAddress(AddressSaveReq req)
        {
            var url = "mweb/address/SaveAddress";
            var result = await PostAsync<ThirdResult<string>>(url, JsonConvert.SerializeObject(req)).ConfigureAwait(false);
            if (result.IsSuccess) {
                return new BaseResult<string>("", 0, "");
            } else if (result.ResultNo == "01030008") {
                return new BaseResult<string>("", -29, result.ResultRemark);
            }
            return new BaseResult<string>("", 9999, result.ResultRemark);
        }
        /// <summary>
        /// 保存配送收货地址
        /// </summary>
        /// <returns>The save.</returns>
        /// <param name="req">Req.</param>
        public async Task<BaseResult<AddressBaseRsp>> Save(AddressSaveReq req)
        {
            var url = "mweb/address/Save";
            var result = await PostAsync<ThirdResult<AddressBaseRsp>>(url, JsonConvert.SerializeObject(req)).ConfigureAwait(false);

            if (result.IsSuccess) {
                return new BaseResult<AddressBaseRsp>(result.Data, 0, "操作成功");
            }

            if (result.ResultNo == "01030010")//01030010
            {
                return new BaseResult<AddressBaseRsp>(null, -29, result.ResultRemark);
            }
            return new BaseResult<AddressBaseRsp>(null, 999, result.ResultRemark);

        }
        /// <summary>
        /// Checks the location.
        /// </summary>
        /// <returns>The location.</returns>
        /// <param name="storeId">Store identifier.</param>
        /// <param name="consigneeId">Consignee identifier.</param>
        public async Task<ThirdResult<string>> CheckLocation(int storeId, int consigneeId)
        {

            //        "ResultNo": "01030010",
            //"ResultRemark": "超出配送范围"

            var data = new { storeId = storeId, consigneeId = consigneeId };
            var url = "mweb/address/CheckLocation";
            var result = await PostAsync<ThirdResult<string>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Deletes the address.
        /// </summary>
        /// <returns>The address.</returns>
        /// <param name="id">Identifier.</param>
        /// <param name="mobile">Mobile.</param>
        public async Task<string> DeleteAddress(int id, string mobile)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/address/DeleteAddress");

            var data = new { id = id, mobile = mobile };
            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            var url = "mweb/address/DeleteAddress";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            return result;
        }
        /// <summary>
        /// Sets the address default.
        /// </summary>
        /// <returns>The address default.</returns>
        /// <param name="id">Identifier.</param>
        /// <param name="mobile">Mobile.</param>
        public async Task<string> SetAddressDefault(int id, string mobile)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/address/SetAddressDefault");

            var data = new { id = id, mobile = mobile };
            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            var url = "mweb/address/SetAddressDefault";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            return result;
        }
        /// <summary>
        /// Gets the list by degree.
        /// </summary>
        /// <returns>The list by degree.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <param name="storeId">Store identifier.</param>
        public async Task<List<AddressBaseRsp>> GetListByDegree(string mobile, int storeId)
        {
            var data = new { storeId = storeId, mobile = mobile };
            var url = "mweb/address/GetListByDegree";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            var list = JsonConvert.DeserializeObject<List<AddressBaseRsp>>(dynamicdata["Data"].ToString());
            return list;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<string> Login(string username, string password)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/tmall/Login");
            var data = new { UserName = username, Password = password };
            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            var url = "mweb/tmall/Login";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<string> LogOut(string token)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/tmall/LoginOut");
            var data = new { Token = token };
            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            var url = "mweb/tmall/LoginOut";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<string> CheckToken(string token)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/tmall/CheckToken");
            var data = new { Token = token };
            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            var url = "mweb/tmall/CheckToken";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<BaseResult<string>> CheckTokenAsync(string token)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/tmall/CheckToken");
            var data = new { Token = token };
            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            var url = "TPP/v1/Account/CheckToken";
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task AddReferral(string mobile, string code)
        {
            var data = new {mobile,code};
            var url = "v3/Account/AddReferral";
            await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public async Task<string> GetGoodsListForTmall(string token, string title)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/tmall/GetProductList");
            var data = new { Token = token, Title = title };
            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            var url = "mweb/tmall/GetProductList";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Adds the tmall order.
        /// </summary>
        /// <returns>The tmall order.</returns>
        /// <param name="data">Data.</param>
        public async Task<string> AddTmallOrder(TMallAddOrderReq data)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/tmall/addorder");

            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            var url = "mweb/tmall/addorder";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            return result;
        }
        /// <summary>
        /// Gets the tmall order list.
        /// </summary>
        /// <returns>The tmall order list.</returns>
        /// <param name="data">Data.</param>
        public async Task<string> GetTmallOrderList(TmallOrderListReq data)
        {
            //var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{_settings.StoreApiUrl}/tmall/GetOrderList");

            //requestMessage.Content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json");

            //var response = await _httpClient.SendAsync(requestMessage);
            //if (response.StatusCode == HttpStatusCode.InternalServerError)
            //{
            //    throw new HttpRequestException();
            //}
            //var result = await response.Content.ReadAsStringAsync();
            var url = "mweb/tmall/GetOrderList";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            return result;
        }


  

        /// <summary>
        /// 改变达达订单状态
        /// </summary>
        /// <returns>The TM all dada status.</returns>
        /// <param name="req">Req.</param>
        public async Task<string> ChangeDadaStatus(ChangeDadaStatusReq req)
        {

            var url = "tpp/order/ChangeDadaStatus";
            var result = await PostAsync(url, JsonConvert.SerializeObject(req)).ConfigureAwait(false);

            return result;
        }
        /// <summary>
        /// JDs the order accept operate.
        /// </summary>
        /// <returns>The order accept operate.</returns>
        /// <param name="data">Data.</param>
        public async Task<string> JDorderAcceptOperate(string data)
        {
            var url = "api/OpendjAPI/OrderAPI/orderAcceptOperate";
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, url)) {
                requestMessage.Content = new StringContent(data, System.Text.Encoding.UTF8, "application/json");

                var client = _clientFactory.CreateClient("JdOpenApi");
                using (var response = await client.SendAsync(requestMessage).ConfigureAwait(false)) {
                    if (response.IsSuccessStatusCode) {
                        var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        return result;
                    }
                }
            }

            throw new HttpRequestException();
        }
        /// <summary>
        /// 删除订单
        /// </summary>
        /// <param name="orderCode"></param>
        /// <returns></returns>
        public async Task<string> OrderDelete(string orderCode)
        {
            var url = "tpp/order/Delete";
            var data = new { orderCode = orderCode };
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);

            return result;
        }
        /// <summary>
        /// Sends the order notice.
        /// </summary>
        /// <returns>The order notice.</returns>
        /// <param name="orderCode">Order code.</param>
        public async Task<string> SendOrderNotice(string orderCode)
        {
            var url = "mweb/order/SendNotice";
            var data = new { orderCode = orderCode };
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<BaseResult<string>> AddMtOrderV2Async(AddMeiTuanOrderReq data)
        {
            var url = "TPP/v3/Order/AddMeiTuan";
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Adds the MTO rder.
        /// </summary>
        /// <returns>The MTO rder.</returns>
        /// <param name="data">Data.</param>
        public async Task<string> AddMtOrder(AddMeiTuanOrderReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(nameof(data));
            }

            var url = "tpp/order/AddMeiTuan";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Meituans the cancel.
        /// </summary>
        /// <returns>The cancel.</returns>
        /// <param name="orderId">Order identifier.</param>
        /// <param name="reasonCode">Reason code.</param>
        /// <param name="reason">Reason.</param>
        public async Task<string> MeituanCancel(long orderId, string reasonCode, string reason)
        {
            var url = "tpp/order/MeituanCancel";
            var data = new { orderId = orderId, reasonCode = reasonCode, reason };
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;//reason
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="reasonCode"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        public async Task<BaseResult<string>> MeituanCancelV2Async(long orderId, string reasonCode, string reason)
        {
            var url = "tpp/v3/order/MeituanCancel";
            var data = new { orderId, reason };
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;//reason
        }
        /// <summary>
        /// Meituans the complate.
        /// </summary>
        /// <returns>The complate.</returns>
        /// <param name="data">Data.</param>
        public async Task<string> MeituanComplate(AddMeiTuanOrderReq data)
        {
            var url = "tpp/order/MeituanComplate";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// v2MeituanComplate
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<BaseResult<string>> MeituanComplateV2Async(AddMeiTuanOrderReq data)
        {
            var url = "tpp/v1/order/MeituanComplate";
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Meituans the refund.
        /// </summary>
        /// <returns>The refund.</returns>
        /// <param name="notifyType">Notify type.</param>
        /// <param name="orderId">Order identifier.</param>
        /// <param name="reason">Reason.</param>
        public async Task<string> MeituanRefund(string notifyType, long orderId, string reason)
        {
            var url = "tpp/order/MeituanRefund";
            var data = new { notifyType = notifyType, orderId = orderId, reason };
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notifyType"></param>
        /// <param name="orderId"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        public async Task<BaseResult<string>> MeituanRefundV2Async(string notifyType, long orderId, string reason)
        {
            var url = "tpp/v1/order/MeituanRefund";
            var data = new { notifyType = notifyType, orderId = orderId, reason };
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Meituans the change express status.
        /// </summary>
        /// <returns>The change express status.</returns>
        /// <param name="dispatcherMobile">Dispatcher mobile.</param>
        /// <param name="dispatcherName">Dispatcher name.</param>
        /// <param name="orderId">Order identifier.</param>
        /// <param name="shippingStatus">Shipping status.</param>
        /// <param name="time">Time.</param>
        public async Task<string> MeituanChangeExpressStatus(string dispatcherMobile, string dispatcherName, long orderId, int shippingStatus, int time)
        {
            var url = "tpp/order/MeituanChangeExpressStatus";
            var data = new { dispatcherMobile = dispatcherMobile, dispatcherName = dispatcherName, orderId = orderId, shippingStatus = shippingStatus, time = time };
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dispatcherMobile"></param>
        /// <param name="dispatcherName"></param>
        /// <param name="orderId"></param>
        /// <param name="shippingStatus"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        public async Task<BaseResult<string>> MeituanChangeExpressStatusV2Async(string dispatcherMobile, string dispatcherName, long orderId, int shippingStatus, int time)
        {
            var url = "tpp/v3/order/MeituanChangeExpressStatus";
            var data = new { dispatcherMobile = dispatcherMobile, dispatcherName = dispatcherName, orderId = orderId, shippingStatus = shippingStatus, time = time };
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Meituans the complate2.
        /// </summary>
        /// <returns>The complate2.</returns>
        /// <param name="orderId">Order identifier.</param>
        public async Task<string> MeituanComplate2(string orderId)
        {
            var url = "tpp/order/MeituanComplate2";
            var data = new { orderId = orderId };
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 意外情况下需完成订单接口
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public async Task<BaseResult<string>> MeituanComplate2V2Async(string orderId)
        {
            var url = "TPP/v1/Order/Complate";
            var data = new { orderId = orderId };
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Gets the shop list for dada.
        /// </summary>
        /// <returns>The shop list for dada.</returns>
        public async Task<string> GetShopListForDada()
        {
            var url = "mweb/store/getlist";
            var result = await PostAsync(url, "").ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 饮品skuid集合
        /// </summary>
        /// <returns></returns>
        public async Task<string> GetDrinksGoodsId()
        {
            var url = "tpp/Goods/GetDrinksGoodsId";
            var result = await PostAsync(url, "").ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// Meituans the tradedetail.
        /// </summary>
        /// <returns>The tradedetail.</returns>
        /// <param name="data">Data.</param>
        public async Task<string> MeituanTradedetail(JObject data)
        {
            var url = "/tpp/trade/AddMeituan";
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<BaseResult<string>> MeituanTradedetailV2Async(JObject data)
        {
            var url = "TPP/v1/Trade/AddMeituan";
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
       

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<BaseResult<string>> EleNotifyAsync(NotifyMessageReq data)
        {
            var url = "TPP/v3/Order/EleNotify";
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Changes the sf status.
        /// </summary>
        /// <returns>The sf status.</returns>
        /// <param name="req">Req.</param>
        public async Task<string> ChangeSfStatus(Order_ChangeSfStatusReq req)
        {
            var url = "/tpp/order/ChangeSfStatus";
            var result = await PostAsync(url, JsonConvert.SerializeObject(req)).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<BaseResult<string>> ChangeSfStatusAsync(Order_ChangeSfStatusReq req)
        {
            var url = "TPP/v3/Order/ChangeSfStatus";
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(req)).ConfigureAwait(false);
            return result;
        }



        /// <summary>
        /// 改变拼团订单状态
        /// </summary>
        /// <param name="OrderCode"></param>
        /// <param name="Mobile"></param>
        /// <param name="Type">类型 1 支付完成 2 超时自动取消 3 已完成 4 已发券 5 券已使用or已转赠 6 </param>
        /// <param name="PayMethod"></param>
        /// <returns></returns>
        public async Task <BaseResult<string>>ChangeGrouponOrderStatusAsync(string OrderCode,string Mobile,int @Type,int PayMethod)
        {
            var data =new  { OrderCode, Mobile, Type = @Type, PayMethod };
 
            var url = "v3/Activity/ChangeGrouponOrderStatus";
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Gets the app version.
        /// </summary>
        /// <returns>The app version.</returns>
        /// <param name="type">Type.</param>
        public async Task<AppVesion> GetAppVersion(int type)
        {
            var url = "/mweb/news/GetAppVersion";
            var data = new { type = type };
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            if (dynamicdata["ResultNo"].ToString() == "00000000") {
                var entity = JsonConvert.DeserializeObject<AppVesion>(dynamicdata["Data"].ToString());
                return entity;
            }

            throw new Exception(dynamicdata["ResultRemark"].ToString());

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<BaseResult<AppVesion>> GetAppVersionAsync(int type)
        {
            var url = "CakeMall/v1/Index/GetAppVersion";
            var data = new { type = type };
            var result = await PostThirdApiAsync<AppVesion>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConfigKey"></param>
        /// <returns></returns>
        public async Task<BaseResult<CommonConfigRsp>> GetConfigByKeyAsync(string ConfigKey)
        {
            var url = "v3/Common/GetConfigByKey";
            var data = new { ConfigKey };
            var result = await PostThirdApiAsync<CommonConfigRsp>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
    }
}
