﻿using NLog;

namespace Casamiel.API.Application.Services
{
    /// <summary>
    /// Nlog service.
    /// </summary>
    public class NlogService:INlogService
    {
        /// <summary>
        /// </summary>
        public NlogService()
        {
        }
        /// <summary>
        /// Gets the loger.
        /// </summary>
        /// <value>The loger.</value>
        public  Logger Logger{

            get { return LogManager.GetLogger("IicApiService"); }
        }
          

    }
}
