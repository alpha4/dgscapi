﻿using System;
namespace Casamiel.API.Application
{
    /// <summary>
    /// Ticket largess service.
    /// </summary>
    public interface ITicketLargessService
    {
        /// <summary>
        /// Adds the ticket.
        /// </summary>
        void AddTicket();
    }

}
