﻿using System;
using System.Threading.Tasks;

namespace Casamiel.API.Application
{

    /// <summary>
    /// 支付服务
    /// </summary>
    public interface IAllPayService
    {

        /// <summary>
        /// Charges the pay complete.
        /// </summary>
        /// <returns><c>true</c>, if pay complete was charged, <c>false</c> otherwise.</returns>
        /// <param name="tradeNO">商户交易号</param>
        /// <param name="OutTradeNO">OutTradeNO </param>
        /// <param name="Amount">Amount.</param>
        /// <param name="MchId"></param>
        Task<bool> PayComplete(string tradeNO, string OutTradeNO, double Amount, string MchId);
	}
}
