﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Casamiel.API.Application.Models;
using Casamiel.API.Application.Models.Mall.Req;
using Casamiel.API.Application.Models.Mall.Rsp;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;

namespace Casamiel.API.Application
{
    /// <summary>
    /// 全国配商城
    /// </summary>
    public interface IMallService
    {
        /// <summary>
        ///获取首页商品数据
        /// </summary>
        /// <returns>The index goods list.</returns>
        void SetIndexGoodsList(ref List<IndexProductBaseRsp> list, ref int code, ref string msg);

        /// <summary>
        /// Gets the index goods list async.
        /// </summary>
        /// <returns>The index goods list async.</returns>
        Task<BaseResult<List<IndexProductBaseRsp>>> GetIndexGoodsListAsync();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<dynamic> GetFreightRule();
        /// <summary>
        /// 获取首页标签列表
        /// </summary>
        /// <returns>The index tags list asyc.</returns>
        void SetIndexTagsList(ref List<IndexTagBaseRsp> list, ref int code, ref string msg);

        /// <summary>
        /// Gets the index tags list async.
        /// </summary>
        /// <returns>The index tags list async.</returns>
        Task<BaseResult<List<IndexTagBaseRsp>>> GetIndexTagsListAsync();

        /// <summary>
        /// 获取收获地址
        /// </summary>
        ///<param name="mobile">mobile</param>
        /// <param name="list">List.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        void SetAddressList(string mobile, ref List<AddressBaseRsp> list, ref int code, ref string msg);

        /// <summary>
        /// Gets the address list.
        /// </summary>
        /// <returns>The address list.</returns>
        /// <param name="mobile">Mobile.</param>
        Task<BaseResult<List<AddressBaseRsp>>> GetAddressListAsync(string mobile);

        /// <summary>
        /// 保存收货地址
        /// </summary>
        /// <param name="data">Data.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        void AddressSaveBase(string data, ref int code, ref string msg);

        /// <summary>
        /// Saves the address async.
        /// </summary>
        /// <returns>The address async.</returns>
        /// <param name="data">Data.</param>
        Task<BaseResult<string>> SaveAddressAsync(string data);


        /// <summary>
        /// 获取地址明细
        /// </summary>
        /// <param name="mobile">Mobile.</param>
        /// <param name="consigneeId">Consignee identifier.</param>
        /// <param name="rsp">Rsp.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        void SetAddressBase(string mobile, int consigneeId, ref AddressBaseRsp rsp, ref int code, ref string msg);

        /// <summary>
        /// Gets the address base async.
        /// </summary>
        /// <returns>The address base async.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <param name="consigneeId">Consignee identifier.</param>
        Task<BaseResult<AddressBaseRsp>> GetAddressBaseAsync(string mobile, int consigneeId);

        /// <summary>
        ///设置默认地址
        /// </summary>
        /// <param name="mobile">Mobile.</param>
        /// <param name="consigneeId">Consignee identifier.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        void SetAddressDefault(string mobile, int consigneeId, ref int code, ref string msg);

        /// <summary>
        /// Sets the address default async.
        /// </summary>
        /// <returns>The address default async.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <param name="consigneeId">Consignee identifier.</param>
        Task<BaseResult<string>> SetAddressDefaultAsync(string mobile, int consigneeId);

        /// <summary>
        /// 删除收获地址
        /// </summary>
        /// <param name="mobile">Mobile.</param>
        /// <param name="consigneeId">Consignee identifier.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        void AddressDelete(string mobile, int consigneeId, ref int code, ref string msg);

        /// <summary>
        /// Addresses the delete async.
        /// </summary>
        /// <returns>The delete async.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <param name="consigneeId">Consignee identifier.</param>
        Task<BaseResult<string>> AddressDeleteAsync(string mobile, int consigneeId);

        /// <summary>
        ///获取分类下产品列表
        /// </summary>
        /// <param name="req">Req.</param>
        /// <param name="list">List.</param>
        /// <param name="total"></param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        void SetGoodsList(GoodsGetListReq req, ref List<ProductBaseRsp> list, ref int total, ref int code, ref string msg);

        /// <summary>
        /// Gets the goods list async.
        /// </summary>
        /// <returns>The goods list async.</returns>
        /// <param name="req">Req.</param>
        Task<PagingResultRsp<List<ProductBaseRsp>>> GetGoodsListAsync(GoodsGetListReq req);

        /// <summary>
        /// 通过产品ProductBaseId 获取商品明细
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="rsp">Rsp.</param>
        /// <param name="total">Total.</param>
        /// <param name="msg">Message.</param>
        void SetGoodDetail(int id, ref GoodsBaseRsp rsp, ref int total, ref string msg);

        /// <summary>
        /// Gets the goods detail async.
        /// </summary>
        /// <returns>The goods detail async.</returns>
        /// <param name="id">Identifier.</param>
        Task<BaseResult<GoodsBaseRsp>> GetGoodsDetailAsync(int id);

        /// <summary>
        /// Adds the shopping cart async.
        /// </summary>
        /// <returns>The shopping cart async.</returns>
        /// <param name="req">Req.</param>
        /// <param name="mobile">Mobile.</param>
        Task<BaseResult<string>> AddShoppingCartAsync(AddShoppingCartReq req, string mobile);

       
        /// <summary>
        /// Shoppings the cart change quantity async.
        /// </summary>
        /// <returns>The cart change quantity async.</returns>
        /// <param name="data">Data.</param>
        /// <param name="mobile">Mobile.</param>
        Task<BaseResult<string>> ShoppingCartChangeQuantityAsync(AddShoppingCartReq data, string mobile);

         
        /// <summary>
        /// Shoppings the cart set check async.
        /// </summary>
        /// <returns>The cart set check async.</returns>
        /// <param name="req">Req.</param>
        Task<BaseResult<string>> ShoppingCartSetCheckAsync(ShoppingCartSetIsCheckReq req);

        /// <summary>
        ///删除购物车商品
        /// </summary>
        /// <param name="req">Req.</param>
        /// <param name="mobile">Mobile.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        void ShoppingCartDelete(List<int> req, string mobile, ref int code, ref string msg);

        /// <summary>
        /// Shoppings the cart delete async.
        /// </summary>
        /// <returns>The cart delete async.</returns>
        /// <param name="req">Req.</param>
        /// <param name="mobile">Mobile.</param>
        Task<BaseResult<string>> ShoppingCartDeleteAsync(List<int> req, string mobile);
        /// <summary>
        /// 获取购物车商品集合
        /// </summary>
        /// <param name="rsp">Rsp.</param>
        /// <param name="mobile">Mobile.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        void SetShoppingCartList(ref List<ShoppingCartBaseRsp> rsp, string mobile, ref int code, ref string msg);

        /// <summary>
        /// Gets the shopping cart list async.
        /// </summary>
        /// <returns>The shopping cart list async.</returns>
        /// <param name="mobile">Mobile.</param>
        Task<BaseResult<List<ShoppingCartBaseRsp>>> GetShoppingCartListAsync(string mobile);

        /// <summary>
        /// 创建订单.
        /// </summary>

        Task<BaseResult<OrderCreateRsp>> CreateOrder(OrderCreateReq req);

        /// <summary>
        /// 获取订单信息
        /// </summary>
        /// <param name="mobile">Mobile.</param>
        /// <param name="ordercode">Ordercode.</param>
        Task<BaseResult<MWebOrderBaseRsp>> GetOrderBase(string mobile, string ordercode);

        /// <summary>
        /// Orders the change status.
        /// </summary>
        /// <returns>The change status.</returns>
        /// <param name="ordercode">订单编号</param>
        /// <param name="mobile"></param>
        /// <param name="type">1 支付完成 2 确认收货 3 用户取消订单 4 超时自动取消.</param>
        /// <param name="payMethod">支付方式 只有在Type为1 可用</param>
        /// <param name="resBillNo">处理完成单号 </param>
        Task<BaseResult<string>> OrderChangeStatus(string ordercode, string mobile, int type, int payMethod, string resBillNo);

        /// <summary>
        /// Gets the order list.
        /// </summary>
        /// <returns>The order list.</returns>
        /// <param name="status">订单状态 99 全部订单 1 待付款 2 待取货 3 已完成</param>
        /// <param name="mobile">Mobile.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        Task<PagingResultRsp<List<MWebOrderListRsp>>> GetOrderList(int status, string mobile, int pageIndex, int pageSize);
        /// <summary>
        ///获取展位数据
        /// </summary>
        /// <returns>The booth list.</returns>
        /// <param name="code">Code.</param>
        Task<BaseResult<List<MWeb_Booth_GetListRsp>>> GetBoothList(string code);
        /// <summary>
        /// 获取单个图文素材信息
        /// </summary>
        /// <returns>The new by identifier.</returns>
        /// <param name="Id">Identifier.</param>
        Task<BaseResult<MWebNewsBaseRsp>> GetNewById(int Id);


    }
}
