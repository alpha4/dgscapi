﻿using System;
using Casamiel.API.Application.Models;

namespace Casamiel.API.Application
{
    /// <summary>
    /// 库存服务
    /// </summary>
    public interface IStockService
    {
		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
        void AddQueue(StockPush info);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        long GetUnpushtime();
    }
}
