﻿using System;
using NLog;

namespace Casamiel.API.Application
{
    /// <summary>
    /// Nlog service.
    /// </summary>
    public interface INlogService
    {
        /// <summary>
        /// Gets the loger.
        /// </summary>
        /// <value>The loger.</value>
        Logger Logger { get; }
    }
}
