﻿using System;
using Casamiel.API.Application.Models;

namespace Casamiel.API.Application
{
    /// <summary>
    /// 支付接口服务
    /// </summary>
    public interface IPayGateWayService
    {
        /// <summary>
        /// Creates the wechatpay order.
        /// </summary>
        /// <returns>The wechatpay order.</returns>
        /// <param name="outTradeNo">Out trade no.</param>
        /// <param name="amount">Amount.</param>
        /// <param name="shopname"></param>
        /// <param name="shopno"></param>
        string CreateWechatpayOrder(string outTradeNo,double amount,string shopname,string shopno);


        /// <summary>
        /// 支付宝小程序支付
        /// </summary>
        /// <param name="buyerid"></param>
        /// <param name="outTradeNo"></param>
        /// <param name="amount"></param>
        /// <param name="shopname"></param>
        /// <param name="shopid"></param>
        /// <returns></returns>
        string CreateAlipayAppletOrder(string buyerid, string outTradeNo, double amount, string shopname, string shopid);
        /// <summary>
        /// Creates the mini wechatpay order.
        /// </summary>
        /// <returns>The mini wechatpay order.</returns>
        /// <param name="outTradeNo">Out trade no.</param>
        /// <param name="amount">Amount.</param>
        /// <param name="openId">Open identifier.</param>
        /// <param name="appId">小程序appId</param>
        /// <param name="shopname"></param>
        /// <param name="shopno"></param>
        /// <param name="timeExpire">超时支付时间(yyyyMMddHHmmss)</param>
        string CreateMiniWechatpayOrder(string outTradeNo, double amount, string openId, string appId,string shopname,string shopno, string timeExpire="");

        /// <summary>
        /// Creates the alipay order.
        /// </summary>
        /// <returns>The alipay order.</returns>
        /// <param name="outTradeNo">Out trade no.</param>
        /// <param name="amount">Amount.</param>
        /// <param name="shopname"></param>
        /// <param name="shopno"></param>
        string CreateAlipayOrder(string outTradeNo, double amount,string shopname,string shopno);
        /// <summary>
        /// Creates the public wechat pay.
        /// </summary>
        /// <returns>The public wechat pay.</returns>
        /// <param name="outTradeNo">Out trade no.</param>
        /// <param name="amount">Amount.</param>
        /// <param name="openId">Open identifier.</param>
        string CreatePublicWechatPay(string outTradeNo, double amount, string openId);

    }
}
