﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application
{
    /// <summary>
    /// Mobile check code.
    /// </summary>
    public interface IMobileCheckCode
    {
        /// <summary>
        /// 发送验证码
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
         Task<string> SendCheckCodeAsync(string mobile);
        /// <summary>
        /// 校验验证码
        /// 修改密码时再次验证验证码是否正确,但不验证有效期,仅做安全处理
        /// </summary>
        /// <param name="phone">接收验证的手机号码</param>
        /// <param name="code">验证码</param>
        /// <param name="isCheckOut">是否验证过期时间</param>
        /// <returns></returns>
        JObject CheckCode(string phone, string code, bool isCheckOut);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="code"></param>
        /// <param name="isCheckOut"></param>
        /// <returns></returns>
        JObject CheckSmsCode (string phone, string code, bool isCheckOut);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="phone"></param>
		/// <param name="code"></param>
		/// <param name="isCheckOut"></param>
		/// <returns></returns>
		Task<JObject> CheckSmsCodeAsync(string phone, string code, bool isCheckOut);
	}
}
