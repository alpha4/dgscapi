﻿using System;
using System.Threading.Tasks;
using Casamiel.API.Application.Models;

namespace Casamiel.API.Application
{

    /// <summary>
    /// 已支付订单退款接口
    /// </summary>
    public interface IRefundService
    {
        /// <summary>
        /// OrderRefund the specified data.
        /// </summary>
        /// <returns>The refund.</returns>
        /// <param name="data">Data.</param>
        Task<Zmodel> OrderRefund(OrderRefundReq data);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="IcTradeNO"></param>
		/// <returns></returns>
		Task<Zmodel> Icchargeback(string IcTradeNO);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="OutTradeNO"></param>
        /// <returns></returns>
        Task<Zmodel> IcchargebackByOutTradeNO(string OutTradeNO);

        /// <summary>
        /// 拼团订单退款
        /// </summary>
        /// <param name="OrderCode"></param>
        /// <returns></returns>

        Task<Zmodel> GroupPurchaseOrderRefund(string OrderCode);


    }
}
