﻿using Casamiel.API.Application.Models;
using Casamiel.API.Application.Models.Ele;
using Casamiel.API.Application.Models.Store;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Request.Vote;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Vote;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Casamiel.Domain.Request.Vote.GetVoteListReq;

namespace Casamiel.API.Application
{
    /// <summary>
    /// 
    /// </summary>
    public interface IStoreApiService
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="OpenId"></param>
        /// <returns></returns>
        Task<BaseResult<List<VoteListRsp>>> GetVoteListAsync(string OpenId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        Task<BaseResult<string>> AddVoteAsync(VoteAddReq req);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        Task<BaseResult<VoteDetailRsp>> GetVoteDetail(VoteGetDetailReq req);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="storeid"></param>
        /// <param name="productid"></param>
        /// <returns></returns>
        Task<ProductBase> GetGoodsById(string storeid, string productid);

        /// <summary>
        /// IndexGoodsList
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns></returns>
        Task<List<IndexGoods>> GetIndexGoodsList(int storeId);

        /// <summary>
        /// 我的购物车
        /// </summary>
        /// <param name="mobile">手机号</param>
        /// <returns></returns>
        Task<List<ShoppingCartBaseRsp>> GetMyCartList(string mobile);

        /// <summary>
        /// 购物车
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="GoodsId"></param>
        /// <param name="GoodsQuantity"></param>
        /// <param name="Mobile"></param>
        /// <returns></returns>
        Task<string> ShoppingCartInsetOrUpate(int Id, int GoodsId, int GoodsQuantity, string Mobile = "");
        /// <summary>
        /// 选中购物车Item
        /// </summary>
        /// <param name="Ids">ID集，英文，隔开</param>
        /// <param name="isCheck"></param>
		/// <param name="Mobile"></param>
        /// <returns></returns>
        Task<string> ShoppingCartItemsChecked(string Ids, bool isCheck, string Mobile);
        /// <summary>
        /// 删除购物车Item
        /// </summary>
        /// <param name="Ids">ID集，英文，隔开</param>
		/// <param name="Mobile">手机号</param>
        /// <returns></returns>
        Task<string> ShoppingCartItemsDelete(string Ids, string Mobile);
        /// <summary>
        /// 更换门店
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        Task<string> ChangeCartStore(int storeId, string mobile);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderCode"></param>
        /// <param name="mobile"></param>
        /// <param name="type"></param>
        /// <param name="payMethod"></param>
        /// <param name="resBillNo"></param>
        /// <returns></returns>
        Task<BaseResult<string>> CakeOrderChangeStatus(string orderCode, string mobile, int type, int payMethod, string resBillNo);

        /// <summary>
        /// Gets the goods get base.
        /// </summary>
        /// <returns>The goods get base.</returns>
        /// <param name="storeid">Storeid.</param>
        /// <param name="GoodsId">Goods identifier.</param>
        Task<GoodsGetBaseRsp> GetGoodsBase(int storeid, int GoodsId);

        /// <summary>
        /// Creates the single order.
        /// </summary>
        /// <returns>The single order.</returns>
        /// <param name="data">Data.</param>
        Task<string> CreateSingleOrder(Order_AddSingleReq data);


        /// <summary>
        /// Gets the order by order code async.
        /// </summary>
        /// <returns>The order by order code async.</returns>
        /// <param name="orderCode">Order code.</param>
        /// <param name="mobile">Mobile.</param>
        Task<OrderBaseRsp> GetOrderByOrderCodeAsync(string orderCode, string mobile);

        /// <summary>
        /// Gets the goods list.
        /// </summary>
        /// <returns>The goods list.</returns>
        /// <param name="TagId">Tag identifier.</param>
        /// <param name="pageindex">Pageindex.</param>
        /// <param name="pagesize">Pagesize.</param>
        Task<string> GetGoodsList(int TagId, int pageindex, int pagesize);

        /// <summary>
        /// my order list 
        /// </summary>
        /// <returns>The MYO rder list.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <param name="pageindex">Pageindex.</param>
        /// <param name="pagesize">Pagesize.</param>
        /// <param name="OrderStatus"></param>
        Task<string> GetMYOrderList(string mobile, int pageindex, int pagesize, int OrderStatus);
        /// <summary>
        /// Gets my order list.
        /// </summary>
        /// <param name="mobile">Mobile.</param>
        /// <param name="pageindex">Pageindex.</param>
        /// <param name="pagesize">Pagesize.</param>
        /// <param name="OrderStatus">Order status.</param>
        /// <param name="list">List.</param>
        /// <param name="total">Total.</param>
        void GetMyOrderList(string mobile, int pageindex, int pagesize, int OrderStatus, ref List<OrderBaseRsp> list, ref int total);
        /// <summary>
        /// 获取首页Tag
        /// </summary>
        /// <returns>The index tag list.</returns>
        Task<List<TagModel>> GetIndexTagList();
        /// <summary>
        /// 我的提货单明细
        /// </summary>
        /// <returns>The take order detail.</returns>
        /// <param name="orderCode">Order code.</param>
        /// <param name="mobile">Mobile.</param>
        /// <exception cref="TaskCanceledException"></exception>
        Task<OrderTakeBaseRsp> GetTakeOrderDetail(string orderCode, string mobile);

        /// <summary>
        /// 幻灯片
        /// </summary>
        /// <returns>The slide get lists.</returns>
        /// <param name="code">Code.（AppIndex）</param>
        Task<List<SlideGetListRsp>> GetSlideList(string code);

        /// <summary>
        /// 幻灯片
        /// </summary>
        /// <returns>The slide get lists.</returns>
        /// <param name="code">Code.（AppIndex）</param>
        Task<List<SlideGetListRsp>> GetSlideListAsync(string code);
        /// <summary>
        /// Gets the new by identifier.
        /// </summary>
        /// <returns>The new by identifier.</returns>
        /// <param name="id">Identifier.</param>
        Task<NewsBaseRsp> GetNewsById(int id);

        /// <summary>
        /// Adds the collect async.
        /// </summary>
        /// <returns>The collect async.</returns>
        /// <param name="productBaseId">Product base identifier.</param>
        /// <param name="mobile">Mobile.</param>
        Task AddCollectAsync(int productBaseId, string mobile);
        /// <summary>
        /// Products the check collect.
        /// </summary>
        /// <returns>The check collect.</returns>
        /// <param name="productBaseId">Product base identifier.</param>
        /// <param name="mobile">Mobile.</param>
        Task<bool> ProductCheckCollect(int productBaseId, string mobile);
        /// <summary>
        /// Cancels the collect async.
        /// </summary>
        /// <returns>The collect async.</returns>
        /// <param name="productBaseId">Product base identifier.</param>
        /// <param name="mobile">Mobile.</param>
        Task CancelCollectAsync(int productBaseId, string mobile);


        /// <summary>
        /// 改变拼团订单状态
        /// </summary>
        /// <param name="OrderCode"></param>
        /// <param name="Mobile"></param>
        /// <param name="Type">类型 1 支付完成 2 超时自动取消 3 已完成 4 已发券 5 券已使用or已转赠 6 已退款</param>
        /// <param name="PayMethod"></param>
        /// <returns></returns>
        Task<BaseResult<string>> ChangeGrouponOrderStatusAsync(string OrderCode, string Mobile, int @Type, int PayMethod);

        /// <summary>
        /// Clears the collect async.
        /// </summary>
        /// <returns>The collect async.</returns>
        /// <param name="mobile">Mobile.</param>
        Task ClearCollectAsync(string mobile);

        /// <summary>
        /// Gets the collect list async.
        /// </summary>
        /// <returns>The collect list async.</returns>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="mobile">Mobile.</param>
        Task<string> GetCollectListAsync(int pageIndex, int pageSize, string mobile);
        /// <summary>
        /// Gets the list.
        /// </summary>
        /// <returns>The list.</returns>
        /// <param name="mobile">Mobile.</param>
        Task<List<AddressBaseRsp>> GetList(string mobile);

        /// <summary>
        /// Gets the base.
        /// </summary>
        /// <returns>The base.</returns>
        /// <param name="id">Identifier.</param>
        /// <param name="mobile">Mobile.</param>
        Task<AddressBaseRsp> GetBase(int id, string mobile);

        /// <summary>
        /// Saves the address.
        /// </summary>
        /// <returns>The address.</returns>
        /// <param name="req">Req.</param>
        Task<BaseResult<string>> SaveAddress(AddressSaveReq req);
        /// <summary>
        /// 
        /// Deletes the address.
        /// </summary>
        /// <returns>The address.</returns>
        /// <param name="id">Identifier.</param>
        /// <param name="mobile">Mobile.</param>
        Task<string> DeleteAddress(int id, string mobile);
        /// <summary>
        /// Sets the address default.
        /// </summary>
        /// <returns>The address default.</returns>
        /// <param name="id">Identifier.</param>
        /// <param name="mobile">Mobile.</param>
        Task<string> SetAddressDefault(int id, string mobile);
        /// <summary>
        /// Gets the list by degree.
        /// </summary>
        /// <returns>The list by degree.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <param name="storeId">Store identifier.</param>
        Task<List<AddressBaseRsp>> GetListByDegree(string mobile, int storeId);

        /// <summary>
        /// 保存配送收货地址
        /// </summary>
        /// <returns>The save.</returns>
        /// <param name="req">Req.</param>
        Task<BaseResult<AddressBaseRsp>> Save(AddressSaveReq req);

        /// <summary>
        /// Checks the location.
        /// </summary>
        /// <returns>The location.</returns>
        /// <param name="storeId">Store identifier.</param>
        /// <param name="consigneeId">Consignee identifier.</param>
        Task<ThirdResult<string>> CheckLocation(int storeId, int consigneeId);
        /// <summary>
        /// 登陆
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<string> Login(string username, string password);

        /// <summary>
        /// 登出
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<string> LogOut(string token);
        /// <summary>
        /// 检查token有效
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<string> CheckToken(string token);

        /// <summary>
        /// 检查token有效
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<BaseResult<string>> CheckTokenAsync(string token);
        /// <summary>
        /// Gets GetGoodsListForTmall goods list.
        /// </summary>
        /// <returns>The goods list.</returns>
        ///<param name="title"></param>
        ///<param name="token"></param>
        Task<string> GetGoodsListForTmall(string token, string title);


        /// <summary>
        /// 添加推荐记录(东哥专用)
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        Task AddReferral(string mobile, string code);

        /// <summary>
        /// 创建天猫订单
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<string> AddTmallOrder(TMallAddOrderReq data);

        /// <summary>
        /// Gets the tmall order list.
        /// </summary>
        /// <returns>The tmall order list.</returns>
        /// <param name="data">Data.</param>
        Task<string> GetTmallOrderList(TmallOrderListReq data);
     
        /// <summary>
        /// 美团订单
        /// </summary>
        /// <returns>The mt order.</returns>
        /// <param name="data">Data.</param>
        Task<string> AddMtOrder(AddMeiTuanOrderReq data);

        /// <summary>
        /// 美团订单
        /// </summary>
        /// <returns>The mt order.</returns>
        /// <param name="data">Data.</param>
        Task<BaseResult<string>> AddMtOrderV2Async(AddMeiTuanOrderReq data);
        
        
        /// <summary>
        /// 改变达达订单状态
        /// </summary>
        /// <returns>The TM all dada status.</returns>
        /// <param name="req">Req.</param>
        Task<string> ChangeDadaStatus(ChangeDadaStatusReq req);
        /// <summary>
        /// 删除订单
        /// </summary>
        /// <param name="orderCode"></param>
        /// <returns></returns>
        Task<string> OrderDelete(string orderCode);
        /// <summary>
        /// Sends the notice.
        /// </summary>
        /// <returns>The notice.</returns>
        /// <param name="orderCode">Order code.</param>
        Task<string> SendOrderNotice(string orderCode);
        /// <summary>
        /// Meituans the cancel.
        /// </summary>
        /// <returns>The cancel.</returns>
        /// <param name="orderId">Order identifier.</param>
        /// <param name="reasonCode">Reason code.</param>
        /// <param name="reason">Reason.</param>
        Task<string> MeituanCancel(long orderId, string reasonCode, string reason);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="reasonCode"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        Task<BaseResult<string>> MeituanCancelV2Async(long orderId, string reasonCode, string reason);
        /// <summary>
        /// Meituans the complate.
        /// </summary>
        /// <returns>The complate.</returns>
        /// <param name="data">Data.</param>
        Task<string> MeituanComplate(AddMeiTuanOrderReq data);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<BaseResult<string>> MeituanComplateV2Async(AddMeiTuanOrderReq data);
        /// <summary>
        /// Meituans the complate2.
        /// </summary>
        /// <returns>The complate2.</returns>
        /// <param name="orderId">Order identifier.</param>
        Task<string> MeituanComplate2(string orderId);

        /// <summary>
        /// 意外情况下需完成订单接口
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        Task<BaseResult<string>> MeituanComplate2V2Async(string orderId);
        /// <summary>
        /// Meituans the refund.
        /// </summary>
        /// <returns>The refund.</returns>
        /// <param name="notifyType">apply 发起退款
        ///agree 确认退款
        ///reject 驳回退款
        ///cancelRefund 用户取消退款申请
        ///cancelRefundComplaint 取消退款申诉</param>
        /// <param name="orderId">Order identifier.</param>
        /// <param name="reason">Reason.</param>
        Task<string> MeituanRefund(string notifyType, long orderId, string reason);// 退款处理

        /// <summary>
        /// Meituans the refund.
        /// </summary>
        /// <returns>The refund.</returns>
        /// <param name="notifyType">apply 发起退款
        ///agree 确认退款
        ///reject 驳回退款
        ///cancelRefund 用户取消退款申请
        ///cancelRefundComplaint 取消退款申诉</param>
        /// <param name="orderId">Order identifier.</param>
        /// <param name="reason">Reason.</param>

        Task<BaseResult<string>> MeituanRefundV2Async(string notifyType, long orderId, string reason);
        /// <summary>
        /// Meituans the change express status.
        /// </summary>
        /// <returns>The change express status.</returns>
        /// <param name="dispatcherMobile">Dispatcher mobile.</param>
        /// <param name="dispatcherName">Dispatcher name.</param>
        /// <param name="orderId">Order identifier.</param>
        /// <param name="shippingStatus">Shipping status.</param>
        /// <param name="time">Time.</param>
        Task<string> MeituanChangeExpressStatus(string dispatcherMobile, string dispatcherName, long orderId, int shippingStatus, int time);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dispatcherMobile"></param>
        /// <param name="dispatcherName"></param>
        /// <param name="orderId"></param>
        /// <param name="shippingStatus"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        Task<BaseResult<string>> MeituanChangeExpressStatusV2Async(string dispatcherMobile, string dispatcherName, long orderId, int shippingStatus, int time);

        /// <summary>
        /// 同步到达达
        /// </summary>
        /// <returns>The shop list for dada.</returns>
        Task<string> GetShopListForDada();

        /// <summary>
        /// Gets the group identifier.
        /// </summary>
        /// <returns>The group identifier.</returns>
        /// <param name="Id">Identifier.</param>
        Task<string> GetGroupId(string Id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ThirdResult<int>> GetGroupIdAsync(int Id);

        /// <summary>
        /// GetDrinksGoodsId
        /// </summary>
        /// <returns></returns>
        Task<string> GetDrinksGoodsId();

        /// <summary>
        /// Meituans the tradedetail.
        /// </summary>
        /// <returns>The tradedetail.</returns>
        /// <param name="data">Data.</param>
        Task<string> MeituanTradedetail(JObject data);

        ///
        Task<BaseResult<string>> MeituanTradedetailV2Async(JObject data);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<BaseResult<string>> EleNotifyAsync(NotifyMessageReq data);

        /// <summary>
        /// Changes the sf status.
        /// </summary>
        /// <returns>The sf status.</returns>
        /// <param name="req">Req.</param>
        Task<string> ChangeSfStatus(Order_ChangeSfStatusReq req);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        Task<BaseResult<string>> ChangeSfStatusAsync(Order_ChangeSfStatusReq req);

        /// <summary>
        /// Gets the app version.
        /// </summary>
        /// <returns>The app version.</returns>
        /// <param name="type">Type.</param>
        Task<AppVesion> GetAppVersion(int type);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        Task<BaseResult<AppVesion>> GetAppVersionAsync(int type);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConfigKey"></param>
        /// <returns></returns>
        Task<BaseResult<CommonConfigRsp>> GetConfigByKeyAsync(string ConfigKey);
    }
}
