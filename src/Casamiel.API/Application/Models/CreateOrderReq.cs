﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Create order req.
    /// </summary>
    public class CreateOrderReq
    {
        /// <summary>
        /// 门店id
        /// </summary>
        [Required(ErrorMessage = "门店id必填")]
        public int storeid { get; set; }
        /// <summary>
        /// 门店关联id
        /// </summary>
        [Required(ErrorMessage = "门店关联id必填")]
        public int storeRelationId { get; set; }
        /// <summary>
        /// 产品sku id
        /// </summary>
        [Required(ErrorMessage = "产品sku id必填")]
        public int goodsBaseId { get; set; }
        /// <summary>
        /// 0自提，1配送
        /// </summary>
        /// <value>The type of the buy.</value>
        public int buyType { get; set; }
        ///// <summary>
        ///// 产品sku关联Id
        ///// </summary>
        //[Required(ErrorMessage = "产品sku关联Id必填")]
        //public int relationId { get; set; }

        /// <summary>
        /// 提货人
        /// </summary>
        [Required(ErrorMessage ="提货人必填")]
        public string TakeName { get; set; }
        /// <summary>
        /// 提货人联系方式
        /// </summary>
        [StringLength(11,MinimumLength =11,ErrorMessage = "请输入11位手机号")]
        
        [Required(ErrorMessage ="提货人联系方式")]
        public string TakeMobile { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        [Required(ErrorMessage = "数量必填")]
        public int count { get; set; }
        /// <summary>
        /// 自提时间
        /// </summary>
        [Required(ErrorMessage = "自提时间必填")]
        public DateTime picktime { get; set; }

        /// <summary>
        /// Gets or sets the remark.
        /// </summary>
        /// <value>The remark.</value>
        public string remark { get; set; }
        /// <summary>
        /// 优惠券id
        /// </summary>
        public int DiscountCouponId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <value>The cardno.</value>
        public string cardno { get; set; }
        /// <summary>
        /// Gets or sets the paycardno.
        /// </summary>
        /// <value>The paycardno.</value>
        public string Paycardno {get; set;}
        /// <summary>
        /// 优惠券金额
        /// </summary>
        public decimal DiscountCouponMoney { get; set; }
        /// <summary>
        /// 支付方式 1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
        /// </summary>
        /// <value>The paymeothd.</value>
        [Required(ErrorMessage = "支付方式必填")]
        public int payMethod { get; set; }

        /// <summary>
        /// 小程序专用Id
        /// </summary>
        /// <value>The identifier.</value>
        public int id { get; set; }

        /// <summary>
        /// 地址Id
        /// </summary>
        public int ConsigneeId { get; set; }
        //{"storeid":1,"storeRelationId":1,"goodsBaseId": 1,"relationId": 141783,"count":1,"picktime":"2018-04-20 12:30"}
    }
}
