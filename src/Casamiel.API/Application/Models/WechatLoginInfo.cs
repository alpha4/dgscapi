﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    public class WechatLoginSuccessParm
    {
       public string openid { get; set; }
        public string session_key { get; set; }
        public string unionid { get; set; }
        public string errmsg { get; set; }
        public int errcode { get; set; }
    }
    public class WechatLoginParm
    {
        public string code { get; set; }
    }

    /// <summary>  
    /// 微信小程序登录信息结构  
    /// </summary>  
    public class WechatLoginInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string code { get; set; }
        /// <summary>
        /// 用户信息对象，不包含 openid 等敏感信息
        /// </summary>
        public string userInfo { get; set; }
        /// <summary>
        /// 不包括敏感信息的原始数据字符串，用于计算签名
        /// </summary>
        public string rawData { get; set; }
        /// <summary>
        /// 使用 sha1( rawData + sessionkey ) 得到字符串，用于校验用户信息
        /// </summary>
        public string signature { get; set; }
        /// <summary>
        /// 包括敏感数据在内的完整用户信息的加密数据
        /// </summary>
        public string encryptedData { get; set; }
        /// <summary>
        /// 加密算法的初始向量
        /// </summary>
        public string iv { get; set; }
    }
}
