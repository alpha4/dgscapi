﻿using System;
namespace Casamiel.API.Application.Models.Meituan
{
 /// <summary>
 /// Data.
 /// </summary>
public class MtOrderData
{
    /// <summary>
    /// 
    /// </summary>
    public int CTime { get; set; }
    /// <summary>
    /// [预定人]13967142081 收餐人隐私号 15658874684_7948，手机号 139****2081 5人用餐
    /// </summary>
    public string Caution { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int CityId { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string DaySeq { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int DeliveryTime { get; set; }
    /// <summary>
    /// [{"app_food_code":"2777","box_num":1,"box_price":0,"cart_id":0,"food_discount":1,"food_name":"金色年华","food_property":"","price":118,"quantity":1,"sku_id":"2777","spec":"6寸","unit":"份"}]
    /// </summary>
    public string Detail { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int DinnersNumber { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string EPoiId { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string Extras { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string Favorites { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int HasInvoiced { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string InvoiceTitle { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int IsPre { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int IsThirdShipping { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public double Latitude { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int LogisticsCancelTime { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string LogisticsCode { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int LogisticsCompletedTime { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int LogisticsConfirmTime { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string LogisticsDispatcherMobile { get; set; }
    /// <summary>
    /// 秦建
    /// </summary>
    public string LogisticsDispatcherName { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int LogisticsFetchTime { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int LogisticsId { get; set; }
    /// <summary>
    /// 混合加盟
    /// </summary>
    public string LogisticsName { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int LogisticsSendTime { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int LogisticsStatus { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public double Longitude { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int OrderCompletedTime { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int OrderConfirmTime { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public long OrderId { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public long OrderIdView { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int OrderSendTime { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public decimal OriginalPrice { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int PayType { get; set; }
    /// <summary>
    /// 杭州市萧山区育才路470号（育才路与文化路交叉口）
    /// </summary>
    public string PoiAddress { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string PoiFirstOrder { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int PoiId { get; set; }
    /// <summary>
    /// 可莎蜜兒(育才店)
    /// </summary>
    public string PoiName { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string PoiPhone { get; set; }
    /// <summary>
    /// {"actOrderChargeByMt":[{"comment":"活动款","feeTypeDesc":"活动款","feeTypeId":10019,"moneyCent":0}],"actOrderChargeByPoi":[],"foodShareFeeChargeByPoi":1770,"logisticsFee":410,"onlinePayment":12210,"wmPoiReceiveCent":10030}
    /// </summary>
    public string PoiReceiveDetail { get; set; }
    /// <summary>
    /// 萧山育才东苑 (39幢2单元3O2室)@#浙江省杭州市萧山区北干山南路北干山南路育才东苑
    /// </summary>
    public string RecipientAddress { get; set; }
    /// <summary>
    /// 黄文(女士)
    /// </summary>
    public string RecipientName { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string RecipientPhone { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string ShipperPhone { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public decimal ShippingFee { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int Status { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string TaxpayerId { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public decimal Total { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int uTime { get; set; }
}

    /// <summary>
    /// Order view.
    /// </summary>
public class OrderView
{
    /// <summary>
    /// 
    /// </summary>
    public MtOrderData Data { get; set; }
}
}
