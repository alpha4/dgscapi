﻿using System;
using System.Collections.Generic;

namespace Casamiel.API.Application.Models.Meituan
{
    /// <summary>
    /// Cate2.
    /// </summary>
    public class DishCategory
    {
        /// <summary>
        /// 素菜
        /// </summary>
        public string categoryName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int dishId { get; set; }
        /// <summary>
        /// 干煸豆角
        /// </summary>
        public string dishName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string eDishCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<WaiMaiDishSkuBasesItem> waiMaiDishSkuBases { get; set; }
    }
    /// <summary>
    /// Wai mai dish sku bases item.
    /// </summary>
    public class WaiMaiDishSkuBasesItem
    {
        /// <summary>
        /// 
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int dishSkuId { get; set; }
        /// <summary>
        /// 干煸豆角
        /// </summary>
        public string dishSkuName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string eDishSkuCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal price { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string spec { get; set; }
    }
}
