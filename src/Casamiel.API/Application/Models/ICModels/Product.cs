﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Casamiel.API.Application.Models.ICModels
{
 
    /// <summary>
    /// Product rsp.
    /// </summary>
    public class Product
    {
        /// <summary>
        /// 产品标识
        /// </summary>
        /// <value>The pid.</value>
        public int Pid { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        /// <value>The pname.</value>
        public string Pname { get; set; }
        /// <summary>
        /// Gets or sets the pno.
        /// </summary>
        /// <value>The pno.</value>
        public string Pno { get; set; }
        /// <summary>
        /// 计量单位
        /// </summary>
        /// <value>The unit.</value>
        public string Dwmc { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        /// <value>The price.</value>
        public decimal Price { get; set; }


    }
    /// <summary>
    /// Bom.
    /// </summary>
    public class Bom : Product
    {
		/// <summary>
		/// 数量
		/// </summary>
		/// <value>The count.</value>
#pragma warning disable IDE1006 // 命名样式
		public int count { get; set; }
#pragma warning restore IDE1006 // 命名样式
	}
    /// <summary>
    /// Product rsp.
    /// </summary>
    public class ProductRsp : Product
    {
		/// <summary>
		/// Gets or sets the bomlist.
		/// </summary>
		/// <value>The bomlist.</value>
#pragma warning disable CA2227 // 集合属性应为只读
#pragma warning disable IDE1006 // 命名样式
		public List<Bom> bomlist { get; set; }
#pragma warning restore IDE1006 // 命名样式
#pragma warning restore CA2227 // 集合属性应为只读
	}
}
