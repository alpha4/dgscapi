﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models.ICModels
{

    /// <summary>
    /// IC产品价格
    /// </summary>
    public class ICPriceRoot
    {
        /// <summary>
        /// 
        /// </summary>
        public List<ProductItem> Product { get; set; }


    }

    /// <summary>
    /// 
    /// </summary>
    public class ProductItem
    {
        /// <summary>
        /// 
        /// </summary>
        public string Pid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Pname { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Originprice { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Icprice { get; set; }
    }
}
