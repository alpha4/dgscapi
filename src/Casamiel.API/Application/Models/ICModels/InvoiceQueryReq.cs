﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models.ICModels
{
    /// <summary>
    /// Invoice query req.
    /// </summary>
    public class InvoiceQueryReq
    {
        /// <summary>
        /// Gets or sets the sellerphone.
        /// </summary>
        /// <value>The sellerphone.</value>
        [Required(ErrorMessage = "业务员手机号不能空")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "手机号11位")]
        public string Sellerphone { get; set; }

        /// <summary>
        ///对应单据
        /// </summary>
        /// <value>The list.</value>
        [Required]
#pragma warning disable CA2227 // 集合属性应为只读
#pragma warning disable IDE1006 // 命名样式
		public List<Orderinfo> list { get; set; }
#pragma warning restore IDE1006 // 命名样式
#pragma warning restore CA2227 // 集合属性应为只读

		/// <summary>
		/// Gets or sets the timestamp.
		/// </summary>
		/// <value>The timestamp.</value>
		public string Timestamp { get; set; }

        


    }
    /// <summary>
    /// Orderinfo.
    /// </summary>
    public class Orderinfo
    {
        /// <summary>
        /// 单据
        /// </summary>
        /// <value>The tradeno.</value>
        [Required]
        public string Tradeno { get; set; }
    }
}
