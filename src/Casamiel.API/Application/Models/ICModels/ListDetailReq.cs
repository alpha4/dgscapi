﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models.ICModels
{
    /// <summary>
    /// 单据详情
    /// </summary>
    public class ListDetailReq
    {
        /// <summary>
        /// 单据
        /// </summary>
        /// <value>The tradeno.</value>
        [Required(ErrorMessage ="单据不能为空")]
        [StringLength(32,MinimumLength =8,ErrorMessage ="单据长度8～32位")]
        public string Tradeno { get; set; }
        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>The timestamp.</value>
        public string Timestamp { get; set; }
    }
}
