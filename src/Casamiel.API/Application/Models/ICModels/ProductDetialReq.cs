﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models.ICModels
{
    /// <summary>
    /// Product detial req.
    /// </summary>
    public class ProductDetailReq
    {
        /// <summary>
        /// 产品标识
        /// 
        /// </summary>
        /// <value>The pid.</value>

        public int Pid { get; set; }
    }
}
