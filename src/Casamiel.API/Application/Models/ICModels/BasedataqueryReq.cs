﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models.ICModels
{
    /// <summary>
    /// Basedataquery req.
    /// </summary>
    public class BasedataqueryReq
    {

        /// <summary>
        /// 
        /// </summary>
        public int Pageindex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Pagesize { get; set; }


        /// <summary>
        ///   
        /// 数据类型，1、产品；2、员工；3、销售客户；4、会员客户; 11、可以用来查团购客户
        /// </summary>
        [Required(ErrorMessage = "数据类型必填")]
        public int Datatype { get; set; }

        /// <summary>
        /// Gets or sets the datavalue.
        /// </summary>
        /// <value>The datavalue.</value>
        [Required(ErrorMessage = "关键字必填")]
        public string Datavalue { get; set; }

    }
}
