﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using Newtonsoft.Json;

namespace Casamiel.API.Application.Models.ICModels
{
    /// <summary>
    /// 库存推送计划
    /// </summary>
    public class StockpushPlanReq
    {
        /// <summary>
        /// Gets or sets a value indicating whether this
        /// </summary>
        /// <value><c>true</c> if start; otherwise, <c>false</c>.</value>
        public bool Start { get; set; } = true;

        /// <summary>
        /// Gets or sets the pushurl.
        /// </summary>
        /// <value>The pushurl.</value>
        [Required(ErrorMessage = "推送地址不能为空")]
        public string Pushurl { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>The timestamp.</value>

        public string Timestamp { get; set; } = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
    }
}
