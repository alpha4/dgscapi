﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Casamiel.API.Application.Models.ICModels
{
    /// <summary>
    /// List query req.
    /// 单据清单查询
    /// </summary>
    public class ListQueryReq
    {

        /// <summary>
        /// 关键字，单号，客户信息
        /// </summary>
        /// <value>The searchtext.</value>
        public string Searchtext { get; set; }
        //wxopenid：微信id，可不填，如果填写且也填写了卡号，服务端将判断微信号和卡号是否相符
        /// <summary>
        /// ：单据类型，51、订单；61、POS销售；63、POS退货；65、券卡销售；7、产品销售；
        /// </summary>
        /// <value>The listtype.</value>
        [Required(ErrorMessage = "数据类型必填")]
        public int Listtype { get; set; }
        /// <summary>
        ///订单状态：0、未处理；1、已开始处理；2、已提货；3、已退货；4、已过期
        /// </summary>
        /// <value>The status.</value>
        public int Status { get; set; }
        /// <summary>
        /// 单据开始时间
        /// 格式（2018-07-01）
        /// </summary>
        /// <value>The startdate.</value>
        public string Startdate { get; set; }
        /// <summary>
        /// ：单据结束时间
        /// 格式（2018-07-01）
        /// </summary>
        /// <value>The enddate.</value>
        public string Enddate { get; set; }
        /// <summary>
        /// 业务员
        /// </summary>
        /// <value>The sellername.</value>
        public string Sellername{ get; set; }
        /// <summary>
        /// 业务员电话号码
        /// </summary>
        /// <value>The sellerphone.</value>
        public string  Sellerphone{ get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Pageindex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Pagesize { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>The timestamp.</value>

        public string Timestamp { get; set; }
    }
}
