﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models.ICModels
{
    /// <summary>
    /// Invoice req.
    /// </summary>
    public class InvoiceReq
    {
        /// <summary>
        /// 发票类型，1、增值税普票；9、增值税专票
        /// </summary>
        /// <value>The invoicetype.</value>
        [Required]
        public int Invoicetype { get; set; }
        /// <summary>
        ///发票抬头类型，1、公司发票；2、个人/其它
        /// </summary>
        /// <value>The invoiceheadtype.</value>
        [Required]
        public int Invoiceheadtype { get; set; }
        /// <summary>
        /// 发票抬头（单位名称）
        /// </summary>
        /// <value>The invoicehead.</value>
        [Required(ErrorMessage = "发票抬头（单位名称,个人/其它）不能为空")]
		[StringLength(50,MinimumLength =2,ErrorMessage = "发票抬头长度1~50")]
        public string Invoicehead { get; set; }
		/// <summary>
		///纳税人识别号
		/// </summary>
		[StringLength(50, MinimumLength = 0, ErrorMessage = "税号长度1~50")]
		public string Taxnumber { get; set; }
        /// <summary>
        /// 开票金额
        /// </summary>
        /// <value>The invoicemoney.</value>
        public decimal Invoicemoney { get; set; }
        /// <summary>
        ///发票内容
        /// </summary>
        public string Invoicecontent { get; set; }
        /// <summary>
        ///对应单据
        /// </summary>
        /// <value>The list.</value>
        [Required(ErrorMessage = "对应单据 必填")]
        public List<TradeInfo> list { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>The timestamp.</value>
        public string Timestamp { get; set; }
	
    }

    /// <summary>
    /// 
    /// </summary>
    public class TradeInfo
    {
        /// <summary>
        /// 单据
        /// </summary>
        /// <value>The tradeno.</value>
        [Required(ErrorMessage = "单据号不能为空")]
        [StringLength(32, MinimumLength = 4, ErrorMessage = "单据号长度4~32位")]
        public string Tradeno { get; set; }
    }
}
