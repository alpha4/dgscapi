﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models.ICModels
{
    /// <summary>
    /// Liststatus req.
    /// </summary>
    public class ListstatusReq
    {
        //                1、  发送数据内容：
        //cardno：卡号，可不填写
        //phoneno：手机号，可不填，如果填写且也填写了卡号，服务端将判断手机号和卡号是否相符
        //wxopenid：微信id，可不填，如果填写且也填写了卡号，服务端将判断微信号和卡号是否相符
        //tradeno：单据交易号，必填
        //datasource：可选项，来源，11、微信；15、手机APP(默认)；16、PAD
        //public string cardno { get; set; }
        /// <summary>
        ///单据交易号，必填
        /// </summary>
        /// <value>The tradeno.</value>
        [Required(ErrorMessage = "产品类型必填")]
        public string Tradeno { get; set; }
    }
}
