﻿using System;
namespace Casamiel.API.Application.Models.ICModels
{
    /// <summary>
    /// Get product by shop identifier pid req.
    /// </summary>
    public class GetProductByShopIdPidReq
    {
        /// <summary>
        /// Gets or sets the shop identifier.
        /// </summary>
        /// <value>The shop identifier.</value>
        public int ShopId { get; set; }
        /// <summary>
        /// Gets or sets the pid.
        /// </summary>
        /// <value>The pid.</value>
        public int Pid { get; set; }
    }
}
