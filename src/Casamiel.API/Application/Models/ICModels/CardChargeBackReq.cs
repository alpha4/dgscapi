﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace Casamiel.API.Application.Models.ICModels
{
    /// <summary>
    /// Card charge back req.
    /// </summary>
    public class CardChargeBackReq
    {
        /// <summary>
        /// 卡号
        /// </summary>
        [Required]
        public string cardno { get; set; }
        /// <summary>
        /// 充值金额
        /// </summary>
        [Required]
        public double chargemoney { get; set; }
        /// <summary>
        /// Gets or sets the phoneno.
        /// </summary>
        /// <value>The phoneno.</value>
        [Required]
        public string phoneno { get; set; }
        /// <summary>
        /// 外部交易号
        /// </summary>
        [Required]
        public string tradeno { get; set; }
        /// <summary>
        /// The timestamp.
        /// </summary>
        public string timestamp { get; set; } = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss", CultureInfo.CurrentCulture);
    }
}
