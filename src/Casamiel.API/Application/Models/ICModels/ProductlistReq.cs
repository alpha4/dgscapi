﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models.ICModels
{
    /// <summary>
    /// Productlist req.
    /// </summary>
    public class ProductlistReq
    { 
        /// <summary>
        /// proudcttype：产品类型，C1、生日蛋糕；C2、端午产品；C4、中秋产品；C8、饮料；T1、生日蛋糕券；T2；端午产品券；T4、中秋产品券；T8、饮料券
        /// </summary>
        /// <value>The producttype.</value>
       
         [Required(ErrorMessage = "产品类型必填")]
        public string Producttype { get; set; }
        /// <summary>
        /// 业务员手机号
        /// </summary>
        /// <value>The sellerphone.</value>
        public string Sellerphone { get; set; }
    }
}
