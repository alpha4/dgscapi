﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Wx app bind mobile dto.
    /// </summary>
    public class WxAppBindMobileReq
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public int id { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        [Required]
        public string mobile { get; set; }
        /// <summary>
        /// 验证码
        /// </summary>
        [Required]
        public string yzm { get; set; }

        /// <summary>
        /// 设备号
        /// </summary>
        public string registration_Id { get; set; }
        /// <summary>
        /// Gets or sets the scene.
        /// </summary>
        /// <value>The scene.</value>
        public string Scene { get; set; }
        
    }
}
