﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 消费记录
    /// </summary>
    public class IcRecordResponse
    {
        /// <summary>
        /// Gets or sets the wxopenid.
        /// </summary>
        /// <value>The wxopenid.</value>
        public string wxopenid { get; set; }
        /// <summary>
        /// Gets or sets the cardno.
        /// </summary>
        /// <value>The cardno.</value>
        public string cardno { get; set; }
        /// <summary>
        /// Gets or sets the shopid.
        /// </summary>
        /// <value>The shopid.</value>
        public string  shopid{ get; set; }
        /// <summary>
        /// Gets or sets the shopname.
        /// </summary>
        /// <value>The shopname.</value>
        public string shopname { get; set; }
        /// <summary>
        /// Gets or sets the recdate.
        /// </summary>
        /// <value>The recdate.</value>
        public DateTime recdate{ get; set; }
        /// <summary>
        /// Gets or sets the consumetype.
        /// </summary>
        /// <value>The consumetype.</value>
        public string consumetype { get; set; }
        /// <summary>
        /// Gets or sets the tradeno.
        /// </summary>
        /// <value>The tradeno.</value>
        public string tradeno { get; set; }
        /// <summary>
        /// Gets or sets the totalmoney.
        /// </summary>
        /// <value>The totalmoney.</value>
        public decimal totalmoney{ get; set; }
        /// <summary>
        /// Gets or sets the discountmoney.
        /// </summary>
        /// <value>The discountmoney.</value>
        public decimal discountmoney{ get; set; }
        /// <summary>
        /// Gets or sets the realpaymoney.
        /// </summary>
        /// <value>The realpaymoney.</value>
        public decimal realpaymoney{ get; set; }
        /// <summary>
        /// Gets or sets the directmoney.
        /// </summary>
        /// <value>The directmoney.</value>
        public decimal directmoney{ get; set; }
        /// <summary>
        /// Gets or sets the othermoney.
        /// </summary>
        /// <value>The othermoney.</value>
        public decimal othermoney{ get; set; }
        /// <summary>
        /// Gets or sets the point.
        /// </summary>
        /// <value>The point.</value>
        public int  point{ get; set; }
    }
}
