﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models.Store
{
	/// <summary>
	///京东到家更改订单状态
	/// </summary>
    public class ChangeOrderStausReq
    {
		/// <summary>
		/// 
		/// </summary>
		[Required(ErrorMessage ="订单号不能为空")]
		public string orderId { get; set; }
		/// <summary>
		/// 
		/// </summary>
		[Required(ErrorMessage = "orderstatus不能为空")]
		public string orderstatus { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string remark { get; set; }

	}
}
