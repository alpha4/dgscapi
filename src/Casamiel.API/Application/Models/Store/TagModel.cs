﻿using System;
namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// Tag model.
    /// </summary>
    public class TagModel
    {
        /// <summary>
        /// Gets or sets the tag base identifier.
        /// </summary>
        /// <value>The tag base identifier.</value>
        public int TagBaseId { get; set; }
        /// <summary>
        /// Gets or sets the name of the tag.
        /// </summary>
        /// <value>The name of the tag.</value>
        public string TagName { get; set; }
        /// <summary>
        /// Gets or sets the SI mg URL.
        /// </summary>
        /// <value>The SI mg URL.</value>
        public string SImgUrl { get; set; }
    }
}
