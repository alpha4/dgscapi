﻿using System;
namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// App vesion.
    /// </summary>
    public class AppVesion
    {
        /// <summary>
        /// 自增ID
        /// </summary>
        public int VersionId { get; set; }

        /// <summary>
        /// APP类型 1 安卓后台
        /// </summary>
        public int AppType { get; set; }

        /// <summary>
        /// 版本描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// 下载地址
        /// </summary>
        public string AppUrl { get; set; }
        /// <summary>
        /// Gets or sets the name of the app type.
        /// </summary>
        /// <value>The name of the app type.</value>
        public string AppTypeName { get; set; }
    }
}
