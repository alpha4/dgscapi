﻿using System;
namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// Tmall order list req.
    /// </summary>
    public class TmallOrderListReq
    {
        /// <summary>
        /// Gets or sets the token.
        /// </summary>
        /// <value>The token.</value>
        public string token { get; set; }
        /// <summary>
        /// Gets or sets the store identifier.
        /// </summary>
        /// <value>The store identifier.</value>
        public int storeId { get; set; }
        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int pageSize { get; set; }
        /// <summary>
        /// Gets or sets the index of the page.
        /// </summary>
        /// <value>The index of the page.</value>
        public int pageIndex { get; set; }
        /// <summary>
        /// Gets or sets the order status.默认99
        /// </summary>
        /// <value>The order status.</value>
        public int orderStatus { get; set; }
    }
}
