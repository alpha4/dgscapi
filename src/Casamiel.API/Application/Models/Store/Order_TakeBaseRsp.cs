﻿using System;
namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// Order take base rsp.
    /// </summary>
    public class OrderTakeBaseRsp
    {
        /// <summary>
        /// Gets or sets the order base identifier.
        /// </summary>
        /// <value>The order base identifier.</value>
        public int OrderBaseId { get; set; }
        /// <summary>
        /// Gets or sets the take identifier.
        /// </summary>
        /// <value>The take identifier.</value>
        public int TakeId { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderCode { get; set; }
        /// <summary>
        /// Gets or sets the store identifier.
        /// </summary>
        /// <value>The store identifier.</value>
        public int StoreId { get; set; }

        /// <summary>
        /// 门店名称
        /// </summary>
        /// <value>The name of the store.</value>
        public string StoreName { get; set; }

        /// <summary>
        /// Gets or sets the store phone.
        /// </summary>
        /// <value>The store phone.</value>
        public string StorePhone { get; set; }

        /// <summary>
        /// 提货时间
        /// </summary>
        public DateTime TakeTime { get; set; }

        /// <summary>
        /// 提货码
        /// </summary>
        public string TakeCode { get; set; }


        /// <summary>
        /// 状态 -1未支付 0 未提货 1 已提货
        /// </summary>
        public int TaskStatus { get; set; }

        /// <summary>
        /// 订单金额
        /// </summary>
        public decimal OrderMoney { get; set; }

        #region 商品信息

        /// <summary>
        /// 标题
        /// </summary>
        public string GoodsTitle { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public string GoodsImage { get; set; }


        /// <summary>
        /// 单价
        /// </summary>
        public decimal GoodsPrice { get; set; }






        /// <summary>
        /// 数量
        /// </summary>
        public int GoodsQuantity { get; set; }






        /// <summary>
        /// 规格
        /// </summary>
        public string GoodsPropery { get; set; }






        /// <summary>
        /// 商品Id
        /// </summary>
        public int GoodsBaseId { get; set; }






        /// <summary>
        /// 商品门店关联Id
        /// </summary>
        public int GoodsId { get; set; }






        /// <summary>
        /// 产品ID
        /// </summary>
        public int ProductBaseId { get; set; }


        #endregion
    }
}
