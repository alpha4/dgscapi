﻿using System;
namespace Casamiel.API.Application.Models.Store
{
	/// <summary>
    /// 改变达达订单状态
    /// </summary>
    public class ChangeDadaStatusReq
    {
        /// <summary>
        /// 订单id
        /// </summary>
        public string order_id { get; set; }
        /// <summary>
        /// 订单状态
        /// </summary>
        public int order_status { get; set; }
        /// <summary>
        /// 取消原因
        /// </summary>
        public string cancel_reason { get; set; } = "";
        /// <summary>
        /// 取消来源
        /// </summary>
        public int cancel_from { get; set; } = 0;
        /// <summary>
        /// 配送人姓名
        /// </summary>
        public string dm_name { get; set; } = "";
#pragma warning disable CA1707 // Identifiers should not contain underscores
                              /// <summary>
                              /// 配送人电话
                              /// </summary>
        public string dm_mobile { get; set; } = "";
#pragma warning restore CA1707 // Identifiers should not contain underscores
    }
}
