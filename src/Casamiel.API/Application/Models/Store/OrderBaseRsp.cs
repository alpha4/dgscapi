﻿using System;
namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// Order base rsp.
    /// </summary>
    public class OrderBaseRsp
    {
        /// <summary>
        /// 配送方式 订单类型 1: 自提 2:本地配送 3：快递配送
        /// </summary>
        public int OrderType { get; set; }
        /// <summary>
        /// 订单id
        /// </summary>
        public int OrderBaseId { get; set; }

        /// <summary>
        /// 订单编码
        /// </summary>
        public string OrderCode { get; set; }

        /// <summary>
        /// 订单状态
        /// </summary>
        public int OrderStatus { get; set; }

        /// <summary>
        /// 删除状态
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 门店ID
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// Gets or sets the store phone.
        /// </summary>
        /// <value>The store phone.</value>
        public string StorePhone { get; set; }
        /// <summary>
        /// 门店名称
        /// </summary>
        /// <value>The name of the store.</value>
        public string StoreName { get; set; }
        /// <summary>
        /// Gets or sets the store relation identifier.
        /// </summary>
        /// <value>The store relation identifier.</value>
        public int StoreRelationId { get; set; }

        /// <summary>
        /// 提货人
        /// </summary>
        public string TakeName { get; set; }
        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>The address.</value>
        public string Address { get; set; }
        /// <summary>
        /// 提货手机
        /// </summary>
        public string TakeMobile { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 订单金额
        /// </summary>
        public decimal OrderMoney { get; set; }

        /// <summary>
        /// 支付时间
        /// </summary>
        /// <value>The pay time.</value>
        public DateTime PayTime { get; set; }

        /// <summary>
        /// 提货时间
        /// </summary>
        public DateTime TakeTime { get; set; }
        /// <summary>
        /// Gets or sets the ic trade no.
        /// </summary>
        /// <value>The ic trade no.</value>
        public string IcTradeNo { get; set; }

        #region 商品信息

        /// <summary>
        /// 标题
        /// </summary>
        public string GoodsTitle { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public string GoodsImage { get; set; }

        /// <summary>
        /// 单价
        /// </summary>
        public decimal GoodsPrice { get; set; }
        /// <summary>
        /// 会员价
        /// </summary>
        /// <value>The cost price.</value>
        public decimal CostPrice { get; set; }

		

        /// <summary>
        /// 数量
        /// </summary>
        public int GoodsQuantity { get; set; }
        /// <summary>
        /// 规格
        /// </summary>
        public string GoodsPropery { get; set; }

        /// <summary>
        /// 商品Id
        /// </summary>
        public int GoodsBaseId { get; set; }

        /// <summary>
        /// 商品门店关联Id
        /// </summary>
        public int GoodsId { get; set; }

        /// <summary>
        /// 产品ID
        /// </summary>
        public int ProductBaseId { get; set; }

        #endregion
        #region 优惠券信息
        /// <summary>
        /// 状态
        /// </summary>
        public int DiscountCouponStatus { get; set; }

        /// <summary>
        /// id
        /// </summary>
        public int DiscountCouponId { get; set; }


        /// <summary>
        /// Gets or sets the name of the discount coupon.
        /// </summary>
        /// <value>The name of the discount coupon.</value>
        public string DiscountCouponName { get; set; }


        /// <summary>
        /// 卡号
        /// </summary>
        public string CardNo { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        public decimal DiscountCouponActualMoney { get; set; }

        #endregion

        /// <summary>
        /// 提货码
        /// </summary>
        public string TakeCode { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
#pragma warning disable CA1707 // Identifiers should not contain underscores
                              /// <summary>
                              /// 第三方 配送状态 
                              /// 未配送=0 待接单＝1 待取货＝2 配送中＝3 已完成＝4 已取消＝5 已过期＝7 
                              /// 指派单=8 妥投异常之物品返回中=9 妥投异常之物品返回完成=10 创建达达运单失败=1000 可参考文末的状态说明
                              /// </summary>
        public string TPP_OrderStatus { get; set; } = "";
#pragma warning restore CA1707 // Identifiers should not contain underscores
#pragma warning disable IDE1006 // 命名样式
        /// <summary>
        /// 配送员姓名
        /// </summary>
        public string dm_name { get; set; } = "";
#pragma warning restore IDE1006 // 命名样式
                               /// <summary>
                               /// 配送员电话
                               /// </summary>
        public string dm_mobile { get; set; } = "";
        /// <summary>
        /// 支付方式 :0 默认   1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
        /// </summary>
        public int PayMethod { get; set; }
    }
}
