﻿using System;
using System.Collections.Generic;

namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// 
    /// </summary>
    public class Order_AddJDReq
    {
        /// <summary>
        /// 订单号
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 订单来源类型(0:原订单，20:补货单，30:直赔商品 ，50:上门换新)
        /// </summary>
        public int SrcInnerType { get; set; }

        /// <summary>
        /// 订单类型（10000：从门店出的订单）
        /// </summary>
        public int OrderType { get; set; }

        /// <summary>
        /// 订单状态（20010:锁定，20020:订单取消，20040:超时未支付系统取消，20060:系统撤销订单，31000:等待付款，31020:已付款，41000:待处理，32000:等待出库，33040:配送中，33060:已妥投, 34000:京东已收款，90000:订单完成）
        /// </summary>
        public string OrderStatus { get; set; }

        /// <summary>
        /// 订单状态最新更改时间
        /// </summary>
        public DateTime OrderStatusTime { get; set; }

        /// <summary>
        /// 下单时间
        /// </summary>
        public DateTime OrderStartTime { get; set; }

        /// <summary>
        /// 订单成交时间
        /// </summary>
        public DateTime OrderPurchaseTime { get; set; }

        /// <summary>
        /// 订单时效类型(0:无时效;2:自定义时间;1:次日达;27:七小时达;24:六小时达;21:五小时达;18:四小时达;15:三小时达;12:两小时达;9:一小时达;6:半小时达;)
        /// </summary>
        public int OrderAgingType { get; set; }

        /// <summary>
        /// 预计送达开始时间
        /// </summary>
        public DateTime OrderPreStartDeliveryTime { get; set; }

        /// <summary>
        /// 预计送达结束时间
        /// </summary>
        public DateTime OrderPreEndDeliveryTime { get; set; }

        /// <summary>
        /// 订单取消时间
        /// </summary>
        public DateTime OrderCancelTime { get; set; }

        /// <summary>
        /// 订单取消备注
        /// </summary>
        public string OrderCancelRemark { get; set; }

        /// <summary>
        /// 收货人名称
        /// </summary>
        public string BuyerFullName { get; set; }

        /// <summary>
        /// 收货人地址
        /// </summary>
        public string BuyerFullAddress { get; set; }

        /// <summary>
        /// 收货人电话
        /// </summary>
        public string BuyerTelephone { get; set; }

        /// <summary>
        /// 收货人手机号
        /// </summary>
        public string BuyerMobile { get; set; }

        /// <summary>
        /// 收货人真实手机号后四位
        /// </summary>
        public string LastFourDigitsOfBuyerMobile { get; set; }

        /// <summary>
        /// 配送门店编号
        /// </summary>
        public string DeliveryStationNo { get; set; }

        /// <summary>
        /// 商家配送门店编号
        /// </summary>
        public string DeliveryStationNoIsv { get; set; }

        /// <summary>
        /// 配送门店名称
        /// </summary>
        public string DeliveryStationName { get; set; }

        /// <summary>
        /// 承运商编号(9966:京东众包;2938:商家自送;1130:达达同城送)
        /// </summary>
        public string DeliveryCarrierNo { get; set; }

        /// <summary>
        /// 承运商名称
        /// </summary>
        public string DeliveryCarrierName { get; set; }

        /// <summary>
        /// 承运单号
        /// </summary>
        public string DeliveryBillNo { get; set; }

        /// <summary>
        /// 包裹重量（单位：kg）
        /// </summary>
        public Double DeliveryPackageWeight { get; set; }

        /// <summary>
        /// 妥投时间
        /// </summary>
        public DateTime DeliveryConfirmTime { get; set; }

        /// <summary>
        /// 订单支付类型(4:在线支付;)
        /// </summary>
        public int OrderPayType { get; set; }

        /// <summary>
        /// 订单商品销售价总金额   
        /// </summary>
        public long OrderTotalMoney { get; set; }

        /// <summary>
        /// 订单级别优惠金额
        /// </summary>
        public long OrderDiscountMoney { get; set; }

        /// <summary>
        /// 订单运费金额
        /// </summary>
        public long OrderFreightMoney { get; set; }

        /// <summary>
        /// 达达同城送运费(单位：分)
        /// </summary>
        public int LocalDeliveryMoney { get; set; }

        /// <summary>
        /// 订单应收运费，即未优惠前应付运费(满免，运费优惠券，VIP免基础运费，用户小费)
        /// </summary>
        public long OrderReceivableFreight { get; set; }

        /// <summary>
        /// 用户积分抵扣金额
        /// </summary>
        public long PlatformPointsDeductionMoney { get; set; }

        /// <summary>
        /// 用户应付金额（单位为分）=商品总金额 -订单优惠总金额 +订单运费总金额 +包装金额 -用户积分抵扣金额
        /// </summary>
        public long OrderBuyerPayableMoney { get; set; }

        /// <summary>
        /// 包装金额
        /// </summary>
        public long PackagingMoney { get; set; }

        /// <summary>
        /// 商家运费小费
        /// </summary>
        public long Tips { get; set; }

        /// <summary>
        /// 是否存在调整单(false:否;true:是)
        /// </summary>
        public bool AdjustIsExists { get; set; }

        /// <summary>
        /// 调整单编号
        /// </summary>
        public long AdjustId { get; set; }

        /// <summary>
        /// 收货人地址坐标类型(当buyerCoordType值为空或为1时，坐标类型为gps，如为其他值时，坐标类型为腾讯坐标)
        /// </summary>
        public int BuyerCoordType { get; set; }

        /// <summary>
        /// 收货人地址坐标经度
        /// </summary>
        public Double BuyerLng { get; set; }

        /// <summary>
        /// 收货人地址坐标纬度
        /// </summary>
        public Double BuyerLat { get; set; }

        /// <summary>
        /// 收货人市ID
        /// </summary>
        public string BuyerCity { get; set; }

        /// <summary>
        /// 收货人市名称
        /// </summary>
        public string BuyerCityName { get; set; }

        /// <summary>
        /// 收货人县(区)ID
        /// </summary>
        public string BuyerCountry { get; set; }

        /// <summary>
        /// 收货人县(区)名称
        /// </summary>
        public string BuyerCountryName { get; set; }

        /// <summary>
        /// 订单买家备注
        /// </summary>
        public string OrderBuyerRemark { get; set; }

        /// <summary>
        /// 业务标识，用英文分号分隔（订单打标写入此字段，如one_dingshida 定时达，dj_aging_nextday 隔夜达，dj_aging_immediately 立即达，picking_up 拣货完成）
        /// </summary>
        public string BusinessTag { get; set; }

        /// <summary>
        /// 设备id
        /// </summary>
        public string EquipmentId { get; set; }

        /// <summary>
        /// 收货人POI信息
        /// </summary>
        public string BuyerPoi { get; set; }

        /// <summary>
        /// 订购人姓名(此字段针对鲜花业务)
        /// </summary>
        public string OrdererName { get; set; }

        /// <summary>
        /// 订购人电话(此字段针对鲜花业务)
        /// </summary>
        public string OrdererMobile { get; set; }

        /// <summary>
        /// 订单门店序号
        /// </summary>
        public long OrderNum { get; set; }

        /// <summary>
        /// 用户小费（用户给配送员加小费）
        /// </summary>
        public long UserTip { get; set; }

        /// <summary>
        /// 收货人电话中间号有效期
        /// </summary>
        public DateTime MiddleNumBindingTime { get; set; }

        /// <summary>
        /// 订单抛入达达抢单池时间
        /// </summary>
        public DateTime DeliverInputTime { get; set; }

        /// <summary>
        /// 订单业务类型(1：京东到家商超，2：京东到家美食，4：京东到家开放仓，5：哥伦布店内订单，6：货柜订单，8：轻松购订单，9：是自助收银)
        /// </summary>
        public int BusinessType { get; set; }

        /// <summary>
        /// VIP卡号
        /// </summary>
        public string VenderVipCardId { get; set; }

        /// <summary>
        /// 包含需要查询订单的商品List列表
        /// </summary>
        public List<JDOrderDetail> Product { get; set; }

        /// <summary>
        /// 包含需要查询订单的优惠List列表
        /// </summary>
        public List<JDDiscount> Discount { get; set; }
        /// <summary>
        /// 发票具体信息
        /// </summary>
        public Invoice OrderInvoice { get; set; }
        /// <summary>
        /// Gets or sets the ic trade no.
        /// </summary>
        /// <value>The ic trade no.</value>
		public string IcTradeNo { get; set; }
        /// <summary>
        /// Gets or sets the bill no.
        /// </summary>
        /// <value>The bill no.</value>
        public string BillNo { get; set; }
    }
    /// <summary>
    /// 订单商品信息
    /// </summary>
    public class JDOrderDetail
    {
        /// <summary>
        /// Gets or sets the order identifier.
        /// </summary>
        /// <value>The order identifier.</value>
		public long OrderId { get; set; }
        /// <summary>
        /// Gets or sets the adjust identifier.
        /// </summary>
        /// <value>The adjust identifier.</value>
        public long AdjustId { get; set; }
        /// <summary>
        /// Gets or sets the name of the sku.
        /// </summary>
        /// <value>The name of the sku.</value>
        public string SkuName { get; set; }
        /// <summary>
        /// Gets or sets the sku identifier isv.
        /// </summary>
        /// <value>The sku identifier isv.</value>
        public string SkuIdIsv { get; set; }
        /// <summary>
        /// Gets or sets the sku jd price.
        /// </summary>
        /// <value>The sku jd price.</value>
		public long SkuJdPrice { get; set; }
        /// <summary>
        /// Gets or sets the sku count.
        /// </summary>
        /// <value>The sku count.</value>
		public int SkuCount { get; set; }
        /// <summary>
        /// Gets or sets the adjust mode.
        /// </summary>
        /// <value>The adjust mode.</value>
		public int AdjustMode { get; set; }
        /// <summary>
        /// Gets or sets the upc code.
        /// </summary>
        /// <value>The upc code.</value>
        public string UpcCode { get; set; }
        /// <summary>
        /// Gets or sets the artificer identifier.
        /// </summary>
        /// <value>The artificer identifier.</value>
        public int ArtificerId { get; set; }
        /// <summary>
        /// Gets or sets the name of the artificer.
        /// </summary>
        /// <value>The name of the artificer.</value>
		public string ArtificerName { get; set; }
        /// <summary>
        /// Gets or sets the sku store price.
        /// </summary>
        /// <value>The sku store price.</value>
		public long SkuStorePrice { get; set; }
        /// <summary>
        /// Gets or sets the sku cost price.
        /// </summary>
        /// <value>The sku cost price.</value>
		public long SkuCostPrice { get; set; }
        /// <summary>
        /// Gets or sets the type of the promotion.
        /// </summary>
        /// <value>The type of the promotion.</value>
		public int PromotionType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
		public string SkuTaxRate { get; set; }
        /// <summary>
        /// Gets or sets the promotion identifier.
        /// </summary>
        /// <value>The promotion identifier.</value>
		public long PromotionId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
		public double SkuWeight { get; set; }
        /// <summary>
        /// Gets or sets the canteen money.
        /// </summary>
        /// <value>The canteen money.</value>
        public long CanteenMoney { get; set; }
    }
    /// <summary>
    /// JDD iscount.
    /// </summary>
	public class JDDiscount
    {
        /// <summary>
        /// Gets or sets the order identifier.
        /// </summary>
        /// <value>The order identifier.</value>
		public long OrderId { get; set; }
        /// <summary>
        /// Gets or sets the adjust identifier.
        /// </summary>
        /// <value>The adjust identifier.</value>
        public long AdjustId { get; set; }
        /// <summary>
        /// Gets or sets the sku identifier.
        /// </summary>
        /// <value>The sku identifier.</value>
		public long SkuId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
		public string SkuIds { get; set; }
        /// <summary>
        /// Gets or sets the type of the discount.
        /// </summary>
        /// <value>The type of the discount.</value>
		public int DiscountType { get; set; }
        /// <summary>
        /// Gets or sets the type of the discount detail.
        /// </summary>
        /// <value>The type of the discount detail.</value>
		public int DiscountDetailType { get; set; }
        /// <summary>
        /// Gets or sets the discount code.
        /// </summary>
        /// <value>The discount code.</value>
		public string DiscountCode { get; set; }
        /// <summary>
        /// Gets or sets the type of the discount dediscount pricetail.
        /// </summary>
        /// <value>The type of the discount dediscount pricetail.</value>
		public long DiscountDediscountPricetailType { get; set; }
    }
    /// <summary>
    /// Invoice.
    /// </summary>
    public class Invoice
    {
        /// <summary>
        /// Gets or sets the type of the invoice form.
        /// </summary>
        /// <value>The type of the invoice form.</value>
        public int InvoiceFormType { get; set; }
        /// <summary>
        /// Gets or sets the invoice title.
        /// </summary>
        /// <value>The invoice title.</value>
        public string InvoiceTitle { get; set; }
        /// <summary>
        /// Gets or sets the invoice duty no.
        /// </summary>
        /// <value>The invoice duty no.</value>
        public string InvoiceDutyNo { get; set; }
        /// <summary>
        /// Gets or sets the invoice mail.
        /// </summary>
        /// <value>The invoice mail.</value>
        public string InvoiceMail { get; set; }
        /// <summary>
        /// Gets or sets the invoice money.
        /// </summary>
        /// <value>The invoice money.</value>
        public long InvoiceMoney { get; set; }
        /// <summary>
        /// Gets or sets the type of the invoice.
        /// </summary>
        /// <value>The type of the invoice.</value>
        public int InvoiceType { get; set; }
        /// <summary>
        /// Gets or sets the invoice money detail.
        /// </summary>
        /// <value>The invoice money detail.</value>
        public string InvoiceMoneyDetail { get; set; }
        /// <summary>
        /// Gets or sets the content of the invoice.
        /// </summary>
        /// <value>The content of the invoice.</value>
        public string InvoiceContent { get; set; }
    }
}
