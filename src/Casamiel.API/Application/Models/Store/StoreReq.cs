﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// Store req.
    /// </summary>
    public class StoreReq
    {
        /// <summary>
        /// Gets or sets the store identifier.
        /// </summary>
        /// <value>The store identifier.</value>

        public int StoreId { get; set; }
    }
}
