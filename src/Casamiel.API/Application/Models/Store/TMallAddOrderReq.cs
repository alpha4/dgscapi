﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models.Store
{
	/// <summary>
	/// 
	/// </summary>
	public class TMallAddOrderReq 
	{
		/// <summary>
		/// 天猫订单号
		/// </summary>
		[Required(ErrorMessage = "天猫订单号不能为空")]
		public string OrderCode { get; set; }
		/// <summary>
		///令牌
		/// </summary>
		[Required(ErrorMessage = "token不能为空")]
		public string Token { get; set; }
		/// <summary>
		/// 省Id
		/// </summary>
		public int ProvinceId { get; set; }
		/// <summary>
		/// 市Id
		/// </summary>
		public int CityId { get; set; }
		/// <summary>
		/// 区Id
		/// </summary>
		public int DistrictId { get; set; }
		/// <summary>
		/// 详细地址
		/// </summary>
		[Required(ErrorMessage = "FullAddress不能为空")]
		public string FullAddress { get; set; }
		/// <summary>
		/// 联系人
		/// </summary>
		public string ContactName { get; set; }
		/// <summary>
		/// 联系电话
		/// </summary>
		[Required(ErrorMessage = "ContactPhone不能为空")]
		public string ContactPhone { get; set; }
		/// <summary>
		/// 备注
		/// </summary>
		public string Remark { get; set; }
		/// <summary>
		/// 订单支付金额
		/// </summary>
		public decimal OrderMoney { get; set; }
		/// <summary>
		/// 提货时间
		/// </summary>
		public DateTime TakeTime { get; set; }

        /// <summary>
        /// 订单优惠金额
        /// </summary>
        public decimal DiscountMoney { get; set; }

        /// <summary>
        /// 运费差价
        /// </summary>
        public decimal FreightMoney { get; set; }

       /// <summary>
       /// 经度
       /// </summary>
       /// <value>The longitude.</value>
        public double Longitude { get; set; }
        /// <summary>
        /// 纬度
        /// </summary>
        public double Latitude { get; set; }
        /// <summary>
        /// 城市名称
        /// </summary>
        public string CityName { get; set; }


        /// <summary>
        /// 商品信息
        /// </summary>
        public List<TOrderGoods> GoodsList { get; set; }
		/// <summary>
		/// 门店ID
		/// </summary>
		/// 
		public int StoreId { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string IcTradeNO { get; set; }
		/// <summary>
		/// 业务单据
		/// </summary>
		public string BillNo { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public class TOrderGoods
		{
			/// <summary>
			/// 商品id
			/// </summary>
			public int GoodsBaseId { get; set; }
			/// <summary>
			/// 购买数量
			/// </summary>
			public int GoodsQuantity { get; set; }
			/// <summary>
			/// 单价
			/// </summary>
			public decimal Price { get; set; }
            /// <summary>
            /// Gets or sets the title.
            /// </summary>
            /// <value>The title.</value>
            public string Title { get; set; }
            /// <summary>
            /// 关联ID
            /// </summary>
            /// <value>The relation identifier.</value>
            public int RelationId { get; set; }
		}
	}
}
