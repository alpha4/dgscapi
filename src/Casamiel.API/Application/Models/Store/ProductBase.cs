﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// Product base.
    /// </summary>

    public class ProductBase
	{
        /// <summary>
        /// 门店id
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 门店关联id
        /// </summary>
        public int StoreRelationId { get; set; }
		/// <summary>
		/// 产品Id
		/// </summary>
		public int ProductBaseId { get; set; }
		/// <summary>
		/// 标题
		/// </summary>
		public string Title { get; set; }
		/// <summary>
		/// 子标题
		/// </summary>
		public string SubTitle { get; set; } 
		/// <summary>
		/// 描述
		/// </summary>
		public string Content { get; set; }
		/// <summary>
		/// 售卖价格
		/// </summary>
		public string Price { get; set; }
		/// <summary>
		/// 原价
		/// </summary>
		public string CostPrice { get; set; }
		/// <summary>
		/// 地区
		/// </summary>
		public string Area { get; set; }
		/// <summary>
		/// 销售属性
		/// </summary>
#pragma warning disable CA2227 // 集合属性应为只读
		public List<Property> SalePropertyList { get; set; }
#pragma warning restore CA2227 // 集合属性应为只读

		/// <summary>
		/// 商品图片列表
		/// </summary>
		public List<string> Images { get; set; }
        /// <summary>
        /// 包含的商品SKU
        /// </summary>
        public List<GoodsModel> GoodsList { get; set; }
    }
    /// <summary>
    /// Goods model.
    /// </summary>
    public class GoodsModel
		{
			/// <summary>
            /// Gets or sets the goods base identifier.
            /// </summary>
            /// <value>The goods base identifier.</value>
            public int GoodsBaseId { get; set; }
			/// <summary>
			/// 关联Id
			/// </summary>
			public int RelationId { get; set; }
			/// <summary> 
			/// 价格  
			/// </summary> 
			public string Price { get; set; }
            /// <summary>
            /// 会员价
            /// </summary>
            /// <value>The cost price.</value>
            public string costPrice { get; set; }

            /// <summary> 
            /// 状态 0 上架 1 下架
            /// </summary> 
            public int Status { get; set; }

			/// <summary> 
			/// 库存数量  
			/// </summary> 
			public int StockQuentity { get; set; }
            /// <summary>
            /// Gets or sets the image path.
            /// </summary>
            /// <value>The image path.</value>
			public string ImgPath { get; set; }
		/// <summary>
		/// 商品销售属性列表
		/// </summary>
#pragma warning disable CA2227 // 集合属性应为只读
		public List<PropertyItem> GoodsTypeItemList { get; set; }
#pragma warning restore CA2227 // 集合属性应为只读
	}

		/// <summary>
		/// 属性
		/// </summary>
		public class Property
		{
			/// <summary>
			/// 属性ID
			/// </summary>
			public int PropertyId { get; set; }
			/// <summary>
			/// 属性名称
			/// </summary>
			public string PropertyName { get; set; }
		/// <summary>
		/// Gets or sets the item list.
		/// </summary>
		/// <value>The item list.</value>
#pragma warning disable CA2227 // 集合属性应为只读
		public List<PropertyItem> ItemList { get; set; }
#pragma warning restore CA2227 // 集合属性应为只读
	}
		/// <summary>
		/// 属性值
		/// </summary>
		public class PropertyItem
		{
            /// <summary>
            /// Gets or sets the property item identifier.
            /// </summary>
            /// <value>The property item identifier.</value>
			public int PropertyItemId { get; set; }
			/// <summary>
			/// 属性ID
			/// </summary>
			public int PropertyId { get; set; }
			/// <summary>
			/// 属性值
			/// </summary>
			public string PropertyValue { get; set; }
		}
	
}
