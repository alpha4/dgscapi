﻿using System;
namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// Goods collect rsp.
    /// </summary>
    public class GoodsCollectRsp
    {
        /// <summary>
        /// Gets or sets the collect identifier.
        /// </summary>
        /// <value>The collect identifier.</value>
        public int CollectId { get; set; }
        /// <summary>
        /// Gets or sets the product base identifier.
        /// </summary>
        /// <value>The product base identifier.</value>
        public int ProductBaseId { get; set; }
        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        public string Title { get; set; }
#pragma warning disable IDE1006 // 命名样式
        /// <summary>
        /// Gets or sets the image URL.
        /// </summary>
        /// <value>The image URL.</value>
        public string ImageUrl { get; set; }
#pragma warning restore IDE1006 // 命名样式
        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>The price.</value>
        public decimal Price { get; set; }
        /// <summary>
        /// Gets or sets the cost price.
        /// </summary>
        /// <value>The cost price.</value>
        public decimal CostPrice { get; set; }
    }
}
