﻿using System;
namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// Slide get list rsp.
    /// </summary>
    public class SlideGetListRsp
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 副标题
        /// </summary>
        public string SubTitle { get; set; }

        /// <summary>
        /// 链接地址 或者Id或产品id  视情况定义
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        public string ImageUrl { get; set; }
    }
}
