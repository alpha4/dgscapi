﻿using System;
namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// Get all stores req.
    /// </summary>
    public class GetAllStoresReq
    {
        /// <summary>
        /// Gets or sets the name of the store.
        /// </summary>
        /// <value>The name of the store.</value>
        public string StoreName { get; set; }
        /// <summary>
        /// Gets or sets the index of the page.
        /// </summary>
        /// <value>The index of the page.</value>
        public int PageIndex { get; set; }
        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize{get;set;}
        /// <summary>
        /// Gets or sets the lat.
        /// </summary>
        /// <value>The lat.</value>
        public double Lat { get; set; }
        /// <summary>
        /// Gets or sets the lng.
        /// </summary>
        /// <value>The lng.</value>
        public double Lng { get; set; }
    }

    /// <summary>
    /// Get all stores req.
    /// </summary>
    public class GetAllStoresByStoreNameReq
    {
        /// <summary>
        /// Gets or sets the name of the store.
        /// </summary>
        /// <value>The name of the store.</value>
        public string StoreName { get; set; }
        /// <summary>
        /// Gets or sets the index of the page.
        /// </summary>
        /// <value>The index of the page.</value>
        public string PageIndex { get; set; }
        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public string PageSize { get; set; }
        /// <summary>
        /// Gets or sets the lat.
        /// </summary>
        /// <value>The lat.</value>
        public double Lat { get; set; }
        /// <summary>
        /// Gets or sets the lng.
        /// </summary>
        /// <value>The lng.</value>
        public double Lng { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetAllStoreReq
    {
        /// <summary>
        /// 
        /// </summary>
        public double Lat { get; set; }
        /// <summary>
        /// Gets or sets the lng.
        /// </summary>
        /// <value>The lng.</value>
        public double Lng { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double Distance { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class TmallGetAllStoreReq : GetAllStoreReq
    {
        /// <summary>
        /// 
        /// </summary>
        public string Token { get; set; }
    }
}
