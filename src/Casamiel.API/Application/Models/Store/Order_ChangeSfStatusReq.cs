﻿using System;
namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// Order change sf status req.
    /// </summary>
    public class Order_ChangeSfStatusReq
    {
#pragma warning disable CA1707 // Identifiers should not contain underscores
                              /// <summary>
                              /// 顺丰订单ID
                              /// </summary>
        public long sf_order_id { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
                              /// <summary>
                              /// 商家订单ID
                              /// </summary>
        public string shop_order_id { get; set; }
#pragma warning disable CA1056 // Uri properties should not be strings
                              /// <summary>
                              /// 回调url前缀
                              /// </summary>
        public string url_index { get; set; }
#pragma warning restore CA1056 // Uri properties should not be strings
                              /// <summary>
                              /// 配送员姓名 操作人
                              /// </summary>
        public string operator_name { get; set; }
#pragma warning disable CA1707 // Identifiers sshould not contain underscores
                              /// <summary>
                              /// 配送员电话
                              /// </summary>
        public string operator_phone { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
                              /// <summary>
                              /// 配送员位置经度
                              /// </summary>
        public string rider_lng { get; set; }
        /// <summary>
        /// 配送员位置纬度
        /// </summary>
        public string rider_lat { get; set; }
        /// <summary>
        /// 订单状态 10-配送员确认;12:配送员到店;15:配送员配送中,17配送完成
        /// </summary>
        public int order_status { get; set; }
#pragma warning disable CA1707 // Identifiers should not contain underscores
                              /// <summary>
                              /// 状态描述 17配送员点击完成
                              /// </summary>
        public string status_desc { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
                              /// <summary>
                              /// 推送时间戳
                              /// </summary>
        public int push_time { get; set; }
    }
}
