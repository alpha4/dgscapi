﻿using System;
namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// Order add mei tuan req.
    /// </summary>
    public class AddMeiTuanOrderReq
    {
        /// <summary>
        /// 订单创建时间
        /// </summary>
        public long cTime { get; set; }

        /// <summary>
        /// 订单备注
        /// </summary>
        public string Caution { get; set; }

        /// <summary>
        /// 用户预计送达时间，“立即送达”时为0
        /// </summary>
        public long deliveryTime { get; set; }

        /// <summary>
        /// 订单菜品详情 
        /// </summary>
        public string detail { get; set; }

        /// <summary>
        /// 城市Id 110100
        /// </summary>
        public long cityId { get; set; }

        /// <summary>
        /// ERP方门店id
        /// </summary>
        public string ePoiId { get; set; }

        /// <summary>
        /// 订单扩展信息 
        /// </summary>
        public string extras { get; set; }

        /// <summary>
        /// 是否需要发票 1-需要发票；0-不需要
        /// </summary>
        public int hasInvoiced { get; set; }

        /// <summary>
        /// 发票抬头
        /// </summary>
        public string invoiceTitle { get; set; }

        /// <summary>
        /// 发票url
        /// </summary>
        public string InvoiceUrl { get; set; }

        /// <summary>
        /// 发票税号
        /// </summary>
        public string taxpayerId { get; set; }

        /// <summary>
        /// 是否第三方配送 0：否；1：是
        /// </summary>
        public int isThirdShipping { get; set; }

        /// <summary>
        /// 实际送餐地址纬度
        /// </summary>
        public Double Latitude { get; set; }

        /// <summary>
        /// 实际送餐地址经度
        /// </summary>
        public Double Longitude { get; set; }

        /// <summary>
        /// 商家对账信息
        /// </summary>
        public string PoiReceiveDetail { get; set; }

        /// <summary>
        /// 配送类型码
        /// </summary>
        public string LogisticsCode { get; set; }

        /// <summary>
        /// 配送完成时间
        /// </summary>
        public long LogisticsCompletedTime { get; set; }

        /// <summary>
        /// 配送单确认时间，骑手接单时间
        /// </summary>
        public long logisticsConfirmTime { get; set; }

        

        /// <summary>
        /// 骑手姓名
        /// </summary>
        public string logisticsDispatcherName { get; set; }

        /// <summary>
        /// 骑手取单时间
        /// </summary>
        public long logisticsFetchTime { get; set; }

        /// <summary>
        /// 配送方ID
        /// </summary>
        public int logisticsId { get; set; }

        /// <summary>
        /// 配送方名称
        /// </summary>
        public string logisticsName { get; set; }

        /// <summary>
        /// 配送单下单时间
        /// </summary>
        public long logisticsSendTime { get; set; }

        /// <summary>
        /// 配送订单状态
        /// </summary>
        public int logisticsStatus { get; set; }

        /// <summary>
        /// 订单完成时间
        /// </summary>
        public long orderCompletedTime { get; set; }

        /// <summary>
        /// 商户确认时间
        /// </summary>
        public long orderConfirmTime { get; set; }

        /// <summary>
        /// 订单取消时间
        /// </summary>
        public long orderCancelTime { get; set; }

        /// <summary>
        /// 订单Id
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 订单展示Id
        /// </summary>
        public long OrderIdView { get; set; }

        /// <summary>
        /// 用户下单时间
        /// </summary>
        public long OrderSendTime { get; set; }

        /// <summary>
        /// 订单原价
        /// </summary>
        public Decimal OriginalPrice { get; set; }

        /// <summary>
        /// 订单支付类型 1：货到付款；2：在线支付
        /// </summary>
        public int PayType { get; set; }

        /// <summary>
        /// 门店地址
        /// </summary>
        public string PoiAddress { get; set; }

        /// <summary>
        /// 门店Id
        /// </summary>
        public long PoiId { get; set; }

        /// <summary>
        /// 门店名称
        /// </summary>
        public string poiName { get; set; }

        /// <summary>
        /// 门店服务电话
        /// </summary>
        public string PoiPhone { get; set; }

        /// <summary>
        /// 收货人地址
        /// </summary>
        public string RecipientAddress { get; set; }

        /// <summary>
        /// 收货人名称
        /// </summary>
        public string RecipientName { get; set; }

        /// <summary>
        /// 收货人电话
        /// </summary>
        public string RecipientPhone { get; set; }

        /// <summary>
        /// 骑手电话
        /// </summary>
        public string ShipperPhone { get; set; }

        /// <summary>
        /// 配送费用
        /// </summary>
        public Decimal ShippingFee { get; set; }

        /// <summary>
        /// 订单状态
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 总价
        /// </summary>
        public Decimal Total { get; set; }

        /// <summary>
        /// 订单更新时间
        /// </summary>
        public long UTime { get; set; }

        /// <summary>
        /// 门店当天的订单流水号
        /// </summary>
        public int DaySeq { get; set; }

        /// <summary>
        /// 就餐人数 0：用户没有选择用餐人数；1-10：用户选择的用餐人数；-10：10人以上用餐；99：用户不需要餐具
        /// </summary>
        public int DinnersNumber { get; set; }

        /// <summary>
        /// 取餐类型 0：普通取餐；1：到店取餐
        /// </summary>
        public int PickType { get; set; }
        /// <summary>
        /// Gets or sets the ic trade no.
        /// </summary>
        /// <value>The ic trade no.</value>
        public string IcTradeNo { get; set; }
        /// <summary>
        /// Gets or sets the bill no.
        /// </summary>
        /// <value>The bill no.</value>
        public string BillNo { get; set; }

        /// <summary>
        /// Extras detail.
        /// </summary>
        public class ExtrasDetail
        {
            /// <summary>
            /// 该活动中美团承担的费用
            /// </summary>
            public decimal mt_charge { get; set; }
            /// <summary>
            /// 该活动中商家承担的费用
            /// </summary>
            public decimal poi_charge { get; set; }
            /// <summary>
            /// 活动优惠金额，是在原价基础上减免的金额
            /// </summary>
            public decimal reduce_fee { get; set; }
            /// <summary>
            /// 优惠说明
            /// </summary>
            public string remark { get; set; }
            /// <summary>
            /// 优惠活动类型（1-新用户立减；2-满减；4-套餐赠送；5-满赠；9-使用红包；
            /// 11-提前下单减；16-满免配送费(即将废弃)；17-折扣商品；18-美团专送再减(即将废弃)；
            /// 19-点评券；20-第二份半价；21-会员免配送费；22-门店新客立减；23-买赠；
            /// 24-平台新用户立减；25-满减配送费；100-满返商家代金券；101-使用商家代金券；103-进店领券）
            /// </summary>
            public int type { get; set; }
        }
        /// <summary>
        /// Goods detail.
        /// </summary>
        public class GoodsDetail
        {
#pragma warning disable CA1707 // Identifiers should not contain underscores
#pragma warning disable IDE1006 // 命名样式
                               /// <summary>
                               /// ERP方菜品id
                               /// </summary>
            public string App_food_code { get; set; }
#pragma warning restore IDE1006 // 命名样式
#pragma warning restore CA1707 // Identifiers should not contain underscores
#pragma warning disable CA1707 // Identifiers should not contain underscores
                              /// <summary>
                              /// 餐盒数量
                              /// </summary>
            public decimal Box_num { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
                              /// <summary>
                              /// 餐盒单价
                              /// </summary>
            public decimal box_price { get; set; }
#pragma warning disable CA1707 // Identifiers should not contain underscores
                              /// <summary>
                              /// 菜品名
                              /// </summary>
            public string food_name { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
                              /// <summary>
                              /// ERP方菜品sku
                              /// </summary>
            public string sku_id { get; set; }
            /// <summary>
            /// 菜品份数
            /// </summary>
            public int quantity { get; set; }
#pragma warning disable IDE1006 // 命名样式
                               /// <summary>
                               /// 菜品原价
                               /// </summary>
            public decimal price { get; set; }
#pragma warning restore IDE1006 // 命名样式
                               /// <summary>
                               /// 单位
                               /// </summary>
            public string unit { get; set; }
#pragma warning disable IDE1006 // 命名样式
                               /// <summary>
                               /// 菜品折扣
                               /// </summary>
            public decimal food_discount { get; set; }
#pragma warning restore IDE1006 // 命名样式
#pragma warning disable CA1707 // Identifiers should not contain underscores
            /// <summary>
            /// 菜品属性 "中辣,微甜"
            /// </summary>
            public string food_property { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
                              /// <summary>
                              /// 菜品分成
                              /// </summary>
            public decimal foodShareFeeChargeByPoi { get; set; }
            /// <summary>
            /// 商品所在的口袋
            /// </summary>
            public int cart_id { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        public class GoodsDetailExt
        {
#pragma warning disable CA1707 // Identifiers should not contain underscores
#pragma warning disable IDE1006 // 命名样式
            /// <summary>
            /// ERP方菜品id
            /// </summary>
            public string app_food_code { get; set; }
#pragma warning restore IDE1006 // 命名样式
#pragma warning restore CA1707 // Identifiers should not contain underscores
#pragma warning disable CA1707 // Identifiers should not contain underscores
            /// <summary>
            /// 餐盒数量
            /// </summary>
            public int box_num { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
            /// <summary>
            /// 餐盒单价
            /// </summary>
            public decimal box_price { get; set; }
#pragma warning disable CA1707 // Identifiers should not contain underscores
            /// <summary>
            /// 菜品名
            /// </summary>
            public string food_name { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
            /// <summary>
            /// ERP方菜品sku
            /// </summary>
            public string sku_id { get; set; }
            /// <summary>
            /// 菜品份数
            /// </summary>
            public int quantity { get; set; }
#pragma warning disable IDE1006 // 命名样式
            /// <summary>
            /// 菜品原价
            /// </summary>
            public decimal price { get; set; }
#pragma warning restore IDE1006 // 命名样式
            /// <summary>
            /// 单位
            /// </summary>
            public string unit { get; set; }
#pragma warning disable IDE1006 // 命名样式
            /// <summary>
            /// 菜品折扣
            /// </summary>
            public decimal food_discount { get; set; }
#pragma warning restore IDE1006 // 命名样式
#pragma warning disable CA1707 // Identifiers should not contain underscores
            /// <summary>
            /// 菜品属性 "中辣,微甜"
            /// </summary>
            public string food_property { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
            /// <summary>
            /// 菜品分成
            /// </summary>
            public decimal foodShareFeeChargeByPoi { get; set; }
            /// <summary>
            /// 商品所在的口袋
            /// </summary>
            public int cart_id { get; set; }
        }
    }
}
