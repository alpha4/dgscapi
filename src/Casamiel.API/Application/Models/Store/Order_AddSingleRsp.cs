﻿using System;
namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// Order add single rsp.
    /// </summary>
    public class OrderAddSingleRsp
    {
        /// <summary>
        /// Gets or sets the order base identifier.
        /// </summary>
        /// <value>The order base identifier.</value>
        public int OrderBaseId { get; set; }
        /// <summary>
        /// Gets or sets the order code.
        /// </summary>
        /// <value>The order code.</value>
        public string OrderCode { get; set; }
        /// <summary>
        /// Gets or sets the order money.
        /// </summary>
        /// <value>The order money.</value>
        public decimal OrderMoney { get; set; }
    }
}
