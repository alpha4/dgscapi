﻿using Casamiel.Domain.Request;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// Order payment req.
    /// </summary>
    public class OrderPaymentReq: OrderPaymentRequest
    {
        /// <summary>
        /// 优惠券id
        /// </summary>
        public int DiscountCouponId { get; set; }
        /// <summary>
        /// 优惠券金额
        /// </summary>
        public decimal DiscountCouponMoney { get; set; }

        /// <summary>
        /// 优惠券卡号
        /// </summary>
        /// <value>The card no.</value>
        public string CardNO { get; set; }

        
         
    }
    /// <summary>
    /// Order payment v2 req.
    /// </summary>
    public class OrderPaymentV2Req: OrderPaymentRequest
    {

		/// <summary>
		/// 优惠券信息
		/// </summary>
#pragma warning disable CA2227 // 集合属性应为只读
		public List<OrderDiscountReq> DiscountList { get; set; }
#pragma warning restore CA2227 // 集合属性应为只读

	}

    /// <summary>
    /// 
    /// </summary>
    public class OrderPaymentRequest
    {
        /// <summary>
        /// Gets or sets the order code.
        /// </summary>
        /// <value>The order code.</value>
        public string OrderCode { get; set; }

         
        /// <summary>
        /// Gets or sets the pay method.
        /// 支付方式 1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
        /// </summary>
        /// <value>The pay method.</value>
        [Required(ErrorMessage = "支付方式必填")]
        public int PayMethod { get; set; }

        /// <summary>
        /// Gets or sets the pay card no.
        /// </summary>
        /// <value>The pay card no.</value>
        public string PayCardNO { get; set; }

        /// <summary>
        /// 小程序专用Id
        /// </summary>
        /// <value>The identifier.</value>
        public int id { get; set; }
    }

}
