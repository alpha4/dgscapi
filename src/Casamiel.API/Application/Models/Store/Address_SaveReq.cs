﻿//
//  Copyright 2018  
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using System;
namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// Address save req.
    /// </summary>
    public class AddressSaveReq
    {
        /// <summary>
        /// Gets or sets the consignee identifier.
        /// </summary>
        /// <value>The consignee identifier.</value> 
        public int ConsigneeId { get; set; }
        /// <summary>
        /// 是否默认
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// 排序编号，倒序
        /// </summary>
        public int OrderNo { get; set; }

        /// <summary>
        /// 联系人姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 手机 会员信息
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 电话 
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string FullAddress { get; set; }

        /// <summary>
        /// 省ID
        /// </summary>
        public int ProvinceId { get; set; }       

        /// <summary>
        /// 市ID
        /// </summary>
        public int CityId { get; set; }       

        /// <summary>
        /// 区ID
        /// </summary>
        public int DistrictId { get; set; }       

        /// <summary>
        /// 邮编
        /// </summary>
        public string Postcode { get; set; }


        /// <summary>
        /// 身份证
        /// </summary>
        public string IdentityCard { get; set; } = "";

        /// <summary>
        /// 纬度
        /// </summary>
        public double Latitude { get; set; }
        /// <summary>
        /// 经度
        /// </summary>
        public double Longitude { get; set; }
        /// <summary>
        /// 门店ID
        /// </summary>
        public int StoreId { get; set; } = 0;
    }
}
