﻿using System;
namespace Casamiel.API.Application.Models.Store
{
    /// <summary>
    /// Order add single req.
    /// </summary>
    public class Order_AddSingleReq
    {
        /// <summary>
        /// 商品id
        /// </summary>
        public int GoodsId { get; set; }
        /// <summary>
        /// 门店id
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// Gets or sets the cost price.
        /// </summary>
        /// <value>The cost price.</value>
        public decimal CostPrice { get; set; }
        /// <summary>
        /// 购买数量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 手机号 用户
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 提货人
        /// </summary>
        public string TakeName { get; set; }
        /// <summary>
        /// 提货人联系方式
        /// </summary>
        public string TakeMobile { get; set; }
        /// <summary>
        /// 优惠券id
        /// </summary>
        public int DiscountCouponId { get; set; }
        /// <summary>
        /// 优惠券金额
        /// </summary>
        public decimal DiscountCouponMoney { get; set; }
        /// <summary>
        /// Gets or sets the name of the discount coupon.
        /// </summary>
        /// <value>The name of the discount coupon.</value>
        public string DiscountCouponName { get; set; }
        /// <summary>
        /// 优惠券卡号
        /// </summary>
        /// <value>The card no.</value>
        public string CardNO { get; set; }
        /// <summary>
        /// 备注信息
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// IC交易号
        /// </summary>
        public string IcTradeNO { get; set; }

		/// <summary>
		/// 一网烘培单号
		/// </summary>
		public string BillNo { get; set; }
        /// <summary>
        /// 商品规格
        /// </summary>
        public string GoodsProperty { get; set; }
        /// <summary>
        /// 提货时间
        /// </summary>
        public DateTime TakeTime { get; set; }

        /// <summary>
        /// 配送方式 订单类型 1: 自提 2:本地配送 3：快递配送
        /// </summary>
        public int OrderType { get; set; }
        /// <summary>
        /// 地址Id
        /// </summary>
        public int ConsigneeId { get; set; }
        /// <summary>
        /// Gets or sets the pay method.
        /// </summary>
        /// <value>The pay method.</value>
        public int Paymethod { get; set; }
    }
}