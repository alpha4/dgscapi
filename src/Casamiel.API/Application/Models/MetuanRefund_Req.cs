﻿using System;
namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Metuan refund req.
    /// </summary>
    public class MetuanRefundReq
    {
        /// <summary>
        ///apply	发起退款
        ///agree 确认退款
        ///reject 驳回退款
        ///cancelRefund 用户取消退款申请
        ///cancelRefundComplaint 取消退款申诉
        /// </summary>
        /// <value>The type of the notify.</value>
        public string NotifyType { get; set; }

        /// <summary>
        /// Gets or sets the order identifier.
        /// </summary>
        /// <value>The order identifier.</value>
        public long OrderId { get; set; }

        /// <summary>
        /// Gets or sets the reason.
        /// </summary>
        /// <value>The reason.</value>
        public string Reason { get; set; }
    }
}
