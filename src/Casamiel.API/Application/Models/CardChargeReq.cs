﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 会员卡充值
    /// </summary>
    public class CardChargeReq
    {
        /// <summary>
        /// 卡号
        /// </summary>
        [Required]
        public string cardno { get; set; }
        /// <summary>
        /// 充值金额
        /// </summary>
        [Required]
        public double  chargemoney { get; set; }
        /// <summary>
        /// 支付方式（1、微信；2、支付宝；3、会员卡；4、银行卡；5、券
        /// </summary>
        [Required]
        public int paytype { get; set; }
        /// <summary>
        /// 外部交易号
        /// </summary>
        [Required]
        public string paytradeno { get; set; }
    }
}
