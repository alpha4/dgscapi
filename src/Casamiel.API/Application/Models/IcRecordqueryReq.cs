﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class IcRecordqueryReq
    {
        /// <summary>
        /// 消费记录查询
        /// </summary>
        public string Phoneno { get; set; }

        /// <summary>
        /// 卡号
        /// </summary>
        public string Cardno { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        [Required]
        public DateTime Startdate { get; set; }

        /// <summary>
        /// 结束时间 
        /// </summary>
        [Required]
        public DateTime Enddate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Pageindex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Pagesize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Timestamp { get; set; }
    }
}
