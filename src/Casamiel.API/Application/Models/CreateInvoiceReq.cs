﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Create invoice req.
    /// </summary>
    public class CreateInvoiceReq
    {
        /// <summary>
        /// Gets or sets the identifiers.
        /// </summary>
        /// <value>The identifiers.</value>
        [Required(ErrorMessage = "IDs必填空")]
        public string IDS { get; set; }
        /// <summary>
        ///购方名称 
        /// </summary>
        /// <value>The name of the buyer.</value>
        [Required(ErrorMessage = "购方名称必填空")]
        [StringLength(100, MinimumLength = 0, ErrorMessage = "购方名称长度超出")]
        public string BuyerName { get; set; }
        /// <summary>
        /// 开票类型：0个人，1企业
        /// </summary>
        /// <value>The type of the invoice.</value>
        public int InvoiceType { get; set; }
        /// <summary>
        /// 银行账号
        /// </summary>
        /// <value>The account.</value>

        [StringLength(100, MinimumLength = 0, ErrorMessage = "银行账号长度超出")]
        public string Account { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        [Required(ErrorMessage = "电子邮箱必填")]
        public string Email { get; set; }
        /// <summary>
        ///接收短信手机号选填
        /// </summary>
        /// <value>The sms mobile.</value>
        public string SmsMobile { get; set; }
        /// <summary>
        /// 购方地址选填
        /// </summary>
        /// <value>The address.</value>
        [StringLength(100, MinimumLength = 0, ErrorMessage = "购方地址长度超出")]
        public string Address { get; set; }
        /// <summary>
        /// 购方税号 否（企业要填，个人 可为空）） 20
        /// </summary>
        /// <value>The tax number.</value>
        [StringLength(20, MinimumLength = 0, ErrorMessage = "税号长度超出")]
        public string TaxNum { get; set; }
    }
}
