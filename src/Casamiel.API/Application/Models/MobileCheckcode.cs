﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
	/// mobile_checkcode: mobile_checkcode
	/// </summary>
    public  class MobileCheckcode
    {
        private long _id;
       
        private string _mobile;
        private string _checkcode;
        private DateTime _checkcode_outdate;
        private int _send_num;
        private DateTime _create_time;
        /// <summary>
        /// id
        /// </summary>
        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }
         
        /// <summary>
        /// 手机号
        /// </summary>
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        /// <summary>
        /// 激活或找回密码时,存储的校验码
        /// </summary>
        public string Checkcode
        {
            get { return _checkcode; }
            set { _checkcode = value; }
        }

        /// <summary>
        /// 激活码的过期时间
        /// </summary>
        public DateTime Checkcode_outdate
        {
            get { return _checkcode_outdate; }
            set { _checkcode_outdate = value; }
        }

        /// <summary>
        /// 当天发送的次数
        /// </summary>
        public int Send_num
        {
            get { return _send_num; }
            set { _send_num = value; }
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime Create_time
        {
            get { return _create_time; }
            set { _create_time = value; }
        }
    }
  }
