﻿using System;
namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Get coupon req.
    /// </summary>
    public class GetCouponReq
    {
        /// <summary>
        /// 活动ID
        /// </summary>
        /// <value>The activity identifier.</value>
        public int ActivityId { get; set; }
    }
}
