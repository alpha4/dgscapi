﻿using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 支付
    /// </summary>
    public class PaymentReq
    {
        /// <summary>
        /// 手机号码
        /// </summary>
        [Required]
        public string mobile { get; set; }
        /// <summary>
        /// 支付方式（1、App微信支付；2、支付宝App；6支付宝小程序支付  4:微信小程序支付
        /// </summary>
        [Required]
        public int paytype { get; set; }
        /// <summary>
        /// 操作方式1卡充值
        /// </summary>
        [Required]
        public int operationMethod { get; set; }
        /// <summary>
        /// 充值金额
        /// </summary>
        [Required]
        public double chargemoney { get; set; }
		/// <summary>
		/// 业务单据（卡号）
		/// </summary>
		[Required]
		public string billNO { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string summary { get; set; }

        /// <summary>
        /// 小程序专属id（支付宝/微信）
        /// </summary>
        public int id { get; set; }
		/// <summary>
		/// 门店id
		/// </summary>
		public int? shopId { get; set; }
    }
}
