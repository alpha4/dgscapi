﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class BindCardReq
    {
        /// <summary>
        /// 手机号
        /// </summary>
        [Required]
        public string Mobile { get; set; }
        /// <summary>
        /// 验证码(必填）
        /// </summary>
        [Required]
        public string Yzm { get; set; }
        /// <summary>
        /// 绑卡需要卡号
        /// </summary>
        /// 
        [Required()]
        [StringLength(16, MinimumLength = 6, ErrorMessage = "卡号长度6-16位")]
        public string Cardno { get; set; }
        /// <summary>
        /// 门店标识码
        /// </summary>
        /// <value>The shopid.</value>
        public string Shopid { get; set; }
    }
}
