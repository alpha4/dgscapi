﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Get get member info req.
    /// </summary>
    public class GetGetMemberInfoReq
    {
        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; set; }
        /// <summary>
        /// Gets or sets the card no.
        /// </summary>
        /// <value>The card no.</value>
        public string CardNO { get; set; }
        /// <summary>
        /// Gets or sets the token.
        /// </summary>
        /// <value>The token.</value>
        [Required]
        public string Token { get; set; }

    }
}
