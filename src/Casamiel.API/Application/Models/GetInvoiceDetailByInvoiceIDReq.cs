﻿using System;
namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Get invoice detail by invoice IDR eq.
    /// </summary>
    public class GetInvoiceDetailByInvoiceIDReq
    {
       /// <summary>
       /// Gets or sets the invoice identifier.
       /// </summary>
       /// <value>The invoice identifier.</value>
        public long InvoiceID { get; set; }
    }

}
