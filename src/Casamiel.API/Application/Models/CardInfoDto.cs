﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Card info dto.
    /// </summary>
    public class CardInfoDto
    {
        public string cardno { get; set; }
        public decimal totalmoney { get; set; }
        public decimal directmoney { get; set; }
        public decimal othermoney { get; set; }
        public int point { get; set; }

    }
}
