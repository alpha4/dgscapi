﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class GetConfigByKeyReq
    {
        /// <summary>
        /// 
        /// </summary>
        public string ConfigKey { get; set; }
    }
}
