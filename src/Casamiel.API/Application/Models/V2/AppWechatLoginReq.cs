﻿using System;
namespace Casamiel.API.Application.Models.V2
{
    /// <summary>
    /// App wechat login req.
    /// </summary>
    public class AppWechatLoginReq
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        public string Code { get; set; }
        /// <summary>
        /// Gets or sets the app identifier.
        /// </summary>
        /// <value>The app identifier.</value>
        public string AppId { get; set; }
        /// <summary>
        /// Gets or sets the registration identifier.
        /// </summary>
        /// <value>The registration identifier.</value>
        public string Registration_Id { get; set; }
    }
}
