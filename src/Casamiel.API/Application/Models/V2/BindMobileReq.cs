﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models.V2
{
    /// <summary>
    /// Bind mobile req.
    /// </summary>
    public class BindMobileReq
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        [Required]
        public string Mobile { get; set; }
        /// <summary>
        /// Gets or sets the yzm.
        /// </summary>
        /// <value>The yzm.</value>
        [Required]
        public string Yzm { get; set; }
        /// <summary>
        /// Gets or sets the registration identifier.
        /// </summary>
        /// <value>The registration identifier.</value>
        public string Registration_Id { get; set; }
    }
}
