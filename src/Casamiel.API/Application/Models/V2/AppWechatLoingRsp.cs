﻿using System;
using Casamiel.API.Infrastructure.Middlewares;
using Casamiel.Domain.Entity;

namespace Casamiel.API.Application.Models.V2
{
    /// <summary>
    /// App wechat loing rsp.
    /// </summary>
    public class AppWechatLoingRsp
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <value><c>true</c> if is reg; otherwise, <c>false</c>.</value>
        public bool IsReg { get; set; }
        /// <summary>
        /// Gets or sets the token.
        /// </summary>
        /// <value>The token.</value>
        public TokenEntity data { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value>The member.</value>
        public Member member { get; set; }
    }
}
