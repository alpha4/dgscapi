﻿using System;
namespace Casamiel.API.Application.Models.V2
{
    /// <summary>
    /// Get product req.
    /// </summary>
    public class GetProductReq
    {
        /// <summary>
        /// Gets or sets the storeid.
        /// </summary>
        /// <value>The storeid.</value>
         public int Storeid { get; set; }
        /// <summary>
        /// Gets or sets the productid.
        /// </summary>
        /// <value>The productid.</value>
         public int Productid { get; set; }
    }
}
