﻿using System;
namespace Casamiel.API.Application.Models.V2
{
    /// <summary>
    /// Wx save form identifier req.
    /// </summary>
    public class WxSaveFormIdReq
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the form identifier.
        /// </summary>
        /// <value>The form identifier.</value>
        public string FormId { get; set; }
    }
}
