﻿using System;
namespace Casamiel.API.Application.Models.V2
{
    /// <summary>
    /// Get scene req.
    /// </summary>
    public class GetSceneReq
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the scene.
        /// </summary>
        /// <value>The scene.</value>
        public string Scene { get; set; }
    }
}
