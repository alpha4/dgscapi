﻿using System;
namespace Casamiel.API.Application.Models.V2
{
    /// <summary>
    /// Get index goods list req.
    /// </summary>
    public class GetIndexGoodsListReq
    {
        /// <summary>
        /// Gets or sets the storeid.
        /// </summary>
        /// <value>The storeid.</value>
        public int Storeid { get; set; }
    }
}
