﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models.V2
{
    /// <summary>
    /// 小程序登陆
    /// </summary>
    public class WechatLogin
    {
        /// <summary>
        /// 用户code
        /// </summary>
        [Required]
        public string Code { get; set; }
        /// <summary>
        /// 小程序appid
        /// </summary>
        [Required]
        public string Appid { get; set; }
        /// <summary>
        /// Gets or sets the scene.
        /// </summary>
        /// <value>The scene.</value>
        public string Scene { get; set; }
    }
}
