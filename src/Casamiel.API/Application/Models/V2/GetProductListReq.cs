﻿using System;
namespace Casamiel.API.Application.Models.V2
{
    /// <summary>
    /// Get product list req.
    /// </summary>
    public class GetProductListReq
    {
        /// <summary>
        /// Gets or sets the tagid.
        /// </summary>
        /// <value>The tagid.</value>
        public int Tagid { get; set; }
        /// <summary>
        /// Gets or sets the pageindex.
        /// </summary>
        /// <value>The pageindex.</value>
        public int Pageindex { get; set; }
        /// <summary>
        /// Gets or sets the pagesize.
        /// </summary>
        /// <value>The pagesize.</value>
        public int Pagesize { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int StoreId { get; set; }
    }
}
