﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 注册会员绑实体卡
    /// </summary>
    public class RegBindCardReq
    {
        /// <summary>
        /// 手机号
        /// </summary>
        [Required]
        public string Mobile { get; set; }
        /// <summary>
        /// 验证码(必填）
        /// </summary>
        [Required]
        public string Yzm { get; set; }
        /// <summary>
        /// 绑卡需要卡号
        /// </summary>
        /// 
        [Required]
        public string Cardno { get; set; }
        /// <summary>
        /// 设备号
        /// </summary>
        public string Registration_Id { get; set; }

        /// <summary>
        /// 邀请码/推荐码
        /// </summary>
        [StringLength(50, ErrorMessage = "邀请码最大长度为50位")]
        public string InvitationCode { get; set; }
    }
}
