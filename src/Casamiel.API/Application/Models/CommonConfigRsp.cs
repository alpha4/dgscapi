﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class CommonConfigRsp
    {
        /// <summary>
        /// 键
        /// </summary>
        public string ConfigKey { get; set; }

        /// <summary>
        /// 键值
        /// </summary>
        public string ConfigValue { get; set; }
    }
}
