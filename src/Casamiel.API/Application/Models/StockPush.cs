﻿using System;
using System.Collections.Generic;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Productstock item.
    /// </summary>
    public class ProductstockItem
    {
        /// <summary>
        /// 
        /// </summary>
        public int Shopid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Shopname { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<ProductItem> Product { get; set; }
    }
    /// <summary>
    /// Stock push.
    /// </summary>
    public class StockPush
    {
        /// <summary>
        /// 
        /// </summary>

        public string Timestamp { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Stocktime { get; set; }

		/// <summary>
		/// 
		/// </summary>
#pragma warning disable CA2227 // 集合属性应为只读
		public List<ProductstockItem> Productstock { get; set; }
#pragma warning restore CA2227 // 集合属性应为只读


		/// <summary>
		/// 
		/// </summary>
		public int Source { get; set; }
    }
}
