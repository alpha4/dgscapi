﻿using System;
namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Version req.
    /// </summary>
    public class VersionReq
    {
       /// <summary>
       /// Gets or sets the type.
       /// </summary>
       /// <value>The type.</value>
        public int type { get; set; }
    }
}
