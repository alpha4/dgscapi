﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 解绑会员卡
    /// </summary>
    public class UnicCard
    {
        /// <summary>
        /// Gets or sets the cardno.
        /// </summary>
        /// <value>The cardno.</value>
        [Required]
        public string cardno { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        [Required]
        public string mobile { get; set; }
    }
}
