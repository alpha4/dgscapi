﻿using System;
using Newtonsoft.Json;

namespace Casamiel.API.Application.Models.Ele
{
    /// <summary>
    /// Notify message req.
    /// </summary>
    public class NotifyMessageReq
    {
        /// <summary>
        /// 应用id，应用创建时系统分配的唯一id
        /// </summary>
        public int AppId { get; set; }
        /// <summary>
        /// 消息的唯一id，用于唯一标记每个消息
        /// </summary>
        public string RequestId { get; set; }
        /// <summary>
        /// 消息类型
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// JSON格式字符串
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 商户的店铺id
        /// </summary>
        public int ShopId { get; set; }
        /// <summary>
        /// 消息发送的时间戳
        /// </summary>
        public long Timestamp { get; set; }
        /// <summary>
        /// 授权商户的账号id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 消息签名，32位全大写
        /// </summary>
        public string Signature { get; set; }

		/// <summary>
		/// 
		/// </summary>
        public string IcTradeNo { get; set; }
        /// <summary>
        /// Gets or sets the bill no.
        /// </summary>
        /// <value>The bill no.</value>
       
        public string BillNo { get; set; }
    }
}
