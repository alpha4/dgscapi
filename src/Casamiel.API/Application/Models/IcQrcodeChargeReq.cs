﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 礼券充值
    /// </summary>
    public class IcQrcodeChargeReq
    {
        /// <summary>
        /// 卡号
        /// </summary>
        public string Cardno { get; set; }
        /// <summary>
        /// 充值码
        /// </summary>
        [Required(AllowEmptyStrings =false,ErrorMessage ="不能为空")]
        [StringLength(32,ErrorMessage ="码有误")]
        public string Qrcode { get; set; }
        
    }
}
