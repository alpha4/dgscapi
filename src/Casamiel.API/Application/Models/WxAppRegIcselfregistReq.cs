﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Wx app reg icselfregist dto.
    /// </summary>
    public class WxAppRegIcselfregistReq
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [Required]
        public int Id { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        [Required]
        public string Mobile { get; set; }
        /// <summary>
        /// 验证码(必填）
        /// </summary>
        [Required]
        public string Yzm { get; set; }

        /// <summary>
        /// 设备号
        /// </summary>
        public string Registration_Id { get; set; }
    }
}
