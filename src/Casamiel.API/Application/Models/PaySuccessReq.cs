﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PaySuccessReq
    {
        /// <summary>
        /// 金额
        /// </summary>
        [Required]
        public double amount { get; set; }
        /// <summary>
        /// 第三方支付交易号(支付宝，微信单据号）
        /// </summary>
       [Required]
       public string outTradeNO { get; set; }
        /// <summary>
        /// 
        /// </summary>
       [Required]
       public string tradeNo { get; set; }


    }
}
