﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 充值消费记录
    /// </summary>
    public class ChargeRecord
    {
        /// <summary>
        /// 
        /// </summary>
        public string wxopenid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string cardno { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int shopid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string shopname { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string recdate { get; set; }
        /// <summary>
        ///  值为：消费、退货、充值、退充
        /// </summary>
        public string consumetype { get; set; }
        /// <summary>
        /// 唯一交易号
        /// </summary>
        public string tradeno { get; set; }
        /// <summary>
        /// 唯一交易号
        /// </summary>
        public double totalmoney { get; set; }
        /// <summary>
        /// 折扣金额
        /// </summary>
        public double discountmoney { get; set; }
        /// <summary>
        /// 会员卡实付金额
        /// </summary>
        public double realpaymoney { get; set; }
        /// <summary>
        /// 使用卡上直接金额（充值金额）
        /// </summary>
        public double directmoney { get; set; }
        /// <summary>
        /// 使用卡上直接金额（充值金额）
        /// </summary>
        public double othermoney { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int point { get; set; }
    }
}
