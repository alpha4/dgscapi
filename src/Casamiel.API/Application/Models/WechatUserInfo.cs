﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>  
    /// 微信小程序用户信息结构  
    /// </summary>  
    public class WechatUserInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string openId { get; set; }
        /// <summary>
        /// Gets or sets the name of the nick.
        /// </summary>
        /// <value>The name of the nick.</value>
        public string NickName { get; set; }
        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        /// <value>The gender.</value>
        public int Gender { get; set; }
        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The city.</value>
        public string city { get; set; }
        /// <summary>
        /// Gets or sets the province.
        /// </summary>
        /// <value>The province.</value>
        public string province { get; set; }
        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        public string country { get; set; }
        /// <summary>
        /// Gets or sets the avatar URL.
        /// </summary>
        /// <value>The avatar URL.</value>
        public string avatarUrl { get; set; }
        /// <summary>
        /// Gets or sets the union identifier.
        /// </summary>
        /// <value>The union identifier.</value>
        public string unionId { get; set; }
        /// <summary>
        /// Gets or sets the watermark.
        /// </summary>
        /// <value>The watermark.</value>
        public Watermark watermark { get; set; }
        
    }

    /// <summary>
    /// 
    /// </summary>
    public class Watermark
    {
        /// <summary>
        /// Gets or sets the appid.
        /// </summary>
        /// <value>The appid.</value>
        public string appid { get; set; }
        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>The timestamp.</value>
        public string timestamp { get; set; }
    }
}
