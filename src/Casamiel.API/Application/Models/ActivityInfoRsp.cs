﻿using System;
namespace Casamiel.API.Application.Models
{
  /// <summary>
  /// Activity info rsp.
  /// </summary>
    public class ActivityInfoRsp
    {
		/// <summary>
		/// 
		/// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the name of the activity.
        /// </summary>
        /// <value>The name of the activity.</value>
        public string ActivityName { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        /// <value>The describe.</value>
        public string Describe { get; set; }
        /// <summary>
        /// Gets or sets the begin time.
        /// </summary>
        /// <value>The begin time.</value>
        public DateTime BeginTime { get; set; }
        /// <summary>
        /// Gets or sets the end time.
        /// </summary>
        /// <value>The end time.</value>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// 领用次数
        /// </summary>
        /// <value>The total.</value>
        public int Total { get; set; }
        /// <summary>
        /// </summary>
        /// <value><c>true</c> if enable; otherwise, <c>false</c>.</value>
        public bool Enable { get; set; }
    }
}
