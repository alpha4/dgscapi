﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 用户信息
    /// </summary>
    public class AccountReq
    {
        /// <summary>
        /// 昵称
        /// </summary>
        [StringLength(20, ErrorMessage = "昵称最大长度为20位")]
        public string Nick { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        [StringLength(10, ErrorMessage = "真实姓名最大长度为10位")]
        public string TrueName { get; set; }
        /// <summary>
        /// 性别（1男，2女）
        /// </summary>
        public int? Sex { get; set; }
        /// <summary>
        /// 生日
        /// </summary>
        public DateTime? Birthday { get; set; }
        /// <summary>
        /// email
        /// </summary>
        [StringLength(128, ErrorMessage = "Email最大长度为128位")]
        public string Email { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StringLength(50, ErrorMessage = "邀请码最大长度为50位")]
        public string InvitationCode { get; set; }
    }
}
