﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 优惠券
    /// </summary>
    public class Ticket
    {
        /// <summary>
        /// 
        /// </summary>
        public string cardno { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ticketid { get; set; }
        /// <summary>
        /// 券名称
        /// </summary>
        public string ticketname { get; set; }
        /// <summary>
        /// 充值赠送券
        /// </summary>
        public string tickettype { get; set; }
        /// <summary>
        /// 券来源
        /// </summary>
        public string source { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int state { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double je { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string makedate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string startdate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string enddate { get; set; }

        public List<PrangeItem> prange { get; set; }

    }
    public class PrangeItem
    {
        /// <summary>
        /// 
        /// </summary>
        public int pid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int count { get; set; }
    }
}
