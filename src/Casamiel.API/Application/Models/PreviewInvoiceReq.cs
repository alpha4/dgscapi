﻿using System;
namespace Casamiel.API.Application.Models
{
    /// <summary>
    ///开票预览
    /// </summary>
    public class PreviewInvoiceReq
    {
        /// <summary>
        /// 流行号集合
        /// </summary>
        /// <value>The identifiers.</value>
        public string Ids { get; set; }
    }
}
