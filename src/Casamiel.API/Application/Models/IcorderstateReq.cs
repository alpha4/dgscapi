﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 订单状态
    /// </summary>
    public class IcorderstateReq
    {
        /// <summary>
        /// 会原卡号
        /// </summary>
        public string Cardno { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Phoneno { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// 唯一交易号
        [Required(ErrorMessage= "唯一交易号")]
        public string Tradeno { get; set; }
        /// <summary>
        /// 2016-02-16 12:01:02
        /// 格式为yyyy-MM-dd HH-mm-ss
        /// </summary>
        public string Timestamp { get; set; }
    }
}
