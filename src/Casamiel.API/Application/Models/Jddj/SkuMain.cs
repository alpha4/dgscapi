﻿using System;
using System.Collections.Generic;
namespace Casamiel.API.Application.Models.Jddj
{

    /// <summary>
    /// Sku main.
    /// </summary>
    public class SkuMain
    {
        /// <summary>
        /// 
        /// </summary>
        public long skuId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string outSkuId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int orgCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int categoryId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int brandId { get; set; }
        /// <summary>
        /// 可莎蜜兒 美式咖啡 （冷）500ml/杯
        /// </summary>
        public string skuName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string slogan { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int skuPrice { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int stockNum { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double weight { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string upcCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int payType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string fixedUpTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string fixedDownTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int fixedStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<long> shopCategories { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<long> sellCities { get; set; }
    }
}
