﻿using System;
namespace Casamiel.API.Application.Models.Jddj
{
    /// <summary>
    /// Ts.
    /// </summary>
    public class Ts
    {
        /// <summary>
        /// 
        /// </summary>
        public int date { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int day { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int hours { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int minutes { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int month { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int nanos { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int seconds { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long time { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int timezoneOffset { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int year { get; set; }
    }
    /// <summary>
    /// Update time.
    /// </summary>
    public class UpdateTime
    {
        /// <summary>
        /// 
        /// </summary>
        public int date { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int day { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int hours { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int minutes { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int month { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int seconds { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long  time { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int timezoneOffset { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int year { get; set; }
    }
    /// <summary>
    /// Create time.
    /// </summary>

    public class CreateTime
    {
        /// <summary>
        /// 
        /// </summary>
        public int date { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int day { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int hours { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int minutes { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int month { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int seconds { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long  time { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int timezoneOffset { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int year { get; set; }
    }
    /// <summary>
    /// Store info.
    /// </summary>
    public class StoreInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string firstOrderTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string phone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int qualifyStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int orderAging { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Ts ts { get; set; }
        /// <summary>
        /// 杭州市
        /// </summary>
        public string cityName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string venderId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string coordinateAddress { get; set; }
        /// <summary>
        /// 杭州市
        /// </summary>
        public string provinceName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int city { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string serviceTimeEnd1Str { get; set; }
        /// <summary>
        /// _sn_11766903vi_326321p_1213c_1213co_2963oi_222105wt_2sne_可莎蜜兒（工厂店）sa_浙江省杭州市江干区3号大街201yn_0pe_2
        /// </summary>
        public string cacheKey4StoreList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string deliveryServiceName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string shopId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string serviceTimeEnd2Str { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int province { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int supportOfflinePurchase { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int timePmType { get; set; }
        /// <summary>
        /// 可莎蜜兒（工厂店）
        /// </summary>
        public string stationName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int closeStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public UpdateTime updateTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int timeAmType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int serviceTimeStart1 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string serviceTimeStart2 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string onlineTime { get; set; }
        /// <summary>
        /// 门店表标识
        /// </summary>
        public string outSystemId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string blockId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int county { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string createPin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string town { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string serviceTimeStart2Str { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public CreateTime createTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string stationNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string storeDisInfoList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string standByPhone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int innerNoStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string townName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string storeNotice { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double lng { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string selfPickSupport { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string secondQualifyStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int industryTag { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 江干区
        /// </summary>
        public string countyName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int preWarehouse { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string busiType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int isNoPaper { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string queryPhone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string updatePin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double lat { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string logo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string popVenderId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string coordinate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string deliveryServiceType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int stationDeliveryStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string blockName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string qrCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int isAutoOrder { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int regularFlag { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int carrierNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string otherPlatformUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string source { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int serviceTimeEnd1 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string serviceTimeEnd2 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string serviceTimeStart1Str { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string doorPhotoUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int yn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int supportInvoice { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int easyPurchaseStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int isMembership { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int wareType { get; set; }
        /// <summary>
        /// 浙江省杭州市江干区3号大街201
        /// </summary>
        public string stationAddress { get; set; }
        /// <summary>
        /// 京东到家-杭州可莎蜜儿
        /// </summary>
        public string venderName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int orderNoticeType { get; set; }
    }

}