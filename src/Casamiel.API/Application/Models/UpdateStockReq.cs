﻿using System;
namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Update stock req.
    /// </summary>
    public class UpdateStockReq
    {
        /// <summary>
        /// Gets or sets the token.
        /// </summary>
        /// <value>The token.</value>
        public string token { get; set; }
        /// <summary>
        /// Gets or sets the sign.
        /// </summary>
        /// <value>The sign.</value>
        public string sign { get; set; }
        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>The content.</value>
        public  string content { get; set; }
    }
}
