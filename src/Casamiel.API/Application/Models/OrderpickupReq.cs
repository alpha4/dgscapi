﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Orderpickup req.
    /// </summary>
    public class OrderpickupReq
    {
        /// <summary>
        /// 一网交易号
        /// </summary>
        /// <value>The tradeno.</value>
        [Required]
        public string Tradeno { get; set; }
        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        [Required]
        public string Mobile { get; set; }
        /// <summary>
        /// Gets or sets the secrect.
        /// </summary>
        /// <value>The secrect.</value>
        [Required]
        public string Secrect { get; set; }
    }
}
