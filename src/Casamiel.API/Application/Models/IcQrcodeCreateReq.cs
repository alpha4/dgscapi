﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Ic qrcode create dto.
    /// </summary>
    public class IcQrcodeCreateReq
    {
        /// <summary>
        /// Gets or sets the cardno.
        /// </summary>
        /// <value>The cardno.</value>
        public string Cardno { get; set; }
        /// <summary>
        /// Gets or sets the phoneno.
        /// </summary>
        /// <value>The phoneno.</value>
        public string Phoneno { get; set; }
        /// <summary>
        /// Gets or sets the qrcodetype.
        /// 必填项，二维码处理方式，1、充值；2、充券(会员)；11、获取门店标识(shopid)；暂时充值、充券不可用
        /// </summary>
        /// <value>The qrcodetype.</value>
        [Required(ErrorMessage ="二维码处理方式")]
        public string Qrcodetype { get; set; }
        /// <summary>
        /// 门店id
        /// </summary>
        /// <value>The shopid.</value>
        public int Shopid { get; set; }
        /// <summary>
        /// 选填项，如果填写，就使用特定门店的配置
        /// </summary>
        /// <value>The shopno.</value>
        public string Shopno { get; set; }
        /// <summary>
        /// Gets or sets the shopname.
        /// ：选填项，如果填写，就使用特定门店的配置
        /// </summary>
        /// <value>The shopname.</value>
        public string Shopname { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// ”2016-02-16 12:01:02”
        /// </summary>
        /// <value>The timestamp.</value>
        public string Timestamp { get; set; }

    }
}
