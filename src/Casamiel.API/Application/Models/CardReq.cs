﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class CardReq
    {
        /// <summary>
        /// 卡号
        /// </summary>
        [Required]
        public string cardno { get; set; }
        
    }
    /// <summary>
    /// 检测消费
    /// </summary>
    public class CheckConsumecodeReq
    {
        /// <summary>
        /// 卡号
        /// </summary>
        [Required]
        [StringLength(16, MinimumLength = 6, ErrorMessage = "卡号长度6-16位")]
        public string cardno { get; set; }
        /// <summary>
        /// 消费码
        /// </summary>
        [Required]
        public string consumecode { get; set; }
    }
}
