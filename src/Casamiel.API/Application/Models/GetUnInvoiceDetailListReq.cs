﻿using System;
namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 待开票请求
    /// </summary>
    public class GetUnInvoiceDetailListReq
    {
        /// <summary>
        /// Gets or sets the index of the page.
        /// </summary>
        /// <value>The index of the page.</value>
        public int PageIndex { get; set; }
        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; set; }
    }
}
