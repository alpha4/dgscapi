﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 微信登录
    /// </summary>
    public class WechatLoginReq
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string code { get; set; }
    }
}
