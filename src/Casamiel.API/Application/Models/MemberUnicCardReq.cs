﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    ///解绑会员卡
    /// </summary>
    public class MemberUnicCardReq
    {
        /// <summary>
        /// 卡号
        /// </summary>
        [Required(ErrorMessage ="请输入卡号")]
        public string cardno { get; set; }
        /// <summary>
        /// 验证码
        /// </summary>
        [Required(ErrorMessage ="请输入验证码")]
        public string yzm { get; set; }
    }
}
