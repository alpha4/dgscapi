﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Pay dto.
    /// </summary>
    public class WechatPayReq
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        [Required]
        public string code { get; set; }
        /// <summary>
        /// Gets or sets the summary.
        /// </summary>
        /// <value>The summary.</value>
        public string summary { get; set; }

    }

    /// <summary>
    /// Wxpay dto.
    /// </summary>
    public class WxpayReq
    {
        /// <summary>
        /// 手机号码
        /// </summary>
        [Required]
        public string mobile { get; set; }

        /// <summary>
        /// 操作方式1卡充值
        /// </summary>
        [Required]
        public int operationMethod { get; set; }
        /// <summary>
        /// 充值金额
        /// </summary>
        [Required]
        public double chargemoney { get; set; }
        /// <summary>
        /// 业务单据（卡号）
        /// </summary>
        public string billNO { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string summary { get; set; }
#pragma warning disable IDE1006 // 命名样式
        /// <summary>
        /// 返回url
        /// </summary>
        public string returnUrl { get; set; }
#pragma warning restore IDE1006 // 命名样式
    }
}
