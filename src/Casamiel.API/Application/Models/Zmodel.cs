﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Domain.Response;
using MediatR;
using Newtonsoft.Json;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Zmodel.
    /// </summary>
    public class Zmodel
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        public int code { get; set; }
        /// <summary>
        /// 未指定参数
        /// </summary>
        public string msg { get; set; }
        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>The content.</value>
        public string content { get; set; }
    }
    /// <summary>
    /// Zmodel ext.
    /// </summary>
    public class ZmodelExt : Zmodel
    {
        /// <summary>
        /// Gets or sets the pagesize.
        /// </summary>
        /// <value>The pagesize.</value>
        public int pagesize { get; set; }
        /// <summary>
        /// Gets or sets the pageindex.
        /// </summary>
        /// <value>The pageindex.</value>
        public int pageindex { get; set; }
        /// <summary>
        /// Gets or sets the pagecount.
        /// </summary>
        /// <value>The pagecount.</value>
        public int pagecount { get; set; }
    }

    /*
    /// <summary>
    /// BaseResult
    /// </summary>
    public class BaseResult<T>
    {
        /// <summary>
        /// The code.
        /// </summary>
        protected int _code;
        /// <summary>
        /// The content.
        /// </summary>
        protected T _content;
        /// <summary>
        /// The message.
        /// </summary>
        protected string _msg;
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Controllers.V2.sModel`1"/> class.
        /// </summary>
        /// <param name="defaultValue">Default value.</param>
        public BaseResult(T defaultValue)
        {
            _content = defaultValue;
            _code = 0;
            _msg = "";

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Application.Models.BaseResult`1"/> class.
        /// </summary>
        public BaseResult()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Controllers.V2.sModel`1"/> class.
        /// </summary>
        /// <param name="defaultValue">Default value.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        public BaseResult(T defaultValue, int code, string msg)
        {
            _content = defaultValue;
            _code = code;
            _msg = msg;

        }
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        public int code { get { return _code; } }
        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>The content.</value>
        public T content
        { get { return _content; } }
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string msg
        { get { return _msg; } }

    }
    */
    /// <summary>
    /// Paging results.
    /// </summary>
    public class PagingResults<T>  
    {
         

        /// <summary>
        ///  
        /// </summary>
        /// <param name="defaultValue">Default value.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="total">Total.</param>
        /// <param name="code">Code.</param>
        /// <param name="msg">Message.</param>
        public PagingResults(T defaultValue, int pageIndex, int pageSize, int total, int code, string msg)
        {
            Code = code;
            Msg = msg;
            Content = defaultValue;
           this.pageSize = pageSize;
            this.pageIndex = pageIndex;
           this.total = total;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Msg { get; private set; }
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        public int Code { get; private set; }
        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>The content.</value>
        public T Content
        { get; private set; }
        /// <summary>
        /// Gets the total.
        /// </summary>
        /// <value>The total.</value>
        public int total { get; private set; }
        /// <summary>
        /// Gets the index of the page.
        /// </summary>
        /// <value>The index of the page.</value>
        public int pageIndex { get; private set; }
        /// <summary>
        /// Gets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int pageSize { get; private set; }
    }
    /// <summary>
    /// Nlog entity.
    /// </summary>
    public class NlogEntity:IRequest<bool>
    {
		/// <summary>
		/// 
		/// </summary>
        public string Message { get; set; }
    }
    /// <summary>
    /// New user handler.
    /// </summary>
    public class NewUserHandler : IRequestHandler<NlogEntity, bool>
    {
        private readonly NLog.ILogger logger = NLog.LogManager.GetLogger("OrderService");
       /// <summary>
       /// Handle the specified request and cancellationToken.
       /// </summary>
       /// <returns>The handle.</returns>
       /// <param name="request">Request.</param>
       /// <param name="cancellationToken">Cancellation token.</param>
        public Task<bool> Handle(NlogEntity request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            logger.Trace(request.Message);
            // save to database  
            return Task.FromResult(true);
        }
    }
    /// <summary>
    /// NE rror entity.
    /// </summary>
    public class NErrorEntity:INotification{
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message { get; set; }
        /// <summary>
        /// Gets or sets the create time.
        /// </summary>
        /// <value>The create time.</value>
        public DateTime CreateTime { get; set; }
    }
    /// <summary>
    /// NE rror handler.
    /// </summary>
    public class NErrorHandler : INotificationHandler<NErrorEntity>
    {
        private readonly NLog.ILogger logger = NLog.LogManager.GetLogger("OrderService");
        /// <summary>
        /// Handle the specified notification and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="notification">Notification.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public Task Handle(NErrorEntity notification, CancellationToken cancellationToken)
        {
            //Save to log  
            logger.Trace($"NErrorHandler:{JsonConvert.SerializeObject(notification)}");
            return Task.FromResult(true);
        }
    }
	/// <summary>
	/// 
	/// </summary>
    public class LogHandler2 : INotificationHandler<NErrorEntity>
    {
        private readonly NLog.ILogger logger = NLog.LogManager.GetLogger("OrderService");
        /// <summary>
        /// Handle the specified notification and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="notification">Notification.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public Task Handle(NErrorEntity notification, CancellationToken cancellationToken)
        {
            //Save to log  
            logger.Trace($"Save to log2 :{JsonConvert.SerializeObject(notification)}");
            Console.WriteLine($" ****  User save to log  *****");
            return Task.FromResult(true);
        }
    }
    
    /// <summary>
    /// Log handler.
    /// </summary>
    public class LogHandler : INotificationHandler<NErrorEntity>
    {
        private readonly NLog.ILogger logger = NLog.LogManager.GetLogger("OrderService");
        /// <summary>
        /// Handle the specified notification and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="notification">Notification.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public Task Handle(NErrorEntity notification, CancellationToken cancellationToken)
        {
        //Save to log  
        logger.Trace($"Save to log :{JsonConvert.SerializeObject(notification)}");
            Console.WriteLine(" ****  User save to log  *****");
            return Task.FromResult(true);
        }
    }
}
