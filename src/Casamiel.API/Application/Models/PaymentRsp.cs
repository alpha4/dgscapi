﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
	/// <summary>
	/// 支付请求响应
	/// </summary>
	public class BasePaymentRsp
	{
		/// <summary>
		/// 
		/// </summary>
		public string Paymentrequest { get; set; }
			/// <summary>
			/// 
			/// </summary>
		public string Ordercode { get; set; }
		/// <summary>
		/// 支付方式2app支付宝支付，4小程序
		/// </summary>
		public string PayMethod { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public bool Payed { get; set; }
		//paymentrequest = paymentrequsta, ordercode = reorder.OrderCode, payMethod = payment.PayMethod.ToString(), payed = false
	}
}
