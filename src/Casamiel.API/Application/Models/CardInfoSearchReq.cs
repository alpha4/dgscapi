﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 查询会员卡消费和充值记录
    /// </summary>
    public class CardInfoSearchReq
    {
        /// <summary>
        /// 卡号
        /// </summary>
        [Required]
        [StringLength(16, MinimumLength = 6, ErrorMessage = "卡号长度6-16位")]
        public string Cardno { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        [Required]
        public DateTime Startdate { get; set; }
        /// <summary>
        /// 结束时间 
        /// </summary>
        [Required]
        public DateTime Enddate { get; set; }
    }
}
