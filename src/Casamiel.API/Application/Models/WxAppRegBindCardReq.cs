﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 注册会员绑实体卡
    /// </summary>
    public class WxAppRegBindCardReq
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public int id { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        [Required]
        public string mobile { get; set; }
        /// <summary>
        /// 验证码(必填）
        /// </summary>
        [Required]
        public string yzm { get; set; }
        /// <summary>
        /// 绑卡需要卡号
        /// </summary>
        /// 
        [Required]
        public string Cardno { get; set; }
        /// <summary>
        /// 设备号
        /// </summary>
        public string registration_Id { get; set; }
    }
}
