﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 消费码
    /// </summary>
    public class Qrbarcode
    {
        /// <summary>
        /// 
        /// </summary>
        public string wxopenid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string cardno { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string consumecode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string qrcodeurl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string barcodeurl { get; set; }
    }
}
