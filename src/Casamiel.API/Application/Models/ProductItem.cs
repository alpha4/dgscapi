﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{/// <summary>
/// 
/// </summary>
    public class ProductItem
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string pid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string pname { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int count { get; set; }

        /// <summary>
        /// 折扣率
        /// 大于0小于等于1
        /// </summary>
        /// <value>The rebaterate.</value>
        public decimal rebaterate { get; set; }
    }

}
