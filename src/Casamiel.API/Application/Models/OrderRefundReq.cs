﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Order refund req.
    /// </summary>
    public class OrderRefundReq
    {
        /// <summary>
        /// Gets or sets the order code.
        /// </summary>
        /// <value>The order code.</value>
        [Required]
        public string OrderCode { get; set; }

        /// <summary>
        /// Gets or sets the cancel reason.
        /// </summary>
        /// <value>The cancel reason.</value>
        public string CancelReason { get; set; }
    }
}
