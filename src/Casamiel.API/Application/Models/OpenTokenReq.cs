﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class OpenTokenReq
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]

        public string clientId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string clientsecret { get; set; }
    }
}
