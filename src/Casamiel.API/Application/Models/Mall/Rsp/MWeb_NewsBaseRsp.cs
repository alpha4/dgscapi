﻿using System;
namespace Casamiel.API.Application.Models.Mall.Rsp
{
    /// <summary>
    /// MW eb news base rsp.
    /// </summary>
    public class MWeb_NewsBaseRsp
    {

        /// <summary>
        /// 图文信息id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 副标题
        /// </summary>
        public string SubTitle { get; set; }
        /// <summary>
        ///图片
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
    }
}
