﻿using System;
using System.Collections.Generic;

namespace Casamiel.API.Application.Models.Mall.Rsp
{
    /// <summary>
    /// Index product base rsp.
    /// </summary>
    public class Index_ProductBaseRsp
    {
        /// <summary>
        /// 标签名称
        /// </summary>
        public string TagName { get; set; }
        /// <summary>
        /// 标签Id
        /// </summary>
        public int TagId { get; set; }
        /// <summary>
        /// 标签背景图
        /// </summary>
        public string TagBigImage { get; set; }

        /// <summary>
        /// 商品列表
        /// </summary>
        public List<ProductBaseRsp> ProductList { get; set; }
    }
}
