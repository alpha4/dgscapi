﻿using System;
namespace Casamiel.API.Application.Models.Mall.Rsp
{
    /// <summary>
    /// Product base rsp.
    /// </summary>
     public class ProductBaseRsp
    {
        /// <summary>
        /// 产品Id
        /// </summary>
        public int ProductId { get; set; }
        /// <summary>
        /// 产品标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 副标题
        /// </summary>
        public string SubTitle { get; set; }
        /// <summary>
        /// 产品图片
        /// </summary>
        public string Image { get; set; }
        /// <summary>
        /// 产品价格
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 排序字段
        /// </summary>
        public int OrderNo { get; set; }        
    }
}
