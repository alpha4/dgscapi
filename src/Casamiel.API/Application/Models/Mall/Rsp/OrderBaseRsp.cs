﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;

namespace Casamiel.API.Application.Models.Mall.Rsp
{
    /// <summary>
    /// MW eb order base rsp.
    /// </summary>
    public class MWebOrderBaseRsp
    {

        /// <summary>
        /// 订单id
        /// </summary>
        public int OrderBaseId { get; set; }
        /// <summary>
        /// 订单编码
        /// </summary>
        public string OrderCode { get; set; }
        /// <summary>
        /// IC交易号
        /// </summary>
        public string IcTradeNO { get; set; }
        /// <summary>
        /// 销售号
        /// </summary>
        public string BillNo { get; set; }
        /// <summary>
        /// 处理完成单号
        /// </summary>
        public string ResBillNo { get; set; }
        /// <summary>
        /// 订单状态，1：新建、2：已支付、3:退款中、4：退款成功 、5：申请取消、 7：已完成 、8：已取消(退款) 、9：交易关闭(未支付自动取消) 
        /// </summary>
        public int OrderStatus { get; set; }
        /// <summary>
        /// 快递状态，1：待发货、2：已发货、3：已签收
        /// </summary>
        public int ExpressStatus { get; set; }
        /// <summary>
        /// 下单时间
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 支付时间
        /// </summary>
        public string PayTime { get; set; }
        /// <summary>
        /// 发货时间
        /// </summary>
        public string ShipmentsTime { get; set; }
        /// <summary>
        /// 确认收货到期时间
        /// </summary>
        public string ConfrimReceiveTime { get; set; }
        /// <summary>
        /// 支付方式 :  1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
        /// </summary>
        public int PayMethod { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
        public string ContactName { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string ContactPhone { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 订单应付金额
        /// </summary>
        public decimal OrderMoney { get; set; }
        /// <summary>
        /// 订单支付金额
        /// </summary>
        public decimal PayMoney { get; set; }
        /// <summary>
        /// 优惠金额
        /// </summary>
        public decimal DiscountMoney { get; set; }
        /// <summary>
        /// 运费金额
        /// </summary>
        public decimal FreightMoney { get; set; }
        /// <summary>
        /// 收获地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 订单商品
        /// </summary>
        public List<MWebOrderGoodsBaseRsp> OrderGoodsList { get; set; }
		#region 优惠券信息
		/// <summary>
		/// 状态
		/// </summary>
		public int DiscountCouponStatus { get; set; }
        /// <summary>
        /// id
        /// </summary>
        public int DiscountCouponId { get; set; }
        /// <summary>
        /// 卡号
        /// </summary>
        public string CardNo { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        public decimal DiscountCouponActualMoney { get; set; }
        /// <summary>
        /// 优惠券名称
        /// </summary>
        public string DiscountCouponName { get; set; }
        #endregion
        #region 物流信息
        /// <summary>
        /// 快递单号
        /// </summary>
        public string ExpressCode { get; set; }
        /// <summary>
        /// 物流公司
        /// </summary>
        public string ExpressName { get; set; }
        /// <summary>
        /// 物流轨迹
        /// </summary>
        public List<Trace> ExpressTraces { get; set; }
        
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public InvoiceInfo InvoiceModel { get; set; }
    }
    /// <summary>
    /// Trace.
    /// </summary>
    public class Trace
    {
        /// <summary>
        /// 时间
        /// </summary>
        public DateTime AcceptTime { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string AcceptStation { get; set; }
    }

}
