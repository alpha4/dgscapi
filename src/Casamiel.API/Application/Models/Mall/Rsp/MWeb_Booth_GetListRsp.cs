﻿using System;
namespace Casamiel.API.Application.Models.Mall.Rsp
{
    /// <summary>
    /// MW eb booth get list rsp.
    /// </summary>
    public class MWeb_Booth_GetListRsp
    {
        /// <summary>
        /// 信息id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 副标题
        /// </summary>
        public string SubTitle { get; set; }
        /// <summary>
        /// 跳转类型 1图文 2 商品 3 营销活动 4 无跳转
        /// </summary>
        public int LinkType { get; set; }
        /// <summary>
        /// 链接地址 或者Id或产品id  视情况定义
        /// </summary>
        public string Link { get; set; }
        /// <summary>
        /// 图片地址
        /// </summary>
        public string ImageUrl { get; set; }
    }
}
