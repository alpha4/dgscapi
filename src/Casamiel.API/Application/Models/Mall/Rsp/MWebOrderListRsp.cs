﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;

namespace Casamiel.API.Application.Models.Mall.Rsp
{
    /// <summary>
    /// MW eb order list rsp.
    /// </summary>
    public class MWebOrderListRsp
    {

        /// <summary>
        /// 订单id
        /// </summary>
        public int OrderBaseId { get; set; }
        /// <summary>
        /// 订单编码
        /// </summary>
        public string OrderCode { get; set; }
        /// <summary>
        /// 订单状态，1：新建、2：已支付、3:退款中、4：退款成功 、5：申请取消、 7：已完成 、8：已取消(退款) 、9：交易关闭(未支付自动取消) 
        /// </summary>
        public int OrderStatus { get; set; }

        /// <summary>
        /// 快递状态，1：待发货、2：已发货、3：已签收
        /// </summary>
        public int ExpressStatus { get; set; }

        /// <summary>
        /// 订单应付金额
        /// </summary>
        public decimal OrderMoney { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

		/// <summary>
		/// 订单商品
		/// </summary>
#pragma warning disable CA2227 // 集合属性应为只读
		public List<MWebOrderGoodsBaseRsp> OrderGoodsList { get; set; }
#pragma warning restore CA2227 // 集合属性应为只读
	}

}
