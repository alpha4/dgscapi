﻿using System;
namespace Casamiel.API.Application.Models.Mall.Rsp
{
    /// <summary>
    /// Index tag base rsp.
    /// </summary>
    public class Index_TagBaseRsp
    {
        /// <summary>
        /// 标签id
        /// </summary>
        public int TagBaseId { get; set; }
        /// <summary>
        /// 标签名称
        /// </summary>
        public string TagName { get; set; }
        /// <summary>
        /// 标签图标
        /// </summary>
        public string SImgUrl { get; set; }
    }
}
