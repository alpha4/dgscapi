﻿using System;
namespace Casamiel.API.Application.Models.Mall.Req
{
    /// <summary>
    /// MW eb order get list req.
    /// </summary>
    public class MWebOrderGetListReq
    {
        /// <summary>
        /// 订单状态 99 全部订单 1 待付款 2 待取货 3 已完成
        /// </summary>
        /// <value>The status.</value>
        public int Status { get; set; }
        /// <summary>
        /// 当前页索引
        /// </summary>
        /// <value>The index of the page.</value>
        public int PageIndex { get; set; }
        /// <summary>
        /// 每页大小
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; set; }
    }
}
