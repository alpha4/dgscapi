﻿using System;
namespace Casamiel.API.Application.Models.Mall.Req
{
    /// <summary>
    /// Address save base req.
    /// </summary>
    public class AddressSaveBaseReq
    {
        /// <summary>
        /// 地址id
        /// </summary>
        public int ConsigneeId { get; set; }
        /// <summary>
        /// 是否默认
        /// </summary>
        public bool IsDefault { get; set; }
        /// <summary>
        /// 联系人姓名
        /// </summary>
        public string LinkName { get; set; }
        /// <summary>
        /// 电话 
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 详细地址
        /// </summary>
        public string FullAddress { get; set; }
        /// <summary>
        /// 省ID
        /// </summary>
        public int ProvinceId { get; set; }
        /// <summary>
        /// 市ID
        /// </summary>
        public int CityId { get; set; }
        /// <summary>
        /// 区ID
        /// </summary>
        public int DistrictId { get; set; }
        /// <summary>
        /// 邮编
        /// </summary>
        public string Postcode { get; set; }
    }
}
