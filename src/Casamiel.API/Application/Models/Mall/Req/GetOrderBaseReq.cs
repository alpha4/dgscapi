﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models.Mall.Req
{
    /// <summary>
    /// Get order base req.
    /// </summary>
    public class GetOrderBaseReq
    {
        /// <summary>
        /// Gets or sets the ordercode.
        /// </summary>
        /// <value>The ordercode.</value>
        [Required(ErrorMessage = "订单号不能空")]
        public string ordercode { get; set; }
    }
}
