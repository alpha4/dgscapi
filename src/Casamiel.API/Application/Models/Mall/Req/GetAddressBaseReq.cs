﻿using System;
namespace Casamiel.API.Application.Models.Mall.Req
{
    /// <summary>
    /// Get address base req.
    /// </summary>
    public class GetAddressBaseReq
    {
        /// <summary>
        /// Gets or sets the consignee identifier.
        /// </summary>
        /// <value>The consignee identifier.</value>
       public int consigneeId { get; set; }
    }
}
