﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Casamiel.API.Application.Models.Mall.Req
{
    /*
    /// <summary>
    /// Create order req.
    /// </summary>
   public class CreateOrderReq
    {
         
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 优惠券id
        /// </summary>
        public int DiscountCouponId { get; set; }

        /// <summary>
        /// 优惠券对应的卡号
        /// </summary>
        /// <value>The cardno.</value>
        public string Cardno { get; set; }
        /// <summary>
        /// 会员卡支付的卡号
        /// </summary>
        /// <value>The paycardno.</value>
        public string Paycardno {get; set;}
        /// <summary>
        /// 优惠券金额
        /// </summary>
        public decimal DiscountCouponMoney { get; set; }
        /// <summary>
        /// 支付方式 1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
        /// </summary>
        /// <value>The paymeothd.</value>
        [Required(ErrorMessage = "支付方式必填")]
        public int PayMethod { get; set; }

        /// <summary>
        /// 小程序专用Id
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }

        /// <summary>
        /// 地址Id
        /// </summary>
        public int ConsigneeId { get; set; }

        /// <summary>
        /// 商品信息
        /// </summary>
        public List<Goods> GoodsList { get; set; }
        /// <summary>
        /// Order goods.
        /// </summary>
        public class Goods
        {

            /// <summary>
            /// 商品id
            /// </summary>
            public int GoodsBaseId { get; set; }
            /// <summary>
            /// Gets or sets the price.
            /// </summary>
            /// <value>The price.</value>
            public decimal Price { get; set; }
            /// <summary>
            /// 商品关联Id
            /// </summary>
            public int GoodsRelationId { get; set; }
            /// <summary>
            /// 购买数量
            /// </summary>
            public int GoodsQuantity { get; set; }
        }
    }
    */
}
