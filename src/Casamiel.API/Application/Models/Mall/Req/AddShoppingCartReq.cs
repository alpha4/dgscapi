﻿using System;
namespace Casamiel.API.Application.Models.Mall.Req
{
    /// <summary>
    /// Add shopping cart req.
    /// </summary>
    public class AddShoppingCartReq
    {
        /// <summary>
        /// Gets or sets the goods base identifier.
        /// </summary>
        /// <value>The goods base identifier.</value>
        public int GoodsBaseId{get;set;}
        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        /// <value>The quantity.</value>
        public int Quantity { get; set;}
    }
}
