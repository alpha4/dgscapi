﻿using System;
using System.Collections.Generic;

namespace Casamiel.API.Application.Models.Mall.Req
{
    /// <summary>
    /// Shopping cart set is check req.
    /// </summary>
    public class ShoppingCartSetIsCheckReq
    {
		/// <summary>
		/// 购物车 商品id集合
		/// </summary>
#pragma warning disable CA2227 // 集合属性应为只读
		public List<int> GoodsIdList { get; set; }
#pragma warning restore CA2227 // 集合属性应为只读

		/// <summary>
		/// 是否选中
		/// </summary>
		public bool IsCheck { get; set; }

        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; set; }
    }
}
