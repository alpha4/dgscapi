﻿using System;
using System.Collections.Generic;

namespace Casamiel.API.Application.Models.Mall.Req
{
    /// <summary>
    /// Cart delete req.
    /// </summary>
    public class CartDeleteReq
    {
        /// <summary>
        /// Gets or sets the identifiers.
        /// </summary>
        /// <value>The identifiers.</value>
        public List<int> Ids;
    }
}
