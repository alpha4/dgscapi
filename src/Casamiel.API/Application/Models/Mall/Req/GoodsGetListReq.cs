﻿using System;
namespace Casamiel.API.Application.Models.Mall.Req
{
    /// <summary>
    /// Goods get list req.
    /// </summary>
    public class GoodsGetListReq
    {
        /// <summary>
        /// 标签id
        /// </summary>
        public int TagBaseId { get; set; }

        ///<summary>
        /// 当前页索引
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 每页大小
        /// </summary>
        public int PageSize { get; set; }
    }

}

