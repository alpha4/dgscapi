﻿using System;
namespace Casamiel.API.Application.Models.Mall.Req
{
    /// <summary>
    ///  
    /// </summary>
    public class CancelOrderReq
    {       
        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderCode { get; set; }

    }
}
