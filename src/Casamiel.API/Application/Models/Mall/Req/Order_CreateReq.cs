﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Casamiel.Domain.Request;

namespace Casamiel.API.Application.Models.Mall.Req
{
    /// <summary>
    /// 
    /// </summary>
    public class GetMyCouponsRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public List<OrderGoods> GoodsList;
    }


    /// <summary>
    /// 
    /// </summary>
    public class GetTicketRequest
    {
        /// <summary>
        /// 
        /// </summary>
        public List<OrderGoods> GoodsList;
    }

    

    /// <summary>
    /// Create order req.
    /// </summary>
    public class OrderCreateReq
    {
        /// <summary>
        /// 销售号
        /// </summary>
        public string BillNo { get; set; }
        /// <summary>
        /// ic交易号
        /// </summary>
        public string IcTradeNO { get; set; }
        /// <summary>
        /// 购买类型 1 购物车 2 直接购买
        /// </summary>
        public int BuyType { get; set; }
        /// <summary>
        /// 手机号 身份
        /// </summary>
        public string Mobile { get; set; }
        /// <summary> 
        /// 收货地址ID
        /// </summary> 
        public int ConsigneeId { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 商品信息
        /// </summary>
        public List<OrderGoods> GoodsList { get; set; }

        /// <summary>
        /// 运费金额 满88包邮
        /// </summary>
        public decimal FreightMoney { get; set; }

        /// <summary>
        /// 优惠券id
        /// </summary>
        public int DiscountCouponId { get; set; }
        /// <summary>
        /// 优惠券名称
        /// </summary>
        public string DiscountCouponName { get; set; }
        /// <summary>
        /// 卡号
        /// </summary>
        public string CardNo { get; set; }
        /// <summary>
        /// 优惠券金额
        /// </summary>
        public decimal DiscountCouponMoney { get; set; }

        /// <summary>
        /// 开票信息ID
        /// </summary>
        public int InvoiceId { get; set; }
        /// <summary>
        ///  支付用的会员卡好
        /// </summary>
        /// <value>The paycardno.</value>
        public string Paycardno { get; set; }
        /// <summary>
        /// 支付方式 1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
        /// </summary>
        /// <value>The paymeothd.</value>
        [Required(ErrorMessage = "支付方式必填")]
        public int payMethod { get; set; }

        /// <summary>
        /// 小程序专用Id
        /// </summary>
        /// <value>The identifier.</value>
        public int id { get; set; }

    }

   
}
