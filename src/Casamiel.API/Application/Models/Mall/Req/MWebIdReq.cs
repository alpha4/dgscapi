﻿using System;
namespace Casamiel.API.Application.Models.Mall.Req
{
    /// <summary>
    /// MW eb identifier req.
    /// </summary>
    public class MWebIdReq
    {
        /// <summary>
        /// 通用id
        /// </summary>
        public int Id { get; set; }
    }
}
