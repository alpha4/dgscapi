﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Check charge limit req.
    /// </summary>
    public class CheckChargeLimitReq
    {
        /// <summary>
        /// 卡号
        /// </summary>
        [Required]
        public string cardno { get; set; }

        /// <summary>
        /// 充值金额
        /// </summary>
        [Required]
        public decimal chargeAmount { get; set; }
    }
}
