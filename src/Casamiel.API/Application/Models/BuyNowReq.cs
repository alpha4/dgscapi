﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Application.Models
{
    /// <summary>
    /// Buy now dto.
    /// </summary>
    public class BuyNowReq
    {
        /// <summary>
        /// 门店id
        /// </summary>
        [Required(ErrorMessage = "门店id必填")]
        public int storeid { get; set; }
        /// <summary>
        /// 门店关联id
        /// </summary>
        [Required(ErrorMessage = "门店关联id必填")]
        public int storeRelationId { get; set; }
        /// <summary>
        /// 产品sku id
        /// </summary>
        [Required(ErrorMessage = "产品sku id必填")]
        public int goodsBaseId { get; set; }

        /// <summary>
        /// 产品sku关联Id
        /// </summary>
        [Required(ErrorMessage = "产品sku关联Id必填")]
        public int relationId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        [Required(ErrorMessage = "数量必填")]
        public int count { get; set; }

        /// <summary>
        /// Gets or sets the type of the buy.
        /// 0自提，1配送
        /// </summary>
        /// <value>The type of the buy.</value>
        public int buyType { get; set; }
        /// <summary>
        /// 自提时间
        /// </summary>
        [Required(ErrorMessage = "自提时间必填")]
        public DateTime picktime { get; set; }
        //{"storeid":1,"storeRelationId":1,"goodsBaseId": 1,"relationId": 141783,"count":1,"picktime":"2018-04-20 12:30"}
    }
}
