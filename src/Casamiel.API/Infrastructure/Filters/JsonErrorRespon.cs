﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Infrastructure.Filters
{
    /// <summary>
    /// Json error response.
    /// </summary>
    public class JsonErrorResponse
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        public int code { get; set; }
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string msg { get; set; }
        /// <summary>
        /// Gets or sets the developer message.
        /// </summary>
        /// <value>The developer message.</value>
        public object DeveloperMessage { get; set; }
    }
}
