﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Infrastructure.Middlewares
{
    /// <summary>
    /// 
    /// </summary>
    public class FailingOptions
    {
        /// <summary>
        /// 
        /// </summary>
        public string ConfigPath = "/Failing";
        /// <summary>
        /// /
        /// </summary>
        public List<string> EndpointPaths { get; set; } = new List<string>();
    }
}
