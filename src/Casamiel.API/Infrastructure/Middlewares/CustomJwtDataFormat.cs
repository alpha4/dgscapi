﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Casamiel.API.Infrastructure.Middlewares
{
    /// <summary>
    /// Custom jwt data format.
    /// </summary>
    public class CustomJwtDataFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private readonly string algorithm;
        private readonly TokenValidationParameters validationParameters;
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.API.Infrastructure.Middlewares.CustomJwtDataFormat"/> class.
        /// </summary>
        /// <param name="algorithm">Algorithm.</param>
        /// <param name="validationParameters">Validation parameters.</param>
        public CustomJwtDataFormat(string algorithm, TokenValidationParameters validationParameters)
        {
            this.algorithm = algorithm;
            this.validationParameters = validationParameters;
        }
        /// <summary>
        /// Unprotect the specified protectedText.
        /// </summary>
        /// <returns>The unprotect.</returns>
        /// <param name="protectedText">Protected text.</param>
        public AuthenticationTicket Unprotect(string protectedText)
            => Unprotect(protectedText, null);
        /// <summary>
        /// Unprotect the specified protectedText and purpose.
        /// </summary>
        /// <returns>The unprotect.</returns>
        /// <param name="protectedText">Protected text.</param>
        /// <param name="purpose">Purpose.</param>
        public AuthenticationTicket Unprotect(string protectedText, string purpose)
        {
            var handler = new JwtSecurityTokenHandler();
            ClaimsPrincipal principal = null;
            SecurityToken validToken = null;

            try
            {
                principal = handler.ValidateToken(protectedText, this.validationParameters, out validToken);

                var validJwt = validToken as JwtSecurityToken;

                if (validJwt == null)
                {
                    throw new ArgumentException("Invalid JWT");
                }

                if (!validJwt.Header.Alg.Equals(algorithm, StringComparison.Ordinal))
                {
                    throw new ArgumentException($"Algorithm must be '{algorithm}'");
                }

                // Additional custom validation of JWT claims here (if any)
            }
            catch (SecurityTokenValidationException)
            {
                return null;
            }
            catch (ArgumentException)
            {
                return null;
            }

            // Validation passed. Return a valid AuthenticationTicket:
            return new AuthenticationTicket(principal, new AuthenticationProperties(), "Cookie");
        }

        /// <summary>
        /// Protect the specified data.
        /// </summary>
        /// <returns>The protect.</returns>
        /// <param name="data">Data.</param>
        public string Protect(AuthenticationTicket data)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Protect the specified data and purpose.
        /// </summary>
        /// <returns>The protect.</returns>
        /// <param name="data">Data.</param>
        /// <param name="purpose">Purpose.</param>
        public string Protect(AuthenticationTicket data, string purpose)
        {
            throw new NotImplementedException();
        }
    }
}
