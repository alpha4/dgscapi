﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Infrastructure.Middlewares
{
    /// <summary>
    /// Failing startup filter.
    /// </summary>
    public class FailingStartupFilter:IStartupFilter
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly Action<FailingOptions> _action;
        /// <summary>
        /// Initializes a new instance of the
        /// </summary>
        /// <param name="action">Action.</param>
        public FailingStartupFilter(Action<FailingOptions> action)
        {
            _action = action;
        }
        /// <summary>
        /// Configure the specified next.
        /// </summary>
        /// <returns>The configure.</returns>
        /// <param name="next">Next.</param>
        public Action<IApplicationBuilder> Configure(Action<IApplicationBuilder> next)
        {
            return app =>
            {
                app.UseFailingMiddleware(_action);
                next(app);
            };
        }
    }
}
