﻿using Microsoft.IdentityModel.Tokens;
using System;

namespace Casamiel.API.Infrastructure.Middlewares
{
    /// <summary>
    /// JWTT oken options.
    /// </summary>
    public class JWTTokenOptions
    {
        /// <summary>
        /// Gets or sets the audience.
        /// </summary>
        /// <value>The audience.</value>
        public string Audience { get; set; }
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>The key.</value>
        public RsaSecurityKey Key { get; set; }
        /// <summary>
        /// Gets or sets the credentials.
        /// </summary>
        /// <value>The credentials.</value>
        public SigningCredentials Credentials { get; set; }
        /// <summary>
        /// Gets or sets the issuer.
        /// </summary>
        /// <value>The issuer.</value>
        public string Issuer { get; set; }
    }
    /// <summary>
    /// Token provider options.
    /// </summary>
    public class TokenProviderOptions
    {
        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        /// <value>The path.</value>
        public string Path { get; set; } = "/token";
        /// <summary>
        /// Gets or sets the issuer.
        /// </summary>
        /// <value>The issuer.</value>
        public string Issuer { get; set; }
        /// <summary>
        /// Gets or sets the audience.
        /// </summary>
        /// <value>The audience.</value>
        public string Audience { get; set; }
        /// <summary>
        /// Gets or sets the expiration.
        /// </summary>
        /// <value>The expiration.</value>
        public TimeSpan Expiration { get; set; } = TimeSpan.FromDays(30);
        /// <summary>
        /// Gets or sets the signing credentials.
        /// </summary>
        /// <value>The signing credentials.</value>
        public SigningCredentials SigningCredentials { get; set; }
    }
}