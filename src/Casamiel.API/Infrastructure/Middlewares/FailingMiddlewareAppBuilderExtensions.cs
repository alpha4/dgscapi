﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Infrastructure.Middlewares
{
    /// <summary>
    /// Failing middleware app builder extensions.
    /// </summary>
    public static class FailingMiddlewareAppBuilderExtensions
    {
        /// <summary>
        /// Uses the failing middleware.
        /// </summary>
        /// <returns>The failing middleware.</returns>
        /// <param name="builder">Builder.</param>
        public static IApplicationBuilder UseFailingMiddleware(this IApplicationBuilder builder)
        {
            return UseFailingMiddleware(builder, null);
        }

        /// <summary>
        /// Uses the failing middleware.
        /// </summary>
        /// <returns>The failing middleware.</returns>
        /// <param name="builder">Builder.</param>
        /// <param name="action">Action.</param>
        public static IApplicationBuilder UseFailingMiddleware(this IApplicationBuilder builder, Action<FailingOptions> action)
        {
            var options = new FailingOptions();
            action?.Invoke(options);
            builder.UseMiddleware<FailingMiddleware>(options);
            return builder;
        }
        /// <summary>
        /// Uses the response time.
        /// </summary>
        /// <returns>The response time.</returns>
        /// <param name="builder">Builder.</param>
        public static IApplicationBuilder UseResponseTime(this IApplicationBuilder builder){
            return builder.UseMiddleware<ResponseTimeMiddleware>();
        }
    }
}
