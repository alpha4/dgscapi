﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
//using Casamiel.API.Application.Interface;
using Casamiel.API.Application.Models;
using Casamiel.API.Application.Services;
using Casamiel.Domain.AggregatesModel.V2;
using Casamiel.Infrastructure.Repositories.V2;

namespace Casamiel.API.Infrastructure.AutofaceModules.V2
{
    /// <summary>
    /// 
    /// </summary>
    public class ApplicationModule : Autofac.Module
    {
        /// <summary>
        /// 
        /// </summary>
        public string QueriesConnectionString { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="qconstr"></param>
        public ApplicationModule(string qconstr)
        {
            QueriesConnectionString = qconstr;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterType<MobilCheckCodeService>()
            //    .As<IMobileCheckCode>()
            //    .InstancePerLifetimeScope();
            //builder.Register(c => new ErrorLogRepository(QueriesConnectionString))
            //   .As<IErrorLog>()
            //   .InstancePerLifetimeScope();
            //builder.Register(c => new UserLogRepository(QueriesConnectionString))
            //   .As<IUserLog>()
            //   .InstancePerLifetimeScope();
            //builder.RegisterType<MemberRepository>()
            //  .As<IMemberRepository>()
            //  .InstancePerLifetimeScope();

            //builder.Register(c => new PaymentRecordRepository(QueriesConnectionString))
            // .As<IPaymentRecordRepository>()
            // .InstancePerLifetimeScope();
            builder.Register(c=> new MemberCardRepository(QueriesConnectionString))
            .As<IMemberCardRepository>()
            .InstancePerLifetimeScope();

            builder.Register(c => new MemberRepository(QueriesConnectionString)).As<IMemberRespository>().SingleInstance();
            builder.Register(c => new MemberAccessTokenRepository(QueriesConnectionString)).As<IMemberAccessTokenRespository>().InstancePerLifetimeScope();
            builder.RegisterType<CommService>()
              .As<Application.Interface.ICommonService>()
              .InstancePerLifetimeScope();
            //builder.Register(c => new PaymentRecordService(QueriesConnectionString))
            //   .As<IPaymentRecord>()
            //   .SingleInstance();
        }
    }
}
