﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.API.Infrastructure
{
    /// <summary>
    /// Member helper.
    /// </summary>
    public static class MemberHelper
    {
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <returns>The mobile.</returns>
        /// <param name="token">Token.</param>
        public static string GetMobile(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            JwtSecurityToken dtoken = handler.ReadJwtToken(token);
            var rsa = new RSAHelper(RSAType.RSA2, Encoding.UTF8, RSAHelper.privateKey, RSAHelper.publicKey);

            var exp = dtoken.Payload.Exp;
            string Jti = rsa.Decrypt(dtoken.Payload.Jti);
            return Jti;
        }

 
    }
}
