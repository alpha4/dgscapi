﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Infrastructure
{
    /// <summary>
    /// Constant.
    /// </summary>
    public static class Constant
    {
        /// <summary>
        /// token缓存键
        /// </summary>
        public const string TOKEN_PREFIX = "t_";
        /// <summary>
        /// 我的会员卡
        /// </summary>
        public const string MYCARDS_PREFIX = "clist_";
        /// <summary>
        /// 消费码
        /// </summary>
        public const string CONSUMECODE = "consumecode_";

		/// <summary>
		/// INDEXGOODSLIST
		/// </summary>
		public const string INDEXGOODSLIST = "indexgoodslist_";
		/// <summary>
		/// SLIDELIST
		/// </summary>
		public const string SLIDELIST = "slidelist_";
        /// <summary>
        /// The drinksgoodsid.
        /// </summary>
        public const string DRINKSGOODSID = "drinksgoodsid";
	}
}
