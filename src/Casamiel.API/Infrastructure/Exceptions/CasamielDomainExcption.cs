﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.Infrastructure.Exceptions
{
    /// <summary>
    /// 
    /// </summary>
    public class CasamielDomainException : Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public CasamielDomainException()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public CasamielDomainException(string message) : base(message)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public CasamielDomainException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
