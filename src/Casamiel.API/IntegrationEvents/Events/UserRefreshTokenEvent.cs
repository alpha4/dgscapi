﻿using Casamiel.EventBus.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API.IntegrationEvents.Events
{
    public class UserRefreshTokenEvent: IntegrationEvent
    {
        public string Mobile { get; private set; }
        public string Oldtoken { get; private set; }
        public string Newtoken { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="oldtoken"></param>
        /// <param name="newtoken"></param>
        public UserRefreshTokenEvent(string mobile,string oldtoken,string newtoken)
        {
            Mobile = mobile;
            Oldtoken = oldtoken;
            Newtoken = newtoken;
        }
    }
}
