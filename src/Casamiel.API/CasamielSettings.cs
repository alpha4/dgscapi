﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.API
{
    /// <summary>
    /// 微信小程序设置
    /// </summary>
    public class MiniAppSettings
    {
        /// <summary>
        /// 小程序appid
        /// </summary>
        public string appid { get; set; }
        /// <summary>
        /// 小程序secret
        /// </summary>
        public string secret { get; set; }
        /// <summary>
        /// 微信支付商户id
        /// </summary>
        public string MchId { get; set; }
        /// <summary>
        /// 微信支付key
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// 微信支付AppSecret
        /// </summary>
        public string AppSecret { get; set; }

        /// <summary>
        /// 微信支付 SslCertPassword
        /// </summary>
        public string SslCertPassword { get; set; }
        /// <summary>
        /// 微信支付SslCertPath
        /// </summary>
        public string SslCertPath { get; set; }
        /// <summary>
        /// 小程序支付通知
        /// </summary>
        public string NotifyUrl { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class IcServiceSettings
    {
        /// <summary>
        /// Gets or sets the app key.
        /// </summary>
        /// <value>The app key.</value>
        public string appKey { get; set; }
        /// <summary>
        /// Gets or sets the sign key.
        /// </summary>
        /// <value>The sign key.</value>
        public string signKey { get; set; }
        /// <summary>
        /// Gets or sets the token.
        /// </summary>
        /// <value>The token.</value>
        public string token { get; set; }
        /// <summary>
        /// Gets or sets the app URL.
        /// </summary>
        /// <value>The app URL.</value>
        public string appUrl { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class AliyunSettings
    {
        /// <summary>
        /// 
        /// </summary>
        public string accessKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string accessKeySecret { get; set; }
        /// <summary>
        /// Gets or sets the yzm template code.
        /// </summary>
        /// <value>The yzm template code.</value>
        public string YzmTemplateCode { get; set; }
        /// <summary>
        /// Gets or sets the name of the sms sign.
        /// </summary>
        /// <value>The name of the sms sign.</value>
        public string SmsSignName { get; set; }
    }
    /// <summary>
    ///微信支付-app支付
    /// </summary>
    public class WxPayAppSettings
    {
        /// <summary>
        /// Gets or sets the app identifier.
        /// </summary>
        /// <value>The app identifier.</value>
        public string AppId { get; set; }
        /// <summary>
        /// Gets or sets the mch identifier.
        /// </summary>
        /// <value>The mch identifier.</value>
        public string MchId { get; set; }
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>The key.</value>
        public string Key { get; set; }
        /// <summary>
        /// Gets or sets the app secret.
        /// </summary>
        /// <value>The app secret.</value>
        public string AppSecret { get; set; }
        /// <summary>
        /// Gets or sets the ssl cert path.
        /// </summary>
        /// <value>The ssl cert path.</value>
        public string SslCertPath { get; set; }
        /// <summary>
        /// Gets or sets the ssl cert password.
        /// </summary>
        /// <value>The ssl cert password.</value>
        public string SslCertPassword { get; set; }
        /// <summary>
        /// Gets or sets the notify URL.
        /// </summary>
        /// <value>The notify URL.</value>
        public string NotifyUrl { get; set; }
    }
    /// <summary>
    /// 微信支付-默认
    /// </summary>
    public class WxPaySettigs
    {
        /// <summary>
        /// Gets or sets the app identifier.
        /// </summary>
        /// <value>The app identifier.</value>
        public string AppId { get; set; }
        /// <summary>
        /// Gets or sets the mch identifier.
        /// </summary>
        /// <value>The mch identifier.</value>
        public string MchId { get; set; }
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>The key.</value>
        public string Key { get; set; }
        /// <summary>
        /// Gets or sets the app secret.
        /// </summary>
        /// <value>The app secret.</value>
        public string AppSecret { get; set; }
        /// <summary>
        /// Gets or sets the ssl cert path.
        /// </summary>
        /// <value>The ssl cert path.</value>
        public string SslCertPath { get; set; }
        /// <summary>
        /// Gets or sets the ssl cert password.
        /// </summary>
        /// <value>The ssl cert password.</value>
        public string SslCertPassword { get; set; }
        /// <summary>
        /// Gets or sets the notify URL.
        /// </summary>
        /// <value>The notify URL.</value>
        public string NotifyUrl { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class CasamielSettings
    {
        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>The connection string.</value>
        public string ConnectionString { get; set; }
        /// <summary>
        /// Gets or sets the identity URL.
        /// </summary>
        /// <value>The identity URL.</value>
        public string IdentityUrl { get; set; }
		/// <summary>
		/// 门店接口url
		/// </summary>
		public string StoreApiUrl { get; set; }
		/// <summary>
		/// 是否测试
		/// </summary>
		public bool IsTest { get; set; }
		/// <summary>
		/// 是否检测推送
		/// </summary>
		public bool CheckPushStock { get; set; }
		/// <summary>
		/// 启用缓存
		/// </summary>
		public bool CacheEnable { get; set; }
        /// <summary>
        /// 缓存前缀
        /// </summary>
        /// <value>The memcached pre.</value>
        public string MemcachedPre { get; set; }
        /// <summary>
        /// 接口名
        /// </summary>
        public string ApiName { get; set; }
		/// <summary>
		/// 接口token
		/// </summary>
		public string ApiToken { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Subject { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string wxpayUrl { get; set; }
        //public string alipayReturnUrl { get; set; }
        //public string alipayNotifyUrl { get; set; }
        ///// <summary>
        ///// 微信支付通知
        ///// </summary>
        // public string wxpayNotifyUrl { get; set; }
        ///// <summary>
        ///// app微信支付通知
        ///// </summary>
        //public string AppWxpayNotify { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string wxpayAppid { get; set; }
        /// <summary>
        /// 微信支付密钥
        /// </summary>
        public string wxpayAppSecret { get; set; }
        /// <summary>
        ///微信支付key
        /// </summary>
        public string wxpayKey { get; set; }
    }
}
