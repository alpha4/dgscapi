﻿using System;

namespace Casamiel.API
{
    /// <summary>
    /// 
    /// </summary>
    public class TokenExpireException : Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public string TokenExpireMsg { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public TokenExpireException() : this("登录超时啦")
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenExpireMsg"></param>
        public TokenExpireException(string tokenExpireMsg)
        {
            this.TokenExpireMsg = tokenExpireMsg;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public TokenExpireException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}