﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.API.Application;
using Casamiel.API.Infrastructure.Filters;
using Casamiel.Domain.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Casamiel.Domain.Request;
using Casamiel.API.Infrastructure;
using Casamiel.Application.Commands;
using Casamiel.Domain;
using Newtonsoft.Json;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V1
{
    /// <summary>
    /// 券接口
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    [ApiController]
     
    [EnableCors("any")]
    
    public class CouponController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly IMobileCheckCode _mobileCheckCode;
        private readonly NLog.ILogger _BizInfologger = NLog.LogManager.GetLogger("BizInfo");
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobileCheckCode"></param>
        /// <param name="mediator"></param>
        public CouponController(IMobileCheckCode mobileCheckCode, IMediator mediator)
        {
            _mobileCheckCode = mobileCheckCode;
            _mediator = mediator;
        }


        /// <summary>
        /// 绑定券
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        //[TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<BaseResult<string>> BindCoupon(BindCouponRequest request, [FromHeader(Name = "u-token")] string token)
        {
            if (request is null) {
                throw new ArgumentNullException(nameof(request));
            }

            var mobile =  MemberHelper.GetMobile(token);
            //var sends = await _mobileCheckCode.CheckSmsCodeAsync(mobile, request.Yzm, false).ConfigureAwait(false);
            //if (sends["code"].ToString() != "0") {

            //    return new BaseResult<string>("", -2, sends["msg"].ToString());
            //}

            var cmd = new BindCouponCommand(mobile, request.Ticketcode, LoginInfo.Item3);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }

        /// <summary>
        /// 券转赠
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]

        public async Task<BaseResult<MemberCouponGive>> CouponForward(CouponForwardRequest request,[FromHeader(Name = "u-token")] string token)
        {
            if (request is null) {
                throw new ArgumentNullException(nameof(request));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new CouponForwardCommand(mobile, request.MCID,request.Message, LoginInfo.Item3);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }

       
        /// <summary>
        ///  赠券详情
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<CouponGiveReponse>> GetCouponGiveInfo(GetCouponGiveRequest request)
        {
            if (request is null) {
                throw new ArgumentNullException(nameof(request));
            }
            var cmd = new GetCouponGiveDetailCommand(request.ForwardCode, LoginInfo.Item3);
            return await _mediator.Send(cmd).ConfigureAwait(false);
        }
      


        /// <summary>
        /// 券码
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<BaseResult<string>>GetTicketCodeById(GetTicketCodeByIdRequest request,[FromHeader(Name = "u-token")] string token)
        {
            if (request is null) {
                throw new ArgumentNullException(nameof(request));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetTicketCodeByIdCommand(mobile, request.McID, LoginInfo.Item3);
            return await _mediator.Send(cmd).ConfigureAwait(false);
        }
        /// <summary>
        /// 我的券列表
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<PagingResultRsp<List<MemberCouponReponse>>>GetMyCouponList(GetMyCouponListRequest request,[FromHeader(Name = "u-token")] string token)
        {
            if (request is null) {
                throw new ArgumentNullException(nameof(request));
            }
            
            var mobile =  MemberHelper.GetMobile(token);//"13857175708";//
            var cmd = new GetMyCouponListCommand( LoginInfo.Item3) { Mobile = mobile, PageIndex = request.PageIndex, PageSize = request.PageSize, State = request.State };
            var rsp= await _mediator.Send(cmd).ConfigureAwait(false);
            _BizInfologger.Trace($"GetMyCouponList:{JsonConvert.SerializeObject(rsp)}");
            return rsp;
        }

        /// <summary>
        /// 领取券
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> GetCoupon(GetCouponRequest request)
        {
            if (request is null) {
                throw new ArgumentNullException(nameof(request));
            }
            var sends = await _mobileCheckCode.CheckSmsCodeAsync(request.Mobile, request.Yzm, false).ConfigureAwait(false);
            if (sends["code"].ToString() != "0") {

                return new BaseResult<string>("", -2, sends["msg"].ToString());
            }
            var cmd = new GetCouponCommand(request.Mobile,request.Yzm,request.ForwardCode, LoginInfo.Item3);
            return await _mediator.Send(cmd).ConfigureAwait(false);
        }
    }
}
