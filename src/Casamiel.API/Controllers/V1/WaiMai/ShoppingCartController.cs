﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.API.Infrastructure;
using Casamiel.Application.Commands.Waimai;
using Casamiel.Domain.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Casamiel.Domain.Request;
using System.Net;
using Casamiel.API.Application.Models.Mall.Req;

using Casamiel.Domain.Request.Waimai;
using Casamiel.Domain.Response.Waimai;
using Microsoft.AspNetCore.Authorization;
using Casamiel.API.Infrastructure.Filters;
using Microsoft.AspNetCore.Cors;

namespace Casamiel.API.Controllers.V1.WaiMai
{
  
        /// <summary>
        /// 
        /// </summary>

    [ApiVersion("1.0")]
    [Route("api/v{api-version:apiVersion}/WaiMai/[controller]")]
    [ApiController]
    [Authorize]
    [TypeFilterAttribute(typeof(CheckTokenAttribute))]
    [EnableCors("any")]
    public class ShoppingCartController : BaseController
    {
        private readonly IMediator _mediator; 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediator"></param>
        public ShoppingCartController(IMediator mediator)
        {
            _mediator = mediator;
        }
        /// <summary>
        ///  添加到购物车---外卖
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> Add(ShoppingCardAddBaseReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile =  MemberHelper.GetMobile(token);
            var cmd = new ShoppingCardAddBaseCommand { GoodsBaseId = req.GoodsBaseId, Mobile = mobile, Quantity = req.Quantity,StoreId=req.StoreId };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 清空购物车
        /// </summary>
        /// <param name="req"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> Clean(ShoppingCartReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new CleanShoppingCartCommand(LoginInfo.Item3) {  StoreId = req.StoreId,Mobile=mobile };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 删除购物内一款产品
        /// </summary>
        /// <param name="req"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>>Remove(ShoppingCardAddBaseReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            
            var cmd = new CleanShoppingCardByIdsCommand(new List<int> { req.GoodsBaseId },LoginInfo.Item3 ) { Mobile = mobile, StoreId = req.StoreId };

            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 移除没库存的产品
        /// </summary>
        /// <param name="req"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> RemoveNoStockItem(ShoppingCartReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new ShoppingCardCleanNoStockCommand(LoginInfo.Item3) { Mobile = mobile, StoreId = req.StoreId};
          
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 我的购物车
        /// </summary>
        /// <param name="req"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<List<TakeOutShoppingCartBaseRsp>>> GetMyShoppingCart(ShoppingCartReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetMyShoppingCartCommand(LoginInfo.Item3) { StoreId = req.StoreId, Mobile = mobile};
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 修改购物车商品数量----外卖
        /// </summary>
        /// <param name="req"></param>
        /// <param name="token"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> ChangeQuantity([FromBody] ShoppingCartChangeQuantityReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new ShoppingCartChangeQuantityCommand(LoginInfo.Item3) { Quantity=req.Quantity, GoodsBaseId=req.GoodsBaseId, StoreId = req.StoreId, Mobile = mobile };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
    }
}