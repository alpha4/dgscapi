﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.API.Application.Models.V2;
using Casamiel.Application.Commands.Waimai;
using Casamiel.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;

namespace Casamiel.API.Controllers.V1.WaiMai
{
    /// <summary>
    /// waimai产品
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{api-version:apiVersion}/WaiMai/[controller]")]
    [ApiController]
    [Authorize]
    [EnableCors("any")]

    public class ProductController : BaseController
    {
        private readonly IMediator _mediator;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediator"></param>
        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        ///外卖 根据商品Id和门店Id获取商品信息
        /// </summary>
        /// <returns>The product by identifier store identifier.</returns>
        /// <param name="req">Req.</param>
        [HttpPost]
        [Route("[Action]")]
        public async Task<ActionResult<Casamiel.Domain.Response.BaseResult<GoodsProductBaseRsp>>> GetDetail([FromBody] GetProductReq req)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var loginInfo = this.LoginInfo;
            var cmd = new GetProductByIdStoreIdCommand(req.Productid, req.Storeid,"v3",loginInfo.Item3)
            {
                Source = loginInfo.Item2,
                RequestUrl = Request.GetShortUri(),
                UserIp = Request.GetUserIp()
            };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 外卖 分类下产品列表
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[Action]")]
        public async Task<PagingResultRsp<List<ProductBaseRsp>>> GetProductList([FromBody]GetProductListReq data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }
            var cmd = new GetProductListByTagIdCommand { PageIndex = data.Pageindex, PageSize = data.Pagesize, TagBaseId = data.Tagid, StoreId = data.StoreId };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 获取外卖标签列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("[Action]")]
        public async Task<BaseResult<List<IndexTagBaseRsp>>> GetTagsList()
        {
            var cmd = new GetTagsListCommand();
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }



    }
}
