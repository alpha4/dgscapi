﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.API.Application.Models.Store;
using Casamiel.Application;
using Casamiel.Common;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NLog;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V1.WaiMai
{
    /// <summary>
    /// Store controller.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{api-version:apiVersion}/WaiMai/[controller]")]
    [ApiController]
    [Authorize]
    [EnableCors("any")]
    public class StoreController : Controller
    {
        private readonly IStoreService _storeService;
        private readonly NLog.ILogger logger = LogManager.GetLogger("OrderService");
        private readonly IMediator _mediator;
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Controllers.V1.WaiMai.StoreController"/> class.
        /// </summary>
        /// <param name="storeService">Store service.</param>
        /// <param name="mediator">Mediator.</param>
        public StoreController(IStoreService storeService,IMediator mediator)
        {
            _storeService = storeService;
            _mediator = mediator;
        }
        /// <summary>
        /// 外卖门店列表
        /// </summary>
        /// <returns>The all stores by store name.</returns>
        /// <param name="data">Data.</param>
        /// <remarks>
        /// {"storeName":"","pageIndex":"1","pageSize":"10","lat":30,"lng":12.02222}
        /// </remarks>
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> GetAllStoresByStoreName([FromBody] GetAllStoresReq data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            logger.Trace($"GetAllStoresByStoreName:{JsonConvert.SerializeObject(data)}");

            var lat = data.Lat;
            var lng = data.Lng;

            var newlist = await _storeService.GetAllStoreByNameAsync<ICasaMielSession>(data.StoreName).ConfigureAwait(false);


            var list= newlist.Where(c=>c.IsWaimaiClose==0).ToList();
            list = list.Where(c => c.IsWaimaiShopClose == 0).ToList();
            var _count = list.Count;
            for (int i = 0; i < _count; i++)
            {
                list[i].Distance = DistanceHelper.GetDistance(lat, lng, list[i].Latitude, list[i].Longitude);// _commonService.MapService.GetGistance(a, new Map { lat=item.Latitude.ToDouble(0), lng=item.Longitude.ToDouble(0)});
            }

            if (data.PageIndex <= 0)
            {
                data.PageIndex = 1;
            }
            if (data.PageSize <= 0)
            {
                data.PageSize = 10;
            }
            var entity = list.OrderBy(p => p.Distance).ToList().Skip((data.PageIndex - 1) * data.PageSize).Take(data.PageSize);
            //var query = entity.Skip((pageindex-1) * pagesize).Take(pagesize);
            var result = new { code = 0, pageindex = data.PageIndex, pagesize = data.PageSize, total = list.Count, content = entity };
            return Ok(result);
        }
    }
}
