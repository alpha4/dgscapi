﻿using Casamiel.API.Infrastructure;
using Casamiel.API.Infrastructure.Filters;
using Casamiel.Application.Commands.Waimai;
using Casamiel.Domain.Request;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Casamiel.API.Controllers.V1.WaiMai
{
    /// <summary>
    /// waimai收货地址
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{api-version:apiVersion}/WaiMai/[controller]")]
    [ApiController]
    [Authorize]
    [TypeFilterAttribute(typeof(CheckTokenAttribute))]
    [EnableCors("any")]
    public class ShippingAddressController : BaseController
    {
        private readonly IMediator _mediator;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediator"></param>
        public ShippingAddressController(IMediator mediator)
        {
            _mediator = mediator;
        }
        /// <summary>
        /// 获取外卖收货地址列表
        /// </summary>
        /// <param name="req"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<BaseResult<List<CakeAddressBaseRsp>>> GetMyAddressList([FromBody] GetListByStoreIdReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetAddressListByStoreIdCommand(req.StoreId, mobile,LoginInfo.Item3);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 保存外卖带有坐标
        /// </summary>
        /// <param name="req"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<CakeAddressBaseRsp>> Save([FromBody]CakeAddressSaveBaseReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new SaveAddressCommand()
            {
                CityId = req.CityId,
                RealName = req.RealName,
                ConsigneeId = req.ConsigneeId,
                Mobile = mobile,
                DistrictId = req.DistrictId,
                FullAddress = req.FullAddress,
                IsDefault = req.IsDefault,
                Latitude = req.Latitude,
                Longitude = req.Longitude,
                StoreId = req.StoreId,
                Sort = req.Sort,
                Postcode = req.Postcode,
                Phone = req.Phone,
                ProvinceId = req.ProvinceId
            };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        
        /// <summary>
        /// 设置默认地址
        /// </summary>
        /// <returns>The default.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> SetDefault([FromBody]BaseAddressReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new Casamiel.Application.Commands.AddressSetDefaultCommand(req.ConsigneeId, mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 删除蛋糕商城地址
        /// </summary>
        /// <returns>The delete.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> Delete([FromBody]BaseAddressReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new Casamiel.Application.Commands.DeleteAddressCommand(req.ConsigneeId, mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 联系信息
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<AddressLinkInfoRsp>> GetLinkInfo([FromHeader(Name = "u-token")] string token)
        {
            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetLinkInfoCommand(LoginInfo.Item3) {  Mobile=mobile };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

    }
}
