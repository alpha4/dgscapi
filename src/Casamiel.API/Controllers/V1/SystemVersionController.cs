﻿using System;
using System.Threading.Tasks;
using Casamiel.API.Application;
using Casamiel.API.Application.Models;
using Casamiel.Domain.Response;
using Microsoft.AspNetCore.Mvc;

namespace Casamiel.API.Controllers.V1
{

    /// <summary>
    /// 系统版本号
    /// </summary>
    [Produces("application/json")]
    [Route("api/SystemVersion")]
    //[Authorize]
    [ApiVersionNeutral]
    [ApiController]
    public class SystemVersionController : Controller
    {
        private readonly IStoreApiService _storeApiService;
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Controllers.V1.SystemVersionController"/> class.
        /// </summary>
        /// <param name="storeApiService">Store API service.</param>
        public SystemVersionController(IStoreApiService storeApiService)
        {
            _storeApiService = storeApiService;
        }
        /// <summary>
        /// Gets the app version.
        /// </summary>
        /// <returns>The app version.</returns>
        /// <param name="data">Data.</param>
        /// <remarks>{"code":0,"content": {"VersionId": 1,"AppType": 1,"Description": "测试","Version": "1.0.0","AppUrl": "http://store.casamiel.cn/","AppTypeName": "安卓后台"},"msg ":""}</remarks>
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> GetAppVersion([FromBody] VersionReq data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var info = await _storeApiService.GetAppVersionAsync(data.type).ConfigureAwait(false);
            return Ok(info);
        }

        /// <summary>
        ///UnixDate
        /// </summary>
        /// <returns>The sytem date time.</returns>
        [HttpPost]
        [Route("[action]")]
        public ActionResult<BaseResult<long>> GetSytemDateTimeTicks()
        {
            long UnixDate = (DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000000;
            return new BaseResult<long>(UnixDate, 0, "");
        }



        /// <summary>
        /// 获取配置信息
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<CommonConfigRsp>> GetConfigByKey(GetConfigByKeyReq req)
        {
            return await _storeApiService.GetConfigByKeyAsync(req.ConfigKey).ConfigureAwait(false);
        }
    }
}