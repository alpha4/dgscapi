﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.Application;
using Casamiel.Domain.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V1
{
    /// <summary>
    /// System base controller.
    /// </summary>
    [Produces("application/json")]
    //[Route("api/V1/[controller]")]
    //[Route("api/v{api-version:apiVersion}/[controller]")]
    //[Authorize]
	[ApiVersion("1.0")]
	[Route("api/v{api-version:apiVersion}/[controller]")]
    [ApiController]
    public class SystemBaseController : Controller
    {
        //private readonly IHintInformation _hintInformation;
        private readonly ITempProductsRepository _tempProducts;
       
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Controllers.V1.SystemBaseController"/> class.
        /// </summary>
        /// <param name="tempProducts">Temp products.</param>
        public SystemBaseController(
			ITempProductsRepository tempProducts)
        {
            //_hintInformation = hint;
            _tempProducts = tempProducts;
        }

        /// <summary>
        /// Gets the products.
        /// </summary>
        /// <returns>The products.</returns>
        /// <param name="GroupId">Group identifier.</param>
        [HttpGet]
        [HttpPost]
        [Route("[Action]")]
        public  async Task<IActionResult>GetProducts(int GroupId)
        {
            var list = await _tempProducts.GetListAsync<ICasaMielSession>(GroupId).ConfigureAwait(false);
            //foreach (var item in list)
            //{
            //    if (item.ProductDes!=null && item.ProductDes.Length> 0)
            //    {
            //        if (!item.ProductDes.StartsWith("http://"))
            //        {
            //            item.ProductDes = "http://www.casamiel.cn/images" + item.ProductDes;
            //        }
            //    }
            //    if (!item.ProductImg.StartsWith("http://"))
            //    {
            //        item.ProductImg = "http://www.casamiel.cn/images" + item.ProductImg;
            //    }
            //}
            return new JsonResult(list);
        }
    }
}
