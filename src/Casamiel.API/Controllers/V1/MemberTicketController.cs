﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.API.Application;
using Casamiel.API.Infrastructure;
using Casamiel.API.Infrastructure.Filters;
using Casamiel.Application.Commands.MemberTicket;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Request;
using Casamiel.Domain.Request.IC;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.IC;
using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V1
{
    /// <summary>
    /// 
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    [ApiController]

    [EnableCors("any")]
    public class MemberTicketController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly IMobileCheckCode _mobileCheckCode;
        private readonly NLog.ILogger _BizInfologger = NLog.LogManager.GetLogger("BizInfo");

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobileCheckCode"></param>
        /// <param name="mediator"></param>
        public MemberTicketController(IMobileCheckCode mobileCheckCode, IMediator mediator)
        {
            _mobileCheckCode = mobileCheckCode;
            _mediator = mediator;
        }



        /// <summary>
        ///会员券转赠
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]

        public async Task<BaseResult<MemberTicketGive>> TicketForward(TicketForwardRequest request, [FromHeader(Name = "u-token")] string token)
        {
            if (request is null) {
                throw new ArgumentNullException(nameof(request));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new TicketForwardCommand(mobile, request.Ticketid,request.TicketName,request.Je, request.Message,LoginInfo.Item3);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }
        /// <summary>
        ///  赠券详情
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<TicketGiveReponse>> GetCouponGiveInfo(GetCouponGiveRequest request)
        {
            if (request is null) {
                throw new ArgumentNullException(nameof(request));
            }
            var cmd = new GetMemberTicketGiveDetailCommand(request.ForwardCode,LoginInfo.Item3);
            return await _mediator.Send(cmd).ConfigureAwait(false);
        }

        /// <summary>
        /// 领取券
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> GetTicket(GetCouponRequest request)
        {
            if (request is null) {
                throw new ArgumentNullException(nameof(request));
            }
            var sends = await _mobileCheckCode.CheckSmsCodeAsync(request.Mobile, request.Yzm, false).ConfigureAwait(false);
            if (sends["code"].ToString() != "0") {

                return new BaseResult<string>("", -2, sends["msg"].ToString());
            }
            var cmd = new GetTicketCommand(request.Mobile, request.Yzm, request.ForwardCode,LoginInfo.Item3);
            return await _mediator.Send(cmd).ConfigureAwait(false);
        }
    }
}
