﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.GroupPurchase;
using Casamiel.Domain.Request.GroupPurchase;
using Casamiel.Application.Commands.GroupPurchase;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V1.GroupPurchase
{
    /// <summary>
    /// 
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{api-version:apiVersion}/GroupPurchase/[controller]")]
    [ApiController]
    [Authorize]
    [EnableCors("any")]
    public class ActivityController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IMediator _mediator;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediator"></param>
        public ActivityController(IMediator mediator)
        {
            _mediator = mediator; 
        }


        /// <summary>
        ///
        /// 拼团活动列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<List<GroupActivityResponse>>> GetList(GroupActivityListRequest req)
        {
            var cmd = new GetGroupActivityListCommand(req.Platform, req.Source);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }

        /// <summary>
        /// 拼团活动详情
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<GrouponActivityDetailResponse>> GetDetail(GetGroupActivityDetailRequest req)
        {
            var cmd = new GetDetailCommand(req.GrouponId, LoginInfo.Item3);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }
    }
}
