﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Request;
using Casamiel.Domain.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MediatR;
using Casamiel.Application.Commands.MeituanDelivery;

namespace Casamiel.API.Controllers.V1
{
    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [ApiVersion("1.0")]
    [Route("api/v{api-version:apiVersion}/[controller]")]

    [ApiController]
    public class DeliveryController : ControllerBase
    {
        private readonly string _apitoken;
        private readonly IMediator _mediator;
      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="snapshot"></param>
        /// <param name="mediator"></param>
        public DeliveryController(IOptionsSnapshot<CasamielSettings> snapshot, IMediator mediator)
        {
            _apitoken = snapshot.Value.ApiToken;
            _mediator = mediator;
        }


        /// <summary>
        /// 美团配送订单状态变更
        /// </summary>
        /// <param name="req"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[Action]")]
        public async Task<BaseResult<string>> ChangeMeituanOrderStatus([FromBody] CallbackOrderStatusReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (token != _apitoken)
            {
                return new BaseResult<string>(null, 99999, "token无效");
            }
            var cmd = new ChangeMeituanOrderStatusCommand(req);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }

        /// <summary>
        /// 推送美团配送异常
        /// </summary>
        /// <param name="req"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[Action]")]
        public async Task<BaseResult<string>> MeituanUnusualPush([FromBody] OrderUnusualCallbackReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (token != _apitoken)
            {
                return new BaseResult<string>(null, 99999, "token无效");
            }
            var cmd = new MeituanUnusualPushCommand (req);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }
    }
}