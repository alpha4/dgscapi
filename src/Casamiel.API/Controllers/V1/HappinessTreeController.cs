﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.API.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.HappinessTree;
using Casamiel.API.Infrastructure;
using Casamiel.Application.Commands.HappinessTree;
using Casamiel.Domain.Request.HappinessTree;
using Casamiel.Domain.Request.HappinessTress;
using Casamiel.Domain.Request;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V1
{
    /// <summary>
    /// Happiness tree controller.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize]
    [TypeFilterAttribute(typeof(CheckTokenAttribute))]
    public class HappinessTreeController : BaseController
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediator">Mediator.</param>
        public HappinessTreeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 活动报名
        /// </summary>
        /// <returns>The sign up.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> ActivitySignUp(HappinessTreeSignUpRequest req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null) {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new ActivitySignUpCommand {
                Mobile = mobile,
                BabyAge = req.BabyAge,
                FatherName = req.FatherName,
                MotherName = req.MotherName,
                Phone = req.Phone,
                StoreName = req.StoreName
            };
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }


        /// <summary>
        /// 我的奖励
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<List<RewardResponse>>> MyRewards([FromHeader(Name = "u-token")] string token)
        {
            var mobile = MemberHelper.GetMobile(token);
            var cmd = new MyRewardsCommand(mobile, LoginInfo.Item3);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }



        /// <summary>
        /// 领取奖励
        /// </summary>
        /// <param name="req"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> GetRewards(GetRewardsRequest req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null) {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetRewardsCommand(mobile, req.RewardId);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }


        /// <summary>
        /// Adds the name of the nick.
        /// </summary>
        /// <returns>The nick name.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> AddNickName(AddNickNameRequest req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null) {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new AddNickNameCommand(mobile, req.NickName, LoginInfo.Item3);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }

        /// <summary>
        /// 升级
        /// </summary>
        /// <returns>The upgrade.</returns>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> Upgrade([FromHeader(Name = "u-token")] string token)
        {
            var mobile = MemberHelper.GetMobile(token);
            var cmd = new UpgradeCommand(mobile);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }


        /// <summary>
        /// 打卡
        /// </summary>
        /// <returns>The in.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> SignIn(SignInRequest req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null) {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new SignInCommand(mobile, req.Content, req.ImageList, req.Type);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }


        /// <summary>
        ///点赞状态更改
        /// </summary>
        /// <returns>The state change.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> LikedStateChange(LikedStateChangeRequest req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null) {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new LikedStateChangeCommand(mobile, req.Type, req.ContentId,LoginInfo.Item3);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }
        /// <summary>
        /// Gets the user info.
        /// </summary>
        /// <returns>The user info.</returns>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<UserInfoResponse>> GetUserInfo([FromHeader(Name = "u-token")] string token)
        {
            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetUserInfoCommand(mobile);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }

        /// <summary>
        /// 获取保留打卡信息
        /// </summary>
        /// <returns>The temp sign in content.</returns>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<HappinessTreeResponse>> GetTempSignInContent([FromHeader(Name = "u-token")] string token)
        {
            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetTempSignInContentCommand(mobile);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }

        /// <summary>
        /// 
        ///取消保留信息
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> CancelTempSignInContent([FromHeader(Name = "u-token")] string token)
        {
            var mobile = MemberHelper.GetMobile(token);
            var cmd = new CancelTempSignInContentCommand(mobile);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }

        /// <summary>
        /// 删除打卡信息
        /// </summary>
        /// <returns>The sign in content.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> RemoveSignInContent(RemoveSignInContentRequest req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null) {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new RemoveSignInContentCommand(mobile, req.ContentId);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }

        /// <summary>
        /// 获取打卡内容列表
        /// </summary>
        /// <returns>The my sign in content list.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<PagingResultRsp<List<HappinessTreeResponse>>> GetMySignInContentList(GetMySignInContentListRequest req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null) {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetMySignInContentListCommand {
                Mobile = mobile,
                PageSize = req.PageSize,
                PageIndex = req.PageIndex
            };

            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }

        /// <summary>
        /// 获取我的打卡内容列表
        /// </summary>
        /// <param name="req"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<PagingResultRsp<List<HappinessTreeResponse>>> GetMyContentList(GetMySignInContentListRequest req, [FromHeader(Name = "u-token")] string token)
        {
            if(req == null) {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetMyContentListCommand(mobile,req.PageIndex,req.PageSize);

            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }
        /// <summary>
        /// 上传图片签名
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<AliyunSignResponse>> GetUploadSign([FromHeader(Name = "u-token")] string token)
        {
            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetUploadSignCommand(mobile);
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }
        //           v3 /Common/GetUploadSign
        //获取上传图片签名
        //移动端获取
        //Android/iOS
    }
}
