﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.Application;
using Casamiel.Common;
using Casamiel.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ApiVersionNeutral]
    [Route("api/[controller]")]
    [ApiController]
    public class JobController : Controller
    {
        private readonly INetICService _netICService;
        private readonly AliyunSettings _aliyunSettings;
        private readonly CasamielSettings _casamielSettings;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="netICService"></param>
        /// <param name="aliyunsettings"></param>
        /// <param name="snapshot"></param>
        public JobController(INetICService netICService,IOptionsSnapshot<AliyunSettings> aliyunsettings,IOptionsSnapshot<CasamielSettings> snapshot)
        {
            _netICService = netICService;
            _aliyunSettings=aliyunsettings.Value;
            _casamielSettings=snapshot.Value;
        }
        /// <summary>
        /// 
        /// </summary>
        public class MRoot
        {
            /// <summary>
            /// 
            /// </summary>
            public List<long> mobile { get; set; }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> SendNotice()
        {
            if(_casamielSettings.ApiName!="hgspApi" || DateTime.Now.Hour!=15 ){
                return Ok("");
                }
            var data3 = DateTime.Now.AddDays(3);
            var data7 = DateTime.Now.AddDays(7);
            var a3 = await _netICService.ICTicketExpireQuery(data3.ToString("yyyy-MM-dd 1:00:00"), data3.ToString("yyyy-MM-dd  23:59:59")).ConfigureAwait(false);
            
            if (a3.code == 0) {
                var listmobile = JsonConvert.DeserializeObject<MRoot>(a3.content);

               foreach(var mobile in listmobile.mobile){
                    await SendSms(3,$"{mobile}").ConfigureAwait(false);
                    }
              
            }

            var a = await _netICService.ICTicketExpireQuery(data7.ToString("yyyy-MM-dd 1:00:00"), data7.ToString("yyyy-MM-dd  23:59:59")).ConfigureAwait(false);
            if (a.code == 0) {
                var ddfd = JsonConvert.DeserializeObject<MRoot>(a.content);

               foreach(var mobile in ddfd.mobile)
               {
                    await SendSms(7,$"{mobile}").ConfigureAwait(false);
               }
              
            }


            return Ok("");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="days"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        private async Task SendSms(int days,string mobile)
        {
            string signName = _aliyunSettings.SmsSignName;// "可莎蜜兒";

            var tem = new JObject {
                ["duedate"] = DateTime.Now.AddDays(days).ToString("yyyy-MM-dd")
            };
            string templateParam = tem.ToString();
            string templateCode = "SMS_180351724";
            var response = await SmsHelper.SendSms(_aliyunSettings.accessKey, _aliyunSettings.accessKeySecret, mobile, signName, templateParam, templateCode, "").ConfigureAwait(false);
          
        }
    }
}
