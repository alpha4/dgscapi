﻿using System.Threading.Tasks;
using Casamiel.API.Application;
using Casamiel.API.Application.Models.ICModels;
using Casamiel.API.Application.Services;
using Casamiel.Application;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers
{
    /// <summary>
    /// Refund controller.
    /// </summary>
    [ApiVersionNeutral]
    [ApiController]
    public class RefundController : Controller
    {
        //private readonly IGateways gateways;
        private readonly IWxService _wxService;

        private readonly  IIcApiService _iicApiService;
        //private readonly NLog.ILogger logger = LogManager.GetLogger("OrderService");
		private readonly IRefundService _refundService;

       
       /// <summary>
       /// 
       /// </summary>
       /// <param name="refundService">Refund service.</param>
       /// <param name="wxService">Wx service.</param>
       /// <param name="iicApiService">Iic API service.</param>
        public RefundController(IRefundService refundService, IWxService wxService,IIcApiService iicApiService)
        {
			_refundService = refundService;
            _wxService = wxService;
            _iicApiService = iicApiService;
           
        }
        
 
    //    public IActionResult Index()
    //    {
    //        try
    //        {
				//string a = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "cert\\1495718342\\apiclient_cert.p12"));
				
				//logger.Trace(a);
    //            var cert = new X509Certificate2(a, "1495718342");
    //            Console.WriteLine(cert.Subject);
    //            return Ok(cert.Subject);
    //        }
    //        catch (Exception ex)
    //        {
    //            return Ok(ex.StackTrace);
    //        }
          
    //        //var notify = RefundWechatpetrayOrder(id);
             
    //        //return Json(notify);
    //    }
      
        /// <summary>
        /// Tests the charge back.
        /// </summary>
        /// <returns>The charge back.</returns>
        /// <param name="data">Data.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult>TestChargeBack([FromBody] CardChargeBackReq data){
            var result = await _iicApiService.Icchargeback(data).ConfigureAwait(false);
            return Ok(result);
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="ICTradeNO"></param>
		/// <returns></returns>
		[HttpPost]
		[Route("[action]")]
		public async Task<IActionResult> ChargeBack(string ICTradeNO)
		{
			var info = await _refundService.Icchargeback(ICTradeNO).ConfigureAwait(false);
			return Ok(info);
		}


        /// <summary>
		/// 通过外部交易号退款
		/// </summary>
		/// <param name="OutTradeNO"></param>
		/// <returns></returns>
		[HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> ChargeBackByOutTradeNO(string OutTradeNO)
        {
            var info = await _refundService.IcchargebackByOutTradeNO(OutTradeNO).ConfigureAwait(false);
            return Ok(info);
        }
    }
}
