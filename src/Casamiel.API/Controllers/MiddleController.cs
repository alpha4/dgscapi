﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.API.Application.Models;
using Casamiel.API.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers
{
    [ApiVersionNeutral]
    [Route("api/[controller]")]
    [Authorize]
    public class MiddleController : Controller
    {
        private readonly IMobileCheckCode _mobileCheckCode;

        public MiddleController(IMobileCheckCode mobileCheckCode){
            _mobileCheckCode = mobileCheckCode;
        }
        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <returns>The data.</returns>
        /// <param name="data">Data.</param>
        /// <param name="actionName">Action name.</param>
        /// <param name="token">Token.</param>
        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> GetData([FromBody] JObject data, [FromHeader(Name = "actionName")]string actionName, [FromHeader(Name = "u-token")] string token){
            
            switch (actionName)
            {
                case "casa.v1.wxapp.getCheckcode":
                    return Ok(await getCheckcode(data));
                case "":
                    
                default:
                    return Ok(new { code = 9999, msg = "" });
                   
            }

        }
        //[TypeFilterAttribute(typeof(CheckTokenAttribute))]
        private async  Task<Zmodel> getCheckcode(JObject data)
        {
            if (string.IsNullOrEmpty(data["mobile"].ToString()))
            {
                return new Zmodel { code = -5, msg = "手机号不能为空" };
            }
            string mobile = data["mobile"].ToString();
            var msg = await _mobileCheckCode.SendCheckCodeAsync(mobile);
            if (string.IsNullOrEmpty(msg))
            {
                return new Zmodel { code = 0, msg = "验证码已发送" };
            }
            return new Zmodel { code = -6, msg = msg };
        }
    }
}
