﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.API.Application.Models.V2;
using Casamiel.Application.Commands;
using Casamiel.Domain.Request;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;
using MediatR;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers
{

    /// <summary>
    /// Share controller.
    /// </summary>
    [ApiVersionNeutral]
    [Route("api/[controller]")]
    [ApiController]
    public class ShareController : BaseController
    {

        private readonly IMediator _mediator;
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Controllers.V3.ProductController"/> class.
        /// </summary>
        /// <param name="mediator">Mediator.</param>
        public ShareController(IMediator mediator)
        {
            _mediator = mediator;
        }
        /// <summary>
        /// 获取简单的商品明细
        /// </summary>
        /// <returns>The stock product.</returns>
        /// <param name="data">Data.</param>
        [HttpPost]
        [Route("[Action]")]
        public async Task<BaseResult<SimpleGoodsBaseRsp>> GetSimpleProductDetail([FromBody]ProductCollectReq data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var command = new ProductGetSimpleDetailCommand (LoginInfo.Item3) { Productid = data.ProductBaseId};
            var result = await _mediator.Send(command).ConfigureAwait(false);
            return result;
        }
    }
}
