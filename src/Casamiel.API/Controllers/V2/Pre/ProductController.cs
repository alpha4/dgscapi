﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.API.Application.Models.V2;
using Casamiel.Application.Commands.Pre;
 
using Casamiel.Application.Commands.Waimai.V2;
using Casamiel.Common;
using Casamiel.Domain.Request.Pre;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V2.Pre
{
    /// <summary>
    /// 
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api/v{api-version:apiVersion}/Pre/[controller]")]
    [ApiController]
    [Authorize]
    [EnableCors("any")]
    public class ProductController : BaseController
    {

        private readonly IMediator _mediator;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediator"></param>
        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 预点单分类下产品列表
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[Action]")]
        public async Task<PagingResultRsp<List<TakeOutProductBaseRsp>>> GetList([FromBody] GetProductListReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(nameof(data));
            }
            var cmd = new GetProductListByTagIdCommand("pre") { PageIndex = data.Pageindex, PageSize = data.Pagesize, TagBaseId = data.Tagid, StoreId = data.StoreId };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }


        /// <summary>
        /// 预点单搜索产品
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[Action]")]
        public async Task<BaseResult<List<TakeOutProductBaseRsp>>> Search(ProductSearchRequest request)
		{
			if (request is null) {
				throw new ArgumentNullException(nameof(request));
			}

			var cmd = new ProductSearchCommand { KeyWord=request.Keyword,
            StoreId=request.StoreId};
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }


        /// <summary>
        /// 预点单获取产品标签列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("[Action]")]
        public async Task<BaseResult<List<IndexTagBaseRsp>>> GetTagsList()
        {
            var cmd = new Casamiel.Application.Commands.Waimai.GetTagsListCommand("pre");
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 根据商品Id和门店Id获取商品信息
        /// </summary>
        /// <returns>The product by identifier store identifier.</returns>
        /// <param name="req">Req.</param>
        [HttpPost]
        [Route("[Action]")]
        public async Task<ActionResult<Casamiel.Domain.Response.BaseResult<GoodsProductBaseRsp>>> GetDetail([FromBody] GetProductReq req)
        {
            if (req == null) {
                throw new ArgumentNullException(nameof(req));
            }

            var loginInfo = this.LoginInfo;
            var cmd = new GetProductByIdStoreIdCommand(req.Productid, req.Storeid,"pre",loginInfo.Item3) {
                Source = loginInfo.Item2,
                RequestUrl = Request.GetShortUri(),
                UserIp = Request.GetUserIp()
            };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
    }
}
