﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Casamiel.API.Application.Interface;
using Casamiel.API.Application.Services;
using Casamiel.Application;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using NLog;
using Casamiel.Common;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V2
{
    /// <summary>
    /// Kou bei controller.
    /// </summary>
    [Produces("application/json")]
    [ApiVersion("2.0")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    [ApiController]
    public class KouBeiController : Controller
    {
        private readonly NLog.ILogger logger = LogManager.GetLogger("KouBeiOrderService");
        private readonly NLog.ILogger _stocklogger = LogManager.GetLogger("StockService");
        private readonly IicApiService _icApiService;
        private IStoreApiService _storeApiService;
       // private readonly IStoreService _storeService;
        private readonly ICasaMielSession _session;
        private readonly IThirdPartyPlatformConfigService _thirdPartyPlatformConfigService;
        private readonly string _apitoken;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IMemberCardService _memberCardService;

       /// <summary>
       /// Initializes a new instance of the <see cref="T:Casamiel.API.Controllers.V2.KouBeiController"/> class.
       /// </summary>
       /// <param name="snapshot">Snapshot.</param>
       /// <param name="session">Session.</param>
       /// <param name="iicApiService">Iic API service.</param>
       /// <param name="storeApiService">Store API service.</param>
       /// <param name="thirdPartyPlatformConfigService">Third party platform config service.</param>
       /// <param name="httpClientFactory">Http client factory.</param>
       /// <param name="memberCardService">Member card service.</param>
        public KouBeiController(IOptionsSnapshot<CasamielSettings> snapshot, ICasaMielSession session, IicApiService iicApiService,IStoreApiService storeApiService,IThirdPartyPlatformConfigService thirdPartyPlatformConfigService, IHttpClientFactory httpClientFactory, IMemberCardService memberCardService)
        {
            _apitoken = snapshot.Value.ApiToken;
            _session = session;
            _icApiService = iicApiService;
            _storeApiService = storeApiService;
            _thirdPartyPlatformConfigService = thirdPartyPlatformConfigService;
            _httpClientFactory = httpClientFactory;
            _memberCardService = memberCardService;
        }

         /// <summary>
         /// Creates the order.
         /// </summary>
         /// <returns>The order.</returns>
         /// <param name="token">Token.</param>
        [Route("[action]")]
        [HttpPost]
        public  IActionResult CreateOrder([FromHeader(Name = "u-token")] string token)
        {

            if (token != _apitoken)
            {
                return Ok(new { code = 99999, msg = "token无效" });
            }
            return Ok("");
        }
    }
}
