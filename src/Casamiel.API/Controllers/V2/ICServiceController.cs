﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.API.Application.Services;
using Microsoft.AspNetCore.Mvc;
using Casamiel.API.Application.Models.ICModels;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Request.IC;
using System.Globalization;
//using ProductStock = Casamiel.API.Application.Models.ICModels.ProductStock;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V2
{

    /// <summary>
    /// 
    /// </summary>
    [ApiVersionNeutral]
    [Route("api/[controller]")]
    [ApiController]
    public class ICServiceController : Controller
    {
        private readonly IIcApiService _icApiService;
        private readonly string _apitoken;
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Controllers.V2.ICServiceController"/> class.
        /// </summary>
        /// <param name="iicApiService">Iic API service.</param>
        /// <param name="snapshot">Snapshot.</param>
        public ICServiceController(IIcApiService iicApiService, IOptionsSnapshot<CasamielSettings> snapshot)
        {
            if (snapshot == null)
            {
                throw new System.ArgumentNullException(nameof(snapshot));
            }

            _icApiService = iicApiService;
            _apitoken = snapshot.Value.ApiToken;
        }
        /// <summary>
        ///  查询单个产品信息
        /// </summary>
        /// <returns>The product.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<BaseResult<ProductRsp>>> GetProduct(ProductDetailReq req, [FromHeader(Name = "u-token")]string token)
        {
            if (req == null)
            {
                throw new System.ArgumentNullException(nameof(req));
            }

            if (token != _apitoken)
            {
                return Ok(new { code = 99999, msg = "token无效" });
            }
            var d = await _icApiService.Productdetail(req).ConfigureAwait(false);
            if (d.code == 0)
            {
                var list = JsonConvert.DeserializeObject<ProductRsp>(d.content);
                return new BaseResult<ProductRsp>(list, 0, d.msg);
            }
            return Ok(d);
        }

        /// <summary>
        /// 根据门店id和产品标识pid查询单个产品信息
        /// </summary>
        /// <returns>The product by shop identifier pid.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<BaseResult<Domain.Response.ProductStock>>> GetProductByShopIdPid(GetProductByShopIdPidReq req, [FromHeader(Name = "u-token")]string token)
        {
            if (req == null)
            {
                throw new System.ArgumentNullException(nameof(req));
            }

            if (token != _apitoken)
            {
                return Ok(new { code = 99999, msg = "token无效" });
            }
            var data = new IcPriceReq { Shopid = req.ShopId, Product = new List<ProductItemReq>() };
            data.Product.Add(new ProductItemReq { Pid = req.Pid.ToString(CultureInfo.CurrentCulture) });
            var d = await _icApiService.ProductStockQuery(data).ConfigureAwait(false);
            if(d!=null && d.product.Count>0)
            {
                return new BaseResult<ProductStock>(d.product[0], 0,"");
            }
            return new BaseResult<ProductStock>(null, 0,"");
          
        }
    }
}
