﻿using System;
using System.Threading.Tasks;
using Casamiel.API.Application;
using Casamiel.API.Application.Models.Store;
using Casamiel.Application;
using Casamiel.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V2
{
    /// <summary>
    /// SFC ontroller.
    /// </summary>
    [Produces("application/json")]
    [ApiVersion("2.0")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    [ApiController]
    public class SFController : Controller
    {
        private readonly NLog.ILogger logger = NLog.LogManager.GetLogger("SFOrderService");
        private readonly IStoreApiService _storeApiService;

        private readonly IOrderService _order;
        private readonly string _apitoken;
        /// <summary>
        ///  
        /// </summary>
        /// <param name="orderService"></param>
        /// <param name="storeApiService">Store API service.</param>
        /// <param name="snapshot"></param>
        public SFController(IOrderService orderService, IStoreApiService storeApiService, IOptionsSnapshot<CasamielSettings> snapshot)
        {
            if (snapshot == null)
            {
                throw new ArgumentNullException(nameof(snapshot));
            }

            _storeApiService = storeApiService;
            _apitoken = snapshot.Value.ApiToken;
            _order = orderService;
        }
        /// <summary>
        /// Changes the sf status.
        /// </summary>
        /// <returns>The sf status.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> ChangeSfStatus([FromBody] Order_ChangeSfStatusReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            if (token != _apitoken)
            {
                return Ok(new { code = 99999, msg = "token无效" });
            }
            var result = await _storeApiService.ChangeSfStatusAsync(req).ConfigureAwait(false);
            return Ok(result);
            //if (req.order_status == 17)
            //{
            //    var entity = await _order.GetByOrderCodeAsync<ICasaMielSession>(req.shop_order_id).ConfigureAwait(false);
            //    if (entity != null)
            //    {
            //        IcorderpickupReq dto = new IcorderpickupReq()
            //        {
            //            tradeno = entity.IcTradeNO,
            //            phoneno = entity.Mobile
            //        };
            //        var re = await _icApiService.Icorderpickup(dto).ConfigureAwait(false);
            //        logger.Trace($"SForderpickup,result:{JsonConvert.SerializeObject(re)},req:{JsonConvert.SerializeObject(dto)}");
            //        if (re.code == 0)
            //        {
            //            var jobject = JsonConvert.DeserializeObject<JObject>(re.content);
            //            var billno = jobject["billno"].ToString();
            //            entity.OrderStatus = 7;
            //            await _order.UpdateOrderStatusAndResBillNOAsync<ICasaMielSession>(entity.OrderBaseId, entity.OrderStatus, billno).ConfigureAwait(false);
            //        }
            //    }
            //}

            //var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            //if (dynamicdata["ResultNo"].ToString() == "00000000")
            //{
            //    return Ok(new { code = 0, msg = "" });
            //}
            //if (dynamicdata["ResultNo"].ToString() == "06010007")
            //{
            //    return Ok(new { code = 0, msg = "" });
            //}
            //return Ok(new { code = 9999, msg = dynamicdata["ResultRemark"].ToString() });
        }
    }
}
