﻿using System;
using System.Threading.Tasks;
using Casamiel.API.Application;
using Casamiel.API.Application.Models.Store;
using Casamiel.API.Application.Services;
using Casamiel.API.Infrastructure;
using Casamiel.API.Infrastructure.Filters;
using Casamiel.Application;
using Casamiel.Domain.Request;
using Casamiel.Domain.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V2
{
    /// <summary>
    /// Order take controller.
    /// </summary>
    [Produces("application/json")]
    [ApiVersion("2.0")]
    [Route("api/v{api-version:apiVersion}/OrderTake")]
    [TypeFilterAttribute(typeof(CheckTokenAttribute))]
    [Authorize]
    [ApiController]
    public class OrderTakeController : Controller
    {
        private readonly IIcApiService _icApiService;
        private readonly IStoreApiService _storeApiService;
        private readonly IStoreService _storeService;
        /// <summary>
        /// Initializes a new instance of the <see cref="Casamiel.API.Controllers.V2.OrderTakeController"/> class.
        /// </summary>
        /// <param name="iicApiService">Iic API service.</param>
        /// <param name="storeApiService">Store API service.</param>
        /// <param name="storeService">storeSerivce</param>
        public OrderTakeController(IIcApiService iicApiService, IStoreApiService storeApiService,IStoreService storeService)
        {
            _icApiService = iicApiService;
            _storeApiService = storeApiService;
            _storeService = storeService;
        }

        /// <summary>
        /// 提货单号
        /// </summary>
        /// <returns>The detail.</returns>
        /// <param name="data">Data.</param>
        /// <param name="token">Token.</param>
        /// <remarks>
        /// {"ordercode":"xxxxxxxx"}
        /// </remarks>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<OrderTakeBaseRsp>>GetDetail([FromBody]GetOrderDetailReq data,[FromHeader(Name ="u-token")]string token){
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var mobile = MemberHelper.GetMobile(token);
            var entity = await _storeApiService.GetTakeOrderDetail(data.OrderCode, mobile).ConfigureAwait(false);
            return new BaseResult<OrderTakeBaseRsp>(entity, 0, "");
            //var json = new { code = 0, content = entity };

            //return Ok(json);
        }
    }
}

