﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.API.Application.Models.V2;
using Casamiel.Application.Commands.Waimai.V2;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V2.WaiMai
{
    /// <summary>
    /// waimai产品
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api/v{api-version:apiVersion}/WaiMai/[controller]")]
    [ApiController]
    [Authorize]
    [EnableCors("any")]

    public class ProductController : BaseController
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediator"></param>
        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 外卖 分类下产品列表
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[Action]")]
        public async Task<PagingResultRsp<List<TakeOutProductBaseRsp>>> GetProductList([FromBody]GetProductListReq data)
        {
            if (data == null) {
                throw new ArgumentNullException(nameof(data));
            }
            var cmd = new GetProductListByTagIdCommand { PageIndex = data.Pageindex, PageSize = data.Pagesize, TagBaseId = data.Tagid, StoreId = data.StoreId };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
    }
}
