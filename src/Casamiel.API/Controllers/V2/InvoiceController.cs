﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Casamiel.API.Application.Models;
using Casamiel.API.Infrastructure;
using Casamiel.API.Infrastructure.Filters;
using Casamiel.Application.Commands;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Request;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Invoice;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V2
{
    /// <summary>
    /// Invoice controller.
    /// </summary>
    [Produces("application/json")]
    [ApiVersion("2.0")]
    [Route("api/v{api-version:apiVersion}/[Controller]")]
   
    [Authorize]
    [ApiController]
    public class InvoiceController : Controller
    {
        private readonly IMediator _mediator;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediator">Mediator.</param>
        public InvoiceController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Gets the invoice detail.
        /// </summary>
        /// <returns>The invoice detail.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<Domain.Response.BaseResult<List<InvoiceDetail>>> GetInvoiceDetail([FromBody] GetInvoiceDetailByInvoiceIDReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetInvoiceDetailsCommand(req.InvoiceID, mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 待开票
        /// </summary>
        /// <returns>The un invoice detail list.</returns>
        /// <param name="req"></param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<Domain.Response.PagingResultRsp<List<InvoiceDetail>>> GetUnInvoiceDetailList([FromBody] GetUnInvoiceDetailListReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetUnInvoiceDetailListByMobileCommand(mobile, req.PageIndex, req.PageSize);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Creates the invoice.
        /// </summary>
        /// <returns>The invoice.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> CreateInvoice([FromBody] CreateInvoiceReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new CreateInvoiceCommand(req.InvoiceType, req.IDS, req.BuyerName, req.TaxNum, req.Address, req.Account, req.SmsMobile, req.Email, mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// Previews the invoice.
        /// </summary>
        /// <returns>The invoice.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<Domain.Response.BaseResult<PreviewInvoiceRsp>> PreviewInvoice([FromBody] PreviewInvoiceReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new PreviewInvoiceCommand(req.Ids, mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Gets my invoice.
        /// </summary>
        /// <returns>The my invoice.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<PagingResultRsp<List<BaseInvoice>>> GetMyInvoice([FromBody] GetMyInvoicesReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetMyInvoicesCommand(req.PageIndex, req.PageSize, mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Gets the invoice by identifier.
        /// </summary>
        /// <returns>The invoice by identifier.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<BaseResult<BaseInvoice>> GetInvoiceById([FromBody]GetInvoiceByIdReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetInvoiceByIdMobileCommand(req.Id, mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
       
        /// <summary>
        /// 通过订单号查看开票信息
        /// </summary>
        /// <param name="req"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<OrderInvoiceInformationRsp>>GetOrderInvoceInfoByOrderCode([FromBody]  GetOrderInvoceInfoByOrderCodeReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var cmd = new GetOrderInvoiceInvoiceInfoByOrderCodeCommand { OrderCode = req.OrderCode };
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }
    }
}
