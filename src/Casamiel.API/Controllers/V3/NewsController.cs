﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Casamiel.Application.Commands;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;
using Casamiel.API.Application.Models.Mall.Req;

namespace Casamiel.API.Controllers.V3
{
    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [ApiVersion("3.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [EnableCors("any")]
    public class NewsController : BaseController
    {
        private readonly IMediator _mediator;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediator"></param>
        public NewsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 单个图文素材信息
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[Action]")]
        public async Task<BaseResult<MWebNewsBaseRsp>> GetDetail([FromBody]MWebIdReq req)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var cmd = new NewsGetDetailCommand(req.Id);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
    }
}