﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.API.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Casamiel.Domain.Request;
using Casamiel.API.Infrastructure;
using Casamiel.Domain.Response;
using Casamiel.Application.Commands;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V3
{
    /// <summary>
    /// Shipping address controller.
    /// </summary>
    [Produces("application/json")]
    [ApiVersion("3.0")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    [TypeFilterAttribute(typeof(CheckTokenAttribute))]
    [Authorize]
    [ApiController]
    public class ShippingAddressController : BaseController
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediator">Mediator.</param>
        public ShippingAddressController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 获取蛋糕商城收货地址列表
        /// </summary>
        /// <returns>The list by store identifier.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<List<CakeAddressBaseRsp>>> GetListByStoreId([FromBody] GetListByStoreIdReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetAddressListByStoreIdCommand(req.StoreId, mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        ///获取蛋糕商城收货地址列表(新）返回所有地址
        /// </summary>
        /// <param name="req"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<List<CakeAddressBaseRsp>>> GetNewListByStoreId([FromBody] GetListByStoreIdReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetAddressListByStoreIdCommand(req.StoreId, mobile,"v2");
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 获取蛋糕商城收获地址
        /// </summary>
        /// <returns>The detail.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<CakeAddressBaseRsp>> GetDetail([FromBody]BaseAddressReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetAddressByIdCommand(req.ConsigneeId, mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 设置蛋糕商城默认地址
        /// </summary>
        /// <returns>The default.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> SetDefault([FromBody]BaseAddressReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new AddressSetDefaultCommand(req.ConsigneeId, mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 保存蛋糕商城地址 带有坐标
        /// </summary>
        /// <returns>The save.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<CakeAddressBaseRsp>> Save([FromBody]CakeAddressSaveBaseReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new SaveAddressCommand("v1")
            {
                CityId = req.CityId,
                RealName=req.RealName,
                ConsigneeId = req.ConsigneeId,
                Mobile = mobile,
                DistrictId = req.DistrictId,
                FullAddress = req.FullAddress,
                IsDefault = req.IsDefault,
                Latitude = req.Latitude,
                Longitude = req.Longitude,
                StoreId = req.StoreId,
                Sort = req.Sort,
                Postcode = req.Postcode,
                Phone = req.Phone,
                ProvinceId = req.ProvinceId
            };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 保存蛋糕商城地址 带有坐标（新）
        /// </summary>
        /// <param name="req"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<CakeAddressBaseRsp>> SaveAddress([FromBody]CakeAddressSaveBaseReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new SaveAddressCommand("v2")
            {
                CityId = req.CityId,
                RealName = req.RealName,
                ConsigneeId = req.ConsigneeId,
                Mobile = mobile,
                DistrictId = req.DistrictId,
                FullAddress = req.FullAddress,
                IsDefault = req.IsDefault,
                Latitude = req.Latitude,
                Longitude = req.Longitude,
                StoreId = req.StoreId,
                Sort = req.Sort,
                Postcode = req.Postcode,
                Phone = req.Phone,
                ProvinceId = req.ProvinceId
            };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 删除蛋糕商城地址
        /// </summary>
        /// <returns>The delete.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        public async Task<BaseResult<string>> Delete([FromBody]BaseAddressReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var mobile = MemberHelper.GetMobile(token);
            var cmd = new DeleteAddressCommand(req.ConsigneeId, mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
    }
}
