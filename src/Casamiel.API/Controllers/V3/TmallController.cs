﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.API.Infrastructure.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Casamiel.Domain.Request.Tmall;
using Casamiel.Application.Commands;
using Casamiel.Domain.Response.MWeb;
using Casamiel.Domain.Response;
using Casamiel.Domain.Request;
using Microsoft.AspNetCore.Cors;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V3
{
    /// <summary>
    /// Tmall controller.
    /// </summary>
    [Produces("application/json")]
    [ApiVersion("3.0")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    [ApiController]
    [EnableCors("any")]
    public class TmallController : BaseController
    {
        private readonly IMediator _mediator;
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Controllers.V3.TmallController"/> class.
        /// </summary>
        /// <param name="mediator">Mediator.</param>
        public TmallController(IMediator mediator)
        {
            _mediator = mediator;
        }
        /// <summary>
        ///  login
        /// </summary>
        /// <returns>The login.</returns>
        /// <param name="req">Req.</param>
        [HttpPost]
        [Route("[Action]")]
        public async Task<IActionResult> Login([FromBody] TmallLoginReq req)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var cmd = new TmallLoginCommand(req.UserName, req.Password);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return Ok(result);
        }
        /// <summary>
        /// Logout the specified req.
        /// </summary>
        /// <returns>The logout.</returns>
        /// <param name="req">Req.</param>
        [HttpPost]
        [Route("[Action]")]
        public async Task<IActionResult> Logout([FromBody] TmallLogoutReq req)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var cmd = new TmallLogOutCommand(req.Token, req.StoreBaseId);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return Ok(result);
        }


        /// <summary>
        /// 商品列表
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        /// <remarks>{"token":"","title":""}</remarks>
        [HttpPost]
        [Route("[Action]")]
        public async Task<ActionResult<BaseResult<List<TPP_Goods_ProductRsp>>>> GetProductList([FromBody]TmallGetProductListReq req)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var checktoken = new TmallCheckTokenCommand(req.Token);
            var r = await _mediator.Send(checktoken).ConfigureAwait(false);
            if (r == false)
            {
                return new BaseResult<List<TPP_Goods_ProductRsp>>(null, 9999, "token无效");
            }
            var cmd = new TmallGetProductListCommand(req.Title, req.Token, req.StoreBaseId);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 商品信息
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[Action]")]
        public async Task<BaseResult<GoodsProductBaseRsp>> GetProduct([FromBody]TmallGetProductReq req)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var checktoken = new TmallCheckTokenCommand(req.Token);
            var r = await _mediator.Send(checktoken).ConfigureAwait(false);
            if (r == false)
            {
                return new BaseResult<GoodsProductBaseRsp>(null, 9999, "token无效");
            }
            var cmd = new GetProductByIdStoreIdCommand(req.ProductId, req.StoreId,LoginInfo.Item3);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        public async Task<PagingResultRsp<List<TPPTMallOrderBaseRsp>>> OrderList([FromBody]  TmallGetOrderListReq req)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var cmd = new TmallGetOrderListCommand
            {
                Token = req.Token,
                OrderStatus = req.OrderStatus,
                PageIndex = req.PageIndex,
                PageSize = req.PageSize,
                StoreId=req.StoreId
            };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[Action]")]
        public async Task<BaseResult<string>> CreateOrder([FromBody]TmallCreateOrderReq req)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var checktoken = new TmallCheckTokenCommand(req.Token);
            var r = await _mediator.Send(checktoken).ConfigureAwait(false);
            if (r == false)
            {
                return new BaseResult<string>(null, 9999, "token无效");
            }
            var cmd = new TmallCreateOrderCommand
            {
                ProvinceId=req.ProvinceId,
                CityId = req.CityId,
                CityName = req.CityName,
                ContactName = req.ContactName,
                ContactPhone = req.ContactPhone,
                DiscountMoney = req.DiscountMoney,
                GoodsList = req.GoodsList,
                Latitude = req.Latitude,
                Longitude = req.Longitude,
                OrderCode = req.OrderCode,
                OrderMoney = req.OrderMoney,
                TakeTime = req.TakeTime,
                FreightMoney = req.FreightMoney,
                FullAddress = req.FullAddress,
                Remark = req.Remark,
                StoreId = req.StoreId,
                Token = req.Token,
                DistrictId = req.DistrictId,
                OrderType =req.OrderType==0?2:req.OrderType
            };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
       }
}
