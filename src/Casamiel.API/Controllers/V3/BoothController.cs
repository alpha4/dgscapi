﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;
using Casamiel.API.Application.Models.Mall.Req;
using Casamiel.Domain.Request;
using Casamiel.Application.Commands;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V3
{
    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [ApiVersion("3.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [Authorize]
    [ApiController]
    [EnableCors("any")]
    public class BoothController : BaseController
    {
        private readonly IMediator _mediator;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediator"></param>
        public BoothController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 展位数据
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[Action]")]
        public async Task<BaseResult<List<MWeb_Booth_GetListRsp>>>GetList([FromBody]GetBoothListReq req)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            var cmd = new BoothGetListCommand { Code = req.Code };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
    }
}
