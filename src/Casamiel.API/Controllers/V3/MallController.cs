﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Casamiel.API.Infrastructure;
using Casamiel.Application.Commands;
using Casamiel.Common;
using Microsoft.Extensions.Options;
using Casamiel.Domain.Response;
using Casamiel.API.Application.Models.Mall.Req;
using Casamiel.Domain.Request;
using Casamiel.API.Infrastructure.Filters;
using Newtonsoft.Json;
using Casamiel.API.Application.Commands;
using Casamiel.API.Application;

using Casamiel.Common.Extensions;
using Casamiel.Domain.Entity;
using System.Globalization;
using Casamiel.Application;
using Newtonsoft.Json.Linq;
using Casamiel.API.Application.Models.ICModels;
using Casamiel.API.Application.Services;
using Enyim.Caching;
using System.Data;
using Casamiel.API.Application.Models.Store;
using Casamiel.Domain;
using Casamiel.Domain.Response.MWeb;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V3
{

    /// <summary>
    /// 全国配商城接口
    /// </summary>
    [Produces("application/json")]
    [ApiVersion("3.0")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    [ApiController]
    [EnableCors("any")]
    [Authorize]
    public class MallController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly MallSettings _mallSettings;
        private readonly NLog.ILogger logger = NLog.LogManager.GetLogger("MallService");
        private readonly IIcApiService _icApiService;
        private readonly IMemcachedClient _memcachedClient;
         
        private readonly string _memcachedPre;
        private readonly IPaymentService _paymentService;
        private readonly ICasaMielSession _session;
        private readonly IMallService _mallService;
        private readonly IOrderService _order;
        private readonly IMemberCouponService _memberCouponService;
        private readonly IUserLogService _userLogService;
        private readonly IMemberCardService _memberCardService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="snapshot"></param>
        /// <param name="icApiService"></param>
        /// <param name="paymentService"></param>
        /// <param name="mallService"></param>
        /// <param name="userLogService"></param>
        /// <param name="memcachedClient"></param>
        /// <param name="optionsSnapshot"></param>
        /// <param name="casaMielSession"></param>
        /// <param name="memberCardService"></param>
        /// <param name="orderService"></param>
        /// <param name="memberCouponService"></param>
        public MallController(IMediator mediator, IOptionsSnapshot<MallSettings> snapshot, IIcApiService icApiService, IPaymentService paymentService, IMallService mallService,IUserLogService userLogService,
            IMemcachedClient memcachedClient, IOptionsSnapshot<CasamielSettings> optionsSnapshot, ICasaMielSession casaMielSession, IMemberCardService memberCardService,
            IOrderService orderService, IMemberCouponService memberCouponService)
        {
            if (snapshot is null) {
                throw new ArgumentNullException(nameof(snapshot));
            }

            if (optionsSnapshot is null) {
                throw new ArgumentNullException(nameof(optionsSnapshot));
            }

            _userLogService = userLogService;
            _memcachedClient = memcachedClient;
            _memcachedPre = optionsSnapshot.Value.MemcachedPre;
            _mediator = mediator;
            _mallSettings = snapshot.Value;
            _icApiService = icApiService;
            _paymentService = paymentService;
            _session = casaMielSession;
            _order = orderService;
            _mallService = mallService;
            _memberCouponService = memberCouponService;
            _memberCardService = memberCardService;
        }


        /// <summary>
        /// 我的代金券列表
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<BaseResult<List<MemberCouponReponse>>> GetMyCoupons(GetMyCouponsRequest request, [FromHeader(Name = "u-token")] string token)
        {
            if (request is null) {
                throw new ArgumentNullException(nameof(request));
            }

            var glist = new List<Domain.Request.Waimai.TakeOutOrderGoods>();
            foreach (var item in request.GoodsList) {
                glist.Add(new Domain.Request.Waimai.TakeOutOrderGoods {
                    GoodsBaseId = item.GoodsBaseId,
                    GoodsQuantity = item.GoodsQuantity,
                    GoodsRelationId = item.GoodsRelationId,
                    Price = item.Price
                });
            }
            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetMyUsableCouponsCommand(glist, _mallSettings.StoreId, mobile, false,LoginInfo.Item3);
            return await _mediator.Send(cmd).ConfigureAwait(false);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<BaseResult<MallOrderPreviewReponse>> OrderPreview(CreateMallOrderRequest request, [FromHeader(Name = "u-token")] string token)
        {
            if (request is null) {
                throw new ArgumentNullException(nameof(request));
            }

            logger.Trace($"OrderPreview:{JsonConvert.SerializeObject(request)}");
            var mobile =MemberHelper.GetMobile(token);//13857175708
            var cmd = new MallOrderPreviewCommand(LoginInfo.Item3) {
                BuyType = request.BuyType, ConsigneeId = request.ConsigneeId,
                DiscountList = request.DiscountList,
                GoodsList = request.GoodsList,
                Id = request.Id,
                InvoiceId = request.InvoiceId,
                Mobile = mobile,
                StoreId = _mallSettings.StoreId
            };
            var rsp = await _mediator.Send(cmd).ConfigureAwait(false);
            return rsp;
        }


        /// <summary>
        /// 下单
        /// </summary>
        /// <returns>The order.</returns>
        /// <param name="request">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<ActionResult<BaseResult<PaymentRsp>>> CreateOrder(CreateMallOrderRequest request, [FromHeader(Name = "u-token")] string token)
        {
         
           
            if (request is null) {
                throw new ArgumentNullException(nameof(request));
            }
            var mobile = MemberHelper.GetMobile(token);
            //if (mobile != "13777415620") {
            //    if (request.DiscountList != null && request.DiscountList.Count > 0) {
            //        return new BaseResult<PaymentRsp>(null, 9999, "抱歉，系统维护中");
            //    }
            //}
            
           
            var cmd = new CreateMallOrderCommand(LoginInfo.Item3) {
                Mobile = mobile,
                GoodsList = request.GoodsList,
                DiscountList = request.DiscountList,
                ConsigneeId = request.ConsigneeId,
                BuyType = request.BuyType,
                Id = request.Id,
                Paycardno = request.Paycardno,
                Paymethod = request.PayMethod,
                Remark = request.Remark,
                InvoiceId = request.InvoiceId,
                StoreId = _mallSettings.StoreId

            };

            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            logger.Trace($"{JsonConvert.SerializeObject(result)}");
            if (result.Code == 0 && result.Content.Payed != true) {
                logger.Trace($"{JsonConvert.SerializeObject(result.Content)}");
                var array = result.Content.Paymentrequest.Split(',');
                var p_tradeno = $"C{DateTime.Now.ToString("yyMMddHHmmssfff", CultureInfo.CurrentCulture)}";
                var payment = new PaymentRecord {
                    ShopName = "",
                    OperationMethod = (int)OperationMethod.Mall,
                    TradeNo = p_tradeno,
                    Amount = array[0].ToDouble(0),
                    PayMethod = request.PayMethod,
                    BillNO = result.Content.OrderCode,
                    IcTradeNO = array[1].ToString(CultureInfo.CurrentCulture),
                    State = 0,
                    OperationState = 0,
                    CreateTime = DateTime.Now,
                    Mobile = mobile
                };

                string shopno;
                var shopinfo = await _paymentService.GetShopIdAndShopNameByBillNOAsync<ICasaMielSession>(result.Content.OrderCode).ConfigureAwait(false);
                payment.ShopName = shopinfo.Item2;
                payment.ShopId = shopinfo.Item1;

                var cachekey = string.Format(CultureInfo.CurrentCulture, Constant.SHOPNO, _memcachedPre, payment.ShopId);
                shopno = await _memcachedClient.GetValueAsync<string>(cachekey).ConfigureAwait(false);
                if (string.IsNullOrEmpty(shopno)) {
                    var basedata = await _icApiService.BasedataQuery(new BasedataqueryReq { Datatype = 3, Datavalue = $"{payment.ShopId.Value}" }).ConfigureAwait(false);
                    if (basedata.code == 0) {
                        var d = JsonConvert.DeserializeObject<JArray>(basedata.content);
                        if (d.Count > 0) {
                            shopno = d[0]["no"].ToString();
                            await _memcachedClient.AddAsync(cachekey, shopno, 72000).ConfigureAwait(false);
                        }
                    }
                }
                payment.ShopNO = shopno;
                if (request.PayMethod == (int)PayMethod.WechatMiniPay && request.Id == 0) {
                    return new BaseResult<PaymentRsp>(null, 999, "出错了，请联系我们");
                }
                var command = new CreatePaymentRequestCommand(payment, request.Id, PaymentScenario.Mall);
                var result1 = await _mediator.Send(command).ConfigureAwait(false);


                var pay = new PaymentRsp {
                    OrderCode = result1.Ordercode,
                    Payed = result1.Payed,
                    Paymentrequest = result1.Paymentrequest,
                    PayMethod = result1.PayMethod.ToInt32(0)
                };
                return new BaseResult<PaymentRsp>(pay, 0, "");
            }
 


            return result;
        }

        /// <summary>
        /// 支付 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<BaseResult<PaymentRsp>> Payment([FromBody] OrderPaymentReq data, [FromHeader(Name = "u-token")]string token)
        {
            if (data == null) {
                throw new ArgumentNullException(nameof(data));
            }
            var mobile = MemberHelper.GetMobile(token);
            var order = await _order.GetByOrderCodeAsync<ICasaMielSession>(data.OrderCode).ConfigureAwait(false);
            if (order == null || order.Mobile!=mobile) { 
                return new BaseResult<PaymentRsp>(null, -23, "订单不存在");
            }
            
            if (order.OrderStatus != (int)OrderStatus.Create) {
                //return Ok(new { code = -24, msg = "订单状态不是待支付" });
                return new BaseResult<PaymentRsp>(null, -24, "订单状态不是待支付");
            }
            var discount = await _order.GetListByOrderBaseId<ICasaMielSession>(order.OrderBaseId).ConfigureAwait(false);
            foreach (var item in discount) {
                if (item.DiscountCouponType == 1) {
                    var a = await _icApiService.Geticticket(new GetTicticketReq { Cardno = data.CardNO, Pagesize = 200, State = 2, Pageindex = 1 }).ConfigureAwait(false);
                    if (a.code == 0) {
                        var list = JsonConvert.DeserializeObject<TicketItemRoot>(a.content);
                        var ticket = list.Tickets.SingleOrDefault(t => t.Ticketid == data.DiscountCouponId);
                        if (ticket != null) {
                            data.DiscountCouponMoney = ticket.Je.ToDecimal(0);
                        } else {
                            return new BaseResult<PaymentRsp>(null, -23, "优惠券不可用");
                            // return Ok(new { code = -23, msg = "优惠券不可用" });
                        }
                    } else {
                        return new BaseResult<PaymentRsp>(null, -23, "优惠券不可用");
                        // return Ok(new { code = -23, msg = "优惠券不可用" });
                    }
                }  
                
            }
                
             
            var p_tradeno = $"C{DateTime.Now.ToString("yyMMddHHmmssfff", CultureInfo.CurrentCulture)}";
            //var reorder = await _storeApiService.GetOrderByOrderCodeAsync(data.OrderCode, mobile);
            // var entity = await _storeService.GetByOrderCodeAsync<ICasaMielSession>(order.OrderCode, mobile);
            var payment = new PaymentRecord
            {
                OperationMethod = (int)OperationMethod.Mall,
                TradeNo = p_tradeno,
                Amount = order.PayMoney.ToString(CultureInfo.CurrentCulture).ToDouble(0),
                PayMethod =data.PayMethod,
                BillNO = order.OrderCode,
                IcTradeNO = order.IcTradeNO,
                State = 0, OperationState = 0,  
                CreateTime = DateTime.Now,
                Mobile = mobile
            };


            if (order.PayMoney == 0) {

                var cardno = "";
                var card = await _memberCardService.GetBindCardAsync<ICasaMielSession>(mobile).ConfigureAwait(false);
                if (card != null)
                {
                    cardno = card.CardNO;
                }
                var pay = new IcConsumepayReq {
                    Shopid = _mallSettings.ShopId,
                    Tradeno = order.IcTradeNO,
                    Phoneno = mobile,
                    Cardno=cardno,
                    Createinvoice = true
                };
                pay.Paycontent = new List<IcConsumepayReq.PaycontentItem>();

                foreach (var x in discount) {
                    var _paytype = "5";
                    var _payuser = $"{ x.DiscountCouponId }";
                    if (x.DiscountCouponType == 1) {
                        _paytype = "6";
                    } else {
                        var Coupon = await _memberCouponService.FindAsync<ICasaMielSession>(x.DiscountCouponId).ConfigureAwait(false);
                        _payuser = Coupon.Ticketcode;
                    }

                    pay.Paycontent.Add(new IcConsumepayReq.PaycontentItem {
                        Paytype = _paytype,
                        Paymoney = $"{ x.DiscountCouponUseMoney}".ToDouble(0),
                        Paytradeno = $"t{DateTime.Now.ToString("yyMMddHHmmssffff", CultureInfo.CreateSpecificCulture("en-US"))}",
                        Payuser = _payuser
                    });
                }
                //if (data.DiscountCouponId > 0) {
                //    pay.Paycontent.Add(new IcConsumepayReq.PaycontentItem {
                //        Paytype = "6",
                //        Paymoney = data.DiscountCouponMoney.ToString(CultureInfo.CurrentCulture).ToDouble(0),
                //        Paytradeno = $"t{DateTime.Now.ToString("yyMMddHHmmssffff", CultureInfo.CurrentCulture)}",
                //        Payuser = $"{data.DiscountCouponId}"
                //    });
                //}
                var orderen = await _order.GetByOrderCodeAsync<ICasaMielSession>(data.OrderCode).ConfigureAwait(false);
                var zmodel = await _icApiService.Icconsumepay(pay).ConfigureAwait(false);
                if (zmodel.code == 0) {
                    var jobject = JsonConvert.DeserializeObject<JObject>(zmodel.content);
                    if (jobject != null && jobject["invoiceqrcode"] != null) {
                        var invoiceUrl = jobject["invoiceqrcode"].ToString();
                        await _order.UpdateOrderInvoiceUrlByOrderCodeAsync<ICasaMielSession>(payment.BillNO, invoiceUrl).ConfigureAwait(false);
                    } else {
                        this.logger.Error($"Invoideurl:{zmodel.content}");
                    }

                    payment.PayMethod =(int)PayMethod.Card;
                    payment.PayTime = DateTime.Now;
                    payment.State = 1;
                    payment.OperationState = 1;
                    using (var uow = _session.UnitOfWork(IsolationLevel.Serializable)) {
                        orderen.Status = (int)OrderStatus.Paid;
                        orderen.PayTime = DateTime.Now;
                        await _order.OrderBaseUPdateAsync(orderen, uow).ConfigureAwait(true);
                        //order.OrderStatus = 2;
                        //order.PayTime = DateTime.Now;
                        //await _storeService.OrderBaseUPdateAsync(order, uow);
                        //await _storeService.UpdateOrderTakeStatus(order.OrderBaseId, 0, "", uow);
                        //payment.State = 1;
                        //payment.OperationState = 1;
                        //payment.PayTime = DateTime.Now;
                        await _paymentService.AddAsync(payment, uow).ConfigureAwait(true);
                        //var noticEntity = new Notice_baseEntity()
                        //{
                        //    RemindTime = DateTime.Now.AddMinutes(-3),
                        //    CreateTime = DateTime.Now,
                        //    RelationId = order.OrderBaseId,
                        //    StoreId = order.StoreId,
                        //    NoticeType = 0
                        //};
                        //await _storeService.AddNoticeAsync(noticEntity, uow);

                    }
                    await _mallService.OrderChangeStatus(order.OrderCode, mobile, 1, 3, "").ConfigureAwait(false);
                    var payed = new PaymentRsp { PayMethod = payment.PayMethod, Paymentrequest = "", OrderCode = order.OrderCode, Payed = true };
                    return new BaseResult<PaymentRsp>(payed, 0, "");
                    //return new JsonResult(new { code = 0, content = new { paymentrequest = "", ordercode = order.OrderCode, payMethod = payment.PayMethod.ToString(), payed = true } });

                    //return new JsonResult(new { code = 0, content = entity, payMethod = payment.PayMethod, payed = true });
                }
                return new BaseResult<PaymentRsp>(null, zmodel.code, zmodel.msg);
            }

            var shopno = "";
            var shopinfo = await _paymentService.GetShopIdAndShopNameByBillNOAsync<ICasaMielSession>(order.OrderCode).ConfigureAwait(false);
            payment.ShopName = shopinfo.Item2;
            payment.ShopId = shopinfo.Item1;
            if (payment.ShopId.HasValue) {
                var cachekey = string.Format(CultureInfo.CurrentCulture, Constant.SHOPNO, _memcachedPre, payment.ShopId.Value);
                shopno = await _memcachedClient.GetValueAsync<string>(cachekey).ConfigureAwait(false);
                if (string.IsNullOrEmpty(shopno)) {
                    var basedata = await _icApiService.BasedataQuery(new BasedataqueryReq { Datatype = 3, Datavalue = $"{payment.ShopId.Value}" }).ConfigureAwait(false);
                    if (basedata.code == 0) {
                        var d = JsonConvert.DeserializeObject<JArray>(basedata.content);
                        if (d.Count > 0) {
                            shopno = d[0]["no"].ToString();
                            await _memcachedClient.AddAsync(cachekey, shopno, 72000).ConfigureAwait(false);
                        }
                    }
                }
            }
            payment.ShopNO = shopno;
            if(data.PayMethod==(int)PayMethod.Card) {
               
                    if (string.IsNullOrEmpty(data.PayCardNO)) {
                        return new BaseResult<PaymentRsp>(null, 999, "卡号不能为空");
                        //return Ok(new { code = 999, msg = "卡号不能为空" });
                    }
                    var pay1 = new IcConsumepayReq {
                        Cardno = data.PayCardNO,
                        Shopid = _mallSettings.ShopId,
                        Tradeno = order.IcTradeNO
                    };
                    pay1.Paycontent = new List<IcConsumepayReq.PaycontentItem>
                    {
                        new IcConsumepayReq.PaycontentItem
                        {
                            Paytype = "3",
                            Paymoney = order.PayMoney.ToString(CultureInfo.CurrentCulture).ToDouble(0),
                            Paytradeno = $"c{DateTime.Now.ToString("yyMMddHHmmssffff", CultureInfo.CurrentCulture)}"
                        }
                    };
                //if (data.DiscountCouponId > 0) {
                //    pay1.Paycontent.Add(new IcConsumepayReq.PaycontentItem {
                //        Paytype = "6",
                //        Paymoney = data.DiscountCouponMoney.ToString(CultureInfo.CurrentCulture).ToDouble(0),
                //        Paytradeno = $"t{DateTime.Now.ToString("yyMMddHHmmssffff", CultureInfo.CurrentCulture)}",
                //        Payuser = $"{data.DiscountCouponId}"
                //    });
                //}
                foreach (var x in discount) {
                    var _paytype = "5";
                    var _payuser = $"{ x.DiscountCouponId }";
                    if (x.DiscountCouponType == 1) {
                        _paytype = "6";
                    } else {
                        var Coupon = await _memberCouponService.FindAsync<ICasaMielSession>(x.DiscountCouponId).ConfigureAwait(false);
                        _payuser = Coupon.Ticketcode;
                    }

                    pay1.Paycontent.Add(new IcConsumepayReq.PaycontentItem {
                        Paytype = _paytype,
                        Paymoney = $"{ x.DiscountCouponUseMoney}".ToDouble(0),
                        Paytradeno = $"t{DateTime.Now.ToString("yyMMddHHmmssffff", CultureInfo.CreateSpecificCulture("en-US"))}",
                        Payuser = _payuser
                    }); 
                }
                if (order.FreightMoney > 0) {
                        pay1.Payfee = new List<IcConsumepayReq.PayfeeItem>{new IcConsumepayReq.PayfeeItem
                        {
                            Fee = order.FreightMoney.ToString(CultureInfo.CurrentCulture).ToDouble(0),
                            Feetype="1",
                            Description = "配送费"
                        }};
                    }
                    payment.PayMethod = data.PayMethod;
                    payment.State = 1;
                    payment.OperationState = 1;
                    payment.PayTime = DateTime.Now;
                    var zmodel = await _icApiService.Icconsumepay(pay1).ConfigureAwait(false);
                    logger.Trace($"mallorderpay;req:{JsonConvert.SerializeObject(pay1)},result:{JsonConvert.SerializeObject(zmodel)}");
                    if (zmodel.code == 0) {
                        //var jobject = JsonConvert.DeserializeObject<JObject>(zmodel.content);
                        //var tradeno = jobject["tradeno"].ToString();
                        //payment.OutTradeNo = tradeno;
                        //todo:zhifucg
                        using (var uow = _session.UnitOfWork(IsolationLevel.Serializable)) {
                            order.OrderStatus = (int)OrderStatus.Paid;
                            //await _storeService.OrderBaseUPdateAsync(order, uow);
                            //await _storeService.UpdateOrderTakeStatus(order.OrderBaseId, 0, "", uow);
                            await _paymentService.AddAsync(payment, uow).ConfigureAwait(false);

                        }
                        await _mallService.OrderChangeStatus(order.OrderCode, mobile, 1, 3, "").ConfigureAwait(false);
                        var logdata = new UserLog { Url = Request.GetShortUri(), OPInfo = $"全国送单支付,卡号:[{data.CardNO},支付金额{order.PayMoney}，订单号：{order.OrderCode}", OPTime = DateTime.Now, UserName = mobile, UserIP = Request.GetUserIp() };
                        await _userLogService.AddAsync<ICasaMielSession>(logdata).ConfigureAwait(false);
                        // return new JsonResult(new { code = 0, content = new { paymentrequest = "", ordercode = order.OrderCode, payMethod = payment.PayMethod.ToString(), payed = true } });
                        var paycard = new PaymentRsp { PayMethod = payment.PayMethod, Paymentrequest = "", OrderCode = order.OrderCode, Payed = true };
                        return new BaseResult<PaymentRsp>(paycard, 0, "");

                        //return new JsonResult(new { code = 0, content = entity, payMethod = 3, payed = true });
                    } else {
                        return new BaseResult<PaymentRsp>(null, zmodel.code, zmodel.msg);
                    }

                 
            }
            if (data.PayMethod == (int)PayMethod.WechatMiniPay && data.id == 0) {
                return new BaseResult<PaymentRsp>(null, 999, "出错了，请联系我们");
            }

            using (var uow = _session.UnitOfWork(IsolationLevel.Serializable)) {
                order.LastUpdateTime = DateTime.Now;
                await _order.OrderBaseUPdateAsync(order, uow).ConfigureAwait(true);
            }
            var command = new CreatePaymentRequestCommand(payment, data.id, PaymentScenario.Mall);
            var result1 = await _mediator.Send(command).ConfigureAwait(false);


            var pay2 = new PaymentRsp {
                OrderCode = result1.Ordercode,
                Payed = result1.Payed,
                Paymentrequest = result1.Paymentrequest,
                PayMethod = result1.PayMethod.ToInt32(0)
            };
            return new BaseResult<PaymentRsp>(pay2, 0, "");

             
        }


       
        /// <summary>
        /// 获取订单信息
        /// </summary>
        /// <returns>The order base.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<ActionResult<BaseResult<MallOrderResponse>>> GetOrderBase(GetOrderBaseReq req, [FromHeader(Name = "u-token")]  string token)
        {
            if (req == null) {
                throw new ArgumentNullException(nameof(req));
            }
            var mobile = MemberHelper.GetMobile(token);

            var cmd = new GetMallOrderDetailCommand(req.ordercode, mobile,LoginInfo.Item3);

            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 物流信息
        /// </summary>
        /// <param name="req"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<BaseResult<List<OrderExpressTracesReponse>>> GetExpressTraces(GetOrderBaseReq req, [FromHeader(Name = "u-token")]  string token)
        {
            if (req == null) {
                throw new ArgumentNullException(nameof(req));
            }
            var mobile = MemberHelper.GetMobile(token);

            var cmd = new GetMallOrderExpressCommand(req.ordercode, mobile,LoginInfo.Item3);

            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
         

    }

}
