﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using Casamiel.Domain.Response.MWeb;
using Casamiel.API.Application.Models.V2;
using Casamiel.Application.Commands;
using Casamiel.Common;
using Casamiel.Domain.Request;
using Casamiel.API.Infrastructure.Filters;
using Casamiel.API.Infrastructure;
using Casamiel.Domain.Response;
using Microsoft.AspNetCore.Cors;
using Newtonsoft.Json.Linq;
using Casamiel.Common.Extensions;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Casamiel.API.Controllers.V3
{
    /// <summary>
    /// 商品接口
    /// </summary>
    [Produces("application/json")]
    [ApiVersion("3.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [Authorize]
    [ApiController]
    [EnableCors("any")]
    public class ProductController : BaseController
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediator">Mediator.</param>
        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }
        /// <summary>
        /// 今日达商品详情列表
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        /// <remarks>{"storeId":2}</remarks>
        [HttpPost]
        [Route("[Action]")]
        public async Task<BaseResult<List<ProductBaseRsp>>> GetStockProductList([FromBody]GetStockProductListReq req)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }
            var command = new GetDayProductListByStoreIdCommand(req.StoreId);
            var result = await _mediator.Send(command).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        /// 今日达商品详情
        /// </summary>
        /// <returns>The stock product.</returns>
        /// <param name="data">Data.</param>
        [HttpPost]
        [Route("[Action]")]
        public async Task<BaseResult<GoodsProductBaseRsp>> GetStockProduct([FromBody]GetProductReq data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }
            var command = new GetDayProductDetailCommand { Productid = data.Productid, Storeid = data.Storeid };
            var result = await _mediator.Send(command).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Gets the product by identifier store identifier.
        /// </summary>
        /// <returns>The product by identifier store identifier.</returns>
        /// <param name="req">Req.</param>
        [HttpPost]
        [Route("[Action]")]
        public async Task<ActionResult<Casamiel.Domain.Response.BaseResult<GoodsProductBaseRsp>>> GetDetail([FromBody] GetProductReq req)
        {
            if(req== null)
            {
                throw new ArgumentNullException(nameof(req));
            }
            var loginInfo = this.LoginInfo;
            var cmd = new GetProductByIdStoreIdCommand(req.Productid, req.Storeid, loginInfo.Item3)
            {
                
                RequestUrl = Request.GetShortUri(),
                UserIp = Request.GetUserIp()
            };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 获取蛋糕商城分类下产品列表
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[Action]")]
        public async Task<PagingResultRsp<List<ProductBaseRsp>>> GetProductList([FromBody]GetProductListReq data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }
            var cmd = new GetProductListByTagIdCommand { PageIndex = data.Pageindex, PageSize = data.Pagesize, TagBaseId = data.Tagid, StoreId = data.StoreId };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }


        /// <summary>
        /// 获取首页Tag.
        /// </summary>
        /// <returns>The index tag list.</returns>
        [HttpPost]
        [Route("[Action]")]
        public async Task<BaseResult<List<IndexTagBaseRsp>>> GetIndexTagList()
        {
            var cmd = new GetCakeIndexTagListCommand();
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// 获取蛋糕商城首页商品数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>

        [HttpPost]
        [Route("[Action]")]
        public async Task<BaseResult<List<IndexProductBaseRsp>>> GetIndexGoodsList([FromBody]GetIndexGoodsListReq data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }
            var cmd = new GetCakeIndexProductListCommand { StoreId = data.Storeid };
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        ///the add collect.
        /// </summary>
        /// <returns>The add collect.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<ActionResult<BaseResult<string>>> AddCollect([FromBody] ProductCollectReq req, [FromHeader(Name = "u-token")]string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }
            var mobile = MemberHelper.GetMobile(token);
            var cmd = new ProductAddCollectCommand(req.ProductBaseId, mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

        /// <summary>
        ///CancelCollect
        /// </summary>
        /// <returns>The add collect.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<ActionResult<BaseResult<string>>> CancelCollect([FromBody] ProductCollectReq req, [FromHeader(Name = "u-token")]string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }
            var mobile = MemberHelper.GetMobile(token);
            var cmd = new ProductCancelCollectCommand(req.ProductBaseId, mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Products the check collect.
        /// </summary>
        /// <returns>The check collect.</returns>
        /// <param name="req">Data.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<IActionResult> CheckCollect([FromBody] ProductCollectReq req, [FromHeader(Name = "u-token")]string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }
            var mobile = MemberHelper.GetMobile(token);
            var cmd = new ProductCheckCollectCommand(req.ProductBaseId, mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return Ok(new { code = 0, content = new { collected = result } });
        }
        /// <summary>
        /// Products the clear collect.
        /// </summary>
        /// <returns>The clear collect.</returns>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<ActionResult<BaseResult<string>>> ClearCollect([FromHeader(Name = "u-token")] string token)
        {
            var mobile = MemberHelper.GetMobile(token);
            var cmd = new ProductClearCollectCommand(mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }
        /// <summary>
        /// Gets the collect list.
        /// </summary>
        /// <returns>The collect list.</returns>
        /// <param name="req">Req.</param>
        /// <param name="token">Token.</param>
        [HttpPost]
        [Route("[action]")]
        [TypeFilterAttribute(typeof(CheckTokenAttribute))]
        public async Task<PagingResultRsp<List<GoodsCollectRsp>>> GetMyCollectList([FromBody]BasePagingReq req, [FromHeader(Name = "u-token")] string token)
        {
            if (req == null)
            {
                throw new ArgumentNullException(nameof(req));
            }

            if (req.PageIndex <= 0)
            {
                req.PageIndex = 1;
            }
            if (req.PageSize <= 0)
            {
                req.PageSize = 20;
            }
            var mobile = MemberHelper.GetMobile(token);
            var cmd = new GetProductCollectListCommand(req.PageIndex, req.PageSize, mobile);
            var result = await _mediator.Send(cmd).ConfigureAwait(false);
            return result;
        }

    }
}
