﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using App.Metrics.Health;
namespace Casamiel.API.HealthChecks
{
    /// <summary>
    /// OKH ealth check.
    /// </summary>
    public class OKHealthCheck : HealthCheck
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.HealthChecks.OKHealthCheck"/> class.
        /// </summary>
        public OKHealthCheck() : base("正常的检查(OKHealthCheck)") { }
        /// <summary>
        /// Checks the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="token">Token.</param>
   //     protected override ValueTask<HealthCheckResult> CheckAsync(CancellationToken token = default(CancellationToken))
   //     {
			////return ValueTask < HealthCheckResult.Healthy("OK") >;
        //    //返回正常的信息
        //   return ValueTask(HealthCheckResult.Healthy("OK"));
        //}
        /// <summary>
        /// Checks the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="token">Token.</param>
        protected override Task<HealthCheckResult> CheckAsync(CancellationToken token = default(CancellationToken))
        {
            return Task.FromResult(HealthCheckResult.Degraded("Degraded"));
        }
    }
}
