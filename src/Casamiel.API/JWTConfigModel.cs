﻿using System.Security.Cryptography;

namespace Casamiel.API
{
    /// <summary>
    /// JWTC onfig model.
    /// </summary>
    public class JWTConfigModel
    {
        /// <summary>
        /// Gets or sets the secret key.
        /// </summary>
        /// <value>The secret key.</value>
        public RSA SecretKey { get; set; }
        /// <summary>
        /// Gets or sets the issuer.
        /// </summary>
        /// <value>The issuer.</value>
        public string Issuer { get; set; }
        /// <summary>
        /// Gets or sets the audience.
        /// </summary>
        /// <value>The audience.</value>
        public string Audience { get; set; }
    }
}