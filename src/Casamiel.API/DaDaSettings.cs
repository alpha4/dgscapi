﻿using System;
namespace Casamiel.API
{
    /// <summary>
    /// Da da settings.
    /// </summary>
    public class DaDaSettings
    {
#pragma warning disable IDE1006 // 命名样式
        /// <summary>
        /// Gets or sets the appkey.
        /// </summary>
        /// <value>The appkey.</value>
        public string appkey { get; set; }
#pragma warning restore IDE1006 // 命名样式

 
        /// <summary>
        /// Gets or sets the app secret.
        /// </summary>
        /// <value>The app secret.</value>
        public string app_secret { get; set; }
         /// <summary>
        /// Gets or sets the apiurl.
        /// </summary>
        /// <value>The apiurl.</value>
        public string apiurl { get; set; }


#pragma warning disable IDE1006 // 命名样式
        /// <summary>
        /// Gets or sets the source identifier.
        /// </summary>
        /// <value>The source identifier.</value>
        public string source_id { get; set; }

        /// <summary>
        /// 业务类型21蛋糕，2饮料
        /// </summary>
        public string Business { get; set; }
#pragma warning restore IDE1006 // 命名样式


#pragma warning disable IDE1006 // 命名样式
        /// <summary>
        /// 联系人
        /// </summary>
        /// <value>The name of the contact.</value>
        public string contact_name { get; set; }
#pragma warning restore IDE1006 // 命名样式
#pragma warning disable IDE1006 // 命名样式
        /// <summary>
        /// 门店前缀
        /// </summary>
        /// <value>The store pre.</value>
        public string store_pre { get; set; }
#pragma warning restore IDE1006 // 命名样式
        /// <summary>
        ///联系电话
        /// </summary>
        /// <value>The phone.</value>
        public string phone { get; set; }
    }
}
