JsBarcode("#barcode1", "Hi!", {
  fontSize: 40,
  background: "#4b8b7f",
  lineColor: "#ffffff",
  margin: 40,
  marginLeft: 80
});

JsBarcode("#barcode2", "Hi!", {
  textAlign: "left",
  textPosition: "top",
  font: "cursive",
  fontOptions: "bold",
  fontSize: 40,
  textMargin: 15,
  text: "Special"
});

JsBarcode("#barcode3", "1234", {
  format: "pharmacode",
  displayValue: false,
  height: 50,
  width: 6
});