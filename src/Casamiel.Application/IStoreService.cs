﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Casamiel.Domain;
using Casamiel.Domain.Entity;
using Newtonsoft.Json.Linq;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application
{
    /// <summary>
    /// 通知
    /// </summary>
    public interface INoticeService
    {
        /// <summary>
        /// Adds the notice.
        /// </summary>
        /// <param name="entity">Entity.</param>
        /// <param name="uow">Uow.</param>
        Task AddAsync(NoticeBaseEntity entity, IUnitOfWork uow);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="RelationId"></param>
        /// <param name="Status"></param>
        /// <param name="uow"></param>
        /// <returns></returns>
        Task UpdateNoticeByRelationIdAsync(int RelationId, int Status, IUnitOfWork uow);
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IProductService
    {
        /// <summary>
        /// Gets the good base list async.
        /// </summary>
        /// <returns>The good base list async.</returns>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<GoodBaseEntity>> GetGoodBaseListAsync<TSession>() where TSession : class, ISession;

        /// <summary>
        /// Updates the stock.
        /// </summary>
        /// <returns>The stock.</returns>
        /// <param name="storeid">Storeid.</param>
        /// <param name="pid">Pid.</param>
        /// <param name="stock">Stock.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task UpdateStockAsync<TSession>(int storeid, int pid, int stock) where TSession : class, ISession;
        /// <summary>
        /// 更新库存
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="storeid"></param>
        /// <param name="pid"></param>
        /// <param name="stock"></param>
        /// <returns></returns>
        Task UpdateStockV2Async<TSession>(int storeid, int pid, int stock) where TSession : class, ISession;

		 
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IOrderService
    {
        //Task<List<Order_BaseEntity>> GetList<TSession>() where TSession : class, ISession;

        Task<Order_DiscountCouponEntity> GetByOrderBaseId<TSession>(int OrderBaseId) where TSession : class, ISession;

        Task<List<Order_DiscountCouponEntity>> GetListByOrderBaseId<TSession>(int OrderBaseId) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="entity"></param>
		/// <returns></returns>
		Task OrderOperationLogAddAsync<TSession>(OrderOperationLog entity) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="source"></param>
		/// <returns></returns>
		Task<List<OrderBaseEntity>> GetPayedOrderListAsync<TSession>(int source) where TSession : class, ISession;
        /// <summary>
        /// Gets the by order code async.
        /// </summary>
        /// <returns>The by order code async.</returns>
        /// <param name="orderCode">Order code.</param>
        /// <param name="mobile">mobile</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<OrderBaseEntity> GetByOrderCodeAsync<TSession>(string orderCode, string mobile) where TSession : class, ISession;
        /// <summary>
        /// Gets the by order code async.
        /// </summary>
        /// <returns>The by order code async.</returns>
        /// <param name="orderCode">Order code.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<OrderBaseEntity> GetByOrderCodeAsync<TSession>(string orderCode) where TSession : class, ISession;

      
        /// <summary>
        /// Deletes the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="OrderCode">Order code.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task DeleteAsync<TSession>(string OrderCode) where TSession : class, ISession;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="OrderCode"></param>
        /// <param name="InvoiceUrlstr"></param>
        /// <returns></returns>
        Task UpdateOrderInvoiceUrlByOrderCodeAsync<TSession>(string OrderCode, string InvoiceUrlstr) where TSession : class, ISession;

        /// <summary>
        /// Updates the order status and res bill NOA sync.
        /// </summary>
        /// <returns>The order status and res bill NOA sync.</returns>
        /// <param name="OrderBaseId">Order base identifier.</param>
        /// <param name="OrderStatus">Order status.</param>
        /// <param name="Resbillno">Resbillno.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task UpdateOrderStatusAndResBillNOAsync<TSession>(int OrderBaseId, int OrderStatus, string Resbillno) where TSession : class, ISession;

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The order status and res bill NOA sync.</returns>
        /// <param name="entity">Entity.</param>
        /// <param name="NewOrderStatus">New order status.</param>
        /// <param name="Resbillno">Resbillno.</param>
        /// <param name="uow">Uow.</param>
        Task<bool> UpdateOrderStatusAndResBillNOAsync(OrderBaseEntity entity, int NewOrderStatus, string Resbillno, IUnitOfWork uow);

        /// <summary>
        /// UPs the date async.
        /// 订单状态，1：新建、2：已支付、7：已完成 、8：已取消 、9：交易关闭
        /// </summary>
        /// <returns>The date async.</returns>
        /// <param name="entity">Entity.</param>
        /// <param name="uow">Uow.</param>
        Task<bool> OrderBaseUPdateAsync(OrderBaseEntity entity, IUnitOfWork uow);
        /// <summary>
        /// Updates the order tacke status.
        /// 状态 -1 未支付 0 未提货 1 已提货
        /// </summary>
        /// <returns>The order tacke status.</returns>
        /// <param name="OrderBaseId">Order base identifier.</param>
        /// <param name="Status">Status.</param>
		/// <param name="billno">billno</param>
        /// <param name="uow">Uow.</param>
        Task<bool> UpdateOrderTakeStatus(int OrderBaseId, int Status, string billno, IUnitOfWork uow);

        /// <summary>
        /// Updates the order discount status.
        /// 优惠券状态：0正常，1取消
        /// </summary>
        /// <returns>The order discount status.</returns>
        /// <param name="OrderBaseId">Order base identifier.</param>
        /// <param name="Status">Status.</param>
        /// <param name="uow">Uow.</param>
        Task<bool> UpdateOrderDiscountStatus(int OrderBaseId, int Status, IUnitOfWork uow);

        /// <summary>
        /// Gets the list async.
        /// </summary>
        /// <returns>The list async.</returns>
        /// <param name="n">N.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<OrderBaseEntity>> GetUnPayOrderListAsync<TSession>(int n) where TSession : class, ISession;
    }
    /// <summary>
    /// 网店
    /// </summary>
    public interface IStoreService
    {
        /// <summary>
        /// Gets all store.
        /// </summary>
        /// <returns>The all store.</returns>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<StoreEntity>> GetAllStore<TSession>() where TSession : class, ISession;
        /// <summary>
        /// Gets the store by name async.
        /// </summary>
        /// <returns>The store by name async.</returns>
        /// <param name="StoreName">Store name.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>

        /// <summary>
        /// Gets the store by name async.
        /// </summary>
        /// <returns>The store by name async.</returns>
        /// <param name="StoreName">Store name.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<PagingResult<StoreEntity>> GetStoreByNameAsync<TSession>(string StoreName, int pageIndex, int pageSize) where TSession : class, ISession;
        /// <summary>
        /// Gets the store by name async.
        /// </summary>
        /// <returns>The store by name async.</returns>
        /// <param name="StoreName">Store name.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<StoreEntity>> GetStoreByNameAsync<TSession>(string StoreName) where TSession : class, ISession;
        /// <summary>
        /// Gets all store by name async.
        /// </summary>
        /// <returns>The all store by name async.</returns>
        /// <param name="StoreName">Store name.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<StoreEntity>> GetAllStoreByNameAsync<TSession>(string StoreName) where TSession : class, ISession;

        /// <summary>
        /// Gets the list async.
        /// </summary>
        /// <returns>The list async.</returns>
        /// <param name="StoreName">Store name.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<StoreEntity>> GetListAsync<TSession>(string StoreName) where TSession : class, ISession;
        /// <summary>
        /// Gets the by store identifier async.
        /// </summary>
        /// <returns>The by store identifier async.</returns>
        /// <param name="StoreId">Store identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<StoreEntity> GetByStoreIdAsync<TSession>(int StoreId) where TSession : class, ISession;
        /// <summary>
        /// Gets the by relation identifier async.
        /// </summary>
        /// <returns>The by relation identifier async.</returns>
        /// <param name="RelationId">Relation identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<StoreEntity> GetByRelationIdAsync<TSession>(int RelationId) where TSession : class, ISession;
        /// <summary>
        /// Gets the detail.
        /// </summary>
        /// <returns>The detail.</returns>
        /// <param name="StoreId">Store identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<StoreEntity> GetDetail<TSession>(int StoreId) where TSession : class, ISession;



        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="minLat"></param>
        /// <param name="minLng"></param>
        /// <param name="maxLat"></param>
        /// <param name="maxLng"></param>
        /// <returns></returns>
        Task<List<StoreEntity>> GeRectRangeAsync<TSession>(double minLat, double minLng, double maxLat, double maxLng) where TSession : class, ISession;
        /// <summary>
        /// 产品标签
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <returns></returns>
        Task<List<Product_tagbaseEntity>> GetProductTagAsync<TSession>() where TSession : class, ISession;

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="orderBaseId"></param>
        /// <returns></returns>
        Task<List<Order_GoodsEntity>> GetGoodsByOrderBaseIdAsync<TSession>(int orderBaseId) where TSession : class, ISession;

        /// <summary>
        /// Updates the notice by relation identifier asyncint.
        /// </summary>
        /// <returns>The notice by relation identifier asyncint.</returns>
        /// <param name="RelationId">Relation identifier.</param>
        /// <param name="Status">Status.</param>
        /// <param name="uow">Uow.</param>


        /// <summary>
        /// Adds the mbr extend info async.
        /// </summary>
        /// <returns>The mbr extend info async.</returns>
        /// <param name="entity">Entity.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task AddMbrExtendInfoAsync<TSession>(MbrExtendInfoEntity entity) where TSession : class, ISession;
        /// <summary>
        /// Gets the orde take by order code async.
        /// </summary>
        /// <returns>The orde take by order code async.</returns>
        /// <param name="orderCode">Order code.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<Order_TakeEntity> GetOrdeTakeByOrderCodeAsync<TSession>(int OrderBaseId) where TSession : class, ISession;



        Task<StoreEntity> GetByELEStoreIdAsync<TSession>(string ELEStoreId) where TSession : class, ISession;
        /// <summary>
        /// Gets the good detail by releation identifier async.
        /// </summary>
        /// <returns>The good detail by releation identifier async.</returns>
        /// <param name="RelationId">Relation identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<GoodBaseEntity> GetGoodDetailByReleationIdAsync<TSession>(int RelationId) where TSession : class, ISession;

		Task<GoodBaseEntity> GetDetailV2ByReleationIdAsync<TSession>(int RelationId) where TSession : class, ISession;
		/// <summary>
		/// Gets the goods quantity by pid async.
		/// </summary>
		/// <returns>The goods quantity by pid async.</returns>
		/// <param name="pid">Pid.</param>
		/// <param name="dt">Dt.</param>
		/// <typeparam name="TSession">The 1st type parameter.</typeparam>
		Task<int> GetGoodsQuantityByPidAsync<TSession>(int pid, DateTime dt) where TSession : class, ISession;


        /// <summary>
        /// Gets the product identifier s async.
        /// </summary>
        /// <returns>The product identifier s async.</returns>
        /// <param name="StoreId">Store identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<string>> GetProductIDsAsync<TSession>(int StoreId) where TSession : class, ISession;
    }
}
