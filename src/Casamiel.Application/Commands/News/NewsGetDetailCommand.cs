﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Response.MWeb;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class NewsGetDetailCommand:IRequest<BaseResult<MWebNewsBaseRsp>> 
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        public NewsGetDetailCommand(int Id)
        {
            this.Id = Id;
        }
    }
}
