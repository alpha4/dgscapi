﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Microsoft.Extensions.Options;
using Casamiel.Common;

namespace Casamiel.Application.Commands
{  

   
    public sealed  class NewsGetDetailCommandHandler:BaseCommandHandler,IRequestHandler<NewsGetDetailCommand,BaseResult<MWebNewsBaseRsp>>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="netICService"></param>
        /// <param name="httpClientFactory"></param>
        /// <param name="settings"></param>
        public NewsGetDetailCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
        }

        public async Task<BaseResult<MWebNewsBaseRsp>> Handle(NewsGetDetailCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "CakeMall/v1/News/GetOne";
            var result = await PostThirdApiAsync<MWebNewsBaseRsp>(url, JsonConvert.SerializeObject(new { id=request.Id})).ConfigureAwait(false);
            return result;
           
        }
    }
}
