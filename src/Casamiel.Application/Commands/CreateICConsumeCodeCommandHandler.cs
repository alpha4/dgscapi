﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common.Extensions;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// Create ICC onsume code command handler.
	/// </summary>
	public sealed class CreateICConsumeCodeCommandHandler:IRequestHandler<CreateICConsumeCodeCommand,Tuple<ICConsumeCodeRsp,int,string>>
    {
        private readonly INetICService _netICService;
        /// <summary>
        /// Initializes a new instance of the
        /// <param name="netICService">Net ICS ervice.</param>
        public CreateICConsumeCodeCommandHandler(INetICService netICService)
        {
            _netICService = netICService;
        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<Tuple<ICConsumeCodeRsp, int, string>> Handle(CreateICConsumeCodeCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            return await _netICService.Icconsumecode(request.CardNO,$"{request.DataSource}099".ToInt32(0)).ConfigureAwait(false);
        }
    }
}
