﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Microsoft.Extensions.Options;
using Casamiel.Common;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class BoothGetListCommandHandler:BaseCommandHandler,IRequestHandler<BoothGetListCommand,BaseResult<List<MWeb_Booth_GetListRsp>>>
    {
        public BoothGetListCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<BaseResult<List<MWeb_Booth_GetListRsp>>> Handle(BoothGetListCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "CakeMall/v1/Booth/GetList";
            var data = new { code = request.Code };
            var result = await PostThirdApiAsync<List<MWeb_Booth_GetListRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
            //var rsp = JsonConvert.DeserializeObject<JObject>(result);

            //if (rsp["resultNo"].ToString() == "00000000")
            //{
            //    var list = JsonConvert.DeserializeObject<List<MWeb_Booth_GetListRsp>>(rsp["data"].ToString());
            //    return new BaseResult<List<MWeb_Booth_GetListRsp>>(list, 0, "");//
            //}
            //else
            //{
            //    BizInfologger.Error($"{url},{JsonConvert.SerializeObject(request)},{rsp["resultRemark"].ToString()}");

            //    return new BaseResult<List<MWeb_Booth_GetListRsp>>(null, 9999, rsp["resultRemark"].ToString());
            //}
        }
    }
}
