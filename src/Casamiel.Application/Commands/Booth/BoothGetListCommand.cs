﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public sealed   class BoothGetListCommand:IRequest<BaseResult<List<MWeb_Booth_GetListRsp>>>
    {
        public string Code { get; set; }
    }
}
