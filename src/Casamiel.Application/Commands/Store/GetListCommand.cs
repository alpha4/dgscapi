﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class GetListCommand:BaseCommand,IRequest<BaseResult<List<StoreEntity>>>
    {
        /// <summary>
        /// 纬度
        /// </summary>
        public double Lat { get; private set; }

        /// <summary>
        /// 经度（x)
        /// </summary>
        public double Lng { get; private set; }

        public double Distance { get; private set; }

       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Lat"></param>
        /// <param name="Lng"></param>
        /// <param name="StoreName"></param>
        /// <param name="Distance"></param>
        public GetListCommand(double Lat,double Lng,double Distance):base(1)
        {
            this.Distance = Distance;
            this.Lat = Lat;
            this.Lng = Lng;
           
        }
    }
}
