﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands.Store
{
    public sealed  class GetAllStoreByStoreNameCommand : IRequest< PagingResultRsp<List<StoreEntity>>>
    {
        public string storeName { get; set; }
        /// <summary>
        /// Gets or sets the index of the page.
        /// </summary>
        /// <value>The index of the page.</value>
        public int pageIndex { get; set; }
        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int pageSize { get; set; }
        /// <summary>
        /// Gets or sets the lat.
        /// </summary>
        /// <value>The lat.</value>
        public double lat { get; set; }
        /// <summary>
        /// Gets or sets the lng.
        /// </summary>
        /// <value>The lng.</value>
        public double lng { get; set; }
    }
}
