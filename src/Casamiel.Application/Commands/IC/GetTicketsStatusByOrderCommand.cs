﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Request.Waimai;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{ 

	/// <summary>
	/// 
	/// </summary>
	public sealed class GetTicketsStatusByOrderCommand:BaseCommand,IRequest<BaseResult<List<TicketItem>>>
	{
		public List<TakeOutOrderGoods> GoodsList { get; private set; }

		public int StoreId { get; private set; }


		public string Mobile { get; private set; }

		public bool IsTakeOut { get; private set; }
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderGoods"></param>
		/// <param name="StoreId"></param>
		/// <param name="Mobile"></param>
		/// <param name="IsTakeOut"></param>
		/// <param name="DataSource"></param>
		public GetTicketsStatusByOrderCommand(List<TakeOutOrderGoods>  orderGoods,int StoreId, string Mobile,bool IsTakeOut,int DataSource):base(DataSource)
		{
			this.GoodsList = orderGoods;
			this.StoreId = StoreId;
			this.Mobile = Mobile;
			this.IsTakeOut = IsTakeOut;
		}
	}
}
