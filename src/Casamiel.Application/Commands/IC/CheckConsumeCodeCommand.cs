﻿using System;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands.IC
{
	/// <summary>
	/// 监测消费码
	/// </summary>
	public sealed class CheckConsumeCodeCommand : BaseCommand, IRequest<ICResult>
	{
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public string CardNO { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public string ConsumeCode { get; private set; }
		 
		/// <summary>
		/// 
		/// </summary>
		/// <param name="mobile"></param>
		/// <param name="cardNO"></param>
		/// <param name="consumeCode"></param>
		public CheckConsumeCodeCommand(string mobile, string cardNO, string consumeCode,int DataSource):base(DataSource)
		{
			this.CardNO = cardNO;
			this.Mobile = mobile;
			this.ConsumeCode = consumeCode;
		}

	}
}
