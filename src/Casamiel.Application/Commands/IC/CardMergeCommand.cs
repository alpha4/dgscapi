﻿using System;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands.IC
{
	/// <summary>
	/// 
	/// </summary>
	public class CardMergeCommand : BaseCommand, IRequest<BaseResult<string>>
	{
		public CardMergeCommand(int DataSource) : base(DataSource) { }
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; set; }
	}
}
