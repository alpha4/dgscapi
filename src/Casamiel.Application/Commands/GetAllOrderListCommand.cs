﻿using System;
using System.Collections.Generic;
using Casamiel.Domain;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get all order list command.
    /// </summary>
    public sealed class GetAllOrderListCommand : IRequest<Domain.Response.PagingResultRsp<List<CakeOrderGetListRsp>>>
    {
        /// <summary>
        /// Gets the index of the page.
        /// </summary>
        /// <value>The index of the page.</value>
        public int PageIndex { get; private set; }
        /// <summary>
        /// Gets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; private set; }
        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>The status.</value>
        public int Status { get; private set; }
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.GetAllOrderListCommand"/> class.
        /// </summary>
        /// <param name="PageIndex">Page index.</param>
        /// <param name="PageSize">Page size.</param>
        /// <param name="Status">Status.</param>
        /// <param name="Mobile">Mobile.</param>
        public GetAllOrderListCommand(int PageIndex, int PageSize, int Status, string Mobile)
        {
            this.PageIndex = PageIndex;
            this.PageSize = PageSize;
            this.Status = Status;
            this.Mobile = Mobile;
        }
    }
}
