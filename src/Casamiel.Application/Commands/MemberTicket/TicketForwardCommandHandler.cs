﻿using System;
using System.Data;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;

namespace Casamiel.Application.Commands.MemberTicket
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class TicketForwardCommandHandler : BaseCommandHandler, IRequestHandler<TicketForwardCommand, BaseResult<MemberTicketGive>>
	{
		private readonly IMemberRepository _memberRepository;
		private readonly IMemberCardRepository _memberCardRepository;
		private readonly IMemberCouponRepository _memberCouponRepository;
		private readonly IMemberTicketGiveRepository _memberTicketGiveRepository ;
		private readonly ICasaMielSession _session;

		 /// <summary>
		 /// 
		 /// </summary>
		 /// <param name="memberRepository"></param>
		 /// <param name="memberCardRepository"></param>
		 /// <param name="memberCouponRepositor"></param>
		 /// <param name="netICService"></param>
		 /// <param name="httpClientFactory"></param>
		 /// <param name="settings"></param>
		 /// <param name="casaMielSession"></param>
		 /// <param name="memberTicketGiveRepository"></param>
		public TicketForwardCommandHandler(IMemberRepository memberRepository,IMemberCardRepository memberCardRepository, IMemberCouponRepository memberCouponRepositor, INetICService netICService,
			IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings, ICasaMielSession casaMielSession, IMemberTicketGiveRepository
			 memberTicketGiveRepository) : base(netICService, httpClientFactory, settings)

		{
			_memberRepository = memberRepository;
			_memberCardRepository = memberCardRepository;
			_memberCouponRepository = memberCouponRepositor;
			_session = casaMielSession;
			_memberTicketGiveRepository = memberTicketGiveRepository;
		}

		public async Task<BaseResult<MemberTicketGive>> Handle(TicketForwardCommand request, CancellationToken cancellationToken)
		{
			if (request is null) {
				throw new ArgumentNullException(nameof(request));
			}

			var entity = await _memberRepository.FindAsync<ICasaMielSession>(request.Mobile).ConfigureAwait(false);
			if (entity == null) {
				return new BaseResult<MemberTicketGive>(null, 999, "会员不存在");
			}
			//var coupon = await _memberCouponRepository.GetKeyAsync<ICasaMielSession>(request.MCID).ConfigureAwait(false);
			//if (coupon == null || coupon.MID != entity.ID) {
			//	return new BaseResult<MemberTicketGive>(null, 999, "券不存在");
			//}

			//if (coupon.GiveStatus != 0 && coupon.GiveStatus != -1) {
			//	string msg = "";
			//	switch (coupon.GiveStatus) {
			//		case 1:
			//			msg = "不可赠送";
			//			break;

			//		case -2:
			//			msg = "券已经赠送";
			//			break;
			//	}
			//	return new BaseResult<MemberCouponGive>(null, 999, msg);
			//}

			var forwardcode = Guid.NewGuid().ToString();// Common.Utilities.SecurityUtil.EncryptText($"{DateTime.Now.Ticks}-{request.MCID}", entity.Mobile);
			var coupongive = new MemberTicketGive {
				ForwardCode = forwardcode,
				Ticketid=request.Ticketid,
				Je=request.Je,
				Message = request.Message,
				Ticketname = request.TicketName,
				Status = 0,
				 
				CreateTime = DateTime.Now,


			};
			 
			using (var uow = _session.UnitOfWork(IsolationLevel.Serializable)) {
				await _memberTicketGiveRepository.SaveAsync(coupongive, uow).ConfigureAwait(false);
				//s = await _memberCouponRepository.UpdateCouponGiveStatusAsnync(coupon, -1, uow).ConfigureAwait(false);
				//if (s) {
					uow.Commit();
				//} else {
				//	uow.Rollback();
				//}
			} 
			return new BaseResult<MemberTicketGive>(coupongive, 0, "");
 
		}
	}
}
