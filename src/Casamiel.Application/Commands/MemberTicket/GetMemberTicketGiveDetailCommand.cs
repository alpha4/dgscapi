﻿using System;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.IC;
using MediatR;

namespace Casamiel.Application.Commands.MemberTicket
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetMemberTicketGiveDetailCommand : BaseCommand, IRequest<BaseResult<TicketGiveReponse>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string ForwardCode { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="ForwardCode"></param>
		/// <param name="DataSource"></param>
		public GetMemberTicketGiveDetailCommand(string ForwardCode,int DataSource):base(DataSource)
		{
			this.ForwardCode = ForwardCode;
		}
	}
}
