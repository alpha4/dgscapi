﻿using System;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands.MemberTicket
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetTicketCommand : BaseCommand, IRequest<BaseResult<string>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; private set; }


		/// <summary>
		/// yzm
		/// </summary>
		public string Yzm { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public string ForwardCode { get; private set; }


		/// <summary>
		/// 
		/// </summary>
		/// <param name="Mobile"></param>
		/// <param name="yzm"></param>
		/// <param name="ForwardCode"></param>
		/// <param name="DataSource"></param>
		public GetTicketCommand(string Mobile, string yzm, string ForwardCode,int DataSource):base(DataSource)
		{
			this.Mobile = Mobile;
			this.Yzm = yzm;
			this.ForwardCode = ForwardCode;
		}
	}
}
