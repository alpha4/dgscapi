﻿using System;
using Casamiel.Domain;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands.MemberTicket
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class TicketForwardCommand : BaseCommand, IRequest<BaseResult<MemberTicketGive>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; private set; }

		/// <summary>
		///券ID
		/// </summary>
		public long Ticketid { get; private set; }

		/// <summary>
		/// 券名称
		/// </summary>
		public string TicketName { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public decimal Je { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string Message { get; private set; }

		/// <summary>
		/// /
		/// </summary>
		/// <param name="Mobile"></param>
		/// <param name="Ticketid"></param>
		/// <param name="TicketName"></param>
		/// <param name="je"></param>
		/// <param name="Message"></param>
		/// <param name="DataSource"></param>
		public TicketForwardCommand(string Mobile,long Ticketid, string TicketName,decimal je, string Message,int DataSource):base(DataSource)
		{
			this.TicketName = TicketName;
			this.Ticketid = Ticketid;
			this.Je = je;
			
			this.Mobile = Mobile;
			this.Message = Message;
		}
	}
}