﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Pre;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.Pre
{
	/// <summary>
	/// 预点单门店列表
	/// </summary>
	public class GetStoreListCommandHandler:BaseCommandHandler, IRequestHandler<GetStoreListCommand, PagingResultRsp<List<GetStoreListResponse>>>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
		public GetStoreListCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
		{
			 
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public async Task<PagingResultRsp<List<GetStoreListResponse>>> Handle(GetStoreListCommand request, CancellationToken cancellationToken)
		{
			var url = "Pred/v3/Store/GetStoreList";
			var result = await PostThirdApiWithPagingAsync<List<GetStoreListResponse>>(url, JsonConvert.SerializeObject(request)).ConfigureAwait(false);

			return result;
		}
	}
}
