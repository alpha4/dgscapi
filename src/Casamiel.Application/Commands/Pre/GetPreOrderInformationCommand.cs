﻿using System;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Pre;
using MediatR;

namespace Casamiel.Application.Commands.Pre
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetPreOrderInformationCommand: IRequest<BaseResult<OrderInformationReponse>>
	{
		 public int StoreId { get; set; }
	}
}
