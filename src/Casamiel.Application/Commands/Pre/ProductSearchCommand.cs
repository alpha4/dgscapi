﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands.Pre
{
	/// <summary>
	/// 产品查找
	/// </summary>
	public class ProductSearchCommand:IRequest<BaseResult<List<TakeOutProductBaseRsp>>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string KeyWord { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int StoreId { get; set; }
	}
}
