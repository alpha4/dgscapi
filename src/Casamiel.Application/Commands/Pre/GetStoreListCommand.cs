﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Pre;

namespace Casamiel.Application.Commands.Pre
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class GetStoreListCommand:BaseCommand,IRequest<PagingResultRsp<List<GetStoreListResponse>>>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DataSource"></param>
       public  GetStoreListCommand(int DataSource) : base(DataSource) { }
        /// <summary>
        /// 
        /// </summary>
        public string StoreName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double Lat { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double Lng { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; }

    }
}
