﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Pre;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.Pre
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetPreOrderInformationCommandHandler : BaseCommandHandler, IRequestHandler<GetPreOrderInformationCommand, BaseResult<OrderInformationReponse>>
	{



		/// <summary>
		/// 
		/// </summary>
		/// <param name="mbrStoreRepository"></param>
		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
		public GetPreOrderInformationCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
		{

		}

		public async Task<BaseResult<OrderInformationReponse>> Handle(GetPreOrderInformationCommand request, CancellationToken cancellationToken)
		{
			if (request is null) {
				throw new ArgumentNullException(nameof(request));
			}

			var url = "Pred/v3/Order/GetPrepare";

			var result = await PostThirdApiAsync<OrderInformationReponse>(url, JsonConvert.SerializeObject(request)).ConfigureAwait(false);

			return result;
		}
	}
}
