﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;

using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.Pre
{
	public class ProductSearchCommandHandler: BaseCommandHandler, IRequestHandler<ProductSearchCommand, BaseResult<List<TakeOutProductBaseRsp>>>
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mbrStoreRepository"></param>
        /// <param name="netICService"></param>
        /// <param name="httpClientFactory"></param>
        /// <param name="settings"></param>
        public ProductSearchCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
             
        }
        public async Task<BaseResult<List<TakeOutProductBaseRsp>>> Handle(ProductSearchCommand request, CancellationToken cancellationToken)
		{
			if (request is null) {
				throw new ArgumentNullException(nameof(request));
			}

			var url = "Pred/v3/Goods/GetListByName";
            var data = new { request.KeyWord, request.StoreId};
            var result = await PostThirdApiAsync<List<TakeOutProductBaseRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            
            return result;
        }
	}
}
