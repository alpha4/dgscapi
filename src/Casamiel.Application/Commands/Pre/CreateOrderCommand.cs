﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Request;
using Casamiel.Domain.Request.Waimai;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands.Pre
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class CreateOrderCommand: BaseCommand, IRequest<BaseResult<PaymentRsp>>
	{
        public CreateOrderCommand(int DataSource) : base(DataSource) { }
        /// <summary>
        /// IC交易号
        /// </summary>
        public string IcTradeNO { get; set; }
        /// <summary>
        /// 销售号
        /// </summary>
        public string BillNo { get; set; }
		/// <summary>
		/// 商品信息
		/// </summary>
#pragma warning disable CA2227 // 集合属性应为只读
		public List<TakeOutOrderGoods> GoodsList { get; set; }
#pragma warning restore CA2227 // 集合属性应为只读

		/// <summary>
		/// 门店id
		/// </summary>
		public int StoreId { get; set; }

        /// <summary>
        /// 支付方式 :0 默认   1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
        /// </summary>
        public int Paymethod { get; set; }

        /// <summary>
		/// 支付卡号
		/// </summary>
        public string PayCardNO { get; set; }

        /// <summary>
        /// 手机号 用户
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 联系人
        /// </summary>
        public string ContactName { get; set; }
        /// <summary>
        /// 联系方式
        /// </summary>
        public string ContactPhone { get; set; }

        /// <summary>
        /// 备注信息
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 发票id
        /// </summary>
        public int InvoiceId { get; set; }

        /// <summary>
        /// 减免金额
        /// </summary>
        public decimal Rebate { get; set; }

        /// <summary>
		/// 
		/// </summary>
        public  int Id { get; set; }

		/// <summary>
		/// 优惠券信息
		/// </summary>
#pragma warning disable CA2227 // 集合属性应为只读
		public List<OrderDiscountReq> DiscountList { get; set; }
#pragma warning restore CA2227 // 集合属性应为只读

	}
}
