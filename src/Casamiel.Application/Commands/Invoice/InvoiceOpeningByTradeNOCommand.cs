﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Entity;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class InvoiceOpeningByTradeNOCommand:IRequest<bool>
    {
        /// <summary>
        /// 
        /// </summary>
        public string TradeNO { get; set; }
    }
}
