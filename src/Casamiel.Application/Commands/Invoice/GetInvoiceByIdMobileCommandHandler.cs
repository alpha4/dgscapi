﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get invoice by identifier mobile command handler.
    /// </summary>
    public class GetInvoiceByIdMobileCommandHandler:IRequestHandler<GetInvoiceByIdMobileCommand,BaseResult<BaseInvoice>>
    {
        private readonly IInvoiceRepository _invoiceRepository;
        public GetInvoiceByIdMobileCommandHandler(IInvoiceRepository invoiceRepository)
        {
            _invoiceRepository = invoiceRepository;
        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<BaseResult<BaseInvoice>> Handle(GetInvoiceByIdMobileCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var entity = await _invoiceRepository.GetKeyAsync<ICasaMielSession>(request.Id).ConfigureAwait(false);
            if(entity!=null && entity.Mobile == request.Mobile)
            {
                return new BaseResult<BaseInvoice>(entity, 0, "");
            }
            return new BaseResult<BaseInvoice>(null, 0, "");
        }
    }
}
