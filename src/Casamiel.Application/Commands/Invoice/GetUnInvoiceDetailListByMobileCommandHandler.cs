﻿using System;
using MediatR;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Response;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Domain;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get un invoice detail list by mobile command handler.
    /// </summary>
    public sealed class GetUnInvoiceDetailListByMobileCommandHandler:IRequestHandler<GetUnInvoiceDetailListByMobileCommand, PagingResultRsp<List<InvoiceDetail>>>
    {
        private readonly IInvoiceDetailRepository _invoiceDetailRepository;
        /// <summary>
        /// Initializes a new instance of the
        ///  
        /// </summary>
        /// <param name="invoiceDetailRepository">Invoice detail repository.</param>
        public GetUnInvoiceDetailListByMobileCommandHandler(IInvoiceDetailRepository invoiceDetailRepository)
        {
            _invoiceDetailRepository = invoiceDetailRepository;
        }

        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<PagingResultRsp<List<InvoiceDetail>>> Handle(GetUnInvoiceDetailListByMobileCommand request, CancellationToken cancellationToken)
        {
            if(request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var list = await _invoiceDetailRepository.GetListAsync<ICasaMielSession>(request.Mobile, request.CreateTime,request.PageIndex,request.PageSize).ConfigureAwait(false);
            return new PagingResultRsp<List<InvoiceDetail>>(list.Data,list.PageIndex,list.PageSize,list.Total,0,"");
        }
    }
}
