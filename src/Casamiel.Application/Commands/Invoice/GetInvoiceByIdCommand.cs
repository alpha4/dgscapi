﻿using System;
using MediatR;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get invoice by identifier mobile command.
    /// </summary>
    public class GetInvoiceByIdMobileCommand:IRequest<BaseResult<BaseInvoice>>
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public long Id { get; set; }
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public  string Mobile { get; private set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.GetInvoiceByIdMobileCommand"/> class.
        /// </summary>
        /// <param name="Id">Identifier.</param>
        /// <param name="Mobile">Mobile.</param>
        public GetInvoiceByIdMobileCommand(long Id,string Mobile)
        {
            this.Id = Id;
            this.Mobile = Mobile;
        }
    }
}
