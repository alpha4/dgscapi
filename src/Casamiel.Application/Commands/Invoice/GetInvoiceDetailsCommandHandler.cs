﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 发票详情
    /// </summary>
    public sealed class GetInvoiceDetailsCommandHandler : IRequestHandler<GetInvoiceDetailsCommand, BaseResult<List<InvoiceDetail>>>
    {
        // Fields
        private readonly IInvoiceDetailRepository _detailRepository;

        /// <summary>
        /// Initializes a new instance of the
        /// 
        /// </summary>
        /// <param name="detailRepository">Detail repository.</param>
        public GetInvoiceDetailsCommandHandler(IInvoiceDetailRepository detailRepository)
        {
            this._detailRepository = detailRepository;
        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<BaseResult<List<InvoiceDetail>>> Handle(GetInvoiceDetailsCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var list = await _detailRepository.GetListAsync<ICasaMielSession>(request.InvoiceID,request.Mobile).ConfigureAwait(false);
            return new BaseResult<List<InvoiceDetail>>(list, 0, "");
        }
    }
}
