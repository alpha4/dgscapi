﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 发票详情
    /// </summary>
    public sealed  class GetInvoiceDetailsCommand : IRequest<BaseResult<List<InvoiceDetail>>>, IBaseRequest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.GetInvoiceDetailsCommand"/> class.
        /// </summary>
        /// <param name="InvoiceID">Invoice identifier.</param>
        /// <param name="Mobile">Mobile.</param>
        public GetInvoiceDetailsCommand(long InvoiceID, string Mobile)
        {
            this.InvoiceID = InvoiceID;
            this.Mobile = Mobile;
        }

        /// <summary>
        /// Gets the invoice identifier.
        /// </summary>
        /// <value>The invoice identifier.</value>
        public long InvoiceID { get; private set; }
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
    }
}
