﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get un invoice detail list by mobile command.
    /// </summary>
    public class GetUnInvoiceDetailListByMobileCommand : IRequest<PagingResultRsp<List<InvoiceDetail>>>, IBaseRequest
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.GetUnInvoiceDetailListByMobileCommand"/> class.
        /// </summary>
        /// <param name="Mobile">Mobile.</param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        public GetUnInvoiceDetailListByMobileCommand(string Mobile,int PageIndex,int PageSize)
        {
            this.Mobile = Mobile;
            this.CreateTime = DateTime.Now.AddDays(-90.0);
            this.PageSize = PageSize;
            this.PageIndex = PageIndex;
        }

        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
        /// <summary>
        /// Gets the create time.
        /// </summary>
        /// <value>The create time.</value>
        public DateTime CreateTime { get; private set; }
        /// <summary>
        /// Gets the index of the page.
        /// </summary>
        /// <value>The index of the page.</value>
        public int PageIndex { get; private set;}
        /// <summary>
        /// Gets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; private set; }
    }


}
