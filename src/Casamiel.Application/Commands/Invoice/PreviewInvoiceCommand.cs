﻿using System;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Preview invoice command.
    /// </summary>
    public class PreviewInvoiceCommand:IRequest<BaseResult<PreviewInvoiceRsp>>
    {
        /// <summary>
        /// Gets the identifiers.
        /// </summary>
        /// <value>The identifiers.</value>
        public string IDS { get; private set; }
        public string  Mobile { get; private set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.PreviewInvoiceCommand"/> class.
        /// </summary>
        /// <param name="Ids">Identifiers.</param>
        /// <param name="Mobile">Mobile.</param>
        public PreviewInvoiceCommand(string Ids,string Mobile)
        {
            this.IDS = Ids;
            this.Mobile = Mobile;
        }
    }
}
