﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Create invoice command.
    /// </summary>
    public class CreateInvoiceCommand : IRequest<BaseResult<string>>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.CreateInvoiceCommand"/> class.
        /// </summary>
        /// <param name="InvoiceType">Invoice type.</param>
        /// <param name="IDs">Identifier.</param>
        /// <param name="BuyerName">Buyer name.</param>
        /// <param name="TaxNum">Tax number.</param>
        /// <param name="Address">Address.</param>
        /// <param name="Account">Account.</param>
        /// <param name="SmsMobile">Sms mobile.</param>
        /// <param name="Email">Email.</param>
        /// <param name="Mobile">Mobile.</param>
        public CreateInvoiceCommand(int InvoiceType,string IDs,string BuyerName, string TaxNum,string Address, string Account, string SmsMobile, string Email,string Mobile)
        {
            this.IDS = IDs;
            this.Address = Address;
            this.BuyerName = BuyerName;
            this.Account = Account;
            this.SmsMobile = SmsMobile;
            this.Email = Email;
            this.TaxNum = TaxNum;
            this.Mobile = Mobile;
            this.InvoiceType = InvoiceType;
        }

        /// <summary>
        /// Gets the type of the invoice.
        /// </summary>
        /// <value>The type of the invoice.</value>
        public int InvoiceType { get; private set; }
        /// <summary>
        /// Gets the tax number.
        /// </summary>
        /// <value>The tax number.</value>
        public string TaxNum { get; private set; }
        /// <summary>
        /// Gets or sets the identifiers.
        /// </summary>
        /// <value>The identifiers.</value>
        public string IDS { get; private set; }

        public string Mobile { get; private set; }
        /// <summary>
        /// Gets or sets the order no.
        /// </summary>
        /// <value>The order no.</value>

        /// <summary>
        /// Gets the account.
        /// </summary>
        /// <value>The account.</value>
        public string Account { get; private set; }
        /// <summary>
        /// Gets the address.
        /// </summary>
        /// <value>The address.</value>
        public string Address { get; private set; }
        /// <summary>
        /// Gets the name of the buyer.
        /// </summary>
        /// <value>The name of the buyer.</value>
        public string BuyerName { get; private set; }
        /// <summary>
        /// Gets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; private set; }
        /// <summary>
        /// Gets the sms mobile.
        /// </summary>
        /// <value>The sms mobile.</value>
        public string SmsMobile
        {
            get; private set;
        }
    }
}
