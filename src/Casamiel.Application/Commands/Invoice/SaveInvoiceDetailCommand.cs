﻿using System;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Save invoice detail command.
    /// </summary>
    public class SaveInvoiceDetailCommand : IRequest<bool>, IBaseRequest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.SaveInvoiceDetailCommand"/> class.
        /// </summary>
        /// <param name="Mobile">Mobile.</param>
        /// <param name="Amount">Amount.</param>
		/// <param name="OutTradeNO"></param>
        public SaveInvoiceDetailCommand(string Mobile, decimal Amount,string OutTradeNO)
        {
            this.Mobile = Mobile;
            this.Amount = Amount;
			this.OutTradeNO = OutTradeNO;

		}
		public string OutTradeNO { get;private set; }
		/// <summary>
		/// Gets the amount.
		/// </summary>
		/// <value>The amount.</value>
		public decimal Amount { get; private set; }
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
    }

}
