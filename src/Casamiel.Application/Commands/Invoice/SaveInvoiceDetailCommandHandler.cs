﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Casamiel.Domain;
using Casamiel.Domain.Entity;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Save invoice detail command handler.
    /// </summary>
    public sealed class SaveInvoiceDetailCommandHandler : IRequestHandler<SaveInvoiceDetailCommand, bool>
    {
        private readonly IInvoiceDetailRepository _invoiceDetailRepository;
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.SaveInvoiceDetailCommandHandler"/> class.
        /// </summary>
        /// <param name="invoiceDetailRepository">Invoice detail repository.</param>
        public SaveInvoiceDetailCommandHandler(IInvoiceDetailRepository invoiceDetailRepository)
        {
            _invoiceDetailRepository = invoiceDetailRepository;
        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<bool> Handle(SaveInvoiceDetailCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var entity = new InvoiceDetail
            {
                Amount = request.Amount,
                CreateTime = DateTime.Now,
                Mobile = request.Mobile,
                Status = 1,
				OutTradeNO=request.OutTradeNO
            };
            await _invoiceDetailRepository.SaveAsync<ICasaMielSession>(entity).ConfigureAwait(false);
            return true;
        }
    }
}
