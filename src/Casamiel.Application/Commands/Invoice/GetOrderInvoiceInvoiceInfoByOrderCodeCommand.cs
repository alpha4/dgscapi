﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Invoice;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class GetOrderInvoiceInvoiceInfoByOrderCodeCommand:IRequest<BaseResult<OrderInvoiceInformationRsp>>
    {
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderCode { get; set; }
    }
}
