﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.GroupPurchase;
using MediatR;

namespace Casamiel.Application.Commands.GroupPurchase
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class MyOrderListCommand:BaseCommand,IRequest<PagingResultRsp<List<GrouponPurchaseOrderResponse>>>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="DataSource"></param>
		public MyOrderListCommand(int DataSource) : base(DataSource) { }
		/// <summary>
		/// 会员手机号
		/// </summary>
		public string Mobile { get; set; }
		 
		/// <summary>		/// 页码		/// </summary>        public int PageIndex { get; set; }

		 
		/// <summary>		/// 每页数量		/// </summary>        public int PageSize { get; set; }
	}
}
