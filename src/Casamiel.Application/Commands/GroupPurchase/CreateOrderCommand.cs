﻿using System;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.GroupPurchase;
using MediatR;

namespace Casamiel.Application.Commands.GroupPurchase
{

	/// <summary>
	/// 
	/// </summary>
	public sealed class CreateOrderCommand:BaseCommand,IRequest<BaseResult<CreateOrderRsp>>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="DataSource"></param>
		public CreateOrderCommand(int DataSource) : base(DataSource) { }
		/// <summary>		/// 拼团类型 1 单独购买 2 我要开团 3 我要参团		/// </summary>
		public int OrderType { get; set; }

		 
		/// <summary>		/// 会员头像（主要是微信有）		/// </summary>        public string HeadImgurl { get; set; }


		/// <summary>
		/// 
		/// </summary>

		public string Mobile { get; set; }



		/// <summary>		/// 活动id		/// </summary>        public int GrouponId { get; set; }

 
		 /// <summary>		/// 数量		/// </summary>        public int Quantity { get; set; }
		 
		/// <summary>		/// 备注		/// </summary>        public string Remark { get; set; }





		/// <summary>
		/// 支付方式 :  1微信app支付，2支付宝app支付,4，小程序微信支付
		/// </summary>
		public int Paymethod { get; set; }



		

		 
		/// <summary>		/// 发票id		/// </summary>        public int InvoiceId { get; set; }







		/// <summary>		/// 开团订单号		/// </summary>        public string LeaderCode { get; set; }
	}
}
