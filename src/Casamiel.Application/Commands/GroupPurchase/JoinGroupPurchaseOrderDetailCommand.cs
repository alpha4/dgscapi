﻿using System;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.GroupPurchase;

namespace Casamiel.Application.Commands.GroupPurchase
{
	/// <summary>
	/// 参与拼团详情
	/// </summary>
	public sealed class JoinGroupPurchaseOrderDetailCommand:BaseCommand,IRequest<BaseResult<JoinGroupPurchseOrderResponse>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string OrderCode { get; private set; }
		public JoinGroupPurchaseOrderDetailCommand(string OrderCode,int DataSource) : base(DataSource) { 
			this.OrderCode = OrderCode;
		}
	}
}
