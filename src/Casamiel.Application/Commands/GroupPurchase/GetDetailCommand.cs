﻿using System;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.GroupPurchase;
using MediatR;

namespace Casamiel.Application.Commands.GroupPurchase
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetDetailCommand:BaseCommand,IRequest<BaseResult<GrouponActivityDetailResponse>>
	{
		/// <summary>
		/// 
		/// </summary>
		public int GrouponId { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="GrouponId"></param>	
		/// <param name="DataSource"></param>
		public GetDetailCommand(int GrouponId,int DataSource):base(DataSource)
		{
			this.GrouponId = GrouponId;
		}
	}
}
