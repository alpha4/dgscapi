﻿using System;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.GroupPurchase;
using MediatR;

namespace Casamiel.Application.Commands.GroupPurchase
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetOrderDetailCommand:BaseCommand,IRequest<BaseResult<GroupPurchaseOrderDetailResponse>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string OrderCode { get; private set; }


		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="OrderCode"></param>
		/// <param name="Mobile"></param>
		/// <param name="DataSource"></param>
		public GetOrderDetailCommand(string OrderCode,string Mobile,int DataSource):base(DataSource)
		{
			this.OrderCode = OrderCode;
			this.Mobile = Mobile;
		}
	}
}
