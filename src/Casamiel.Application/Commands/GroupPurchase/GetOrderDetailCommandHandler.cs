﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.GroupPurchase;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.GroupPurchase
{
	/// <summary>
	/// 拼团订单详情
	/// </summary>
	public sealed class GetOrderDetailCommandHandler:BaseCommandHandler,IRequestHandler<GetOrderDetailCommand, BaseResult<GroupPurchaseOrderDetailResponse>>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
		public GetOrderDetailCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) :
			base(netICService, httpClientFactory, settings)
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public async Task<BaseResult<GroupPurchaseOrderDetailResponse>> Handle(GetOrderDetailCommand request, CancellationToken cancellationToken)
		{
			if (request is null) {
				throw new ArgumentNullException(nameof(request));
			}

			var url = $"v3/Activity/GetGrouponOrderDetail";
			var data = new { request.Mobile, request.OrderCode};
			var result = await PostThirdApiAsync<GroupPurchaseOrderDetailResponse>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
			return result;
		}
	}
}
