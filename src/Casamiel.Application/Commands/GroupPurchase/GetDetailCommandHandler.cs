﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.GroupPurchase;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.GroupPurchase
{
	/// <summary>
	/// 拼团活动详情
	/// </summary>
	public sealed class GetDetailCommandHandler : BaseCommandHandler, IRequestHandler<GetDetailCommand, BaseResult<GrouponActivityDetailResponse>>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
		public GetDetailCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) :
			base(netICService, httpClientFactory, settings)
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public async Task<BaseResult<GrouponActivityDetailResponse>> Handle(GetDetailCommand request, CancellationToken cancellationToken)
		{
			if (request == null) {
				throw new ArgumentNullException(nameof(request));
			}
			var url = $"v3/Activity/GetGrouponDetail";
			var data = new { request.GrouponId };
			var result = await PostThirdApiAsync<GrouponActivityDetailResponse>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
			return result;

		}
	}
}
