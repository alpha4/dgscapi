﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.GroupPurchase;
using MediatR;

namespace Casamiel.Application.Commands.GroupPurchase
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetGroupActivityListCommand:BaseCommand,IRequest<BaseResult<List<GroupActivityResponse>>>
	{

		/// <summary>
		/// 平台：0小程序，1App
		/// </summary>
		public int Platform { get; private set; }


		 /// <summary>
		 /// 
		 /// </summary>
		public int Flag { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="Platform"></param>
		/// <param name="Source"></param>
		public GetGroupActivityListCommand(int Platform, int Flag) : base(1)
		{
			this.Platform = Platform;
			this.Flag = Flag;
		}
	}
}
