﻿using System;
using MediatR;
using Casamiel.Domain.Response;


namespace Casamiel.Application.Commands.GroupPurchase
{
	/// <summary>
	/// 
	/// </summary>
	public class SendTicketCommand:BaseCommand,IRequest<BaseResult<string>>
	{

		/// <summary>
		/// 订单号
		/// </summary>
		public string OrderCode { get; private set; }

		/// <summary>
		/// 1立即送、2成团送
		/// </summary>
		public int Flag { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="OrderCode"></param>
		/// <param name="Flag"></param>
		/// <param name="DataSource"></param>
		public SendTicketCommand(string OrderCode,int Flag,int DataSource):base(DataSource)
		{
			this.OrderCode = OrderCode;
			this.Flag = Flag;
		}
	}
}
