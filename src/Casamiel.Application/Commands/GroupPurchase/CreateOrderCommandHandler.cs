﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.GroupPurchase;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.GroupPurchase
{
	/// <summary>
	/// 创建拼团订单
	/// </summary>
	public sealed class CreateOrderCommandHandler:BaseCommandHandler,IRequestHandler<CreateOrderCommand,BaseResult<CreateOrderRsp>>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
		public CreateOrderCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) :
			base(netICService, httpClientFactory, settings)
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public async Task<BaseResult<CreateOrderRsp>> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
		{
			if (request is null) {
				throw new ArgumentNullException(nameof(request));
			}

			var url = $"v3/Activity/CreateGrouponOrder";
			 
			var result = await PostThirdApiAsync<CreateOrderRsp>(url, JsonConvert.SerializeObject(request)).ConfigureAwait(false);
			return result;
		}
	}
}
