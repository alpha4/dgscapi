﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.GroupPurchase;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.GroupPurchase
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class JoinGroupPurchaseOrderDetailCommandHandler:BaseCommandHandler,IRequestHandler<JoinGroupPurchaseOrderDetailCommand, BaseResult<JoinGroupPurchseOrderResponse>>
	{
		public JoinGroupPurchaseOrderDetailCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) :
			base(netICService, httpClientFactory, settings)
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public async Task<BaseResult<JoinGroupPurchseOrderResponse>> Handle(JoinGroupPurchaseOrderDetailCommand request, CancellationToken cancellationToken)
		{
			if (request is null) {
				throw new ArgumentNullException(nameof(request));
			}

			var url = $"v3/Activity/JoinGroupon";
			var data = new {  request.OrderCode };
			var result = await PostThirdApiAsync<JoinGroupPurchseOrderResponse>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
			return result;
		}
	}
}
