﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.GroupPurchase;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.GroupPurchase
{
	/// <summary>
	/// 我的拼团订单列表
	/// </summary>
	public sealed class MyOrderListCommandHandler : BaseCommandHandler, IRequestHandler<MyOrderListCommand, PagingResultRsp<List<GrouponPurchaseOrderResponse>>>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
		public MyOrderListCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) :
			base(netICService, httpClientFactory, settings)
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public async Task<PagingResultRsp<List<GrouponPurchaseOrderResponse>>> Handle(MyOrderListCommand request, CancellationToken cancellationToken)
		{
			if (request == null) {
				throw new ArgumentNullException(nameof(request));
			}
			var url = "v3/Activity/GetMyGrouponOrderList";
			var data = new {
				 request.Mobile,request.PageIndex,request.PageSize
			};
			var rsp = await PostThirdApiWithPagingAsync<List<GrouponPurchaseOrderResponse>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
			return rsp;
		}
	}
}