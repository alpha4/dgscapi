﻿using System;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Product get simple detail command.
    /// </summary>
    public sealed class ProductGetSimpleDetailCommand:BaseCommand, IRequest<BaseResult<SimpleGoodsBaseRsp>>
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DataSource"></param>
        public ProductGetSimpleDetailCommand(int DataSource) : base(DataSource) { }
        /// <summary>
        /// Gets or sets the productid.
        /// </summary>
        /// <value>The productid.</value>
        public int Productid { get; set; }
    }
}
