﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Common.Extensions;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get product collect list command handler.
    /// </summary>
    public sealed class GetProductCollectListCommandHandler:BaseCommandHandler,IRequestHandler<GetProductCollectListCommand,PagingResultRsp<List<GoodsCollectRsp>>>
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.GetProductCollectListCommandHandler"/> class.
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public GetProductCollectListCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
         
        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<PagingResultRsp<List<GoodsCollectRsp>>> Handle(GetProductCollectListCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "CakeMall/v1/Goods/GetCollectList";
            var data = new { request.PageIndex, request.PageSize, request.Mobile };
            var result = await PostThirdApiWithPagingAsync<List<GoodsCollectRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
            //var rsp = JsonConvert.DeserializeObject<JObject>(result);

            //var total = 0;
            //if (rsp["resultNo"].ToString() == "00000000")
            //{
            //    total = rsp["total"].ToString().ToInt32(0);
            //    var list = JsonConvert.DeserializeObject<List<Goods_CollectRsp>>(rsp["data"].ToString());
            //    return new PagingResultRsp<List<Goods_CollectRsp>>(list, request.PageIndex, request.PageSize, total, 0, "");//
            //}
            //else
            //{
            //    return new PagingResultRsp<List<Goods_CollectRsp>>(null, request.PageIndex, request.PageSize, total,  9999, rsp["resultRemark"].ToString());
            //}
        }
    }
}
