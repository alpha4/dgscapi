﻿using System;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Product check collect command.
    /// </summary>
    public class ProductCheckCollectCommand:IRequest<bool>
    {
        /// <summary>
        /// Gets the product base identifier.
        /// </summary>
        /// <value>The product base identifier.</value>
        public int ProductBaseId { get; private set; }
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.AddProductCollectCommand"/> class.
        /// </summary>
        /// <param name="ProductBaseId">Product base identifier.</param>
        /// <param name="Mobile">Mobile.</param>
        public ProductCheckCollectCommand(int ProductBaseId, string Mobile)
        {
            this.ProductBaseId = ProductBaseId;
            this.Mobile = Mobile;
        }
    }
}
