﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{

    /// <summary>
    /// Get product stock list by store identifier command.
    /// </summary>
    public class GetProductStockListByStoreIdCommand:IRequest<BaseResult<List<GoodsGetListRsp>>>
    {
        public int  StoreId { get; private set; }
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.GetProductStockListByStoreIdCommand"/> class.
        /// </summary>
        /// <param name="StoreId">Product list.</param>
        public GetProductStockListByStoreIdCommand(int StoreId)
        {
            this.StoreId = StoreId;
        }
    }
}
