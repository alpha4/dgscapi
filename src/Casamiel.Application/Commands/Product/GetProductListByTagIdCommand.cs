﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 获取蛋糕商城分类下产品列表
    /// </summary>
    public sealed class GetProductListByTagIdCommand : IRequest<PagingResultRsp<List<ProductBaseRsp>>>
    {
        /// <summary>
        /// 
        /// </summary>
        public int TagBaseId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int StoreId { get; set; }
    }

}
