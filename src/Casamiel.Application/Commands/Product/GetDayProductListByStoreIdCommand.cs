﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 今日达
    /// </summary>
    public sealed   class GetDayProductListByStoreIdCommand:IRequest<BaseResult<List<ProductBaseRsp>>>
    {
        public int StoreId { get; private set; }
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.GetProductStockListByStoreIdCommand"/> class.
        /// </summary>
        /// <param name="StoreId">Product list.</param>
        public GetDayProductListByStoreIdCommand(int StoreId)
        {
            this.StoreId = StoreId;
        }
    }
}
