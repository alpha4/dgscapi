﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Product cancel collect command handler.
    /// </summary>
    public sealed class ProductCancelCollectCommandHandler : BaseCommandHandler, IRequestHandler<ProductCancelCollectCommand, BaseResult<string>>
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.ProductCancelCollectCommandHandler"/> class.
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public ProductCancelCollectCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<BaseResult<string>> Handle(ProductCancelCollectCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "CakeMall/v1/Goods/CancelCollect";
            var data = new { request.ProductBaseId, request.Mobile };
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
    }
}
