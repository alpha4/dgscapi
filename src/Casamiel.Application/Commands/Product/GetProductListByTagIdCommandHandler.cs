﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Casamiel.Domain;
using Casamiel.Common.Extensions;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class GetProductListByTagIdCommandHandler:BaseCommandHandler,IRequestHandler<GetProductListByTagIdCommand, PagingResultRsp<List<ProductBaseRsp>>>
    {
        private readonly IMbrStoreRepository _mbrStoreRepository;
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mbrStoreRepository"></param>
        /// <param name="netICService"></param>
        /// <param name="httpClientFactory"></param>
        /// <param name="settings"></param>
        public GetProductListByTagIdCommandHandler(IMbrStoreRepository mbrStoreRepository,INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
            _mbrStoreRepository = mbrStoreRepository;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PagingResultRsp<List<ProductBaseRsp>>> Handle(GetProductListByTagIdCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var storeinfo = await _mbrStoreRepository.GetKeyAsync<ICasaMielSession>(request.StoreId).ConfigureAwait(false);
            if (storeinfo == null)
            {
                return new PagingResultRsp<List<ProductBaseRsp>>(null,request.PageIndex,request.PageSize,0, 9999, "门店信息有误！");
            }
			if (storeinfo.IsCakeShopClose == 1) {
				return new PagingResultRsp<List<ProductBaseRsp>>(null, request.PageIndex, request.PageSize, 0, 9999, "抱歉门店已闭店！");
			}
			if (storeinfo.IsCakeClose == 1)
            {
                return new PagingResultRsp<List<ProductBaseRsp>>(null, request.PageIndex, request.PageSize, 0, 9999, "抱歉门店未营业！");
            }
            

            var url = "CakeMall/v1/Goods/GetList";
            var data = new { request.PageIndex, request.PageSize, request.TagBaseId,request.StoreId };
            var rsp = await PostThirdApiWithPagingAsync<List<ProductBaseRsp >> (url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return rsp;
            //if (rsp.IsSuccess)
            //{
            //    return new PagingResultRsp<List<ProductBaseRsp>>(rsp.Data, request.PageIndex, request.PageSize, rsp.Total, 0, "");
            //}
            //return new PagingResultRsp<List<ProductBaseRsp>>(null, request.PageIndex, request.PageSize,0, 9999, rsp.ResultRemark);
            //var rsp = JsonConvert.DeserializeObject<JObject>(result);

            //var total = 0;
            //if (rsp["resultNo"].ToString() == "00000000")
            //{
            //    total = rsp["total"].ToString().ToInt32(0);
            //    var list = JsonConvert.DeserializeObject<List<ProductBaseRsp>>(rsp["data"].ToString());
            //    return new PagingResultRsp<List<ProductBaseRsp>>(list, request.PageIndex, request.PageSize, total, 0, "");//
            //}
            //else
            //{
            //    return new PagingResultRsp<List<ProductBaseRsp>>(null, request.PageIndex, request.PageSize, total, 9999, rsp["resultRemark"].ToString());
            //}
        }
    }
}
