﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Product get simple detail command handler.
    /// </summary>
    public sealed class ProductGetSimpleDetailCommandHandler : BaseCommandHandler, IRequestHandler<ProductGetSimpleDetailCommand, BaseResult<SimpleGoodsBaseRsp>>
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.ProductGetSimpleDetailCommandHandler"/> class.
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public ProductGetSimpleDetailCommandHandler( INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
       
        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<BaseResult<SimpleGoodsBaseRsp>> Handle(ProductGetSimpleDetailCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "MWeb/v1/Goods/GetDetail";
            var data = new { id = request.Productid };
            var rsp = await PostThirdApiAsync<SimpleGoodsBaseRsp>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return rsp;
        }
    }
}
