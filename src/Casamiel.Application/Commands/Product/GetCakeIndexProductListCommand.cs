﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 获取蛋糕商城首页商品数据
    /// </summary>
    public sealed  class GetCakeIndexProductListCommand:IRequest<BaseResult<List<IndexProductBaseRsp>>>
    {
        /// <summary>
        /// 
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 获取蛋糕商城首页商品数据
        /// </summary>
        public GetCakeIndexProductListCommand() { }
    }
}
