﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public sealed   class GetCakeIndexProductListCommandHandler:BaseCommandHandler,IRequestHandler<GetCakeIndexProductListCommand, BaseResult<List<IndexProductBaseRsp>>>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="netICService"></param>
        /// <param name="httpClientFactory"></param>
        /// <param name="settings"></param>
        public GetCakeIndexProductListCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<BaseResult<List<IndexProductBaseRsp>>> Handle(GetCakeIndexProductListCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "CakeMall/v1/Index/GetGoodsList";

            var rsp = await PostAsync<ThirdResult<List< IndexProductBaseRsp>>>(url, JsonConvert.SerializeObject(new { storeId=request.StoreId })).ConfigureAwait(false);
          //  var rsp = JsonConvert.DeserializeObject<JObject>(result);

            if (rsp.IsSuccess)
            {
                return new BaseResult<List<IndexProductBaseRsp>>(rsp.Data, 0, "");//
            }
            else
            {
                return new BaseResult<List<IndexProductBaseRsp>>(null, 9999, rsp.ResultRemark);
            }
        }
    }
}
