﻿using System;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Product cancel collect command.
    /// </summary>
    public class ProductCancelCollectCommand : IRequest<BaseResult<string>>
    {
        /// <summary>
        /// Gets the product base identifier.
        /// </summary>
        /// <value>The product base identifier.</value>
        public int ProductBaseId { get; private set; }
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.AddProductCollectCommand"/> class.
        /// </summary>
        /// <param name="ProductBaseId">Product base identifier.</param>
        /// <param name="Mobile">Mobile.</param>
        public ProductCancelCollectCommand(int ProductBaseId, string Mobile)
        {
            this.ProductBaseId = ProductBaseId;
            this.Mobile = Mobile;
        }
    }
}
