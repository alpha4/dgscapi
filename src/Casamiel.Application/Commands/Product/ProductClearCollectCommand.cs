﻿using System;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Product clear collect commad.
    /// </summary>
    public class ProductClearCollectCommand:IRequest<BaseResult<string>>
    {
      
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
         /// <summary>
         /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.ProductClearCollectCommad"/> class.
         /// </summary>
         /// <param name="Mobile">Mobile.</param>
        public ProductClearCollectCommand(string Mobile)
        {
              this.Mobile = Mobile;
        }
    }
}
