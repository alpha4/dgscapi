﻿using System;
using System.Net.Http;
using System.Threading;
using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Product clear collect commad handler.
    /// </summary>
    public class ProductClearCollectCommadHandler : BaseCommandHandler, IRequestHandler<ProductClearCollectCommand, BaseResult<string>>
    {
        /// <summary>
        /// Initializes a new instance of the
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public ProductClearCollectCommadHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async System.Threading.Tasks.Task<BaseResult<string>> Handle(ProductClearCollectCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var url = "CakeMall/v1/Goods/ClearCollect";
            var data = new { request.Mobile };
            var result = await PostAsync(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            var rsp = JsonConvert.DeserializeObject<JObject>(result);
            if (rsp["resultNo"].ToString() != "00000000")
            {
                return new BaseResult<string>("", 9999, rsp["resultRemark"].ToString());
            }
            return new BaseResult<string>("", 0, "");
        }
    }
}
