﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Product get list by search name command.
    /// </summary>
    public sealed class ProductGetListBySearchNameCommand : IRequest<PagingResultRsp<List<ProductBaseRsp>>>
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }
        public int PageIndex { get; set; }
        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; set; }
        /// <summary>
        ///  0蛋糕订购 1 蜜儿优选
        /// </summary>
        /// <value>The platfrom.</value>
        public int Platfrom { get; set; }
        public ProductGetListBySearchNameCommand()
        {
        }
    }
}
