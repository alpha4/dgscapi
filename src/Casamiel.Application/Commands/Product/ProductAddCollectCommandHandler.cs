﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Domain.Response;
using MediatR;
using System.Net.Http;
using Microsoft.Extensions.Options;
using Casamiel.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Product add collect command handler.
    /// </summary>
    public sealed class ProductAddCollectCommandHandler:BaseCommandHandler,IRequestHandler<ProductAddCollectCommand,BaseResult<string>>
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.ProductAddCollectCommandHandler"/> class.
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public ProductAddCollectCommandHandler(INetICService netICService,IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) :base(netICService,httpClientFactory, settings)
        {
        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<BaseResult<string>> Handle(ProductAddCollectCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "CakeMall/v1/Goods/AddCollect";
            var data = new { request.ProductBaseId, request.Mobile };
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
            //var rsp = JsonConvert.DeserializeObject<JObject>(result);
            //if (rsp["resultNo"].ToString() != "00000000")
            //{
            //    BizInfologger.Error($"{url},{JsonConvert.SerializeObject(request)},{rsp["resultRemark"].ToString()}");
            //    return new BaseResult<string>("", 9999, rsp["resultRemark"].ToString());
            //}
            //return new BaseResult<string>("", 0, "");
        }
    }
}
