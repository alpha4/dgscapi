﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Product check collect command handler.
    /// </summary>
    public sealed class ProductCheckCollectCommandHandler:BaseCommandHandler, IRequestHandler<ProductCheckCollectCommand, bool>
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.ProductCheckCollectCommandHandler"/> class.
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public ProductCheckCollectCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<bool> Handle(ProductCheckCollectCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "CakeMall/v1/Goods/CheckCollect";
            var data = new { request.ProductBaseId, request.Mobile };
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            if (result.Code == 0)
            {
                return true;
            }
            //var rsp = JsonConvert.DeserializeObject<JObject>(result);
            //if (rsp["resultNo"].ToString() != "00000000")
            //{
            //    BizInfologger.Error($"{url},{JsonConvert.SerializeObject(request)},{rsp["resultRemark"].ToString()}");
            //    return false;
            //}
            return false;
        }
    }
}
