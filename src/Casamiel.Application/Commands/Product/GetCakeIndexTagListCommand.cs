﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Casamiel.Domain.Response;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 获取蛋糕商城首页标签列表
    /// </summary>
    public sealed class  GetCakeIndexTagListCommand:IRequest<BaseResult<List<IndexTagBaseRsp>>>
    {
        /// <summary>
        /// 
        /// </summary>
        public int StoreId { get; set; }
        
    }
}
