﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Common.Extensions;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Product get list by search name command handler.
    /// </summary>
    public sealed class ProductGetListBySearchNameCommandHandler:BaseCommandHandler,IRequestHandler<ProductGetListBySearchNameCommand, PagingResultRsp<List<ProductBaseRsp>>>
    {
        public ProductGetListBySearchNameCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings):base(netICService,httpClientFactory,settings)
        {
        }

        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<PagingResultRsp<List<ProductBaseRsp>>> Handle(ProductGetListBySearchNameCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "MWeb/v1/Goods/GetListByName";
            var data = new { request.Name,request.PageIndex, request.PageSize,request.Platfrom };
            var result = await PostThirdApiWithPagingAsync<List<ProductBaseRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
            //var rsp = JsonConvert.DeserializeObject<JObject>(result);

            //var total = 0;
            //if (rsp["resultNo"].ToString() == "00000000")
            //{
            //    total = rsp["total"].ToString().ToInt32(0);
            //    var list = JsonConvert.DeserializeObject<List<ProductBaseRsp>>(rsp["data"].ToString());
            //    return new PagingResultRsp<List<ProductBaseRsp>>(list, request.PageIndex, request.PageSize, total, 0, "");//
            //}
            //else
            //{
            //    return new PagingResultRsp<List<ProductBaseRsp>>(null, request.PageIndex, request.PageSize, total, 9999, rsp["resultRemark"].ToString());
            //}
        }
    }
}
