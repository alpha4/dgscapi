﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get product collect list command.
    /// </summary>
    public class GetProductCollectListCommand:IRequest<PagingResultRsp<List<GoodsCollectRsp>>>
    {
        /// <summary>
        /// Gets the index of the page.
        /// </summary>
        /// <value>The index of the page.</value>
        public int PageIndex { get; private set; }
        /// <summary>
        /// Gets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; private set; }
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.Product.GetProductCollectListCommand"/> class.
        /// </summary>
        /// <param name="PageIndex">Page index.</param>
        /// <param name="PageSize">Page size.</param>
        /// <perame name="Mobile"></perame>
        public GetProductCollectListCommand(int PageIndex,int PageSize,string Mobile)
        {
            this.PageSize = PageSize;
            this.PageIndex = PageIndex;
            this.Mobile = Mobile;
        }
    }
}
