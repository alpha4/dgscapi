﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Casamiel.Application.Commands
{

    /// <summary>
    /// 
    /// </summary>
    public sealed class GetCakeIndexTagListCommandHandler:BaseCommandHandler,IRequestHandler<GetCakeIndexTagListCommand, BaseResult<List<IndexTagBaseRsp>>>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="netICService"></param>
        /// <param name="httpClientFactory"></param>
        /// <param name="settings"></param>
        public GetCakeIndexTagListCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<BaseResult<List<IndexTagBaseRsp>>> Handle(GetCakeIndexTagListCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "CakeMall/v1/Index/GetTagsList";
            
            var result = await PostThirdApiAsync<List<IndexTagBaseRsp>>(url, JsonConvert.SerializeObject(new { request.StoreId })).ConfigureAwait(false);
            return result;
        }
    }
}
