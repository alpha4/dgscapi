﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 今日达产品详情
    /// </summary>
    public sealed class GetDayProductDetailCommand:IRequest<BaseResult<GoodsProductBaseRsp>>
    {
        public int Storeid { get; set; }
        /// <summary>
        /// Gets or sets the productid.
        /// </summary>
        /// <value>The productid.</value>
        public int Productid { get; set; }
    }
}
