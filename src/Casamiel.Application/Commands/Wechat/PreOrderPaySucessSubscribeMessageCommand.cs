﻿using System;
using MediatR;

namespace Casamiel.Application.Commands.Wechat
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class PreOrderPaySucessSubscribeMessageCommand :   IRequest<bool>
	{
		/// <summary>
		/// 
		/// </summary>
		public string[] Data { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string OrderCode { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; set; }
	}
}
