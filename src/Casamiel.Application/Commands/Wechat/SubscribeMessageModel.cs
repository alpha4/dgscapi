﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.Wechat
{
    /// <summary>
    /// 
    /// </summary>
    public class SubscribeMessageModel
    {
        /// <summary>
		/// 
		/// </summary>
		/// <param name="OpenId"></param>
        public SubscribeMessageModel(string OpenId)
        {
            Touser = OpenId;
        }
        /// <summary>
        /// 消息接收者的openid
        /// </summary>
        [JsonProperty("touser")]
        public string Touser { get; set; }
        /// <summary>
        /// 消息模板ID
        /// </summary>
        [JsonProperty("template_id")]
        public string TemplateId { get; set; }
        /// <summary>
        /// 點擊模板卡片后的跳轉頁面，僅限本小程序内的頁面，支持带参数（示例index?foo=bar），该字段不填則不跳轉
        /// </summary>
        [JsonProperty("page")]
        public string Page { get; set; }
        /// <summary>
        /// 跳转小程序類型：developer开发版、trial体验版、formal正式版，默认为正式版
        /// </summary>
        [JsonProperty("miniprogram_state")]
        public string MiniprogramState { get; set; }
        /// <summary>
        /// 進入小程序查看的语言类型，支持zh_CN(简体中文)、en_US(英文)、zh_HK(繁體中文)、zh_TW(繁体中文)，默认为zh_CN
        /// </summary>
        [JsonProperty("lang")]
        public string Lang { get; set; }
        /// <summary>
        /// 模板内容
        /// </summary>
        [JsonProperty("data")]
        public Dictionary<string, DataValue> Data { get; set; }
    }
    /// <summary>
    /// 模板内容关键字
    /// </summary>
    public class DataValue
    {
        /// <summary>
        /// 订阅消息参数值
        /// </summary>
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
