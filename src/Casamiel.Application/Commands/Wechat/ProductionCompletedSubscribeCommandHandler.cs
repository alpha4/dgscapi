﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain;
using Enyim.Caching;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.Wechat
{


    /// <summary>
    /// 预点单下单小程序推送
    /// </summary>
    public sealed class PreOrderSubscribeMessage : SubscribeMessageModel
    {
        private string page;

        
        public PreOrderSubscribeMessage(string OpenId) : base(OpenId)
        {

        }
        /// <summary>
        /// 消息模板ID
        /// </summary>
        [JsonProperty("template_id")]
        public new string TemplateId => "Aii9cgijy2-d-mXgaTwKbP-2vzWf4RFtHyPqWEXlqnM";//"250";

        /// <summary>
        /// 跳转小程序类型：developer开发版、trial体验版、formal正式版，默认为正式版
        /// </summary>
        [JsonProperty("miniprogram_state")]
        public new string MiniprogramState => "formal";


        /// <summary>
        /// 设置小程序订阅消息跳转页面
        /// </summary>
        /// <param name="pageurl"></param>
         
        public void SetPageUrl(string pageurl)
        {
            page = pageurl;
        }
        /// <summary>
        /// 点击模板卡片后的跳转页面
        /// </summary>
        [JsonProperty("page")]
        public new string Page {
            get {
                return page;
            }
            set {
                page = value;
                return;
            }
        }
        /// <summary>
        ///  
        /// </summary>
        /// <param name="Datas"></param>
        public void SetTemplateData(string[] Datas)
        {
			if (Datas is null) {
				throw new ArgumentNullException(nameof(Datas));
			}
            //取餐号         { { character_string19.DATA} }
            //订单状态     { { phrase16.DATA} }
            //门店名称     { { thing23.DATA} }
            //门店地址     { { thing24.DATA} }
            Dictionary<string, DataValue> data1 = new Dictionary<string, DataValue> {
                { "character_string19", new DataValue { Value = Datas[0] } },//取餐号{{character_string19.DATA}}
			    { "phrase16", new DataValue{ Value= Datas[1] }},//订单状态{{phrase16.DATA}}
                { "thing23", new DataValue{ Value= Datas[2] }},//门店名称{{thing23.DATA}}
                 { "thing24", new DataValue{ Value=Datas[3].Trim().Length>20? Datas[3].Trim().Substring(0,20):Datas[3]}}//门店地址{ { thing24.DATA} }
             };
            Data = data1;
        }
        
    }



    /// <summary>
    /// 
    /// </summary>
    public class ProductionCompletedSubscribeCommandHandler : BaseCommandHandler, IRequestHandler<ProductionCompletedSubscribeCommand, bool>
    {

        private readonly IWxService _wxService;
        private readonly IMiniAppSettingsRepository _miniAppSettingsRepository;

        private readonly IMemcachedClient _memcachedClient;
        private readonly string _ApiName;
        

        /// <summary>
		/// 
		/// </summary>
		/// <param name="miniAppSettingsRepository"></param>
		/// <param name="memcachedClient"></param>
		/// <param name="wxService"></param>
		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
        public ProductionCompletedSubscribeCommandHandler(IMiniAppSettingsRepository miniAppSettingsRepository,
        IMemcachedClient memcachedClient, IWxService wxService, INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
            _miniAppSettingsRepository = miniAppSettingsRepository;

            _memcachedClient = memcachedClient;
            _wxService = wxService;
            
            _ApiName = settings.Value.ApiName;
        }

        public async Task<bool> Handle(ProductionCompletedSubscribeCommand request, CancellationToken cancellationToken)
        {
            var miniappelist = await _miniAppSettingsRepository.GetAllAsync<ICasaMielSession>().ConfigureAwait(false);
            var miniappentity = miniappelist.Where(c => c.Sort == 3).SingleOrDefault();
            if (miniappentity == null) {
                throw new Exception("小程序未配置");
            }
            var _cacheKey = MemcachedPre + Constant.MINIAPPPTOKEN + miniappentity.AppId;
            var entity = await _memcachedClient.GetValueAsync<string>(_cacheKey).ConfigureAwait(false);

            if (string.IsNullOrEmpty(entity)) {
                string url = "cgi-bin/token?"; //grant_type=client_credential&appid=APPID&secret=APPSECRET";//https://api.weixin.qq.com/
                string param = string.Format(System.Globalization.CultureInfo.CurrentCulture, "appid={0}&secret={1}&grant_type={2}", miniappentity.AppId, miniappentity.Secret, "client_credential");
                string result = "";
                var client = HttpClientFactory.CreateClient("weixin");
                using (var requestMessage = new HttpRequestMessage(HttpMethod.Get, url + param)) {
                    using var response = await client.SendAsync(requestMessage, cancellationToken).ConfigureAwait(false);
                    if (response.IsSuccessStatusCode) {
                        result = await response.Content.ReadAsStringAsync(cancellationToken).ConfigureAwait(false);
                    }
                }


                var wxopenInfo = JsonConvert.DeserializeObject<dynamic>(result);
                entity = wxopenInfo.access_token;

                // long expires_in = wxopenInfo.expires_in;
                await _memcachedClient.AddAsync(_cacheKey, entity, 7000).ConfigureAwait(false);
            }
            var openIdenity = await _wxService.GetByAppIdMobileAsync<ICasaMielSession>(miniappentity.AppId, request.Mobile).ConfigureAwait(false);
            if (openIdenity != null) {
                var url = "cgi-bin/message/subscribe/send?access_token={0}";

                var premessage = new PreOrderSubscribeMessage(openIdenity.OpenId);
                premessage.SetTemplateData(request.Data);
                premessage.SetPageUrl($"selfOrder/pages/orderDetail/orderDetail?orderCode={request.OrderCode}");
                var content = JsonConvert.SerializeObject(premessage);
                Console.WriteLine(content);
                var result = await this.PostAsync(string.Format(CultureInfo.InvariantCulture, url, entity), content, "weixin", cancellationToken).ConfigureAwait(false);
                OrderLogger.Trace($"PreOrderProductionCompleted:{miniappentity.AppId},{content},{request.Mobile},{request.Data[0]},{result}");

            }
            return true;
        }


    }
}
