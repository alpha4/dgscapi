﻿using Casamiel.Common;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Interface;
using Casamiel.Infrastructure.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Application.Commands.Wechat
{
    /// <summary>
    /// Send template message.
    /// </summary>
    public abstract  class SendTemplateMessage
    {
        protected string access_token;
        protected string data;
        protected bool hasformId;
        protected abstract Task SetTeTemplate(IWxService wxService, IOrderBaseRepository orderBaseRepository, string orderCode );

        public async  Task<ValueTuple<string,string>>  SendMessage(IWxService wxService, IOrderBaseRepository orderBaseRepository, string orderCode)
        {
             await   this.SetTeTemplate(wxService,orderBaseRepository, orderCode).ConfigureAwait(false);
             return new ValueTuple<string,string>(data, access_token);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class CakeStoreSendTemplateMessage : SendTemplateMessage
    {
        
        public CakeStoreSendTemplateMessage()
        {

          //  _orderBaseRepository = new OrderBaseRepository();
        }
        protected override async Task SetTeTemplate(IWxService _wxService,IOrderBaseRepository _orderBaseRepository,string OrderCode)
        {
            var order = await _orderBaseRepository.GetByOrderCodeAsync<ICasaMielSession>(OrderCode).ConfigureAwait(false);
            if (order != null)
            {
                var list = await _wxService.GetMiniAppSettingsList<ICasaMielSession>().ConfigureAwait(false);
                var item = list.Find(c => c.Sort == (int)PaymentScenario.CakeStore);
                var wx = await _wxService.GetByAppIdMobileAsync<ICasaMielSession>(item.AppId, order.Mobile).ConfigureAwait(false);
                data = $"{order.StoreId}";
                access_token = order.Mobile;
            }
        }
    }
}
