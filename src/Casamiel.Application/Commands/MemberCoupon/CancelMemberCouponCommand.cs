﻿using System;
using System.Collections.Generic;
using System.Text;

using Casamiel.Domain.Request;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{   
    /// <summary>
    /// 
    /// </summary>
    public sealed  class CancelMemberCouponCommand:IRequest<BaseResult<string>>
    {
        /// <summary>
        /// 
        /// </summary>
        public string CardNO { get; private set; }

        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderCode { get; private set; }


        /// <summary>
        /// 0卡号退单，
        /// 1订单号退单
        /// </summary>
        public int RefundMode { get; private set; }
         
        /// <summary>
        /// 
        /// </summary>
        public int ActivityId { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="CardNO"></param>
        /// <param name="OrderCode"></param>
        /// <param name="RefundMode">0卡号退单， 1订单号退单</param>
        /// <param name="ActivityId"></param>
        public CancelMemberCouponCommand(string CardNO,string OrderCode,int RefundMode,int ActivityId)
        {
           this.CardNO = CardNO;
            this.RefundMode = RefundMode;
            this.OrderCode = OrderCode;
            this.ActivityId = ActivityId;
        }
    }
}
