﻿using System;
using MediatR;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetCouponGiveDetailCommand:BaseCommand,IRequest<BaseResult<CouponGiveReponse>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string ForwardCode { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="ForwardCode"></param>
		/// <param name="DataSource"></param>
		public GetCouponGiveDetailCommand(string ForwardCode,int DataSource):base(DataSource)
		{
			this.ForwardCode = ForwardCode;
		}
	}
}
