﻿using System;
using MediatR;
using Casamiel.Domain;

using Casamiel.Domain.Response;
using System.Net.Http;
using Microsoft.Extensions.Options;
using Casamiel.Common;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.Data;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetCouponGiveDetailCommandHandler: BaseCommandHandler, IRequestHandler<GetCouponGiveDetailCommand, BaseResult<CouponGiveReponse>>
	{
		private readonly ICasaMielSession _session;
		private readonly IMemberCouponGiveRepository _memberCouponGiveRepository;
		private readonly IMemberCouponRepository _memberCouponRepository;
		public GetCouponGiveDetailCommandHandler(ICasaMielSession casaMielSession, IMemberCouponGiveRepository  memberCouponGiveRepository, IMemberCouponRepository memberCouponRepositor, INetICService netICService,
			IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)

		{
			_session = casaMielSession;
			_memberCouponGiveRepository = memberCouponGiveRepository;
			_memberCouponRepository = memberCouponRepositor;

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public async Task<BaseResult<CouponGiveReponse>> Handle(GetCouponGiveDetailCommand request, CancellationToken cancellationToken)
		{
			if (request is null) {
				throw new ArgumentNullException(nameof(request));
			}

			var entity = await _memberCouponGiveRepository.FindAsync<ICasaMielSession>(request.ForwardCode).ConfigureAwait(false);
			if (entity != null) {
				if (entity.Status == 0 && entity.CreateTime < DateTime.Now.AddDays(-2)) {
					entity.Status = -1;
					await _memberCouponGiveRepository.UpdateAsync<ICasaMielSession>(entity).ConfigureAwait(false);
				}


				var list = await _memberCouponGiveRepository.GetListAsync<ICasaMielSession>(entity.MID, 0, false, DateTime.Now.AddDays(-2)).ConfigureAwait(false);
				if (list.Any()) {
					var listCount = list.Count;
					for (int i = 0; i < listCount; i++) {
						var Couponentity = await _memberCouponRepository.GetKeyAsync<ICasaMielSession>(list[i].MCID).ConfigureAwait(false);
						if (Couponentity == null) continue;
						var items = await _memberCouponGiveRepository.GetListAsync<ICasaMielSession>(entity.MID, list[i].MCID, 0, true, DateTime.Now.AddDays(-2)).ConfigureAwait(false);
						if (!items.Any()&& Couponentity.GiveStatus==-1) {
							using (var uow = _session.UnitOfWork(IsolationLevel.Serializable)) {
								await _memberCouponRepository.UpdateCouponGiveStatusAsnync(Couponentity, 0, uow).ConfigureAwait(false);
							}
						}
						list[i].Status = -1;
						list[i].LastTime = DateTime.Now;
						await _memberCouponGiveRepository.UpdateAsync<ICasaMielSession>(list[i]).ConfigureAwait(false);
					}
				}

				


				var rsp = new CouponGiveReponse {
					ForwardCode = entity.ForwardCode,
					Message = entity.Message,
					State=entity.Status,
					Productname = entity.Productname
				};
				return new BaseResult<CouponGiveReponse>(rsp, 0, "");
			}
			return new BaseResult<CouponGiveReponse>(null, 999, "信息不存在");

		}
	}
}
