﻿using System;
using MediatR;

namespace Casamiel.Application.Commands
{

	/// <summary>
	/// 退单恢复代金券
	/// </summary>
	public sealed class CancelOrderMemberCouponCommand:INotification
	{
		/// <summary>
		/// 
		/// </summary>
		public string OrderCode { get; set; }
		 
		 
	}
}
