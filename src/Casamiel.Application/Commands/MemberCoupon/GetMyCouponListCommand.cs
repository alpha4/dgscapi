﻿using System;
using MediatR;
using Casamiel.Domain.Response;
using System.Collections.Generic;
using Casamiel.Domain;

namespace Casamiel.Application.Commands
{
	public sealed class GetMyCouponListCommand:BaseCommand, IRequest<PagingResultRsp<List<MemberCouponReponse>>>
	{
		public GetMyCouponListCommand(int DataSource) : base(DataSource) { }
		/// <summary>
		/// 
		/// </summary>
		public int PageSize { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int PageIndex { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int State { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; set; }

		 
	}
}
