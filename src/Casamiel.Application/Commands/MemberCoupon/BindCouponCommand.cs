﻿using System;
using MediatR;
using Casamiel.Domain;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands
{


	/// <summary>
	/// 
	/// </summary>
	public sealed class BindCouponCommand:BaseCommand,IRequest<BaseResult<string>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public string Ticketcode { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="Mobile"></param>
		/// <param name="Ticketcode"></param>
		/// <param name="DataSource"></param>
		public BindCouponCommand(string Mobile, string Ticketcode,int DataSource):base(DataSource)
		{
			this.Mobile = Mobile;
			this.Ticketcode = Ticketcode;
		}
	}
}
