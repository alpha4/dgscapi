﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Common.Extensions;
using Casamiel.Domain;


using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetMyCouponListCommandHandler : BaseCommandHandler, IRequestHandler<GetMyCouponListCommand, PagingResultRsp<List<MemberCouponReponse>>>
	{
		private readonly IMemberCouponGiveRepository _memberCouponGiveRepository;
		private readonly IMemberCouponRepository _memberCouponRepository;
		private readonly IMemberRepository _memberRepository;
		private readonly ICasaMielSession _session;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="casaMielSession"></param>
		/// <param name="memberCouponGiveRepository"></param>
		/// <param name="memberCouponRepositor"></param>
		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
		/// <param name="memberRepository"></param>
		public GetMyCouponListCommandHandler(ICasaMielSession casaMielSession, IMemberCouponGiveRepository memberCouponGiveRepository, IMemberCouponRepository memberCouponRepositor, INetICService netICService,
			IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings, IMemberRepository memberRepository) : base(netICService, httpClientFactory, settings)

		{
			_session = casaMielSession;
			_memberCouponGiveRepository = memberCouponGiveRepository;
			_memberCouponRepository = memberCouponRepositor;
			_memberRepository = memberRepository;
		}

		public async Task<PagingResultRsp<List<MemberCouponReponse>>> Handle(GetMyCouponListCommand request, CancellationToken cancellationToken)
		{
			if (request is null) {
				throw new ArgumentNullException(nameof(request));
			}
			BizInfologger.Trace($"GetMyCouponListCommandHandler:{JsonConvert.SerializeObject(request)}");
			var member = await _memberRepository.FindAsync<ICasaMielSession>(request.Mobile).ConfigureAwait(false);
			if (member == null) {
				return new PagingResultRsp<List<MemberCouponReponse>>(new List<MemberCouponReponse>(), 0, 0, 0, 999, "会员不存在");
			}

			var list = await _memberCouponGiveRepository.GetListAsync<ICasaMielSession>(member.ID, 0, false, DateTime.Now.AddHours(-48)).ConfigureAwait(false);
			if (list.Any()) {
				var listCount = list.Count;
				for (int i = 0; i < listCount; i++) {
					var entity = await _memberCouponRepository.GetKeyAsync<ICasaMielSession>(list[i].MCID).ConfigureAwait(false);
					if (entity == null) continue;
					
					list[i].Status = -1;
					list[i].LastTime = DateTime.Now;
					await _memberCouponGiveRepository.UpdateAsync<ICasaMielSession>(list[i]).ConfigureAwait(false);

					var items = await _memberCouponGiveRepository.GetListAsync<ICasaMielSession>(member.ID, list[i].MCID, 0, true, DateTime.Now.AddHours(-48)).ConfigureAwait(false);
					if (!items.Any() && entity.GiveStatus == -1) {
						using (var uow = _session.UnitOfWork(IsolationLevel.Serializable)) {
							await _memberCouponRepository.UpdateCouponGiveStatusAsnync(entity, 0, uow).ConfigureAwait(false);
						}
					}
				}
			}
			//var Couponlist = await _memberCouponRepository.GetListAsnyc<ICasaMielSession>(member.ID, 0, DateTime.Now.AddDays(-7)).ConfigureAwait(false);
			//if (Couponlist.Any()) {
			//	var count = list.Count();
			//	for (int i = 0; i < count; i++) {
			//		var coupon = Couponlist[i];
			//		if (coupon.State != 0) {
			//			continue;
			//		}
			//		var irsp = await NetICService.Tmticketquery(new Domain.Request.IC.TmticketqueryReq { ticketcode = Couponlist[i].Ticketcode }).ConfigureAwait(false);
			//		if (irsp.code == 0) {
			//			var j = JObject.Parse(irsp.content);
			//			var state = j["state"].ToInt16(0);

			//			var Startdate = DateTime.Parse(j["startdate"].ToString(), CultureInfo.InvariantCulture);
			//			var Enddate = DateTime.Parse(j["enddate"].ToString(), CultureInfo.InvariantCulture);
			//			if (state != Couponlist[i].State || !coupon.UsedDate.HasValue) {
			//				coupon.State = state;

			//				coupon.Productname = j["productname"].ToString();
			//				coupon.Productid = j["productid"].ToInt64(0);
			//				coupon.Startdate = Startdate;
			//				coupon.Enddate = Enddate;
			//				if (coupon.State == 0) {
			//					coupon.UsedDate = DateTime.Parse(j["usedddate"].ToString(), CultureInfo.InvariantCulture);
			//				}
			//				await _memberCouponRepository.UpdateAsync<ICasaMielSession>(coupon).ConfigureAwait(false);
			//			}
			//		}
			//	}
			//}


			var rsp = await _memberCouponRepository.GetListAsnyc<ICasaMielSession>(member.ID, request.State, request.PageIndex, request.PageSize).ConfigureAwait(false);
			if (rsp.Content.Any()) {
				var count = rsp.Content.Count();
				for (int i = 0; i < count; i++) {
					var coupon = rsp.Content[i];
					var irsp = await NetICService.Tmticketquery(new Domain.Request.IC.TmticketqueryReq { ticketcode = rsp.Content[i].Ticketcode }).ConfigureAwait(false);
					if (irsp.code == 0) {
						var j = JObject.Parse(irsp.content);
						var state = j["state"].ToInt16(0);

						var Startdate = DateTime.Parse(j["startdate"].ToString(), CultureInfo.InvariantCulture);
						var Enddate = DateTime.Parse(j["enddate"].ToString(), CultureInfo.InvariantCulture);
						if (state != rsp.Content[i].State || coupon.Enddate!=Enddate) {
							coupon.State = state;
							rsp.Content[i].State = state;
							coupon.Productname = j["productname"].ToString();
							coupon.Productid= j["productid"].ToInt64(0);
							coupon.Startdate = Startdate;
							coupon.Enddate = Enddate;
							if (coupon.State == 0) {
								coupon.UsedDate = DateTime.Parse(j["usedddate"].ToString(), CultureInfo.InvariantCulture);
							}
							await _memberCouponRepository.UpdateAsync<ICasaMielSession>(coupon).ConfigureAwait(false);
						}
					}
				}
				var dd = from x in rsp.Content.Where(c => c.State == request.State)
						 select (new MemberCouponReponse {
							 MID = x.MID,
							 GiveStatus = x.GiveStatus,
							 MCID = x.MCID,
							 Productname = x.Productname,
							 Enddate = x.Enddate,
							 Price = x.Price,
							 Startdate = x.Startdate,
							 State = x.State,
							 CreateTime=x.CreateTime
						 });
		        
				return new PagingResultRsp<List<MemberCouponReponse>>(dd.ToList(), request.PageIndex, request.PageSize, rsp.Total, 0, "");
			}

			return new PagingResultRsp<List<MemberCouponReponse>>(new List<MemberCouponReponse>(), request.PageIndex, request.PageSize, 0, 0, "");
		}
	}
}