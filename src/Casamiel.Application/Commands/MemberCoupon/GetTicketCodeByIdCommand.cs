﻿using System;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetTicketCodeByIdCommand : BaseCommand, IRequest<BaseResult<string>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public long MCID { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="Mobile"></param>
		/// <param name="MCID"></param>
		/// <param name="DataSource"></param>
		public GetTicketCodeByIdCommand(string Mobile, long MCID,int DataSource):base(DataSource)
		{
			this.Mobile = Mobile;
			this.MCID = MCID;
		}
	}
}
