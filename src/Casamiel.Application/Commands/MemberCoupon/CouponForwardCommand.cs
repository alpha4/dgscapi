﻿using System;
using MediatR;
using Casamiel.Domain;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class CouponForwardCommand:BaseCommand,IRequest<BaseResult<MemberCouponGive>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public long MCID { get;private set; }

		/// <summary>
		/// 
		/// </summary>
		public string Message { get; private set; } 

		/// <summary>
		/// 
		/// </summary>
		/// <param name="Mobile"></param>
		/// <param name="MCID"></param>
		/// <param name="Message"></param>>
		/// <param name="DataSource"
		public CouponForwardCommand(string Mobile,long MCID,string Message,int DataSource):base(DataSource)
		{
			this.MCID = MCID;
			this.Mobile = Mobile;
			this.Message = Message;
		}
	}
}
