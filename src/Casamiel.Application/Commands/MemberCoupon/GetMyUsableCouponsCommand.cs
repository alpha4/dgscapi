﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Request.Waimai;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 订单里可用的代金券
	/// </summary>
	public sealed class GetMyUsableCouponsCommand:BaseCommand, IRequest<BaseResult<List<MemberCouponReponse>>>
	{
		/// <summary>
		/// 
		/// </summary>
		public List<TakeOutOrderGoods> GoodsList { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public int StoreId { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public bool IsTakeOut { get; private set; }


		/// <summary>
		/// 
		/// </summary>
		public int State { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="goodslist"></param>
		/// <param name="StoreId"></param>
		/// <param name="Mobile"></param>
		/// <param name="IsTakeOut"></param>
		/// <param name="DataSource"></param>
		public GetMyUsableCouponsCommand(List<TakeOutOrderGoods> goodslist, int StoreId,string Mobile,bool IsTakeOut,int DataSource):base(DataSource)
		{
			this.GoodsList = goodslist;
			this.StoreId = StoreId;
			this.Mobile = Mobile;
			this.IsTakeOut = IsTakeOut;
			this.State = 3;
		}
	}
}
