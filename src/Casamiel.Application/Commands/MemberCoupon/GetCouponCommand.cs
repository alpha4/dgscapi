﻿using System;
using MediatR;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetCouponCommand:BaseCommand,IRequest<BaseResult<string>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; private set; }


		/// <summary>
		/// yzm
		/// </summary>
		public string Yzm { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public string ForwardCode { get; private set; }


		/// <summary>
		/// 
		/// </summary>
		/// <param name="Mobile"></param>
		/// <param name="yzm"></param>
		/// <param name="ForwardCode"></param>
		/// <param name="DataSource"></param>
		public GetCouponCommand(string Mobile,string yzm,string ForwardCode,int DataSource):base(DataSource)
		{
			this.Mobile = Mobile;
			this.Yzm = yzm;
			this.ForwardCode = ForwardCode;
		}
	}
}
