﻿using System;
using MediatR;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class UpdateMemberCouponCommand:IRequest<BaseResult<string>>
	{
		public UpdateMemberCouponCommand()
		{
		}
	}
}
