﻿using System;

using MediatR;
using Casamiel.Infrastructure;
using CasaMiel.Domain.Entity;
using Casamiel.Domain.Response;
using Casamiel.Domain;

using System.Net.Http;
using Microsoft.Extensions.Options;
using Casamiel.Common;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;
using Newtonsoft.Json.Linq;
using System.Linq;
using Casamiel.Common.Extensions;

namespace Casamiel.Application.Commands
{
	 public sealed class UpdateMemberCouponCommandHandler:BaseCommandHandler, IRequestHandler<UpdateMemberCouponCommand, BaseResult<string>>
		{
		private readonly IMemberCouponGiveRepository _memberCouponGiveRepository;
		private readonly IMemberCouponRepository _memberCouponRepository;
		private readonly IMemberRepository _memberRepository;
		private readonly ICasaMielSession _session;
		private readonly IMbrStoreRepository _mbrStoreRepository;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="casaMielSession"></param>
		/// <param name="memberCouponGiveRepository"></param>
		/// <param name="memberCouponRepositor"></param>
		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
		/// <param name="memberRepository"></param>
		public UpdateMemberCouponCommandHandler(ICasaMielSession casaMielSession, IMemberCouponGiveRepository memberCouponGiveRepository, IMemberCouponRepository memberCouponRepositor, INetICService netICService,
			IHttpClientFactory httpClientFactory, IMbrStoreRepository mbrStoreRepository, IOptionsSnapshot<CasamielSettings> settings, IMemberRepository memberRepository) : base(netICService, httpClientFactory, settings)

		{
			_session = casaMielSession;
			_memberCouponGiveRepository = memberCouponGiveRepository;
			_memberCouponRepository = memberCouponRepositor;
			_memberRepository = memberRepository;
			_mbrStoreRepository = mbrStoreRepository;
		}

		public async Task<BaseResult<string>> Handle(UpdateMemberCouponCommand request, CancellationToken cancellationToken)
		{
			var rsp = await _memberCouponRepository.GetAllAsync<ICasaMielSession>().ConfigureAwait(false);
			var list = rsp.ToList();
			var count = list.Count;
			if (count > 0) {
				
				for (int i = 0; i < count; i++) {
					var coupon = list[i];
					if (coupon.State != 0) {
						continue;
					}
					var irsp = await NetICService.Tmticketquery(new Domain.Request.IC.TmticketqueryReq { ticketcode = list[i].Ticketcode }).ConfigureAwait(false);
					if (irsp.code == 0) {
						var j = JObject.Parse(irsp.content);
						var state = j["state"].ToInt16(0);

						var Startdate = DateTime.Parse(j["startdate"].ToString(), CultureInfo.InvariantCulture);
						var Enddate = DateTime.Parse(j["enddate"].ToString(), CultureInfo.InvariantCulture);
						if (state != list[i].State || !coupon.UsedDate.HasValue) {
							coupon.State = state;
							 
							coupon.Productname = j["productname"].ToString();
							coupon.Productid = j["productid"].ToInt64(0);
							coupon.Startdate = Startdate;
							coupon.Enddate = Enddate;
							if (coupon.State == 0) {
								coupon.UsedDate= DateTime.Parse(j["usedddate"].ToString(), CultureInfo.InvariantCulture);
							}
							await _memberCouponRepository.UpdateAsync<ICasaMielSession>(coupon).ConfigureAwait(false);
						}
					}
				}
				 
				//return new PagingResultRsp<List<MemberCouponReponse>>(dd.ToList(), request.PageIndex, request.PageSize, rsp.Total, 0, "");
			}
			return new BaseResult<string>("", 0, "");
		}
	}
}
