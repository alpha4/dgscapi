﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
    
    /// <summary>
    /// 下单赠券
    /// </summary>
    public sealed  class OrderCreateMemberCouponCommand: IRequest<BaseResult<string>>
    {
        /// <summary>
        /// 
        /// </summary>
        public string OrderCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ActivityId { get; set; }
    }
}
