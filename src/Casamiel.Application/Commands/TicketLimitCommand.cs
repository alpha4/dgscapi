﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class TicketLimitCommand:IRequest<ICResult>
	{
		/// <summary>
		/// 
		/// </summary>
		public int ShopId { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public List<long> TicketIds { get;private set; }

        /// <summary>
        /// 
        /// </summary>
        public int Type { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ShopId"></param>
        /// <param name="ticketIds"></param>
        /// <param name="Type">0-productid,1-ticketid</param>
        public TicketLimitCommand(int ShopId,List<long> ticketIds,int Type)
		{
			this.ShopId = ShopId;
			this.TicketIds = ticketIds;
            this.Type = Type;
		}
	}
}
