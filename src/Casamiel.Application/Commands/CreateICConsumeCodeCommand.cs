﻿using System;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// Create ICC onsume code command.
	/// </summary>
	public class CreateICConsumeCodeCommand:BaseCommand,IRequest<Tuple<ICConsumeCodeRsp,int,string>>
    {
        /// <summary>
        /// Gets the card no.
        /// </summary>
        /// <value>The card no.</value>
        public string CardNO { get; private set; }

        /// <summary>
		/// 
		/// </summary>
		/// <param name="CardNO"></param>
		/// <param name="Datasource"></param>
        public CreateICConsumeCodeCommand(string CardNO,int Datasource):base(Datasource)
        {
            this.CardNO = CardNO;
        }
        
    }
}
