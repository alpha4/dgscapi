﻿using System;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Identified command.
    /// </summary>
    public class IdentifiedCommand<T, R> : IRequest<R>
         where T : IRequest<R>
    {
        /// <summary>
        /// Gets the command.
        /// </summary>
        /// <value>The command.</value>
        public T Command { get; }
         
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        //public Guid Id { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.IdentifiedCommand`2"/> class.
        /// </summary>
        /// <param name="command">Command.</param>
        /// <param name="session">Session.</param>
        public IdentifiedCommand(T command, T session)
        {
            Command = command;

        }
    }
}
