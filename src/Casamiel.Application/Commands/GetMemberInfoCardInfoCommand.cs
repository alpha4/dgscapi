﻿using System;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get member info card info command.
    /// </summary>
    public sealed class GetMemberInfoCardInfoCommand:BaseCommand,IRequest<BaseResult<GetMemberInfoCardInfoRsp>>
    {
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
        /// <summary>
        /// Gets the card no.
        /// </summary>
        /// <value>The card no.</value>
        public string CardNO { get; private set; }

      

        /// <summary>
		/// 
		/// </summary>
		/// <param name="Mobile"></param>
		/// <param name="CardNO"></param>
		/// <param name="DataSource"></param>
        public GetMemberInfoCardInfoCommand(string Mobile,string CardNO,int DataSource):base(DataSource)
        {
            this.Mobile = Mobile;
            this.CardNO = CardNO;
           
        }
    }
}
