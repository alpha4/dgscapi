﻿using System;
using Casamiel.Domain;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Account;
using MediatR;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public class GetMemberInfoCommand:IRequest<BaseResult<MemberInfoResponse>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get;private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="Mobile"></param>
		public GetMemberInfoCommand(string Mobile)
		{
			this.Mobile = Mobile;
		}
	}
}
