﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 我的发票
    /// </summary>
    public sealed class GetMyInvoicesCommandHandler:IRequestHandler<GetMyInvoicesCommand, PagingResultRsp<List<BaseInvoice>>>
    {
        private readonly IInvoiceRepository _invoiceRepository;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceRepository">Invoice repository.</param>
        public GetMyInvoicesCommandHandler(IInvoiceRepository invoiceRepository)
        {
            _invoiceRepository = invoiceRepository;
        }

        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public  async Task<PagingResultRsp<List<BaseInvoice>>> Handle(GetMyInvoicesCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var list= await _invoiceRepository.GetListAsnyc<ICasaMielSession>(request.Mobile, request.PageIndex, request.PageSize).ConfigureAwait(false);
            return new PagingResultRsp<List<BaseInvoice>>(list.Data, list.PageIndex, list.PageSize, list.Total, 0, "");
        }
    }
}
