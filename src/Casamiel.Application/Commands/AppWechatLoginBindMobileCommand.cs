﻿using System;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// App wechat login bind mobile command.
    /// </summary>
    public sealed class AppWechatLoginBindMobileCommand:BaseCommand,IRequest<ValueTuple<bool,string>>
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; private set; }
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
        /// <summary>
        /// Gets the yzm.
        /// </summary>
        /// <value>The yzm.</value>
        public string Yzm { get; private set; }
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.AppWechatLoginBindMobileCommand"/> class.
        /// </summary>
        /// <param name="Id">Identifier.</param>
        /// <param name="Mobile">Mobile.</param>
        /// <param name="Yzm">Yzm.</param>
		/// <param name="DataSource"></param>
        public AppWechatLoginBindMobileCommand(int Id,string Mobile,string Yzm,int DataSource):base(DataSource)
        {
            this.Mobile = Mobile;
            this.Yzm = Yzm;
            this.Id = Id;
        }
    }
}
