﻿using System;
using Casamiel.Domain.Response;
using Casamiel.Domain.Request;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Save address command.
    /// </summary>
    public class SaveAddressCommand:IRequest<BaseResult<CakeAddressBaseRsp>>
    {
        /// <summary>
        /// Gets or sets the consignee identifier.
        /// </summary>
        /// <value>The consignee identifier.</value>
        public int ConsigneeId { get; set; }
        /// <summary>
        /// 是否默认
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// 排序编号，倒序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 联系人姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 手机 会员信息
        /// </summary>
       
        public string Mobile { get; set; }

        /// <summary>
        /// 电话 
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        public string FullAddress { get; set; }

        /// <summary>
        /// 省ID
        /// </summary>
        public int ProvinceId { get; set; }

        /// <summary>
        /// 市ID
        /// </summary>
        public int CityId { get; set; }

        /// <summary>
        /// 区ID
        /// </summary>
        public int DistrictId { get; set; }

        /// <summary>
        /// 邮编
        /// </summary>
        public string Postcode { get; set; }

        /// <summary>
        /// 纬度
        /// </summary>
        public double Latitude { get; set; }
        /// <summary>
        /// 经度
        /// </summary>
        public double Longitude { get; set; }
        /// <summary>
        /// 门店ID
        /// </summary>
        public int StoreId { get; set; } 

        /// <summary>
        /// 版本号
        /// </summary>
        public string Version { get; private set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.Address.SaveAddressCommand"/> class.
        /// </summary>
        public SaveAddressCommand(string Version)
        {
            this.Version = Version; 
        }
    }
}
