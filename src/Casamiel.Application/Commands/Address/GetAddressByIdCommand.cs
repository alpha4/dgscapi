﻿using System;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get address by identifier command.
    /// </summary>
    public class GetAddressByIdCommand: IRequest<BaseResult<CakeAddressBaseRsp>>
    {
        /// <summary>
        /// Gets the consignee identifier.
        /// </summary>
        /// <value>The consignee identifier.</value>
        public int ConsigneeId { get; private set; }
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.GetAddressByIdCommand"/> class.
        /// </summary>
        /// <param name="ConsigneeId">Consignee identifier.</param>
        /// <param name="Mobile">Mobile.</param>
        public GetAddressByIdCommand(int ConsigneeId,string Mobile)
        {
            this.ConsigneeId = ConsigneeId;
            this.Mobile = Mobile;
        }
    }
}
