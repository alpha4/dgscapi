﻿using System;
using MediatR;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Delete address command.
    /// </summary>
    public class DeleteAddressCommand:IRequest<BaseResult<string>>
    {
        /// <summary>
        /// Gets the consignee identifier.
        /// </summary>
        /// <value>The consignee identifier.</value>
        public int ConsigneeId { get; private set; }
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.Address.DeleteAddressCommand"/> class.
        /// </summary>
        /// <param name="ConsigneeId">Consignee identifier.</param>
        /// <param name="Mobile">Mobile.</param>
        public DeleteAddressCommand( int ConsigneeId,string Mobile)
        {
            this.ConsigneeId = ConsigneeId;
            this.Mobile = Mobile;
        }
    }
}
