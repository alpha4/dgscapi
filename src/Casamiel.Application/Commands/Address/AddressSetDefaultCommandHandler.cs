﻿using System;
using System.Net.Http;
using Casamiel.Common;
using Microsoft.Extensions.Options;
using MediatR;
using System.Threading;
using Casamiel.Domain.Response;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Address set default command handler.
    /// </summary>
    public sealed class AddressSetDefaultCommandHandler : BaseCommandHandler, IRequestHandler<AddressSetDefaultCommand, BaseResult<string>>
    {
        /// <summary>
        /// Initializes a new instance of the
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public AddressSetDefaultCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {

        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<BaseResult<string>> Handle(AddressSetDefaultCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "CakeMall/v1/Address /SetDefault";
            var data = new { request.Mobile, request.ConsigneeId };
            var rsp = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            BizInfologger.Error($"{url},{JsonConvert.SerializeObject(request)},{JsonConvert.SerializeObject(rsp)}");
            return rsp;
        }
    }
}
