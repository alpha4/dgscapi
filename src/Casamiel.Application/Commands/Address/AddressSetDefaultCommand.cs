﻿using System;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Address set default command.
    /// </summary>
    public class AddressSetDefaultCommand:IRequest<BaseResult<string>>
    {
        /// <summary>
        /// Gets the consignee identifier.
        /// </summary>
        /// <value>The consignee identifier.</value>
        public int ConsigneeId { get; private set; }
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
        /// <summary>
        /// Initializes a new instance of the
        /// </summary>
        /// <param name="ConsigneeId">Consignee identifier.</param>
        /// <param name="Mobile">Mobile.</param>
        public AddressSetDefaultCommand(int ConsigneeId,string Mobile)
        {
            this.ConsigneeId = ConsigneeId;
            this.Mobile = Mobile;
        }

        // Copy constructor
        public AddressSetDefaultCommand(AddressSetDefaultCommand other)
            : this(PassThroughNonNull(other).ConsigneeId,
              PassThroughNonNull(other).Mobile)
        {
        }

        // Null check method
        private static AddressSetDefaultCommand PassThroughNonNull(AddressSetDefaultCommand addressSetDefaultCommand)
        {
            if (addressSetDefaultCommand == null)
                throw new ArgumentNullException(nameof(addressSetDefaultCommand));
            return addressSetDefaultCommand;
        }
    }
}
