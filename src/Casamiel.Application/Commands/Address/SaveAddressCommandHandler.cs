﻿using System;
using System.Net.Http;
using Casamiel.Common;
using Microsoft.Extensions.Options;
using MediatR;
using System.Threading;
using Casamiel.Domain.Response;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Save address command handler.
    /// </summary>
    public sealed class SaveAddressCommandHandler : BaseCommandHandler, IRequestHandler<SaveAddressCommand, BaseResult<CakeAddressBaseRsp>>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.SaveAddressCommandHandler"/> class.
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public SaveAddressCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {

        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async System.Threading.Tasks.Task<BaseResult<CakeAddressBaseRsp>> Handle(SaveAddressCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = $"CakeMall/{request.Version}/Address/Save";
            var result = await PostThirdApiAsync<CakeAddressBaseRsp>(url, JsonConvert.SerializeObject(request)).ConfigureAwait(false);
            OrderLogger.Trace($"{JsonConvert.SerializeObject(request)},{JsonConvert.SerializeObject(result)}");
            return result;
           
        }

    }
}
