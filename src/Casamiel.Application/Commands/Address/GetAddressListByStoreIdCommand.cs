﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get address list by store identifier command.
    /// </summary>
    public class GetAddressListByStoreIdCommand:IRequest<BaseResult<List<CakeAddressBaseRsp>>>
    {
        /// <summary>
        /// Gets the store identifier.
        /// </summary>
        /// <value>The store identifier.</value>
        public int StoreId { get; private set; }
        public string Mobile { get; private set; }
        public string Version { get; private set; }
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.Address.GetAddressListByStoreIdCommand"/> class.
        /// </summary>
        /// <param name="StoreId">Store identifier.</param>
        /// <param name="Mobile">Mobile.</param>
        /// <param name="Vesion">版本号默认v1</param>
        public GetAddressListByStoreIdCommand(int StoreId,string Mobile,string Version="v1")
        {
            this.StoreId = StoreId;
            this.Mobile = Mobile;
            this.Version = Version;
        }
    }
}
