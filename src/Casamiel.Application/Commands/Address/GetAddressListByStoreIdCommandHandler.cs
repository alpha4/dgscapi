﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get address list by store identifier command handler.
    /// </summary>
    public sealed class GetAddressListByStoreIdCommandHandler:BaseCommandHandler,IRequestHandler<GetAddressListByStoreIdCommand, BaseResult<List<CakeAddressBaseRsp>>>
    {
        /// <summary>
        /// Initializes a new instance of the
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public GetAddressListByStoreIdCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {

        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<BaseResult<List<CakeAddressBaseRsp>>> Handle(GetAddressListByStoreIdCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = $"CakeMall/{request.Version}/Address/GetListByStore";
            var data = new { request.StoreId, request.Mobile };
            var result = await PostThirdApiAsync<List<CakeAddressBaseRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
           
        }
    }
}
