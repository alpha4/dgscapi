﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get address by identifier command handler.
    /// </summary>
    public sealed class GetAddressByIdCommandHandler:BaseCommandHandler,IRequestHandler<GetAddressByIdCommand, BaseResult<CakeAddressBaseRsp>>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public GetAddressByIdCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<BaseResult<CakeAddressBaseRsp>> Handle(GetAddressByIdCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "CakeMall/v1/Address/GetBase";
            var data = new { request.ConsigneeId, request.Mobile };
            var result = await PostThirdApiAsync< CakeAddressBaseRsp>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
            //var rsp = JsonConvert.DeserializeObject<JObject>(result);
            //if (rsp["resultNo"].ToString() != "00000000")
            //{
            //    return new BaseResult<Cake_Address_BaseRsp>(null, 9999, rsp["resultRemark"].ToString());
            //}
            //var info = JsonConvert.DeserializeObject<Cake_Address_BaseRsp> (rsp["data"].ToString());
            //return new BaseResult<Cake_Address_BaseRsp>(info, 0, "");
        }
    }
}
