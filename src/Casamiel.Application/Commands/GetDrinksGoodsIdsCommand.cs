﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetDrinksGoodsIdsCommand:BaseCommand,IRequest<List<string>>
	{
		public GetDrinksGoodsIdsCommand():base(1)
		{
		}
	}
}
