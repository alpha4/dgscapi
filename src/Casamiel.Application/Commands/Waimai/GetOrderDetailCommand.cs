﻿using Casamiel.Domain.Response;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace  Casamiel.Application.Commands.Waimai
{
    public sealed class GetOrderDetailCommand:BaseCommand,IRequest<BaseResult<CakeOrderBaseRsp>>
    {
        /// <summary>
        /// Gets the order code.
        /// </summary>
        /// <value>The order code.</value>
        public string OrderCode { get; private set; }
    /// <summary>
    /// Gets the mobile.
    /// </summary>
    /// <value>The mobile.</value>
    public string Mobile { get; private set; }

    /// <summary>
    /// /
    /// </summary>
    /// <param name="OrderCode">Order code.</param>
    /// <param name="Mobile">Mobile.</param>
    /// <param name="DataSource"></param>
    public GetOrderDetailCommand(string OrderCode, string Mobile,int DataSource):base(DataSource)
    {
        this.Mobile = Mobile;
        this.OrderCode = OrderCode;
    }
}
}
