﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Common.Extensions;
using Casamiel.Domain;
using Casamiel.Domain.Entity;

using Casamiel.Domain.Request;
using Casamiel.Domain.Request.IC;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Waimai;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 计算运费
    /// </summary>
    public sealed class CalculateFreightFeeCommandHandler : BaseCommandHandler, IRequestHandler<CalculateFreightFeeCommand, BaseResult<TakeOutGetFreightMoneyRsp>>
    {

        public CalculateFreightFeeCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {

        }

        public async Task<BaseResult<TakeOutGetFreightMoneyRsp>> Handle(CalculateFreightFeeCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            try
            {
                var FreightMoney = await CalculateWaimaiOrderFreightMoney(request.OrderMoney).ConfigureAwait(false);
                var rsp = new TakeOutGetFreightMoneyRsp { FreightMoney = FreightMoney };
                return new BaseResult<TakeOutGetFreightMoneyRsp>(rsp, 0, "");
            }
            catch (Exception ex)
            {
                return new BaseResult<TakeOutGetFreightMoneyRsp>(null, 999, ex.Message);
                throw;
            }
        }
    }
}
