﻿using System;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Waimai;
using MediatR;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 已完成订单的订单开票
    /// </summary>
    public sealed class OrderInvoiceCommand:BaseCommand,IRequest<BaseResult<string>>
    {
        /// <summary>
        /// 
        /// </summary>
        public string OrderCode { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public  int InvoiceId { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="orderCode"></param>
        /// <param name="invoiceId"></param>
        /// <param name="DataSource"></param>
        public OrderInvoiceCommand(string mobile,string orderCode,int invoiceId,int DataSource):base(DataSource)
        {
            this.OrderCode = orderCode;
            this.Mobile = mobile;
            this.InvoiceId = invoiceId;
        }
    }
}
