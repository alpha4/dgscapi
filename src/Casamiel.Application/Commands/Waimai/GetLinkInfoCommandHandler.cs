﻿using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// Get link info command handler.
    /// </summary>
    public sealed class GetLinkInfoCommandHandler : BaseCommandHandler, IRequestHandler<GetLinkInfoCommand, BaseResult<AddressLinkInfoRsp>>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.GetAddressListByStoreIdCommandHandler"/> class.
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public GetLinkInfoCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {

        }

        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<BaseResult<AddressLinkInfoRsp>> Handle(GetLinkInfoCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var url = $"TakeOut/v1/Address/GetLinkInfo";
            var result = await PostThirdApiAsync<AddressLinkInfoRsp>(url, JsonConvert.SerializeObject(request)).ConfigureAwait(false);
            if (result.Code==0 && result.Content==null)
            {
                return new BaseResult<AddressLinkInfoRsp>(new AddressLinkInfoRsp(), 0, "");
            }
            return result;
        }
    }
}
