﻿using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Waimai;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed   class CalculateFreightFeeCommand:IRequest<BaseResult<TakeOutGetFreightMoneyRsp>>
    {
        /// <summary>
        /// 
        /// </summary>
        public  decimal OrderMoney { get; set; }
    }
}
