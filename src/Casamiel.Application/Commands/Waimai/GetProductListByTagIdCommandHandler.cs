﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Casamiel.Domain;
using Casamiel.Common.Extensions;
using System.Linq;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class GetProductListByTagIdCommandHandler : BaseCommandHandler, IRequestHandler<GetProductListByTagIdCommand, PagingResultRsp<List<ProductBaseRsp>>>
    {
        private readonly IMbrStoreRepository _mbrStoreRepository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mbrStoreRepository"></param>
        /// <param name="netICService"></param>
        /// <param name="httpClientFactory"></param>
        /// <param name="settings"></param>
        public GetProductListByTagIdCommandHandler(IMbrStoreRepository mbrStoreRepository, INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
            _mbrStoreRepository = mbrStoreRepository;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PagingResultRsp<List<ProductBaseRsp>>> Handle(GetProductListByTagIdCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var storeinfo = await _mbrStoreRepository.GetKeyAsync<ICasaMielSession>(request.StoreId).ConfigureAwait(false);
            if (storeinfo == null)
            {
                return new PagingResultRsp<List<ProductBaseRsp>>(null, request.PageIndex, request.PageSize, 0, 9999, "门店信息有误！");
            }
            if (storeinfo.IsWaimaiClose == 1)
            {
                return new PagingResultRsp<List<ProductBaseRsp>>(null, request.PageIndex, request.PageSize, 0, 9999, "抱歉门店已闭店！");
            }


            var url = "TakeOut/v1/Goods/GetList";
            var data = new { request.PageIndex, request.PageSize, request.TagBaseId, request.StoreId };
            var result = await PostThirdApiWithPagingAsync<List<ProductBaseRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            if (result.Code == 0 && result.Content.Any())
            {
                for (int i = 0; i < result.Content.Count; i++)
                {
                    result.Content[i].TagId = request.TagBaseId;
                    
                }
               
            }
            return result;
            //var rsp = JsonConvert.DeserializeObject<JObject>(result);

            //var total = 0;
            //if (rsp["resultNo"].ToString() == "00000000")
            //{
            //    total = rsp["total"].ToString().ToInt32(0);
            //    var list = JsonConvert.DeserializeObject<List<ProductBaseRsp>>(rsp["data"].ToString());
            //    if (list.Any())
            //    {
            //        foreach (var item in list)
            //        {
            //            item.TagId = request.TagBaseId;
            //        }
            //    }
            //    return new PagingResultRsp<List<ProductBaseRsp>>(list, request.PageIndex, request.PageSize, total, 0, "");//
            //}
            //else
            //{
            //    return new PagingResultRsp<List<ProductBaseRsp>>(null, request.PageIndex, request.PageSize, total, 9999, rsp["resultRemark"].ToString());
            //}
        }
    }
}
