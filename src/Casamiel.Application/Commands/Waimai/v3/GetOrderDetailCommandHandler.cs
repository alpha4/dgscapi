﻿using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Casamiel.Application.Commands.Waimai.v3
{
	public sealed class GetOrderDetailCommandHandler : BaseCommandHandler, IRequestHandler<GetOrderDetailCommand, BaseResult<CakeOrderBaseRespone>>
	{
		private readonly IStoreService _storeService;
		/// <summary>
		/// 获取订单详情
		/// </summary>
		/// <param name="storeService"></param>
		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
		public GetOrderDetailCommandHandler(IStoreService storeService, INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
		{
			_storeService = storeService;
		}
		/// <summary>
		/// Handle the specified request and cancellationToken.
		/// </summary>
		/// <returns>The handle.</returns>
		/// <param name="request">Request.</param>
		/// <param name="cancellationToken">Cancellation token.</param>
		public async Task<BaseResult<CakeOrderBaseRespone>> Handle(GetOrderDetailCommand request, CancellationToken cancellationToken)
		{
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "TakeOut/v3/Order/GetBase";
			var data = new { mobile = request.Mobile, orderCode = request.OrderCode };
			var result = await PostThirdApiAsync<CakeOrderBaseRespone>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
			return result;
		}
	}
}
