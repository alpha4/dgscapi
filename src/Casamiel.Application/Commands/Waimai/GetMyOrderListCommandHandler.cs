﻿using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class GetMyOrderListCommandHandler : BaseCommandHandler, IRequestHandler<GetMyOrderListCommand, PagingResultRsp<List<CakeOrderGetListRsp>>>
    {
        /// <summary>
        /// 获取蛋糕商城订单列表
        /// </summary>
        /// <param name="netICService"></param>
        /// <param name="httpClientFactory"></param>
        /// <param name="settings"></param>
        public GetMyOrderListCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
        }

        public async Task<PagingResultRsp<List<CakeOrderGetListRsp>>> Handle(GetMyOrderListCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "TakeOut/v1/Order/GetList";
            var data = new
            {
                status = request.Status,
                pageIndex = request.PageIndex,
                pageSize = request.PageSize,
                mobile = request.Mobile
            };
            var rsp = await PostThirdApiWithPagingAsync<List<CakeOrderGetListRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return rsp;
        }
    }
    }
