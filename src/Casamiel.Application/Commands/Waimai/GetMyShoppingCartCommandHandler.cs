﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;

using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Waimai;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class GetMyShoppingCartCommandHandler : BaseCommandHandler, IRequestHandler<GetMyShoppingCartCommand, BaseResult<List<TakeOutShoppingCartBaseRsp>>>
    {

         
        /// <summary>
        /// 
        /// </summary>
        /// <param name="netICService"></param>
        /// <param name="httpClientFactory"></param>
        /// <param name="settings"></param>
        public GetMyShoppingCartCommandHandler( INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
             
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
        public async Task<BaseResult<List<TakeOutShoppingCartBaseRsp>>> Handle(GetMyShoppingCartCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = $"TakeOut/{request.Version}/ShoppingCart/GetList";
            var data = new { request.Mobile, request.StoreId };
            var result = await PostThirdApiAsync<List<TakeOutShoppingCartBaseRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
           
        }
    }
}
