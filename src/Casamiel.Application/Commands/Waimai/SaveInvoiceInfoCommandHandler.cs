﻿using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Waimai;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
 
namespace Casamiel.Application.Commands.Waimai
{

	/// <summary>
	/// 
	/// </summary>
	public sealed class SaveInvoiceInfoCommandHandler : BaseCommandHandler, IRequestHandler<SaveInvoiceInfoCommand, BaseResult<InvoiceBaseRsp>>
	{
		public SaveInvoiceInfoCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
		{

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public async Task<BaseResult<InvoiceBaseRsp>> Handle(SaveInvoiceInfoCommand request, CancellationToken cancellationToken)
		{
			if (request == null) {
				throw new ArgumentNullException(nameof(request));
			}

			var url = "TakeOut/v1/Invoice/Save";
			if (request.Type == 0 && (request.TaxpayerId == null ||request.TaxpayerId.Trim().Length==0))
            {
                return new BaseResult<InvoiceBaseRsp>(null,9999, "税号不能为空");
            }
            var result = await PostThirdApiAsync<InvoiceBaseRsp>(url, JsonConvert.SerializeObject(request)).ConfigureAwait(false);
            return result;
           
        }
    }
}
