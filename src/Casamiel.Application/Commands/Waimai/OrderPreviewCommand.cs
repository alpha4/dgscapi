﻿using System;
using MediatR;
 
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Waimai;
using Casamiel.Domain.Request.Waimai;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 外卖下单预览
    /// </summary>
    public sealed class OrderPreviewCommand:BaseCommand,IRequest<BaseResult<OrderPreviewReponse>>
    {
        /// <summary>
        /// 会员手机号
        /// </summary>
        public string Mobile { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public  OrderPreviewRequest OrderPreview { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public OrderPreviewCommand(OrderPreviewRequest orderPreview,string Mobile,int DataSource):base(DataSource)
        {
            this.OrderPreview = orderPreview;
            this.Mobile = Mobile;
        }
    }
}
