﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Waimai;
using MediatR;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed   class GetMyShoppingCartCommand:BaseCommand,IRequest<BaseResult<List<TakeOutShoppingCartBaseRsp>>>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DataSource"></param>
        public GetMyShoppingCartCommand(int DataSource) : base(DataSource) { }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int StoreId { get; set; }

		/// <summary>
		/// 版本号
		/// </summary>
		public string Version { get; set; } = "v3";
    }
}
