﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.Waimai
{
	/// <summary>
	/// 
	/// </summary>
	public sealed  class CleanShoppingCardByIdsCommandHandler : BaseCommandHandler, IRequestHandler<CleanShoppingCardByIdsCommand, BaseResult<string>>
    {

        public CleanShoppingCardByIdsCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {

        }

        public async Task<BaseResult<string>> Handle(CleanShoppingCardByIdsCommand request, CancellationToken cancellationToken)
        {
            var url = "TakeOut/v1/ShoppingCart/CleanByIds";
            return  await PostAsync<BaseResult<string>>(url, JsonConvert.SerializeObject(request)).ConfigureAwait(false);
        }
    }
}
