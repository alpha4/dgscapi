﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class GetLinkInfoCommand:BaseCommand,IRequest<BaseResult<AddressLinkInfoRsp>>
    {
        public GetLinkInfoCommand(int DataSource) : base(DataSource) { }
        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; set; }
    }
}
