﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Common.Extensions;
using Casamiel.Domain;
using Casamiel.Domain.Entity;

using Casamiel.Domain.Request;
using Casamiel.Domain.Request.IC;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class GetTagsListCommandHandler : BaseCommandHandler, IRequestHandler<GetTagsListCommand, BaseResult<List<IndexTagBaseRsp>>>
    {
        public GetTagsListCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {

        }
        public async Task<BaseResult<List<IndexTagBaseRsp>>> Handle(GetTagsListCommand request, CancellationToken cancellationToken)
        {
			if (request is null) {
				throw new ArgumentNullException(nameof(request));
			}

			var url = "TakeOut/v1/Index/GetTagsList";
            if (request.Version == "pre") {
                url = "Pred/v3/Goods/GetTagsList";
            }
            
            var result = await PostThirdApiAsync<List<IndexTagBaseRsp>>(url, "").ConfigureAwait(false);
            return result;

        }
    }
}
