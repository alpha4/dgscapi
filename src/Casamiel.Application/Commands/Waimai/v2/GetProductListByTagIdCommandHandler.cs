﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Casamiel.Domain;
using Casamiel.Common.Extensions;
using System.Linq;
using Enyim.Caching;

namespace Casamiel.Application.Commands.Waimai.V2
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class GetProductListByTagIdCommandHandler : BaseCommandHandler, IRequestHandler<GetProductListByTagIdCommand, PagingResultRsp<List<TakeOutProductBaseRsp>>>
    {
        private readonly IMbrStoreRepository _mbrStoreRepository;
		private readonly IMemcachedClient _memcachedClient;
		private readonly string _memcachedPrex;
		/// <summary>
		/// 
		/// </summary>
		/// <param name="mbrStoreRepository"></param>
		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
		/// <param name="memcachedClient"></param>
		public GetProductListByTagIdCommandHandler(IMbrStoreRepository mbrStoreRepository, INetICService netICService, IHttpClientFactory httpClientFactory,
			IMemcachedClient memcachedClient,IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
            _mbrStoreRepository = mbrStoreRepository;
			_memcachedClient = memcachedClient;
			_memcachedPrex = settings.Value.MemcachedPre;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PagingResultRsp<List<TakeOutProductBaseRsp>>> Handle(GetProductListByTagIdCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var storeinfo = await _mbrStoreRepository.GetKeyAsync<ICasaMielSession>(request.StoreId).ConfigureAwait(false);
            if (storeinfo == null)
            {
                return new PagingResultRsp<List<TakeOutProductBaseRsp>>(new List<TakeOutProductBaseRsp>(), request.PageIndex, request.PageSize, 0, 9999, "门店信息有误！");
            }
            var url = "TakeOut/v3/Goods/GetList";
            if (request.Version is not { Length: > 0 }) {
                if (storeinfo.IsWaimaiShopClose == 1) {
                    return new PagingResultRsp<List<TakeOutProductBaseRsp>>(new List<TakeOutProductBaseRsp>(), request.PageIndex, request.PageSize, 0, 9999, "抱歉门店不支持外卖服务！");
                }
                if (storeinfo.IsWaimaiClose == 1) {
                    return new PagingResultRsp<List<TakeOutProductBaseRsp>>(new List<TakeOutProductBaseRsp>(), request.PageIndex, request.PageSize, 0, 9999, "抱歉门店还没开启外卖服务！");
                }
            } else if(request.Version=="pre"){
                url = "Pred/v3/Goods/GetList";
            }
			

			//var cachekey = $"{_memcachedPrex}{Common.Constant.WAIMAIPRODUCTLIST}-{request.PageIndex}-{request.PageSize}-{request.TagBaseId}-{request.StoreId}";
			//var ddd = await _memcachedClient.GetAsync<PagingResultRsp<List<TakeOutProductBaseRsp>>>(cachekey).ConfigureAwait(false);
			//if (ddd.Success) {
			//	return ddd.Value;
			//}

			
            var data = new { request.PageIndex, request.PageSize, request.TagBaseId, request.StoreId };
            var result = await PostThirdApiWithPagingAsync<List<TakeOutProductBaseRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            if (result.Code == 0 && result.Content.Any())
            {
                for (int i = 0; i < result.Content.Count; i++)
                {
                    result.Content[i].TagId = request.TagBaseId;
                    
                }
               
            }
			//await _memcachedClient.AddAsync(cachekey, result, 60).ConfigureAwait(false);

			return result;
            
        }
    }
}
