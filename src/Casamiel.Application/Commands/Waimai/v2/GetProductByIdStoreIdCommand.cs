﻿using System;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;
using MediatR;

namespace Casamiel.Application.Commands.Waimai.V2
{
    /// <summary>
    /// Get product by identifier store identifier command.
    /// </summary>
    public sealed class GetProductByIdStoreIdCommand : BaseCommand,IRequest<BaseResult<GoodsProductBaseRsp>>
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int ProductId { get; private set; }
        /// <summary>
        /// Gets the store identifier.
        /// </summary>
        /// <value>The store identifier.</value>
        public int StoreId { get; private set; }

        /// <summary>
		/// 版本号
		/// </summary>
        public string Version { get; set; }

        /// <summary>
		/// 
		/// </summary>
		/// <param name="ProductId"></param>
		/// <param name="StoreId"></param>
		/// <param name="Version"></param>
        /// <param name="DataSource"></param>
        public GetProductByIdStoreIdCommand(int ProductId, int StoreId,string Version,int DataSource):base(DataSource)
        {
            this.ProductId = ProductId;
            this.StoreId = StoreId;
            this.Version = Version;
        }
    }
}
