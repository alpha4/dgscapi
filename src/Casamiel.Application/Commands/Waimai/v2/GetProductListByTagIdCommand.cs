﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands.Waimai.V2
{
    /// <summary>
    /// 获取蛋糕商城分类下产品列表
    /// </summary>
    public sealed class GetProductListByTagIdCommand : IRequest<PagingResultRsp<List<TakeOutProductBaseRsp>>>
    {
        /// <summary>
        /// 
        /// </summary>
        public int TagBaseId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
		/// 
		/// </summary>
        public string Version { get; private set; }

        /// <summary>
		/// 
		/// </summary>
        public GetProductListByTagIdCommand()
		{
            Version = "";

        }

        /// <summary>
		/// 
		/// </summary>
		/// <param name="version"></param>
        public GetProductListByTagIdCommand(string version)
        {
            Version = version;

        }
    }

}
