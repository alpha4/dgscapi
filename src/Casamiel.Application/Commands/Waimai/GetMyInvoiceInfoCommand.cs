﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Waimai;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 我的开票信息
    /// </summary>
    public sealed  class GetMyInvoiceInfoCommand:IRequest<BaseResult<List<InvoiceBaseRsp>>>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; set; }
    }
}
