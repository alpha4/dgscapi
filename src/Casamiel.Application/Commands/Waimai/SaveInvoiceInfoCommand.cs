﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Waimai;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 保存开票信息
    /// </summary>
    public sealed  class SaveInvoiceInfoCommand:IRequest<BaseResult<InvoiceBaseRsp>>
    {
        /// <summary>
        /// 发票信息Id 
        /// </summary>
        public int InvoiceId { get; set; }

        /// <summary>
        /// 会员手机号
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 发票类型 0 公司抬头 1 个人
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 发票抬头
        /// </summary>
        public string InvoiceTitle { get; set; }

        /// <summary>
        /// 发票税号
        /// </summary>
        public string TaxpayerId { get; set; }

        /// <summary>
        /// 地址 电话
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 开户行 账号
        /// </summary>
        public string OpeningBank { get; set; }

        /// <summary>
        /// 接受邮箱
        /// </summary>
        public string EMail { get; set; }
        /// <summary>
        /// 接受发票手机号
        /// </summary>
        public string Phone { get; set; }
    }
}
