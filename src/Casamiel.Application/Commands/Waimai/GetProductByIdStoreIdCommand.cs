﻿using System;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;
using MediatR;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// Get product by identifier store identifier command.
    /// </summary>
    public sealed class GetProductByIdStoreIdCommand : BaseCommand,IRequest<BaseResult<GoodsProductBaseRsp>>
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int ProductId { get; private set; }
        /// <summary>
        /// Gets the store identifier.
        /// </summary>
        /// <value>The store identifier.</value>
        public int StoreId { get; private set; }

		/// <summary>
		/// 绑本号
		/// </summary>

		public string Version { get; private set; }
		/// <summary>
		/// Initializes a new instance of the
		/// <see cref="T:Casamiel.API.Application.Commands.GetProductByIdStoreIdCommand"/> class.
		/// </summary>
		/// <param name="ProductId">Identifier.</param>
		/// <param name="StoreId">Store identifier.</param>
		/// <param name="Version">版本号</param>
        /// <param name="DataSource"></param>
		public GetProductByIdStoreIdCommand(int ProductId, int StoreId,string Version,int DataSource):base(DataSource)
        {
            this.ProductId = ProductId;
            this.StoreId = StoreId;
			this.Version = Version;
        }
    }
}
