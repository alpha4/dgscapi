﻿using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Waimai;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class GetFreightRuleCommandHander : BaseCommandHandler, IRequestHandler<GetFreightRuleCommand, BaseResult<TakeOutConfigRsp>>
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.GetAddressListByStoreIdCommandHandler"/> class.
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public GetFreightRuleCommandHander(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {

        }

        public async Task<BaseResult<TakeOutConfigRsp>> Handle(GetFreightRuleCommand request, CancellationToken cancellationToken)
        {
			if (request == null) {
				throw new ArgumentNullException(nameof(request)) ;
			}

            var url = "TakeOut/v1/Index/GetConfig";
            var rsp = await PostThirdApiAsync<TakeOutConfigRsp>(url, "").ConfigureAwait(false);
            return rsp;
        }
    }
}
