﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands.Waimai
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetTagsListCommand : IRequest<BaseResult<List<IndexTagBaseRsp>>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string Version { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public GetTagsListCommand()
		{
			this.Version = "";
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="Version"></param>
		public GetTagsListCommand(string Version)
		{
			this.Version = Version;
		}
	}
}
