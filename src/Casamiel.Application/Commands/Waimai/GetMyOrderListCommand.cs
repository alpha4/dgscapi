﻿using Casamiel.Domain.Response;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class GetMyOrderListCommand : BaseCommand, IRequest<PagingResultRsp<List<CakeOrderGetListRsp>>>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DataSource"></param>
        public GetMyOrderListCommand(int DataSource) : base(DataSource) { }
        /// <summary>
        /// 
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; set; }
    }
  }
