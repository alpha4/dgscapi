﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.Waimai
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class ShoppingCardAddBaseCommandHandler : BaseCommandHandler, IRequestHandler<ShoppingCardAddBaseCommand, BaseResult<string>>
    {

        private readonly IMbrStoreRepository _mbrStoreRepository;

        /// <summary>
		/// 
		/// </summary>
		/// <param name="mbrStoreRepository"></param>
		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
        public ShoppingCardAddBaseCommandHandler(IMbrStoreRepository mbrStoreRepository, INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
            _mbrStoreRepository = mbrStoreRepository;
        }
        public async Task<BaseResult<string>> Handle(ShoppingCardAddBaseCommand request, CancellationToken cancellationToken)
        {
            var url = $"TakeOut/v1/ShoppingCart/AddBase";
            OrderLogger.Trace($"{JsonConvert.SerializeObject(request)}");
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(request)).ConfigureAwait(false);
            return result;
            //var rsp = JsonConvert.DeserializeObject<JObject>(result);
            //if (rsp["resultNo"].ToString() == "00000000")
            //{
            //    var info = JsonConvert.DeserializeObject<Cake_Address_BaseRsp>(rsp["data"].ToString());
            //    return new BaseResult<string>("",0, "操作成功");
            //}
            //return new BaseResult<string>(null, 9999, rsp["resultRemark"].ToString());
        }
    }
}
