﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class GetAddressListByStoreIdCommand:BaseCommand,IRequest<BaseResult<List<CakeAddressBaseRsp>>>
    {
        /// <summary>
        /// 
        /// </summary>
        public int StoreId { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; private set; }
       
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.Address.GetAddressListByStoreIdCommand"/> class.
        /// </summary>
        /// <param name="StoreId">Store identifier.</param>
        /// <param name="Mobile">Mobile.</param>
       ///<param name="DataSource"></param>
        public GetAddressListByStoreIdCommand(int StoreId, string Mobile,int DataSource):base(DataSource)
        {
            this.StoreId = StoreId;
            this.Mobile = Mobile;
           
        }
    }
}
