﻿using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.Waimai;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 我的开票信息
    /// </summary>
    public sealed class GetMyInvoiceInfoCommandHandler : BaseCommandHandler, IRequestHandler<GetMyInvoiceInfoCommand, BaseResult<List<InvoiceBaseRsp>>>
    {
        public GetMyInvoiceInfoCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {

        }
        public async Task<BaseResult<List<InvoiceBaseRsp>>> Handle(GetMyInvoiceInfoCommand request, CancellationToken cancellationToken)
        {
            var url = "TakeOut/v1/Invoice/GetList";
            var result = await PostThirdApiAsync<List<InvoiceBaseRsp>>(url, JsonConvert.SerializeObject(request)).ConfigureAwait(false);
            return result;
           
        }
    }
}
