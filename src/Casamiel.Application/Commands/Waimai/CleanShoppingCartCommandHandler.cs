﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Common.Extensions;
using Casamiel.Domain;
using Casamiel.Domain.Entity;

using Casamiel.Domain.Request;
using Casamiel.Domain.Request.IC;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class CleanShoppingCartCommandHandler:BaseCommandHandler,IRequestHandler<CleanShoppingCartCommand, BaseResult<string>>
    {
      
        public CleanShoppingCartCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
          
        }

        public async Task<BaseResult<string>> Handle(CleanShoppingCartCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            OrderLogger.Trace($"{JsonConvert.SerializeObject(request)}");
            var url = $"TakeOut/v1/ShoppingCart/Clean";
            var data = new { request.Mobile, request.StoreId };
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
            //var rsp = JsonConvert.DeserializeObject<JObject>(result);
            //if (rsp["resultNo"].ToString() == "00000000")
            //{
            //    return new BaseResult<string>("", 0, "操作成功");
            //}
            //BizInfologger.Error($"{url},{JsonConvert.SerializeObject(request)},{rsp["resultRemark"].ToString()}");
            //return new BaseResult<string>(null, 9999, rsp["resultRemark"].ToString());
        }
    }
}
