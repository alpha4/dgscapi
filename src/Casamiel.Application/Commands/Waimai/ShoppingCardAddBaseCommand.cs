﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Casamiel.Domain.Response;
namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed   class ShoppingCardAddBaseCommand:IRequest<BaseResult<string>>
    {
        /// <summary>
        /// 
        /// </summary>
        public int StoreId { get; set; }
        public int GoodsBaseId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Quantity { get; set; }
    }
}
