﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class CleanShoppingCartCommand:BaseCommand,IRequest<BaseResult<string>>
    {
        public CleanShoppingCartCommand(int DataSource) : base(DataSource) { }
        /// <summary>
        /// 
        /// </summary>
        public int StoreId { get; set; }
       
        /// <summary>
        /// 
        /// </summary>
        public  string Mobile { get; set; }

    }
}
