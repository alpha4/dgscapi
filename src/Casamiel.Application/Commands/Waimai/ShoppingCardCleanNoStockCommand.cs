﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class ShoppingCardCleanNoStockCommand:BaseCommand,IRequest<BaseResult<string>>
    {
        public int StoreId
        {
            get; set;
        }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DataSource"></param>
        public ShoppingCardCleanNoStockCommand(int DataSource) : base(DataSource) { }
    }
}
