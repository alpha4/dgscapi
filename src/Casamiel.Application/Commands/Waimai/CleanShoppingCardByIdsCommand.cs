﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands.Waimai
{
    /// <summary>
    /// 
    /// </summary>
    public sealed    class CleanShoppingCardByIdsCommand:BaseCommand,IRequest<BaseResult<string>>
    {
        /// <summary>
        /// 
        /// </summary>
        public List<int> GoodsBaseIds { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public int StoreId
        {
            get;set;
        }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; set; }
    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="GoodsBaseIds"></param>
        /// <param name="DataSource"></param>
        public CleanShoppingCardByIdsCommand(List<int> GoodsBaseIds,int DataSource):base(DataSource)
        {
            this.GoodsBaseIds = GoodsBaseIds;
        }
    }
}
