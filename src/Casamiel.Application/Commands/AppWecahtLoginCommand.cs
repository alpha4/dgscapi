﻿using System;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// App wecaht login command.
    /// </summary>
    public sealed class AppWecahtLoginCommand:BaseCommand,IRequest<Tuple<int, bool,string>>
    {
        /// <summary>
        /// Gets the app identifier.
        /// </summary>
        /// <value>The app identifier.</value>
        public string AppId { get; private set; }
        /// <summary>
        /// Gets the code.
        /// </summary>
        /// <value>The code.</value>
        public string Code { get; private set; }

        /// <summary>
		/// 
		/// </summary>
		/// <param name="AppId"></param>
		/// <param name="Code"></param>
		/// <param name="DataSource"></param>
        public AppWecahtLoginCommand(string AppId,string Code,int DataSource):base(DataSource)
        {
            this.Code = Code;
            this.AppId = AppId;
        }
    }
}
