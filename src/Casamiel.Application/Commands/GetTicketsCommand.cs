﻿using System;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get tickets command.
    /// </summary>
    public class GetTicketsCommand:BaseCommand,IRequest<bool>
    {
        /// <summary>
        /// Gets the card no.
        /// </summary>
        /// <value>The card no.</value>
         public string CardNO { get; private set; }
         

        /// <summary>
        /// 1、已使用；2、未使用；3、已过期；4、其它(如被
        /// </summary>
        public int state { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int pageindex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int pagesize { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string timestamp { get; set; }
        /// <summary>
		/// 
		/// </summary>
		/// <param name="CardNO"></param>
		/// <param name="DataSource"></param>
        public GetTicketsCommand(string CardNO,int DataSource):base(DataSource)
        {
            this.CardNO = CardNO;
        }

    }
}
