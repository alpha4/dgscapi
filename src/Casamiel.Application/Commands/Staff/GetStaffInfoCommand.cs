﻿using System;
using MediatR;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public  sealed	class GetStaffInfoCommand:BaseCommand,IRequest<BaseResult<StaffInfoResponse>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="mobile"></param>
		public GetStaffInfoCommand(string mobile):base(1)
		{
			this.Mobile = mobile;
		}
	}
}
