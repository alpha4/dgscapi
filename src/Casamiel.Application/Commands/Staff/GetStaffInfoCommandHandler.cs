﻿using System;
using MediatR;
using Casamiel.Domain;
using Casamiel.Domain.Response;
using System.Threading;
using System.Threading.Tasks;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed  class GetStaffInfoCommandHandler:IRequestHandler<GetStaffInfoCommand, BaseResult<StaffInfoResponse>>
	{
		private readonly IHlEmployeeRepository _hlEmployeeRepository;
		public GetStaffInfoCommandHandler(IHlEmployeeRepository hlEmployeeRepository)
		{
			_hlEmployeeRepository = hlEmployeeRepository;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public async Task<BaseResult<StaffInfoResponse>> Handle(GetStaffInfoCommand request, CancellationToken cancellationToken)
		{
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var entity = await _hlEmployeeRepository.FindAsync<ICasaMielSession>(request.Mobile).ConfigureAwait(false);
			if (entity != null) {
				var rsp = new StaffInfoResponse {
					Name = entity.Name,
					Mobile = entity.Mobile,
					CardNo = entity.CardNo
				};
				return new BaseResult<StaffInfoResponse>(rsp, 0, "");
			}
			return new BaseResult<StaffInfoResponse>(null, 9999, "员工信息不存在！");
		}
	}
}
