﻿using System;
using MediatR;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands
{

	/// <summary>
	/// 
	/// </summary>
	public class UnlockCommand :  IRequest<BaseResult<string>>
	{
		/// <summary>
		/// 密码
		/// </summary>
		public string Password { get; set; }

		/// <summary>
		/// 手机号
		/// </summary>
		public string Mobile { get; set; }
	}
}
