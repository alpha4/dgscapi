﻿using System;
using MediatR;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands.Staff
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class ModifyPasswordCommand : IRequest<BaseResult<string>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string Password { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public string OldPassword { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="password"></param>
		/// <param name="oldpassword"></param>
		public ModifyPasswordCommand(string password, string oldpassword,string mobile)
		{
			Password = password;
			OldPassword = oldpassword;
			Mobile = mobile;
		}
	}
}
