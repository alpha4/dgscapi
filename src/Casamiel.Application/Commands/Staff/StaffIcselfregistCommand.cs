﻿using System;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands.Staff
{
	/// <summary>
	/// 申请员工卡
	/// </summary>
	public sealed class StaffIcselfregistCommand : BaseCommand, IRequest<BaseResult<string>>
	{
		/// <summary>
		/// 手机号
		/// </summary>
		public string Mobile { get; private set; }
		 
		/// <summary>
		/// </summary>
		/// <param name="mobile">Mobile.</param>
		/// <param name="Source">Source.</param>
		public StaffIcselfregistCommand(string mobile, int Source):base(1)
		{
			Mobile = mobile;
			this.Source = Source;
		}
		/// <summary>
		/// 
		/// 
		/// </summary>
		/// <param name="mobile">Mobile.</param>
		public StaffIcselfregistCommand(string mobile):base(1)
		{
			Mobile = mobile;
			this.Source = 11;
		}
	}
}
