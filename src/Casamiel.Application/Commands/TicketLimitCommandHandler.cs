﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;
using Casamiel.Domain.Response.Waimai;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MediatR;
using System.Threading;

namespace Casamiel.Application.Commands
{
	public sealed class TicketLimitCommandHandler : BaseCommandHandler, IRequestHandler<TicketLimitCommand, ICResult>
	{
		public TicketLimitCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> optionsSnapshot)
	   : base(netICService, httpClientFactory, optionsSnapshot)
		{

		}

		public async Task<ICResult> Handle(TicketLimitCommand request, CancellationToken cancellationToken)
		{
			if (request == null) {
				throw new ArgumentNullException(nameof(request));
			}
			var result = await NetICService.TicketLimit(request.ShopId, request.TicketIds,request.Type,true).ConfigureAwait(false);
			return result;

		}
	}
}
