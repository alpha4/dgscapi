﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get my invoices command.
    /// </summary>
    public sealed class GetMyInvoicesCommand : IRequest<PagingResultRsp<List<BaseInvoice>>>, IBaseRequest
    {
       
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.GetMyInvoicesCommand"/> class.
        /// </summary>
        /// <param name="PageIndex">Page index.</param>
        /// <param name="PageSize">Page size.</param>
        /// <param name="Mobile">Mobile.</param>
        public GetMyInvoicesCommand(int PageIndex, int PageSize, string Mobile)
        {
            this.PageIndex = PageIndex;
            this.PageSize = PageSize;
            this.Mobile = Mobile;
        }

        /// <summary>
        /// Gets the index of the page.
        /// </summary>
        /// <value>The index of the page.</value>
        public int PageIndex { get; private set; }
        /// <summary>
        /// Gets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; private set; }
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
    }



}
