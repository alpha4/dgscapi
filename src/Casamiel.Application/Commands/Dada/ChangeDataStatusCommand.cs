﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands.Dada
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class ChangeDataStatusCommand : BaseCommand, IRequest<BaseResult<string>>
    {
        public ChangeDataStatusCommand() : base(1) { }
#pragma warning disable CA1707 // Identifiers should not contain underscores
        /// <summary>
        /// 
        /// </summary>
        public string order_id { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores

#pragma warning disable CA1707 // Identifiers should not contain underscores
        /// <summary>
        /// 订单状态
        /// </summary>
        public int order_status { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
#pragma warning disable CA1707 // Identifiers should not contain underscores
        /// <summary>
        /// 取消原因
        /// </summary>
        public string cancel_reason { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
#pragma warning disable CA1707 // Identifiers should not contain underscores
        /// <summary>
        /// 取消来源
        /// </summary>
        public int cancel_from { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores

#pragma warning disable CA1707 // Identifiers should not contain underscores
        /// <summary>
        /// 配送人姓名
        /// </summary>
        public string dm_name { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores

#pragma warning disable CA1707 // Identifiers should not contain underscores
        /// <summary>
        /// 配送人电话
        /// </summary>
        public string dm_mobile { get; set; }
#pragma warning restore CA1707 // Identifiers should not contain underscores
    }
}
