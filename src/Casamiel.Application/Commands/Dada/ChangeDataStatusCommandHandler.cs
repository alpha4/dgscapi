﻿using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Domain;

namespace Casamiel.Application.Commands.Dada
{
	/// <summary>
	/// 达达订单状态
	/// </summary>
	public sealed class ChangeDataStatusCommandHandler : BaseCommandHandler, IRequestHandler<ChangeDataStatusCommand, BaseResult<string>>
    {
        private readonly IOrderBaseRepository _orderBaseRepository;
        /// <summary>
        /// Initializes a new instance of the
        /// </summary>
        /// <param name="orderBaseRepository"></param>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public ChangeDataStatusCommandHandler(IOrderBaseRepository orderBaseRepository,INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
            _orderBaseRepository = orderBaseRepository;
        }

        public async Task<BaseResult<string>> Handle(ChangeDataStatusCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "TPP/v3/Order/ChangeDadaStatus";
            var rsp = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(request)).ConfigureAwait(false);
            return rsp;
        }
    }
}
