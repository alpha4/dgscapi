﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 获取蛋糕商城订单列表
    /// </summary>
    public sealed class GetMyCakeOrderListCommand : IRequest<PagingResultRsp<List<CakeOrderGetListRsp>>>
    {
        /// <summary>
        /// 
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; set; }
    }

}
