﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.Order
{
	/// <summary>
	/// 订单商城 订单详情v3
	/// </summary>
	public sealed class GetCakeOrderDetailV3CommandHandler : BaseCommandHandler, IRequestHandler<GetCakeOrderDetailV3Command, BaseResult<CakeOrderBaseRespone>>
	{
		/// <summary>
		/// 获取蛋糕商城订单详情
		/// </summary>
		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
		public GetCakeOrderDetailV3CommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
		{
		}
		/// <summary>
		/// Handle the specified request and cancellationToken.
		/// </summary>
		/// <returns>The handle.</returns>
		/// <param name="request">Request.</param>
		/// <param name="cancellationToken">Cancellation token.</param>
		public async Task<BaseResult<CakeOrderBaseRespone>> Handle(GetCakeOrderDetailV3Command request, CancellationToken cancellationToken)
		{
			if (request == null) {
				throw new ArgumentNullException(nameof(request));
			}
			var url = "CakeMall/v3/Order/GetBase";
			var data = new { mobile = request.Mobile, orderCode = request.OrderCode };
			var result = await PostThirdApiAsync<CakeOrderBaseRespone>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
			return result;
		}
	}
}
