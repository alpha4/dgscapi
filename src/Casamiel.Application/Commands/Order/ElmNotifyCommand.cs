﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class ElmNotifyCommand:IRequest<BaseResult<string>>
    {
        /// <summary>
        /// 应用id，应用创建时系统分配的唯一id
        /// </summary>
        public int AppId { get; set; }
        /// <summary>
        /// 消息的唯一id，用于唯一标记每个消息
        /// </summary>
        public string RequestId { get; set; }
        /// <summary>
        /// 消息类型
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// JSON格式字符串
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 商户的店铺id
        /// </summary>
        public int ShopId { get; set; }
        /// <summary>
        /// 消息发送的时间戳
        /// </summary>
        public long Timestamp { get; set; }
        /// <summary>
        /// 授权商户的账号id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 消息签名，32位全大写
        /// </summary>
        public string Signature { get; set; }
        /// <summary>
        /// ic交易号
        /// </summary>
        public string IcTradeNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string BillNo { get; set; }
#pragma warning disable CA1056 // Uri properties should not be strings
                              /// <summary>
                              /// 发票url
                              /// </summary>
        public string InvoiceUrl { get; set; }
#pragma warning restore CA1056 // Uri properties should not be strings
    }
}
