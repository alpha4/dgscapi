﻿using System;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// v3
	/// </summary>
	public sealed class GetCakeOrderDetailV3Command : BaseCommand, IRequest<BaseResult<CakeOrderBaseRespone>>
	{
		/// <summary>
		/// Gets the order code.
		/// </summary>
		/// <value>The order code.</value>
		public string OrderCode { get; private set; }
		/// <summary>
		/// Gets the mobile.
		/// </summary>
		/// <value>The mobile.</value>
		public string Mobile { get; private set; }

		/// <summary>
		/// /
		/// </summary>
		/// <param name="OrderCode">Order code.</param>
		/// <param name="Mobile">Mobile.</param>
		/// <param name="DataSource"></param>
		public GetCakeOrderDetailV3Command(string OrderCode, string Mobile,int DataSource):base(DataSource)
		{
			this.Mobile = Mobile;
			this.OrderCode = OrderCode;
		}
	}
}
