﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands.Order
{
    /// <summary>
    /// Elm notify command handler.
    /// </summary>
    public sealed class ElmNotifyCommandHandler : BaseCommandHandler, IRequestHandler<ElmNotifyCommand, BaseResult<string>>
    {

        public ElmNotifyCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings
        ) : base(netICService, httpClientFactory, settings)
        {

        }
        public async Task<BaseResult<string>> Handle(ElmNotifyCommand request, CancellationToken cancellationToken)
        {
            var url = "TPP/v3/Order/EleNotify";

            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(request)).ConfigureAwait(false);
            return result;

        }
    }
}
