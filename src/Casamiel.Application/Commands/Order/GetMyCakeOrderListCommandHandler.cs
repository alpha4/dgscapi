﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Common.Extensions;
using Casamiel.Domain;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class GetMyCakeOrderListCommandHandler:BaseCommandHandler,IRequestHandler<GetMyCakeOrderListCommand,PagingResultRsp<List<CakeOrderGetListRsp>>>
    {
        /// <summary>
        /// 获取蛋糕商城订单列表
        /// </summary>
        /// <param name="netICService"></param>
        /// <param name="httpClientFactory"></param>
        /// <param name="settings"></param>
        public GetMyCakeOrderListCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PagingResultRsp<List<CakeOrderGetListRsp>>> Handle(GetMyCakeOrderListCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "CakeMall/v1/Order/GetList";
            var data = new
            {
                status = request.Status,
                pageIndex = request.PageIndex,
                pageSize = request.PageSize,
                mobile = request.Mobile
            };
            var rsp = await PostThirdApiWithPagingAsync< List<CakeOrderGetListRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return rsp;
        }
    }
}
