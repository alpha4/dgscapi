﻿using System;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Create cake order command.
    /// </summary>
    public class CreateCakeOrderCommand : BaseCommand, IRequest<BaseResult<PaymentRsp>>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DataSource"></param>
        public CreateCakeOrderCommand(int DataSource) : base(DataSource) { }
        /// <summary>
        /// 商品sku Id
        /// </summary>
        public int GoodsBaseId { get; set; }
        /// <summary>
        /// 门店id
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// Gets or sets the paycardno.
        /// </summary>
        /// <value>The paycardno.</value>
        public string PayCardNO { get; set; }
        /// <summary>
        /// 会员价
        /// </summary>
        public decimal CostPrice { get; set; }
        /// <summary>
        /// 支付方式 :0 默认   1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
        /// </summary>
        public int Paymethod { get; set; }
        /// <summary>
        /// 购买数量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 手机号 用户
        /// </summary>
        public string Mobile { get; set; }



        /// <summary>
        /// 联系人
        /// </summary>
        public string ContactName { get; set; }
        /// <summary>
        /// 联系方式
        /// </summary>
        public string ContactPhone { get; set; }
        /// <summary>
        /// 优惠券id
        /// </summary>
        public int DiscountCouponId { get; set; }
        /// <summary>
        /// 优惠券名称
        /// </summary>
        public string DiscountCouponName { get; set; }
        /// <summary>
        /// 卡号
        /// </summary>
        public string CardNo { get; set; }
        /// <summary>
        /// 优惠券金额
        /// </summary>
        public decimal DiscountCouponMoney { get; set; }
        /// <summary>
        /// 备注信息
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// IC交易号
        /// </summary>
        public string IcTradeNO { get; set; }
        /// <summary>
        /// 商品规格
        /// </summary>
        public string GoodsProperty { get; set; }
        /// <summary>
        /// 配送时间
        /// </summary>
        public DateTime TakeTime { get; set; }
        /// <summary>
        /// 销售号
        /// </summary>
        public string BillNo { get; set; }
        /// <summary>
        /// 配送方式 订单类型 1: 自提 2:外卖 3 快递配送
        /// </summary>
        public int OrderType { get; set; }
        /// <summary>
        /// 地址Id
        /// </summary>
        public int ConsigneeId { get; set; }
        /// <summary>
        /// Gets or sets the invoice identifier.
        /// </summary>
        /// <value>The invoice identifier.</value>
        public int InvoiceId { get; set; }
        /// <summary>
        /// 小程序专用Id
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }
    }
}
