﻿using System;
using MediatR;
using Casamiel.Domain.Response;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Microsoft.Extensions.Options;
using Casamiel.Common;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get cake order detail command handler.
    /// </summary>
    public sealed class GetCakeOrderDetailCommandHandler : BaseCommandHandler,IRequestHandler<GetCakeOrderDetailCommand,BaseResult<CakeOrderBaseRsp>>
    {
        /// <summary>
        /// 获取蛋糕商城订单详情
        /// </summary>
        /// <param name="netICService"></param>
        /// <param name="httpClientFactory"></param>
        /// <param name="settings"></param>
        public GetCakeOrderDetailCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {
        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<BaseResult<CakeOrderBaseRsp>> Handle(GetCakeOrderDetailCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "CakeMall/v3/Order/GetBase";
            var data = new { mobile = request.Mobile, orderCode = request.OrderCode };
            var result = await PostThirdApiAsync<CakeOrderBaseRsp>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return result;
        }
    }
}
