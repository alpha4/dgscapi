﻿using Casamiel.Domain.Request;
using Casamiel.Domain.Response;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Application.Commands.Order
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class CreateCakeOrderV2Command : BaseCommand, IRequest<BaseResult<PaymentRsp>>
    {
        public CreateCakeOrderV2Command(int DataSource) : base(DataSource) { }
        /// <summary>
        /// 商品sku Id
        /// </summary>
        public int GoodsBaseId { get; set; }
        /// <summary>
        /// 门店id
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// Gets or sets the paycardno.
        /// </summary>
        /// <value>The paycardno.</value>
        public string PayCardNO { get; set; }
        /// <summary>
        /// 会员价
        /// </summary>
        public decimal CostPrice { get; set; }
        /// <summary>
        /// 支付方式 :0 默认   1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
        /// </summary>
        public int Paymethod { get; set; }
        /// <summary>
        /// 购买数量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 手机号 用户
        /// </summary>
        public string Mobile { get; set; }



        /// <summary>
        /// 联系人
        /// </summary>
        public string ContactName { get; set; }
        /// <summary>
        /// 联系方式
        /// </summary>
        public string ContactPhone { get; set; }

#pragma warning disable CA2227 // Collection properties should be read only
        /// <summary>
        /// 优惠券信息
        /// </summary>
        public List<OrderDiscountReq> DiscountList { get; set; }
#pragma warning restore CA2227 // Collection properties should be read only
                              /// <summary>
                              /// 卡号
                              /// </summary>
        public string CardNo { get; set; }

        /// <summary>
        /// 备注信息
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// IC交易号
        /// </summary>
        public string IcTradeNO { get; set; }
        /// <summary>
        /// 商品规格
        /// </summary>
        public string GoodsProperty { get; set; }
        /// <summary>
        /// 配送时间
        /// </summary>
        public DateTime TakeTime { get; set; }
        /// <summary>
        /// 销售号
        /// </summary>
        public string BillNo { get; set; }
        /// <summary>
        /// 配送方式 订单类型 1: 自提 2:外卖 3 快递配送
        /// </summary>
        public int OrderType { get; set; }
        /// <summary>
        /// 地址Id
        /// </summary>
        public int ConsigneeId { get; set; }

        /// <summary>
        /// 小程序专用Id
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }
    }
}
