﻿using Casamiel.Domain.Response;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Application.Commands.Order
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class GetCakeOrderDetailV2Command : IRequest<BaseResult<Cake_OrderBaseV2Rsp>>
    {
        /// <summary>
        /// Gets the order code.
        /// </summary>
        /// <value>The order code.</value>
        public string OrderCode { get; private set; }
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }

        /// <summary>
        /// /
        /// </summary>
        /// <param name="OrderCode">Order code.</param>
        /// <param name="Mobile">Mobile.</param>
        public GetCakeOrderDetailV2Command(string OrderCode, string Mobile)
        {
            this.Mobile = Mobile;
            this.OrderCode = OrderCode;
        }
    }
}

