﻿using System;
using MediatR;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get cake order detail command.
    /// </summary>
    public sealed class GetCakeOrderDetailCommand:IRequest<BaseResult<CakeOrderBaseRsp>>
    {
        /// <summary>
        /// Gets the order code.
        /// </summary>
        /// <value>The order code.</value>
        public string OrderCode { get; private set; }
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }
       
        /// <summary>
        /// /
        /// </summary>
        /// <param name="OrderCode">Order code.</param>
        /// <param name="Mobile">Mobile.</param>
        public GetCakeOrderDetailCommand(string OrderCode,string Mobile)
        {
            this.Mobile = Mobile;
            this.OrderCode = OrderCode;
        }
    }
}
