﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class BuyNowCommand:BaseCommand,IRequest<BaseResult<BuyNowRsp>>
    {
        /// <summary>
        /// 配送方式 订单类型 1: 自提 2:外卖 3 快递配送
        /// </summary>
        public int OrderType { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 产品sku id
        /// </summary>
        public int GoodsBaseId { get; set; }

        /// <summary>
        /// 自提时间
        /// </summary>
        public DateTime TakeTime { get; set; }

        /// <summary>
        /// 产品sku关联Id
        /// </summary>
        public int GoodsRelationId { get; set; }

        /// <summary>
        /// 门店id
        /// </summary>
        public int Storeid { get; set; }

        /// <summary>
        /// 门店关联id
        /// </summary>
        public int StoreRelationId { get; set; }

        /// <summary>
		/// 
		/// </summary>
		/// <param name="Datasource"></param>
        public BuyNowCommand(int Datasource) : base(Datasource)
		{

		}
    }
}
