﻿using System;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Base command.
    /// </summary>
    public class BaseCommand
    {
        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>The URL.</value>
        [JsonIgnore]
#pragma warning disable CA1056 // Uri properties should not be strings
        public string RequestUrl { get; set; }
#pragma warning restore CA1056 // Uri properties should not be strings
                              /// <summary>
                              /// Gets or sets the user ip.
                              /// </summary>
                              /// <value>The user ip.</value>
        [JsonIgnore]
        public string UserIp { get; set; }

        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>The source.</value>
        [JsonIgnore]
        public int Source { get; set; }
        /// <summary>
        /// Gets or sets the data source.
        /// </summary>
        /// <value>The data source.</value>
        [JsonIgnore]
        public int DataSource { get; private set; }

        /// <summary>
		/// 
		/// </summary>
		/// <param name="DataSource"></param>
        public  BaseCommand(int DataSource)
		{
            this.DataSource = DataSource;
		}
           

    }
}
