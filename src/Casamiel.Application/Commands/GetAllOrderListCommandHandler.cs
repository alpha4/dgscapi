﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Common.Extensions;
using Casamiel.Domain;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Get all order list command handler.
    /// </summary>
    public sealed class GetAllOrderListCommandHandler:BaseCommandHandler, IRequestHandler<GetAllOrderListCommand, Domain.Response.PagingResultRsp<List<CakeOrderGetListRsp>>>
    {
       // private readonly NLog.ILogger logger = NLog.LogManager.GetLogger("MallService");

        private readonly MallSettings _mallSettings;
   
        /// <summary>
        /// Initializes a new instance of the
        /// 
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        public GetAllOrderListCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings, IOptionsSnapshot<MallSettings> snapshot) :base(netICService,httpClientFactory,settings)
        {
            if (snapshot == null)
            {
                throw new ArgumentNullException(nameof(snapshot));
            }
            _mallSettings = snapshot.Value;
           
        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<PagingResultRsp<List<CakeOrderGetListRsp>>> Handle(GetAllOrderListCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "MWeb/v3/Order/GetListToApp";
            var data = new { status = request.Status, mobile = request.Mobile, pageIndex = request.PageIndex, pageSize = request.PageSize };
            var result = await PostThirdApiWithPagingAsync  <List<CakeOrderGetListRsp>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
			if(result.Code==0 && result.Total == 0) {
				result.Content = new List<CakeOrderGetListRsp>();
			}
            return result;
            //var dynamicdata = JsonConvert.DeserializeObject<JObject>(result);
            //var total = 0;
            //if (dynamicdata["resultNo"].ToString() == "00000000")
            //{
            //    total = dynamicdata["total"].ToString().ToInt32(0);
            //    var rsp = JsonConvert.DeserializeObject<List<Cake_Order_GetListRsp>>(dynamicdata["data"].ToString());
            //    return new PagingResultRsp<List<Cake_Order_GetListRsp>>(rsp, request.PageIndex, request.PageSize, total, 0, "");//
            //}
            //else
            //{
            //    return new PagingResultRsp<List<Cake_Order_GetListRsp>>(null, request.PageIndex, request.PageSize, 0, 999, dynamicdata["resultRemark"].ToString());
            //}
        }
    }
}
