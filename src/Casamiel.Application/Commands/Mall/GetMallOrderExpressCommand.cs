﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;
using MediatR;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 全国送物流信息
	/// </summary>
	public sealed class GetMallOrderExpressCommand:BaseCommand,IRequest<BaseResult<List<OrderExpressTracesReponse>>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string OrderCode { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="OrderCode"></param>
		/// <param name="Mobile"></param>
		/// <param name="DataSource"></param>
		public GetMallOrderExpressCommand(string OrderCode, string Mobile,int DataSource):base(DataSource)
		{
			this.Mobile = Mobile;
			this.OrderCode = OrderCode;
		}
	}
}
