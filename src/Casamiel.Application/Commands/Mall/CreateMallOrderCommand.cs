﻿#region	using

using System.Collections.Generic;
using Casamiel.Domain.Request;
using Casamiel.Domain.Response;
using MediatR;

#endregion

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class CreateMallOrderCommand:BaseCommand,IRequest<BaseResult<PaymentRsp>>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="DataSource"></param>
		public CreateMallOrderCommand(int DataSource) : base(DataSource) { }
       /// <summary>
		/// 
		/// </summary>
		public int StoreId { get; set; }

		/// <summary>
		/// 销售号
		/// </summary>
		public string BillNo { get; set; }

		/// <summary>
		/// ic交易号
		/// </summary>
		public string IcTradeNO { get; set; }

		/// <summary>
		/// 购买类型 1 购物车 2 直接购买
		/// </summary>
		public int BuyType { get; set; }

		/// <summary>
		/// 手机号 身份
		/// </summary>
		public string Mobile { get; set; }

		/// <summary> 
		/// 收货地址ID
		/// </summary> 
		public int ConsigneeId { get; set; }

		/// <summary>
		/// 备注
		/// </summary>
		public string Remark { get; set; }

		/// <summary>
		/// 商品信息
		/// </summary>
		public List<OrderGoods> GoodsList { get; set; }

		/// <summary>
		/// 运费金额 满88包邮
		/// </summary>
		public decimal FreightMoney { get; set; }



		/// <summary>
		/// 开票信息ID
		/// </summary>
		public int InvoiceId { get; set; }

		/// <summary>
		///  支付用的会员卡好
		/// </summary>
		/// <value>The paycardno.</value>
		public string Paycardno { get; set; }

		/// <summary>
		/// 支付方式 1微信app支付，2支付宝app支付，3会员卡，4，小程序微信支付
		/// </summary>
		/// <value>The paymeothd.</value>

		public int Paymethod { get; set; }

		/// <summary>
		/// 优惠券信息
		/// </summary>
#pragma warning disable CA2227 // 集合属性应为只读
		public List<OrderDiscountReq> DiscountList { get; set; }
#pragma warning restore CA2227 // 集合属性应为只读

		/// <summary>
		/// 是否校验运费 false 需要 true 不需要
		/// </summary>
		public bool IsNotPayFreight { get; set; } = true;

		/// <summary>
		/// 小程序专用Id
		/// </summary>
		/// <value>The identifier.</value>
		public int Id { get; set; }
	}
}
