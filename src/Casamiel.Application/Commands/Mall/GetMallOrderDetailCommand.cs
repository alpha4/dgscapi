﻿using System;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetMallOrderDetailCommand:BaseCommand,IRequest<BaseResult<MallOrderResponse>>
	{

		/// <summary>
		/// 
		/// </summary>
		public string OrderCode { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="OrderCode"></param>
		/// <param name="Mobile"></param>
		/// <param name="DataSource"></param>
		public GetMallOrderDetailCommand(string OrderCode,string Mobile,int DataSource):base(DataSource)
		{
			this.Mobile = Mobile;
			this.OrderCode = OrderCode;
		}
	}
}
