﻿using System;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;
using Microsoft.Extensions.Options;
using System.Net.Http;
using Casamiel.Common;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetMallOrderExpressCommandHandler : BaseCommandHandler, IRequestHandler<GetMallOrderExpressCommand, BaseResult<List<OrderExpressTracesReponse>>>
	{

		/// <summary>
		/// 获取订单详情
		/// </summary>

		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
		public GetMallOrderExpressCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
		{

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public async Task<BaseResult<List<OrderExpressTracesReponse>>> Handle(GetMallOrderExpressCommand request, CancellationToken cancellationToken)
		{
			if (request == null) {
				throw new ArgumentNullException(nameof(request));
			}
			var url = "MWeb/v3/Order/GetExpressList";
			var data = new { mobile = request.Mobile, orderCode = request.OrderCode };
			var result = await PostThirdApiAsync< List<OrderExpressTracesReponse>>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
			return result;
		}
	}
}
