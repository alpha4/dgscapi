﻿using System;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.MWeb;
using System.Net.Http;
using Microsoft.Extensions.Options;
using Casamiel.Common;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetMallOrderDetailCommandHandler : BaseCommandHandler, IRequestHandler<GetMallOrderDetailCommand, BaseResult<MallOrderResponse>>
	{

		/// <summary>
		/// 获取订单详情
		/// </summary>

		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
		public GetMallOrderDetailCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
		{

		}

		public async Task<BaseResult<MallOrderResponse>> Handle(GetMallOrderDetailCommand request, CancellationToken cancellationToken)
		{
			if (request == null) {
				throw new ArgumentNullException(nameof(request));
			}
			var url = "MWeb/v3/Order/GetBase";
			var data = new { mobile = request.Mobile, orderCode = request.OrderCode };
			var result = await PostThirdApiAsync<MallOrderResponse>(url, JsonConvert.SerializeObject(data)).ConfigureAwait(false);
			return result;
		}
	}
}
