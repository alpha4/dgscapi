﻿using System;
using MediatR;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using System.Threading;
using System.Threading.Tasks;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class UserLogNoticeCommand:INotification
	{
		/// <summary>
		/// 
		/// </summary>
		public UserLog UserLog { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="userLog"></param>
		public UserLogNoticeCommand(UserLog userLog)
		{
			this.UserLog = userLog;
		}
	}

	/// <summary>
	/// 
	/// </summary>
	public sealed class AddUserLog : INotificationHandler<UserLogNoticeCommand>
	{
		private readonly IUserLogService _userLog;
		public AddUserLog(IUserLogService userLog)
		{
			_userLog = userLog;
		}
		public async Task Handle(UserLogNoticeCommand notification, CancellationToken cancellationToken)
		{
			
			if (notification is null) {
				throw new ArgumentNullException(nameof(notification));
			}

			await _userLog.AddAsync<ICasaMielSession>(notification.UserLog).ConfigureAwait(false);
			
		}
	}
}
