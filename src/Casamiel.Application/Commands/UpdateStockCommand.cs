﻿using System;
using Casamiel.API.Application.Models;
using MediatR;

namespace Casamiel.API.Application.Commands
{
    /// <summary>
    /// Update stock command.
    /// </summary>
    public class UpdateStockCommand:IRequest<bool>
    {
        /// <summary>
        /// Gets the stock push.
        /// </summary>
        /// <value>The stock push.</value>
        public StockPush StockPush { get; private set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.API.Application.Commands.UpdateStockCommand"/> class.
        /// </summary>
        /// <param name="StockPush">Stock push.</param>
         public UpdateStockCommand(StockPush StockPush)
        {
            this.StockPush = StockPush;
        }
    }
}
