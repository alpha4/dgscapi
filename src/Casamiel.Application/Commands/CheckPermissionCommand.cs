﻿using System;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 检测后台 权限
    /// </summary>
    public class CheckPermissionCommand:IRequest<bool>
    {
        /// <summary>
        /// 权限ID
        /// </summary>
        /// <value>The pid.</value>
        public  int Pid { get; private set; }
        /// <summary>
        /// 用户token
        /// </summary>
        /// <value>The token.</value>
        public string Token { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.CheckPermissionCommand"/> class.
        /// </summary>
        /// <param name="Pid">权限ID</param>
        /// <param name="Token">用户token</param>
        public CheckPermissionCommand(int Pid,string Token)
        {
            this.Pid = Pid;
            this.Token = Token;
        }
    }
}
