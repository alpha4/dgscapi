﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Response;
using Casamiel.Domain.Request;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class TmallCreateOrderCommand : IRequest<BaseResult<string>>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// 省Id
        /// </summary>
        public int ProvinceId { get; set; }
        /// <summary>
        /// 市Id
        /// </summary>
        public int CityId { get; set; }
        /// <summary>
        /// 区Id
        /// </summary>
        public int DistrictId { get; set; }
        /// <summary>
        /// 详细地址
        /// </summary>
        public string FullAddress { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
      
        public string ContactName { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
       
        public string ContactPhone { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 订单支付金额
        /// </summary>
        public decimal OrderMoney { get; set; }
        /// <summary>
        /// 运费差价
        /// </summary>
        public decimal FreightMoney { get; set; }
        /// <summary>
        /// 订单优惠金额
        /// </summary>
        public decimal DiscountMoney { get; set; }
        /// <summary>
        /// 商品信息
        /// </summary>
        public List<TPP_OrderGoods> GoodsList { get; set; }
        /// <summary>
        /// 门店ID
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 提货时间
        /// </summary>
        public DateTime TakeTime { get; set; }
        /// <summary>
        /// IC交易号
        /// </summary>
        public string IcTradeNO { get; set; }
        /// <summary>
        /// 交易号
        /// </summary>
        public string BillNo { get; set; }
        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderCode { get; set; }
        /// <summary>
        /// 经度
        /// </summary>
        public double Longitude { get; set; }
        /// <summary>
        /// 纬度
        /// </summary>
        public double Latitude { get; set; }
        /// <summary>
        /// 城市名称
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// 1自提，2配送
        /// </summary>
        /// <value>The type of the order.</value>
        public int OrderType { get; set; }
    }
}
