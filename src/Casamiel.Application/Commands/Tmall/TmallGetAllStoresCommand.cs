﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Casamiel.Domain;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public   class TmallGetAllStoresCommand:IRequest<BaseResult<List<StoreEntity>>>
    {
        //{"lat":30,"lng":12.02222,"distance":5.2,"token":""}
        /// <summary>
        /// 纬度
        /// </summary>
        public double Lat { get; private set; }

        /// <summary>
        /// 经度
        /// </summary>
        public double Lng { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public double Distance { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public string Token { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Latitude">纬度</param>
        /// <param name="Longitude">经度</param>
        /// <param name="Distance">距离</param>
        /// <param name="token"></param>
        public TmallGetAllStoresCommand(double Latitude, double Longitude, double Distance,string token)
        {
            this.Lat = Latitude;
            this.Lng = Longitude;
            this.Distance = Distance;
            this.Token = token;
        }

    }
}
