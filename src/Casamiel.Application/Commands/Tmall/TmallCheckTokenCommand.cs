﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class TmallCheckTokenCommand:IRequest<bool>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Token { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Token"></param>
        public TmallCheckTokenCommand(string Token)
        {
            this.Token = Token;
        }
    }
}
