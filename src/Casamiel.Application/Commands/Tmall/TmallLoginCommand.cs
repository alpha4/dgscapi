﻿using System;
using Casamiel.Domain.Response;
using MediatR;
namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 天猫下单登录
    /// </summary>
    public sealed class TmallLoginCommand:IRequest<BaseResult<string>>
    {
       /// <summary>
       /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.Tmall.TmallLoginCommand"/> class.
       /// </summary>
       /// <param name="UserName">User name.</param>
       /// <param name="Password">Password.</param>
        public TmallLoginCommand(string UserName,string Password)
        {
			if (string.IsNullOrEmpty(UserName)) {
				throw new ArgumentException("message", nameof(UserName));
			}

			if (string.IsNullOrEmpty(Password)) {
				throw new ArgumentException("message", nameof(Password));
			}

			this.UserName = UserName.Trim();
            this.Password = Password.Trim();
        }
        /// <summary>
        /// Gets the name of the user.
        /// </summary>
        /// <value>The name of the user.</value>
        public string UserName { get; private set; }
        /// <summary>
        /// Gets the password.
        /// </summary>
        /// <value>The password.</value>
        public string Password { get; private set; }

    }
}
