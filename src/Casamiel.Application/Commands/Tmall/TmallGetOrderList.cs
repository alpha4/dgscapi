﻿using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain.Response;
using MediatR;


namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public sealed  class TmallGetOrderListCommand: IRequest<PagingResultRsp<List<TPPTMallOrderBaseRsp>>>
    {
        /// <summary>
        /// 
        /// </summary>
        public int OrderStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; }

        public int PageSize { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int StoreId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Token { get; set; }
    }
}
