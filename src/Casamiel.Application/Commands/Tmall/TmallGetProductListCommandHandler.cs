﻿using System;
using MediatR;
using Casamiel.Domain.Response.MWeb;
using Casamiel.Domain.Response;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.Extensions.Options;
using Casamiel.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Tmall get product list command handler.
    /// </summary>
    public sealed class TmallGetProductListCommandHandler:BaseCommandHandler,IRequestHandler<TmallGetProductListCommand,BaseResult<List<TPP_Goods_ProductRsp>>>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.TmallGetProductListCommandHandler"/> class.
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="optionsSnapshot">Options snapshot.</param>
        public TmallGetProductListCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> optionsSnapshot)
        : base(netICService, httpClientFactory, optionsSnapshot)
        {
        }
        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<BaseResult<List<TPP_Goods_ProductRsp>>> Handle(TmallGetProductListCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var url = "TPP/v1/Goods/GetProductList";
            var result = await PostThirdApiAsync<List<TPP_Goods_ProductRsp>>(url, JsonConvert.SerializeObject(new { title=request.ProductName,token=request.Token,storeBaseId=request.StoreBaseId }))
                .ConfigureAwait(false);
            return result;

        }
    }
}
