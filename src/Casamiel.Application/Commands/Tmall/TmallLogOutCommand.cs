﻿using System;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Tmall logout
    /// </summary>
    public class TmallLogOutCommand:IRequest<BaseResult<String>>
    {
        /// <summary>
        /// Gets the token.
        /// </summary>
        /// <value>The token.</value>
        public string Token { get; private set; }
        /// <summary>
        /// 门店id 切换门店专用字段
        /// </summary>
        /// <value>The store base identifier.</value>
        public int StoreBaseId { get; private set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.Tmall.TmallLogOutCommand"/> class.
        /// </summary>
        /// <param name="Token">Token.</param>
        /// <param name="StoreBaseId">Store base identifier.</param>
        public TmallLogOutCommand(string Token,int StoreBaseId)
        {
            this.Token = Token;
            this.StoreBaseId = StoreBaseId;
        }
    }
}
