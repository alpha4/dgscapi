﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// Tmall log out command handler.
    /// </summary>
    public sealed class TmallLogOutCommandHandler: BaseCommandHandler,IRequestHandler<TmallLogOutCommand,BaseResult<string>>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.TmallLogOutCommandHandler"/> class.
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="optionsSnapshot">Options snapshot.</param>
        public TmallLogOutCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> optionsSnapshot)
      :base(netICService, httpClientFactory, optionsSnapshot)
        {
        }

        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<BaseResult<string>> Handle(TmallLogOutCommand request, CancellationToken cancellationToken)
        {
			if (request is null) {
				throw new ArgumentNullException(nameof(request));
			}

			var url = "TPP/v1/Account/LoginOut";
            var result = await PostThirdApiAsync<string>(url, JsonConvert.SerializeObject(new { request.Token, request.StoreBaseId })).ConfigureAwait(false);
            return result;
        }
    }

     
}
