﻿using System;
using MediatR;
using Casamiel.Domain.Response.MWeb;
using Casamiel.Domain.Response;
using System.Collections.Generic;

namespace Casamiel.Application.Commands
{
    /// <summary>
    /// 根据标题查询获取商品列表 ---- 天猫
    /// </summary>
    public class TmallGetProductListCommand:IRequest<BaseResult<List<TPP_Goods_ProductRsp>>>
    {
        /// <summary>
        /// Gets the title.
        /// </summary>
        /// <value>The title.</value>
        public string ProductName { get; private set; }
        /// <summary>
        /// Gets the token.
        /// </summary>
        /// <value>The token.</value>
        public string Token { get;private set; }
        /// <summary>
        /// 门店id 切换门店专用字段
        /// </summary>
        /// <value>The store base identifier.</value>
        public int  StoreBaseId{get;private set;}
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.Tmall.TmallGetProductListCommand"/> class.
        /// </summary>
        /// <param name="ProductName">Product name.</param>
        /// <param name="Token">Token.</param>
        /// <param name="StoreBaseId">Store base identifier.</param>
        public TmallGetProductListCommand(string ProductName,string Token,int StoreBaseId)
        {
            this.ProductName = ProductName;
            this.Token = Token;
            this.StoreBaseId = StoreBaseId;
        }

    }
}
