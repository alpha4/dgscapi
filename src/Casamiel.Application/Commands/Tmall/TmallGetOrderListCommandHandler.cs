﻿using System;
using MediatR;
using Casamiel.Domain.Response;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using Microsoft.Extensions.Options;
using Casamiel.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Casamiel.Domain.Entity;

using Casamiel.Domain.Request;
using Casamiel.Common.Extensions;
using System.Linq;

namespace Casamiel.Application.Commands.Tmall
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class TmallGetOrderListCommandHandler:BaseCommandHandler,IRequestHandler<TmallGetOrderListCommand, PagingResultRsp<List<TPPTMallOrderBaseRsp>>>
    {
        public TmallGetOrderListCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> optionsSnapshot)
      : base(netICService, httpClientFactory, optionsSnapshot)
        {
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PagingResultRsp<List<TPPTMallOrderBaseRsp>>> Handle(TmallGetOrderListCommand request, CancellationToken cancellationToken)
        {
            var url = "TPP/v1/Order/GetTMallOrderList";
          //  var data = new { request.Token, request.PageIndex, request.OrderStatus, request.PageSize };
            var result = await PostThirdApiWithPagingAsync<List<TPPTMallOrderBaseRsp>>(url, JsonConvert.SerializeObject(request)).ConfigureAwait(false);
            return result;
            //var rsp = JsonConvert.DeserializeObject<JObject>(result);

            //var total = 0;
            //if (rsp["resultNo"].ToString() == "00000000")
            //{
            //    total = rsp["total"].ToString().ToInt32(0);
            //    var list = JsonConvert.DeserializeObject<List<TPP_TMallOrderBaseRsp>>(rsp["data"].ToString());
            //    return new PagingResultRsp<List<TPP_TMallOrderBaseRsp>>(list, request.PageIndex, request.PageSize, total, 0, "");//
            //}
            //else
            //{
            //    return new PagingResultRsp<List<TPP_TMallOrderBaseRsp>>(null, request.PageIndex, request.PageSize, total, 9999, rsp["resultRemark"].ToString());
            //}

        }
    }
}
