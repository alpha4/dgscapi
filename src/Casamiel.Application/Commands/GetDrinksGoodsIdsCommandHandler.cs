﻿using System;
using MediatR;
using Casamiel.Domain.Response;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System.Linq;

namespace Casamiel.Application.Commands
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetDrinksGoodsIdsCommandHandler:BaseCommandHandler,IRequestHandler<GetDrinksGoodsIdsCommand, List<string>>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="netICService"></param>
		/// <param name="settings"></param>
		/// <param name="httpClientFactory"></param>
		public GetDrinksGoodsIdsCommandHandler(INetICService netICService, IOptionsSnapshot<CasamielSettings> settings, IHttpClientFactory httpClientFactory) : base(netICService, httpClientFactory, settings)
		{
			 
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public async Task<List<string>> Handle(GetDrinksGoodsIdsCommand request, CancellationToken cancellationToken)
		{
			var list = new List<string>();
			var url = "TPP/v3/Goods/GetDrinksGoodsId";
			// string result = await PostAsync(url, "");
			var rsp=   await PostThirdApiAsync<List<TPPGoodsDrinksSkuIdRsp>>(url, "").ConfigureAwait(false);
			if (rsp.Code == 0 && rsp.Content!=null) {
				
				if (rsp.Content.Any()) {
					foreach (var item in rsp.Content) {
						list.Add($"{item.GoodsRelationId}");
					}
				}
				 
			}
			return list;
		}
	}
}
