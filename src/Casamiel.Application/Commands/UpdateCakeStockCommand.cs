﻿using System;
using MediatR;

namespace Casamiel.Application.Commands
{
    public class UpdateCakeStockCommand:IRequest<bool>
    {
        public UpdateCakeStockCommand()
        {
        }
    }
}
