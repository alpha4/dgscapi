﻿using System;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands.HappinessTree
{

    /// <summary>
    /// 领取奖励
    /// </summary>
    public sealed class GetRewardsCommand : IRequest<BaseResult<string>>
    {
        /// <summary>
        /// 手机号 会员信息
        /// </summary>
        public string Mobile { get; private set; }
        /// <summary>
        /// 奖品id
        /// </summary>
        public int RewardId { get; private set; }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.HappinessTree.GetRewardsCommand"/> class.
        /// </summary>
        /// <param name="Mobile">Mobile.</param>
        /// <param name="rewardId">Reward identifier.</param>
        public GetRewardsCommand(string Mobile,int rewardId)
        {
            this.RewardId = rewardId;
            this.Mobile = Mobile;
        }
    }
}
