﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.HappinessTree;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.HappinessTree
{
    /// <summary>
    /// Get my sign in content list command handler.
    /// </summary>
    public sealed class GetMySignInContentListCommandHandler : BaseCommandHandler, IRequestHandler<GetMySignInContentListCommand, PagingResultRsp<List<HappinessTreeResponse>>>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.HappinessTree.GetUserInfoCommandHandler"/> class.
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public GetMySignInContentListCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {

        }

        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<PagingResultRsp<List<HappinessTreeResponse>>> Handle(GetMySignInContentListCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var data = new { request.Mobile, request.PageSize, request.PageIndex };
            var rsp = await PostThirdApiWithPagingAsync<List<HappinessTreeResponse>>("v3/HappyTree/GetContentList", JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return rsp;
        }
    }
}
