﻿using System.Collections.Generic;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.HappinessTree;
using MediatR;

namespace Casamiel.Application.Commands.HappinessTree
{
    /// <summary>
    /// Liked state change command.
    /// </summary>
    public sealed class LikedStateChangeCommand:BaseCommand,IRequest<BaseResult<string>>
    {
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }

        /// <summary>
        /// 0赞，1不赞
        /// </summary>
        /// <value>The type.</value>
        public int Type { get; private set; }

        /// <summary>
        /// Gets or sets the content identifier.
        /// </summary>
        /// <value>The content identifier.</value>
        public int ContentId { get; private set; }
 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="type"></param>
        /// <param name="contentId"></param>
        /// <param name="DataSource"></param>
        public LikedStateChangeCommand(string mobile,int type,int contentId,int DataSource):base(DataSource)
        {
            this.Mobile = mobile;
            this.Type = type;
            this.ContentId = contentId;
        }
    }
}
