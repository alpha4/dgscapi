﻿using System;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.HappinessTree;
using MediatR;

namespace Casamiel.Application.Commands.HappinessTree
{
    /// <summary>
    /// Upgrade command.
    /// </summary>
    public sealed class UpgradeCommand:IRequest<BaseResult<string>>
    {
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.HappinessTree.UpgradeCommand"/> class.
        /// </summary>
        /// <param name="mobile">Mobile.</param>
        public UpgradeCommand(string mobile)
        {
            this.Mobile = mobile;
        }
    }
}
