﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.HappinessTree;
using MediatR;

namespace Casamiel.Application.Commands.HappinessTree
{
    /// <summary>
    /// Get my sign in content list command.
    /// </summary>
    public class GetMySignInContentListCommand:IRequest<PagingResultRsp<List<HappinessTreeResponse>>>
    {
        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; set; }

        /// <summary>
        /// Gets or sets the index of the page.
        /// </summary>
        /// <value>The index of the page.</value>
        public int PageIndex { get; set; }

        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; set; }
    }
}
