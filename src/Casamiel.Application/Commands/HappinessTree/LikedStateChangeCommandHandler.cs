﻿using System;
using System.Net.Http;
using Casamiel.Common;
using MediatR;
using Microsoft.Extensions.Options;
using Casamiel.Domain.Response;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.HappinessTree
{
    /// <summary>
    /// Liked state change command handler.
    /// </summary>
    public sealed class LikedStateChangeCommandHandler : BaseCommandHandler, IRequestHandler<LikedStateChangeCommand, BaseResult<String>>
    {

        public LikedStateChangeCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {

        }

        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<BaseResult<string>> Handle(LikedStateChangeCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var data = new { request.Mobile, request.ContentId, request.Type };
            var rsp = await PostThirdApiAsync<string>("v3/HappyTree/ChangeStarContent", JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return rsp;
        }

    }
}
