﻿using System;
using MediatR;
using Casamiel.Domain.Response.HappinessTree;
using Casamiel.Domain.Response;
using System.Collections.Generic;

namespace Casamiel.Application.Commands.HappinessTree
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetMyContentListCommand: IRequest<PagingResultRsp<List<HappinessTreeResponse>>>
	{
		/// <summary>
		/// 
		/// </summary>
		public string Mobile { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public int PageIndex { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public int PageSize { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="Mobile"></param>
		/// <param name="PageIndex"></param>
		/// <param name="PageSize"></param>
		public GetMyContentListCommand(string Mobile,int PageIndex,int PageSize)
		{
			this.Mobile = Mobile;
			this.PageIndex = PageIndex;
			this.PageSize = PageSize;

		}
	}
}
