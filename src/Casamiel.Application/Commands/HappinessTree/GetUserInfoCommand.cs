﻿using System;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.HappinessTree;
using MediatR;

namespace Casamiel.Application.Commands.HappinessTree
{
    /// <summary>
    /// Get user info command.
    /// </summary>
    public sealed class GetUserInfoCommand:IRequest<BaseResult<UserInfoResponse>>
    {
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public string Vesion { get; private set; }
 
        public GetUserInfoCommand(string Mobile,string Vesion= "v3")

		{
            this.Mobile = Mobile;
			this.Vesion = Vesion;
        }
    }
}
