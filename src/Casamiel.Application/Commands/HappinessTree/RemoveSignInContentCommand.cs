﻿using System;
using MediatR;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands.HappinessTree
{
    /// <summary>
    /// Remove sign in content command.
    /// </summary>
    public sealed class RemoveSignInContentCommand:IRequest<BaseResult<string>>
    {
        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; set; }

        /// <summary>
        /// Gets the content identifier.
        /// </summary>
        /// <value>The content identifier.</value>
        public int ContentId { get; private set; }

         /// <summary>
        /// 
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="contentId"></param>
        public RemoveSignInContentCommand(string mobile,int contentId)
        {
            this.Mobile = mobile;
            this.ContentId = contentId;
        }
    }
}
