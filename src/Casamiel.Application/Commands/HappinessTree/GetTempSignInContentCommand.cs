﻿using System;
using MediatR;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.HappinessTree;


namespace Casamiel.Application.Commands.HappinessTree
{
    /// <summary>
    /// Get temp sign in content command.
    /// </summary>
    public sealed class GetTempSignInContentCommand:IRequest<BaseResult<HappinessTreeResponse>>
    {
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }

       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobile"></param>
        public GetTempSignInContentCommand(string mobile)
        {
            this.Mobile = mobile;
        }
    }
}
