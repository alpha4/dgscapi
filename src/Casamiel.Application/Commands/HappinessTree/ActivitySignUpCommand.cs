﻿using System;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands.HappinessTree
{

    /// <summary>
    /// Activity sign up command.
    /// </summary>
    public sealed class ActivitySignUpCommand:IRequest<BaseResult<string>>
    {
        public string Mobile { get; set; }
        /// <summary>
        /// 父亲名字
        /// </summary>
        public string FatherName { get; set; }
        /// <summary>
        /// 母亲名称
        /// </summary>
        public string MotherName { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 小孩年龄
        /// </summary>
        public int BabyAge { get; set; }

        /// <summary>
        /// 推荐门店
        /// </summary>
        public string StoreName { get; set; } 
    }
}
