﻿using System;
using Casamiel.Domain.Response;
using MediatR;

namespace Casamiel.Application.Commands.HappinessTree
{
    /// <summary>
    /// 取消保留信息
    /// </summary>
    public sealed  class CancelTempSignInContentCommand:IRequest<BaseResult<string>>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobile"></param>
        public CancelTempSignInContentCommand(string mobile)
        {
            this.Mobile = mobile;
        }
    }
}
