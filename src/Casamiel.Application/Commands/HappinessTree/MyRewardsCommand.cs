﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.HappinessTree;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;


namespace Casamiel.Application.Commands.HappinessTree
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class MyRewardsCommand:BaseCommand,IRequest<BaseResult<List<RewardResponse>>>
    {

        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; private set; }

         
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobile"></param>
        /// <param name="DataSource"></param>
        public MyRewardsCommand(string mobile,int DataSource):base(DataSource)
        {
            this.Mobile = mobile;
        }
    }
}
