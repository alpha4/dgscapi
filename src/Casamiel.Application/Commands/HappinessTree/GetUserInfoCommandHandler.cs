﻿using System;
using System.Net.Http;
using System.Threading;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.HappinessTree;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.HappinessTree
{
    /// <summary>
    /// Get user info command handler.
    /// </summary>
    public sealed class GetUserInfoCommandHandler : BaseCommandHandler, IRequestHandler<GetUserInfoCommand, BaseResult<UserInfoResponse>>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.HappinessTree.GetUserInfoCommandHandler"/> class.
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public GetUserInfoCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {

        }

        /// <summary>
        /// Handle the specified request and cancellationToken.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async System.Threading.Tasks.Task<BaseResult<UserInfoResponse>> Handle(GetUserInfoCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var data = new { request.Mobile };
            var rsp = await PostThirdApiAsync<UserInfoResponse>($"{request.Vesion}/HappyTree/GetMbrBase", JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return rsp;
        }
    }
}
