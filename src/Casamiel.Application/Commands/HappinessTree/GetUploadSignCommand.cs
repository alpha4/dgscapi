﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.HappinessTree;
using MediatR;

namespace Casamiel.Application.Commands.HappinessTree
{
    /// <summary>
    /// 获取上传图片签名
    /// </summary>
    public class GetUploadSignCommand:BaseCommand,IRequest<BaseResult<AliyunSignResponse>>
    {

        /// <summary>
        /// 
        /// </summary>
        public   string Mobile { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mobile"></param>
        public GetUploadSignCommand(string mobile):base(1)
        {
            this.Mobile = mobile;
        }
    }
}
