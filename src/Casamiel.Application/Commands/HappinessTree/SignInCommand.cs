﻿using System;
using System.Collections.Generic;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.HappinessTree;
using MediatR;


namespace Casamiel.Application.Commands.HappinessTree
{

    /// <summary>
    /// Sign in command.
    /// </summary>
    public sealed class SignInCommand:IRequest<BaseResult<string>>
    {
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }

        /// <summary>
        /// Gets the content.
        /// </summary>
        /// <value>The content.</value>
        public string Content { get; private set; }

        /// <summary>
        /// Gets or sets the image list.
        /// </summary>
        /// <value>The image list.</value>
        public List<string> ImageList { get;  private set; }

        /// <summary>
        ///  
        /// 0 正常提交 1 保留信息
        /// </summary>
        /// <value>The type.</value>
        public int Type { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Commands.HappinessTree.SignInCommand"/> class.
        /// </summary>
        /// <param name="mobile">Mobile.</param>
        /// <param name="content">Content.</param>
        /// <param name="imagelist">Imagelist.</param>
        /// <param name="type">Type.</param>
        public SignInCommand(string mobile,string content,List<string>imagelist,int type)
        {
            this.Mobile = mobile;
            this.Content = content;
            this.ImageList = imagelist;
            this.Type = type;
        }
    }
}
