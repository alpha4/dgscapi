﻿using System;
using MediatR;
using Casamiel.Domain.Response;

namespace Casamiel.Application.Commands.HappinessTree
{
    /// <summary>
    /// Add nick name command.
    /// </summary>
    public sealed class AddNickNameCommand:BaseCommand,IRequest<BaseResult<string>>
    {
        /// <summary>
        /// Gets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; private set; }

        /// <summary>
        /// Gets the name of the nick.
        /// </summary>
        /// <value>The name of the nick.</value>
        public string NickName { get; private set; }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.HappinessTree.AddNickNameCommand"/> class.
        /// </summary>
        /// <param name="mobile">Mobile.</param>
        /// <param name="nickname">Nickname.</param>
        /// <param name="DataSource"></param>
        public AddNickNameCommand(string mobile,string nickname,int DataSource):base(DataSource)
        {
            this.Mobile = mobile;
            this.NickName = nickname;
        }
    }
}
