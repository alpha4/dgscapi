﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.HappinessTree;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.HappinessTree
{
     
    /// <summary>
    /// 
    /// </summary>
    public sealed    class MyRewardsCommandHandler : BaseCommandHandler, IRequestHandler<MyRewardsCommand, BaseResult<List<RewardResponse>>>
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="netICService"></param>
        /// <param name="httpClientFactory"></param>
        /// <param name="settings"></param>
        public MyRewardsCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async   Task<BaseResult<List<RewardResponse>>> Handle(MyRewardsCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var data = new { request.Mobile };
            var rsp = await PostThirdApiAsync<List<RewardResponse>>("v3/HappyTree/GetRewardList", JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return rsp;
        }
    }
}
