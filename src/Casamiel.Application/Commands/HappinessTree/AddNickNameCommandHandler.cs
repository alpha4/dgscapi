﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.HappinessTree
{
    public class AddNickNameCommandHandler : BaseCommandHandler, IRequestHandler<AddNickNameCommand, BaseResult<string>>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Commands.HappinessTree.GetUserInfoCommandHandler"/> class.
        /// </summary>
        /// <param name="netICService">Net ICS ervice.</param>
        /// <param name="httpClientFactory">Http client factory.</param>
        /// <param name="settings">Settings.</param>
        public AddNickNameCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="request">Request.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<BaseResult<string>> Handle(AddNickNameCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            var data = new { request.NickName, request.Mobile };
            var rsp = await PostThirdApiAsync<String>("v3/HappyTree/AddNickName", JsonConvert.SerializeObject(data)).ConfigureAwait(false);
            return rsp;
        }
    }
}
