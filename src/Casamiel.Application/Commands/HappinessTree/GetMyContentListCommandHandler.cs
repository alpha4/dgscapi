﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Casamiel.Common;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.HappinessTree;
using MediatR;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Casamiel.Application.Commands.HappinessTree
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class GetMyContentListCommandHandler:BaseCommandHandler, IRequestHandler<GetMyContentListCommand, PagingResultRsp<List<HappinessTreeResponse>>>
	{

		/// <summary>
		/// 
		/// </summary>
		/// <param name="netICService"></param>
		/// <param name="httpClientFactory"></param>
		/// <param name="settings"></param>
		public GetMyContentListCommandHandler(INetICService netICService, IHttpClientFactory httpClientFactory, IOptionsSnapshot<CasamielSettings> settings) : base(netICService, httpClientFactory, settings)
		{

		}
 

		public async Task<PagingResultRsp<List<HappinessTreeResponse>>> Handle(GetMyContentListCommand request, CancellationToken cancellationToken)
		{
			if (request is null) {
				throw new ArgumentNullException(nameof(request));
			}

			var data = new { request.Mobile,request.PageSize,request.PageIndex };
			var rsp = await PostThirdApiWithPagingAsync<List<HappinessTreeResponse>>("v3/HappyTree/GetMyContentList", JsonConvert.SerializeObject(data)).ConfigureAwait(false);
			return rsp;
		}
	}
}
