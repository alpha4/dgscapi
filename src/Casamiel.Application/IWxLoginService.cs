﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application
{
	public  interface IWxService
    {
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="appId"></param>
		/// <returns></returns>
		Task<MiniAppSettings> FindAsync<TSession>(string appId) where TSession : class, ISession;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <returns></returns>
        Task<List<Domain.Entity.MiniAppSettings>> GetMiniAppSettingsList<TSession>() where TSession : class, ISession;
    
        Task<WxOpenId> GetAsync<TSession>(string openid, string appId) where TSession : class, ISession;
        /// <summary>
        /// Gets the by union identifier async.
        /// </summary>
        /// <returns>The by union identifier async.</returns>
        /// <param name="unionId">Union identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<WxOpenId> GetByUnionIdAsync<TSession>(string unionId) where TSession : class, ISession;
        Task AddAsync<TSession>(WxOpenId entity) where TSession : class, ISession;
		Task<bool> UpdateAsync<TSession>(WxOpenId entity) where TSession : class, ISession;
		Task<WxOpenId> GetByIdAsync<TSession>(int id) where TSession : class, ISession;
        /// <summary>
        /// Updates the by moible async.
        /// </summary>
        /// <returns>The by moible async.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task UpdateByMoibleAsync<TSession>(string mobile) where TSession : class, ISession;
        /// <summary>
        /// Gets the by moible async.
        /// </summary>
        /// <returns>The by moible async.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<WxOpenId> GetByMoibleAsync<TSession>(string mobile) where TSession : class, ISession;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="appId"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        Task<WxOpenId> GetByAppIdMobileAsync<TSession>(string appId, string mobile) where TSession : class, ISession;
        /// <summary>
        /// Gets the by union identifier app identifier async.
        /// </summary>
        /// <returns>The by union identifier app identifier async.</returns>
        /// <param name="unionId">Union identifier.</param>
        /// <param name="appId">App identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<WxOpenId> GetByUnionIdAppIdAsync<TSession>(string unionId, string appId) where TSession : class, ISession;
    }
}
