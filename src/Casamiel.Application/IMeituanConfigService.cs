﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application
{
    public interface IMeituanConfigService
    {
        Task<MeituanConfig> FindAsync<TSession>(int shopid) where TSession : class, ISession;

        Task<List<MeituanConfig>> GetAllAsync<TSession>() where TSession : class, ISession;
    }
}
