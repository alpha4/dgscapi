﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Casamiel.Domain;
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application
{
	public interface IPaymentService
    {
		/// <summary>
		/// 获取支付单
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="tradeNO">商家订单号</param>
		/// <returns></returns>
		Task<PaymentRecord> FindAsync<TSession>(string tradeNO) where TSession : class, ISession;
		//void Add<TSession>(PaymentRecord record) where TSession : class, ISession;
		Task AddAsync<TSession>(PaymentRecord record) where TSession : class, ISession;
		Task AddAsync(PaymentRecord record, IUnitOfWork uow);
		//void Add(PaymentRecord record, IUnitOfWork uow);
		//bool Update<TSession>(PaymentRecord record) where TSession : class, ISession;
		Task<bool> UpdateAsync<TSession>(PaymentRecord record) where TSession : class, ISession;
		//bool Update(PaymentRecord record, IUnitOfWork unitOfWork);
		Task UpdateAsync(PaymentRecord record, IUnitOfWork unitOfWork);
		Task<List<SaTester>>GetAllTesterAsync<TSession>() where TSession : class, ISession;
		Task<PagingResult<PaymentRecord>> GetListAsync<TSession>(int pageIndex, int pageSize, string sqlwhere, string orderby) where TSession : class, ISession;

        /// <summary>
        /// 获取支付单
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="BillNO">业务单据号</param>
        /// <param name="OperationMethod">操作方式</param>
        /// <param name="State">状态</param>
        /// <returns></returns>
        Task<PaymentRecord> FindAsync<TSession>(string BillNO, int OperationMethod, int State) where TSession : class, ISession;
		/// <summary>
		/// GetByIcTradeNO
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="IcTradeNO"></param>
		/// <returns></returns>
		Task<PaymentRecord> GetByIcTradeNOAsync<TSession>(string IcTradeNO) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="OutTradeNO">外部交易号</param>
		/// <returns></returns>
		Task<PaymentRecord> FindByOutTradeNOAsync<TSession>(string OutTradeNO) where TSession : class, ISession;
        /// <summary>
        /// Gets the shop identifier and shop name by bill NOA sync.
        /// </summary>
        /// <returns>The shop identifier and shop name by bill NOA sync.</returns>
        /// <param name="billno">Billno.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<Tuple<int, string>> GetShopIdAndShopNameByBillNOAsync<TSession>(string billno) where TSession : class, ISession;

    }
}
