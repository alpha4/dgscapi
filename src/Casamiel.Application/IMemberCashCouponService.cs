﻿    using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application
{
    /// <summary>
    /// 
    /// </summary>
   public  interface IMemberCashCouponService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task UpdateAsync<TSession>(MemberCashCoupon entity) where TSession : class, ISession;

        /// <summary>
        /// 获取代金券
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="ticketCode">券码</param>
        /// <returns></returns>
        Task<MemberCashCoupon> FindAsync<TSession>(string  ticketCode) where TSession : class, ISession;

        /// <summary>
        /// Gets the list asnyc.
        /// </summary>
        /// <returns>The list asnyc.</returns>
        /// <param name="Mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<List<MemberCashCoupon>> GetListAsnyc<TSession>(string Mobile) where TSession : class, ISession;
    }
}
