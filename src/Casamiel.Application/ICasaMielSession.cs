﻿using System;
using System.Data.SqlClient;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application
{
    /// <summary>
    /// Casa miel session.
    /// </summary>
    public interface ICasaMielSession : ISession
    {
    }
       /// <summary>
       /// Casa miel session.
       /// </summary>
    public class CasaMielSession : Smooth.IoC.UnitOfWork.Abstractions.Session<SqlConnection>, ICasaMielSession
    {
           /// <summary>
           /// Initializes a new instance of the <see cref="T:Casamiel.Application.CasaMielSession"/> class.
           /// </summary>
           /// <param name="session">Session.</param>
           /// <param name="ConnectionString">Connection string.</param>
      public CasaMielSession(IDbFactory session, string ConnectionString)
          : base(session, ConnectionString)
      {
      }
    }

}