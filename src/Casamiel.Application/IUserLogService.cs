﻿using Casamiel.Domain;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Application
{
	/// <summary>
	/// 
	/// </summary>
	public interface IUserLogService
    {
		Task AddAsync<TSession>(UserLog userLog) where TSession : class, ISession;
		Task<PagingResult<UserLog>> GetListByPage<TSession>(int pageIndex, int pageSize, string sqlwhere, string orderby) where TSession : class, ISession;
		Task<IEnumerable<dynamic>> GetListByMobileAsnyc<TSession>(string mobile) where TSession : class, ISession;
	}
}
