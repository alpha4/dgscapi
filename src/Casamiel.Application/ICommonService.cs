﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Application
{
	public interface IAgentService
	{
		/// <summary>
		/// 
		/// </summary>
		//IicApiService ICService { get; }

		IMemberCardService MemberCardService { get; }
		IUserLogService UserLogService { get; }
		IPaymentService PaymentService { get; }
		IMapService MapService { get; }
		IStoreService StoreService { get; }
	}
	public class CommService : IAgentService
	{
		//private readonly IicApiService _iicApiService;
		public readonly IMemberCardService _memberCard;
		public readonly IUserLogService _userLogService;
		public readonly IPaymentService _paymentService;
		private readonly IMapService _mapService;
		private readonly IStoreService _StoreSerivce;
		/// <summary>
		/// 
		/// </summary>
		/// <param name="iicApiService"></param>
		public CommService(IMemberCardService memberCard ,IUserLogService userLogService,
			IPaymentService payment,IMapService mapService, IStoreService storeService)
		{
			//_iicApiService = iicApiService;
			_memberCard = memberCard;
			_userLogService = userLogService;
			_paymentService = payment;
			_mapService = mapService;
			_StoreSerivce = storeService;
		}

		public IUserLogService UserLogService => _userLogService;
		public IPaymentService PaymentService => _paymentService;
		public IMapService MapService => _mapService;
		public IMemberCardService MemberCardService => _memberCard;
		public IStoreService StoreService => _StoreSerivce;
	}
}
