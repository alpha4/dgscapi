﻿using Casamiel.Application.Commands.Waimai;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Application.Validations
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class CreateOrderCommandValidlidator : AbstractValidator<CreateOrderCommand>
    { 
        public CreateOrderCommandValidlidator() {
          //  RuleFor(x => x.ConsigneeId).GreaterThan(0).When(x=>x.OrderType>1).WithMessage("收货地址不能为空");
            RuleFor(x => x.Paymethod).InclusiveBetween(0, 4).WithMessage("支付方式有误");
            RuleFor(customer => customer.Id)
                .GreaterThan(0)
                .When(customer => customer.Paymethod==4).WithMessage($"小程序ID不能为空");
            //RuleFor(x => x.Id).GreaterThan(0).DependentRules(() => {
            //    RuleFor(x => x.Paymethod).Equals(4);
            //}).WithMessage($"小程序ID不能为空");

            RuleFor(x => x.PayCardNO).NotEmpty().When(x=>x.Paymethod==3).WithMessage($"PayCardNO不能为空");
        }
    }
}
