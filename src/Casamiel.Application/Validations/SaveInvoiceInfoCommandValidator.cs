﻿using Casamiel.Application.Commands.Waimai;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Application.Validations
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class SaveInvoiceInfoCommandValidator : AbstractValidator<SaveInvoiceInfoCommand>
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Validations.CreateICConsumeCodeCommandValidator"/> class.
        /// </summary>
        public SaveInvoiceInfoCommandValidator()
        {
            RuleFor(order => order.EMail).EmailAddress().WithMessage("邮箱格式不对").NotEmpty().WithMessage("邮箱不能为空");
            RuleFor(order => order.InvoiceTitle).NotEmpty().WithMessage("发票抬头不能空").Length(0,255).WithMessage("发票抬头长度超出255");
            // RuleFor(order => order.Type).NotNull().Must(TaxNotNull).WithMessage("税号不能空");
            RuleFor(x => x.Type).InclusiveBetween(0, 1).WithMessage("0,1");
            //RuleFor(x => x.TaxpayerId).NotEmpty().DependentRules(() => {
            //    RuleFor(x => x.Type).Equals(0);
            //}).WithMessage("税号不能空");
            RuleFor(order => order.Address).Length(0, 255).WithMessage("地址长度超出");
            //RuleFor(order=>order.Phone).Must(PhoneValidator).WithMessage("地址长度超出");
            //RuleFor(order => order.CardNO).NotEmpty().WithMessage("No orderId found");
            RuleFor(order => order.Phone).Custom((aa, content) => {
                if (aa.Trim().Length > 0)
                {

                    if (aa.Trim().Length != 11)
                    {
                        content.AddFailure("手机号不对");
                    }
                }

            });
        }

        private bool PhoneValidator(SaveInvoiceInfoCommand model, string phone)
        {
            if (phone.Length > 0)
            {
                if (phone.Length != 11) return false;
            }
            return true;
        }
        /// <summary>
        /// 判断新旧密码是否一样
        /// </summary>
        /// <param name="model">实体对象</param>
        /// <param name="newPwd">新密码</param>
        /// <returns>结果</returns>
        private bool TaxNotNull(SaveInvoiceInfoCommand model, int newPwd)
        {
            if (newPwd == 0)
            {
                return model.TaxpayerId.Length > 0;
            }
            return true;
        }

    }
}
