﻿using System;
using Casamiel.Application.Commands;
using Casamiel.Application.Commands.Waimai;
using Casamiel.Domain.Request.Waimai;
using FluentValidation;
using FluentValidation.Results;

namespace Casamiel.Application.Validations
{
	/// <summary>
	/// Create ICC onsume code command validator.
	/// </summary>
	public class CreateICConsumeCodeCommandValidator : AbstractValidator<CreateICConsumeCodeCommand>
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:Casamiel.Application.Validations.CreateICConsumeCodeCommandValidator"/> class.
        /// </summary>
        public CreateICConsumeCodeCommandValidator()
        {
            RuleFor(order => order.CardNO).NotEmpty().WithMessage("卡号不能空");
            //RuleFor(order => order.CardNO).NotEmpty().WithMessage("No orderId found");
        }
    }
 
}
