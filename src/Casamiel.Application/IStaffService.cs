﻿
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Threading.Tasks;

namespace Casamiel.Application
{
    /// <summary>
    /// 
    /// </summary>
	public interface IStaffService
    {
        Task<HLLogin> FindAsync<TSession>(string Mobile) where TSession : class, ISession;
        
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task AddAsync<TSession>(HLLogin entity) where TSession : class, ISession;

        Task UpdateAsync<TSession>(HLLogin entity) where TSession : class, ISession;


        Task<HlEmployee> FindEmployeeAsync<TSession>(string Mobile) where TSession : class, ISession;
    }
}
