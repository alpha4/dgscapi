﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application
{
	/// <summary>
	/// 
	/// </summary>
	public interface IMemberCouponService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="MCID"></param>
		/// <returns></returns>
		Task<MemberCoupon> FindAsync<TSession>(long MCID) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="tickectCode"></param>
		/// <returns></returns>
		Task<dynamic> GetListAsync<TSession>(string tickectCode) where TSession : class, ISession;
	}
}
