﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Application
{
    /// <summary>
    /// 
    /// </summary>
    public  interface IEmployeeService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="Mobile"></param>
        /// <returns></returns>
        Task<Mbr_employeeEntity> FindAsync<TSession>(string mobile) where TSession : class, ISession;
    }
}
