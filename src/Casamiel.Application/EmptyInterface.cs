﻿using System;
using System.Threading.Tasks;

namespace Casamiel.Application
{
    /// <summary>
    /// Integration event handler.
    /// </summary>
    public interface IIntegrationEventHandler<in TIntegrationEvent> : IIntegrationEventHandler
         where TIntegrationEvent : IntegrationEvent
    {
        Task Handle(TIntegrationEvent @event);
    }

    public  class IntegrationEvent
    {
    }

    public interface IIntegrationEventHandler
    {
    }
}
