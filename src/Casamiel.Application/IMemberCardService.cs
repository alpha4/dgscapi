﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Application
{
    /// <summary>
    /// 
    /// </summary>
	public interface IMemberCardService
    {
		Task AddAsync<TSession>(MemberCard card) where TSession : class, ISession;
        /// <summary>
        /// Adds the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="cardNO">Card no.</param>
        /// <param name="mobile">Mobile.</param>
        /// <param name="cardtype">Cardtype.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task AddAsync<TSession>(string cardNO,string mobile,string cardtype) where TSession : class, ISession;
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="mobile"></param>
		/// <param name="hasCached"></param>
		/// <returns></returns>
        Task<List<MemberCard>> GetListAsync<TSession>(string mobile, bool hasCached=true) where TSession : class, ISession;
		Task<MemberCard> FindAsync<TSession>(string cardno, string mobile) where TSession : class, ISession;
        Task<MemberCard> FindAsync<TSession>(string cardno) where TSession : class, ISession;

        Task<bool> UpdateAsync<TSession>(MemberCard entity) where TSession : class, ISession;
        /// <summary>
        /// Res the bind async.
        /// </summary>
        /// <returns>The bind async.</returns>
        /// <param name="cardNO">Card no.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<bool> ReBindAsync<TSession>(string cardNO) where TSession : class, ISession;

        /// <summary>
        /// 删除卡
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="CardNO"></param>
        /// <returns></returns>
        Task<bool> DeleteByCardNO<TSession>(string CardNO) where TSession : class, ISession;

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="mobile"></param>
        /// <param name="isGiftCard"></param>
        /// <returns></returns>
        Task<MemberCard> FindAsync<TSession>(string mobile, bool isGiftCard) where TSession : class, ISession;

        /// <summary>
        /// 绑定的卡
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="mobile"></param>
        /// <returns></returns>
        Task<MemberCard> GetBindCardAsync<TSession>(string mobile) where TSession : class, ISession;
        //Task Tes<TSession>() where TSession : class, ISession;

        //Task<MemberCard> GetWithJoins<TSession>(string key) where TSession : class, ISession;
         /// <summary>
         /// 
         /// </summary>
         /// <typeparam name="TSession"></typeparam>
         /// <param name="a"></param>
         /// <param name="b"></param>
         /// <returns></returns>
        Task<List<MemberCard>> GetListAsny<TSession>(string a, string b) where TSession : class, ISession;
         
    }
}
