﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Interface;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application
{
    public class MeituanConfigService : IMeituanConfigService
    {
        private readonly IMeituanConfigRepository _meituanConfigRepository;
        public MeituanConfigService(IMeituanConfigRepository meituanConfigRepository){
            _meituanConfigRepository = meituanConfigRepository;
        }

        /// <summary>
        /// Finds the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="shopid">Shopid.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async    Task<MeituanConfig> FindAsync<TSession>(int shopid)where TSession : class, ISession
        {
            return await _meituanConfigRepository.GetKeyAsync<TSession>(shopid);


        }
        public async Task<List<MeituanConfig>> GetAllAsync<TSession>() where TSession : class, ISession
        {
            var list = await _meituanConfigRepository.GetAllAsync<TSession>();
            return list.ToList();
        }
    }
}
