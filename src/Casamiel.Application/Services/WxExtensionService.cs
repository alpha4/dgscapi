﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application.Services
{
    public sealed class WxExtensionService : IWxExtensionService
    {

        private readonly IWxExtensionRepository _wxExtension;

        public WxExtensionService(IWxExtensionRepository wxExtension)
        {
            _wxExtension = wxExtension;
        }
        /// <summary>
        /// Adds the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="entity">Entity.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task AddAsync<TSession>(WxExtension entity) where TSession : class, ISession
        {
            await _wxExtension.SaveAsync<TSession>(entity).ConfigureAwait(false);
        }
        /// <summary>
        /// Finds the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="WxId">Wx identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task<WxExtension> FindAsync<TSession>(int WxId) where TSession : class, ISession
        {
            return await _wxExtension.GetKeyAsync<TSession>(WxId).ConfigureAwait(false);
        }
        /// <summary>
        /// Updates the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="entity">Entity.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task<bool> UpdateAsync<TSession>(WxExtension entity) where TSession : class, ISession
        {
          return  await _wxExtension.UpdateAsync<TSession>(entity).ConfigureAwait(false);
        }
        /// <summary>
        /// Gets the by mobile async.
        /// </summary>
        /// <returns>The by mobile async.</returns>
        /// <param name="Mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task<WxExtension> FindAsync<TSession>(string Mobile) where TSession : class, ISession{
            return await _wxExtension.FindAsync<TSession>(Mobile).ConfigureAwait(false);
        }
    }
}
