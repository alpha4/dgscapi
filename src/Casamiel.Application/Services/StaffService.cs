﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application.Services
{
	/// <summary>
	/// 
	/// </summary>
	public class StaffService: IStaffService
	{
		private readonly IHLLoginRepository _hLLoginRepository;
        private readonly IHlEmployeeRepository _hlEmployeeRepository;
        public StaffService(IHLLoginRepository iHLLogin, IHlEmployeeRepository hlEmployeeRepository)
		{
			_hLLoginRepository = iHLLogin;
            _hlEmployeeRepository = hlEmployeeRepository;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="Mobile"></param>
		/// <returns></returns>
		public async Task<HLLogin> FindAsync<TSession>(string Mobile) where TSession : class, ISession
		{
			return await _hLLoginRepository.FindAsync<TSession>(Mobile).ConfigureAwait(false);
		}

        public async Task  AddAsync<TSession>(HLLogin entity) where TSession : class, ISession
        {
            await _hLLoginRepository.SaveAsync<TSession>(entity).ConfigureAwait(false);
        }
 
        public async Task<HlEmployee>  FindEmployeeAsync<TSession>(string Mobile) where TSession : class, ISession
        {
            return await _hlEmployeeRepository.FindAsync<TSession>(Mobile).ConfigureAwait(false);
        }

        public async  Task UpdateAsync<TSession>(HLLogin entity) where TSession : class, ISession
        {
            await _hLLoginRepository.UpdateAsync<TSession>(entity).ConfigureAwait(false);
        }
    }
}
