﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain;
using Casamiel.Domain.Entity;

using Casamiel.Infrastructure;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application.Services
{
    /// <summary>
    /// Mobile check code service.
    /// </summary>
    public sealed class MobileCheckCodeService:IMobileCheckCodeService
    {
        private readonly IMobileCheckcodeRepository _mobileCheckcodeRepository;
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Services.MobileCheckCodeService"/> class.
        /// </summary>
        /// <param name="mobileCheckcodeRepository">Mobile checkcode repository.</param>
        public MobileCheckCodeService(IMobileCheckcodeRepository mobileCheckcodeRepository)
        {
            _mobileCheckcodeRepository = mobileCheckcodeRepository;
        }
        /// <summary>
        /// Gets the by mobile async.
        /// </summary>
        /// <returns>The by mobile async.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task<MobileCheckcode> GetByMobileAsync<TSession>(string mobile) where TSession : class, ISession
        {
            return await _mobileCheckcodeRepository.GetKeyAsync<TSession>(mobile).ConfigureAwait(false);
        }
        public Task CheckCode(string Mobile, string Yzm, bool aa)
        {
            throw new NotImplementedException();
        }
    }
}
