﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain;
using Casamiel.Infrastructure;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application.Services
{
	/// <summary>
	/// 
	/// </summary>
	public class MemberCouponService: IMemberCouponService
	{
		private readonly IMemberCouponRepository _memberCouponRepository;
		public MemberCouponService(IMemberCouponRepository memberCouponRepository)
		{
			_memberCouponRepository = memberCouponRepository;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="tickectCode"></param>
		/// <returns></returns>
		public async  Task<dynamic> GetListAsync<TSession>(string tickectCode) where TSession : class, ISession
		{
			return await _memberCouponRepository.GetListAsync<TSession>(tickectCode).ConfigureAwait(false);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="MCID"></param>
		/// <returns></returns>
		public async Task<MemberCoupon> FindAsync<TSession>(long MCID) where TSession : class, ISession
		{
			return await _memberCouponRepository.GetKeyAsync<ICasaMielSession>(MCID).ConfigureAwait(false);
		}
	}
}
