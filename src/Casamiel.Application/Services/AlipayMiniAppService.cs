﻿using System;
using Casamiel.Infrastructure;
using Casamiel.Domain;
using Casamiel.Domain.Entity;
using System.Threading.Tasks;

namespace Casamiel.Application.Services
{
	public class AlipayMiniAppService: IAlipayMiniAppService
	{
		private readonly IAlipayMiniOpenIdRepository _alipayMiniOpenIdRepository;

		public AlipayMiniAppService(IAlipayMiniOpenIdRepository alipayMiniOpenIdRepository)
		{
			_alipayMiniOpenIdRepository = alipayMiniOpenIdRepository;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
		public  async Task  AddAsync(AlipayMiniOpenId entity)
		{
			 await _alipayMiniOpenIdRepository.SaveAsync<ICasaMielSession>(entity).ConfigureAwait(false);
		}

		public async Task<AlipayMiniOpenId> FindAsync(string AppId, long UserId)
		{
			return await _alipayMiniOpenIdRepository.FindAsync<ICasaMielSession>(AppId,UserId).ConfigureAwait(false);
		}


		public async Task<AlipayMiniOpenId> FindAsync(long Id)
		{
			return await _alipayMiniOpenIdRepository.GetKeyAsync<ICasaMielSession>(Id).ConfigureAwait(false);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
		public async Task UpdateAsync(AlipayMiniOpenId entity)
		{
			await _alipayMiniOpenIdRepository.UpdateAsync<ICasaMielSession>(entity).ConfigureAwait(false);
		}
	}
}
