﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application.Services
{
	public sealed class ThirdPartyPlatformConfigService : IThirdPartyPlatformConfigService
	{
		private readonly IThirdPartyPlatformConfigRepository  _thirdPartyPlatformConfigRepository;
		/// <summary>
		/// 
		/// </summary>
		/// <param name="thirdPartyPlatformConfigRepository"></param>
		public ThirdPartyPlatformConfigService(IThirdPartyPlatformConfigRepository thirdPartyPlatformConfigRepository)
		{
			_thirdPartyPlatformConfigRepository = thirdPartyPlatformConfigRepository;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="shopid"></param>
		/// <returns></returns>
		public async	Task<ThirdPartyPlatformConfig> FindAsync<TSession>(int shopid) where TSession : class, ISession
		{
			return await _thirdPartyPlatformConfigRepository.GetKeyAsync<TSession>(shopid).ConfigureAwait(false);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="eleStoreId"></param>
		/// <returns></returns>
		public async Task<ThirdPartyPlatformConfig>FindByEleStoreIdAsync<TSession>(long eleStoreId) where TSession : class, ISession
		{
			return await _thirdPartyPlatformConfigRepository.FindByEleStoreIdAsync<TSession>(eleStoreId).ConfigureAwait(false);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <returns></returns>
		public async Task<List<ThirdPartyPlatformConfig>> GetAllAsync<TSession>() where TSession : class, ISession
		{
			var list = await _thirdPartyPlatformConfigRepository.GetAllAsync<TSession>().ConfigureAwait(false);
			return list.ToList();
		}
	}
}
