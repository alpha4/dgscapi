﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Smooth.IoC.UnitOfWork.Interfaces;
using Dapper.FastCrud;
namespace Casamiel.Application.Services
{
    /// <summary>
    /// Member coupon record service.
    /// </summary>
    public class MemberCouponRecordService: IMemberCouponRecordService
    {
        private readonly IMemberCouponRecordRepository  _recordRepository;

        public MemberCouponRecordService(IMemberCouponRecordRepository recordRepository)
        {
            _recordRepository = recordRepository;
        }
       
        /// <summary>
        /// Adds the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="entity">Entity.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async   Task   AddAsync<TSession>(MemberCouponRecord entity) where TSession : class, ISession
        {
            await _recordRepository.SaveAsync<TSession>(entity).ConfigureAwait(false);
        }

        
        /// <summary>
        /// Gets the count by activity identifier aync.
        /// </summary>
        /// <returns>The count by activity identifier aync.</returns>
        /// <param name="ActivityId">Activity identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public  async  Task<int>GetCountAsync<TSession>(int ActivityId,int flag) where TSession : class, ISession
        {
            return await _recordRepository.GetCountAsync<TSession>(ActivityId,flag).ConfigureAwait(false);
        }
        /// <summary>
        /// Gets the count by activity identifier mobile aync.
        /// </summary>
        /// <returns>The count by activity identifier mobile aync.</returns>
        /// <param name="ActivityId">Activity identifier.</param>
        /// <param name="Mobile">Mobile.</param>
		/// <param name="flag"></param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task<int>  GetCountAsync<TSession>(int ActivityId, string Mobile,int flag=0) where TSession : class, ISession
        {
            return await _recordRepository.GetCountAsync<TSession>(ActivityId,Mobile,flag).ConfigureAwait(false);
        }
    }
}
