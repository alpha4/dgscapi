﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Casamiel.Domain;

using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application.Services
{
    /// <summary>
    /// Error log service.
    /// </summary>
	public sealed class ErrorLogService : IErrorLogService
	{
		private readonly IErrorLogRepository _errorLog;
		 
		public ErrorLogService(IErrorLogRepository logRepostitory)
		{
			_errorLog = logRepostitory;
			 
		}
		public void Add<TSession>(ErrorLog errorLog) where TSession : class, ISession
		{
			_errorLog.Save<TSession>(errorLog);
		}

        /// <summary>
        /// Adds the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="errorLog">Error log.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task AddAsync<TSession>(ErrorLog errorLog) where TSession : class, ISession
        {
           await  _errorLog.SaveAsync<TSession>(errorLog).ConfigureAwait(false);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <returns></returns>
		public async Task<String> GetHappyLandAppUrlAsnyc()
		{
			return await _errorLog.GetHappyLandAppUrlAsnyc<ICasaMielSession>().ConfigureAwait(false);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <returns></returns>
		public async Task<String> GetAppDownloadUrlAsnyc(int apptype)
		{
			return await _errorLog.GetAppDownlaodUrlAsnyc<ICasaMielSession>(apptype).ConfigureAwait(false);
		}
		/// <summary>
		/// Gets the list asnyc.
		/// </summary>
		/// <returns>The list asnyc.</returns>
		/// <param name="pageIndex">Page index.</param>
		/// <param name="pageSize">Page size.</param>
		/// <typeparam name="TSession">The 1st type parameter.</typeparam>
		public async  Task<PagingResult<ErrorLog>> GetListAsnyc<TSession>(int pageIndex, int pageSize) where TSession : class, ISession {
            return await _errorLog.GetListAsync<TSession>(pageIndex, pageSize,"", "ID desc").ConfigureAwait(false);
        }
    }
}
