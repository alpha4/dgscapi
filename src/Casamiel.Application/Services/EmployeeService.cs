﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application.Services
{
    /// <summary>
    /// Employee service.
    /// </summary>
    public sealed class EmployeeService:IEmployeeService
    {
        private readonly IMbrEmployeeRepository _mbrEmployeeRepository;
        public EmployeeService(IMbrEmployeeRepository mbrEmployeeRepository)
        {
            _mbrEmployeeRepository = mbrEmployeeRepository;
        }
        /// <summary>
        /// Gets the by mobile async.
        /// </summary>
        /// <returns>The by mobile async.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async  Task<Mbr_employeeEntity>  FindAsync<TSession>(string mobile) where TSession : class, ISession
        {
            return await _mbrEmployeeRepository.FindAsync<TSession>(mobile).ConfigureAwait(false);
        }
    }
}
