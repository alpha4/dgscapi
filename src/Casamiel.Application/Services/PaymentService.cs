﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System.Threading.Tasks;

using System.Collections.Generic;
using System.Linq;
using Casamiel.Domain;
using System;

namespace Casamiel.Application.Services
{
    /// <summary>
    /// 支付服务
    /// </summary>
    public sealed class PaymentService : IPaymentService
    {
        private readonly IPaymentRecordRepository paymentRecord;
        private readonly ISaTesterRepository saTester;
        public PaymentService(IPaymentRecordRepository paymentRecordRepository,
            ISaTesterRepository saTesterRepository)
        {
            paymentRecord = paymentRecordRepository;
            saTester = saTesterRepository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="tradeNO"></param>
        /// <returns></returns>
        public async Task<PaymentRecord> FindAsync<TSession>(string tradeNO) where TSession : class, ISession
        {
            return await paymentRecord.GetKeyAsync<TSession>(tradeNO).ConfigureAwait(false);
        }

        public bool Update<TSession>(PaymentRecord record) where TSession : class, ISession
        {
            return paymentRecord.Update<TSession>(record);
        }

        public async Task<bool> UpdateAsync<TSession>(PaymentRecord record) where TSession : class, ISession
        {
            return await paymentRecord.UpdateAsync<TSession>(record).ConfigureAwait(false);
        }

        /// <summary>
        /// Update the specified record and unitOfWork.
        /// </summary>
        /// <returns>The update.</returns>
        /// <param name="record">Record.</param>
        /// <param name="unitOfWork">Unit of work.</param>
        public bool Update(PaymentRecord record, IUnitOfWork unitOfWork)
        {
            return paymentRecord.Update(record, unitOfWork);
        }

        public void Add<TSession>(PaymentRecord record) where TSession : class, ISession
        {
            paymentRecord.Save<TSession>(record);
        }

        public void Add(PaymentRecord record, IUnitOfWork uow)
        {
            paymentRecord.Save(record, uow);
        }
        public async Task AddAsync(PaymentRecord record, IUnitOfWork uow)
        {
            await paymentRecord.SaveAsync(record, uow).ConfigureAwait(true);
        }

        /// <summary>
        /// Gets all tester async.
        /// </summary>
        /// <returns>The all tester async.</returns>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task<List<SaTester>> GetAllTesterAsync<TSession>() where TSession : class, ISession
        {
            var s = await saTester.GetAllAsync<TSession>().ConfigureAwait(false);
            return s.ToList();
        }

        public async Task<PagingResult<PaymentRecord>> GetListAsync<TSession>(int pageIndex, int pageSize, string sqlwhere, string orderby) where TSession : class, ISession
        {
            return await paymentRecord.GetListAsync<TSession>(pageIndex, pageSize, sqlwhere, orderby).ConfigureAwait(false);
        }

        /// <summary>
        /// Finds the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="BillNO">Bill no.</param>
        /// <param name="OperationMethod">Operation method.</param>
        /// <param name="State">State.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task<PaymentRecord> FindAsync<TSession>(string BillNO, int OperationMethod, int State) where TSession : class, ISession
        {
            return await paymentRecord.FindAsync<TSession>(BillNO, OperationMethod, State).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets the by ic trade NOA sync.
        /// </summary>
        /// <returns>The by ic trade NOA sync.</returns>
        /// <param name="IcTradeNO">Ic trade no.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task<PaymentRecord> GetByIcTradeNOAsync<TSession>(string IcTradeNO) where TSession : class, ISession
        {
            return await paymentRecord.FindAsync<TSession>(IcTradeNO).ConfigureAwait(false);
        }

        /// <summary>
        /// Adds the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="record">Record.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task AddAsync<TSession>(PaymentRecord record) where TSession : class, ISession
        {
            await paymentRecord.SaveAsync<TSession>(record).ConfigureAwait(false);
        }

        /// <summary>
        /// Updates the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="record">Record.</param>
        /// <param name="unitOfWork">Unit of work.</param>
        public async Task UpdateAsync(PaymentRecord record, IUnitOfWork unitOfWork)
        {
            await paymentRecord.UpdateAsync(record, unitOfWork).ConfigureAwait(true);
        }


		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="OutTradeNO"></param>
		/// <returns></returns>
		public async  Task<PaymentRecord> FindByOutTradeNOAsync<TSession>(string OutTradeNO) where TSession : class, ISession
		{
			return await paymentRecord.FindByOutTradeNOAsync<TSession>(OutTradeNO).ConfigureAwait(false);
		}

		/// <summary>
		/// Gets the shop identifier and shop name by bill NOA sync.
		/// </summary>
		/// <returns>The shop identifier and shop name by bill NOA sync.</returns>
		/// <param name="billno">Billno.</param>
		/// <typeparam name="TSession">The 1st type parameter.</typeparam>
		public async Task<Tuple<int, string>> GetShopIdAndShopNameByBillNOAsync<TSession>(string billno) where TSession : class, ISession
        {
            return await paymentRecord.GetShopIdAndShopNameByBillNOAsync<TSession>(billno).ConfigureAwait(false);
        }

    }
}
