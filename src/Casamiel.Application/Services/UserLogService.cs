﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Casamiel.Domain;

using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application.Services
{
    /// <summary>
    /// User log service.
    /// </summary>
	public sealed class UserLogService : IUserLogService
	{
		private readonly IUserLogRepository userLogRepostitory;
		
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Application.Services.UserLogService"/> class.
        /// </summary>
        /// <param name="logRepostitory">Log repostitory.</param>
        public UserLogService(IUserLogRepository logRepostitory)
		{
			userLogRepostitory = logRepostitory;
		}

        /// <summary>
        /// Add the specified userLog.
        /// </summary>
        /// <param name="userLog">User log.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
		public void Add<TSession>(UserLog userLog) where TSession : class, ISession
		{
            
			userLogRepostitory.Save<TSession>(userLog);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="userLog"></param>
		/// <returns></returns>
		public async Task AddAsync<TSession>(UserLog userLog) where TSession : class, ISession
		{

            await userLogRepostitory.SaveAsync<TSession>(userLog).ConfigureAwait(false);
		}

        /// <summary>
        /// Gets the list by page.
        /// </summary>
        /// <returns>The list by page.</returns>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="sqlwhere">Sqlwhere.</param>
        /// <param name="orderby">Orderby.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
		public async Task<PagingResult<UserLog>> GetListByPage<TSession>(int pageIndex, int pageSize, string sqlwhere, string orderby) where TSession : class, ISession
		{
			return await userLogRepostitory.GetListAsync<TSession>(pageIndex, pageSize, "", "ID desc").ConfigureAwait(false);
		}

        /// <summary>
        /// Gets the list by mobile asnyc.
        /// </summary>
        /// <returns>The list by mobile asnyc.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
		public async Task<IEnumerable<dynamic>> GetListByMobileAsnyc<TSession>(string mobile) where TSession : class, ISession
		{
			return await userLogRepostitory.GetListByMobileAsnyc<TSession>(mobile).ConfigureAwait(false);
		}
	}
}
