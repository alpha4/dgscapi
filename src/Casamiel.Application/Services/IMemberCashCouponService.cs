﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class MemberCashCouponService : IMemberCashCouponService
    {
        private readonly IMemberCashCouponRepository _memberCashCouponRepository;
        public MemberCashCouponService(IMemberCashCouponRepository memberCashCouponRepository)
        {
            _memberCashCouponRepository = memberCashCouponRepository;
        }
        public async Task<MemberCashCoupon> FindAsync<TSession>(string ticketCode) where TSession : class, ISession
        {
            return await _memberCashCouponRepository.GetKeyAsync<TSession>(ticketCode).ConfigureAwait(false);
        }

        public async Task UpdateAsync<TSession>(MemberCashCoupon entity) where TSession : class, ISession
        {
            await _memberCashCouponRepository.UpdateAsync<TSession>(entity).ConfigureAwait(false);
        }

        public async Task<List<MemberCashCoupon>> GetListAsnyc<TSession>(string Mobile) where TSession : class, ISession
        {
            return await _memberCashCouponRepository.GetListAsnyc<TSession>(Mobile).ConfigureAwait(false);
        }
    }
}
