﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<挂起>", Scope = "member", Target = "~P:Casamiel.Application.Commands.TmallCreateOrderCommand.GoodsList")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("样式", "IDE1006:命名样式", Justification = "<挂起>", Scope = "member", Target = "~P:Casamiel.Application.Commands.Dada.ChangeDataStatusCommand.cancel_from")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("样式", "IDE1006:命名样式", Justification = "<挂起>", Scope = "member", Target = "~P:Casamiel.Application.Commands.Dada.ChangeDataStatusCommand.dm_name")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("样式", "IDE1006:命名样式", Justification = "<挂起>", Scope = "member", Target = "~P:Casamiel.Application.Commands.Dada.ChangeDataStatusCommand.cancel_reason")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("样式", "IDE1006:命名样式", Justification = "<挂起>", Scope = "member", Target = "~P:Casamiel.Application.Commands.Dada.ChangeDataStatusCommand.order_status")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("样式", "IDE1006:命名样式", Justification = "<挂起>", Scope = "member", Target = "~P:Casamiel.Application.Commands.Dada.ChangeDataStatusCommand.order_id")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("样式", "IDE1006:命名样式", Justification = "<挂起>", Scope = "member", Target = "~P:Casamiel.Application.Commands.Dada.ChangeDataStatusCommand.dm_mobile")]

