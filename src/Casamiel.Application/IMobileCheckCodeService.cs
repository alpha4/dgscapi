﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application
{
    /// <summary>
    /// Mobile check code service.
    /// </summary>
    public interface IMobileCheckCodeService
    {
        /// <summary>
        /// Gets the by mobile async.
        /// </summary>
        /// <returns>The by mobile async.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<MobileCheckcode> GetByMobileAsync<TSession>(string mobile) where TSession : class, ISession;
    }
}
