﻿using Casamiel.Domain;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Application
{
    /// <summary>
    /// Error log service.
    /// </summary>
	public interface IErrorLogService
    {
        /// <summary>
        /// Add the specified errorLog.
        /// </summary>
        /// <param name="errorLog">Error log.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
		void Add<TSession>(ErrorLog errorLog) where TSession : class, ISession;
        /// <summary>
        /// Adds the async.
        /// </summary>
        /// <param name="errorLog">Error log.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task AddAsync<TSession>(ErrorLog errorLog) where TSession : class, ISession;
        /// <summary>
        /// Gets the name of the store by.
        /// </summary>
        /// <returns>The store by name.</returns>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<PagingResult<ErrorLog>> GetListAsnyc<TSession>(int pageIndex, int pageSize) where TSession : class, ISession;


		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <returns></returns>
		Task<String> GetHappyLandAppUrlAsnyc();

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <returns></returns>
        Task<String> GetAppDownloadUrlAsnyc(int apptype = 5);


    }
}
