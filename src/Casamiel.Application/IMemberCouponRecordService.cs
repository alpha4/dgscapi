﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application
{
    /// <summary>
    /// Member coupon record service.
    /// </summary>
    public interface IMemberCouponRecordService
    {
        Task AddAsync<TSession>(MemberCouponRecord entity) where TSession : class, ISession;


		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="ActivityId"></param>
		/// <param name="falg"></param>
		/// <returns></returns>
        Task<int> GetCountAsync<TSession>(int ActivityId,int falg) where TSession : class, ISession;
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="ActivityId"></param>
		/// <param name="Mobile"></param>
		/// <param name="falg"></param>
		/// <returns></returns>
        Task<int> GetCountAsync<TSession>(int ActivityId, string Mobile,int falg) where TSession : class, ISession;
    }
}
