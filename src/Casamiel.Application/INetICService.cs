﻿using Casamiel.Domain.Entity;
using Casamiel.Domain.Request;
using Casamiel.Domain.Request.IC;
using Casamiel.Domain.Response;
using Casamiel.Domain.Response.IC;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Casamiel.Application
{
    /// <summary>
    /// Net ICS ervice.
    /// </summary>
    public interface INetICService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="mobile"></param>
        /// <param name="datasource"></param>
        /// <returns></returns>
        Task<ICResult> Icregist(string cardno, string mobile, int datasource);

        /// <summary>
        /// 绑定员工卡
        /// </summary>
        /// <param name="cardno"></param>
        /// <param name="mobile"></param>
        /// <param name="datasource"></param>
        /// <returns></returns>
        Task<ICResult> SaffIcregist(string cardno, string mobile, int datasource);


        /// <summary>
		/// 提货核销
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
        Task<ICResult> Icorderpickup(OrderpickupRequest data);

        /// <summary>
		/// 
		/// </summary>
		/// <param name="cardno"></param>
		/// <param name="Datasource"></param>
		/// <returns></returns>
        Task<Tuple<ICConsumeCodeRsp, int, string>> Icconsumecode(string cardno,int Datasource);

        /// <summary>
        /// Iclessquery the specified CardNO and datasource.
        /// </summary>
        /// <returns>The iclessquery.</returns>
        /// <param name="CardNO">Card no.</param>
        /// <param name="datasource">Datasource.</param>
        Task<ICResult> Iclessquery(string CardNO, int datasource);
        /// <summary>
        /// Productstockquery the specified data.
        /// </summary>
        /// <returns>The productstockquery.</returns>
        /// <param name="data">Data.</param>
        Task<ICProductStockQueryRoot> Productstockquery(ProductStockQueryReq data);

        /// <summary>
        /// 获取券码
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<ICResult> TMTicket(TMTicketRequest request);
        /// <summary>
        /// 	变更券码
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<ICResult> Tmticketchange(TmticketchangeRequest request);


        Task<ICPriceRsp> Icprice(IcPriceReq data, CancellationToken cancellationToken = default);
        /// <summary>
        /// Geticticket the specified data.
        /// </summary>
        /// <returns>The geticticket.</returns>
        /// <param name="data">Data.</param>
        Task<ICResultExt> Geticticket(GetTicticketReq data, CancellationToken token = default);


        /// <summary>
        /// 消费
        /// </summary>
        /// <returns>The icconsume.</returns>
        /// <param name="req">Req.</param>
        Task<ICResult> Icconsume(IcconsumeReq req);


        /// <summary>
        ///  通过手机号赠送会员券
        /// </summary>
        /// <returns>The icticket.</returns>
        /// <param name="data">Data.</param>
        Task<ICResult> Icticketlargess(AddTicketsReq data);

        /// <summary>
        /// 修改会员卡
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<ICResult> IcModify(IcModifyRequest request);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<ICResult> Icticketchange(IcticketchangeRequest request);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<ICResult> Icticketlargesscancel(TicketLargessCancelReq data);

        /// <summary>
        /// The icconsumeback.
        /// </summary>
        Task<ICResult> Icconsumeback(IcConsumeBackReq req);

        /// <summary>
        /// Icconsumepay the specified req.
        /// </summary>
        /// <returns>The icconsumepay.</returns>
        /// <param name="req">Req.</param>
        Task<ICResult> Icconsumepay(IcConsumepayReq req);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="icdto"></param>
        /// <returns></returns>
        Task<ICResult> Thirdorder(IcconsumeReq icdto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="shopid"></param>
        /// <param name="Ids"></param>
        /// <param name="type">0-productid,1-ticketid</param>
        /// <param name="Icticket"></param>
        /// <returns></returns>
        /// 
        Task<ICResult> TicketLimit(int shopid, List<long> Ids, int type, bool Icticket, CancellationToken cancellationToken = default);


        Task<ICResult> TicketLimitExts(int shopid, List<long> ids, int type, bool Icticket, List<ProductItem> products);

        Task<ICResult> TicketLimit(int shopid, string ticketcode);


        /// <summary>
		/// 因为大量的券的到期时间是 xxxx年xx月xx天 00:00:00 到期的，所以这个查询时间可能需是 xxxx年xx月xx天 01:00:00 到 xxxx年xx月(xx+1)天 00:59:59 比较合适
		/// </summary>
		/// <param name="Expirestartdate">券到期时间范围：起始时间</param>
		/// <param name="Expireenddate">券到期时间范围：结束时间</param>
		/// <returns></returns>
		Task<ICResult> ICTicketExpireQuery(string Expirestartdate, string Expireenddate);

        /// <summary>
        /// 	查询券码
        /// </summary>
        /// <returns></returns>
        Task<ICResult> Tmticketquery(TmticketqueryReq req);

        /// <summary>
        /// Thirdorderback the specified data.
        /// </summary>
        /// <returns>The thirdorderback.</returns>
        /// <param name="data">Data.</param>
        Task<ICResult> Thirdorderback(ThirdOrderBackReq data);

        /// <summary>
        /// 会员消息推送计划定制
        /// </summary>
        /// <param name="requst"></param>
        /// <returns></returns>
        Task<ICResult> ICPushPlan(ICPushPlanRequst requst);



        /// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
        Task<ICResult> ICtransfer(ICTransferRequest request);


        /// <summary>
		/// 
		/// </summary>
		/// <param name="mobile"></param>
		/// <param name="datasource"></param>
		/// <param name="shopid"></param>
		/// <param name="sex"></param>
		/// <returns></returns>
        Task<ICResult> Icselfregist(string mobile, int datasource, int shopid, string sex = "1");


        /// <summary>
		/// 
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
        Task<ICResult> ICconsumepayprequery(ICconsumepayprequeryRequest request);


        /// <summary>
		/// 会员卡解除和手机号的关联
		/// </summary>
		/// <param name="Cardno"></param>
		/// <param name="Mobile"></param>
		/// <returns></returns>
        Task<ICResult> IcremoveAsync(string Cardno, string Mobile);
    }
}
