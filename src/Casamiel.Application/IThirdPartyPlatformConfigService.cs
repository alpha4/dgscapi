﻿using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Application
{
	/// <summary>
	/// 
	/// </summary>
	public	interface IThirdPartyPlatformConfigService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="shopid"></param>
		/// <returns></returns>
		Task<ThirdPartyPlatformConfig> FindAsync<TSession>(int shopid) where TSession : class, ISession;
		/// <summary>
		/// 饿了么商家ID
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="eleStoreId"></param>
		/// <returns></returns>
		Task<ThirdPartyPlatformConfig> FindByEleStoreIdAsync<TSession>(long eleStoreId) where TSession : class, ISession;
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <returns></returns>
		Task<List<ThirdPartyPlatformConfig>> GetAllAsync<TSession>() where TSession : class, ISession;
	}
}
