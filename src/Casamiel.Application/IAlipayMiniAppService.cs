﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;

namespace Casamiel.Application
{
	/// <summary>
	/// 
	/// </summary>
	public interface IAlipayMiniAppService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
		Task AddAsync(AlipayMiniOpenId entity);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="AppId"></param>
		/// <param name="UserId"></param>
		/// <returns></returns>
		Task<AlipayMiniOpenId> FindAsync(string AppId, long UserId);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		Task<AlipayMiniOpenId> FindAsync(long Id);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
		Task UpdateAsync(AlipayMiniOpenId entity);
	}
}
