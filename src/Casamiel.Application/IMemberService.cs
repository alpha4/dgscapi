﻿using Casamiel.Domain;
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Application
{
	/// <summary>
	/// 
	/// </summary>
	public interface IMemberService
    {
        /// <summary>
        /// Finds the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <param name="loginType">1App,2Pc,3微信，4预定下单</param>
        /// <param name="iscache">If set to <c>true</c> iscache.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
		Task<MemberAccessToken> FindAsync<TSession>(string mobile, int loginType,bool iscache=true) where TSession : class, ISession;
		//bool Update<TSession>(MemberAccessToken token) where TSession : class, ISession;
		Task<bool> UpdateAsync<TSession>(MemberAccessToken token) where TSession : class, ISession;
		Task AddAsync<TSession>(MemberAccessToken token) where TSession : class, ISession;
		Task AddAsync<TSession>(Member member) where TSession : class, ISession;
		Task<Member> FindAsync<TSession>(string mobile,bool IsCache=false) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="pageIndex"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		Task<PagingResult<Member>> GetListAsync<TSession>(int pageIndex, int pageSize) where TSession : class, ISession;
		//bool Update<TSession>(Member member) where TSession : class, ISession;
		Task<bool> UpdateAsync<TSession>(Member member) where TSession : class, ISession;

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="ID"></param>
		/// <param name="LastLoginDate"></param>
		/// <returns></returns>
		Task<bool> UpdateLastLoginDateAsync<TSession>(long ID, DateTime LastLoginDate) where TSession : class, ISession;
	}

}
