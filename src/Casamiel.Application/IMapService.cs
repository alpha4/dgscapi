﻿using Casamiel.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Application
{
	/// <summary>
	/// 
	/// </summary>
    public interface IMapService
    {
		/// <summary>
		/// 
		/// </summary>
		/// <param name="mapfrom"></param>
		/// <param name="mapTo"></param>
		/// <returns></returns>
		double GetGistance(Map mapfrom,Map mapTo);
	}
}
