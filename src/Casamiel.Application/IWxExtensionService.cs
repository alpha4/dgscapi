﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Application
{
    /// <summary>
    /// Wx extension service.
    /// </summary>
    public interface IWxExtensionService
    {
        /// <summary>
        /// Finds the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="WxId">Wx identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<WxExtension> FindAsync<TSession>(int WxId) where TSession : class, ISession;

        /// <summary>
        /// Adds the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="entity">Entity.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task AddAsync<TSession>(WxExtension entity) where TSession : class, ISession;
        /// <summary>
        /// Updates the async.
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="entity">Entity.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<bool> UpdateAsync<TSession>(WxExtension entity) where TSession : class, ISession;

        /// <summary>
        /// Gets the by mobile async.
        /// </summary>
        /// <returns>The by mobile async.</returns>
        /// <param name="Mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        Task<WxExtension> FindAsync<TSession>(string Mobile) where TSession : class, ISession;
    }
}
