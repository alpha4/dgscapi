﻿using Casamiel.Common;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;

namespace Casamiel.Common
{
    /// <summary>
    /// 
    /// </summary>
    public static class ConsoleHelper
    {
        /// <summary>
        /// 记录日志
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <param name="context"></param>
        /// <param name="token"></param>
        public static void DoLog<T>(T item, HttpRequest context,string token="")
        {
            var json = JsonConvert.SerializeObject(item);
            var useragent = context.Headers["User-Agent"].ToString() == "" ? context.Headers["UserAgent"].ToString() : context.Headers["User-Agent"].ToString();
            Console.WriteLine($"时间：{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff")},请求路径：{context.GetAbsoluteUri()}，data:{json},User-Agent:{useragent},客户端ip:{context.GetUserIp()},token;{token}");
        }
    }
}
