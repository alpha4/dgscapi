﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Common.Http
{
    public interface IHttpClient
    {
       /// <summary>
       /// 
       /// </summary>
       /// <param name="uri"></param>
       /// <param name="authorizationToken"></param>
       /// <param name="token"></param>
       /// <param name="userAgent"></param>
       /// <param name="authorizationMethod"></param>
       /// <returns></returns>
        Task<string> GetStringAsync(string uri, string authorizationToken = null, string token = null, string authorizationMethod = "Bearer");
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="item"></param>
        /// <param name="authorizationToken"></param>
        /// <param name="token"></param>
        /// <param name="authorizationMethod"></param>
        /// <returns></returns>
        Task<HttpResponseMessage> PostAsync<T>(string uri, T item, string authorizationToken = null, string token = null,  string authorizationMethod = "Bearer");
        /// <summary>
        /// 
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="authorizationToken"></param>
        /// <param name="token"></param>
        /// <param name="authorizationMethod"></param>
        /// <returns></returns>
        Task<HttpResponseMessage> DeleteAsync(string uri, string authorizationToken = null, string token = null, string authorizationMethod = "Bearer");
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="item"></param>
        /// <param name="authorizationToken"></param>
        /// <param name="token"></param>
        /// <param name="authorizationMethod"></param>
        /// <returns></returns>
        Task<HttpResponseMessage> PutAsync<T>(string uri, T item, string authorizationToken = null, string token = null, string authorizationMethod = "Bearer");
    }
}
