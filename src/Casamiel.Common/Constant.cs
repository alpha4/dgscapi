﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Casamiel.Common
{
    /// <summary>
    /// Constant.
    /// </summary>
    public static class Constant
    {
        /// <summary>
        /// token缓存键
        /// </summary>
        public const string TOKEN_PREFIX = "t_";

		/// <summary>
		/// 幸福卡充值
		/// </summary>
		public const string ICQRCODEVERIFY = "icqrcodeverify_";
        /// <summary>
        /// 活动数量
        /// </summary>
        public const string ACTIVITYTOTAL = "ActivityIdTotal_";
        /// <summary>
        /// 卡消费码
        /// </summary>
        public const string CARDCONSUMECODE = "card_consumecode_";
        /// <summary>
        /// 请求消费码
        /// </summary>
        public const string REQCARDCONSUMECODE = "Req_consumecode_";
        /// <summary>
        /// The activitytmobile.
        /// </summary>
        public const string ACTIVITYTMOBILE = "activity_{0}m_{1}";
        /// <summary>
        ///一键申请卡
        /// </summary>
        public const string ICSELFREGIST = "Icselfregist_";

		/// <summary>
		/// 微信扩展
		/// </summary>
		public const string WxExtension = "WxExtension_";
		/// <summary>
		/// 换卡
		/// </summary>
		public const string CARDREPLACEMENT = "cardreplacement_";
        /// <summary>
        /// 会员绑卡
        /// </summary>
        public const string MEMBERBINDCARD = "mbindcard_";

		/// <summary>
		///绑券
		/// </summary>
		public const string BINDCOUPON = "bindcoupon_";

		/// <summary>
		/// 领券
		/// </summary>
		public const string GETCOUPON = "getcoupon_";


        /// <summary>
        /// 领券
        /// </summary>
        public const string GETTICKET = "getticket_";
        /// <summary>
        /// 我的会员卡
        /// </summary>
        public const string MYCARDS_PREFIX = "clist_";
        /// <summary>
        /// 消费码
        /// </summary>
        public const string CONSUMECODE = "consumecode_";
        /// <summary>
        /// 小程序配置
        /// </summary>
        public const string MINIAPPPREFIX = "miniapp_";
		/// <summary>
		/// INDEXGOODSLIST
		/// </summary>
		public const string INDEXGOODSLIST = "indexgoodslist_";
		/// <summary>
		/// SLIDELIST
		/// </summary>
		public const string SLIDELIST = "slidelist_";
        /// <summary>
        /// The drinksgoodsid.
        /// </summary>
        public const string DRINKSGOODSID = "drinksgoodsid";

        /// <summary>
        /// 新饿了么订单
        /// </summary>
        public const string NEWELMORDERID = "newelm_";

        /// <summary>
        /// The elmorderid.
        /// </summary>
        public const string ELMORDERID = "elorderid_";
        /// <summary>
        /// 美团订单完成
        /// </summary>
        public const string MEITUANORDERID = "mtorderid_";

		/// <summary>
		/// 新建美团
		/// </summary>
		public const string NEWMEITUANORDER = "newmt_";
		/// <summary>
		/// The shoprelationid.
		/// </summary>
		public const string SHOPRELATIONID = "shoprelationid_";
        /// <summary>
        /// The invoice.
        /// </summary>
        public const string INVOICE = "invoice_";
        /// <summary>
        /// 小程序access_token
        /// </summary>
        public const string MINIAPPPTOKEN = "wxtoken_";
        /// <summary>
        /// 门店编号
        /// </summary>
        public const string SHOPNO = "{0}spno_{1}";
        /// <summary>
        /// 代金券绑定
        /// </summary>
        public const string BIND_CASHCOUPON = "bind_cashcoupon_{0}";

		/// <summary>
		/// 
		/// </summary>
		public const string BOOKING_PRODUCTLIST = "b_Productlist_";

		/// <summary>
		/// 会员
		/// </summary>
		public const string MEMBERINFO = "MEMBER_";

		/// <summary>
		/// 支付单号
		/// </summary>
		public const string PAYREQUESTTRADENO = "PAYREQUESTTRADENO_";

		/// <summary>
		/// 支付请求
		/// </summary>
		public const string PAYREQUEST="PAYREQUEST_";

		/// <summary>
		/// 外卖产品列表
		/// </summary>
		public const string WAIMAIPRODUCTLIST = "WAIMAIPRODUCTLIST_";

        /// <summary>
        ///
        /// 卡合并
        /// </summary>
        public const string CARDMERGE = "CARDMERGE_";
    }
}
