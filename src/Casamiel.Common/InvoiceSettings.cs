﻿using System;
namespace Casamiel.Common
{
    /// <summary>
    /// 开票信息配置
    /// </summary>
    public class InvoiceSettings
    {
        public string ApiUrl { get; set; } //开票接口"
        /// <summary>
        /// 税率0.16
        /// </summary>
        /// <value>The taxrate.</value>
        public string Taxrate { get; set; }
        /// <summary>
        /// 开票人
        /// </summary>
        /// <value>The clerk.</value>
        public string Clerk { get; set; }

    }
}
