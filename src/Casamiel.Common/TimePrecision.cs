﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Common
{
    /// <summary>
    /// 时间精度：微秒、毫秒、秒
    /// </summary>
    public enum TimePrecision
    {
        Microsecond,
        Millisecond,
        Second
    }
}
