﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Casamiel.Common.Extensions
{
    public static  class ConvertExtension
    {
        /// <summary>
        /// object to decimal
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="defaultValue">默认值</param>
        /// <returns></returns>
        public static decimal ToDecimal(this object obj, decimal defaultValue)
        {
            if (obj == null)
            {
                return defaultValue;
            }
            return ToDecimal(obj.ToString(), defaultValue);
        }
        /// <summary>
        /// 字符串转decimal
        /// </summary>
        /// <param name="s"></param>
        /// <param name="defaultValue">默认值</param>
        /// <returns></returns>
        public static decimal ToDecimal(this string s,decimal defaultValue)
        {
            decimal result = 0M;
            if (decimal.TryParse(s, out result))
            {
                return result;
            }
            return defaultValue;
        }

		public static double ToDouble(this string s, double defaultValue)
		{
			double result = 0;
			if (double.TryParse(s, out result))
			{
				return result;
			}
			return defaultValue;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static double ToDouble(this object obj, double defaultValue)
		{
			if (obj == null)
			{
				return defaultValue;
			}
			return ToDouble(obj.ToString(), defaultValue);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static short ToInt16(this object obj, short defaultValue)
        {
            if (obj == null)
            {
                return defaultValue;
            }
            if (obj.GetType().IsEnum)
            {
                return (short)obj;
            }
            return ToInt16(obj.ToString(), defaultValue);
        }

        /// <summary>
        /// 转换为Int16，这里会将字符串true转换为1，false转换为0
        /// </summary>
        /// <param name="s"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static short ToInt16(this string s, short defaultValue)
        {
            if (!string.IsNullOrEmpty(s))
            {
                if (s.ToLower().Trim() == "true")
                {
                    return 1;
                }
                if (s.ToLower().Trim() == "false")
                {
                    return 0;
                }
                short result = 0;
                if (short.TryParse(s, out result))
                {
                    return result;
                }
            }
            return defaultValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int ToInt32(this object obj, int defaultValue)
        {
            if (obj == null)
            {
                return defaultValue;
            }
            if (obj.GetType().IsEnum)
            {
                return (int)obj;
            }
            return ToInt32(obj.ToString(), defaultValue);
        }

        /// <summary>
        /// 转换为Int32，这里会将字符串true转换为1，false转换为0
        /// </summary>
        /// <param name="s"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int ToInt32(this string s, int defaultValue)
        {
            if (!string.IsNullOrEmpty(s))
            {
                if (s.ToLower().Trim() == "true")
                {
                    return 1;
                }
                if (s.ToLower().Trim() == "false")
                {
                    return 0;
                }
                int result = 0;
                if (int.TryParse(s, out result))
                {
                    return result;
                }
            }
            return defaultValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static long ToInt64(this object obj, long defaultValue)
        {
            if (obj == null)
            {
                return defaultValue;
            }
            if (obj.GetType().IsEnum)
            {
                return (long)obj;
            }
            return ToInt64(obj.ToString(), defaultValue);
        }

        /// <summary>
        /// 转换为Int64?，这里会将字符串true转换为1，false转换为0
        /// </summary>
        /// <param name="s"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static long ToInt64( this string s, long defaultValue)
        {
            if (!string.IsNullOrEmpty(s))
            {
                if (s.ToLower().Trim() == "true")
                {
                    return 1L;
                }
                if (s.ToLower().Trim() == "false")
                {
                    return 0L;
                }
                long result = 0L;
                if (long.TryParse(s, out result))
                {
                    return result;
                }
            }
            return defaultValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static float ToSingle(this object obj, float defaultValue)
        {
            if (obj == null)
            {
                return defaultValue;
            }
            return ToSingle(obj.ToString(), defaultValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static float ToSingle(this string s, float defaultValue)
        {
            float result = 0f;
            if (float.TryParse(s, out result))
            {
                return result;
            }
            return defaultValue;
        }
        /// <summary>
        /// 日期时间转字符串
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="format">
        /// 日期格式,这里只列出部分(
        /// [ddMMMyy:15Oct12],[D:2012年11月5日],[d:2012-1-5],[F:2007年4月24日 16:30:15],[f:2007年4月24日 16:30],
        /// [G:2007-4-24 16:30:15],[g:2007-4-24 16:30],[T:16:30:15],[t:16:30],[U:2007年4月24日 8:30:15],[u:2007-04-24 16:30:15Z],
        /// [M|m:4月24日],[R|r:Tue, 24 Apr 2007 16:30:15 GMT],[Y|y:2007年4月],[O|o:2007-04-24T15:52:19.1562500+08:00],[s:2007-04-24T16:30:15])
        /// </param>
        /// <param name="cultureInfo">CultureInfo.InvariantCulture:不依赖区域性，比如要将日期转换为“15Oct12”这种格式时；CultureInfo.CurrentCulture:当前线程使用的区域</param>
        /// <returns></returns>
        public static string ToString( this DateTime dt, string format, CultureInfo cultureInfo = null)
        {
            if (cultureInfo != null)
            {
                return dt.ToString(format, cultureInfo);
            }
            return dt.ToString(format);
        }

        /// <summary>
        /// 判断字符串是否都是有空白字符组成，空字符串将返回false
        /// </summary>
        /// <returns>如果该字符串都是空白字符，返回<c>true</c> ，否则返回<c>false</c></returns>
        public static bool IsWhiteSpace(this string s)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }
            if (s.Length == 0)
            {
                return false;
            }
            for (int i = 0; i < s.Length; i++)
            {
                if (!char.IsWhiteSpace(s[i]))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
