﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Common
{

	/// <summary>
	/// 支付场景
	/// </summary>
	public enum PaymentScenario
	{
		/// <summary>
		///会员卡
		/// </summary>
		Card,
		/// <summary>
		/// 蛋糕商城
		/// </summary>
		CakeStore,
		/// <summary>
		/// 全国配
		/// </summary>
		Mall,
		/// <summary>
		/// 外卖
		/// </summary>
		Waimai,

		/// <summary>
		/// 团购
		/// </summary>
		GroupPurchase


	}
	/// 
	/// <summary>
	/// 卡类型（1实体卡，2虚拟卡,3礼品卡，4员工卡）
	/// </summary>
	public enum CardType
	{
		/// <summary>
		/// 实体卡
		/// </summary>
		Ic = 1,

		/// <summary>
		/// 电子卡
		/// </summary>
		ECard = 2,

		/// <summary>
		/// 礼品卡
		/// </summary>
		Gift = 3,

		/// <summary>
		/// 员工卡
		/// </summary>
		Satff = 4
	}
	/// <summary>
	/// 支付方式1、微信app；2、支付宝app，3会员卡,4微信小程序,5幸福卡充值
	/// </summary>
	public enum PayMethod
	{
		/// <summary>
		/// 微信app支付
		/// </summary>
		WechatPayApp = 1,

		/// <summary>
		/// 支付宝app
		/// </summary>
		Alipay = 2,

		/// <summary>
		/// 会员卡
		/// </summary>
		Card = 3,

		/// <summary>
		/// 微信小程序
		/// </summary>
		WechatMiniPay = 4,

		/// <summary>
		/// 礼品卡充值
		/// </summary>
		GiftRecharge = 5

	}

	/// <summary>
	/// 订单状态，1：新建、2:已支付、7：完成、3:已换货 、9：交易关闭
	/// </summary>

	public enum OrderStatus
	{
		/// <summary>
		/// 新建
		/// </summary>
		Create = 1,

		/// <summary>
		/// 已支付
		/// </summary>
		Paid = 2,

		/// <summary>
		/// 完成
		/// </summary>
		Completed = 7,

		/// <summary>
		/// 取消
		/// </summary>
		Cancelled=8,

		/// <summary>
		/// 交易关闭
		/// </summary>
		Closed = 9
	}

	/// <summary>
	/// 操作方式1充值，2商城,3，退款，4 全国配,5外卖,6幸福卡充值
	/// </summary>
	/// 
	public enum OperationMethod
	{
		/// <summary>
		/// 卡充值
		/// </summary>
		CardReCharge = 1,

		/// <summary>
		/// 蛋糕商城
		/// </summary>
		CakeStore = 2,

		/// <summary>
		/// 退款
		/// </summary>
		Refund = 3,

		/// <summary>
		/// 全国配
		/// </summary>
		Mall = 4,

		/// <summary>
		/// 外卖
		/// </summary>
		Waimai = 5,

		/// <summary>
		/// 幸福卡/礼品卡
		/// </summary>
		GiftReCharge = 6,

		/// <summary>
		/// 团购
		/// </summary>
		GroupPurchase=7,

		/// <summary>
		/// 预点单
		/// </summary>
		Pre=11
	}

	/// <summary>
	/// 赠予状态 0可赠送；1获赠；-1赠送中；-2已赠送
	/// </summary>
	public enum GiveStatus
	{
		/// <summary>
		/// 已赠送
		/// </summary>
		Gived = -2,

		/// <summary>
		/// 赠送中
		/// </summary>
		Giving = -1,

		/// <summary>
		/// 可赠送
		/// </summary>
		CanGive,

		/// <summary>
		/// 获赠
		/// </summary>
		Got
	}

}
