﻿namespace Casamiel.Common
{
	public class PositionModel
	{
		public double MinLat { get; set; }
		public double MaxLat { get; set; }
		public double MinLng { get; set; }
		public double MaxLng { get; set; }
	}
}