﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Infrastructure
{
	internal class SelectBuilder
	{
		public SelectBuilderData Data { get; set; }
		//protected ActionsHandler Actions { get; set; }

		public SelectBuilder()
		{
			Data = new SelectBuilderData();
			//Actions = new ActionsHandler(Data);
		}
		public string GetSqlForSelectBuilder(SelectBuilderData data)
		{
			var sql = new StringBuilder();
			//if (data.PagingCurrentPage == 1)
			//{
			//	if (data.PagingItemsPerPage == 0)
			//		sql.Append("select");
			//	else
			//		sql.Append("select top " + data.PagingItemsPerPage.ToString());
			//	sql.Append(" " + data.Select);
			//	sql.Append(" from " + data.From);
			//	if (data.WhereSql.Length > 0)
			//		sql.Append(" where " + data.WhereSql);
			//	if (data.GroupBy.Length > 0)
			//		sql.Append(" group by " + data.GroupBy);
			//	if (data.Having.Length > 0)
			//		sql.Append(" having " + data.Having);
			//	if (data.OrderBy.Length > 0)
			//		sql.Append(" order by " + data.OrderBy);
			//	return sql.ToString();
			//}
			//else
			//{
			sql.Append(" from " + data.From);
			if (data.WhereSql.Length > 0)
				sql.Append(" where " + data.WhereSql);
			if (data.GroupBy.Length > 0)
				sql.Append(" group by " + data.GroupBy);
			if (data.Having.Length > 0)
				sql.Append(" having " + data.Having);

			var pagedSql = string.Format(@"with PagedPersons as
								(
									select top 100 percent {0}, row_number() over (order by {1}) as tt
									{2}
								)
								select *
								from PagedPersons
								where tt between {3} and {4}",
										 data.Select,
										 data.OrderBy,
										 sql,"@begin","@end");
											 //data.GetFromItems(),
											 //data.GetToItems());
				return pagedSql;
			//}
		}

		//private IDbCommand GetPreparedDbCommand()
		//{
		//	if (Data.PagingItemsPerPage > 0
		//			&& string.IsNullOrEmpty(Data.OrderBy))
		//		throw new FluentDataException("Order by must defined when using Paging.");

		//	Data.Command.ClearSql.Sql(Data.Command.Data.Context.Data.FluentDataProvider.GetSqlForSelectBuilder(Data));
		//	return Data.Command;
		//}

		public SelectBuilder Select(string sql)
		{
			Data.Select += sql;
			return this;
		}

		public SelectBuilder From(string sql)
		{
			Data.From += sql;
			return this;
		}

		public SelectBuilder Where(string sql)
		{
			Data.WhereSql += sql;
			return this;
		}

		public SelectBuilder AndWhere(string sql)
		{
			if (Data.WhereSql.Length > 0)
				Data.WhereSql += " and ";
			Data.WhereSql += sql;
			return this;
		}

		public SelectBuilder OrWhere(string sql)
		{
			if (Data.WhereSql.Length > 0)
				Data.WhereSql += " or ";
			Data.WhereSql += sql;
			return this;
		}

		public SelectBuilder OrderBy(string sql)
		{
			Data.OrderBy += sql;
			return this;
		}

		public SelectBuilder GroupBy(string sql)
		{
			Data.GroupBy += sql;
			return this;
		}

		public SelectBuilder Having(string sql)
		{
			Data.Having += sql;
			return this;
		}

		public SelectBuilder Paging(int currentPage, int itemsPerPage)
		{
			Data.PagingCurrentPage = currentPage;
			Data.PagingItemsPerPage = itemsPerPage;
			return this;
		}

		//public SelectBuilder Parameter(string name, object value, DataTypes parameterType, ParameterDirection direction, int size)
		//{
		//	Data.Command.Parameter(name, value, parameterType, direction, size);
		//	return this;
		//}

		//public ISelectBuilder<TEntity> Parameters(params object[] parameters)
		//{
		//	Data.Command.Parameters(parameters);
		//	return this;
		//}
		//public List<TEntity> QueryMany(Action<TEntity, IDataReader> customMapper = null)
		//{
		//	return GetPreparedDbCommand().QueryMany<TEntity>(customMapper);
		//}

		//public List<TEntity> QueryMany(Action<TEntity, dynamic> customMapper)
		//{
		//	return GetPreparedDbCommand().QueryMany<TEntity>(customMapper);
		//}

		//public TList QueryMany<TList>(Action<TEntity, IDataReader> customMapper = null) where TList : IList<TEntity>
		//{
		//	return GetPreparedDbCommand().QueryMany<TEntity, TList>(customMapper);
		//}

		//public TList QueryMany<TList>(Action<TEntity, dynamic> customMapper) where TList : IList<TEntity>
		//{
		//	return GetPreparedDbCommand().QueryMany<TEntity, TList>(customMapper);
		//}

		//public void QueryComplexMany(IList<TEntity> list, Action<IList<TEntity>, IDataReader> customMapper)
		//{
		//	GetPreparedDbCommand().QueryComplexMany<TEntity>(list, customMapper);
		//}

		//public void QueryComplexMany(IList<TEntity> list, Action<IList<TEntity>, dynamic> customMapper)
		//{
		//	GetPreparedDbCommand().QueryComplexMany<TEntity>(list, customMapper);
		//}

		//public TEntity QuerySingle(Action<TEntity, IDataReader> customMapper = null)
		//{
		//	return GetPreparedDbCommand().QuerySingle<TEntity>(customMapper);
		//}

		//public TEntity QuerySingle(Action<TEntity, dynamic> customMapper)
		//{
		//	return GetPreparedDbCommand().QuerySingle<TEntity>(customMapper);
		//}

		//public TEntity QueryComplexSingle(Func<IDataReader, TEntity> customMapper)
		//{
		//	return GetPreparedDbCommand().QueryComplexSingle(customMapper);
		//}

		//public TEntity QueryComplexSingle(Func<dynamic, TEntity> customMapper)
		//{
		//	return GetPreparedDbCommand().QueryComplexSingle(customMapper);
		//}
	}
}
