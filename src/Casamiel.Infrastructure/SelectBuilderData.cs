﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Infrastructure
{
     /// <summary>
     /// 
     /// </summary>
    public static class CheckSqlExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static string CheckSafeSql(this String sql)
        {
			if (string.IsNullOrEmpty(sql)) {
				throw new ArgumentException("message", nameof(sql));
			}

			if (sql.Trim().StartsWith("update ", StringComparison.CurrentCultureIgnoreCase))
            {
                if (sql.Contains("where", StringComparison.CurrentCultureIgnoreCase))
                {
                    return sql;
                }
                else
                {
                    throw new ArgumentNullException(sql, "少了where 条件");
                }
            }
            if (sql.Trim().StartsWith("delete ", StringComparison.CurrentCultureIgnoreCase))
            {
                if (sql.Contains("where", StringComparison.CurrentCultureIgnoreCase))
                {
                    return sql;
                }
                else
                {
                    throw new ArgumentNullException(sql, "少了where 条件");
                }
            }
            return sql;
        }
    }
    /// <summary>
    /// Select builder data.
    /// </summary>
	public class SelectBuilderData //: BuilderData
	{
		public int PagingCurrentPage { get; set; }
		public int PagingItemsPerPage { get; set; }
		public string Having { get; set; }
		public string GroupBy { get; set; }
		public string OrderBy { get; set; }
		public string From { get; set; }
		public string Select { get; set; }
		public string WhereSql { get; set; }

		public SelectBuilderData()
		{
			Having = "";
			GroupBy = "";
			OrderBy = "";
			From = "";
			Select = "";
			WhereSql = "";
			PagingCurrentPage = 1;
			PagingItemsPerPage = 0;
		}

		internal int GetFromItems()
		{
			return (GetToItems() - PagingItemsPerPage + 1);
		}

		internal int GetToItems()
		{
			return (PagingCurrentPage * PagingItemsPerPage);
		}
	}
}
