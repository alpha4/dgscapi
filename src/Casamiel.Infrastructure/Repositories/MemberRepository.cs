﻿using Casamiel.Domain;
using Casamiel.Domain.Entity;

using Dapper;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Infrastructure.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public class MemberRepository : BaseRepository<Member, long>, IMemberRepository
    {
        public MemberRepository(IDbFactory factory) : base(factory)
        {
        }

        public async Task<Member> FindAsync<TSession>(string Mobile) where TSession : class, ISession
        {
            using (var session = Factory.Create<TSession>())
            {
                return await session.QuerySingleOrDefaultAsync<Member>($"SELECT top 1 * FROM {Sql.Table<Member>(session.SqlDialect)} where Mobile=@Mobile", new { Mobile })
                    .ConfigureAwait(false);
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="ID"></param>
		/// <param name="LastLoginDate"></param>
		/// <returns></returns>
		public async Task<bool> UpdateLastLoginDateAsync<TSession>(long ID, DateTime LastLoginDate) where TSession : class, ISession
		{
			using (var session = Factory.Create<TSession>()) {
				return await session.ExecuteAsync($"update {Sql.Table<Member>(session.SqlDialect)} set LastLoginDate=@LastLoginDate where ID=@ID", new { ID, LastLoginDate })
					.ConfigureAwait(false)>0;
			}
		}

	}
}
