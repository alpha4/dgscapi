﻿using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Casamiel.Infrastructure.Repositories
{
	public class SaTesterRepository: BaseRepository<SaTester, string>, ISaTesterRepository
	{
		public SaTesterRepository(IDbFactory factory) : base(factory)
		{
		}
	}
}
