﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Dapper;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Infrastructure.Repositories
{
	/// <summary>
	/// 
	/// </summary>
	public class HLLoginRepository : BaseRepository<HLLogin, int>, IHLLoginRepository
	{
		public HLLoginRepository(IDbFactory factory) : base(factory)
		{
		}

		public async Task<HLLogin> FindAsync<TSession>(string Mobile) where TSession : class, ISession
		{
			using (var session = Factory.Create<TSession>())
            {
                return await session.QuerySingleOrDefaultAsync<HLLogin>($"SELECT top 1 * FROM {Sql.Table<HLLogin>(session.SqlDialect)} where Mobile=@Mobile", new { Mobile })
                    .ConfigureAwait(false);
            }
		}
 
	}
}
