﻿using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Dapper;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Infrastructure.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public class HlEmployeeRepository: BaseRepository<HlEmployee,int>,IHlEmployeeRepository
    {
        public HlEmployeeRepository(IDbFactory factory) : base(factory)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="Mobile"></param>
        /// <returns></returns>
        public async   Task<HlEmployee> FindAsync<TSession>(string Mobile) where TSession : class, ISession
        {
            using (var session = Factory.Create<TSession>())
            {

                return await session.QuerySingleOrDefaultAsync<HlEmployee>($"SELECT top 1 * FROM {Sql.Table<HlEmployee>(session.SqlDialect)} where Mobile=@Mobile", new { Mobile })
                    .ConfigureAwait(false);
            }
        }
    }
}
