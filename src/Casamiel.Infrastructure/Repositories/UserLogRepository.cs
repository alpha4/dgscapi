﻿using Casamiel.Domain;
using Casamiel.Domain.Entity;

using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Casamiel.Infrastructure.Repositories
{
	/// <summary>
	/// 
	/// </summary>
	public sealed class UserLogRepository : BaseRepository<UserLog, int>, IUserLogRepository
	{
		public UserLogRepository(IDbFactory factory) : base(factory)
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="mobile"></param>
		/// <returns></returns>
		public async Task<IEnumerable<dynamic>> GetListByMobileAsnyc<TSession>(string mobile) where TSession : class, ISession
		{
            var sql = @"select username as Mobile,  CONVERT(varchar(100), OPtime, 2) as Date,'小程序'+opinfo as Info from sa_user_log where username=@mobile and url='http://api.casamiel.cn:3789/api/v1/Wxapp/MemberIcselfregist'  union 
select username as Mobile,CONVERT(varchar(100), OPtime, 2) as Date,'小程序' + opinfo as Info  from sa_user_log where username =@mobile and url = 'http://api.casamiel.cn:3789/api/v1/Wxapp/MemberBindCard'  union
    select username as Mobile,CONVERT(varchar(100), OPtime, 2) as Date,'老版本App' + opinfo as Info  from sa_user_log where username =@mobile and url = 'http://api.casamiel.cn:3789/api/v2/card/MemberBindCard'   union
      select  username as Mobile,CONVERT(varchar(100), OPtime, 2) as Date,'老版本App' + opinfo as Info from sa_user_log where username =@mobile and url = 'http://api.casamiel.cn:3789/api/v2/card/MemberIcselfregist'  union
select  Mobile,CONVERT(varchar(100), Createtime, 2) as Date,'卡号：'+CardNO as Info from member_card where createtime<'2018-04-08 16:32:54.020' and mobile=@mobile  and cardtype<3 ";

            using (var session = Factory.Create<TSession>())
			{
				return await session.QueryAsync(sql, new { mobile }).ConfigureAwait(false);
			}

		}
	}
}
