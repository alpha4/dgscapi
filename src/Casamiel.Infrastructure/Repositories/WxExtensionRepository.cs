﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Dapper;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Infrastructure.Repositories
{
    /// <summary>
    /// Wx extension repository.
    /// </summary>
    public sealed class WxExtensionRepository : BaseRepository<WxExtension, int>, IWxExtensionRepository
    {
        public WxExtensionRepository(IDbFactory factory) : base(factory)
        {
        }

        /// <summary>
        /// Gets the by mobile async.
        /// </summary>
        /// <returns>The by mobile async.</returns>
        /// <param name="Mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async  Task<WxExtension> FindAsync<TSession>(string Mobile) where TSession : class, ISession
        {
            using (var session = Factory.Create<TSession>())
            {
                return await session
                    .QuerySingleOrDefaultAsync<WxExtension>($"SELECT top 1 * FROM {Sql.Table<WxExtension>(session.SqlDialect)} where Mobile=@Mobile", new { Mobile })
                    .ConfigureAwait(false);
            }
        }
    }
}
