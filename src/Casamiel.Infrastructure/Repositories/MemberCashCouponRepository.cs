﻿using Casamiel.Domain;
using Casamiel.Domain.Entity;

using Casamiel.Domain.Response;
using Dapper;
using System.Linq;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Infrastructure.Repositories
{

    public    class MemberCashCouponRepository : BaseRepository<MemberCashCoupon, string>, IMemberCashCouponRepository
    {
        public MemberCashCouponRepository(IDbFactory factory) : base(factory)
        {
        }


        public async Task<List<MemberCashCoupon>> GetListAsnyc<TSession>(string mobile) where TSession : class, ISession
        {
            using (var session = Factory.Create<TSession>())
            {
                var list = await session.QueryAsync<MemberCashCoupon>($"SELECT * FROM {Sql.Table<MemberCashCoupon>(session.SqlDialect)} where mobile=@mobile", new { mobile })
                     .ConfigureAwait(false);
                return list.ToList();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="mobie"></param>
        /// <param name="state"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<PagingResult<MemberCashCouponRsp>> GetListAsnyc<TSession>(string mobie, int state, int pageIndex, int pageSize) where TSession : class, ISession
        {
            PagingResult<MemberCashCouponRsp> result = new PagingResult<MemberCashCouponRsp>
            {
                PageIndex = pageIndex,
                PageSize = pageSize
            };
            using (var session = Factory.Create<TSession>())
            {
                var sql = GetSelectSQL($"{Sql.Table<MemberCashCoupon>(session.SqlDialect)}", $" Mobile=@mobie and state=@state", "ID desc", pageIndex, pageSize);
                string countsql = $" select count(*) from  {Sql.Table<MemberCashCoupon>(session.SqlDialect)} where Mobile=@mobie and state=@state";
                int total = 0;
                total = await session.QueryFirstAsync<int>(countsql, new { mobie, state }).ConfigureAwait(false);
                var list = await session.QueryAsync<MemberCashCoupon>(sql.Item1, new{begin=sql.Item2,  end=sql.Item3, mobie, state }).ConfigureAwait(false);
                var dd = from x in list
                         select (new MemberCashCouponRsp
                         {
                             ID = x.ID,
                             CouponName = x.CouponName,
                             EndDate = x.EndDate.ToString("yyyy-MM-dd HH:mm:ss",System.Globalization.CultureInfo.CurrentCulture),
                             Price = x.Price,
                             StartDate = x.StartDate.ToString("yyyy-MM-dd HH:mm:ss",System.Globalization.CultureInfo.CurrentCulture),
                             State = x.State,
                             TicketCode = x.TicketCode
                         });
                result.Data = dd.ToList();
                result.Total = total;
                return result;
                //var list = await session.Connection.QueryAsync<InvoiceDetail>($"SELECT * FROM {Sql.Table<InvoiceDetail>(session.SqlDialect)} where Mobile=@Mobile and CreateTime>@CreateTime", new { Mobile, CreateTime });
                //return list.ToList();
            }
        }
    }
}
