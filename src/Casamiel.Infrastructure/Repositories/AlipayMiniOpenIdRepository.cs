﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain;
using Casamiel.Domain.Entity;
using Dapper;
using Smooth.IoC.Repository.UnitOfWork;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Infrastructure.Repositories
{
	/// <summary>
	/// 
	/// </summary>
	public class AlipayMiniOpenIdRepository : BaseRepository<AlipayMiniOpenId, long>, IAlipayMiniOpenIdRepository, IRepositoryExt<AlipayMiniOpenId, long>, IRepository<AlipayMiniOpenId, long>
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="factory"></param>
		public AlipayMiniOpenIdRepository(IDbFactory factory) : base(factory)
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="AppId"></param>
		/// <param name="UserId"></param>
		/// <returns></returns>
		public async Task<AlipayMiniOpenId> FindAsync<TSession>(string AppId,long UserId) where TSession : class, ISession
		{
			using var session = Factory.Create<TSession>();
			var entity = await session.Connection
.QueryFirstOrDefaultAsync<AlipayMiniOpenId>($"SELECT * FROM {Sql.Table<AlipayMiniOpenId>(session.SqlDialect)} where AppId=@AppId and UserId=@UserId".CheckSafeSql(), new {AppId, UserId })
.ConfigureAwait(false);
			return entity;
		}
	}
}