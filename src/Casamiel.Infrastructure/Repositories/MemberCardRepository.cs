﻿using Casamiel.Domain;
using Casamiel.Domain.Entity;

using Dapper;
using Dapper.FastCrud;
using Smooth.IoC.UnitOfWork.Abstractions;
using Smooth.IoC.UnitOfWork.Helpers;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Infrastructure.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class MemberCardRepository : BaseRepository<MemberCard, string>, IMemberCardRepository
    {
        public MemberCardRepository(IDbFactory factory) : base(factory)
        {
        }

        public async Task<MemberCard> FindAsync<TSession>(string cardno, string mobile) where TSession : class, ISession
        {
			using var uow = Factory.Create<TSession>();
			return await uow.Connection.QuerySingleOrDefaultAsync<MemberCard>($"SELECT top 1 * FROM {Sql.Table<MemberCard>(uow.SqlDialect)} where mobile=@mobile and cardno=@cardno", new { mobile, cardno })
.ConfigureAwait(false);
		}


        public async Task<bool> DeleteByCardNO<TSession>(string CardNO) where TSession : class, ISession
        {
            using var session = Factory.Create<TSession>();
            return await session.ExecuteAsync($"delete FROM {Sql.Table<MemberCard>(session.SqlDialect)} where cardno=@CardNO", new { CardNO }).ConfigureAwait(false) > 0;

        }

        public async Task<MemberCard> FindAsync<TSession>(string mobile, bool isGiftCard) where TSession : class, ISession
        {
            string[] cardtype = new string[] { "1", "2" };
            if (isGiftCard) {
                cardtype = new string[] { "3" };

            }
; using var uow = Factory.Create<TSession>();
            return await uow.Connection.QuerySingleOrDefaultAsync<MemberCard>($"SELECT top 1 * FROM {Sql.Table<MemberCard>(uow.SqlDialect)} where mobile=@mobile and cardtype in@cardtype order by isbind desc", new { mobile, cardtype })
.ConfigureAwait(false);
        }


        public async Task<MemberCard> GetBindCardAsync<TSession>(string mobile) where TSession : class, ISession
        {
            using (var uow = Factory.Create<TSession>())
            {
                return await uow.Connection.QuerySingleOrDefaultAsync<MemberCard>($"SELECT top 1 * FROM {Sql.Table<MemberCard>(uow.SqlDialect)} where mobile=@mobile and isbind=1 and cardtype in('1','2')", new { mobile })
                    .ConfigureAwait(false);
            }
        }

        public async Task<List<MemberCard>> GetListAsync<TSession>(string mobile) where TSession : class, ISession
        {
            using (var session = Factory.Create<TSession>())
            {
                var list = await session.QueryAsync<MemberCard>($"SELECT * FROM {Sql.Table<MemberCard>(session.SqlDialect)} where mobile=@mobile", new { mobile }).ConfigureAwait(false);
                return list.ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public async Task<List<MemberCard>>GetListAsny<TSession>(string a,string b) where TSession : class, ISession
        {
            using(var session = Factory.Create<TSession>())
            {
                var list = await session.Connection.QueryAsync<MemberCard>("select  * from member_card where (c_id between -1 and 0)").ConfigureAwait(false);
                return list.ToList();
            }
        }

 
            //public async Task<MemberCard> GetWithJoins<TSession>(string key) where TSession : class, ISession
            //{
            //    var entity = CreateInstanceHelper.Resolve<MemberCard>();
            //    entity.CardNo = key;
            //    using (var session = Factory.Create<TSession>())
            //    {
            //        return await session.Connection.GetAsync(entity, statement =>
            //    {
            //        statement.Include<Member>(join => join.InnerJoin());
            //    });
            //    }
            //}
            //public async Task  Tes<TSession>() where TSession : class, ISession{
            //    var sql = "select cardno,c_id  from member_card where cardtype=1 and state=0 and cardno not in(SELECT cardno FROM sa_user_log inner join member_card on username=mobile  where opinfo like '%状态：0'  and cardtype=1)";
            //    using (var session = Factory.Create<TSession>())
            //    {
            //        var list = await session.Connection.QueryAsync<cmodel>(sql);
            //        var lists = list.ToList();
            //        foreach (var item in list)
            //        {
            //            if (item.cardno == "573281492") continue;
            //            await session.Connection.ExecuteAsync($"update member_card set state=1 where C_ID=@C_ID", new { item.C_ID });
            //        }
            //    }

            //}
        }
    //public class cmodel{
    //    public int C_ID { get; set;}
    //    public string cardno { get; set; }
    //}
}
