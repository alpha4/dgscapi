﻿using Casamiel.Domain;
using Casamiel.Domain.Entity;

using Dapper;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Infrastructure.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class PaymentRecordRepository : BaseRepository<PaymentRecord, string>, IPaymentRecordRepository
    {
        public PaymentRecordRepository(IDbFactory factory) : base(factory)
        {
        }
        /// <summary>
        /// Gets the by bll NOA sync.
        /// </summary>
        /// <returns>The by bll NOA sync.</returns>
        /// <param name="BillNO">Bill no.</param>
        /// <param name="OperationMethod">Operation method.</param>
        /// <param name="State">State.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task<PaymentRecord> FindAsync<TSession>(string BillNO, int OperationMethod, int State) where TSession : class, ISession
        {
            using (var session = Factory.Create<TSession>())
            {
                return await session.QuerySingleOrDefaultAsync<PaymentRecord>($"SELECT top 1 * FROM {Sql.Table<PaymentRecord>(session.SqlDialect)} where BillNO=@BillNO and State=@State and OperationMethod=@OperationMethod", new { BillNO, OperationMethod, State })
                    .ConfigureAwait(false);

            }
        }
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="IcTradeNO"></param>
		/// <returns></returns>
		public async Task<PaymentRecord> FindAsync<TSession>(string IcTradeNO) where TSession : class, ISession
		{
			using (var session = Factory.Create<TSession>())
			{
				return await session.QuerySingleOrDefaultAsync<PaymentRecord>($"SELECT top 1 * FROM {Sql.Table<PaymentRecord>(session.SqlDialect)} where IcTradeNO=@IcTradeNO", new { IcTradeNO})
                    .ConfigureAwait(false);
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="IcTradeNO"></param>
		/// <returns></returns>
		public async Task<PaymentRecord> FindByOutTradeNOAsync<TSession>(string OutTradeNO) where TSession : class, ISession
		{
			using (var session = Factory.Create<TSession>()) {
				return await session.QuerySingleOrDefaultAsync<PaymentRecord>($"SELECT top 1 * FROM {Sql.Table<PaymentRecord>(session.SqlDialect)} where OutTradeNO=@OutTradeNO", new { OutTradeNO })
					.ConfigureAwait(false);
			}
		}

		/// <summary>
		/// Gets the shop identifier and shop name by bill NOA sync.
		/// </summary>
		/// <returns>The shop identifier and shop name by bill NOA sync.</returns>
		/// <param name="billno">Billno.</param>
		/// <typeparam name="TSession">The 1st type parameter.</typeparam>
		public async Task<Tuple<int,string>>GetShopIdAndShopNameByBillNOAsync<TSession>(string billno) where TSession : class, ISession
        {
            StoreEntity entity;
            var sql = "select top 1  s.relationid,s.substorename from order_Base O inner join mbr_store s on o.storeid=s.storeid where O.OrderCode=@billno";
            using (var session = Factory.Create<TSession>())
            {
                entity =await session.QuerySingleOrDefaultAsync<StoreEntity>(sql, new { billno })
                    .ConfigureAwait(false);
            }
            if(entity!=null){
                return new Tuple<int, string>(entity.RelationId, entity.SubStoreName);
            }
            return new Tuple<int, string>(0, "");
        }
	}
}
