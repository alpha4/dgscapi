﻿using Casamiel.Domain;
using Casamiel.Domain.Entity;
using Casamiel.Domain.Interface;
using Dapper;
using Smooth.IoC.UnitOfWork.Abstractions;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Infrastructure.Repositories
{
    public class MeituanConfigRepository:BaseRepository<MeituanConfig, int>, IMeituanConfigRepository
    {
        public MeituanConfigRepository(IDbFactory factory) : base(factory)
        {
        }


    }
}
