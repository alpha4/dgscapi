﻿using Casamiel.Domain;
using Casamiel.Domain.Entity;

using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Dapper;
using System.Threading.Tasks;

namespace Casamiel.Infrastructure.Repositories
{
	/// <summary>
	/// 
	/// </summary>
	public  sealed class MiniAppSettingsRepository : BaseRepository<MiniAppSettings,string>, IMiniAppSettingsRepository
	{
		public MiniAppSettingsRepository(IDbFactory factory) : base(factory)
		{
		}
		public async Task<MiniAppSettings> FindAsync<TSession>(string appId) where TSession : class, ISession
		{
			using (var session = Factory.Create<TSession>())
			{
				return await session.Connection.QuerySingleOrDefaultAsync<MiniAppSettings>($"SELECT top 1 * FROM {Sql.Table<MiniAppSettings>(session.SqlDialect)} where appId=@appId", new { appId })
                    .ConfigureAwait(false);
			}
		}
	}
}
