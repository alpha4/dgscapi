﻿using Casamiel.Domain;
using Casamiel.Domain.Entity;

using Dapper;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Infrastructure.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public class ShoppingCartRepository : BaseRepository<Mbr_shoppingCartEntity, int>, IShoppingCartRepository
    {
        public ShoppingCartRepository(IDbFactory factory) : base(factory)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="storeId"></param>
        /// <param name="mobile"></param>
        /// <param name="goodsBaseId"></param>
        /// <returns></returns>
        public async Task<Mbr_shoppingCartEntity> FindAsync<TSession>(int storeId, string mobile, int goodsBaseId) where TSession : class, ISession
        {
            using (var uow = Factory.Create<TSession>())
            {
                return await uow
                    .QuerySingleOrDefaultAsync<Mbr_shoppingCartEntity>($"SELECT top 1 * FROM {Sql.Table<Mbr_shoppingCartEntity>(uow.SqlDialect)} where mobile=@mobile and storeId=@storeId and goodsBaseId=@goodsBaseId", new { mobile, storeId,goodsBaseId })
                    .ConfigureAwait(false);
            }
        }
    }
 }
