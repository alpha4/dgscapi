﻿
using System;
using System.Collections.Generic;
using System.Text;
using Casamiel.Domain;
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;
using System.Threading.Tasks;
using Dapper;

namespace Casamiel.Infrastructure.Repositories
{
	/// <summary>
	/// 
	/// </summary>
	public class ThirdPartyPlatformConfigRepository : BaseRepository<ThirdPartyPlatformConfig, int>, IThirdPartyPlatformConfigRepository
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="factory"></param>
		public ThirdPartyPlatformConfigRepository(IDbFactory factory) : base(factory)
		{
		}
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="eleStoreId"></param>
		/// <returns></returns>
		public async Task<ThirdPartyPlatformConfig> FindByEleStoreIdAsync<TSession>(long eleStoreId) where TSession : class, ISession
		{
			using (var session = Factory.Create<TSession>())
			{
				var entity = await session
                    .QuerySingleOrDefaultAsync<ThirdPartyPlatformConfig>($"SELECT top 1 * FROM { Sql.Table<ThirdPartyPlatformConfig>(session.SqlDialect)} where EleStoreId=@eleStoreId", new { eleStoreId })
                    .ConfigureAwait(false);
				return entity;
			}
		}
	}
}
