﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Dapper;
using Smooth.IoC.UnitOfWork.Interfaces;
namespace Casamiel.Infrastructure.Repositories
{
    /// <summary>
    /// Mbr extend info repository.
    /// </summary>
    public class MbrExtendInfoRepository: BaseRepository<MbrExtendInfoEntity,int>, IMbrExtendInfoRepository
    {

        /// <summary>
		/// using Casamiel.Domain;
		/// </summary>
		/// <param name="factory"></param>
        public MbrExtendInfoRepository(IDbFactory factory) : base(factory)
        {
        }
      
    }
}
