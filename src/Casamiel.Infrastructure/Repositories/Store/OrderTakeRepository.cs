﻿using Casamiel.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;

using Smooth.IoC.UnitOfWork.Interfaces;
using Casamiel.Domain;
using System.Threading.Tasks;
using Dapper;

namespace Casamiel.Infrastructure.Repositories
{
    /// <summary>
    /// Order take repository.
    /// </summary>
    public sealed class OrderTakeRepository : BaseRepository<Order_TakeEntity, int>, IOrderTakeRepository
    {
        public OrderTakeRepository(IDbFactory factory) : base(factory)
        {
        }

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <returns>The status.</returns>
        /// <param name="OrderBaseId">Order base identifier.</param>
        /// <param name="Status">Status.</param>
        /// <param name="uow">Uow.</param>
        public async Task<bool> UpdateStatus(int OrderBaseId, int Status,string billno, IUnitOfWork uow)
        {
			if (uow is null) {
				throw new ArgumentNullException(nameof(uow));
			}

			var sql = $"update  {Sql.Table<Order_TakeEntity>(uow.SqlDialect)} set Status=@Status,billno=@billno where OrderBaseId=@OrderBaseId";
			var num = await uow.Connection.ExecuteAsync(sql.CheckSafeSql(), new { Status, billno, OrderBaseId }, uow.Transaction).ConfigureAwait(true);
            return num> 0;
        }

        /// <summary>
        /// Gets the by order code.
        /// </summary>
        /// <returns>The by order code.</returns>
        /// <param name="orderCode">Order code.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task<Order_TakeEntity>GetByOrderBaseIdAsync<TSession>(int OrderBaseId)where TSession : class, ISession{
            using (var session = Factory.Create<TSession>())
            {
                var sql = $"select top 1 * from  {Sql.Table<Order_TakeEntity>(session.SqlDialect)} where OrderBaseId=@OrderBaseId";
				
				return await session.QuerySingleOrDefaultAsync<Order_TakeEntity>(sql.CheckSafeSql(), new {OrderBaseId}).ConfigureAwait(false);
            }
        }
    }
}
