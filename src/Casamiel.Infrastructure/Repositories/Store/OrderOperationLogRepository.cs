﻿using System;
using Casamiel.Domain;
using Casamiel.Domain.Entity;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Infrastructure.Repositories
{
	/// <summary>
	/// 
	/// </summary>
	public class OrderOperationLogRepository : BaseRepository<OrderOperationLog, int>, IOrderOperationLogRepository
	{
		public OrderOperationLogRepository(IDbFactory factory) : base(factory)
		{
		}
	}

}
