﻿using Casamiel.Domain.Entity;

using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Casamiel.Domain;
namespace Casamiel.Infrastructure.Repositories
{
    /// <summary>
    /// Product tagbase repository.
    /// </summary>
	public  class ProductTagbaseRepository : BaseRepository<Product_tagbaseEntity, int>, IProductTagbaseRepository
	{
		public ProductTagbaseRepository(IDbFactory factory) : base(factory)
		{
		
		}

		public async Task<List<Product_tagbaseEntity>> GetListByStateAsync<TSession>(int status, string orderby) where TSession : class, ISession
		{
			using (var session = Factory.Create<TSession>())
			{
				var list = await session.QueryAsync<Product_tagbaseEntity>($"SELECT * FROM {Sql.Table<Product_tagbaseEntity>(session.SqlDialect)} WHERE status=@status order by {orderby}", new { status})
                    .ConfigureAwait(false);
				return list.ToList();
			}
		}
	}
}
