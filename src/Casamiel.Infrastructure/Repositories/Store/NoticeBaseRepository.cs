﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Dapper;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Infrastructure.Repositories
{
    public class NoticeBaseRepository : BaseRepository<NoticeBaseEntity, int>, INoticeBaseRepository
    {
        public NoticeBaseRepository(IDbFactory factory) : base(factory)
        {
        }
        /// <summary>
        /// Updates the by relation identifier async.
        /// </summary>
        /// <returns>The by relation identifier async.</returns>
        /// <param name="RelationId">Relation identifier.</param>
        /// <param name="Status">Status.</param>
        /// <param name="uow">Uow.</param>
        public async Task UpdateByRelationIdAsync(int RelationId, int Status, IUnitOfWork uow)
        {
			if (uow is null) {
				throw new ArgumentNullException(nameof(uow));
			}

			var sql = $"update  {Sql.Table<NoticeBaseEntity>(uow.SqlDialect)} set   Status=@Status where RelationId=@RelationId";
			
			await uow.Connection.ExecuteAsync(sql.CheckSafeSql(), new { Status, RelationId, }, uow.Transaction).ConfigureAwait(true);
        }
    }
}
