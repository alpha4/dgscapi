﻿using Casamiel.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;

using Smooth.IoC.UnitOfWork.Interfaces;
using Casamiel.Domain;

namespace Casamiel.Infrastructure.Repositories
{
    public  sealed class OrderGoodsTakeRepository : BaseRepository<Order_GoodsTakeEntity, int>, IOrderGoodsTakeRepository
    {
        public OrderGoodsTakeRepository(IDbFactory factory) : base(factory)
        {
        }
    }
}
