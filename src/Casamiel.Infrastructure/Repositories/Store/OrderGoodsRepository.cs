﻿using Casamiel.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;

using Smooth.IoC.UnitOfWork.Interfaces;
using Casamiel.Domain;
using System.Threading.Tasks;
using Dapper;
using System.Linq;

namespace Casamiel.Infrastructure.Repositories
{
    public class OrderGoodsRepository : BaseRepository<Order_GoodsEntity, int>, IOrderGoodsRepository
    {
        public OrderGoodsRepository(IDbFactory factory) : base(factory)
        {
        }
        /// <summary>
        /// Gets the by store identifier async.
        /// </summary>
        /// <returns>The by store identifier async.</returns>
        /// <param name="OrderBaseId">Order base identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task<List<Order_GoodsEntity>> GetByOrderBaseIdAsync<TSession>(int OrderBaseId) where TSession : class, ISession
        {
            using (var session = Factory.Create<TSession>())
            {
                string sql = $"select * from { Sql.Table<Order_GoodsEntity>(session.SqlDialect)} where OrderBaseId=@OrderBaseId";
                var list= await session.QueryAsync<Order_GoodsEntity>(sql, new { OrderBaseId }).ConfigureAwait(false);
                return list.ToList();
            }
        }

    }
}
