﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Dapper;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Infrastructure.Repositories
{
    /// <summary>
    /// Mbr employee repository.
    /// </summary>
    public class MbrEmployeeRepository : BaseRepository<Mbr_employeeEntity, int>, IMbrEmployeeRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Casamiel.Infrastructure.Repositories.MbrEmployeeRepository"/> class.
        /// </summary>
        /// <param name="factory">Factory.</param>
        public MbrEmployeeRepository(IDbFactory factory) : base(factory)
        {
        }
        /// <summary>
        /// Gets the by store mobile async.
        /// </summary>
        /// <returns>The by store mobile async.</returns>
        /// <param name="mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task<Mbr_employeeEntity>FindAsync<TSession>(string mobile)where TSession : class, ISession
        {
            using (var session = Factory.Create<TSession>())
            {
                    string sql = $"select * from { Sql.Table<Mbr_employeeEntity>(session.SqlDialect)} where mobile=@mobile";
                    return await session.QueryFirstOrDefaultAsync<Mbr_employeeEntity>(sql, new { mobile }).ConfigureAwait(false);
            }
        }
    }
}
