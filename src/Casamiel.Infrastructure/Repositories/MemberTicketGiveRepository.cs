﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Dapper;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Infrastructure.Repositories
{
	/// <summary>
	/// 
	/// </summary>
	public class MemberTicketGiveRepository : BaseRepository<MemberTicketGive, long>, IMemberTicketGiveRepository
	{
		public MemberTicketGiveRepository(IDbFactory factory) : base(factory)
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="ForwardCode"></param>
		/// <returns></returns>
		public async Task<MemberTicketGive> FindAsync<TSession>(string ForwardCode) where TSession : class, ISession
		{
			using (var session = Factory.Create<TSession>()) {
				var entity = await session.QuerySingleOrDefaultAsync<MemberTicketGive>($"SELECT * FROM {Sql.Table<MemberTicketGive>(session.SqlDialect)} where ForwardCode=@ForwardCode", new { ForwardCode }).ConfigureAwait(false);
				return entity;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="MCID"></param>
		/// <param name="Status"></param>
		/// <param name="uow"></param>
		/// <returns></returns>
		public async Task UpdateAsync(long MTGID, int Status, IUnitOfWork uow)
		{
			if (uow is null) {
				throw new ArgumentNullException(nameof(uow));
			}
			var LastTime = DateTime.Now;
			var sql = $"update  {Sql.Table<MemberTicketGive>(uow.SqlDialect)} set  Status=@Status,LastTime=@LastTime where MTGID=@MTGID";

			await uow.Connection.ExecuteAsync(sql.CheckSafeSql(), new { MTGID, Status, LastTime }, uow.Transaction).ConfigureAwait(true);
		}
	}
}
