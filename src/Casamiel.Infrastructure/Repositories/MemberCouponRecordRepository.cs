﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Dapper;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Infrastructure.Repositories
{
    /// <summary>
    /// Member coupon record repository.
    /// </summary>
    public sealed class MemberCouponRecordRepository : BaseRepository<MemberCouponRecord, int>, IMemberCouponRecordRepository
    {
        public MemberCouponRecordRepository(IDbFactory factory) : base(factory)
        {
        }

        /// <summary>
        /// Gets the count by activity identifier aync.
        /// </summary>
        /// <returns>The count by activity identifier aync.</returns>
        /// <param name="ActivityId">Activity identifier.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task<int> GetCountAsync<TSession>(int ActivityId,int flag) where TSession : class, ISession
        {
            using (var session = Factory.Create<TSession>())
            {
				if (flag == 0) {
					return await session.QuerySingleOrDefaultAsync<int>($"SELECT count(0) FROM {Sql.Table<MemberCouponRecord>(session.SqlDialect)} where ActivityId=@ActivityId and Status=1", new { ActivityId })
									   .ConfigureAwait(false);
				} else {
					var CreateTime = DateTime.Now.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);

					return await session.QuerySingleOrDefaultAsync<int>($"SELECT count(0) FROM {Sql.Table<MemberCouponRecord>(session.SqlDialect)} where ActivityId=@ActivityId and CreateTime>@CreateTime and Status=1", new { ActivityId, CreateTime })
			   .ConfigureAwait(false);   
				}
                 
            }
        }
        /// <summary>
        /// Gets the count by activity identifier mobile aync.
        /// </summary>
        /// <returns>The count by activity identifier mobile aync.</returns>
        /// <param name="ActivityId">Activity identifier.</param>
        /// <param name="Mobile">Mobile.</param>
        /// <typeparam name="TSession">The 1st type parameter.</typeparam>
        public async Task<int> GetCountAsync<TSession>(int ActivityId, string Mobile) where TSession : class, ISession
        {
			return await GetCountAsync<TSession>(ActivityId, Mobile, 0).ConfigureAwait(false);

		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="ActivityId"></param>
		/// <param name="Mobile"></param>
		/// <param name="flag">0当次活动，1每天</param>
		/// <returns></returns>
		public async Task<int> GetCountAsync<TSession>(int ActivityId, string Mobile,int flag) where TSession : class, ISession
		{
			using (var session = Factory.Create<TSession>()) {
				if (flag == 0) {
					return await session.QuerySingleOrDefaultAsync<int>($"SELECT count(0) FROM {Sql.Table<MemberCouponRecord>(session.SqlDialect)} where ActivityId=@ActivityId and Mobile=@Mobile and Status=1", new { ActivityId, Mobile })
					.ConfigureAwait(false);
				} else {
					var CreateTime = DateTime.Now.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);

					string sql = $"SELECT count(0) FROM {Sql.Table<MemberCouponRecord>(session.SqlDialect)} where ActivityId=@ActivityId and Mobile=@Mobile and CreateTime>@CreateTime and Status=1";
					return await session.QuerySingleOrDefaultAsync<int>(sql.CheckSafeSql(), new { ActivityId, Mobile, CreateTime })
					.ConfigureAwait(false);
				}
				
			}
		}

		public async Task<MemberCouponRecord> FindAsync<TSession>(int ActivityId, string CardNO) where TSession : class, ISession
        {
            using (var session = Factory.Create<TSession>())
            {
                return await session.QuerySingleOrDefaultAsync<MemberCouponRecord>($"SELECT  top 1 * FROM {Sql.Table<MemberCouponRecord>(session.SqlDialect)} where ActivityId=@ActivityId and CardNO=@CardNO", new { ActivityId, CardNO="%"+CardNO })
                    .ConfigureAwait(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="ActivityId"></param>
        /// <param name="OrderCode"></param>
        /// <returns></returns>
        public async Task<MemberCouponRecord> FindByCorderCodeAsync<TSession>(int ActivityId, string OrderCode) where TSession : class, ISession
        {
            using (var session = Factory.Create<TSession>())
            {
                return await session.QuerySingleOrDefaultAsync<MemberCouponRecord>($"SELECT  top 1 * FROM {Sql.Table<MemberCouponRecord>(session.SqlDialect)} where ActivityId=@ActivityId and OrderCode=@OrderCode and Status=1", new { ActivityId, OrderCode })
                    .ConfigureAwait(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSession"></typeparam>
        /// <param name="ActivityId"></param>
        /// <returns></returns>
        public async Task<List<MemberCouponRecord>> GetListAsync<TSession>(int ActivityId) where TSession : class, ISession
        {
            using (var session = Factory.Create<TSession>())
            {
                var list= await session.QueryAsync<MemberCouponRecord>($"SELECT top 100    * FROM {Sql.Table<MemberCouponRecord>(session.SqlDialect)} where ActivityId=@ActivityId   and Status=1 and createtime<'2019-5-29' order by 1 desc".CheckSafeSql(), new { ActivityId })
                    .ConfigureAwait(false);
                return list.ToList();
            }
        }

    }
}
