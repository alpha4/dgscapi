﻿using System;
using Casamiel.Domain.Entity;
using Casamiel.Domain;
using Smooth.IoC.UnitOfWork.Interfaces;

namespace Casamiel.Infrastructure.Repositories
{
    /// <summary>
    /// Mobile check code repository.
    /// </summary>
    public class MobileCheckCodeRepository: BaseRepository<MobileCheckcode, string>, IMobileCheckcodeRepository
    {
        public MobileCheckCodeRepository(IDbFactory factory) : base(factory)
        {
        }

    }
}
