﻿using System;
using System.Threading.Tasks;
using Casamiel.Domain;
using Smooth.IoC.UnitOfWork.Interfaces;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace Casamiel.Infrastructure.Repositories
{
	/// <summary>
	/// 
	/// </summary>
	public class MemberCouponGiveRepository : BaseRepository<MemberCouponGive, long>, IMemberCouponGiveRepository
	{
		public MemberCouponGiveRepository(IDbFactory factory) : base(factory)
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="ForwardCode"></param>
		/// <returns></returns>
		public async Task<MemberCouponGive> FindAsync<TSession>(string ForwardCode) where TSession : class, ISession
		{
			using (var session = Factory.Create<TSession>()) {
				var entity = await session.QuerySingleOrDefaultAsync<MemberCouponGive>($"SELECT * FROM {Sql.Table<MemberCouponGive>(session.SqlDialect)} where ForwardCode=@ForwardCode", new { ForwardCode }).ConfigureAwait(false);
				return entity;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="MID"></param>
		/// <param name="Status">0发起转赠；1转赠成功，-1转赠超时</param>
		/// <param name="IsGreaterThan">是否大于dt</param>
		/// <param name="dt"></param>
		/// <returns></returns>
		public async Task<List<MemberCouponGive>>GetListAsync<TSession>(long MID,int Status,bool IsGreaterThan,DateTime dt) where TSession : class, ISession
		{
			using (var session = Factory.Create<TSession>()) {
				string sql;
				if (IsGreaterThan) {
					sql = $"SELECT * FROM {Sql.Table<MemberCouponGive>(session.SqlDialect)} where MID=@MID and Status=@Status and CreateTime>@CreateTime";
				} else {
					sql = $"SELECT * FROM {Sql.Table<MemberCouponGive>(session.SqlDialect)} where MID=@MID and Status=@Status and CreateTime<=@CreateTime";

				}
				var list = await session.QueryAsync<MemberCouponGive>(sql.CheckSafeSql(), new { MID,Status, CreateTime=dt }).ConfigureAwait(false);
				return list.ToList();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSession"></typeparam>
		/// <param name="MID"></param>
		/// <param name="MCID"></param>
		/// <param name="Status">0发起转赠；1转赠成功，-1转赠超时</param>
		/// <param name="IsGreaterThan"></param>
		/// <param name="dt"></param>
		/// <returns></returns>
		public async Task<List<MemberCouponGive>> GetListAsync<TSession>(long MID,long MCID, int Status, bool IsGreaterThan, DateTime dt) where TSession : class, ISession
		{
			using (var session = Factory.Create<TSession>()) {
				string sql;
				if (IsGreaterThan) {
					sql = $"SELECT * FROM {Sql.Table<MemberCouponGive>(session.SqlDialect)} where MCID=@MCID and MID=@MID and Status=@Status and CreateTime>@CreateTime";
				} else {
					sql = $"SELECT * FROM {Sql.Table<MemberCouponGive>(session.SqlDialect)} where MCID=@MCID and MID=@MID and Status=@Status and CreateTime<=@CreateTime";

				}
				var list = await session.QueryAsync<MemberCouponGive>(sql.CheckSafeSql(), new { MID, MCID, Status, CreateTime = dt }).ConfigureAwait(false);
				return list.ToList();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="MCID"></param>
		/// <param name="Status"></param>
		/// <param name="uow"></param>
		/// <returns></returns>
		public async Task UpdateAsync(long MCID, int Status, IUnitOfWork uow)
		{
			if (uow is null) {
				throw new ArgumentNullException(nameof(uow));
			}
			var LastTime = DateTime.Now;
			var sql = $"update  {Sql.Table<MemberCouponGive>(uow.SqlDialect)} set  Status=@Status,LastTime=@LastTime where MCID=@MCID";

			await uow.Connection.ExecuteAsync(sql.CheckSafeSql(), new { MCID, Status, LastTime }, uow.Transaction).ConfigureAwait(true);
		}
	}
}
