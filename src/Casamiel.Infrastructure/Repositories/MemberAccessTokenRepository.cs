﻿using Casamiel.Domain;
using Casamiel.Domain.Entity;

using Dapper;
using Smooth.IoC.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Casamiel.Infrastructure.Repositories
{
	/// <summary>
	/// 
	/// </summary>
	public  class MemberAccessTokenRepository : BaseRepository<MemberAccessToken, int>, IMemberAccessTokenRepository
	{
		public MemberAccessTokenRepository(IDbFactory factory) : base(factory)
		{
		}
		public async	Task<MemberAccessToken> FindAsync<TSession>(string mobile, int loginType) where TSession : class, ISession
		{
			using (var uow = Factory.Create<TSession>())
			{
				return await uow.Connection.QuerySingleOrDefaultAsync<MemberAccessToken>($"SELECT top 1 * FROM {Sql.Table<MemberAccessToken>(uow.SqlDialect)} where mobile=@mobile and Login_Type=@loginType", new { mobile, loginType })
                    .ConfigureAwait(false);
			}
		}
	}
}
